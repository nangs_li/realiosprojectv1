//
//  MWPhoto+Custom.h
//  productionreal2
//
//  Created by Derek Cheung on 16/12/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "MWPhoto.h"

@interface MWPhoto ()

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSURL *photoURL;

@end