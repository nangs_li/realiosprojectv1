//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"
@interface GetChatMessageToJsqMessageArrayModel : BaseModel
+ (GetChatMessageToJsqMessageArrayModel *)shared;
@property(nonatomic, assign) int agentListingIDCount;
@property(nonatomic, assign) int hasGetAgentListingIDCount;
- (void)whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:
(QBChatDialog *)dialog;
- (void)addMessagesFromQBToJSQMessageArrayForDialog:(QBChatDialog *)dialog;

- (void)getChatMessageFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog;
- (void)getChatMessageUntilAgentListingIDFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog;
//// getEarlierChatMessageFromDBToJsqmessagearray
- (void)getEarlierChatMessageFromDBToJsqMessageArrayForDialog:
(QBChatDialog *)dialog chatRoom:(Chatroom *)chatRoom;
- (void)getEarlierChatMessageeUntilAgentListingIDFromDBToJsqMessageArrayForDialog:
(QBChatDialog *)dialog chatRoom:(Chatroom *)chatRoom;



@end

//@implementation Photos

//@end