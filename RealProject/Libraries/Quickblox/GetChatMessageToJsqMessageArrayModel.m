//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GetChatMessageToJsqMessageArrayModel.h"
#import "DBQBChatMessage.h"
#import "Chatroom.h"
#import "ChatRelationModel.h"
#import "DBHasBeenDownloadPhoto.h"
#import "ReceiveMessageCount.h"
#import "ChatRoomSendMessageModel.h"
@implementation GetChatMessageToJsqMessageArrayModel

static GetChatMessageToJsqMessageArrayModel
*sharedGetChatMessageToJsqMessageArrayModel;

+ (GetChatMessageToJsqMessageArrayModel *)shared {
    @synchronized(self) {
        if (!sharedGetChatMessageToJsqMessageArrayModel) {
            sharedGetChatMessageToJsqMessageArrayModel =
            [[GetChatMessageToJsqMessageArrayModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
            
        }
        return sharedGetChatMessageToJsqMessageArrayModel;
    }
}
#pragma mark getMessageFromQuickBloxToChatServiceJSQMessageArrayAndDB-----------------------------------------------------------------------------------
//// DBhaveNotMessageSyncMessages
- (void)whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:
(QBChatDialog *)dialog  {
    NSMutableDictionary *extendedRequest = [[NSMutableDictionary alloc] init];
    [QBRequest countOfMessagesForDialogID:dialog.ID extendedRequest:nil successBlock:^(QBResponse *  response, NSUInteger count) {
       
        QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
        int skip;
        if (count>=100) {
          skip  =(int)count-100;
        }else{
            skip=0;
        }
        
        
        extendedRequest[@"skip"] = @(skip);
        DDLogInfo(@"start get message from dialogid throug quickblox");
        [QBRequest messagesWithDialogID:dialog.ID
                        extendedRequest:extendedRequest
                                forPage:page
                           successBlock:^(QBResponse *response, NSArray *messages,
                                          QBResponsePage *page) {
                               DDLogInfo(
                                         @"start get message from dialogid throug quickblox : sucessBlock "
                                         @"excuting");
                               if (messages.count > 0) {
                                   [[ChatService shared]
                                    addMessagesWhenDBNotMessageToJSQMessageArray:messages
                                    forDialogId:dialog.ID];
                                   
                                   [[NSNotificationCenter defaultCenter]
                                    postNotificationName:kNotificationGetMessageCellTopLabelDateIndexPathArray
                                    object:nil];
                               }
                           }
                             errorBlock:^(QBResponse *response) {
                                 DDLogError(@"QBResponse %@", response);
                                 
                                 
                             }];
        DDLogInfo(@"start get message from dialogid throug quickblox : exit ");

    } errorBlock:^(QBResponse * _Nonnull response) {
        
    }];
   }
//// syncMessages
- (void)addMessagesFromQBToJSQMessageArrayForDialog:(QBChatDialog *)dialog
{
    NSMutableDictionary *extendedRequest = [[NSMutableDictionary alloc] init];
    DBResultSet *r = [[[[[DBQBChatMessage query]
                         whereWithFormat:@" DialogID = %@ AND RecipientID =%@", dialog.ID,
                         [LoginBySocialNetworkModel shared].myLoginInfo.QBID]
                        orderByDescending:@"DateSent"] limit:1] fetch];
    
    for (DBQBChatMessage *dbqbchatmessage in r) {
        extendedRequest[@"date_sent[gte]"] =
        @([dbqbchatmessage.DateSent timeIntervalSince1970] + 1);
    }
    if (r.count==0) {
        return;
    }
   // extendedRequest[@"sort_asc"] = @"_id";
    
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    DDLogInfo(@"start get message from dialogid throug quickblox");
    [QBRequest messagesWithDialogID:dialog.ID
                    extendedRequest:extendedRequest
                            forPage:page
                       successBlock:^(QBResponse *response, NSArray *messages,
                                      QBResponsePage *page) {
                           DDLogInfo(
                                     @"start get message from dialogid throug quickblox : sucessBlock "
                                     @"excuting");
                           
                           if (messages.count > 0) {
                               QBChatMessage *firstMessageFromQBMessageArray = [messages lastObject];
                               DBResultSet *r = [[[DBQBChatMessage query]
                                                  whereWithFormat:@" MessageID = %@",
                                                  firstMessageFromQBMessageArray.ID] fetch];
                               
                               if (r.count == 0) {
                                   
                                   
                                   for (QBChatMessage *message in messages) {
                                       //// check duplicate message
                                       DBResultSet *r = [[[DBQBChatMessage query]
                                                          whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
                                       if (r.count == 0) {
                                           
                                           
                                           if ([[[ReceiveMessageCount query]
                                                 whereWithFormat:@"DialogID = %@", dialog.ID] count] == 0) {
                                               ReceiveMessageCount *muteCaseReceiveMessageCount =
                                               [ReceiveMessageCount new];
                                               muteCaseReceiveMessageCount.DialogID = dialog.ID;
                                               muteCaseReceiveMessageCount.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
                                               muteCaseReceiveMessageCount.LastMessageText = message.text;
                                               muteCaseReceiveMessageCount.MemberID =  [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
                                               muteCaseReceiveMessageCount.LastMessageDate = message.dateSent;
                                               muteCaseReceiveMessageCount.ReceiveMessageCount = 1;
                                               
                                               [muteCaseReceiveMessageCount commit];
                                           } else {
                                               DBResultSet *r = [[[ReceiveMessageCount query]
                                                                  whereWithFormat:@"DialogID = %@", dialog.ID] fetch];
                                               for (ReceiveMessageCount *muteCaseReceiveMessageCount in r) {
                                                   muteCaseReceiveMessageCount.ReceiveMessageCount =
                                                   muteCaseReceiveMessageCount.ReceiveMessageCount + 1;
                                                   muteCaseReceiveMessageCount.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
                                                   muteCaseReceiveMessageCount.LastMessageText = message.text;
                                                   muteCaseReceiveMessageCount.MemberID =  [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
                                                   muteCaseReceiveMessageCount.LastMessageDate = message.dateSent;
                                                   [muteCaseReceiveMessageCount commit];
                                               }
                                           }
                                           
                                       }
                                   }
                                   [[ChatService shared] addMessagesToJSQMessageArray:messages
                                                                          forDialogId:dialog.ID];
                                   [[ChatRelationModel shared]
                                    getCurrentChatRoomRelationObjectFromLocalForDialogId:dialog.ID];
                                   
                                   if (![ChatRelationModel shared]
                                       .currentChatRoomRelationObjectIsEmpty &&
                                       [ChatRelationModel shared].recipienerChatRoomRelation.Muted) {
                                   } else {
                                       AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                                       AudioServicesPlaySystemSound(1003);
                                   }
                                   
                                   [[NSNotificationCenter defaultCenter]
                                    postNotificationName:kNotificationChatRoomReadAllReceivceMessage
                                    object:nil];
                                   [[NSNotificationCenter defaultCenter]
                                    postNotificationName:kNotificationChatRoomReloadCollectionViewAndScrollToBottomYesAnimated
                                    object:nil];
                                   [[NSNotificationCenter defaultCenter]
                                    postNotificationName:kNotificationChatRoomGetImageFromDBQBChatMessageToMWPhotoBrower
                                    object:nil];
                                   
                                   [[NSNotificationCenter defaultCenter]
                                    postNotificationName:kNotificationGetMessageCellTopLabelDateIndexPathArray
                                    object:nil];
                               }
                           }
                           
                           
                           
                       }
                         errorBlock:^(QBResponse *response) {
                             DDLogError(@"QBResponse %@", response);
                             
                             
                         }];
    DDLogInfo(@"start get message from dialogid throug quickblox : exit ");
}
#pragma mark getMessageFromDataBaseToChatServiceJSQMessageArray-----------------------------------------------------------------------------------
//// getChatMessageFromDBToJsqmessagearray
- (void)getChatMessageFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog
{
    if (!dialog.ID) {
        return;
    }
    DBResultSet *r = [[[[[DBQBChatMessage query]
                         whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", dialog.ID]
                        orderByDescending:@"Id"] limit:20]
                      
                      fetch];
    NSArray *r2 = [[r reverseObjectEnumerator] allObjects];
    
    NSMutableArray *jsqmessagearray = [[NSMutableArray alloc] init];
    NSMutableArray *messagearray = [[NSMutableArray alloc] init];
    //// getChatMessageFromDBToJsqmessagearray
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    //  [manager.imageDownloader setMaxConcurrentDownloads:2];
    
    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
    for (DBQBChatMessage *p in r2) {
        [messagearray addObject:p];
        JSQMessage *jsqmessage;
        //// Is  Photo Message
        if (![self isEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
            NSURL *url =
            [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];
            
            JSQPhotoMediaItem *photoItem;
            //// Photo Message  Photo Exists
            if ([manager diskImageExistsForURL:url]) {
                photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                
                photoItem = [[JSQPhotoMediaItem alloc]
                             initWithImage:
                             [manager.imageCache
                              imageFromDiskCacheForKey:[p.CustomParameters
                                                        objectForKey:@"photourl"]]];
                
            } else {
                //// Photo Message  Photo Not Exists Download start
                photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                
                [manager downloadImageWithURL:url
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code
                                     }
                                    completed:^(UIImage *image, NSError *error,
                                                SDImageCacheType cacheType, BOOL finished,
                                                NSURL *imageURL) {
                                         photoItem.image = image;
                                        [[NSNotificationCenter defaultCenter]
                                         postNotificationName:kChatRoomDownLoadPhotoWaitingListToReloadView
                                         object:nil];
                                        if (image&&![[jsqmessage senderId] isEqualToString:[LoginBySocialNetworkModel shared].myLoginInfo.QBID]) {
                                            photoItem.image = image;
                                            DBResultSet *r = [[[[DBChatSetting query]
                                                                whereWithFormat:@"MemberID = %@",
                                                                [LoginBySocialNetworkModel shared]
                                                                .myLoginInfo.MemberID] limit:1]
                                                              
                                                              fetch];
                                            for (DBChatSetting *dbChatSetting in r) {
                                                if (dbChatSetting.ChatRoomPhotoAutoDownload) {
                                                    DDLogDebug(@"DBHasBeenDownloadPhoto %lu",(unsigned long)[[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                                                                              fetch]count]);
                                                    if ([[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                          fetch]count]==0) {
                                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                        [DBHasBeenDownloadPhoto createSearchAgentHistoryWithPhotoUrl:[p.CustomParameters objectForKey:@"photourl"]];
                                                    }
                                                }
                                            }
                                        }
                                    }];
            }
            
            if ([[LoginBySocialNetworkModel shared].myLoginInfo.QBID integerValue] ==
                p.SenderID) {
                photoItem.appliesMediaViewMaskAsOutgoing = YES;
            } else {
                photoItem.appliesMediaViewMaskAsOutgoing = NO;
            }
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          media:photoItem];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        } else {
            //// Is   Message      Not Photo Message
            NSString *text = p.Text;
            if ([self isEmpty:p.Text]) {
                text = @"";
            }
            
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          text:text];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        }
        [jsqmessagearray addObject:jsqmessage];
    }
    if (![self
          isEmpty:[[ChatService shared]
                   .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
              [[ChatService shared].chatroomJsqMessageArray removeObjectForKey:dialog.ID];
          }
    
    [[ChatService shared].chatroomJsqMessageArray setObject:jsqmessagearray
                                                     forKey:dialog.ID];
    //  NSArray *convertedMessageArray = [[ChatService shared]convertRawRoomMessagArrayToSectionMsgArray:jsqmessagearray];
    //  [[ChatService shared]updateChatroomMessage:convertedMessageArray dialogId:dialog.ID];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationChatRoomReloadCollectionViewAndScrollToBottomYesAnimated
     object:nil];
    
}

//// getChatMessageFromDBToJsqmessagearray
- (void)getChatMessageUntilAgentListingIDFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog
{
    
    
    DBResultSet *r = [[[[DBQBChatMessage query]
                        whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", dialog.ID]
                       orderByDescending:@"Id"]fetch];
    
    
    NSMutableArray *jsqmessagearray = [[NSMutableArray alloc] init];
    NSMutableArray *messagearray = [[NSMutableArray alloc] init];
    //// getChatMessageFromDBToJsqmessagearray
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    //  [manager.imageDownloader setMaxConcurrentDownloads:2];
    
    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
    
    for (DBQBChatMessage *p in r) {
        [messagearray addObject:p];
        
        
        JSQMessage *jsqmessage;
        //// Is  Photo Message
        if (![self isEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
            NSURL *url =
            [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];
            
            JSQPhotoMediaItem *photoItem;
            //// Photo Message  Photo Exists
            if ([manager diskImageExistsForURL:url]) {
                photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                
                photoItem = [[JSQPhotoMediaItem alloc]
                             initWithImage:
                             [manager.imageCache
                              imageFromDiskCacheForKey:[p.CustomParameters
                                                        objectForKey:@"photourl"]]];
                
            } else {
                //// Photo Message  Photo Not Exists Download start
                photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                
                [manager downloadImageWithURL:url
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code
                                     }
                                    completed:^(UIImage *image, NSError *error,
                                                SDImageCacheType cacheType, BOOL finished,
                                                NSURL *imageURL) {
                                         photoItem.image = image;
                                        [[NSNotificationCenter defaultCenter]
                                         postNotificationName:kChatRoomDownLoadPhotoWaitingListToReloadView
                                         object:nil];
                                        if (image&&![[jsqmessage senderId] isEqualToString:[LoginBySocialNetworkModel shared].myLoginInfo.QBID]) {
                                            photoItem.image = image;
                                            DBResultSet *r = [[[[DBChatSetting query]
                                                                whereWithFormat:@"MemberID = %@",
                                                                [LoginBySocialNetworkModel shared]
                                                                .myLoginInfo.MemberID] limit:1]
                                                              
                                                              fetch];
                                            for (DBChatSetting *dbChatSetting in r) {
                                                if (dbChatSetting.ChatRoomPhotoAutoDownload) {
                                                    DDLogDebug(@"DBHasBeenDownloadPhoto %lu",(unsigned long)[[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                                                                              fetch]count]);
                                                    if ([[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                          
                                                          fetch]count]==0) {
                                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                        [DBHasBeenDownloadPhoto createSearchAgentHistoryWithPhotoUrl:[p.CustomParameters objectForKey:@"photourl"]];
                                                    }
                                                }
                                            }
                                        }
                                    }];
            }
            
            if ([[LoginBySocialNetworkModel shared].myLoginInfo.QBID integerValue] ==
                p.SenderID) {
                photoItem.appliesMediaViewMaskAsOutgoing = YES;
            } else {
                photoItem.appliesMediaViewMaskAsOutgoing = NO;
            }
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          media:photoItem];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        } else {
            //// Is   Message      Not Photo Message
            [self fillDBQBChatMessageNullValue:p];
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          text:p.Text];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        }
        
        [jsqmessagearray addObject:jsqmessage];
        if ([jsqmessage.messageType isEqualToString:kChatMessageTypeAgentListing]) {
            [ChatRoomSendMessageModel shared].currentListingID = [jsqmessage.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing];
            self.agentListingIDCount=1;
            break;
        }
        
    }
    if (![self
          isEmpty:[[ChatService shared]
                   .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
              [[ChatService shared].chatroomJsqMessageArray removeObjectForKey:dialog.ID];
          }
    
    
    NSArray *r2 = [[jsqmessagearray reverseObjectEnumerator] allObjects];
    [[ChatService shared].chatroomJsqMessageArray setObject:r2
                                                     forKey:dialog.ID];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationChatRoomReloadCollectionViewAndScrollToBottomYesAnimated
     object:nil];
}
//// getEarlierChatMessageFromDBToJsqmessagearray
- (void)getEarlierChatMessageFromDBToJsqMessageArrayForDialog:
(QBChatDialog *)dialog chatRoom:(Chatroom *)chatRoom {
    if (chatRoom.haveNotCreateDialog) {
        return;
    }
    
    if ([self isEmpty:[[ChatService shared]
                       .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
        return;
    }
    NSMutableArray *jsqmessagsForDialogId =
    [[ChatService shared].chatroomJsqMessageArray objectForKey:dialog.ID];
    
    DBResultSet *r = [[[[[[DBQBChatMessage query]
                          whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", dialog.ID]
                         orderByDescending:@"Id"] offset:(int)jsqmessagsForDialogId.count] limit:20] fetch];
    NSArray *r2 = [[r reverseObjectEnumerator] allObjects];
    NSMutableArray *jsqmessagearray = [[NSMutableArray alloc] init];
    //// getEarlierChatMessageFromDBToJsqmessagearray
    if (r2.count == 0) {
        [[[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                                    message:@"Not Earlier Message"
                                   delegate:self
                          cancelButtonTitle:@"OK!"
                          otherButtonTitles:nil] show];
        
        return;
    }
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    //  [manager.imageDownloader setMaxConcurrentDownloads:2];
    
    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
#pragma mark insert Data From DataBase To JsqMessageArray
    for (DBQBChatMessage *p in r2) {
        JSQMessage *jsqmessage;
        //// Is photomessage  add to jsqmessagearray
        if (![self isEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
            NSURL *url =
            [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];
            
            JSQPhotoMediaItem *photoItem;
            
            if ([manager diskImageExistsForURL:url]) {
                photoItem = [[JSQPhotoMediaItem alloc]
                             initWithImage:
                             [manager.imageCache
                              imageFromDiskCacheForKey:[p.CustomParameters
                                                        objectForKey:@"photourl"]]];
                
            } else {
                photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                
                [manager downloadImageWithURL:url
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code
                                     }
                                    completed:^(UIImage *image, NSError *error,
                                                SDImageCacheType cacheType, BOOL finished,
                                                NSURL *imageURL) {
                                         photoItem.image = image;
                                        [[NSNotificationCenter defaultCenter]
                                         postNotificationName:kChatRoomDownLoadPhotoWaitingListToReloadView
                                         object:nil];
                                        if (image&&![[jsqmessage senderId] isEqualToString:[LoginBySocialNetworkModel shared].myLoginInfo.QBID]) {
                                            photoItem.image = image;
                                            DBResultSet *r = [[[[DBChatSetting query]
                                                                whereWithFormat:@"MemberID = %@",
                                                                [LoginBySocialNetworkModel shared]
                                                                .myLoginInfo.MemberID] limit:1]
                                                              
                                                              fetch];
                                            for (DBChatSetting *dbChatSetting in r) {
                                                if (dbChatSetting.ChatRoomPhotoAutoDownload) {
                                                    DDLogDebug(@"DBHasBeenDownloadPhoto %lu",(unsigned long)[[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                                                                              fetch]count]);
                                                    if ([[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                          fetch]count]==0) {
                                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                        [DBHasBeenDownloadPhoto createSearchAgentHistoryWithPhotoUrl:[p.CustomParameters objectForKey:@"photourl"]];
                                                    }
                                                }
                                            }
                                        }
                                    }];
            }
            
            if ([[LoginBySocialNetworkModel shared].myLoginInfo.QBID integerValue] ==
                p.SenderID) {
                photoItem.appliesMediaViewMaskAsOutgoing = YES;
            } else {
                photoItem.appliesMediaViewMaskAsOutgoing = NO;
            }
            DDLogInfo(@"[p.CustomParameters objectForKey:@photourl]]-->%@",
                      [p.CustomParameters objectForKey:@"photourl"]);
            
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          media:photoItem];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        } else {
            //// Is Message Not Photo Message   add to jsqmessagearray
            NSString *text = p.Text;
            if ([self isEmpty:p.Text]) {
                text = @"";
            }
            
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          text:text];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        }
        [jsqmessagearray addObject:jsqmessage];
        //      (@"DBQBChatDialog-->%@",p);
    }
    
    if (![self
          isEmpty:[[ChatService shared]
                   .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
              NSMutableArray *jsqmessagsForDialogId =
              [[ChatService shared].chatroomJsqMessageArray objectForKey:dialog.ID];
              
              [jsqmessagearray addObjectsFromArray:jsqmessagsForDialogId];
              //    NSArray *convertedMessageArray = [[ChatService shared]convertRawRoomMessagArrayToSectionMsgArray:jsqmessagearray];
              //   [[ChatService shared]updateChatroomMessage:convertedMessageArray dialogId:dialog.ID];
              [[ChatService shared].chatroomJsqMessageArray setObject:jsqmessagearray
                                                               forKey:dialog.ID];
          }
    [chatRoom getMessageCellTopLabelDateIndexPathArray];
    NSMutableArray *indexPaths = [NSMutableArray new];
    for (int i = 0; i < r2.count; i ++) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    CGFloat bottomOffset = chatRoom.collectionView.contentSize.height - chatRoom.collectionView.contentOffset.y;
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    [chatRoom.collectionView performBatchUpdates:^{
        [chatRoom.collectionView insertItemsAtIndexPaths:indexPaths];
    } completion:^(BOOL finished) {
        chatRoom.collectionView.contentOffset = CGPointMake(0, chatRoom.collectionView.contentSize.height - bottomOffset);
        [CATransaction commit];
    }];
}
- (void)getEarlierChatMessageeUntilAgentListingIDFromDBToJsqMessageArrayForDialog:
(QBChatDialog *)dialog chatRoom:(Chatroom *)chatRoom{
    if (chatRoom.haveNotCreateDialog) {
        return;
    }
    
    if ([self isEmpty:[[ChatService shared]
                       .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
        
        
        return;
    }
    NSMutableArray *jsqmessagsForDialogId =
    [[ChatService shared].chatroomJsqMessageArray objectForKey:dialog.ID];
    
    DBResultSet *r = [[[[DBQBChatMessage query]
                        whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", dialog.ID]
                       orderByDescending:@"Id"]  fetch];
    NSNumber *jsqMessageArrayCount = @(jsqmessagsForDialogId.count);
    NSMutableArray *jsqmessagearray = [[NSMutableArray alloc] init];
    //// getEarlierChatMessageFromDBToJsqmessagearray
    if (r.count == 0) {
        [[[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                                    message:@"Not Earlier Message"
                                   delegate:self
                          cancelButtonTitle:@"OK!"
                          otherButtonTitles:nil] show];
        
        return;
    }
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    //  [manager.imageDownloader setMaxConcurrentDownloads:2];
    
    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
    self.hasGetAgentListingIDCount=0;
    for (DBQBChatMessage *p in r) {
        if ([self isEmpty:p.Text]) {
            p.Text = @"";
        }
        if ([self isEmpty:p.DateSent]) {
            p.DateSent=[NSDate date];
        }
        JSQMessage *jsqmessage;
        //// Is photomessage  add to jsqmessagearray
        if (![self isEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
            NSURL *url =
            [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];
            
            JSQPhotoMediaItem *photoItem;
            
            if ([manager diskImageExistsForURL:url]) {
                photoItem = [[JSQPhotoMediaItem alloc]
                             initWithImage:
                             [manager.imageCache
                              imageFromDiskCacheForKey:[p.CustomParameters
                                                        objectForKey:@"photourl"]]];
                
            } else {
                photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                
                [manager downloadImageWithURL:url
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code
                                     }
                                    completed:^(UIImage *image, NSError *error,
                                                SDImageCacheType cacheType, BOOL finished,
                                                NSURL *imageURL) {
                                         photoItem.image = image;
                                        [[NSNotificationCenter defaultCenter]
                                         postNotificationName:kChatRoomDownLoadPhotoWaitingListToReloadView
                                         object:nil];
                                        if (image&&![[jsqmessage senderId] isEqualToString:[LoginBySocialNetworkModel shared].myLoginInfo.QBID]) {
                                            photoItem.image = image;
                                            DBResultSet *r = [[[[DBChatSetting query]
                                                                whereWithFormat:@"MemberID = %@",
                                                                [LoginBySocialNetworkModel shared]
                                                                .myLoginInfo.MemberID] limit:1]
                                                              
                                                              fetch];
                                            for (DBChatSetting *dbChatSetting in r) {
                                                if (dbChatSetting.ChatRoomPhotoAutoDownload) {
                                                    DDLogDebug(@"DBHasBeenDownloadPhoto %lu",(unsigned long)[[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                                                                              fetch]count]);
                                                    if ([[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [p.CustomParameters objectForKey:@"photourl"]]
                                                          fetch]count]==0) {
                                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                        [DBHasBeenDownloadPhoto createSearchAgentHistoryWithPhotoUrl:[p.CustomParameters objectForKey:@"photourl"]];
                                                    }
                                                }
                                            }
                                        }
                                    }];
            }
            
            if ([[LoginBySocialNetworkModel shared].myLoginInfo.QBID integerValue] ==
                p.SenderID) {
                photoItem.appliesMediaViewMaskAsOutgoing = YES;
            } else {
                photoItem.appliesMediaViewMaskAsOutgoing = NO;
            }
            DDLogInfo(@"[p.CustomParameters objectForKey:@photourl]]-->%@",
                      [p.CustomParameters objectForKey:@"photourl"]);
            
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          media:photoItem];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        } else {
            //// Is Message Not Photo Message   add to jsqmessagearray
            
            jsqmessage = [[JSQMessage alloc]
                          initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
                          senderDisplayName:@"None"
                          date:p.DateSent
                          text:p.Text];
            [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
        }
        
        
        [jsqmessagearray addObject:jsqmessage];
        if ([jsqmessage.messageType isEqualToString:kChatMessageTypeAgentListing]) {
            
            if (self.hasGetAgentListingIDCount==self.agentListingIDCount) {
                self.agentListingIDCount++;
                break;
            }
            self.hasGetAgentListingIDCount++;
            
            
        }
        //      (@"DBQBChatDialog-->%@",p);
    }
    
    if (![self isEmpty:[[ChatService shared]
                        .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
        NSArray *r2 = [[jsqmessagearray reverseObjectEnumerator] allObjects];
        //    NSArray *convertedMessageArray = [[ChatService shared]convertRawRoomMessagArrayToSectionMsgArray:jsqmessagearray];
        //   [[ChatService shared]updateChatroomMessage:convertedMessageArray dialogId:dialog.ID];
        [[ChatService shared].chatroomJsqMessageArray setObject:r2
                                                         forKey:dialog.ID];
        
        NSMutableArray *indexPaths = [NSMutableArray new];
        for (int i = 0; i < r2.count-jsqMessageArrayCount.integerValue; i ++) {
            [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
        }
        [chatRoom getMessageCellTopLabelDateIndexPathArray];
        CGFloat bottomOffset = chatRoom.collectionView.contentSize.height - chatRoom.collectionView.contentOffset.y;
        
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        
        [chatRoom.collectionView performBatchUpdates:^{
            [chatRoom.collectionView insertItemsAtIndexPaths:indexPaths];
        } completion:^(BOOL finished) {
            chatRoom.collectionView.contentOffset = CGPointMake(0, chatRoom.collectionView.contentSize.height - bottomOffset);
            [CATransaction commit];
        }];
        
        
    }
    
}
@end