//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatDialogModel.h"
#import "DBQBChatDialog.h"
#import "DBQBUUser.h"
#import "DBChatSetting.h"
#import "GetChatMessageToJsqMessageArrayModel.h"
#import "DBQBChatDialogDelete.h"
#import "ReceiveMessageCount.h"
#import "ChatRelationModel.h"
#import "DBQBChatDialogClearMessage.h"
#import "DialogPageAgentProfilesModel.h"
#import "DBChatroomAgentProfiles.h"
@implementation ChatDialogModel

static ChatDialogModel *sharedChatDialogModel;

+ (ChatDialogModel *)shared {
  @synchronized(self) {
    if (!sharedChatDialogModel) {
      sharedChatDialogModel = [[ChatDialogModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedChatDialogModel;
  }
}
#pragma mark requestDialogsWithCompletionBlock----------------------------------------------------------------------------------------------------
- (void)requestDialogsWithCompletionBlock:(void (^)())completionBlock {
    QBResponsePage * responsePage = [QBResponsePage responsePageWithLimit:1000];
    [QBRequest dialogsForPage:responsePage extendedRequest:nil successBlock:^(QBResponse *  response, NSArray  *  dialogObjects,NSSet *  dialogsUsersIDs, QBResponsePage *  page){

    if ([[AppDelegate getAppDelegate] isEmpty:dialogObjects]) {
        completionBlock();
    }
    /**
     *   remove Delete Status Dialog
     */
      
      
       for (QBChatDialog *chatdialog in dialogObjects) {
                            DBResultSet *    r = [[[DBChatroomAgentProfiles query]
                                                   whereWithFormat:
                                                   @"AgentProfileQBID =%@",
                                                   [chatdialog getRecipientIDFromQBChatDialog ]]
                                                  fetch];

      
      if (r.count==0) {
          self.chatlistQBidarray =[[NSMutableArray alloc]init];
          NSString * recipientQBID =[chatdialog getRecipientIDFromQBChatDialog];
               [self.chatlistQBidarray addObject:recipientQBID];
      }
       }
    [[[[DBQBChatDialog query]
        whereWithFormat:@" MemberID = %@",
                        [LoginBySocialNetworkModel shared]
                            .myLoginInfo.MemberID] fetch] removeAll];
    NSMutableArray *dialogObjectsMutableArray = [[NSMutableArray alloc] init];
    for (QBChatDialog *chatDialog in dialogObjects.mutableCopy) {
      if ([[[DBQBChatDialogDelete query]
              whereWithFormat:@" DialogID = %@", chatDialog.ID] count] == 0) {
     
        [dialogObjectsMutableArray addObject:chatDialog];
      }
    }

    self.dialogs = dialogObjectsMutableArray;
      if (!self.chatlistQBidarray.count == 0) {
          [[DialogPageAgentProfilesModel shared]AgentProfileGetListtFromchatlistQBidarray:self.chatlistQBidarray numberofStartRecord:0 success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
              [[DialogPageAgentProfilesModel shared]getAllDBDialogPageSearchAgentProfiles];
              if (completionBlock != nil) {
                  completionBlock();
              }
              [[NSNotificationCenter defaultCenter]
               postNotificationName:kNotificationDialogsUpdated
               object:nil];
          } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
              if (completionBlock != nil) {
                  completionBlock();
              }
                     }];
      }else{
          
          [[DialogPageAgentProfilesModel shared]getAllDBDialogPageSearchAgentProfiles];
          if (completionBlock != nil) {
              completionBlock();
          }
             }
      

    ////store QBChatDialog into local

    for (QBChatDialog *chatdialog in dialogObjectsMutableArray) {
      DDLogInfo(@"chatDialog.recipientID : %ld", (long)chatdialog.recipientID);
     
      DBQBChatDialog *dbqbchatdialog = [DBQBChatDialog new];
      dbqbchatdialog.LastMessageDate = chatdialog.lastMessageDate;
      dbqbchatdialog.DialogID = chatdialog.ID;
//      dbqbchatdialog.LastMessageText = [self getLastMessageFromQBChatDialog:chatdialog];
      dbqbchatdialog.LastMessageUserID = chatdialog.lastMessageUserID;
        if ([[[DBQBChatDialogClearMessage query]
              whereWithFormat:@" DialogID = %@", chatdialog.ID] count] == 0) {
        }else{
            chatdialog.lastMessageDate=nil;
            chatdialog.lastMessageText=@"";

        }
        dbqbchatdialog.QBChatDialog =
          [NSKeyedArchiver archivedDataWithRootObject:[chatdialog copy]];
      dbqbchatdialog.MemberID =
          [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];

      dbqbchatdialog.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
      [[ChatRelationModel shared]
          getCurrentChatRoomRelationObjectFromLocalForDialogId:chatdialog.ID];

      
      
    
      [dbqbchatdialog commit];
     //  if (chatdialog.unreadMessagesCount > 0) {
     //   [[GetChatMessageToJsqMessageArrayModel shared]
     //       syncMessagesForDialogId:chatdialog.ID];
    //  }
    }
          


  } errorBlock:^(QBResponse *  response){
      DDLogError(@"QBResponse *  response %@",response);
       completionBlock();
  }];
}
- (void)requestDialogUpdateWithId:(NSString *)dialogId
                  completionBlock:(void (^)())completionBlock {
  [QBRequest
       dialogsForPage:nil
      extendedRequest:@{
        @"_id" : dialogId
      } successBlock:^(QBResponse *response, NSArray *dialogObjects,
                       NSSet *dialogsUsersIDs, QBResponsePage *page) {

          
          
        BOOL found = NO;
        NSArray *dialogsCopy = [NSArray arrayWithArray:self.dialogs];
        for (QBChatDialog *dialog in dialogsCopy) {
          if ([dialog.ID isEqualToString:dialogId]) {
            [self.dialogs removeObject:dialog];
            found = YES;

            break;
          }
        }
       QBChatDialog * qbChatDialog = dialogObjects.firstObject;
        if (![self isEmpty:qbChatDialog]) {
            QBChatDialog * chatdialog = dialogObjects.firstObject;
            DBResultSet *    r = [[[[DBQBChatDialog query]
                                    whereWithFormat:@"MemberID = %@ AND DialogID =%@",
                                    [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,chatdialog.ID]
                                   orderByDescending:@"LastMessageDate"]
                                  
                                  fetch];
            
            if (r.count==0) {
                NSMutableArray * chatlistQBidarray =[[NSMutableArray alloc]init];
                NSString * recipientQBID =[chatdialog getRecipientIDFromQBChatDialog];
                [chatlistQBidarray addObject:recipientQBID];
                [[DialogPageAgentProfilesModel shared]AgentProfileGetListtFromchatlistQBidarray:chatlistQBidarray numberofStartRecord:0 success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                    
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:kNotificationDialogsUpdated
                     object:nil];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                    
                }];
                
            }

          [self.dialogs insertObject:dialogObjects.firstObject atIndex:0];
          [[[[DBQBChatDialog query]
              whereWithFormat:@" DialogID = %@", dialogId] fetch] removeAll];

          QBChatDialog *serverreturnchatdialog = dialogObjects.firstObject;
          DBQBChatDialog *dbqbchatdialog = [DBQBChatDialog new];
          dbqbchatdialog.LastMessageDate =
              serverreturnchatdialog.lastMessageDate;
          dbqbchatdialog.LastMessageUserID =
              serverreturnchatdialog.lastMessageUserID;
          dbqbchatdialog.DialogID = serverreturnchatdialog.ID;
          dbqbchatdialog.QBChatDialog = [NSKeyedArchiver
              archivedDataWithRootObject:[serverreturnchatdialog copy]];
          dbqbchatdialog.MemberID = [[LoginBySocialNetworkModel shared]
                                         .myLoginInfo.MemberID intValue];
          dbqbchatdialog.QBID =
              [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
          [[ChatRelationModel shared]
              getCurrentChatRoomRelationObjectFromLocalForDialogId:dialogId];

          for (QBChatDialog *dialog in
               [NSArray arrayWithArray:[ChatDialogModel shared].dialogs]) {
            if ([dialog.ID isEqualToString:dialogId]) {
              
                
                
                DBResultSet * r =  [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", dialogId]
                                     orderByDescending:@"Id"] limit:1] fetch];

                for (DBQBChatMessage *dbQBChatMessage in r) {
                    dialog.lastMessageDate = dbQBChatMessage.DateSent;
                    dialog.lastMessageText = [dbQBChatMessage getLastMessage];
                    dbqbchatdialog.LastMessageDate = dbQBChatMessage.DateSent;
                    dbqbchatdialog.LastMessageText = [dbQBChatMessage getLastMessage];
                    dbqbchatdialog.QBChatDialog =
                    [NSKeyedArchiver archivedDataWithRootObject:[dialog copy]];
                    
                }
                
            }
          }
            
            
           
          /**
           *  Delete QBChatDialogDeleteList
           */
          [[[[DBQBChatDialogDelete query]
              whereWithFormat:@" DialogID = %@", dialogId] fetch] removeAll];
            [[[[DBQBChatDialogClearMessage query]
               whereWithFormat:@" DialogID = %@", dialogId] fetch] removeAll];
          [dbqbchatdialog commit];
            completionBlock();
        }else{
             completionBlock();
        }
      } errorBlock:^(QBResponse *  response){ completionBlock();}];
}
- (void)setUsers:(NSMutableArray *)users {
  _users = users;

  NSMutableDictionary *__usersAsDictionary = [NSMutableDictionary dictionary];
  for (QBUUser *user in users) {
    [__usersAsDictionary setObject:user forKey:@(user.ID)];
  }

  _usersAsDictionary = [__usersAsDictionary copy];
}
- (void)setDialogs:(NSMutableArray *)dialogs {
  _dialogs = dialogs;

  if (dialogs != nil && dialogs.count > 0) {
    NSMutableDictionary *__dialogsAsDictionary =
        [NSMutableDictionary dictionary];
    for (QBChatDialog *dialog in dialogs) {
      [__dialogsAsDictionary setObject:dialog forKey:dialog.ID];
    }

    _dialogsAsDictionary = [__dialogsAsDictionary mutableCopy];
  }
}
- (void)updateChatBadgeNumber {
  int number = 0;
  for (QBChatDialog *chatdialog in self.dialogs) {
      
      if ([[[[ReceiveMessageCount query]
             whereWithFormat:@"DialogID = %@", chatdialog.ID] fetch]count]>0) {
          number = number + 1;

      }
      
     

    
    
  }

  if (number == 0) {
    [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
        objectAtIndex:2] setBadgeValue:nil];

  } else {
    [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
        objectAtIndex:2]
        setBadgeValue:[NSString stringWithFormat:@"%d", number]];
  }
}

- (NSString *)getLastMessageFromQBChatDialog:(QBChatDialog *)qbChatDialog {
  if (isEmpty(qbChatDialog.lastMessageText)) {
    DBResultSet *r = [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", qbChatDialog.ID] orderByDescending:@"Id"] limit:1] fetch];

    for (DBQBChatMessage *dbQBChatMessage in r) {
      return [dbQBChatMessage getLastMessage];
    }
    return @" ";
  } else {
    return qbChatDialog.lastMessageText;
  }
}
@end