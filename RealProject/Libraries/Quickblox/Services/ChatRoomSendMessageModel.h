//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"
#import "Chatroom.h"
@interface ChatRoomSendMessageModel : BaseModel
+ (ChatRoomSendMessageModel *)shared;
#pragma mark Send  message-------------------------------------------------------------------------------------------------------------------
- (void)sendMessageInsertIntoDataBase:(NSString *)messageText
                            forDialog:(QBChatDialog *)dialog;
- (void)sendCoverViewMessageInsertIntoDataBaseForDialog:(QBChatDialog *)dialog;
#pragma mark Send Photo message-------------------------------------------------------------------------------------------------------------------
@property(nonatomic, retain) NSMutableArray *chatroomchosenImages;
@property(nonatomic, strong) NSString *currentListingID;
- (void)sendChatRoomChosenImagesForDialog:(QBChatDialog *)dialog;
- (void)sendMessageMethodWithPhoto:(QBChatMessage *)message
                        jsqMessage:(JSQMessage *)jsqmessage
                         forDialog:(QBChatDialog *)dialog;
//-(void)sendErrorMessageSendMethodFromDialog:(QBChatDialog *)dialog messageID:(NSString *)messageID completionBlock:(CommonBlock)completionBlock;
-(void)sendMessageMethodDialog:(QBChatDialog *)dialog message:(QBChatMessage *)message completionBlock:(CommonBlock)completionBlock;
@end
