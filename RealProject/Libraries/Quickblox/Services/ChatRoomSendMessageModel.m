//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatRoomSendMessageModel.h"
#import "DBChatSetting.h"
#import "ChatRelationModel.h"
#import "DBUnreadMessageLabel.h"
#import "DBQBChatDialogClearMessage.h"
@implementation ChatRoomSendMessageModel
static ChatRoomSendMessageModel *sharedChatRoomSendMessageModel;

+ (ChatRoomSendMessageModel *)shared {
    @synchronized(self) {
        if (!sharedChatRoomSendMessageModel) {
            sharedChatRoomSendMessageModel = [[ChatRoomSendMessageModel alloc] init];
            sharedChatRoomSendMessageModel.currentListingID = @"0";
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedChatRoomSendMessageModel;
    }
}

#pragma mark Send message-------------------------------------------------------------------------------------------------------------------
- (void)sendMessageInsertIntoDataBase:(NSString *)messageText
                            forDialog:(QBChatDialog *)dialog {
    // create a message
    QBChatMessage *message = [QBChatMessage markableMessage];
    
    message.text = messageText;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [[ChatRelationModel shared]
     getCurrentChatRoomRelationObjectFromLocalForDialogId:dialog.ID];
    
    if (![ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty &&
        [ChatRelationModel shared].mySelfChatRoomRelation.Muted) {
        params[@"save_to_history"] = @"1";
    } else {
        params[@"save_to_history"] = @"1";
    }
    
    if ([ChatService shared].myQBUUserCustomData.chatroomshowreadreceipts &&
        [ChatService shared]
        .recipienterQBUUserCustomData.chatroomshowreadreceipts) {
        params[@"chatroomshowreadreceipts"] = @"true";
    } else {
        params[@"chatroomshowreadreceipts"] = @"false";
    }
    if (isEmpty([ChatService shared]
                .recipienterQBUUserCustomData)||isEmpty([ChatService shared].myQBUUserCustomData)) {
           params[@"chatroomshowreadreceipts"] = @"true";
    }
    NSString *messageType = kChatMessageTypeText;
    if ([messageText isEqualToString:@"//audioAttachment8866"]){
        messageType = kChatMessageTypeAudio;
    }else if([messageText isEqualToString:@"//videoAttachment8866"]){
        messageType = kChatMessageTypeVideo;
    }
    params[kChatMessageCustomParameterMessageType] = messageType;
    
    [params setObject:[LoginBySocialNetworkModel shared].myLoginInfo.MemberName forKey:kChatMessageCustomParameterSenderName];
    [message setCustomParameters:params];
    
    // 1-1 Chat
    
    // send message
    message.recipientID = [dialog recipientID];
    message.senderID = [QBSession currentSession].currentUser.ID;
    JSQMessage * finishSendJsqmessage=  [self sendMessageSaveintoDataBaseDialog:dialog message:message chatMessageType:kChatMessageTypeText jsqmessage:nil];
   
    
    [self sendMessageMethodDialog:dialog message:message completionBlock:^(id response, NSError *error) {
         
         [[ChatService shared] setUpJSQMessage:finishSendJsqmessage
                             fromQBChatMessage:(DBQBChatMessage *)response];
         
         [[NSNotificationCenter defaultCenter]
          postNotificationName:kNotificationChatRoomReloadCollectionView
          object:nil];
    }];
    //save message into DB
   }
-(JSQMessage *)sendMessageSaveintoDataBaseDialog:(QBChatDialog *)dialog message:(QBChatMessage *)message chatMessageType:(NSString *)chatMessageType jsqmessage:(JSQMessage *)jsqmessage{
     DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
    JSQMessage * sendFinishjsqmessage;
    if ([jsqmessage valid]) {
        sendFinishjsqmessage=jsqmessage;
    }
    message=  [self fillQBChatMessageNullValue:message];
    dbhistorymessage.MessageID = message.ID;
    dbhistorymessage.DialogID = dialog.ID;
    dbhistorymessage.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
    dbhistorymessage.MemberID =
    [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    dbhistorymessage.RecipientID = [dialog recipientID];
    dbhistorymessage.SenderID = [QBSession currentSession].currentUser.ID;
    dbhistorymessage.DateSent = message.dateSent;
    dbhistorymessage.InsertDBDateTime = [NSDate date];
    dbhistorymessage.QBChatHistoryMessage = [NSKeyedArchiver archivedDataWithRootObject:message];
    dbhistorymessage.ChatMessageType =chatMessageType;
    
    dbhistorymessage.CustomParameters = message.customParameters;
    dbhistorymessage.Attachments = message.attachments;
    dbhistorymessage.NumberofDeliverStatus=1;
    dbhistorymessage.DidDeliverStatus = NO;
    dbhistorymessage.DrawStatus = YES;
    dbhistorymessage.ReadStatus = NO;
    dbhistorymessage.Retrycount = 0;
    dbhistorymessage.ErrorStatus = NO;
    dbhistorymessage.IsDeleteMessage = NO;
    if ([chatMessageType isEqualToString:kChatMessageTypeText]) {
        dbhistorymessage.PhotoType = NO;
        dbhistorymessage.Text = message.text;
        [dbhistorymessage commit];
        [[[[DBUnreadMessageLabel query]
           whereWithFormat:@" DialogID = %@ And MemberID = %@", dialog.ID,[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] fetch]removeAll];
       sendFinishjsqmessage=  [[ChatService shared]
         addJSQMessageFromDBQBChatMessageToJSSQMessageArray:dbhistorymessage
         forDialogId:dialog.ID];
  return sendFinishjsqmessage;

    }
    if ([chatMessageType isEqualToString:kChatMessageTypeImage]) {
        dbhistorymessage.PhotoType = YES;
        dbhistorymessage.Text = message.text;
        [dbhistorymessage commit];
        [[[[DBUnreadMessageLabel query]
           whereWithFormat:@" DialogID = %@ And MemberID = %@", dialog.ID,[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] fetch]removeAll];
       sendFinishjsqmessage=   [[ChatService shared] setUpJSQMessage:jsqmessage
                            fromQBChatMessage:dbhistorymessage];
          return sendFinishjsqmessage;

    }
    if ([chatMessageType isEqualToString:kChatMessageTypeAgentListing]) {
        dbhistorymessage.PhotoType = NO;
        dbhistorymessage.Text = kChatMessageCustomParameterAgentListing;
        [dbhistorymessage commit];
        sendFinishjsqmessage=   [[ChatService shared]
         addJSQMessageFromDBQBChatMessageToJSSQMessageArray:dbhistorymessage
         forDialogId:dialog.ID];
          return jsqmessage;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kNotificationGetAgentListingGetList
         object:nil];
         return sendFinishjsqmessage;

    }

     return sendFinishjsqmessage;
   
}
- (void)sendCoverViewMessageInsertIntoDataBaseForDialog:(QBChatDialog *)dialog {
    if ([self isEmpty: [AppDelegate getAppDelegate].currentAgentProfile]) {
        return;
    }
    QBChatMessage *message = [QBChatMessage markableMessage];
    
    message.text = kChatMessageTypeAgentListing;
    int agentListingID = 0;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [[ChatRelationModel shared]
     getCurrentChatRoomRelationObjectFromLocalForDialogId:dialog.ID];
    if (![ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty &&
        [ChatRelationModel shared].mySelfChatRoomRelation.Muted) {
        params[@"save_to_history"] = @"0";
    } else {
        params[@"save_to_history"] = @"1";
    }
    
    if (![self isEmpty:[AppDelegate getAppDelegate].currentAgentProfile.AgentListing]) {
        agentListingID = [AppDelegate getAppDelegate].currentAgentProfile.AgentListing.AgentListingID;
        params[kChatMessageCustomParameterAgentListing] = [NSString stringWithFormat:@"%d",agentListingID];
        DDLogDebug(@"self.delegate.currentAgentProfile.AgentListing.AgentListingID %d",agentListingID);
        self.currentListingID = [NSString stringWithFormat:@"%d",agentListingID];
    }
    params[kChatMessageCustomParameterMessageType] = kChatMessageTypeAgentListing;
    [message setCustomParameters:params];
    
    // 1-1 Chat
    // send message
    message.recipientID = [dialog recipientID];
    message.senderID = [QBSession currentSession].currentUser.ID;
    ////save message into DB
  JSQMessage * finishSendJsqmessage=  [self sendMessageSaveintoDataBaseDialog:dialog message:message chatMessageType:kChatMessageTypeAgentListing jsqmessage:nil];
    [self sendMessageMethodDialog:dialog message:message completionBlock:^(id response, NSError *error) {
         [[ChatService shared] setUpJSQMessage:finishSendJsqmessage
                                         fromQBChatMessage:(DBQBChatMessage *)response];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kNotificationChatRoomReloadCollectionView
         object:nil];
        
    }];
    
  
    
}
#pragma mark Send Photo message-------------------------------------------------------------------------------------------------------------------
//// sendchatroomchosenImages
- (void)sendChatRoomChosenImagesForDialog:(QBChatDialog *)dialog {
    if ([self isEmpty:self.chatroomchosenImages]) {
        return;
    } else {
        for (int i = 0; i < [self.chatroomchosenImages count]; i++) {
            if ([self.chatroomchosenImages[i]
                 isEqual:[UIImage imageNamed:@"emptyphoto"]] ||
                [self.chatroomchosenImages[i]
                 isEqual:[UIImage imageNamed:@"collectionviewaddbutton"]]) {
                    DDLogDebug(@"if not equal to image");
                    
                } else {
                    //// resize image size<124000
                    NSData *imageData = [UIImage compressImageToNSData:self.chatroomchosenImages[i] limitedDataSize:300];
                                       DDLogDebug(@"imageData.length-->%lu", (unsigned long)imageData.length);
                    
                    QBChatMessage *qbchatmessage = [QBChatMessage markableMessage];
                    qbchatmessage.text = @"image";
                    // here
                    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc]
                                                    initWithImage:self.chatroomchosenImages[i]];
                    
                    if ([[LoginBySocialNetworkModel shared]
                         .myLoginInfo.QBID integerValue] == qbchatmessage.senderID) {
                        photoItem.appliesMediaViewMaskAsOutgoing = YES;
                    } else {
                        photoItem.appliesMediaViewMaskAsOutgoing = NO;
                    }
                    
                    qbchatmessage.dateSent =[NSDate date];
                    //// show image to chatroom
                    JSQMessage *jsqmessage = [[JSQMessage alloc]
                                              initWithSenderId:
                                              [NSString stringWithFormat:@"%@", @(qbchatmessage.senderID)]
                                              senderDisplayName:@"None"
                                              date:qbchatmessage.dateSent
                                              media:photoItem];
                    jsqmessage.DidDeliverStatus = NO;
                    jsqmessage.DrawStatus = YES;
                    jsqmessage.ReadStatus = NO;
                    jsqmessage.Retrycount = 0;
                    jsqmessage.IsDeleteMessage = NO;
                    
                    [[ChatService shared] addjsqPhotoMessageToJsqMessageArray:jsqmessage
                                                                  forDialogId:dialog.ID];
                    
                    //// upload file
                    [QBRequest TUploadFile:imageData
                                  fileName:@"photo.png"
                               contentType:@"image/png"
                                  isPublic:YES
                              successBlock:^(QBResponse *response, QBCBlob *uploadedBlob) {
                                  DDLogDebug(@"responsesuccess-->: %@", response);
                                  // Create chat message with attach
                                  
                                  NSURL *url = [NSURL URLWithString:uploadedBlob.publicUrl];
                                  [[ChatService shared]
                                   .sdwebImagemanager downloadImageWithURL:url
                                   options:0
                                   progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                       // progression tracking code
                                   }
                                   completed:^(UIImage *image, NSError *error,
                                               SDImageCacheType cacheType, BOOL finished,
                                               NSURL *imageURL) {
                                        photoItem.image = image;
                                       [[NSNotificationCenter defaultCenter]
                                        postNotificationName:kChatRoomDownLoadPhotoWaitingListToReloadView
                                        object:nil];
                                   }];
                                  
                                  NSMutableDictionary *params = [NSMutableDictionary dictionary];
                                  [[ChatRelationModel shared]
                                   getCurrentChatRoomRelationObjectFromLocalForDialogId:dialog
                                   .ID];
                                  if (![ChatRelationModel shared]
                                      .currentChatRoomRelationObjectIsEmpty &&
                                      [ChatRelationModel shared].mySelfChatRoomRelation.Muted) {
                                      params[@"save_to_history"] = @"1";
                                  } else {
                                      params[@"save_to_history"] = @"1";
                                  }
                                  
                                  if ([ChatService shared]
                                      .myQBUUserCustomData.chatroomshowreadreceipts &&
                                      [ChatService shared]
                                      .recipienterQBUUserCustomData.chatroomshowreadreceipts) {
                                      params[@"chatroomshowreadreceipts"] = @"true";
                                  } else {
                                      params[@"chatroomshowreadreceipts"] = @"false";
                                  }
                                  if (isEmpty([ChatService shared]
                                              .recipienterQBUUserCustomData)||isEmpty([ChatService shared].myQBUUserCustomData)) {
                                      params[@"chatroomshowreadreceipts"] = @"true";
                                  }
                                  params[@"photourl"] = uploadedBlob.publicUrl;
                                  [qbchatmessage setCustomParameters:params];
                                  [self sendMessageMethodWithPhoto:qbchatmessage
                                                        jsqMessage:jsqmessage
                                                         forDialog:dialog];
                                  [[NSNotificationCenter defaultCenter]
                                   postNotificationName:kNotificationChatRoomGetImageFromDBQBChatMessageToMWPhotoBrower
                                   object:nil];
                                  
                              }
                               statusBlock:^(QBRequest *request, QBRequestStatus *status) {
                                   DDLogDebug(@"request-->: %@", request);
                                   //   DDLogInfo(@"status.percentOfCompletion-->: %f",
                                   //   status.percentOfCompletion);
                                   
                               }
                                errorBlock:^(QBResponse *response) {
                                    DDLogError(@"QBResponse %@", response);
                                    
                                }];
                }
        }
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kNotificationChatRoomReloadCollectionViewAndScrollToBottomYesAnimated
         object:nil];
        
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        DDLogInfo(@"scrollToBottomAnimated:YES");
        self.chatroomchosenImages = nil;
    }
}
//// send messgemothodwithphoto
- (void)sendMessageMethodWithPhoto:(QBChatMessage *)message
                        jsqMessage:(JSQMessage *)jsqmessage
                         forDialog:(QBChatDialog *)dialog {
    // create a message
    
    // 1-1 Chat
    // send message
    message.recipientID = [dialog recipientID];
    message.senderID = [QBSession currentSession].currentUser.ID;
    message.text = @"image";
    [message.customParameters setObject:kChatMessageTypeImage forKey:kChatMessageCustomParameterMessageType];
    [message.customParameters setObject:[LoginBySocialNetworkModel shared].myLoginInfo.MemberName forKey:kChatMessageCustomParameterSenderName];
    
    ////save message into DB
   
    JSQMessage * finishSendJsqmessage =[self sendMessageSaveintoDataBaseDialog:dialog message:message chatMessageType:kChatMessageTypeImage jsqmessage:jsqmessage];

    [self sendMessageMethodDialog:dialog message:message completionBlock:^(id response, NSError *error) {
        [[ChatService shared] setUpJSQMessage:finishSendJsqmessage
                            fromQBChatMessage:(DBQBChatMessage *)response];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kNotificationChatRoomReloadCollectionView
         object:nil];
    }];
   
}
-(void)sendMessageMethodDialog:(QBChatDialog *)dialog message:(QBChatMessage *)message completionBlock:(CommonBlock)completionBlock{
    DBQBChatMessage * newestChatMessageFromDataBase;
    DBResultSet * r =  [[[[DBQBChatMessage query] whereWithFormat:@" MessageID = %@", message.ID]
                         orderBy:@"Id"] fetch];
    for (DBQBChatMessage * dbqbChatMessage in r) {
        newestChatMessageFromDataBase=dbqbChatMessage;
    }
    
    if ([self.currentListingID valid]) {
    [self updateDialog:dialog WithAgentListingID:self.currentListingID];
        
#pragma Error Message Retry
        if (![message.customParameters isKindOfClass:[NSMutableDictionary class]]) {
             message.markable=YES;
#pragma if Photo message
            if ([[message.customParameters objectForKey:kChatMessageCustomParameterMessageType]isEqualToString:kChatMessageTypeImage]) {
                [message.customParameters setObject:kChatMessageTypeImage forKey:kChatMessageCustomParameterMessageType];
                [message.customParameters setObject:[LoginBySocialNetworkModel shared].myLoginInfo.MemberName forKey:kChatMessageCustomParameterSenderName];
                
            }else{
#pragma if Text message
           NSMutableDictionary * customParameters= [message.customParameters mutableCopy];
            [customParameters setObject:self.currentListingID forKey:kChatMessageCustomParameterAgentListing];
            message.customParameters=customParameters;
               
            }
        }else{
    [message.customParameters setObject:self.currentListingID forKey:kChatMessageCustomParameterAgentListing];
        }
    newestChatMessageFromDataBase.CustomParameters = [message.customParameters copy];
    }
    if (![ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty &&
        [ChatRelationModel shared].mySelfChatRoomRelation.Blocked) {
       
            newestChatMessageFromDataBase.IsBlockedSendMessage = YES;
            newestChatMessageFromDataBase.NumberofDeliverStatus = 1;
            newestChatMessageFromDataBase.ErrorStatus = NO;
            newestChatMessageFromDataBase.SendToServerDate = [NSDate date];
            [newestChatMessageFromDataBase commit];
        
       
       
         completionBlock(newestChatMessageFromDataBase,nil);
     
    } else {
        
        newestChatMessageFromDataBase.IsBlockedSendMessage = NO;
        if ([[message.customParameters objectForKey:kChatMessageCustomParameterMessageType]isEqualToString:kChatMessageTypeImage]){
            message.text=kChatMessageTypeImage;
        }
        if ([[message.customParameters objectForKey:kChatMessageCustomParameterMessageType]isEqualToString:kChatMessageTypeAgentListing]){
            message.text=kChatMessageTypeAgentListing;
        }
        [self dialogSendMessageMethodDBQBChatMessage:newestChatMessageFromDataBase dialog:dialog message:message retryCount:QBSendMessageRetryCount  completionBlock:completionBlock];
}
   
}
-(void)dialogSendMessageMethodDBQBChatMessage:(DBQBChatMessage *)dbhistorymessage dialog:(QBChatDialog *)dialog message:(QBChatMessage *)message retryCount:(int)retryCount completionBlock:(CommonBlock)completionBlock{
    if (retryCount==0) {
     
          [dbhistorymessage sendFailChangeDB];
            NSError * error =[NSError errorWithDomain:@"retryAlsoFail" code:438 userInfo:nil];
            completionBlock(dbhistorymessage,error);
        
       
    }
     retryCount=retryCount-1;
    [dialog sendMessage:message completionBlock:^(NSError * error) {
        
        if ([error valid]) {
            
            if ((error.code==401)||(error.code==404)) {
           
                    [dbhistorymessage sendFailChangeDB];
                      completionBlock(dbhistorymessage,error);
                
               
              
            }else{
                dispatch_time_t popTime =
                dispatch_time(DISPATCH_TIME_NOW, QBSendMessageRetryCountInterval * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    if (retryCount>=0) {
                   [self dialogSendMessageMethodDBQBChatMessage:dbhistorymessage dialog:dialog message:message retryCount:retryCount  completionBlock:completionBlock];
                    }
                });
              
                
            }
        }else{
                          [[[[DBQBChatDialogClearMessage query]
                   whereWithFormat:@" DialogID = %@", dialog.ID] fetch] removeAll];
                [dbhistorymessage sendSuccessChangeDB];
                  completionBlock(dbhistorymessage,nil);
            
          
        }
        
    }];

    
    
}
-(void)updateDialog:(QBChatDialog*)dialog WithAgentListingID:(NSString*)agentListingID{
    NSMutableDictionary *customData = [[NSMutableDictionary alloc]init];
    [customData setObject:kChatDialogSettingClassName forKey:@"class_name"];
    [customData setObject:agentListingID forKey:kChatDialogCustomParameterAgentListing];
    dialog.data = customData;
    
    [QBRequest updateDialog:dialog successBlock:^(QBResponse *responce, QBChatDialog *dialog) {
        
    } errorBlock:^(QBResponse *response) {
        
    }];
}
@end