//
//  ChatService.h
//  sample-chat
//
//  Created by Igor Khomenko on 10/21/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBQBChatMessage.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "JSQMessages.h"
#import "QBUUserCustomData.h"
#import <QuickbloxWebRTC/QuickbloxWebRTC.h>
#import "RealUtility.h"
#import "TWMessageBarManager.h"
#define kNotificationChatDidAccidentallyDisconnect \
  @"kNotificationСhatDidAccidentallyDisconnect"
#define kNotificationChatDidReconnect @"kNotificationChatDidReconnect"
#define kNotificationDialogsUpdated @"kNotificationDialogsUpdated"
#define kNotificationChatRelationListUpdated @"kNotificationChatRelationListUpdated"
#define kNotificationChatRoomRelationPageButtonSetup @"kNotificationChatRoomRelationPageButtonSetup"
#define kNotificationDidReapplyQBAccount @"kNotificationDidReapplyQBAccount"

#define kNotificationChatRoomReloadCollectionView \
  @"kNotificationChatRoomReloadCollectionView"
#define kNotificationChatRoomReloadCollectionViewAndScrollToBottomYesAnimated \
@"kNotificationChatRoomReloadCollectionViewAndScrollToBottomYesAnimated"
#define kNotificationChatRoomConnectToServerSuccess \
  @"kNotificationChatRoomConnectToServerSuccess"
#define kNotificationGetAgentListingGetList \
@"kNotificationGetAgentListingGetList"
#define kNotificationGetMessageCellTopLabelDateIndexPathArray \
@"kNotificationGetMessageCellTopLabelDateIndexPathArray"
#define kNotificationChatRoomGetImageFromDBQBChatMessageToMWPhotoBrower \
@"kNotificationChatRoomGetImageFromDBQBChatMessageToMWPhotoBrower"

#define kNotificationChatRoomReadAllReceivceMessage \
@"kNotificationChatRoomReadAllReceivceMessage"

#define kNotificationSpotLightSearchOpenRightProfilePage \
@"kNotificationSpotLightSearchOpenRightProfilePage"

#define kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage \
@"kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage"


#define kSystemMessageTypeAlert  @"SystemMessageTypeAlert"
#define kSystemMessageTypeRefreshChatSetting  @"SystemMessageTypeRefreshChatSetting"
#define kSystemMessageTypeRefreshChatPrivacySetting  @"SystemMessageTypeRefreshChatPrivacySetting"
#define kChatRoomDownLoadPhotoWaitingListToReloadView  @"kChatRoomDownLoadPhotoWaitingListToReloadView"
#define kChatMessageTypeImage  @"image"
#define kChatMessageTypeAgentListing @"agentListing"
#define kChatMessageTypeText  @"text"
#define kChatMessageTypeAudio  @"audio"
#define kChatMessageTypeVideo  @"video"
#define kChatMessageTypeUnknown @"unknown"
#define kChatMessageTypeRate @"rate"

#define kChatMessageCustomParameterMessageType @"messageType"
#define kChatMessageCustomParameterSenderName @"senderName"
#define kChatMessageCustomParameterAgentListing @"bindingAgentListingID"

#define kChatDialogCustomParameterAgentListing @"currentAgentListingID"

#define kChatDialogSettingClassName @"dialogSetting"

#define kChatConferenceTypeAudio @"audio"
#define kChatConferenceTypeAudio @"video"

@protocol ChatServiceDelegate<NSObject>

- (BOOL)chatDidReceiveMessage:(QBChatMessage *)message;
- (void)chatDidDeliverMessageWithID:(DBQBChatMessage *)message;
- (void)chatDidReadMessageWithID:(DBQBChatMessage *)message;
- (void)chatDidFailWithStreamError:(NSError *)error;
- (void)chatDidConnect;
- (void)chatRoomCollectionViewReloadOnly;
- (void)getRecipienterFromQBServer;
- (void)chatDidReceiveSystemMessage:(QBChatMessage *)message;
@end
@interface ChatService : NSObject<TWMessageBarStyleSheet>
@property(weak) id<ChatServiceDelegate> delegate;
#pragma mark TWMessageBar 
@property(nonatomic, strong) UIImage *twMessageBarIcon;
@property(nonatomic, strong) QBChatMessage *topBarGroupMessage;
#pragma mark TopBarLoginStatusView-----------------------------------------------------------------------------------------------------------------------
@property(readonly) BOOL isConnected;
@property(assign, nonatomic) BOOL connecttoserversuccess;
@property(nonatomic, readonly) QBUUser *currentUser;
@property(nonatomic, strong) NSTimer *presenceTimer;
@property(nonatomic, strong) NSTimer *reconnectTimer;
//// getrecipientertimer
@property(nonatomic, strong) NSTimer *getrecipientertimer;
@property(nonatomic, strong) QBUUserCustomData *recipienterQBUUserCustomData;
@property(nonatomic, strong) QBUUserCustomData * myQBUUserCustomData;
@property(nonatomic, strong) SDWebImageManager *sdwebImagemanager;
@property(nonatomic, strong) NSMutableDictionary *chatroomJsqMessageArray;
@property(nonatomic, strong) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property(nonatomic, strong) JSQMessagesBubbleImage *incomingBubbleImageData;
#pragma mark group message
@property(nonatomic, assign) BOOL groupMessage;
@property(nonatomic, assign) int groupMessageCount;
+ (instancetype)shared;
#pragma mark Login/Logout-------------------------------------------------------------------------------------------------------------------------
- (void)loginWithUser:(QBUUser *)user
      completionBlock:(void (^)())completionBlock;
- (void)logout;
- (void)disconnectUser;
- (void)startGetRecipienterTimer;
- (void)stopGetRecipienterTimer;

#pragma mark JSQMessageArray----------------------------------------------------------------------------------------------------------------------
- (void)addMessagesToJSQMessageArray:(NSArray *)messages forDialogId:(NSString *)dialogId;
- (void)addMessagesWhenDBNotMessageToJSQMessageArray:(NSArray *)messages forDialogId:(NSString *)dialogId;
- (JSQMessage *)addJSQMessageFromDBQBChatMessageToJSSQMessageArray:(DBQBChatMessage *)message forDialogId:(NSString *)dialogId;
- (void)addjsqPhotoMessageToJsqMessageArray:(JSQMessage *)jsqmessage forDialogId:(NSString *)dialogId;
- (JSQMessage *)covertDBQBChatMessageToJsqMessage:(DBQBChatMessage *)dbhistorymessage;
- (void)whenReceiveMessageAddMessageToJsqmessageArray:(QBChatMessage *)chatmessage forDialogId:(NSString *)dialogId;
//// setUpJSQMessage
- (JSQMessage *)setUpJSQMessage:(JSQMessage *)jsqmessage fromQBChatMessage:(DBQBChatMessage *)dbqbchatmessage;
- (void)reloadChatRoomCollectionViewAfterSecond:(int)second;

- (void)createDialog:(QBChatDialog*)dialog successBlock:(QB_NULLABLE void(^)(QBResponse * QB_NONNULL_S response, QBChatDialog * QB_NULLABLE_S createdDialog))successBlock
          errorBlock:(QB_NULLABLE QBRequestErrorBlock)errorBlock;
- (void)startConferenceWithType:(QBRTCConferenceType )conferenceType opponentIDs:(NSArray* QB_NONNULL_S)opponentIDs;
- (void)sendSystemIncompatibleMessage:(ChatFeatureType)featureType dialog:(QBChatDialog*)dialog;
- (void)sendSystemErrorWithTitle:(NSString*)title message:(NSString*)message goActionTitle:(NSString*)goActionTitle goActionURL:(NSString*)goActionURL cancelActionTitle:(NSString*)cancelActionTitle cancelActionURL:(NSString*)cancelActionURL dialog:(QBChatDialog*)dialog;
@end
