
//
//  ChatService.m
//  sample-chat
//
//  Created by Igor Khomenko on 10/21/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import "ChatService.h"

#import "DBQBChatDialog.h"
#import "DBQBUUser.h"
#import "DBQBChatMessage.h"
#import "Chatroom.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DBChatSetting.h"
#import "QBUUserCustomData.h"
#import "ChatDialogModel.h"
#import "ChatRelationModel.h"
#import "ReceiveMessageCount.h"
#import "DialogPageAgentProfilesModel.h"
#import "DBUnreadMessageLabel.h"
#import "DBHasBeenDownloadPhoto.h"
#import "NSDictionary+Utility.h"
#import "NSObject+Utility.h"
#import <QuickbloxWebRTC/QuickbloxWebRTC.h>
typedef void (^CompletionBlock)();

typedef void (^CompletionBlockWithResult)(NSArray *);

@interface ChatService ()<QBChatDelegate>

@property(copy) CompletionBlock loginCompletionBlock;
@property (nonatomic, strong) NSTimer *sendPresenceTimer;

@end

@implementation ChatService {
    BOOL _chatAccidentallyDisconnected;
}
+ (instancetype)shared {
    static id instance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[self alloc] init];
    });
    
    return instance_;
}

- (id)init {
    self = [super init];
    if (self) {
        [[QBChat instance] addDelegate:self];
        //        [QBRTCClient.instance addDelegate:self];
        [QBSettings setAutoReconnectEnabled:YES];
        self.connecttoserversuccess = NO;
        //
        //  [QBChat instance].streamManagementEnabled = YES;
        //  [QBChat instance].streamResumptionEnabled = YES;
        ////init JSQMessage propert
        self.chatroomJsqMessageArray = [NSMutableDictionary dictionary];
        self.sdwebImagemanager = [SDWebImageManager sharedManager];
        //    [self.sdwebImagemanager.imageDownloader setMaxConcurrentDownloads:2];
        [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
        [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
        self.myQBUUserCustomData = [[QBUUserCustomData alloc] init];
        self.myQBUUserCustomData.chatroomshowlastseen = YES;
        self.myQBUUserCustomData.chatroomshowreadreceipts = YES;
        self.recipienterQBUUserCustomData = [[QBUUserCustomData alloc] init];
        self.recipienterQBUUserCustomData.chatroomshowlastseen = YES;
        self.recipienterQBUUserCustomData.chatroomshowreadreceipts = YES;
        
        JSQMessagesBubbleImageFactory *bubbleFactory =
        [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData =
        [bubbleFactory outgoingMessagesBubbleImageWithColor:
         [UIColor jsq_messageBubbleLightGrayColor]];
        self.outgoingBubbleImageData.messageBubbleImage=[UIImage imageNamed:@"chat_bubble_blue"];
        self.outgoingBubbleImageData.messageBubbleHighlightedImage=[UIImage imageNamed:@"chat_bubble_blue"];
        self.incomingBubbleImageData =
        [bubbleFactory incomingMessagesBubbleImageWithColor:
         [UIColor jsq_messageBubbleGreenColor]];
        self.incomingBubbleImageData.messageBubbleImage=[UIImage imageNamed:@"chat_bubble_grey"];
        self.incomingBubbleImageData.messageBubbleHighlightedImage=[UIImage imageNamed:@"chat_bubble_grey"];
        
        
    }
    return self;
}

#pragma mark Login/Logout-------------------------------------------------------------------------------------------------------------------------
- (void)loginWithUser:(QBUUser *)user
      completionBlock:(void (^)())completionBlock {
    
    if (![LoginBySocialNetworkModel shared].myLoginInfo) {
        return;
    }
    
    self.loginCompletionBlock = completionBlock;
    [[QBChat instance] connectWithUser:user completion:^(NSError * _Nullable error) {
        
        BOOL isMemberLoggedOut = ![[LoginBySocialNetworkModel shared].myLoginInfo.MemberID valid];
        
        if (error) {
            
            // Already connected
            if ([self isConnected]) {
                
                [self chatDidConnect];
                
            } else {
            
                // Retry
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [self loginWithUser:user completionBlock:completionBlock];
                });
            }
            
        } else {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRoomConnectToServerSuccess object:nil];
            
            // check does user logged out
            if (isMemberLoggedOut) {
                // if yes, logout QB user
                [self disconnectUser];
                
            } else {
                if ([self isConnected]) {
                    
                    [self chatDidConnect];
                    
                }
                // if no, update customData
//                [[LoginBySocialNetworkModel shared].myLoginInfo updateCustomData];
            }
            
        }
    }];
}
- (BOOL)isConnected {
    return [QBChat instance].isConnected;
}
- (QBUUser *)currentUser {
    return [[QBChat instance] currentUser];
}
- (void)logout {
    self.myQBUUserCustomData = [[QBUUserCustomData alloc] init];
    self.myQBUUserCustomData.chatroomshowlastseen = YES;
    self.myQBUUserCustomData.chatroomshowreadreceipts = YES;
    [self disconnectUser];
}
-(void)disconnectUser{
    [[QBChat instance] disconnectWithCompletionBlock:^(NSError * _Nullable error){
        
    }];
    
    [ChatService shared].connecttoserversuccess = NO;
}


#pragma mark JSQMessageArray----------------------------------------------------------------------------------------------------------------------
- (NSMutableArray *)jsqMessagsForDialogId:(NSString *)dialogId {
    NSMutableArray *jsqmessages =
    [self.chatroomJsqMessageArray objectForKey:dialogId];
    
    return jsqmessages;
}
- (void)addMessagesToJSQMessageArray:(NSArray *)messages
                         forDialogId:(NSString *)dialogId {
    NSMutableArray *chatroomjsqemessagearray = [[NSMutableArray alloc] init];
    
    [self reloadChatRoomCollectionViewAfterSecond:4];
    [self reloadChatRoomCollectionViewAfterSecond:7];
    [self reloadChatRoomCollectionViewAfterSecond:10];
    
    for (QBChatMessage *message in messages) {
        //// check duplicate message
        DBResultSet *r = [[[DBQBChatMessage query]
                           whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
        if (r.count == 0) {
            DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
            
            dbhistorymessage.MessageID = message.ID;
            dbhistorymessage.DialogID = dialogId;
            dbhistorymessage.QBID =
            [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
            dbhistorymessage.MemberID =
            [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
            dbhistorymessage.RecipientID = message.recipientID;
            dbhistorymessage.SenderID = message.senderID;
            dbhistorymessage.DateSent = message.dateSent;
            dbhistorymessage.InsertDBDateTime = [NSDate date];
            if ([self isEmpty:[message.customParameters objectForKey:@"photourl"]]) {
                dbhistorymessage.PhotoType = NO;
            } else {
                dbhistorymessage.PhotoType = YES;
            }
            dbhistorymessage.QBChatHistoryMessage =
            [NSKeyedArchiver archivedDataWithRootObject:message];
            dbhistorymessage.ChatMessageType=message.customParameters[kChatMessageCustomParameterMessageType];
            dbhistorymessage.Text = message.text;
            dbhistorymessage.CustomParameters = message.customParameters;
            dbhistorymessage.Attachments = message.attachments;
            dbhistorymessage.NumberofDeliverStatus = 2;
            
            dbhistorymessage.DidDeliverStatus = YES;
            dbhistorymessage.ErrorStatus = NO;
            dbhistorymessage.DrawStatus = NO;
            dbhistorymessage.ReadStatus = NO;
            dbhistorymessage.Retrycount = 0;
            dbhistorymessage.IsDeleteMessage = NO;
            dbhistorymessage.IsBlockedSendMessage = NO;
            [dbhistorymessage commit];
            
            
            [chatroomjsqemessagearray
             addObject:[self covertDBQBChatMessageToJsqMessage:dbhistorymessage]];
        }
    }
    
    //// add chatroomJsqMessageArray to chatroomJsqMessageArray
    NSMutableArray *chatroomJsqMessageArray =[self.chatroomJsqMessageArray objectForKey:dialogId];
    
    if (chatroomJsqMessageArray != nil) {
        [chatroomJsqMessageArray addObjectsFromArray:chatroomjsqemessagearray];
    } else {
        [self.chatroomJsqMessageArray setObject:chatroomjsqemessagearray
                                         forKey:dialogId];
    }
    
    //  NSArray *convertMessageArray =[self convertRawRoomMessagArrayToSectionMsgArray:chatroomJsqMessageArray];
    //  [self updateChatroomMessage:convertMessageArray dialogId:dialogId];
}
- (void)addMessagesWhenDBNotMessageToJSQMessageArray:(NSArray *)messages
                                         forDialogId:(NSString *)dialogId {
    NSMutableArray *chatroomjsqemessagearray = [[NSMutableArray alloc] init];
    NSMutableArray *chathistortmessagearray = [[NSMutableArray alloc] init];
    
    [QBRequest countOfMessagesForDialogID:dialogId
                          extendedRequest:nil
                             successBlock:^(QBResponse *response, NSUInteger count) {
                                 [self reloadChatRoomCollectionViewAfterSecond:4];
                                 [self reloadChatRoomCollectionViewAfterSecond:7];
                                 BOOL ReadStatus = YES;
                                 if (count <= 1) {
                                     ReadStatus = NO;
                                 }
                                 for (QBChatMessage *message in messages) {
                                     DBResultSet *r = [[[DBQBChatMessage query]
                                                        whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
                                     if (r.count == 0) {
                                         DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
                                         
                                         dbhistorymessage.MessageID = message.ID;
                                         dbhistorymessage.DialogID = dialogId;
                                         dbhistorymessage.QBID =
                                         [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
                                         dbhistorymessage.MemberID = [[LoginBySocialNetworkModel shared]
                                                                      .myLoginInfo.MemberID intValue];
                                         dbhistorymessage.RecipientID = message.recipientID;
                                         dbhistorymessage.SenderID = message.senderID;
                                         dbhistorymessage.DateSent = message.dateSent;
                                         dbhistorymessage.InsertDBDateTime = [NSDate date];
                                         
                                         dbhistorymessage.QBChatHistoryMessage =
                                         [NSKeyedArchiver archivedDataWithRootObject:message];
                                         if ([self isEmpty:[message.customParameters
                                                            objectForKey:@"photourl"]]) {
                                             dbhistorymessage.PhotoType = NO;
                                         } else {
                                             dbhistorymessage.PhotoType = YES;
                                         }
                                         dbhistorymessage.ChatMessageType=message.customParameters[kChatMessageCustomParameterMessageType];
                                         dbhistorymessage.Text = message.text;
                                         dbhistorymessage.CustomParameters = message.customParameters;
                                         dbhistorymessage.Attachments = message.attachments;
                                         dbhistorymessage.NumberofDeliverStatus = 2;
                                         dbhistorymessage.DidDeliverStatus = YES;
                                         
                                         dbhistorymessage.ErrorStatus = NO;
                                         dbhistorymessage.DrawStatus = YES;
                                         dbhistorymessage.ReadStatus = YES;
                                         dbhistorymessage.Retrycount = 0;
                                         dbhistorymessage.IsDeleteMessage = NO;
                                         dbhistorymessage.IsBlockedSendMessage = NO;
                                         [dbhistorymessage commit];
                                         
                                         [chathistortmessagearray addObject:dbhistorymessage];
                                         [chatroomjsqemessagearray
                                          addObject:
                                          [self covertDBQBChatMessageToJsqMessage:dbhistorymessage]];
                                     }
                                 }
                                 
                                 //// add chatroomJsqMessageArray to self.chatroomJsqMessageArray
                                 NSMutableArray *chatroomJsqMessageArray =
                                 [self.chatroomJsqMessageArray objectForKey:dialogId];
                                 
                                 if (chatroomJsqMessageArray != nil) {
                                     [chatroomJsqMessageArray
                                      addObjectsFromArray:chatroomjsqemessagearray];
                                 } else {
                                     [self.chatroomJsqMessageArray setObject:chatroomjsqemessagearray
                                                                      forKey:dialogId];
                                 }
                                 //          NSArray *convertMessageArray =[self convertRawRoomMessagArrayToSectionMsgArray:chatroomJsqMessageArray];
                                 //         [self updateChatroomMessage:convertMessageArray dialogId:dialogId];
                                 
                                 [self reloadChatRoomCollectionViewAfterSecond:0];
                                 [[NSNotificationCenter defaultCenter]
                                  postNotificationName:kNotificationGetAgentListingGetList
                                  object:nil];
                                 [[NSNotificationCenter defaultCenter]
                                  postNotificationName:kNotificationChatRoomGetImageFromDBQBChatMessageToMWPhotoBrower
                                  object:nil];
                             }
                               errorBlock:^(QBResponse *response) {
                                   DDLogError(@"QBResponse %@", response);
                               }];
}
- (JSQMessage *)covertDBQBChatMessageToJsqMessage:
(DBQBChatMessage *)dbhistorymessage {
    JSQMessage *jsqmessage;
    //// check is photo type message
    if (![self isEmpty:[dbhistorymessage.CustomParameters
                        objectForKey:@"photourl"]]) {
        JSQPhotoMediaItem *photoItem =
        [[JSQPhotoMediaItem alloc] initWithImage:nil];
        if ([[LoginBySocialNetworkModel shared].myLoginInfo.QBID integerValue] ==
            dbhistorymessage.SenderID) {
            photoItem.appliesMediaViewMaskAsOutgoing = YES;
        } else {
            photoItem.appliesMediaViewMaskAsOutgoing = NO;
        }
        NSURL *url = [NSURL URLWithString:[dbhistorymessage.CustomParameters
                                           objectForKey:@"photourl"]];
        
        jsqmessage = [[JSQMessage alloc]
                      initWithSenderId:[NSString stringWithFormat:@"%@", @(dbhistorymessage
                                        .SenderID)]
                      senderDisplayName:@"None"
                      date:dbhistorymessage.DateSent
                      media:photoItem];
        [self.sdwebImagemanager downloadImageWithURL:url
                                             options:0
                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                // progression tracking code
                                            }
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,
                                                       BOOL finished, NSURL *imageURL) {
                                               NSString *messageSenderId = [jsqmessage senderId];
                                                photoItem.image = image;
                                               [[NSNotificationCenter defaultCenter]
                                                postNotificationName:kChatRoomDownLoadPhotoWaitingListToReloadView
                                                object:nil];
                                               if (image&&![[jsqmessage senderId] isEqualToString:[LoginBySocialNetworkModel shared].myLoginInfo.QBID]) {
                                                   photoItem.image = image;
                                                   
                                                   DBResultSet *r = [[[[DBChatSetting query]
                                                                       whereWithFormat:@"MemberID = %@",
                                                                       [LoginBySocialNetworkModel shared]
                                                                       .myLoginInfo.MemberID] limit:1]
                                                                     
                                                                     fetch];
                                                   for (DBChatSetting *dbChatSetting in r) {
                                                       
                                                       if (dbChatSetting.ChatRoomPhotoAutoDownload) {
                                                           DDLogDebug(@"DBHasBeenDownloadPhoto %lu",(unsigned long)[[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [dbhistorymessage.CustomParameters objectForKey:@"photourl"]]
                                                                                                                     fetch]count]);
                                                           if ([[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [dbhistorymessage.CustomParameters objectForKey:@"photourl"]]
                                                                 fetch]count]==0) {
                                                               UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                               [DBHasBeenDownloadPhoto createSearchAgentHistoryWithPhotoUrl:[dbhistorymessage.CustomParameters objectForKey:@"photourl"]];
                                                           }
                                                           
                                                           
                                                       }
                                                   }
                                               }
                                           }];
        
    } else {
        //// check is text type message
        NSString *text = dbhistorymessage.Text;
        if ([self isEmpty:dbhistorymessage.Text]) {
            text = @"";
        }
        jsqmessage = [[JSQMessage alloc]
                      initWithSenderId:[NSString stringWithFormat:@"%@", @(dbhistorymessage
                                        .SenderID)]
                      senderDisplayName:@"None"
                      date:dbhistorymessage.DateSent
                      text:text];
    }
    
    [[ChatService shared] setUpJSQMessage:jsqmessage
                        fromQBChatMessage:dbhistorymessage];
    return jsqmessage;
}

- (JSQMessage *)
addJSQMessageFromDBQBChatMessageToJSSQMessageArray:(DBQBChatMessage *)message
forDialogId:(NSString *)dialogId {
    //// add jsqmessage to chatroomJsqMessageArray
    
    JSQMessage *jsqmessage = [[JSQMessage alloc]
                              initWithSenderId:[NSString stringWithFormat:@"%@", @(message.SenderID)]
                              senderDisplayName:@"None"
                              date:message.DateSent
                              text:message.Text];
  JSQMessage * hasBeenSetupJsqmessage  =[self setUpJSQMessage:jsqmessage fromQBChatMessage:message];
    NSMutableArray *chatroomJsqMessageArray =
    [self.chatroomJsqMessageArray objectForKey:dialogId];
    
    if (chatroomJsqMessageArray != nil) {
        [chatroomJsqMessageArray addObject:hasBeenSetupJsqmessage];
    } else {
        NSMutableArray *messages = [NSMutableArray array];
        [messages addObject:hasBeenSetupJsqmessage];
        [self.chatroomJsqMessageArray setObject:messages forKey:dialogId];
    }
    //   NSArray *convertMessageArray =[self convertRawRoomMessagArrayToSectionMsgArray:chatroomJsqMessageArray];
    //   [self updateChatroomMessage:convertMessageArray dialogId:dialogId];
    
    return hasBeenSetupJsqmessage;
}
- (void)whenReceiveMessageAddMessageToJsqmessageArray:
(QBChatMessage *)chatmessage forDialogId:(NSString *)dialogId {
    [self reloadChatRoomCollectionViewAfterSecond:5];
    [self reloadChatRoomCollectionViewAfterSecond:9];
    NSMutableArray *chatroomjsqemessagearray = [[NSMutableArray alloc] init];
    DBResultSet *r = [[[DBQBChatMessage query]
                       whereWithFormat:@" MessageID = %@ ", chatmessage.ID] fetch];
    if (r.count == 0) {
        DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
        
        dbhistorymessage.MessageID = chatmessage.ID;
        dbhistorymessage.DialogID = chatmessage.dialogID;
        dbhistorymessage.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
        dbhistorymessage.MemberID =
        [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
        dbhistorymessage.RecipientID = chatmessage.recipientID;
        dbhistorymessage.SenderID = chatmessage.senderID;
        dbhistorymessage.DateSent = chatmessage.dateSent;
        dbhistorymessage.InsertDBDateTime = [NSDate date];
        
        dbhistorymessage.QBChatHistoryMessage =
        [NSKeyedArchiver archivedDataWithRootObject:chatmessage];
        if ([self
             isEmpty:[chatmessage.customParameters objectForKey:@"photourl"]]) {
            dbhistorymessage.PhotoType = NO;
        } else {
            dbhistorymessage.PhotoType = YES;
        }
        dbhistorymessage.ChatMessageType=chatmessage.customParameters[kChatMessageCustomParameterMessageType];
        dbhistorymessage.Text = chatmessage.text;
        dbhistorymessage.CustomParameters = chatmessage.customParameters;
        dbhistorymessage.Attachments = chatmessage.attachments;
        dbhistorymessage.NumberofDeliverStatus = 2;
        
        dbhistorymessage.DidDeliverStatus = YES;
        dbhistorymessage.ErrorStatus = NO;
        dbhistorymessage.DrawStatus = NO;
        dbhistorymessage.ReadStatus = NO;
        dbhistorymessage.Retrycount = 0;
        dbhistorymessage.IsDeleteMessage = NO;
        dbhistorymessage.IsBlockedSendMessage = NO;
        [dbhistorymessage commit];
        
        //// add chatroomJsqMessageArray to chatroomJsqMessageArray
        NSMutableArray *chatroomJsqMessageArray =
        [self.chatroomJsqMessageArray objectForKey:dialogId];
        
        if (chatroomJsqMessageArray != nil) {
            [chatroomJsqMessageArray
             addObject:[self covertDBQBChatMessageToJsqMessage:dbhistorymessage]];
        } else {
            [self.chatroomJsqMessageArray setObject:chatroomjsqemessagearray forKey:dialogId];
        }
        //     NSArray *convertMessageArray =[self convertRawRoomMessagArrayToSectionMsgArray:chatroomJsqMessageArray];
        //   [self /updateChatroomMessage:convertMessageArray dialogId:dialogId];
    }
}

- (void)addjsqPhotoMessageToJsqMessageArray:(JSQMessage *)jsqmessage
                                forDialogId:(NSString *)dialogId {
    NSMutableArray *chatroomJsqMessageArray =
    [self.chatroomJsqMessageArray objectForKey:dialogId];
    
    if (chatroomJsqMessageArray != nil) {
        [chatroomJsqMessageArray addObject:jsqmessage];
    } else {
        NSMutableArray *messages = [NSMutableArray array];
        [messages addObject:jsqmessage];
        [self.chatroomJsqMessageArray setObject:messages forKey:dialogId];
    }
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationChatRoomReloadCollectionView
     object:nil];
}

- (JSQMessage *)setUpJSQMessage:(JSQMessage *)jsqmessage
      fromQBChatMessage:(DBQBChatMessage *)dbqbchatmessage {

    jsqmessage.MessageID = dbqbchatmessage.MessageID;
    jsqmessage.DialogID = dbqbchatmessage.DialogID;
    jsqmessage.QBID = dbqbchatmessage.QBID;
    jsqmessage.MemberID = dbqbchatmessage.MemberID;
    jsqmessage.RecipientID = dbqbchatmessage.RecipientID;
    jsqmessage.SenderID = dbqbchatmessage.SenderID;
    jsqmessage.DateSent = dbqbchatmessage.DateSent;
    jsqmessage.InsertDBDateTime = dbqbchatmessage.InsertDBDateTime;
    jsqmessage.PhotoType = dbqbchatmessage.PhotoType;
    jsqmessage.QBChatHistoryMessage = dbqbchatmessage.QBChatHistoryMessage;
    
    NSString *messageType = dbqbchatmessage.CustomParameters[kChatMessageCustomParameterMessageType];
    NSString *senderName = dbqbchatmessage.CustomParameters[kChatMessageCustomParameterSenderName];
    NSString *text = dbqbchatmessage.Text;
    if ([messageType isEqualToString:kChatMessageTypeText]||[messageType isEqualToString:kChatMessageTypeAgentListing]||[dbqbchatmessage.ChatMessageType isEqualToString:kChatMessageTypeImage]||[dbqbchatmessage.ChatMessageType isEqualToString:kChatMessageTypeText]) {
        
    }else if ([messageType isEqualToString:kChatMessageTypeRate] || [dbqbchatmessage.ChatMessageType isEqualToString:kChatMessageTypeRate]){
        text = JMOLocalizedString(@"chatroom__rate_message", nil);
    }else if ([messageType isEqualToString:kChatMessageTypeAudio]) {
        text = [NSString stringWithFormat:JMOLocalizedString(@"incompatible_alert__audio_attachment", nil),senderName];
    }else if([messageType isEqualToString:kChatMessageTypeVideo]){
        text = [NSString stringWithFormat:JMOLocalizedString(@"incompatible_alert__video_attachment", nil),senderName];
    }else{
        text = [NSString stringWithFormat:JMOLocalizedString(@"incompatible_alert__unknown", nil),senderName];
    }

    jsqmessage.Text = text;
    jsqmessage.CustomParameters = dbqbchatmessage.CustomParameters;
    jsqmessage.Attachments = dbqbchatmessage.Attachments;
    jsqmessage.NumberofDeliverStatus = dbqbchatmessage.NumberofDeliverStatus;
    jsqmessage.DidDeliverStatus = dbqbchatmessage.DidDeliverStatus;
    jsqmessage.ErrorStatus = dbqbchatmessage.ErrorStatus;
    jsqmessage.DrawStatus = dbqbchatmessage.DrawStatus;
    jsqmessage.ReadStatus = dbqbchatmessage.ReadStatus;
    jsqmessage.Retrycount = dbqbchatmessage.Retrycount;
    jsqmessage.IsBlockedSendMessage=dbqbchatmessage.IsBlockedSendMessage;
    return jsqmessage;
}
////check empty
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

- (void)reloadChatRoomCollectionViewAfterSecond:(int)second {
    dispatch_time_t popTime =
    dispatch_time(DISPATCH_TIME_NOW, second * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        if ([self.delegate
             respondsToSelector:@selector(chatRoomCollectionViewReloadOnly)]) {
            [self.delegate chatRoomCollectionViewReloadOnly];
        }
    });
}

#pragma mark QBChatDelegate-----------------------------------------------------------------------------------------------------------------------
- (void)chatDidLogin {
    
}
- (void)chatDidFailWithError:(NSInteger)code {
    // relogin here
    
    [[ChatService shared] loginWithUser:[ChatService shared].currentUser
                        completionBlock:^{
                            
                        }];
}
- (void)chatDidFailWithStreamError:(NSError *)error {
    [ChatService shared].connecttoserversuccess = NO;
    
    if ([self.delegate
         respondsToSelector:@selector(chatDidFailWithStreamError:)]) {
        [self.delegate chatDidFailWithStreamError:error];
    }
}
- (void)chatDidReadMessageWithID:(NSString *)messageID dialogID:(NSString *)dialogID readerID:(NSUInteger)readerID {
    
    DBResultSet *r =
    [[[[DBQBChatMessage query] whereWithFormat:@" MessageID = %@", messageID]
      limit:1] fetch];
    DBQBChatMessage *chatDidReadMessage;
    for (DBQBChatMessage *chatDidReadMessageFromDB in r) {
        chatDidReadMessage = chatDidReadMessageFromDB;
        
        DBResultSet *r2 = [[[DBQBChatMessage query]
                            whereWithFormat:@" ReadStatus =0 AND DialogID=%@  And ErrorStatus=0",
                            chatDidReadMessageFromDB.DialogID] fetch];
        
        for (DBQBChatMessage *QBChatMessageFromDB in r2) {
            if ([QBChatMessageFromDB.DateSent
                 compare:chatDidReadMessageFromDB.DateSent] ==
                NSOrderedDescending) {
                DDLogInfo(@"date1 is later than date2");
                
            } else if ([QBChatMessageFromDB.DateSent
                        compare:chatDidReadMessageFromDB.DateSent] ==
                       NSOrderedAscending) {
                DDLogInfo(@"date1 is earlier than date2");
                QBChatMessageFromDB.ReadStatus = YES;
                [QBChatMessageFromDB commit];
            } else {
                DDLogInfo(@"date1 are the same");
                
                QBChatMessageFromDB.ReadStatus = YES;
                [QBChatMessageFromDB commit];
            }
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(chatDidReadMessageWithID:)]) {
        [self.delegate chatDidReadMessageWithID:chatDidReadMessage];
    }
}
- (void)chatDidDeliverMessageWithID:(NSString *)messageID dialogID:(NSString *)dialogID toUserID:(NSUInteger)userID {
    DBResultSet *r = [[[DBQBChatMessage query]
                       whereWithFormat:@" MessageID = %@", messageID] fetch];
    DBQBChatMessage *chatDidDeliverMessage;
    for (DBQBChatMessage *chatDidReadMessageFromDB in r) {
        chatDidDeliverMessage = chatDidReadMessageFromDB;
        
        DBResultSet *r2 = [[[DBQBChatMessage query]
                            whereWithFormat:@" DidDeliverStatus =%@ AND DialogID=%@ ", @"0",
                            chatDidReadMessageFromDB.DialogID] fetch];
        
        for (DBQBChatMessage *QBChatMessageFromDB in r2) {
            if ([QBChatMessageFromDB.MessageID isEqualToString:messageID]) {
                QBChatMessageFromDB.DidDeliverStatus = YES;
                [QBChatMessageFromDB commit];
            }
            
            if ([QBChatMessageFromDB.DateSent
                 compare:chatDidReadMessageFromDB.DateSent] ==
                NSOrderedDescending) {
                DDLogInfo(@"date1 is later than date2");
                
            } else if ([QBChatMessageFromDB.DateSent
                        compare:chatDidReadMessageFromDB.DateSent] ==
                       NSOrderedAscending) {
                DDLogInfo(@"date1 is earlier than date2");
                QBChatMessageFromDB.DidDeliverStatus = YES;
                [QBChatMessageFromDB commit];
            } else {
                DDLogInfo(@"dates are the same");
                
                QBChatMessageFromDB.DidDeliverStatus = YES;
                [QBChatMessageFromDB commit];
            }
        }
    }
    
    if ([self.delegate
         respondsToSelector:@selector(chatDidDeliverMessageWithID:)]) {
        [self.delegate chatDidDeliverMessageWithID:chatDidDeliverMessage];
    }
}
- (void)chatDidReceiveMessage:(QBChatMessage *)message{
    NSString *dialogId = message.dialogID;
    message= [self fillQBChatMessageNullValue:message];
    // notify observers
    //// check duplicate message
    DBResultSet *r = [[[DBQBChatMessage query]
                       whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
    if (r.count != 0) {
        return;
    }
    DBResultSet *r2 = [[[DBUnreadMessageLabel query]
                        whereWithFormat:@" DialogID = %@ And MemberID = %@", dialogId,[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] fetch];
    
    
    if (r2.count==0) {
        DBUnreadMessageLabel *dbUnreadMessageLabel = [DBUnreadMessageLabel new];
        dbUnreadMessageLabel.DialogID=dialogId;
        dbUnreadMessageLabel.MemberID=[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ;
        dbUnreadMessageLabel.QBID=[LoginBySocialNetworkModel shared].myLoginInfo.QBID;
        dbUnreadMessageLabel.MessageID=message.ID;
        dbUnreadMessageLabel.UnReadMessageCount=1;
        [dbUnreadMessageLabel commit];
    }else{
        for (DBUnreadMessageLabel *dbUnreadMessageLabel in r2) {
            dbUnreadMessageLabel.UnReadMessageCount++;
            [dbUnreadMessageLabel commit];
        }
        
    }
    
    BOOL processed = NO;
    if ([self.delegate respondsToSelector:@selector(chatDidReceiveMessage:)]) {
        processed = [self.delegate chatDidReceiveMessage:message];
    }
    
    if (!processed) {
        DBResultSet *r = [[[DBQBChatMessage query]
                           whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
        if (r.count == 0) {
     //// if database not have message save into database.
            DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
            
            dbhistorymessage.MessageID = message.ID;
            dbhistorymessage.DialogID = dialogId;
            dbhistorymessage.QBID =
            [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
            dbhistorymessage.MemberID =
            [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
            dbhistorymessage.RecipientID = message.recipientID;
            dbhistorymessage.SenderID = message.senderID;
            dbhistorymessage.DateSent = message.dateSent;
            dbhistorymessage.InsertDBDateTime = [NSDate date];
            if ([self isEmpty:[message.customParameters objectForKey:@"photourl"]]) {
                dbhistorymessage.PhotoType = NO;
            } else {
                dbhistorymessage.PhotoType = YES;
            }
            dbhistorymessage.QBChatHistoryMessage =
            [NSKeyedArchiver archivedDataWithRootObject:message];
            dbhistorymessage.ChatMessageType=message.customParameters[kChatMessageCustomParameterMessageType];
            dbhistorymessage.Text = message.text;
            dbhistorymessage.CustomParameters = message.customParameters;
            dbhistorymessage.Attachments = message.attachments;
            dbhistorymessage.NumberofDeliverStatus = 2;
            
            dbhistorymessage.DidDeliverStatus = YES;
            dbhistorymessage.ErrorStatus = NO;
            dbhistorymessage.DrawStatus = NO;
            dbhistorymessage.ReadStatus = NO;
            dbhistorymessage.Retrycount = 0;
            dbhistorymessage.IsDeleteMessage = NO;
            dbhistorymessage.IsBlockedSendMessage = NO;
            [dbhistorymessage commit];
            if ([[[ReceiveMessageCount query]
                  whereWithFormat:@"DialogID = %@", dialogId] count] == 0) {
                if ([dbhistorymessage.ChatMessageType isEqualToString:kChatMessageTypeAgentListing]) {
                    
                }else{
                ReceiveMessageCount *muteCaseReceiveMessageCount =
                [ReceiveMessageCount new];
                muteCaseReceiveMessageCount.DialogID = dialogId;
                muteCaseReceiveMessageCount.QBID = dbhistorymessage.QBID;
                muteCaseReceiveMessageCount.LastMessageText = message.text;
                muteCaseReceiveMessageCount.MemberID = dbhistorymessage.MemberID;
                muteCaseReceiveMessageCount.LastMessageDate = message.dateSent;
                muteCaseReceiveMessageCount.ReceiveMessageCount = 1;
                
                [muteCaseReceiveMessageCount commit];
                }
            } else {
                if ([dbhistorymessage.ChatMessageType isEqualToString:kChatMessageTypeAgentListing]) {
                    
                }else{
                DBResultSet *r = [[[ReceiveMessageCount query]
                                   whereWithFormat:@"DialogID = %@", dialogId] fetch];
                for (ReceiveMessageCount *muteCaseReceiveMessageCount in r) {
                    muteCaseReceiveMessageCount.ReceiveMessageCount =
                    muteCaseReceiveMessageCount.ReceiveMessageCount + 1;
                    muteCaseReceiveMessageCount.QBID = dbhistorymessage.QBID;
                    muteCaseReceiveMessageCount.LastMessageText = message.text;
                    muteCaseReceiveMessageCount.MemberID = dbhistorymessage.MemberID;
                    muteCaseReceiveMessageCount.LastMessageDate = message.dateSent;
                    [muteCaseReceiveMessageCount commit];
                }
                     }
            }
            
            ////IS Photo Message Download
            if (![self isEmpty:[message.customParameters objectForKey:@"photourl"]]) {
                NSURL *url = [NSURL
                              URLWithString:[message.customParameters objectForKey:@"photourl"]];
                
                [self.sdwebImagemanager downloadImageWithURL:url
                                                     options:0
                                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                        // progression tracking code
                                                    }
                                                   completed:^(UIImage *image, NSError *error,
                                                               SDImageCacheType cacheType, BOOL finished,
                                                               NSURL *imageURL) {
                                                       if (image) {
                                                           [[NSNotificationCenter defaultCenter]
                                                            postNotificationName:kChatRoomDownLoadPhotoWaitingListToReloadView
                                                            object:nil];
                                                         
                                                           DBResultSet *r = [[[[DBChatSetting query]
                                                                               whereWithFormat:@"MemberID = %@",
                                                                               [LoginBySocialNetworkModel shared]
                                                                               .myLoginInfo.MemberID] limit:1]
                                                                             
                                                                             fetch];
                                                           for (DBChatSetting *dbChatSetting in r) {
                                                               if (dbChatSetting.ChatRoomPhotoAutoDownload) {
                                                                   DDLogDebug(@"DBHasBeenDownloadPhoto %lu",(unsigned long)[[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [dbhistorymessage.CustomParameters objectForKey:@"photourl"]]
                                                                                                                             fetch]count]);
                                                                   if ([[[[DBHasBeenDownloadPhoto query] whereWithFormat:@" PhotoUrl = %@", [dbhistorymessage.CustomParameters objectForKey:@"photourl"]]
                                                                         fetch]count]==0) {
                                                                       UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                                       [DBHasBeenDownloadPhoto createSearchAgentHistoryWithPhotoUrl:[dbhistorymessage.CustomParameters objectForKey:@"photourl"]];
                                                                   }
                                                               }
                                                           }
                                                       }
                                                   }];
            }
        }
        [[ChatDialogModel shared]
         requestDialogUpdateWithId:dialogId
         completionBlock:^{
             
             [[ChatRelationModel shared]
              chatRoomDeleteFromDialogId:dialogId
              deleteStatus:@"0"
              Success:^(id responseObject) {
                  
              }
              failure:^(AFHTTPRequestOperation *operation,
                        NSError *error, NSString *errorMessage,
                        RequestErrorStatus errorStatus,
                        NSString *alert, NSString *ok){
                  
              }];
             
             [[NSNotificationCenter defaultCenter]
              postNotificationName:kNotificationDialogsUpdated
              object:nil];
             [[ChatDialogModel shared] updateChatBadgeNumber];
             
         }];
        
       // [[TWMessageBarManager sharedInstance] hideAll];
        
        [[ChatRelationModel shared]
         getCurrentChatRoomRelationObjectFromLocalForDialogId:dialogId];
         /**
         *  if mute not do any thing or show black top bar.
         */
        if (![ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty &&
            [ChatRelationModel shared].recipienerChatRoomRelation.Muted) {
        } else {
          NSString *messageType = [message.customParameters objectForKey:@"messageType"];
          if (!isEmpty(messageType)) {
            if ([messageType isEqualToString:kChatMessageTypeAgentListing]) {
            } else {
              NSString *messageText = message.text;
              if ([messageType isEqualToString:kChatMessageTypeImage] || [messageType isEqualToString:kChatMessageTypeAgentListing] || [messageType isEqualToString:kChatMessageTypeAudio] || [messageType isEqualToString:kChatMessageTypeVideo] ||
                  [messageType isEqualToString:kChatMessageTypeUnknown] || [messageType isEqualToString:kChatMessageTypeRate]) {
                messageText = messageType;
              }
                           if (self.groupMessageCount == 0) {
                self.groupMessageCount = 1;
              } else {
                self.groupMessageCount++;
                  self.topBarGroupMessage=message;
              }
                if (!self.groupMessage) {
                    
                    self.groupMessage=YES;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void) {
                      

                        if (self.groupMessageCount == 1 ||self.groupMessageCount ==0) {
                              AgentProfile *agentProfile = [[DialogPageAgentProfilesModel shared] getAgentProfileFromQBID:[NSString stringWithFormat:@"%li", message.senderID]];
                            [self showTopBlackBarMessageFromAgentProfile:agentProfile messageText:messageText];
                            self.groupMessageCount = 0;
                            self.groupMessage      = NO;
                        } else {
                            
                            NSString *groupMessageCount = [NSString stringWithFormat:@"%d", self.groupMessageCount];
                              AgentProfile *agentProfile = [[DialogPageAgentProfilesModel shared] getAgentProfileFromQBID:[NSString stringWithFormat:@"%li", self.topBarGroupMessage.senderID]];
                            [self showTopBlackBarMessageFromAgentProfile:agentProfile messageText:[NSString stringWithFormat:JMOLocalizedString(@"chat_lobby__group_unread_message", nil), groupMessageCount]];
                            self.groupMessageCount = 0;
                            self.groupMessage      = NO;
                            
                        }
                        
                    });
                    
                }
            }
          }
        }
    }
}
-(void)showTopBlackBarMessageFromAgentProfile:(AgentProfile *)agentProfile messageText:(NSString *)messageText{
       if (isEmpty(agentProfile)) {
        [[TWMessageBarManager sharedInstance] setStyleSheet:nil];
        [[TWMessageBarManager sharedInstance]
         showMessageWithTitle:@"Message"
         description:messageText
         type:TWMessageBarMessageTypeInfo];
           AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
           AudioServicesPlaySystemSound(1003);
    }else{
        [[TWMessageBarManager sharedInstance] setStyleSheet:self];
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:agentProfile.AgentPhotoURL]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    self.twMessageBarIcon =image;
                                }else{
                                    self.twMessageBarIcon=[UIImage imageNamed:@"icon-info.png"];
                                }
                                NSString * memberName = agentProfile.MemberName;
                                if (isEmpty(memberName)) {
                                    memberName=@"Message";
                                }
                                [[TWMessageBarManager sharedInstance]
                                 showMessageWithTitle:memberName
                                 description:messageText
                                 type:TWMessageBarMessageTypeInfo];
                                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                                AudioServicesPlaySystemSound(1003);
                            }];
    }

}
- (void)chatDidNotSendMessage:(QBChatMessage *)message error:(NSError *)error {
    
}

- (void)chatDidReceiveSystemMessage:(QBChatMessage *)message{
    NSString *isChatSetting = message.customParameters[@"isChatSetting"];
    NSString *messageType = message.customParameters[@"type"];
    if ([messageType isEqualToString:kSystemMessageTypeRefreshChatSetting]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRelationListUpdated object:nil];
    }
    
    if([messageType isEqualToString:kSystemMessageTypeAlert]){
        NSString *contentJSON = message.customParameters[@"content"];
        NSDictionary *content = [NSDictionary fromJsonString:contentJSON];
        NSString *title = content[@"title"];
        NSString *message = content[@"message"];
        NSString *cancelActionTitle = content[@"cancelActionTitle"];
        NSString *cancelActionURL = content[@"cancelActionURL"];
        NSString *goActionTitle = content[@"goActionTitle"];
        NSString *goActionURL = content[@"goActionURL"];


        //        NSString *urlString = content[@"url"];
        UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        if (goActionTitle) {
            UIAlertAction* goAction = [UIAlertAction actionWithTitle:JMOLocalizedString(goActionTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if ([goActionURL valid]) {
                    NSURL *url = [NSURL URLWithString:goActionURL];
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL:url];
                    }
                }
            }];
            [alertViewController addAction:goAction];
        }
        
        if (cancelActionTitle) {
            UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:JMOLocalizedString(cancelActionTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if ([cancelActionTitle valid]) {
                    NSURL *url = [NSURL URLWithString:cancelActionURL];
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL:url];
                    }
                }
            }];
            [alertViewController addAction:cancelAction];
        }
        
        if (!goActionTitle&&!cancelActionTitle) {
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:JMOLocalizedString(JMOLocalizedString(@"alert_ok", nil), nil) style:UIAlertActionStyleDefault handler:nil];
            [alertViewController addAction:defaultAction];
        }
        
        [[kAppDelegate topViewController] presentViewController:alertViewController animated:YES completion:nil];
    }
    
    
    if ([messageType isEqualToString:kSystemMessageTypeRefreshChatPrivacySetting]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSystemMessageTypeRefreshChatPrivacySetting object:nil];
    }
    
    
}

- (void)didReceiveNewSession:(QBRTCSession *)session userInfo:(NSDictionary *)userInfo {
    /* show incompatible alert */
    NSString *senderName = userInfo[kChatMessageCustomParameterSenderName];
    NSString *sessionType = userInfo[@"sessionType"];
    //    NSString *chatVersion = userInfo[@"chatVersion"];
    ChatFeatureType featureType = ChatFeatureTypeUnknown;
    
    if ([sessionType isEqualToString:@"videoCall"]) {
        featureType = ChatFeatureTypeVideoCall;
    }else if ([sessionType isEqualToString:@"audioCall"]){
        featureType = ChatFeatureTypeAudioCall;
    }
    NSMutableDictionary *rejectInfo = [[NSMutableDictionary alloc]init];
    [rejectInfo setObject:@"incompatible_alert__opponents_not_support" forKey:@"errorMessage"];
    [session rejectCall:rejectInfo];
    [self showIncompatibleAlert:featureType senderName:senderName];
}

- (void)session:(QBRTCSession *)session rejectedByUser:(NSNumber *)userID userInfo:(NSDictionary *)userInfo  {
    DDLogDebug(@"Rejected by user %@", userID);
    NSString *errorMessage = userInfo[@"errorMessage"];
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:nil message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertViewController addAction:ok];
    [[kAppDelegate topViewController] presentViewController:alertViewController animated:YES completion:nil];
}


-(void)showIncompatibleAlert:(ChatFeatureType)featureType senderName:(NSString*)senderName{
    NSString *incompatibleMessage = [self incompatibleMessage:featureType];
    NSString *alertMessage = incompatibleMessage;
    NSString *alertTitle = JMOLocalizedString(@"incompatible_alert__title", nil);
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* updateAction = [UIAlertAction actionWithTitle:JMOLocalizedString(@"incompatible_alert__action_update", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:@"http://apple.com"];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }];
    UIAlertAction* laterAction = [UIAlertAction actionWithTitle:JMOLocalizedString(@"incompatible_alert__action_later", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertViewController addAction:updateAction];
    [alertViewController addAction:laterAction];
    [[kAppDelegate topViewController] presentViewController:alertViewController animated:YES completion:nil];
}
#pragma mark TopBarLoginStatusView---------------------------------------------------------------------------------------------------------------------

- (void)tryConnectServer {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate quickBloxTotalLoginWithSuccessBlock:nil errorBlock:nil retry:1];
}

- (void)chatDidAccidentallyDisconnect {
    //  [[QBChat instance] removeAllDelegates];
    
    [ChatService shared].connecttoserversuccess = NO;
    _chatAccidentallyDisconnected = YES;
    
    DDLogInfo(@"chatDidAccidentallyDisconnect");
    
    // Stop presence timer
    if (self.presenceTimer) {
        
        [self.presenceTimer invalidate];
        self.presenceTimer = nil;
    }
    
    // Start reconnect timer
    if (self.reconnectTimer) {
        
        [self.reconnectTimer invalidate];
        self.reconnectTimer = nil;
    }
    
    self.reconnectTimer = [NSTimer scheduledTimerWithTimeInterval:kQBReconnectTimerInterval
                                                           target:self
                                                         selector:@selector(tryConnectServer)
                                                         userInfo:nil
                                                          repeats:YES];
    [self.reconnectTimer fire];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationChatDidAccidentallyDisconnect
     object:nil];
}

- (void)chatDidConnect{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationChatRoomConnectToServerSuccess
     object:nil];
    
    // Start send presence timer
    if (self.presenceTimer) {
        
        [self.presenceTimer invalidate];
        self.presenceTimer = nil;
    }
    
    self.presenceTimer = [NSTimer scheduledTimerWithTimeInterval:kQBPresenceTimerInterval
                                                          target:self
                                                        selector:@selector(sendPresence)
                                                        userInfo:nil
                                                         repeats:YES];
    
    // Stop reconnect timer
    if (self.reconnectTimer) {
        
        [self.reconnectTimer invalidate];
        self.reconnectTimer = nil;
    }
    
    if (_chatAccidentallyDisconnected ||
        [ChatDialogModel shared].dialogs.count > 0) {
        _chatAccidentallyDisconnected = NO;
    }
    
    if (self.loginCompletionBlock != nil) {
        self.loginCompletionBlock();
        self.loginCompletionBlock = nil;
    }
    
    [ChatService shared].connecttoserversuccess = YES;
    
    if ([self.delegate respondsToSelector:@selector(chatDidConnect)]) {
        [self.delegate chatDidConnect];
    }
    
    [[ChatDialogModel shared] requestDialogsWithCompletionBlock:^{
             [[NSNotificationCenter defaultCenter]
              postNotificationName:kNotificationDialogsUpdated
              object:nil];
             [[ChatDialogModel shared] updateChatBadgeNumber];
        
        
    }];
    
}

- (void)chatDidReconnect{
    
}

- (void)stopGetRecipienterTimer{
    [self.getrecipientertimer invalidate];
    self.getrecipientertimer = nil;
}

- (void)startGetRecipienterTimer{
    [self.getrecipientertimer invalidate];
    self.getrecipientertimer = [NSTimer scheduledTimerWithTimeInterval:kQBRecipientTimerInterval
                                     target:self
                                   selector:@selector(getRecipienterFromQBServer)
                                   userInfo:nil
                                    repeats:YES];
    [self.getrecipientertimer fire];
}
- (void)getRecipienterFromQBServer {
    //  DDLogInfo(@"getRecipienterFromQBServer");
    if ([self.delegate
         respondsToSelector:@selector(getRecipienterFromQBServer)]) {
        [self.delegate getRecipienterFromQBServer];
    }
}
- (void)sendPresence {
    if ([[QBChat instance] sendPresence]) {
        DDLogInfo(@"sendPresence-->");
    }
}

-(QBChatMessage *)fillQBChatMessageNullValue:(QBChatMessage*)qbchatmessage{
    
    
    if ([self isEmpty:qbchatmessage.text]) {
        qbchatmessage.text = @"";
    }
    if ([self isEmpty:qbchatmessage.dateSent]) {
        qbchatmessage.dateSent=[NSDate date];
    }
    return qbchatmessage;
}

- (void)createDialog:(QBChatDialog *)dialog successBlock:(void (^)(QBResponse * _Nonnull, QBChatDialog * _Nullable))successBlock errorBlock:(QBRequestErrorBlock)errorBlock{
    [QBRequest createDialog:dialog successBlock:^(QBResponse * _Nonnull response, QBChatDialog * _Nullable createdDialog) {
        successBlock(response,createdDialog);
    } errorBlock:^(QBResponse * _Nonnull response) {
        
    }];
}

- (void)startConferenceWithType:(QBRTCConferenceType)conferenceType opponentIDs:(NSArray*)opponentIDs{
    QBRTCSession *newSession = [QBRTCClient.instance createNewSessionWithOpponents:opponentIDs
                                                                withConferenceType:conferenceType];
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    NSString *sessionType = @"Unknown";
    if (conferenceType == QBRTCConferenceTypeAudio) {
        sessionType = @"audioCall";
    }else if (conferenceType == QBRTCConferenceTypeVideo){
        sessionType = @"videoCall";
    }
    NSString *senderName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
    
    [userInfo setObject:senderName forKey:kChatMessageCustomParameterSenderName];
    [userInfo setObject:sessionType forKey:@"sessionType"];
    [newSession startCall:userInfo];
}
- (void)sendSystemIncompatibleMessage:(ChatFeatureType)featureType dialog:(QBChatDialog*)dialog{
    NSString *title = JMOLocalizedString(@"incompatible_alert__title", nil);
    NSString *message = [self incompatibleMessage:featureType];
    NSString *goActionTitle = @"incompatible_alert__action_update";
    NSString *goActionURL =  @"http://apple.com";
    NSString *cancelActionTitle = @"incompatible_alert__action_later";
    NSString *cancelActionURL = nil;

    [self sendSystemErrorWithTitle:title message:message goActionTitle:goActionTitle goActionURL:goActionURL cancelActionTitle:cancelActionTitle cancelActionURL:cancelActionURL dialog:dialog];
}
- (void)sendSystemErrorWithTitle:(NSString*)title message:(NSString*)message goActionTitle:(NSString*)goActionTitle goActionURL:(NSString*)goActionURL cancelActionTitle:(NSString*)cancelActionTitle cancelActionURL:(NSString*)cancelActionURL dialog:(QBChatDialog*)dialog{
    QBChatMessage *errorMessage = [QBChatMessage message];
    
    NSString *type = kSystemMessageTypeAlert;
    NSMutableDictionary *contentDict = [[NSMutableDictionary alloc]init];
    [contentDict setObject:title forKey:@"title"];
    [contentDict setObject:message forKey:@"message"];
    [contentDict setObject:goActionTitle forKey:@"goActionTitle"];
    [contentDict setObject:goActionURL forKey:@"goActionURL"];
    [contentDict setObject:cancelActionTitle forKey:@"cancelActionTitle"];
    [contentDict setObject:cancelActionURL forKey:@"cancelActionURL"];
    
    NSMutableDictionary *customParams = [NSMutableDictionary new];
    customParams[@"type"] = type;
    customParams[@"content"] = [contentDict jsonPresentation];
    
    errorMessage.customParameters = customParams;
    [errorMessage setRecipientID:dialog.recipientID];
    [[QBChat instance] sendSystemMessage:errorMessage completion:^(NSError * _Nullable error) {
        
    }];
}

-(NSString*)incompatibleMessage:(ChatFeatureType)featureType {
    NSString *senderName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
    NSString *incompatibleMessage = @"";
    switch (featureType) {
        case ChatFeatureTypeUnknown:
            incompatibleMessage = JMOLocalizedString(@"incompatible_alert__unknown", nil);
            break;
        case ChatFeatureTypeAudioCall:
            incompatibleMessage = JMOLocalizedString(@"incompatible_alert__audio_call", nil);
            break;
        case ChatFeatureTypeVideoCall:
            incompatibleMessage = JMOLocalizedString(@"incompatible_alert__video_call", nil);
            break;
        case ChatFeatureTypeAudioAttachment:
            incompatibleMessage = JMOLocalizedString(@"incompatible_alert__audio_attachment", nil);
            break;
        case ChatFeatureTypeVideoAttachment:
            incompatibleMessage = JMOLocalizedString(@"incompatible_alert__video_attachment", nil);
            break;
        default:
            incompatibleMessage = JMOLocalizedString(@"incompatible_alert__unknown", nil);
            break;
    }
    
    NSString *alertMessage = [NSString stringWithFormat:incompatibleMessage,senderName];
    return alertMessage;
}

- (UIImage *)iconImageForMessageType:(TWMessageBarMessageType)type{
    return self.twMessageBarIcon;
    
}
- (UIColor *)backgroundColorForMessageType:(TWMessageBarMessageType)type{
    return [UIColor colorWithWhite:0 alpha:0.8];
    
}

@end
