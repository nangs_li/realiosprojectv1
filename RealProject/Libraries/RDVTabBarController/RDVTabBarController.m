// RDVTabBarController.m
// RDVTabBarController
//
// Copyright (c) 2013 Robert Dimitrov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import <objc/runtime.h>
#import "RealUtility.h"
@interface UIViewController (RDVTabBarControllerItemInternal)
- (void)rdv_setTabBarController:(RDVTabBarController *)tabBarController;

@end

@interface RDVTabBarController () {
    UIView *_contentView;
}

@property (nonatomic, readwrite) RDVTabBar *tabBar;
@property (nonatomic,assign) CGFloat currentNavBarHeight;

@property (nonatomic,assign) NavigationBarType currentBarType;
@property (nonatomic,strong) UIView *navBar;
@property (nonatomic,strong) UIView *loadingView;
@property (nonatomic,strong) UIActivityIndicatorView *loadingIndicator;
@property (nonatomic,strong) UIView *animatingView;
@property (nonatomic, assign) BOOL didSetupConstraints;

@property (nonatomic, strong) NSLayoutConstraint *navBarHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint *tabBarBottomSpacingConstraint;
@end

@implementation RDVTabBarController

#pragma mark - View lifecycle

-(void)loadView{
    self.view = [UIView new];
        self.view.backgroundColor = RealNavBlueColor;
    [self.view addSubview:[self contentView]];
    [self.view addSubview:[self tabBar]];
    [self.view addSubview:[self navBar]];
    [self addAllNavBar];
    [self.view addSubview:[self pageControl]];
    [self.view setNeedsUpdateConstraints];
}

- (void)updateViewConstraints{
    if (!self.didSetupConstraints) {
        self.didSetupConstraints = YES;
         [[self navBar]autoPinEdgeToSuperviewEdge:ALEdgeLeading];
        [[self navBar]autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
        [[self navBar]autoPinEdgeToSuperviewEdge:ALEdgeTop];
        self.currentNavBarHeight = 64;
        self.navBarHeightConstraint = [[self navBar] autoSetDimension:ALDimensionHeight toSize:64];
        
        [[self contentView] autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:[self navBar]];
        [[self contentView]autoPinEdgeToSuperviewEdge:ALEdgeLeading];
        [[self contentView]autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
        [[self contentView]autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        
        self.didSetupConstraints = YES;
        [[self tabBar]autoPinEdgeToSuperviewEdge:ALEdgeLeading];
        [[self tabBar]autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
       self.tabBarBottomSpacingConstraint = [[self tabBar]autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        
        [[self tabBar]autoSetDimension:ALDimensionHeight toSize:49];
        
        [[self pageControl] autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:0];
        [[self pageControl] autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:0];
        [[self pageControl] autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:54];
        [[self pageControl] autoSetDimension:ALDimensionHeight toSize:30];
        
            for (UIView *navBar in [self customNavBarController].navBars) {
                [navBar autoPinEdgesToSuperviewEdges];
            }
        
    }
    [super updateViewConstraints];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setSelectedIndex:[self selectedIndex]];
    
    [self setTabBarHidden:self.isTabBarHidden animated:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.selectedViewController.preferredStatusBarStyle;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return self.selectedViewController.preferredStatusBarUpdateAnimation;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    UIInterfaceOrientationMask orientationMask = UIInterfaceOrientationMaskAll;
    for (UIViewController *viewController in [self viewControllers]) {
        if (![viewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
            return UIInterfaceOrientationMaskPortrait;
        }
        
        UIInterfaceOrientationMask supportedOrientations = [viewController supportedInterfaceOrientations];
        
        if (orientationMask > supportedOrientations) {
            orientationMask = supportedOrientations;
        }
    }
    
    return orientationMask;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    for (UIViewController *viewCotroller in [self viewControllers]) {
        if (![viewCotroller respondsToSelector:@selector(shouldAutorotateToInterfaceOrientation:)] ||
            ![viewCotroller shouldAutorotateToInterfaceOrientation:toInterfaceOrientation]) {
            return NO;
        }
    }
    return YES;
}

#pragma mark - Methods

- (UIViewController *)selectedViewController {
    return [[self viewControllers] objectAtIndex:[self selectedIndex]];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex {
    if (selectedIndex >= self.viewControllers.count) {
        return;
    }
    
    if ([self selectedViewController]) {
        [[self selectedViewController] willMoveToParentViewController:nil];
        [[[self selectedViewController] view] removeFromSuperview];
        [[self selectedViewController] removeFromParentViewController];
    }
    
    _selectedIndex = selectedIndex;
    [[self tabBar] setSelectedItem:[[self tabBar] items][selectedIndex]];
    
    [self setSelectedViewController:[[self viewControllers] objectAtIndex:selectedIndex]];
    [self addChildViewController:[self selectedViewController]];
    /*
     CATransition* transition = [CATransition animation];
     transition.duration = ANIMATION_DURATION;
     transition.type = kCATransitionMoveIn;
     transition.subtype = kCATransitionFromTop;
     
     [[self contentView].layer addAnimation:transition forKey:kCATransition];
     */
    //    [UIView transitionWithView:[self contentView]
    //                      duration:0.8
    //                       options:UIViewAnimationOptionShowHideTransitionViews
    //                    animations:^{
    [[self contentView] addSubview:[self selectedViewController].view];
    [[self selectedViewController].view autoPinEdgesToSuperviewEdges];
    //                    }
    //                    completion:NULL];
    
    
    
    [[self selectedViewController] didMoveToParentViewController:self];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)setViewControllers:(NSArray *)viewControllers {
    if (_viewControllers && _viewControllers.count) {
        for (UIViewController *viewController in _viewControllers) {
            [viewController willMoveToParentViewController:nil];
            [viewController.view removeFromSuperview];
            [viewController removeFromParentViewController];
        }
    }
    
    if (viewControllers && [viewControllers isKindOfClass:[NSArray class]]) {
        _viewControllers = [viewControllers copy];
        
        NSMutableArray *tabBarItems = [[NSMutableArray alloc] init];
        
        for (UIViewController *viewController in viewControllers) {
            RDVTabBarItem *tabBarItem = [[RDVTabBarItem alloc] init];
            [tabBarItem setTitle:viewController.title];
            [tabBarItems addObject:tabBarItem];
            [viewController rdv_setTabBarController:self];
        }
        
        [[self tabBar] setItems:tabBarItems];
    } else {
        for (UIViewController *viewController in _viewControllers) {
            [viewController rdv_setTabBarController:nil];
        }
        
        _viewControllers = nil;
    }
}

- (NSInteger)indexForViewController:(UIViewController *)viewController {
    UIViewController *searchedController = viewController;
    if ([searchedController navigationController]) {
        searchedController = [searchedController navigationController];
    }
    return [[self viewControllers] indexOfObject:searchedController];
}

- (RDVTabBar *)tabBar {
    if (!_tabBar) {
        _tabBar = [[RDVTabBar alloc] init];
        [_tabBar setBackgroundColor:[UIColor clearColor]];
        [_tabBar setDelegate:self];
    }
    return _tabBar;
}

-(UIView *)navBar{
    if (!_navBar) {
        _navBar =[[UIView alloc]initWithFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, RealNavBarHeight)];
        _navBar.backgroundColor = RealNavBlueColor;
    }
    
    return _navBar;
}

-(UIPageControl*)pageControl{
    if (!_pageControl) {
        _pageControl =[[UIPageControl alloc]init];
        _pageControl.currentPageIndicatorTintColor = RealBlueColor;
        _pageControl.pageIndicatorTintColor = [UIColor colorWithWhite:0.8 alpha:0.8];
        _pageControl.frame = CGRectMake(0, 0, [RealUtility screenBounds].size.width, 30);
        _pageControl.numberOfPages = 3;
        _pageControl.currentPage = 1;
        _pageControl.userInteractionEnabled = NO;
        _pageControl.hidden = YES;
        _pageControlHidden = YES;
    }
    
    return _pageControl;
}

-(CustomNavigationBarController*) customNavBarController{
    if (!_customNavBarController) {
        _customNavBarController = [[CustomNavigationBarController alloc]init];
        _customNavBarController.view.frame = CGRectZero;
    }
    return _customNavBarController;
}
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        [_contentView setBackgroundColor:[UIColor clearColor]];
    }
    return _contentView;
}

- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated {
    if (_tabBarHidden == hidden) {
        return;
    }
    
    _tabBarHidden = hidden;
    __weak RDVTabBarController *weakSelf = self;
    [weakSelf.tabBar.layer removeAllAnimations];
    [[weakSelf contentView].layer removeAllAnimations];
    
    void (^block)() = ^{
        if (hidden) {
            weakSelf.tabBarBottomSpacingConstraint.constant = 49;
        }else{
            weakSelf.tabBarBottomSpacingConstraint.constant = 0;
        }
        [weakSelf.view layoutIfNeeded];
    };
    
    void (^completion)(BOOL) = ^(BOOL finished){
        if(finished){
//            if (hidden) {
//                [[weakSelf tabBar] setHidden:YES];
//            }else{
//                [[weakSelf tabBar] setHidden:NO];
//            }
        }
    };
    
    if (animated) {
        [UIView animateWithDuration:0.24 animations:block completion:completion];
    } else {
        block();
        completion(YES);
    }
}

- (void)setTabBarHidden:(BOOL)hidden {
    [self setTabBarHidden:hidden animated:YES];
}

-(void)setNavBarHidden:(BOOL)hidden animated:(BOOL)animated{
    if (_navBarHidden == hidden) {
        return;
    }
    _navBarHidden = hidden;
    
    __weak RDVTabBarController *weakSelf = self;
    [weakSelf.navBar.layer removeAllAnimations];
    [[weakSelf contentView].layer removeAllAnimations];
    void (^block)() = ^{
        if (hidden) {
             weakSelf.navBarHeightConstraint.constant = 0;
        }else{
             weakSelf.navBarHeightConstraint.constant = weakSelf.currentNavBarHeight;
        }
        
        [weakSelf.view layoutIfNeeded];
       
    };
    
    void (^completion)(BOOL) = ^(BOOL finished){
        if(finished){
            if (hidden) {
                [[weakSelf navBar] setHidden:YES];
            }else{
                [[weakSelf navBar] setHidden:NO];
            }
        }
    };
    
    if (animated) {
        [UIView animateWithDuration:0.24 animations:block completion:completion];
    } else {
        block();
        completion(YES);
    }
}

- (void)adjustNavBarHeight:(CGFloat)newHeight animated:(BOOL)animated{
    if (self.navBarHeightConstraint.constant == newHeight) {
        return;
    }
    _currentNavBarHeight = newHeight;
    __weak RDVTabBarController *weakSelf = self;
    [weakSelf.navBar.layer removeAllAnimations];
    [[weakSelf contentView].layer removeAllAnimations];
    void (^block)() = ^{
        weakSelf.navBarHeightConstraint.constant = weakSelf.currentNavBarHeight;
        [weakSelf.view layoutIfNeeded];
    };
    
    void (^completion)(BOOL) = ^(BOOL finished){
        if(finished){
            
        }
    };
    
    if (animated) {
        [UIView animateWithDuration:0.24 animations:block completion:completion];
    } else {
        block();
        completion(YES);
    }
    
}

- (void)changePage:(NSInteger)newPage{
    self.pageControl.currentPage = newPage;
}
-(void)setPageControlHidden:(BOOL)hidden animated:(BOOL)animated{
    if(_pageControlHidden != hidden){
        _pageControlHidden = hidden;
        self.pageControl.hidden = NO;
        void (^block)() = ^{
            self.pageControl.alpha = !hidden;
        };
        
        void (^completion)(BOOL) = ^(BOOL finished){
            self.pageControl.hidden = hidden;
        };
        
        if (animated) {
            [UIView animateWithDuration:0.35 animations:block completion:completion];
        }else{
            block();
            completion(YES);
        }
    }
}

-(void)addAllNavBar{
    
    for (UIView *navBar in [self customNavBarController].navBars) {
        navBar.frame = [self navBar].bounds;
        navBar.alpha =0;
        [[self navBar]addSubview:navBar];
    }
}

- (void)hideAllNavBar{
    for (UIView *navBar in [self customNavBarController].navBars) {
        navBar.alpha =0;
    }
}

- (void)changeNavBarType:(NavigationBarType)newBarType animated:(BOOL)animated block:(NavigationBarBlock)navBlock{
//    [self hideAllNavBar];
    navBlock(YES,self.currentBarType,[self customNavBarController]);
}

#pragma mark - RDVTabBarDelegate

- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index {
    if ([[self delegate] respondsToSelector:@selector(tabBarController:shouldSelectViewController:)]) {
        if (![[self delegate] tabBarController:self shouldSelectViewController:[self viewControllers][index]]) {
            return NO;
        }
    }
    
    if ([self selectedViewController] == [self viewControllers][index]) {
        if ([[self selectedViewController] isKindOfClass:[UINavigationController class]]) {
            UINavigationController *selectedController = (UINavigationController *)[self selectedViewController];
            
            if ([selectedController topViewController] != [selectedController viewControllers][0]) {
                [selectedController popToRootViewControllerAnimated:YES];
            }
        }
        
        return NO;
    }
    
    return !_tabBarHidden;
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index {
    if (index < 0 || index >= [[self viewControllers] count]) {
        return;
    }
    
    [self setSelectedIndex:index];
    
    if ([[self delegate] respondsToSelector:@selector(tabBarController:didSelectViewController:)]) {
        
        [[self delegate] tabBarController:self didSelectViewController:[self viewControllers][index]];
    }
}


-(void)initLoadingViewIfNeeded{
    if (!self.loadingView) {
        self.loadingView = [[UIView alloc]initWithFrame:self.view.bounds];
        self.loadingView.backgroundColor =[UIColor colorWithWhite:0.0 alpha:0.8f];
    }
    
    if (!self.loadingIndicator) {
        self.loadingIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.loadingIndicator.center = self.loadingView.center;
    }
    
    if (!self.loadingIndicator.superview) {
        [self.loadingView addSubview:self.loadingIndicator];
        [self.loadingIndicator autoCenterInSuperview];
    }
    
    if (!self.loadingView.superview) {
        self.loadingView.hidden = YES;
        self.loadingView.alpha = 0.0f;
        [self.view addSubview:self.loadingView];
        [self.loadingView autoCenterInSuperview];
        
    }
}

-(void)showLoading{
    [self initLoadingViewIfNeeded];
    [self.view bringSubviewToFront:self.loadingView];
    self.loadingView.alpha = 0.0f;
    self.loadingView.hidden = NO;
    [self.loadingIndicator startAnimating];
    [UIView animateWithDuration:0.3f animations:^{
        self.loadingView.alpha = 1.0f;
    }];
    
    dispatch_time_t popTime =
    dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        // Do something...
        [self hideLoading];
    });

}

-(void)hideLoading{
    [self initLoadingViewIfNeeded];
    [UIView animateWithDuration:0.3f animations:^{
        self.loadingView.alpha = 0.0f;
    }completion:^(BOOL finished) {
        if (finished) {
            self.loadingView.hidden = YES;
            [self.loadingIndicator stopAnimating];
        }
    }];
}

@end

#pragma mark - UIViewController+RDVTabBarControllerItem

@implementation UIViewController (RDVTabBarControllerItemInternal)

- (void)rdv_setTabBarController:(RDVTabBarController *)tabBarController {
    objc_setAssociatedObject(self, @selector(rdv_tabBarController), tabBarController, OBJC_ASSOCIATION_ASSIGN);
}

@end

@implementation UIViewController (RDVTabBarControllerItem)

- (RDVTabBarController *)rdv_tabBarController {
    RDVTabBarController *tabBarController = objc_getAssociatedObject(self, @selector(rdv_tabBarController));
    
    if (!tabBarController && self.parentViewController) {
        tabBarController = [self.parentViewController rdv_tabBarController];
    }
    
    return tabBarController;
}

- (RDVTabBarItem *)rdv_tabBarItem {
    RDVTabBarController *tabBarController = [self rdv_tabBarController];
    NSInteger index = [tabBarController indexForViewController:self];
    return [[[tabBarController tabBar] items] objectAtIndex:index];
}

- (void)rdv_setTabBarItem:(RDVTabBarItem *)tabBarItem {
    RDVTabBarController *tabBarController = [self rdv_tabBarController];
    
    if (!tabBarController) {
        return;
    }
    
    RDVTabBar *tabBar = [tabBarController tabBar];
    NSInteger index = [tabBarController indexForViewController:self];
    
    NSMutableArray *tabBarItems = [[NSMutableArray alloc] initWithArray:[tabBar items]];
    [tabBarItems replaceObjectAtIndex:index withObject:tabBarItem];
    [tabBar setItems:tabBarItems];
}

@end
