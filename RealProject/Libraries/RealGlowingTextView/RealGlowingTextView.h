//
//  RealGlowingTextView.h
//  productionreal2
//
//  Created by Alex Hung on 22/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealGlowingTextView : UITextView
@property (nonatomic, assign) BOOL alwaysGlowing;
@property (nonatomic, assign) BOOL showShadow;
@property (nonatomic, assign) CGFloat borderCornerRadius;
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, strong) UIColor *glowingColor;

@end
