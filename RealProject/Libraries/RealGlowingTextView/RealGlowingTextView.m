//
//  RealGlowingTextView.m
//  productionreal2
//
//  Created by Alex Hung on 22/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealGlowingTextView.h"
@interface RealGlowingTextView() {
    UIColor *_backgroundColor;
}

- (void)_configureView;

@end

@implementation RealGlowingTextView

- (void)_configureView{

    self.clipsToBounds = YES;
    
    if (!self.backgroundColor)
    {
        self.backgroundColor = [UIColor whiteColor];
    }
    
    if (!self.glowingColor)
    {
        self.glowingColor = [UIColor colorWithRed:(82.f / 255.f) green:(168.f / 255.f) blue:(236.f / 255.f) alpha:0.8];
    }
    
    if (!self.borderColor)
    {
        self.borderColor = [UIColor lightGrayColor];
    }
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.borderCornerRadius;
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = self.borderColor.CGColor;
    
    self.layer.shadowColor = self.glowingColor.CGColor;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.borderCornerRadius].CGPath;
    self.layer.shadowOpacity = 0;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowRadius = 5.f;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.alwaysGlowing = NO;
        [self _configureView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.alwaysGlowing = NO;
        [self _configureView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.alwaysGlowing = NO;
        [self _configureView];
    }
    return self;
}

- (UIColor *)backgroundColor
{
    return _backgroundColor;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:[UIColor clearColor]];
    _backgroundColor = backgroundColor;
}

- (void)setGlowingColor:(UIColor *)glowingColor
{
    if ([self isFirstResponder] || self.alwaysGlowing) {
        [self animateBorderColorFrom:(id)self.layer.borderColor to:(id)glowingColor.CGColor shadowOpacityFrom:(id)[NSNumber numberWithFloat:self.showShadow] to:(id)[NSNumber numberWithFloat:self.showShadow]];
    }
    
    _glowingColor = glowingColor;
    
    self.layer.shadowColor = glowingColor.CGColor;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    _borderColor = borderColor;
    
    if (![self isFirstResponder] && !self.alwaysGlowing)
    {
        self.layer.borderColor = self.borderColor.CGColor;
    }
}

- (void)setAlwaysGlowing:(BOOL)alwaysGlowing
{
    if (_alwaysGlowing && !alwaysGlowing && ![self isFirstResponder]) {
        [self hideGlowing];
    } else if (!_alwaysGlowing && alwaysGlowing && ![self isFirstResponder]) {
        [self showGlowing];
    }
    
    _alwaysGlowing = alwaysGlowing;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self _configureView];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    [_backgroundColor set];
    [[UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.borderCornerRadius] fill];
}

- (void)animateBorderColorFrom:(id)fromColor to:(id)toColor shadowOpacityFrom:(id)fromOpacity to:(id)toOpacity
{
    CABasicAnimation *borderColorAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    borderColorAnimation.fromValue = fromColor;
    borderColorAnimation.toValue = toColor;
    
    CABasicAnimation *shadowOpacityAnimation = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    shadowOpacityAnimation.fromValue = fromOpacity;
    shadowOpacityAnimation.toValue = toOpacity;
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = 1.0f / 3.0f;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    group.removedOnCompletion = NO;
    group.fillMode = kCAFillModeForwards;
    group.animations = @[borderColorAnimation, shadowOpacityAnimation];
    
    [self.layer addAnimation:group forKey:nil];
}

- (BOOL)becomeFirstResponder
{
    BOOL result = [super becomeFirstResponder];
    
    if (result && !self.alwaysGlowing)
    {
        [self showGlowing];
    }
    return result;
}

- (BOOL)resignFirstResponder
{
    BOOL result = [super resignFirstResponder];
    
    if (result && !self.alwaysGlowing)
    {
        [self hideGlowing];
    }
    return result;
}

- (void)showGlowing
{
    [self animateBorderColorFrom:(id)self.layer.borderColor to:(id)self.layer.shadowColor shadowOpacityFrom:(id)[NSNumber numberWithFloat:0.f] to:(id)[NSNumber numberWithFloat:self.showShadow]];
}

- (void)hideGlowing
{
    [self animateBorderColorFrom:(id)self.layer.borderColor to:(id)self.borderColor.CGColor shadowOpacityFrom:(id)[NSNumber numberWithFloat:self.showShadow] to:(id)[NSNumber numberWithFloat:0.f]];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 8, 2);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 8, 2);
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 8, 2);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
