//
//  TDDatePickerController.h
//
//  Created by Nathan  Reed on 30/09/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import	"TDSemiModal.h"

@protocol TDPickerDelegate;

@interface TDPicker : TDSemiModalViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak) id<TDPickerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) NSString *residentialorcommercial;
@property (strong, nonatomic) NSMutableArray *pickerData;
@property (weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) NSString* selectpickerdata;
@property (nonatomic, assign) NSInteger selectindex;
@property (nonatomic, retain) NSString* propertyorspacepicker;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveBtn;
-(IBAction)saveDateEdit:(id)sender;
-(IBAction)clearDateEdit:(id)sender;
-(IBAction)cancelDateEdit:(id)sender;

@end

@protocol TDPickerDelegate <NSObject>

- (void)pickerSetDate:(TDPicker*)viewController;
- (void)pickerClearDate:(TDPicker*)viewController;
- (void)pickerCancel:(TDPicker*)viewController;
- (void)picker:(UIPickerView *)picker didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
@end

