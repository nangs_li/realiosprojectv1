//
//  TDDatePickerController.m
//
//  Created by Nathan  Reed on 30/09/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import "TDPicker.h"

@implementation TDPicker

-(void)viewDidLoad
{
    [super viewDidLoad];

	

	// we need to set the subview dimensions or it will not always render correctly
	// http://stackoverflow.com/questions/1088163
	for (UIView* subview in self.picker.subviews) {
		subview.frame = self.picker.bounds;
	}
    self.picker.dataSource=self;
    self.picker.delegate=self;
    self.cancelBtn.title = JMOLocalizedString(@"common__cancel", nil);
    self.saveBtn.title = JMOLocalizedString(@"error_message__done", nil);
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark -
#pragma mark Actions

-(IBAction)saveDateEdit:(id)sender {
	if([self.delegate respondsToSelector:@selector(pickerSetDate:)]) {
		[self.delegate pickerSetDate:self];
        
        
        
	}
}

-(IBAction)clearDateEdit:(id)sender {
	if([self.delegate respondsToSelector:@selector(pickerClearDate:)]) {
		[self.delegate pickerClearDate:self];
	}
}

-(IBAction)cancelDateEdit:(id)sender {
	if([self.delegate respondsToSelector:@selector(pickerCancel:)]) {
		[self.delegate pickerCancel:self];
	} else {
		// just dismiss the view automatically?
	}
}

#pragma mark -PickerView Delegate
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([self.delegate respondsToSelector:@selector(pickerCancel:)]) {
        [self.delegate picker:pickerView didSelectRow:row inComponent:component];
    }}







#pragma mark -
#pragma mark Memory Management

- (void)viewDidUnload
{
    [self setToolbar:nil];
    [super viewDidUnload];

	self.picker = nil;
	self.delegate = nil;

}


@end


