//
//  TDDatePickerController.m
//
//  Created by Nathan  Reed on 30/09/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import "TDTwoPicker.h"

@implementation TDTwoPicker

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // we need to set the subview dimensions or it will not always render correctly
    // http://stackoverflow.com/questions/1088163
    for (UIView* subview in self.picker.subviews) {
        subview.frame = self.picker.bounds;
    }
    self.picker.dataSource=self;
    self.picker.delegate=self;
    self.yearArray = @[
                              @"2016",
                              @"2015",
                              @"2014",
                              @"2013",
                              @"2012",
                              @"2011",
                              @"2010",
                              @"2009",
                              @"2008",
                              @"2007",
                              @"2006",
                              @"2005",
                              @"2004",
                              @"2003",
                              @"2002",
                              @"2001",
                              @"2000",
                              @"1999",
                              @"1998",
                              @"1997",
                              @"1996",
                              @"1995",
                              @"1994",
                              @"1993",
                              @"1992",
                              @"1991",
                              @"1990",
                              @"1989",
                              @"1988",
                              @"1987",
                              @"1986",
                              @"1985",
                              @"1984",
                              @"1983",
                              @"1982",
                              @"1981",
                              @"1980",
                              @"1979",
                              @"1978",
                              @"1977",
                              @"1976",
                              @"1975",
                              @"1974",
                              @"1973",
                              @"1972",
                              @"1971",
                              @"1970"
                              ];
    NSMutableArray *months = [[NSMutableArray alloc]init];
    for (int i = 0; i <12; i++) {
        [months addObject:[RealUtility getMonthNameByIndex:i]];
    }
    self.monthArray = [NSArray arrayWithArray:months];
    if (!self.year) {
    self.year = self.yearArray[0];
    }
    if (!self.month) {
    self.month = self.monthArray[0];
    }
    [self.picker reloadAllComponents];
    self.cancelBtn.title = JMOLocalizedString(@"common__cancel", nil);
    self.saveBtn.title = JMOLocalizedString(@"error_message__done", nil);
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark -
#pragma mark Actions

-(IBAction)saveDateEdit:(id)sender {
    if([self.delegate respondsToSelector:@selector(tdDatePickerSetDate:)]) {
        [self.delegate tdDatePickerSetDate:self];
        
        
        
    }
}

-(IBAction)clearDateEdit:(id)sender {
    if([self.delegate respondsToSelector:@selector(tdDatePickerClearDate:)]) {
        [self.delegate tdDatePickerClearDate:self];
    }
}

-(IBAction)cancelDateEdit:(id)sender {
    if([self.delegate respondsToSelector:@selector(tdDatePickerCancel:)]) {
        [self.delegate tdDatePickerCancel:self];
    } else {
        // just dismiss the view automatically?
    }
}

#pragma mark -PickerView Delegate
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// The number of rows of data


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component== 1)
    {
        return [self.yearArray count];
    }
    else
    {
        return [self.monthArray count];
    }
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component== 1)
    {
        return self.yearArray[row];
    }
    else
    {
        return self.monthArray[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([self.delegate respondsToSelector:@selector(tdDatePicker:didSelectRow:inComponent:)]) {
        [self.delegate tdDatePicker:pickerView didSelectRow:row inComponent:component];
    }}







#pragma mark -
#pragma mark Memory Management

- (void)viewDidUnload
{
    [self setToolbar:nil];
    [super viewDidUnload];
    
    self.picker = nil;
    self.delegate = nil;
    
}


@end


