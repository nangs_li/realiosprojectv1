//
//  CTAssetsPickerController+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 14/3/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CTAssetsPickerController (Utility)

- (void)showHint:(NSString*)hint;
- (void)hideHint;
@end
