//
//  CTAssetsPickerController+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 14/3/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "CTAssetsPickerController+Utility.h"
#import "UIViewController+Hint.h"
@implementation CTAssetsPickerController (Utility)

-(void)showHint:(NSString*)hint{
    UIViewController *controller = self.childSplitViewController.childViewControllers.firstObject;
    if ([controller isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navController = (UINavigationController *)controller;
        UIViewController *visibleController =navController.viewControllers.lastObject;
        [visibleController showHint:hint];
    }else{
        [controller showHint:hint];
    }
}


- (void)hideHint{
    UIViewController *controller = self.childSplitViewController.childViewControllers.firstObject;
    if ([controller isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navController = (UINavigationController *)controller;
        UIViewController *visibleController =navController.viewControllers.lastObject;
        [visibleController hideHint];
    }else{
        [controller hideHint];
    }
}

@end
