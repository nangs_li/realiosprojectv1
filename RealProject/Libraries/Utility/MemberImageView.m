//
//  MemberImageView.m
//  productionreal2
//
//  Created by Alex Hung on 17/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "MemberImageView.h"

@implementation MemberImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)layoutSubviews{
    [super layoutSubviews];
    self.layer.cornerRadius = self.frame.size.height/2;
}

@end
