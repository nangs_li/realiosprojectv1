//
//  NSDate+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 24/3/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utility)
- (NSString*)lastSeenDateString;
@end
