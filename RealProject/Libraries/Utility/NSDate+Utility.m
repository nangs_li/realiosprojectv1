//
//  NSDate+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 24/3/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "NSDate+Utility.h"

@implementation NSDate (Utility)

- (NSString*)lastSeenDateString{
    //// only between day
    NSInteger interval = abs((int)[self timeIntervalSinceDate:[NSDate date]]);
    if (interval <= kQBCheckOnlineTimePeriod) {
        return JMOLocalizedString(@"chatroom__online", nil);
    }
    NSDateFormatter *Onlydayformat = [NSDateFormatter localeDateFormatter];
    [Onlydayformat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTimestring = [Onlydayformat stringFromDate:self];
    NSString *nowDateTimestring = [Onlydayformat stringFromDate:[NSDate date]];
    DDLogInfo(@"dateTimestring-->%@", dateTimestring);
    DDLogInfo(@"nowDateTimestring-->%@", nowDateTimestring);
    NSDate *dateTimeOnlydayformat = [Onlydayformat dateFromString:dateTimestring];
    NSDate *nowDateOnlydayformat =
    [Onlydayformat dateFromString:nowDateTimestring];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc]
                                     initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components =
    [gregorianCalendar components:NSCalendarUnitDay
                         fromDate:dateTimeOnlydayformat
                           toDate:nowDateOnlydayformat
                          options:NSCalendarWrapComponents];
    DDLogInfo(@"components.day-->%ld", (long)components.day);
    //// only between time
    
    //  NSInteger DayInterval;
    // NSInteger DayModules;
    if (components.day >= 7) {
        
        [Onlydayformat setDateFormat:@"d/M/y"];
        return [NSString stringWithFormat:JMOLocalizedString(@"chatroom__last_seen", nil), [[Onlydayformat stringFromDate:self] lowercaseString]];
        
    } else if (components.day >= 2) {
        
        [Onlydayformat setDateFormat:@"EEEE"];
  
        return [NSString stringWithFormat:JMOLocalizedString(@"chatroom__last_seen", nil), [[Onlydayformat stringFromDate:self]lowercaseString]];
        
    } else if (components.day == 1) {
        NSDate *timeAgoDate = [NSDate dateWithTimeIntervalSinceNow:-86400];
        return   [NSString stringWithFormat:JMOLocalizedString(@"chatroom__last_seen", nil), [JMOLocalizedString(@"common__yesterday", nil) lowercaseString]] ;
 
    }
    
    else {
        [Onlydayformat setDateFormat:@"h:mm a"];
        return [NSString stringWithFormat:JMOLocalizedString(@"chatroom__last_seen_today", nil), [[Onlydayformat stringFromDate:self]lowercaseString]];
    }
}

@end
