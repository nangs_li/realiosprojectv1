//
//  NSURL+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 10/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "NSDateFormatter+Utility.h"
static char NSDATEFORMATTER;
@implementation NSDateFormatter (Utility)

#pragma mark -
//- (NSDateFormatter *)NSDateFormatter {
//    return (NSDateFormatter *)objc_getAssociatedObject(self, &NSDATEFORMATTER);
//}
//- (void)setNSDateFormatterFromCurrentLanguage{
//     NSLocale *Locale = [[NSLocale alloc] initWithLocaleIdentifier:[LanguagesManager sharedInstance].currentLanguage];
//      [self.NSDateFormatter setLocale:Locale];
//    
//}

+ (NSDateFormatter *)localeDateFormatter{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    NSLocale *Locale = [[NSLocale alloc] initWithLocaleIdentifier:[LanguagesManager sharedInstance].currentLanguage];
    [dateFormatter setLocale:Locale];
    return dateFormatter;
}

+ (NSDateFormatter *)enUsDateFormatter{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    return dateFormatter;
}

-(void)setupDateFormatter{
    [self setDateFormat:@"h:mm a"];
    NSString * languageCode= self.locale.localeIdentifier;
    if ([languageCode isEqualToString:@"fr"]) {
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [self setLocale:usLocale];
    }

    if ([languageCode isEqualToString:@"zh-Hant"]||[languageCode isEqualToString:@"zh-Hans"]) {
         [self setDateFormat:@"a h:mm"];
    }
}

@end
