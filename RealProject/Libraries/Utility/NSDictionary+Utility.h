//
//  NSDictionary+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 18/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Utility)
-(NSString*)jsonPresentation;
+ (NSDictionary*) fromJsonString:(NSString*)jsonString;
@end
