//
//  NSObject+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 18/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "NSObject+Utility.h"

@implementation NSObject (Utility)
-(BOOL) valid{
    if ([self isKindOfClass:[NSNull class]] || self == nil){
        return false;
    }
    
    if ([self isKindOfClass:[NSString class]]) {
        NSString *string = (NSString*)self;
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        return string && string.length > 0;
    }else if([self isKindOfClass:[NSDictionary class]]){
        NSDictionary *dict = (NSDictionary*)self;
        return dict && dict.allKeys.count>0;
    }else if([self isKindOfClass:[NSArray class]]){
        NSArray *array = (NSArray*)self;
        return array && array.count;
    }

    return YES;
}
@end
