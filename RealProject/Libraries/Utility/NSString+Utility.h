//
//  NSString+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

-(void)fillIfNil;
- (NSString*)trim;
-(NSArray*)tags;
- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font;
- (CGSize)getSizeWithFont:(UIFont *)font;
@end
