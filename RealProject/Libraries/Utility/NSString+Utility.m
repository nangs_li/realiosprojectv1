//
//  NSString+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)


-(void)fillIfNil{
    
}

- (NSString*) trim{
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    return trimmedString;
}

-(NSArray*)tags{
    NSMutableArray *tags = [[NSMutableArray alloc]init];
    for (int i = 1; i <= self.length; i++) {
        NSString *subString = [self substringToIndex:i];
        [tags addObject:subString];
    }
    return tags;
}
- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

-(CGSize)getSizeWithFont:(UIFont *)font {
    DDLogDebug(@"NSString self %@",self);
  CGSize size         = [self sizeWithAttributes:@{NSFontAttributeName : font}];
  CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
  // Values are fractional -- you should take the ceilf to get equivalent values
  return  adjustedSize;
}

-(void)addUniqueKey{
    [self stringByReplacingOccurrencesOfString:@"}"
                                   withString:@""];
    NSString *uniqueKey =
    [NSString stringWithFormat:@"\"UniqueKey\":\"%f\"}",
     [[NSDate date] timeIntervalSince1970] , nil];
    [self stringByAppendingString:uniqueKey];
 
}
@end
