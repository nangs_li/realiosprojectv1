//
//  NSURL+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 10/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Utility)
- (BOOL)isEquivalent:(NSURL *)aURL;
@end
