//
//  PhotoEditorView.h
//  productionreal2
//
//  Created by Alex Hung on 17/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^PhotoCropBlock)(UIImage *croppedImage);

@protocol PhotosEditorViewDelegate <NSObject>

-(void) photoEditorScrollViewDidChange:(UIScrollView*)scrollView;

@end

@interface PhotoEditorView : UIView <UIScrollViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) id<PhotosEditorViewDelegate>delegate;
@property (nonatomic, assign)CGSize cropSize;
//@property (nonatomic, strong) UIView *imageContainerView;
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic, strong) UIImage *editingImage;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *maskView;
@property (nonatomic, assign) BOOL isRotating;
@property (nonatomic) CGFloat lastRotation;
@property (nonatomic) CGFloat lastZoomScale;
@property (nonatomic,assign) CGFloat originalZoomScale;

-(void)cropImageViewBlock:(PhotoCropBlock)block;
-(void)restorePreviousScrollViewStatus:(UIScrollView*)scrollView;
@end
