//
//  PhotoEditorView.m
//  productionreal2
//
//  Created by Alex Hung on 17/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "PhotoEditorView.h"
#import "RealUtility.h"
#import "CGGeometry+RSKImageCropper.h"

#define preferedScrollViewWidth [RealUtility screenBounds].size.width -20*2
@implementation PhotoEditorView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(instancetype)init{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self  = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self commonInit];
}

-(void) commonInit{
    self.frame = CGRectMake(0, 0, preferedScrollViewWidth, preferedScrollViewWidth);
    if (!_scrollView){
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, self.bounds.size.width+200, self.bounds.size.width+200
                                                                     )];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.minimumZoomScale = 1.0;
        _scrollView.maximumZoomScale = 6.0;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.delegate = self;
        UIRotationGestureRecognizer  *rotation = [[UIRotationGestureRecognizer alloc]initWithTarget:self action:@selector(handleRotate:)];
        rotation.delegate =self;
        [_scrollView addGestureRecognizer:rotation];
        
        
        [_scrollView setZoomScale:_scrollView.minimumZoomScale];
    }
    if (!_imageView){
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_imageView addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
    }
    
//    if (!_imageContainerView) {
//        _imageContainerView = [[UIView alloc]initWithFrame:_imageView.bounds];
//        _imageContainerView.backgroundColor = [UIColor clearColor];
//        _imageContainerView.userInteractionEnabled = NO;
//    }
    
    if (!_maskView){
        _maskView = [[UIImageView alloc] initWithFrame:self.bounds];
        _maskView.userInteractionEnabled = NO;
    }
    
    
    if (_scrollView.superview) {
        return;
    }
    
//    [_imageContainerView addSubview:_imageView];
    
    [self addSubview:_scrollView];
    [_scrollView setViewAlignmentInSuperView:ViewAlignmentCenter padding:0];
    [self.scrollView addSubview:_imageView];
    self.imageView.image = _editingImage;
    [self insertSubview:_maskView aboveSubview:_scrollView];
    self.backgroundColor = [UIColor clearColor];
}

- (void)setCropSize:(CGSize)size{
    _cropSize = size;
    _maskView.image = [self overlayMask];
    
}

-(void)setEditingImage:(UIImage *)editingImage{
    if (_editingImage == editingImage) {
        return;
    }
    _editingImage = editingImage;
    _imageView.image = _editingImage;
}

- (UIImage *)overlayMask{
    return [self squareOverlayMask];
}

- (UIImage *)squareOverlayMask{
    // Constants
    CGRect bounds = self.bounds;
    
    CGFloat width = self.cropSize.width;
    CGFloat height = self.cropSize.height;
    CGFloat margin = (bounds.size.height-self.cropSize.height)/2;
    CGFloat lineWidth = 1.0;
    
    // Create the image context
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, 0);
    
    // Create the bezier path & drawing
    UIBezierPath *clipPath = [UIBezierPath bezierPath];
    [clipPath moveToPoint:CGPointMake(width, margin)];
    [clipPath addLineToPoint:CGPointMake(0, margin)];
    [clipPath addLineToPoint:CGPointMake(0, 0)];
    [clipPath addLineToPoint:CGPointMake(width, 0)];
    [clipPath addLineToPoint:CGPointMake(width, margin)];
    [clipPath closePath];
    [clipPath moveToPoint:CGPointMake(width, CGRectGetHeight(bounds))];
    [clipPath addLineToPoint:CGPointMake(0, CGRectGetHeight(bounds))];
    [clipPath addLineToPoint:CGPointMake(0, margin+height)];
    [clipPath addLineToPoint:CGPointMake(width, margin+height)];
    [clipPath addLineToPoint:CGPointMake(width, CGRectGetHeight(bounds))];
    [clipPath closePath];
    [[UIColor colorWithWhite:0 alpha:0.5] setFill];
    [clipPath fill];
    
    // Add the square crop
    CGRect rect = CGRectMake(lineWidth/2, margin+lineWidth/2, width-lineWidth, self.cropSize.height-lineWidth);
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRect:rect];
    [[UIColor colorWithWhite:1.0 alpha:0.5] setStroke];
    maskPath.lineWidth = lineWidth;
    [maskPath stroke];
    
    //Create the image using the current context.
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - Key Value Observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([object isEqual:self.imageView] && [keyPath isEqualToString:@"image"]) {
        [self setMaxMinZoomScalesForCurrentBounds];
//            [self resetScrollViewInset];
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)setMaxMinZoomScalesForCurrentBounds {
    
    // Reset
    _scrollView.maximumZoomScale = 1;
    _scrollView.minimumZoomScale = 1;
    _scrollView.zoomScale = 1;
    
    // Bail if no image
    if (_imageView.image == nil) return;
    
    // Reset position
    
    // Sizes
    CGSize imageSize = AspectFitSize(self.imageView.image.size, self.imageView.frame.size);
    DDLogDebug(@"imageSize:%@",NSStringFromCGSize(imageSize));
    CGSize boundsSize = _maskView.bounds.size;
    // Calculate Min
    CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
    CGFloat minScale = MAX(xScale, yScale);                 // use minimum of these to allow the image to become fully visible
    self.originalZoomScale =minScale;
    // Calculate Max
    CGFloat maxScale = 5;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // Let them go a bit bigger on a bigger screen!
        maxScale = 4;
    }
    _imageView.frame = CGRectMake(0, 0, imageSize.width*minScale, imageSize.height*minScale);
//    _imageContainerView.frame = _imageView.bounds;
    // Image is smaller than screen so no zooming!
//        if (xScale >= 1 && yScale >= 1) {
//            minScale = 1.01;
//        }
    DDLogDebug(@"scaledImageView:%@",NSStringFromCGRect(_imageView.frame));
    DDLogDebug(@"fixImageSize:%@",NSStringFromCGSize(imageSize));
    
    
    //
    //    CGFloat verticalInset =  _imageView.frame.size.height != boundsSize.height ?fabs(_scrollView.frame.size.height - _imageView.frame.size.height) : fabs(_scrollView.frame.origin.x);
    //    CGFloat horizontalInset = _imageView.frame.size.width != boundsSize.width ?fabs(_scrollView.frame.size.width- _imageView.frame.size.width) : fabs(_scrollView.frame.origin.x);
    //    UIEdgeInsets minContentInset = UIEdgeInsetsMake(verticalInset/2, horizontalInset, verticalInset/2, horizontalInset);
    _scrollView.contentInset = UIEdgeInsetsMake(100, 100, 100, 100);
    // Set min/max zoom
    _scrollView.maximumZoomScale = maxScale;
    _scrollView.minimumZoomScale = 1;
    
    // Initial zoom
    _scrollView.zoomScale = 1;
    
    // If we're zooming to fill then centralise
    if (_scrollView.zoomScale != minScale) {
        
        // Centralise
        CGFloat newContentOffsetX = _imageView.frame.size.width /2 - _scrollView.bounds.size.width/2;
        CGFloat newContentOffsetY = _imageView.frame.size.height /2 - _scrollView.bounds.size.height/2;
        _scrollView.contentOffset = CGPointMake(newContentOffsetX,newContentOffsetY);
        
    }
//
    // Disable scrolling initially until the first pinch to fix issues with swiping on an initally zoomed in photo
    
    // If it's a video then disable zooming
    // Layout
    [self setNeedsLayout];
    
}

- (CGFloat)initialZoomScaleWithMinScale {
    CGFloat zoomScale = _scrollView.minimumZoomScale;
    if (_imageView ) {
        // Zoom image to fill if the aspect ratios are fairly similar
        CGSize boundsSize = _scrollView.bounds.size;
        boundsSize.width -= fabs(_scrollView.frame.origin.x);
        boundsSize.height -=fabs(_scrollView.frame.origin.y);
        CGSize imageSize = _imageView.image.size;
        CGFloat boundsAR = boundsSize.width / boundsSize.height;
        CGFloat imageAR = imageSize.width / imageSize.height;
        CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
        CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
        // Zooms standard portrait images on a 3.5in screen but not on a 4in screen.
        if (ABS(boundsAR - imageAR) < 0.17) {
            zoomScale = MAX(xScale, yScale);
            // Ensure we don't zoom in or out too far, just in case
            zoomScale = MIN(MAX(_scrollView.minimumZoomScale, zoomScale), _scrollView.maximumZoomScale);
        }
    }
    return zoomScale;
}

- (void)updateScrollViewContentInset
{
    CGSize imageSize = AspectFitSize(self.imageView.image.size, self.imageView.frame.size);
    
    CGFloat maskHeight =  self.cropSize.height;
    
    CGFloat hInset =  0.0;
    CGFloat vInset = fabs((maskHeight-imageSize.height)/2);
    
    if (vInset == 0) vInset = 0.25;
    
    UIEdgeInsets inset = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
    
    self.scrollView.contentInset = inset;
}

CGSize AspectFitSize(CGSize aspectRatio, CGSize boundingSize){
    CGFloat hRatio = boundingSize.width / aspectRatio.width;
    CGFloat vRation = boundingSize.height / aspectRatio.height;
    if (hRatio < vRation) {
        boundingSize.height = boundingSize.width / aspectRatio.width * aspectRatio.height;
    }
    else if (vRation < hRatio) {
        boundingSize.width = boundingSize.height / aspectRatio.height * aspectRatio.width;
    }
    return boundingSize;
}

- (void)acceptEdition:(id)sender
{
    if (self.scrollView.zoomScale > self.scrollView.maximumZoomScale || !self.imageView.image) {
        return;
    }
    
    dispatch_queue_t exampleQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(exampleQueue, ^{
        
        UIImage *editedImage = [self trimmedImage:[self editedImage]];
        //
        //        dispatch_queue_t queue = dispatch_get_main_queue();
        //        dispatch_async(queue, ^{
        //
        //            if (self.acceptBlock) {
        //
        //                NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
        //                                                 [NSValue valueWithCGRect:self.guideRect], UIImagePickerControllerCropRect,
        //                                                 @"public.image", UIImagePickerControllerMediaType,
        //                                                 @(self.cropMode), DZNPhotoPickerControllerCropMode,
        //                                                 @(self.scrollView.zoomScale), DZNPhotoPickerControllerCropZoomScale,
        //                                                 nil];
        //
        //                if (self.editingImage) [userInfo setObject:self.editingImage forKey:UIImagePickerControllerOriginalImage];
        //                else [userInfo setObject:self.imageView.image forKey:UIImagePickerControllerOriginalImage];
        //
        //                if (editedImage) [userInfo setObject:editedImage forKey:UIImagePickerControllerEditedImage];
        //
        //                self.acceptBlock(self, userInfo);
        //            }
        //        });
    });
}

/*
 The final edited photo rendering.
 */
- (UIImage *)editedImage
{
    UIImage *image = nil;
    
    CGRect viewRect = self.bounds;
    CGRect guideRect = [self guideRect];
    
    CGFloat verticalMargin = (viewRect.size.height-guideRect.size.height)/2;
    
    guideRect.origin.x = -self.scrollView.contentOffset.x;
    guideRect.origin.y = -self.scrollView.contentOffset.y - verticalMargin;
    
    //    if (DZN_IS_IPAD && self.cropMode == DZNPhotoEditorViewControllerCropModeCircular) {
    //        guideRect.origin.y -= CGRectGetHeight(self.navigationController.navigationBar.bounds)/2.0;
    //    }
    
    UIGraphicsBeginImageContextWithOptions(guideRect.size, NO, 0);{
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextTranslateCTM(context, guideRect.origin.x, guideRect.origin.y);
        [self.scrollView.layer renderInContext:context];
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return image;
}

- (CGRect)guideRect
{
    CGFloat margin = (CGRectGetHeight(self.bounds)-self.cropSize.height)/2;
    return CGRectMake(0.0, margin, self.cropSize.width, self.cropSize.height);
}
/**
 Trims the image removing any alpha in its edges.
 Code from http://stackoverflow.com/a/12617031/590010
 */
- (UIImage *)trimmedImage:(UIImage *)image
{
    CGImageRef inImage = image.CGImage;
    CFDataRef m_DataRef;
    m_DataRef = CGDataProviderCopyData(CGImageGetDataProvider(inImage));
    
    UInt8 * m_PixelBuf = (UInt8 *) CFDataGetBytePtr(m_DataRef);
    
    size_t width = CGImageGetWidth(inImage);
    size_t height = CGImageGetHeight(inImage);
    
    CGPoint top,left,right,bottom;
    
    BOOL breakOut = NO;
    for (int x = 0;breakOut==NO && x < width; x++) {
        for (int y = 0; y < height; y++) {
            NSInteger loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                left = CGPointMake(x, y);
                breakOut = YES;
                break;
            }
        }
    }
    
    breakOut = NO;
    for (int y = 0;breakOut==NO && y < height; y++) {
        
        for (int x = 0; x < width; x++) {
            
            NSInteger loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                top = CGPointMake(x, y);
                breakOut = YES;
                break;
            }
            
        }
    }
    
    breakOut = NO;
    for (NSInteger y = height-1;breakOut==NO && y >= 0; y--) {
        
        for (NSInteger x = width-1; x >= 0; x--) {
            
            NSInteger loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                bottom = CGPointMake(x, y);
                breakOut = YES;
                break;
            }
            
        }
    }
    
    breakOut = NO;
    for (NSInteger x = width-1;breakOut==NO && x >= 0; x--) {
        
        for (NSInteger y = height-1; y >= 0; y--) {
            
            NSInteger loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                right = CGPointMake(x, y);
                breakOut = YES;
                break;
            }
            
        }
    }
    
    
    CGFloat scale = image.scale;
    
    CGRect cropRect = CGRectMake(left.x / scale, top.y/scale, (right.x - left.x)/scale, (bottom.y - top.y) / scale);
    
    UIGraphicsBeginImageContextWithOptions(cropRect.size, NO, scale);
    [image drawAtPoint:CGPointMake(-cropRect.origin.x, -cropRect.origin.y) blendMode:kCGBlendModeCopy alpha:1.];
    
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CFRelease(m_DataRef);
    return croppedImage;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
- (IBAction)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    UIGestureRecognizerState state = [recognizer state];
    if (state == UIGestureRecognizerStateBegan) {
        
    }else if (state ==UIGestureRecognizerStateEnded || state == UIGestureRecognizerStateCancelled){
        //        _isRotating = NO;
    }
    
    _scrollView.transform = CGAffineTransformRotate(_scrollView.transform, recognizer.rotation);
    recognizer.rotation = 0;
    [self resetScrollViewInset];
    //    CGSize scrollViewContentSize = self.scrollView.contentSize;
    //    CGSize imageSize = AspectFitSize(self.imageView.image.size,scrollViewContentSize);
    //    CGSize containerSizee = self.scrollView.contentSize;
    //    if (containerSizee.width >containerSizee.height) {
    //        containerSizee.height = containerSizee.width;
    //    }else{
    //        containerSizee.width = containerSizee.height;
    //    }
    
    //
    //    double scaleToFill = [self scaleToFill:containerSizee withSize:imageSize atAngle:radians];
    //    double preferdZoomScale = scaleToFill;
    //    if(preferdZoomScale > _scrollView.maximumZoomScale){
    //        _scrollView.maximumZoomScale = preferdZoomScale;
    //    }
    //    if (_scrollView.zoomScale< preferdZoomScale) {
    //      _scrollView.zoomScale = preferdZoomScale;
    //    }
}

- (NSArray *)intersectionPointsOfLineSegment:(RSKLineSegment)lineSegment withRect:(CGRect)rect
{
    RSKLineSegment top = RSKLineSegmentMake(CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect)),
                                            CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect)));
    
    RSKLineSegment right = RSKLineSegmentMake(CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect)),
                                              CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect)));
    
    RSKLineSegment bottom = RSKLineSegmentMake(CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect)),
                                               CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect)));
    
    RSKLineSegment left = RSKLineSegmentMake(CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect)),
                                             CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect)));
    
    CGPoint p0 = RSKLineSegmentIntersection(top, lineSegment);
    CGPoint p1 = RSKLineSegmentIntersection(right, lineSegment);
    CGPoint p2 = RSKLineSegmentIntersection(bottom, lineSegment);
    CGPoint p3 = RSKLineSegmentIntersection(left, lineSegment);
    
    NSMutableArray *intersectionPoints = [@[] mutableCopy];
    if (!RSKPointIsNull(p0)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p0]];
    }
    if (!RSKPointIsNull(p1)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p1]];
    }
    if (!RSKPointIsNull(p2)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p2]];
    }
    if (!RSKPointIsNull(p3)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p3]];
    }
    
    return [intersectionPoints copy];
}


- (void)resetScrollViewInset{
//    return;
    CGRect frame;
    CGFloat angle = atan2f(_scrollView.transform.b, _scrollView.transform.a);
    CGFloat degrees = angle * (180 / M_PI);
    // Step 1: Rotate the left edge of the initial rect of the image scroll view clockwise around the center by `rotationAngle`.
    CGRect initialRect = _maskView.bounds;
    CGFloat rotationAngle = angle;
    
    CGPoint leftTopPoint = CGPointMake(initialRect.origin.x, initialRect.origin.y);
    CGPoint leftBottomPoint = CGPointMake(initialRect.origin.x, initialRect.origin.y + initialRect.size.height);
    RSKLineSegment leftLineSegment = RSKLineSegmentMake(leftTopPoint, leftBottomPoint);
    
    CGPoint pivot = RSKRectCenterPoint(initialRect);
    
    CGFloat alpha = fabs(rotationAngle);
    RSKLineSegment rotatedLeftLineSegment = RSKLineSegmentRotateAroundPoint(leftLineSegment, pivot, alpha);
    
    // Step 2: Find the points of intersection of the rotated edge with the initial rect.
    NSArray *points = [self intersectionPointsOfLineSegment:rotatedLeftLineSegment withRect:initialRect];
    
    // Step 3: If the number of intersection points more than one
    // then the bounds of the rotated image scroll view does not completely fill the mask area.
    // Therefore, we need to update the frame of the image scroll view.
    // Otherwise, we can use the initial rect.
    CGFloat altitude =0;
    CGFloat x = 0;
    CGFloat y = 0;
    if (points.count > 1) {
        // We have a right triangle.
        
        // Step 4: Calculate the altitude of the right triangle.
        if ((alpha > M_PI_2) && (alpha < M_PI)) {
            alpha = alpha - M_PI_2;
        } else if ((alpha > (M_PI + M_PI_2)) && (alpha < (M_PI + M_PI))) {
            alpha = alpha - (M_PI + M_PI_2);
        }
        CGFloat sinAlpha = sin(alpha);
        CGFloat cosAlpha = cos(alpha);
        CGFloat tanAlpha = tan(alpha);
        CGFloat hypotenuse = RSKPointDistance([points[0] CGPointValue], [points[1] CGPointValue]);
        altitude = hypotenuse * sinAlpha * cosAlpha;
        
        // Step 5: Calculate the target width.
        CGFloat initialWidth = CGRectGetWidth(initialRect);
        CGFloat targetWidth = initialWidth + altitude * 2;
        
        // Step 6: Calculate the target frame.
        CGFloat scale = targetWidth / initialWidth;
        CGPoint center = RSKRectCenterPoint(initialRect);
        frame = RSKRectScaleAroundPoint(initialRect, center, scale, scale);
        
        // Step 7: Avoid floats.
        frame.origin.x = round(CGRectGetMinX(frame));
        frame.origin.y = round(CGRectGetMinY(frame));
        frame = CGRectIntegral(frame);
        y = (_maskView.bounds.size.width - hypotenuse)/ (1+tanAlpha);
        x = tanAlpha *y;
    }

    if (x>=y) {
        x= y;
    }
    CGSize imageSize = AspectFitSize(self.imageView.image.size, self.maskView.frame.size);
    CGSize boundsSize = frame.size;
    // Calculate Min
    CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
    CGFloat minScale = MAX(xScale, yScale);
    minScale = minScale -self.originalZoomScale;
    minScale = 1+minScale;
    _scrollView.minimumZoomScale = minScale;
    DDLogDebug(@"y:%f",y);
    DDLogDebug(@"x:%f",x);
    DDLogDebug(@"altitude:%f",altitude);
    DDLogDebug(@"angle:%f",degrees);
    DDLogDebug(@"minScale:%f",minScale);
    _scrollView.contentInset = UIEdgeInsetsMake(100 -x, 100-x, 100-x, 100-x);
    if ([self.delegate respondsToSelector:@selector(photoEditorScrollViewDidChange:)]) {
        [self.delegate photoEditorScrollViewDidChange:_scrollView];
    }
}

-(double)scaleToFill:(CGSize)containerSize withSize:(CGSize)contentSize atAngle:(double)angle {
    double theta = fabs(angle - 2*M_PI*truncf(angle/M_PI/2) - M_PI);
    if (theta > M_PI_2) {
        theta = fabs(M_PI - theta);
    }
    double h = contentSize.height;
    double H = containerSize.height;
    double w = contentSize.width;
    double W = containerSize.width;
    double scale1 = (H*cos(theta) + W*sin(theta))/h;
    double scale2 = (H*sin(theta) + W*cos(theta))/w;
    double scaleFactor = MAX(scale1, scale2);
    return scaleFactor;
}

-(double)scaleToFit:(CGSize)contentSize atAngle:(double)angle withinSize:(CGSize)containerSize {
    double theta = fabs(angle - 2*M_PI*truncf(angle/M_PI/2) - M_PI);
    if (theta > M_PI_2) {
        theta = fabs(M_PI - theta);
    }
    double h = contentSize.height;
    double H = containerSize.height;
    double w = contentSize.width;
    double W = containerSize.width;
    double scale1 = H/(h*cos(theta) + w*sin(theta));
    double scale2 = W/(h*sin(theta) + w*cos(theta));
    double scaleFactor = MIN(scale1, scale2);
    return scaleFactor;
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
//     DDLogDebug(@"scrollview Offset:%@",NSStringFromCGPoint(_scrollView.contentOffset));
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([self.delegate respondsToSelector:@selector(photoEditorScrollViewDidChange:)]) {
        [self.delegate photoEditorScrollViewDidChange:scrollView];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.isRotating?nil:self.imageView;
}


- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    _lastZoomScale = self.scrollView.zoomScale;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale{
    
    [self resetScrollViewInset];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    //    CGRect innerFrame = _imageView.frame;
    //    CGRect scrollerBounds = scrollView.bounds;
    //
    //    if ( ( innerFrame.size.width < scrollerBounds.size.width ) || ( innerFrame.size.height < scrollerBounds.size.height ) )
    //    {
    //        CGFloat tempx = _imageView.center.x - ( scrollerBounds.size.width / 2 );
    //        CGFloat tempy = _imageView.center.y - ( scrollerBounds.size.height / 2 );
    //        CGPoint myScrollViewOffset = CGPointMake( tempx, tempy);
    //
    //        scrollView.contentOffset = myScrollViewOffset;
    //
    //    }
    //
    //    UIEdgeInsets anEdgeInset = { 0, 0, 0, 0};
    //    if ( scrollerBounds.size.width > innerFrame.size.width )
    //    {
    //        anEdgeInset.left = (scrollerBounds.size.width - innerFrame.size.width) / 2;
    //        anEdgeInset.right = -anEdgeInset.left;  // I don't know why this needs to be negative, but that's what works
    //    }
    //    if ( scrollerBounds.size.height > innerFrame.size.height )
    //    {
    //        anEdgeInset.top = (scrollerBounds.size.height - innerFrame.size.height) / 2;
    //        anEdgeInset.bottom = -anEdgeInset.top;  // I don't know why this needs to be negative, but that's what works
    //    }
    //    scrollView.contentInset = anEdgeInset;
}

- (void)dealloc
{
    [_imageView removeObserver:self forKeyPath:@"image" context:nil];
    
//    _imageView.image = nil;
//    _imageView = nil;
//    _scrollView = nil;
//    _editingImage = nil;
    
}

-(void)restorePreviousScrollViewStatus:(UIScrollView*)scrollView{
    _scrollView.transform = scrollView.transform;
    _scrollView.zoomScale = scrollView.zoomScale;
    _scrollView.minimumZoomScale = scrollView.minimumZoomScale;
    _scrollView.maximumZoomScale =scrollView.maximumZoomScale;
    _scrollView.contentInset = scrollView.contentInset;
    _scrollView.contentOffset = scrollView.contentOffset;

}

-(void)cropImageViewBlock:(PhotoCropBlock)block;{
//    if ([self.delegate respondsToSelector:@selector(imageCropViewController:willCropImage:)]) {
//        [self.delegate imageCropViewController:self willCropImage:self.originalImage];
//    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        UIImage *image = nil;
        CGRect viewRect = self.bounds;
        _maskView.hidden = YES;
        
        UIGraphicsBeginImageContextWithOptions(viewRect.size, self.opaque, 0);{
            CGContextRef context = UIGraphicsGetCurrentContext();
            
            CGContextTranslateCTM(context, viewRect.origin.x, viewRect.origin.y);
            [self.layer renderInContext:context];
            
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            _maskView.hidden = NO;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (block) {
                block(image);
            }
        });
    });
}

- (CGRect)cropRect{
    CGRect cropRect = CGRectZero;
    float zoomScale = 1.0 / _scrollView.zoomScale;
    
    cropRect.origin.x = round(_scrollView.contentOffset.x+_scrollView.contentInset.left * zoomScale);
    cropRect.origin.y = round((_scrollView.contentOffset.y+_scrollView.contentInset.top )* zoomScale);
    cropRect.size.width = CGRectGetWidth(_scrollView.bounds) * zoomScale;
    cropRect.size.height = CGRectGetHeight(_scrollView.bounds) * zoomScale;
    
    cropRect = CGRectIntegral(cropRect);
    
    return cropRect;
}

- (UIImage *)croppedImage:(UIImage *)image cropRect:(CGRect)cropRect rotationAngle:(CGFloat)rotationAngle zoomScale:(CGFloat)zoomScale maskPath:(UIBezierPath *)maskPath applyMaskToCroppedImage:(BOOL)applyMaskToCroppedImage
{
    // Step 1: check and correct the crop rect.
    CGSize imageSize = image.size;
    CGFloat x = CGRectGetMinX(cropRect);
    CGFloat y = CGRectGetMinY(cropRect);
    CGFloat width = CGRectGetWidth(cropRect);
    CGFloat height = CGRectGetHeight(cropRect);
    
    UIImageOrientation imageOrientation = image.imageOrientation;
    if (imageOrientation == UIImageOrientationRight || imageOrientation == UIImageOrientationRightMirrored) {
        cropRect.origin.x = y;
        cropRect.origin.y = round(imageSize.width - CGRectGetWidth(cropRect) - x);
        cropRect.size.width = height;
        cropRect.size.height = width;
    } else if (imageOrientation == UIImageOrientationLeft || imageOrientation == UIImageOrientationLeftMirrored) {
        cropRect.origin.x = round(imageSize.height - CGRectGetHeight(cropRect) - y);
        cropRect.origin.y = x;
        cropRect.size.width = height;
        cropRect.size.height = width;
    } else if (imageOrientation == UIImageOrientationDown || imageOrientation == UIImageOrientationDownMirrored) {
        cropRect.origin.x = round(imageSize.width - CGRectGetWidth(cropRect) - x);
        cropRect.origin.y = round(imageSize.height - CGRectGetHeight(cropRect) - y);
    }
    
    // Step 2: create an image using the data contained within the specified rect.
    CGImageRef croppedCGImage = CGImageCreateWithImageInRect(image.CGImage, cropRect);
    UIImage *croppedImage = [UIImage imageWithCGImage:croppedCGImage scale:1.0f orientation:imageOrientation];
    CGImageRelease(croppedCGImage);
    
    // Step 3: fix orientation of the cropped image.
    croppedImage = [croppedImage fixOrientation];
    
    // Step 4: If current mode is `RSKImageCropModeSquare` and the image is not rotated
    // or mask should not be applied to the image after cropping and the image is not rotated,
    // we can return the cropped image immediately.
    // Otherwise, we must further process the image.
        // Step 5: create a new context.
        CGSize maskSize = CGRectIntegral(maskPath.bounds).size;
        CGSize contextSize = CGSizeMake(maskSize.width / zoomScale, maskSize.height / zoomScale);
        UIGraphicsBeginImageContext(contextSize);
        
        // Step 6: apply the mask if needed.
        if (applyMaskToCroppedImage) {
            // 6a: scale the mask to the size of the crop rect.
            UIBezierPath *maskPathCopy = [maskPath copy];
            CGFloat scale = 1 / zoomScale;
            [maskPathCopy applyTransform:CGAffineTransformMakeScale(scale, scale)];
            
            // 6b: move the mask to the top-left.
            CGPoint translation = CGPointMake(-CGRectGetMinX(maskPathCopy.bounds),
                                              -CGRectGetMinY(maskPathCopy.bounds));
            [maskPathCopy applyTransform:CGAffineTransformMakeTranslation(translation.x, translation.y)];
            
            // 6c: apply the mask.
            [maskPathCopy addClip];
        }
        
        // Step 7: rotate the cropped image if needed.
        if (rotationAngle != 0) {
            croppedImage = [croppedImage rotateByAngle:rotationAngle];
        }
        
        // Step 8: draw the cropped image.
        CGPoint point = CGPointMake(round((contextSize.width - croppedImage.size.width) * 0.5f),
                                    round((contextSize.height - croppedImage.size.height) * 0.5f));
        [croppedImage drawAtPoint:point];
        
        // Step 9: get the cropped image affter processing from the context.
        croppedImage = UIGraphicsGetImageFromCurrentImageContext();
        
        // Step 10: remove the context.
        UIGraphicsEndImageContext();
        
        croppedImage = [UIImage imageWithCGImage:croppedImage.CGImage];
        
        // Step 11: return the cropped image affter processing.
        return croppedImage;
}

@end
