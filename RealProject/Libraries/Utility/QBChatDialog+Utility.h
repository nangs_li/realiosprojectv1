//
//  QBChatDialog+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 5/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Quickblox/Quickblox.h>

@interface QBChatDialog (Utility)
-(NSString *)getRecipientIDFromQBChatDialog;
@end
