//
//  QBChatDialog+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 5/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "QBChatDialog+Utility.h"

@implementation QBChatDialog (Utility)


-(NSString *)getRecipientIDFromQBChatDialog{
    
    NSNumber *tableViewCellQBID;
    NSNumber *occupantQBIDIndex0 = [self.occupantIDs objectAtIndex:0];
    if (isEmpty([LoginBySocialNetworkModel shared].myLoginInfo.QBID)) {
        return @" ";
    }else{
    if ([[occupantQBIDIndex0 stringValue]
         isEqualToString:[LoginBySocialNetworkModel shared].myLoginInfo.QBID]) {
        tableViewCellQBID = [self.occupantIDs objectAtIndex:1];
    } else {
        tableViewCellQBID = occupantQBIDIndex0;
    }
    }
    return [tableViewCellQBID stringValue];
    
}
@end
