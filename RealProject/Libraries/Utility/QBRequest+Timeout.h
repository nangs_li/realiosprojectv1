//
//  QBRequest+Timeout.h
//  productionreal2
//
//  Created by Li Ken on 17/5/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import <Quickblox/Quickblox.h>

@interface QBRequest (Timeout)

- (void)startTimeoutTimer;
- (void)stopTimeoutTimer;

@end
