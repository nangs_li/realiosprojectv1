//
//  QBRequest+Timeout.m
//  productionreal2
//
//  Created by Li Ken on 17/5/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import "QBRequest+Timeout.h"


static char kTimeOutTimerTag;

@implementation QBRequest (Timeout)

- (NSTimer *)timeoutTimer {
    return (NSTimer *)objc_getAssociatedObject(self, &kTimeOutTimerTag);
}

- (void)setTimeoutTimer:(NSTimer *)timeoutTimer {
    objc_setAssociatedObject(self, &kTimeOutTimerTag, timeoutTimer, OBJC_ASSOCIATION_RETAIN);
}

- (void)startTimeoutTimer{
    [self stopTimeoutTimer];
    [self setTimeoutTimer:[NSTimer scheduledTimerWithTimeInterval:QBRequestTimeOutInterval target:self selector:@selector(requestDidTimeOut) userInfo:nil repeats:NO]];
}

- (void)stopTimeoutTimer{
    if([self timeoutTimer]){
        [[self timeoutTimer]invalidate];
    }
}

- (void)requestDidTimeOut{
    [self cancel];
}

@end
