//
//  RealUtility.h
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+Utility.h"
#import "UIImage+Utility.h"
#import "UIColor+Utility.h"
#import "UILabel+Utility.h"
#import "NSObject+Utility.h"
#import "UIButton+Utility.h"
#import "UIActionSheet+Blocks.h"
#import <Photos/Photos.h>
#import "UIButton+Utility.h"
#import <AssetsLibrary/AssetsLibrary.h>

typedef enum {
    AgentProfileStatusNormal = 0,
    AgentProfileStatusPreview,
    AgentProfileStatusEdit
}AgentProfileStatus;

typedef enum {
    RealRemoteNotificationTypeOther = 0,
    RealRemoteNotificationTypeFollow = 1,
    RealRemoteNotificationTypeChat = 2
}RealRemoteNotificationType;

typedef enum {
    ActivityLogTypeFollow = 0,
    ActivityLogTypeUnfollow,
    ActivityLogTypeInviteToFollow
}ActivityLogType;


typedef enum {
    ActivityLogViewTypeFollow = 0,
    ActivityLogViewTypeFollowing
}ActivityLogViewType;

typedef enum {
    QBUUserRelationMute = 0,
    QBUUserRelationBlock
}QBUUserRelation;

typedef enum {
    ChatFeatureTypeUnknown = 0,
    ChatFeatureTypeAudioCall,
    ChatFeatureTypeVideoCall,
    ChatFeatureTypeAudioAttachment,
    ChatFeatureTypeVideoAttachment,
}ChatFeatureType;

typedef enum {
    TutorialTypeSearch = 0,
    TutorialTypeCreatePost,
    TutorialTypeInviteToFollow
}TutorialType;


typedef enum {
    ItemViewStyleWhite = 0,
    ItemViewStyleBlack
}ItemViewStyle;

typedef enum {
    AgentStatusNotAgent = 0,
    AgentStatusWithoutListing,
    AgentStatusWithListing,
}AgentStatus;

typedef void (^PHAssetBlock)(UIImage *result, NSDictionary *info);
#define kNSUserDefaultKeyUpdateVersion  @"kNSUserDefaultKeyUpdateVersion"
#define kChatVersion @"1"
#define kNotificationApplcationWillEnterBackGround @"kNotificationApplcationWillEnterBackGround"
#define kPushNotificationEnterChatRoom @"kPushNotificationEnterChatRoom"
#define kNotificationApplcationDidBecomeActive @"kNotificationApplcationDidBecomeActive"
#define kNotificationApplicationWillResignActive @"kNotificationApplicationWillResignActive"
#define kNotificationMyAgentProfileImageDidUpdate @"kNotificationMyAgentProfileImageDidUpdate"
#define kNotificationDidReceiveNewFollow @"kNotificationDidReceiveNewFollow"

#define kNotificationNewFeedNeedUpdate @"kNotificationNewFeedNeedUpdate"
#define kNotificationAddressSearchUpdate @"kNotificationAddressSearchUpdate"
#define kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView @"kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView"
#define kNotificationHideUpdateNewsFeedView @"kNotificationHideUpdateNewsFeedView"
#define kNotificationShowUpdateNewsFeedView @"kNotificationShowUpdateNewsFeedView"
#define AppDownLoadLink @"https://bnc.lt/download-real"
#define kAppDelegate (AppDelegate*)[[UIApplication sharedApplication] delegate]
#define MaxBlurRadius 100.0f
#define MaxImageBlurRadius 20.0f
#define expandOffsetY    44.0f
#define profileHeaderMinHeight 118.0f
#define profileActionHeaderMinHeight 158.0f
#define is568h  [RealUtility screenBounds].size.height >= 568.0
#define RealLightBlueColor [UIColor colorWithRed:26/255.0f green:197/255.0f blue:236/255.0f alpha:1.0f]
#define RealBlueColor [UIColor realBlueColor]
#define RealDarkBlueColor [UIColor colorWithRed:50/255.0f green:60/255.0f blue:75/255.0f alpha:1.0]
#define RealNavBlueColor [UIColor colorWithRed:66/255.0f green:77/255.0f blue:93/255.0f alpha:1.0]
#define UnfollowRedColor [UIColor colorWithRed:231/255.0f green:89/255.0f blue:93/255.0f alpha:1.0]
#define NotificationRedColor [UIColor colorWithRed:255/255.0f green:39/255.0f blue:0/255.0f alpha:1.0]
#define RealPhoneNumberBlueColor [UIColor colorWithRed:18/255.0f green:144/255.0f blue:255/255.0f alpha:1.0]
#define RealNavBarHeightWithPageControl 80.0
#define RealNavBarHeight    64.0
#define RealUploadPhotoSize   CGSizeMake(1000,1000)
#define  UIViewAutoresizingAll UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin

#define decimalMax     [[NSDecimalNumber decimalNumberWithString:@"79028162514264337593543950330"]doubleValue]
#define kTimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]
#pragma mark     ------------------------------------- Error Message global  Key

#define NotNetWorkConnectionText @"error_message__network_failure"
#define SuspendEmailSubject @"Help, my account is being suspended!"
#define ContactText @"Contact"
#define StartDatePicker @"start Date Picker"
#define EndDatePicker @"end Date Picker"

#define ThisAccountIsInUseOnAnotherDevice @"error_message__another_account_logged_into_app"

#define PleaseTryAgainLater @"error_message__please_try_again_later"
#define SorryServerIsUndermaintenance @"error_message__server_maintenace"
#define Done @"error_message__done"
#define Retry @"error_message__retry"

#define Pleaseuploadaprofilephoto @"error_message__please_upload"
#define Firstnameisrequired @"error_message__first_name"
#define Lastnameisrequired @"error_message__last_name"
#define Passwordisrequired @"error_message__passowrd_cannot"
#define PasswordlengthmustbebetweenCharacters @"error_message__password_must_contain"
#define Pleaseconfirmyourpassword @"error_message__confirm_your_password"
#define Passworddoesnotmatch @"error_message__password_does_not_match"
#define Emailisrequried @"error_message__email_cannot_be"
#define Pleaseenteravalidemailaddress @"error_message__please_enter_valid"
#define Pleaseenteranewpassword  @"error_message__new_password_cannot"
#define Newpasswordcantbethesameastheoldpassword @"error_message__old_password_and_new"
#define Emailhasbeenchangedsuccessfully @"error_message__change_email_success"
#define Pleaseenteryourpassword @"error_message__password_cannot_be_blank"
#define Passwordhasbeechangedsuccessfully @"error_message__change_password_success"
#define Resetemailhasbeesent @"error_message__reset_email_sent"
#define Tocontinuepleasecheckyouremailandfollowtheinstructions @"error_message__check_email_follow_instructions"
#define VerifyYourEmailAddress @"error_message__verift_your_email"
#define Verificationemailhasbeensent @"error_message__we_automatically_send"
#define Publishing @"error_message__publishing"
#define Areyousureyouwanttopublishthispost @"error_message__are_you_sure_want_to"
#define AgentinvitationSMShasbeensentsuccessfully @"error_message__send_invite_agents"



#define UpdateNow @"chatroom_error_message__update_now"
#define Later @"chatroom_error_message__later"
#define IncompatibleError @"chatroom_error_message__incompatible_error"
#define Therecipiensappversiondoesntsupportthisfunction @"chatroom_error_message__your_opponents"
#define XXwouldliketohaveavideocallwithyoubutthecurrentappversiondoesnotsupportthisfunction @"chatroom_error_message__want_to_have_a_video"
#define XXwouldliketohaveaaudiocall @"chatroom_error_message__want_to_have_a_audio"
#define XXwouldliketosendyouavideobutthecurrentappversiondoesnot @"chatroom_error_message___want_to_have_a_video"
#define XXwouldliketosendyouanaudiobutthecurrentappversiondoesnot  @"chatroom_error_message___want_to_have_a_audio"
#define XXwouldliketosharesomethingwithyoubutthecurrentappversiondoesnot  @"chatroom_error_message___want_to_have_a_something"



#define Followme @"invite_to_chat__follow_me"
#define MakeyourfriendssmileonReal @"invite_to_chat__make_your_friends_smile"
#define AgentinvitationSMShasbeensentsuccessfully @"invite_to_chat__send_invite_agents_sms"   //
#define kSuccess @"invite_to_chat__success"
#define DownloadRealNow @"invite_to_chat__download_real_now"
#define RealdestroysthetraditonalindustryCheckusout @"invite_to_chat__out_of_all_the_apps"
@interface RealUtility : NSObject

+(CGRect)screenBounds;
+(NSString*)getDecimalFormatByString:(NSString*)numberString withUnit:(NSString*)unit;
+(BOOL)isValid:(id)object;
+(NSString*)abbreviateNumber:(double)num;
+(NSDateComponents*)getTimeDifferenceFromDate:(NSDate*)fromDate toDate:(NSDate*)toDate;
+ (NSString*) timeDifferenceStringFromDate:(NSDate*)fromDate toDate:(NSDate*)toDate;

+(int) abbreviateToNumber:(NSString*)abbreviateString;
+(NSString*)removeDecimalFormat:(NSString*)decimalString;
+(void)getImageBy:(PHAsset*)asset size:(CGSize)size handler:(PHAssetBlock)handler;
+(void)prefetchPhotoURL:(NSArray*)photoURL;
+(BOOL)validatePhoneNumber:(NSString*)phoneNumber countryCode:(NSString*)countryCode;
+(NSString*)getMonthNameByIndex:(int)month;
@end
