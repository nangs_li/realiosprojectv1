//
//  RealUtility.m
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "RealUtility.h"
#import "SDWebImagePrefetcher.h"
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
@implementation RealUtility

+ (CGRect)screenBounds{
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        screenBounds.size = CGSizeMake(screenBounds.size.height, screenBounds.size.width);
    }
    return screenBounds;
}
+(NSString*)getDecimalFormatByString:(NSString*)numberString withUnit:(NSString*)unit{
    NSDecimalNumber *rawDecimal = [NSDecimalNumber decimalNumberWithString:numberString];
    NSString *decimalFormatString= [NSNumberFormatter localizedStringFromNumber:rawDecimal
                                                                    numberStyle:NSNumberFormatterDecimalStyle];
    if ([RealUtility isValid:unit]) {
        decimalFormatString = [decimalFormatString stringByAppendingString:unit];
    }
    return decimalFormatString;
}

+(BOOL)isValid:(id)object{
    if ([object isKindOfClass:[NSNull class]]){
        return false;
    }
    
    if ([object isKindOfClass:[NSString class]]) {
        NSString *string =object;
        [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        return string && string.length > 0;
    }else if([object isKindOfClass:[NSDictionary class]]){
        NSDictionary *dict = object;
        return dict && dict.allKeys.count>0;
    }else if([object isKindOfClass:[NSArray class]]){
        NSArray *array = object;
        return array && array.count;
    }else{
        return object != nil;
    }
}

+(NSString *)abbreviateNumber:(double)num {
    NSString *abbrevNum;
    float number = (float)num;
    
    //Prevent numbers smaller than 1000 to return NULL
    if (num >= 1000) {
        NSArray *abbrev = @[@"k", @"M", @"B", @"T" ,@"Q"];
        
        for (int i = abbrev.count - 1; i >= 0; i--) {
            
            // Convert array index to "1000", "1000000", etc
            double size = pow(10,(i+1)*3);
            
            if(size <= number) {
                // Removed the round and dec to make sure small numbers are included like: 1.1K instead of 1K
                number = number/size;
                NSString *numberString = [self floatToString:number];
                
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
            
        }
    } else {
        
        // Numbers like: 999 returns 999 instead of NULL
        abbrevNum = [NSString stringWithFormat:@"%d", (int)number];
    }
    
    return abbrevNum;
}

+(int) abbreviateToNumber:(NSString*)abbreviateString{
    if ([RealUtility isValid:abbreviateString]) {
        BOOL haveK = [abbreviateString rangeOfString:@"K"].location != NSNotFound;
        BOOL haveM = [abbreviateString rangeOfString:@"M"].location != NSNotFound;
        BOOL haveB = [abbreviateString rangeOfString:@"B"].location != NSNotFound;
        BOOL isAbbreviate = haveK || haveM || haveB;
        if (isAbbreviate) {
            int size = 1;
            abbreviateString = [[abbreviateString componentsSeparatedByCharactersInSet:
                                 [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                componentsJoinedByString:@""];
            if (haveK) {
                size = pow(10, 3);
            }else if(haveM){
                [abbreviateString stringByReplacingOccurrencesOfString:@"M" withString:@""];
                size = pow(10, 6);
            }else if(haveB){
                [abbreviateString stringByReplacingOccurrencesOfString:@"B" withString:@""];
                size = pow(10, 9);
            }
            int baseValue = [abbreviateString intValue];
            baseValue = baseValue *size;
            return baseValue;
        }else{
            return [abbreviateString intValue];
        }
    }
    return 0;
}

+(NSString*)removeDecimalFormat:(NSString*)decimalString{
    if ([RealUtility isValid:decimalString]) {
        return [[decimalString componentsSeparatedByCharactersInSet:
                 [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                componentsJoinedByString:@""];
    }else{
        return decimalString;
    }
}

+ (NSDateComponents*) getTimeDifferenceFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setFirstWeekday:1];
    //    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Hong_Kong"]];
    
    NSDateComponents *components;
    NSCalendarUnit unitFlags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitWeekOfYear;
    components = [calendar components: unitFlags
                             fromDate: fromDate toDate: toDate options: 0];
    return components;
}

+ (NSString*) timeDifferenceStringFromDate:(NSDate*)fromDate toDate:(NSDate*)toDate{
    NSString *differenceString = @"";
    NSString *agoString = @"";
    NSDateComponents *differnece = [RealUtility getTimeDifferenceFromDate:fromDate toDate:toDate];
    if (differnece.weekOfYear >0) {
        agoString = [NSString stringWithFormat:@"%d",(int)differnece.weekOfYear];
        differenceString = [NSString stringWithFormat:JMOLocalizedString(@"me__time_diff_w", nil),agoString];
    }else if (differnece.day >0){
           agoString = [NSString stringWithFormat:@"%d",(int)differnece.day];
          differenceString = [NSString stringWithFormat:JMOLocalizedString(@"me__time_diff_d", nil),agoString];
    }else if (differnece.hour >0){
        agoString = [NSString stringWithFormat:@"%d",(int)differnece.hour];
          differenceString = [NSString stringWithFormat:JMOLocalizedString(@"me__time_diff_h", nil),agoString];
    }else if (differnece.minute >0){
        agoString = [NSString stringWithFormat:@"%d",(int)differnece.minute];
          differenceString = [NSString stringWithFormat:JMOLocalizedString(@"me__time_diff_m", nil),agoString];
    }else{
        differenceString = JMOLocalizedString(@"me__now_time", nil);
    }
    return differenceString;
}

+ (NSString *) floatToString:(float) val {
    NSString *ret = [NSString stringWithFormat:@"%.1f", val];
    unichar c = [ret characterAtIndex:[ret length] - 1];
    
    while (c == 48) { // 0
        ret = [ret substringToIndex:[ret length] - 1];
        c = [ret characterAtIndex:[ret length] - 1];
        
        //After finding the "." we know that everything left is the decimal number, so get a substring excluding the "."
        if(c == 46) { // .
            ret = [ret substringToIndex:[ret length] - 1];
        }
    }
    
    return ret;
}

+(void)getImageBy:(PHAsset*)asset size:(CGSize)size handler:(PHAssetBlock)handler{
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    
    // this one is key
    requestOptions.synchronous = true;
    
    PHImageManager *manager = [PHImageManager defaultManager];
    
    [manager requestImageForAsset:asset
                       targetSize:size
                      contentMode:PHImageContentModeDefault
                          options:requestOptions
                    resultHandler:handler];
}

+(void)prefetchPhotoURL:(NSArray *)photoURL{
    SDWebImagePrefetcher *webImagePrefetcher =  [[SDWebImagePrefetcher alloc]init];
    webImagePrefetcher.options = SDWebImageHighPriority;
    webImagePrefetcher.maxConcurrentDownloads = 40;
    if (!isEmpty(photoURL)) {
    [webImagePrefetcher prefetchURLs:photoURL];
        }
}

+(BOOL)validatePhoneNumber:(NSString*)phoneNumber countryCode:(NSString*)countryCode{
    NSString *region = [[NBPhoneNumberUtil sharedInstance]getRegionCodeForCountryCode:@([countryCode intValue])];
    NSError *parseError = nil;
    NBPhoneNumber *nbPhoneNumber = [[NBPhoneNumberUtil sharedInstance]parse:phoneNumber defaultRegion:region error:&parseError];
    return [[NBPhoneNumberUtil sharedInstance]isValidNumber:nbPhoneNumber] && !parseError;
}

+(NSString*)getMonthNameByIndex:(int)index{
    NSDateFormatter *df = [NSDateFormatter localeDateFormatter];
    return [[df monthSymbols] objectAtIndex:index];
}
@end
