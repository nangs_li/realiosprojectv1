//
//  SlideAnimatedTransitioning.m
//  productionreal2
//
//  Created by Alex Hung on 30/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "SlideAnimatedTransitioning.h"

// Tries to duplicate the default iOS 7 slide transition for UINavigationController
@implementation SlideAnimatedTransitioning

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIView *containerView = [transitionContext containerView],
    *fromView = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey].view,
    *toView   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey].view;
    
    CGRect leftRect = containerView.bounds;
    leftRect.origin.x = -containerView.frame.size.width;
    CGRect rightRect = containerView.bounds;
    rightRect.origin.x = containerView.bounds.size.width;
    CGRect centerRect = containerView.bounds;
    
    if ([self navigationControllerOperation] == UINavigationControllerOperationPush){
        toView.frame = rightRect;
        toView.layer.shadowRadius = 5;
        toView.layer.shadowOpacity = 0.4;
        [containerView addSubview:toView];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            toView.frame = centerRect;
            
            fromView.frame = leftRect;
            fromView.layer.opacity = 0.9;
        } completion:^(BOOL finished) {
            fromView.layer.opacity = 1;
            toView.layer.shadowOpacity = 0;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];
    }else if ([self navigationControllerOperation] == UINavigationControllerOperationPop){
        toView.frame = leftRect;
        toView.layer.shadowRadius = 5;
        toView.layer.shadowOpacity = 0.4;
        [containerView addSubview:toView];
        [containerView bringSubviewToFront:fromView];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            toView.frame = centerRect;
            
            fromView.frame = rightRect;
            fromView.layer.opacity = 0.9;
        } completion:^(BOOL finished) {
            fromView.layer.opacity = 1;
            toView.layer.shadowOpacity = 0;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];
    }

}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.2;
}

@end
