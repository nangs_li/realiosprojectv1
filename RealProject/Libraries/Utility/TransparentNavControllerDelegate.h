//
//  TransparentNavControllerDelegate.h
//  productionreal2
//
//  Created by Alex Hung on 11/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransparentNavControllerDelegate : NSObject<UIViewControllerAnimatedTransitioning>
@property (nonatomic,assign) UINavigationControllerOperation navigationControllerOperation;
@end
