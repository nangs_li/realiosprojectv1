//
//  TransparentNavControllerDelegate.m
//  productionreal2
//
//  Created by Alex Hung on 11/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "TransparentNavControllerDelegate.h"

@implementation TransparentNavControllerDelegate
- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext     {
    // the containerView is the superview during the animation process.
    UIView *container = transitionContext.containerView;
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *fromView = fromVC.view;
    UIView *toView = toVC.view;
    CGFloat containerWidth = container.frame.size.width;
    
    // Set the needed frames to animate.
    
    CGRect toInitialFrame = [container frame];
    CGRect fromDestinationFrame = fromView.frame;
    
    if ([self navigationControllerOperation] == UINavigationControllerOperationPush)
    {
        toInitialFrame.origin.x = containerWidth;
        toView.frame = toInitialFrame;
        fromDestinationFrame.origin.x = -containerWidth;
    }
    else if ([self navigationControllerOperation] == UINavigationControllerOperationPop)
    {
        toInitialFrame.origin.x = -containerWidth;
        toView.frame = toInitialFrame;
        fromDestinationFrame.origin.x = containerWidth;
    }
    
    // Create a screenshot of the toView.
    UIView *move = [toView snapshotViewAfterScreenUpdates:YES];
    move.frame = toView.frame;
    [container addSubview:move];
    
    [UIView animateWithDuration:0.5 delay:0
         usingSpringWithDamping:1000 initialSpringVelocity:1
                        options:0 animations:^{
                            move.frame = container.frame;
                            fromView.frame = fromDestinationFrame;
                        }
                     completion:^(BOOL finished) {
                         if (![[container subviews] containsObject:toView])
                         {
                             [container addSubview:toView];
                         }
                         
                         toView.frame = container.frame;
                         [fromView removeFromSuperview];
                         [move removeFromSuperview];
                         [transitionContext completeTransition: YES];
                     }];}
@end
