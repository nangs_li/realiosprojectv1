//
//  NSString+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIButton (Utility)

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing;
- (void)setUpDefaultTitleFontSize;
- (void)setUpDefaultDetailTextFontSize;
-(void)setUpAutoScaleButton;
@end
