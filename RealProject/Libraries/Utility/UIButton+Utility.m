//
//  NSString+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "UIButton+Utility.h"

@implementation UIButton (Utility)

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing {
    CGFloat insetAmount = spacing / 2.0;
    self.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, insetAmount);
    self.titleEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
    self.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, insetAmount);
}
- (void)setUpDefaultTitleFontSize{
    
    
    [self.titleLabel setFont:  [UIFont fontWithName:@"HelveticaNeue" size:18.0]];
}


- (void)setUpDefaultDetailTextFontSize{
    
    [self.titleLabel setFont:  [UIFont fontWithName:@"HelveticaNeue" size:15.0]];
}
-(void)setUpAutoScaleButton{
self.titleLabel.numberOfLines = 1;
self.titleLabel.adjustsFontSizeToFitWidth = YES;
self.titleLabel.lineBreakMode = NSLineBreakByClipping;
}
@end
