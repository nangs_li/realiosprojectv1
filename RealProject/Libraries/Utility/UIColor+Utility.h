//
//  UIColor+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 31/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utility)

// R252, G207, B29
+ (UIColor *)alexYellow;

// R66, G77, B93
+ (UIColor *)darkBlue;

+ (UIColor *)white;

+ (UIColor *)clear;

+ (UIColor *)paleLightGrayColor;

+ (UIColor *)realBlueColor;

+ (UIColor *)realGreenColor;

// R68, G68, B68
+ (UIColor *)lightGrey;

+ (UIColor *)textFieldPlaceholderColor;

+ (UIColor *)settingsRowBlueColor;

+ (UIColor *)settingsCellGreyTextColor;

@end
