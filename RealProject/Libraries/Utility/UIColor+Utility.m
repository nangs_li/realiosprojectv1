//
//  UIColor+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 31/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "UIColor+Utility.h"

@implementation UIColor (Utility)

// R252, G207, B29
+ (UIColor *)alexYellow
{
    return [UIColor colorWithRed:252.0/255.0f green:207.0/255.0f blue:29.0/255.0f alpha:1.0f];
}

// R66, G77, B93
+ (UIColor *)darkBlue
{
    return [UIColor colorWithRed:66.0/255.0f green:77.0/255.0f blue:93.0/255.0f alpha:1.0f];
}

+ (UIColor *)white
{
    return [UIColor whiteColor];
}

+ (UIColor *)clear
{
    return [UIColor clearColor];
}

+ (UIColor *)paleLightGrayColor
{
    return [UIColor colorWithRed:140/255.0f green:140/255.0f blue:140/255.0f alpha:1.0f];
}

+ (UIColor *)realBlueColor
{
    return [UIColor colorWithRed:0/255.0f green:164/255.0f blue:233/255.0f alpha:1.0f];
}

+ (UIColor *)realGreenColor
{
     return [UIColor colorWithRed:37/255.0f green:185/255.0f blue:14/255.0f alpha:1.0f];
}

// R68, G68, B68
+ (UIColor *)lightGrey
{
    return [UIColor colorWithRed:68/255.0f green:68/255.0f blue:68/255.0f alpha:1.0f];
}

+ (UIColor *)textFieldPlaceholderColor
{
    return [UIColor colorWithRed:193/255.0 green:193/255.0 blue:193/255.0 alpha:1.0];
}

+ (UIColor *)settingsRowBlueColor
{
    return [UIColor colorWithRed:50/255.0 green:55/255.0 blue:60/255.0 alpha:1.0];
}

+ (UIColor *)settingsCellGreyTextColor
{
    return [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
}

@end
