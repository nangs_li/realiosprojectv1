//
//  UILabel+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Utility)

- (void)setAttributedTextWithMarkUp:(NSString*)markup;
- (void)setUpDefaultTitleFontSize;
- (void)setUpDefaultDetailTextFontSize;
- (void)setUpBigTitleFontSize;
- (void)setUpTitleFontSize;
- (void)setUpSubTitleFontSize;
- (void)setUpBoldTitleFontSize;
- (void)setUpBoldSubTitleFontSize;

@end
