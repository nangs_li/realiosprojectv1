//
//  UILabel+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "UILabel+Utility.h"
#import <Slash/Slash.h>


// Models
#import "UIColor+Utility.h"

@implementation UILabel (Utility)

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setAttributedTextWithMarkUp:(NSString*)markup{
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = NSTextAlignmentCenter;
    NSDictionary *style = @{
                            @"$default" : @{NSFontAttributeName  : self.font,NSForegroundColorAttributeName : self.textColor},
                            @"strong"   : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Bold" size:14],NSForegroundColorAttributeName : self.textColor},
                            @"em"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Italic" size:14],NSParagraphStyleAttributeName:paragraphStyle , NSForegroundColorAttributeName : self.textColor},
                            @"h1"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Medium" size:48],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : self.textColor},
                            @"h2"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Medium" size:36],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : self.textColor},
                            @"h3"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Medium" size:34],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : self.textColor},
                            @"h4"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Medium" size:24],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : self.textColor},
                            @"h5"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Medium" size:18],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : self.textColor},
                            @"h6"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue-Medium" size:16],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : self.textColor},
                            @"s1"       : @{NSFontAttributeName  : [UIFont fontWithName:@"HelveticaNeue" size:10],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : self.textColor},
                            @"blue":@{NSFontAttributeName  :self.font ,NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : RealLightBlueColor},
                            @"realBlueBold":@{NSFontAttributeName  :[UIFont fontWithName:@"HelveticaNeue-Medium" size:self.font.pointSize]  ,NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : RealBlueColor}
                            ,@"white":@{NSFontAttributeName  :self.font ,NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : [UIColor whiteColor]}
                            ,@"UnfollowRedColor":@{NSFontAttributeName  :[UIFont fontWithName:@"HelveticaNeue-Medium" size:self.font.pointSize] ,NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : UnfollowRedColor},
                            @"yellow":@{NSFontAttributeName  :self.font ,NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : [UIColor alexYellow]},
                            @"white":@{NSFontAttributeName  :self.font ,NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName : [UIColor whiteColor]},
                            @"paleLightGraySmall" : @{NSFontAttributeName  :  [UIFont systemFontOfSize:10],NSForegroundColorAttributeName : [UIColor paleLightGrayColor]},
                            };
    NSAttributedString *myAttributedString = [SLSMarkupParser attributedStringWithMarkup:markup style:style error:NULL];
    self.attributedText = myAttributedString;
}




- (void)setUpDefaultTitleFontSize{
    
    
    [self setFont:  [UIFont fontWithName:@"HelveticaNeue" size:18.0]];
}


- (void)setUpDefaultDetailTextFontSize{
    
    [self setFont:  [UIFont fontWithName:@"HelveticaNeue" size:15.0]];
}

- (void)setUpBigTitleFontSize{
    
    [self setFont:  [UIFont systemFontOfSize:40.0]];
}

- (void)setUpTitleFontSize{
    
    [self setFont:  [UIFont systemFontOfSize:18.0]];
}


- (void)setUpSubTitleFontSize{
    
    [self setFont:  [UIFont systemFontOfSize:14.0]];
}

- (void)setUpBoldTitleFontSize{
    
    [self setFont:  [UIFont boldSystemFontOfSize:18.0]];
}

- (void)setUpBoldSubTitleFontSize{
    
    [self setFont:  [UIFont boldSystemFontOfSize:14.0]];
}

@end
