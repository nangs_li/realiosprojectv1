//
//  UILabel+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Utility)

-(void)tableViewReloadAnimationOptionTransitionFlipFromLeft;
-(void)tableViewReloadAnimationAnimationOptionCurveEaseIn;
-(void)tableViewReloadAnimationOptionCurveEaseOut;
-(void)tableViewReloadAnimationOptionTransitionFlipFromTop;
-(void)tableViewReloadAnimationOptionTransitionCurlDown;
-(void)tableViewReloadAnimationOptionTransitionCrossDissolve;
-(void)tableViewReloadAnimation;
@end
