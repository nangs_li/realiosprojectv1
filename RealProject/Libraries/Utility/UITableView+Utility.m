//
//  UILabel+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "UITableView+Utility.h"
@implementation UITableView (Utility)
#define reloadDataDuration 4.0f
-(void)tableViewReloadAnimationOptionTransitionFlipFromLeft{
    
    [UIView transitionWithView:self
                      duration:reloadDataDuration
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^(void)
     {
         [self reloadData];
     }
                    completion:nil];
}
-(void)tableViewReloadAnimationAnimationOptionCurveEaseIn{
    
    [UIView transitionWithView:self
                      duration:reloadDataDuration
                       options:UIViewAnimationOptionCurveEaseIn
                    animations:^(void)
     {
         [self reloadData];
     }
                    completion:nil];
}

-(void)tableViewReloadAnimationOptionCurveEaseOut{
    
    [UIView transitionWithView:self
                      duration:reloadDataDuration
                       options:UIViewAnimationOptionCurveEaseOut
                    animations:^(void)
     {
         [self reloadData];
     }
                    completion:nil];
}
-(void)tableViewReloadAnimationOptionTransitionFlipFromTop{
    
    [UIView transitionWithView:self
                      duration:reloadDataDuration
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^(void)
     {
         [self reloadData];
     }
                    completion:nil];
}
-(void)tableViewReloadAnimationOptionTransitionCurlDown{
    
    [UIView transitionWithView:self
                      duration:reloadDataDuration
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^(void)
     {
         [self reloadData];
     }
                    completion:nil];
}
-(void)tableViewReloadAnimationOptionTransitionCrossDissolve{
    
    [UIView transitionWithView:self
                      duration:reloadDataDuration
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void)
     {
         [self reloadData];
     }
                    completion:nil];
}
-(void)tableViewReloadAnimation{
    [UIView transitionWithView:self
                      duration:reloadDataDuration
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^(void)
     {
         [self reloadData];
     }
                    completion:nil];

    
}
@end
