//
//  UILabel+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Utility)

- (void)setUpDefaultTitleFontSize;
- (void)setUpDefaultDetailTextFontSize;
- (void)setUpTitleFontSize;
- (void)setUpSubTitleFontSize;
- (void)setUpBoldTitleFontSize;
- (void)setUpBoldSubTitleFontSize;
- (void)setUpSubTitleFontSize:(double )size;
- (void)setUpBoldSubTitleFontSize:(double )size;
- (void)setWhiteTextColor;
- (void)setupCountryCodeTextField;
- (void)setUpPhoneNumberType;
- (void)setUpBigPhoneNumberType;
@end
