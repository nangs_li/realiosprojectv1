//
//  UILabel+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "UITextField+Utility.h"

@implementation UITextField (Utility)

- (void)setUpDefaultTitleFontSize {
    
    [self setFont:  [UIFont fontWithName:@"HelveticaNeue" size:18.0]];
    
}

- (void)setUpDefaultDetailTextFontSize {
    
    [self setFont:  [UIFont fontWithName:@"HelveticaNeue" size:15.0]];
}

- (void)setUpTitleFontSize {
    
    [self setFont:  [UIFont systemFontOfSize:18.0]];
    
}

- (void)setUpSubTitleFontSize {
    
    [self setFont:  [UIFont systemFontOfSize:14.0]];
    
}

- (void)setUpBoldTitleFontSize {
    
    [self setFont:  [UIFont boldSystemFontOfSize:18.0]];
    
}

- (void)setUpBoldSubTitleFontSize {
    
    [self setFont:  [UIFont boldSystemFontOfSize:14.0]];
    
}

- (void)setUpSubTitleFontSize:(double )size {
    
    [self setFont:  [UIFont systemFontOfSize:size]];
    
}

- (void)setUpBoldSubTitleFontSize:(double )size {
    
    [self setFont:  [UIFont boldSystemFontOfSize:size]];
    
}

- (void)setWhiteTextColor {
    
    [self setTextColor:[UIColor white]];
}

- (void)setupCountryCodeTextField {
    
    [self setTextAlignment:NSTextAlignmentRight];
    [self setTextColor:[UIColor white]];
    [self setUpTitleFontSize];
    
    self.layer.borderColor = [UIColor white].CGColor;
    self.layer.borderWidth = 1.0;
    
    UIImage *pulldownImage = [UIImage imageNamed:@"btn_pop_pulldown_small"];
    UIImageView *pulldownImageView =[[UIImageView alloc] initWithImage:pulldownImage];
    CGRect frame = pulldownImageView.frame;
    frame.size.width += 20.0;
    pulldownImageView.frame = frame;
    pulldownImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.rightViewMode = UITextFieldViewModeAlways;
    self.rightView = pulldownImageView;
    
}


- (void)setUpPhoneNumberType {
    
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setTextColor:[UIColor white]];
    [self setFont:[UIFont boldSystemFontOfSize:17]];
    
    self.layer.borderColor = [UIColor white].CGColor;
    self.layer.borderWidth = 1.0;
    self.keyboardType = UIKeyboardTypeNumberPad;
    
}

- (void)setUpBigPhoneNumberType {
    
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setTextColor:[UIColor white]];
    [self setFont:[UIFont boldSystemFontOfSize:40]];
    
    self.layer.borderColor = [UIColor white].CGColor;
    self.layer.borderWidth = 1.0;
    self.keyboardType = UIKeyboardTypeNumberPad;
    
}
@end
