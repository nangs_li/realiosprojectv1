//
//  UIView+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "UIView+Utility.h"
#import "RadialGradientLayer.h"
@implementation UIView (Utility)

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void) moveViewTo:(UIView*)targetView direction:(RelativeDirection)direction padding:(int)padding{
    CGRect relativeTargetFrame =targetView.frame;
    relativeTargetFrame.origin = [self getRelativePositionWithTargetView:targetView];
    
    CGRect newFrame = self.frame;
    switch (direction) {
        case RelativeDirectionTop:
            newFrame.origin.y = relativeTargetFrame.origin.y - newFrame.size.height - padding;
            break;
        case RelativeDirectionBottom:
            newFrame.origin.y = relativeTargetFrame.origin.y + relativeTargetFrame.size.height + padding;
            break;
        case RelativeDirectionLeft:
            newFrame.origin.x = relativeTargetFrame.origin.x - newFrame.size.width - padding;
            break;
        case RelativeDirectionRight:
            newFrame.origin.x = relativeTargetFrame.origin.x  + relativeTargetFrame.size.width + padding;
            break;
        default:
            break;
    }
    
    
    self.frame = newFrame;
}

-(void)setViewAlignmentInSuperView:(ViewAlignment)aligment padding:(int)padding{
    CGRect newFrame = self.frame;
    switch (aligment) {
        case ViewAlignmentCenter:
            newFrame.origin.x = (self.superview.frame.size.width - self.frame.size.width)/2;
            newFrame.origin.y = (self.superview.frame.size.height - self.frame.size.height)/2 +padding;
            break;
        case ViewAlignmentLeft:
            newFrame.origin.x = 0.0f +padding;
            break;
        case ViewAlignmentRight:
            newFrame.origin.x = self.superview.frame.size.width -self.frame.size.width -padding;
            break;
        case ViewAlignmentTop:
            newFrame.origin.y =0.0f + padding;
            break;
        case ViewAlignmentBottom:
            newFrame.origin.y = self.superview.frame.size.height - self.frame.size.height -padding;
            break;
        case ViewAlignmentHorizontalCenter:
            newFrame.origin.x = (self.superview.frame.size.width - self.frame.size.width)/2 +padding;
            break;
        case ViewAlignmentVerticalCenter:
            newFrame.origin.y = (self.superview.frame.size.height - self.frame.size.height)/2 + padding;
            break;
        default:
            break;
    }
    self.frame = newFrame;
}

-(void)align:(UIView *)targetView direction:(ViewAlignment)aligment padding:(int)padding{
    CGRect relativeTargetFrame =targetView.frame;
    relativeTargetFrame.origin = [self getRelativePositionWithTargetView:targetView];
    CGRect newFrame = self.frame;
    switch (aligment) {
        case ViewAlignmentTop:
            newFrame.origin.y = relativeTargetFrame.origin.y + padding;
            break;
        case ViewAlignmentBottom:
            newFrame.origin.y = relativeTargetFrame.origin.y + relativeTargetFrame.size.height -self.frame.size.height +padding;
            break;
        case ViewAlignmentLeft:
            newFrame.origin.x = relativeTargetFrame.origin.x + padding;
            break;
        case ViewAlignmentRight:
            newFrame.origin.x = relativeTargetFrame.origin.x  + relativeTargetFrame.size.width -self.frame.size.width + padding;
            break;
        case ViewAlignmentVerticalCenter:
            newFrame.origin.y = relativeTargetFrame.origin.y + relativeTargetFrame.size.height/2 - self.frame.size.height/2 + padding;
            break;
        default:
            break;
    }
    self.frame = newFrame;
}
-(CGPoint)getRelativePositionWithTargetView:(UIView*)targetView{
    if (targetView.superview == self.superview) {
        return targetView.frame.origin;
    }else{
        CGPoint relativeCGPoint = targetView.frame.origin;
        UIView *targetSuperView = targetView.superview;
        while (targetSuperView &&targetSuperView != self.superview) {
            relativeCGPoint.x +=targetSuperView.frame.origin.x;
            relativeCGPoint.y += targetSuperView.frame.origin.y;
            targetSuperView = targetSuperView.superview;
        }
        return relativeCGPoint;
    }
}
-(void)addSlidingBlinkEffect:(CGFloat)gradientWidth transparency:(CGFloat)transparency duration:(CGFloat)duration repeatCount:(CGFloat)repeat{
    [self.layer.mask removeAnimationForKey:@"animateGradient"];
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = self.bounds;
    CGFloat gradientSize = gradientWidth/self.bounds.size.width;
    UIColor *gradient = [UIColor colorWithWhite:1.0f alpha:transparency];
    NSArray *startLocations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:(gradientSize / 2)], [NSNumber numberWithFloat:gradientSize]];
    NSArray *endLocations = @[[NSNumber numberWithFloat:(1.0f - gradientSize)], [NSNumber numberWithFloat:(1.0f -(gradientSize / 2))], [NSNumber numberWithFloat:1.0f]];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"locations"];
    
    gradientMask.colors = @[(id)gradient.CGColor, (id)[UIColor whiteColor].CGColor, (id)gradient.CGColor];
    gradientMask.locations = startLocations;
    gradientMask.startPoint = CGPointMake(0 - (gradientSize * 2), .5);
    gradientMask.endPoint = CGPointMake(1 + gradientSize, .5);
    
    self.layer.mask = gradientMask;
    //    [view removeFromSuperview];
    //    view.layer.mask = gradientMask;
    //    [superview addSubview:view];
    
    animation.fromValue = startLocations;
    animation.toValue = endLocations;
    animation.repeatCount = repeat;
    animation.duration  = duration;
    
    [gradientMask addAnimation:animation forKey:@"animateGradient"];
}

-(void)addFlashingEffect:(UIColor*)flashingColor duration:(CGFloat)duration{
    [self.layer removeAnimationForKey:@"shadowOpacity"];
    self.layer.shadowColor = [flashingColor CGColor];
    self.layer.shadowRadius = 20.0f;
    self.layer.shadowOpacity = 1.0;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.masksToBounds = NO;
    
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    anim.fromValue = [NSNumber numberWithFloat:0.5];
    anim.toValue = [NSNumber numberWithFloat:1.0];
    anim.repeatCount = HUGE_VALF;
    anim.autoreverses=YES;
    anim.duration = duration;
    [self.layer addAnimation:anim forKey:@"shadowOpacity"];
    self.layer.shadowOpacity = 0.0;
}


-(void)addRadialGradient:(CGFloat)duration{
    
    RadialGradientLayer *gradientLayer =nil;
    for (RadialGradientLayer *layer in self.layer.sublayers) {
        gradientLayer = layer;
    }
    
    if (!gradientLayer) {
        gradientLayer = [RadialGradientLayer new];
    }
    gradientLayer.frame = self.bounds;
    gradientLayer.masksToBounds = NO;
    self.layer.masksToBounds = NO;
    self.clipsToBounds = NO;
    self.layer.mask = gradientLayer;
    
    [gradientLayer removeAnimationForKey:@"RadialGradient"];
    CABasicAnimation *locationAnim = [CABasicAnimation animationWithKeyPath:@"locations"];
    locationAnim.fromValue = @[@(0.0f), @(0.5f)];
    locationAnim.toValue = @[@(0.0f), @(1.0f)];
    
    CABasicAnimation *scaleAnim=[CABasicAnimation animationWithKeyPath:@"scales"];
    scaleAnim.fromValue=@[@(1.0),@(0.8f)];
    scaleAnim.toValue=@[@(1.5f),@(1.0f)];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.repeatCount = HUGE_VALF;
    group.duration = duration;
    group.autoreverses = YES;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    group.animations = @[locationAnim, scaleAnim];
    
    [gradientLayer addAnimation:group forKey:@"RadialGradient"];
}
@end
