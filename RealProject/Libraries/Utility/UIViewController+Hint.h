//
//  UIViewController+Hint.h
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Hint)

@property (nonatomic, strong) UILabel *hintLabel;
@property (nonatomic, strong) NSLayoutConstraint *hintViewTopSpaceConstraint;
@property (nonatomic, strong) UIVisualEffectView *blurEffectView;
@property (nonatomic, assign) BOOL isShowingHint;
@property (nonatomic, strong) NSTimer *dismissTimer;

- (void)showHint:(NSString *)hint offset:(CGFloat)offset;
- (void)showHint:(NSString *)hint;
- (void)hideHint;

@end
