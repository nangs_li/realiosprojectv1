//
//  UIViewController+Hint.m
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "UIViewController+Hint.h"

static char TAG_HINT_LABEL;
static char TAG_HINT_VIEW_TOP_SPACE_CONSTRAINT;
static char TAG_BLUR_EFFECT_VIEW;
static char TAG_IS_SHOWING_HINT;
static char TAG_DISMISS_TIMER;

@implementation UIViewController (Hint)

- (UILabel *)hintLabel {
    return (UILabel *)objc_getAssociatedObject(self, &TAG_HINT_LABEL);
}

- (void)setHintLabel:(UILabel *)hintLabel {
    objc_setAssociatedObject(self, &TAG_HINT_LABEL, hintLabel, OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)isShowingHint {
    id object = objc_getAssociatedObject(self, &TAG_IS_SHOWING_HINT);
    
    return [object boolValue];
}

- (void)setIsShowingHint:(BOOL)isShowingHint {
    objc_setAssociatedObject(self, &TAG_IS_SHOWING_HINT, @(isShowingHint), OBJC_ASSOCIATION_RETAIN);
}

- (NSLayoutConstraint *)hintViewTopSpaceConstraint {
    return (NSLayoutConstraint *)objc_getAssociatedObject(self, &TAG_HINT_VIEW_TOP_SPACE_CONSTRAINT);
}

- (void)setHintViewTopSpaceConstraint:(NSLayoutConstraint *)hintViewTopSpaceConstraint {
    objc_setAssociatedObject(self, &TAG_HINT_VIEW_TOP_SPACE_CONSTRAINT, hintViewTopSpaceConstraint, OBJC_ASSOCIATION_RETAIN);
}

- (UIVisualEffectView *)blurEffectView {
    return (UIVisualEffectView *)objc_getAssociatedObject(self, &TAG_BLUR_EFFECT_VIEW);
}

- (void)setBlurEffectView:(UIVisualEffectView *)blurEffectView {
    objc_setAssociatedObject(self, &TAG_BLUR_EFFECT_VIEW, blurEffectView, OBJC_ASSOCIATION_RETAIN);
}

- (NSTimer *)dismissTimer {
    return objc_getAssociatedObject(self, &TAG_DISMISS_TIMER);
}

- (void)setDismissTimer:(NSTimer *)dismissTimer {
    objc_setAssociatedObject(self, &TAG_DISMISS_TIMER, dismissTimer, OBJC_ASSOCIATION_RETAIN);
}

- (void)showHint:(NSString *)hint offset:(CGFloat)offset {
    if (!self.isShowingHint) {
        self.isShowingHint = YES;
        
        if (!self.blurEffectView) {
            UIVisualEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
            self.blurEffectView = [[UIVisualEffectView alloc] initWithEffect:effect];
            [self.view addSubview:self.blurEffectView];
            self.hintViewTopSpaceConstraint = [self.blurEffectView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:-44];
            [self.blurEffectView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
            [self.blurEffectView autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
            [self.blurEffectView autoSetDimension:ALDimensionHeight toSize:44];
        }
        
        if (!self.hintLabel) {
            self.hintLabel = [UILabel new];
            self.hintLabel.backgroundColor = [UIColor clearColor];
            self.hintLabel.textAlignment = NSTextAlignmentCenter;
            self.hintLabel.text = @"";
            self.hintLabel.adjustsFontSizeToFitWidth = YES;
            self.hintLabel.numberOfLines = 0;
            [self.blurEffectView addSubview:self.hintLabel];
            [self.hintLabel autoPinEdgesToSuperviewEdges];
            [self.view layoutIfNeeded];
        }
        
        self.hintLabel.text = hint;
        
        if (self.hintViewTopSpaceConstraint.constant != offset) {
            [UIView animateWithDuration:0.3 animations:^{
                self.hintViewTopSpaceConstraint.constant = offset
                ;
                [self.view layoutIfNeeded];
            }completion:^(BOOL finished) {
                if (finished) {
                    [self startDismissTimer];
                }
            }];
        }
        
    } else {
        
        if (self.dismissTimer) {
            [self startDismissTimer];
        }
    }
}

- (void)showHint:(NSString *)hint {
    [self showHint:hint offset:64];
}

- (void)startDismissTimer {
    
    if (self.dismissTimer) {
        [self.dismissTimer invalidate];
    }
    
    self.dismissTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideHint) userInfo:nil repeats:NO];
}


- (void)hideHint {
    
    if (self.dismissTimer) {
        [self.dismissTimer invalidate];
    }
    self.dismissTimer = nil;
    
    if (self.isShowingHint) {
        self.isShowingHint = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.hintViewTopSpaceConstraint.constant = -44;
            [self.view layoutIfNeeded];
        }];
    }
}

@end
