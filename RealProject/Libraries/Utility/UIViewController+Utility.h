//
//  UIViewController+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 2/11/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController (Utility)

-(void)autoCorrectFrameIfNeeded;
@end
