//
//  UIViewController+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 2/11/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "UIViewController+Utility.h"

@implementation UIViewController (Utility)
-(void)autoCorrectFrameIfNeeded{
    CGRect currentFrame = self.view.frame;
    if (currentFrame.size.height+currentFrame.origin.y > [RealUtility screenBounds].size.height) {
        currentFrame.size.height = [RealUtility screenBounds].size.height - currentFrame.origin.y;
    }
    self.view.frame = currentFrame;
}
@end
