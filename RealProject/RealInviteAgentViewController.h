//
//  RealLoginViewController.h
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface RealInviteAgentViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UILabel *inviteAgentTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *inviteAgentDescLabel;
@property (nonatomic, strong) IBOutlet UIButton *inviteAgentBottomBtn;
@property (nonatomic, strong) NSMutableArray *logsArray;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *moveAgentBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@end
