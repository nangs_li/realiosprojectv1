//
//  RealInviteMoreAgentViewController.h
//  productionreal2
//
//  Created by Li Ken on 31/5/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealInviteMoreAgentViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *logsArray;
@property (nonatomic, strong) IBOutlet UIButton *followAllBtn;
@property (nonatomic, strong) IBOutlet UILabel *inviteMoreAgentLabel;
@property (nonatomic, strong) IBOutlet UIButton *backBtn;

@end
