//
//  RealInviteMoreAgentViewController.m
//  productionreal2
//
//  Created by Li Ken on 31/5/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import "RealInviteMoreAgentViewController.h"
#import "AgentProfile.h"
#import "FollowAgentModel.h"
#import "UnFollowAgentModel.h"
#import "UIScrollView+EmptyDataSet.h"
#import "ActivityLogFollowingCell.h"
#import "FollowerModel.h"

@interface RealInviteMoreAgentViewController ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,ActivityLogFollowingCellDelegate>


@end

@implementation RealInviteMoreAgentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage {
    // [[AppDelegate getAppDelegate]setUpBlueAndWhiteTitleFontSizeUILabel:self.inviteMoreAgentLabel];
    NSString *loginTitle =JMOLocalizedString(@"login__loginlogin__loginlogin__loginlogin__loginlogin__loginlogin", nil);
    self.inviteMoreAgentLabel.text = @"login__loginlogin__loginlogin__loginlogin__loginlogin__loginlogin";
    
    NSString *loginString =JMOLocalizedString(@"login__signinfdskfisdfijdsji", nil);
    [self.followAllBtn setTitle:loginString forState:UIControlStateNormal];
    [self.followAllBtn setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    
}

- (IBAction)followAllBtnPress:(id)sender {
    
    [[FollowAgentModel shared] callAllFollowAgentApiByMemberIDArray:nil success:^(id response) {
        
        for (FollowerModel *model in self.logsArray) {
            model.IsFollowing =YES;
        }
        
        [self.tableView reloadData];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
         object:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
        
        [[AppDelegate getAppDelegate] handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
        
    }];
    
}

- (IBAction)backBtn:(id)sender {
    [self btnBackPressed:sender];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.logsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ActivityLogFollowingCell *followingCell = [tableView dequeueReusableCellWithIdentifier:@"ActivityLogFollowingCell"];
    followingCell.delegate = self;
    FollowerModel *model  = [self followerModelAtIndexPath:indexPath];
    [followingCell configureWithFollowerModel:model];
    followingCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return followingCell;
}

- (FollowerModel*)followerModelAtIndexPath:(NSIndexPath*)indexPath{
    FollowerModel *model = nil;
    
    if (self.logsArray.count >indexPath.row) {
        
        model = self.logsArray[indexPath.row];
        
    }
    
    return model;
}

#pragma mark - ActivityLogFollowingCellDelegate
- (void)followingCell:(ActivityLogFollowingCell *)cell followerButtonDidPress:(FollowerModel *)model{
    NSString *memberIDString = [@(model.MemberID) stringValue];
    NSString *listingId = [@(model.ListingID) stringValue];
    BOOL isFollowing = [model didFollow];
    
    if (isFollowing) {
        UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil
                                                        delegate:nil
                                               cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                             
                                          destructiveButtonTitle:JMOLocalizedString(@"agent_profile__unfollow_button", nil)
                                               otherButtonTitles:nil, nil];
        
        as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        
        as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            
            if (buttonIndex == 0) {
                
                [[UnFollowAgentModel shared] callUnFollowAgentApiByAgentProfileMemberID:memberIDString success:^(id response) {
                    
                    [cell didFollow:!isFollowing];
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
                     object:nil];
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                    
                    [[AppDelegate getAppDelegate] handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                    
                }];
            }
            
        };
        
        [as showInView:self.view];
        
    }else{
        
        [[FollowAgentModel shared] callFollowAgentApiByAgentProfileMemberID:memberIDString AgentProfileAgentListingAgentListingID:listingId success:^(id response) {
            
            [cell didFollow:!isFollowing];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
             object:nil];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            
            [[AppDelegate getAppDelegate] handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
            
        }];
    }
    
}


@end
