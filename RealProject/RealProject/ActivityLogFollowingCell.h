//
//  ActivityLogFollowingCell.h
//  productionreal2
//
//  Created by Alex Hung on 1/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealFadingTableViewCell.h"
// Views
#import "RealBouncyButton.h"

@class FollowerModel,ActivityLogFollowingCell,REJoinedAgentContact;
@protocol ActivityLogFollowingCellDelegate <NSObject>

- (void)followingCell:(ActivityLogFollowingCell *)cell followerButtonDidPress:(id)model;

@end

@interface ActivityLogFollowingCell : RealFadingTableViewCell
@property (nonatomic,strong) id<ActivityLogFollowingCellDelegate> delegate;
@property (nonatomic,strong) IBOutlet UIImageView *profileImageView;
@property (nonatomic,strong) IBOutlet UILabel *memberNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *locationLabel;
@property (nonatomic,strong) IBOutlet RealBouncyButton *followButton;
@property (nonatomic,strong) FollowerModel *followerModel;
@property (nonatomic, strong) REJoinedAgentContact *agentContact;
@property (nonatomic, assign) BOOL isFollowing;
@property (nonatomic, strong) IBOutlet UIView *dotImageView;

- (void)configureWithAgentContact:(REJoinedAgentContact *)agentContact;
- (void)configureWithFollowerModel:(FollowerModel*)followerModel;
- (void)didFollow:(BOOL)follow;
- (IBAction)followerButtonDidPress:(id)sender;
@end
