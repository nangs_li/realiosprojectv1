//
//  ActivityLogFollowingCell.m
//  productionreal2
//
//  Created by Alex Hung on 1/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ActivityLogFollowingCell.h"
#import "FollowerModel.h"
#import "REJoinedAgentContact.h"
#import <SDWebImage/UIImageView+WebCache.h>
@implementation ActivityLogFollowingCell

- (void)awakeFromNib {
    // Initialization code
    self.profileImageView.layer.cornerRadius = CGRectGetHeight(self.profileImageView.frame)/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)configureWithFollowerModel:(FollowerModel*)followerModel{
    NSString *memberName = followerModel.MemberName;
    NSString *location = followerModel.Location;
    NSString *profileURL = followerModel.AgentPhotoURL;
    BOOL isFollowing = [followerModel didFollow];
    
    self.followerModel = followerModel;
    self.memberNameLabel.text = memberName;
    self.locationLabel.text = location;
    [self.profileImageView loadImageURL:[NSURL URLWithString:profileURL] withIndicator:YES];
    
    [self didFollow:isFollowing];
}

- (void)configureWithAgentContact:(REJoinedAgentContact *)agentContact {
    NSString *name = agentContact.Name;
    NSString *location = agentContact.Location;
    NSURL *imageURL = [NSURL URLWithString:agentContact.PhotoURL];
    BOOL isFollowing = [agentContact didFollow];
    self.agentContact = agentContact;
    
    
    self.memberNameLabel.textColor = [UIColor white];
    self.locationLabel.textColor = [UIColor lightTextColor];
    
    self.memberNameLabel.text = name;
    self.locationLabel.text = location;
    [self.profileImageView loadImageURL:imageURL withIndicator:YES];
    [self.profileImageView loadImageURL:imageURL withIndicator:YES];
    [self didFollow:isFollowing];
}

- (void)didFollow:(BOOL)follow{
    self.isFollowing = follow;
    if (follow) {
        [self.followButton setTitle:JMOLocalizedString(@"common__following", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.followButton setBackgroundColor:[UIColor realBlueColor]];
        [self.followButton setImage:[UIImage imageNamed:@"btn_ico_follow_unfollow"]
                           forState:UIControlStateNormal];
        self.profileImageView.borderType = UIImageViewBorderTypeFollowingSmall;
    }else{
        [self.followButton setTitle:JMOLocalizedString(@"common__follow", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor realBlueColor]
                                forState:UIControlStateNormal];
        [self.followButton setBackgroundColor:[UIColor clearColor]];
        [self.followButton setImage:[UIImage imageNamed:@"btn_ico_follow_follow"]
                           forState:UIControlStateNormal];
        [self.followButton
         setBackgroundImage:
         [UIImage imageWithBorder:[UIColor realBlueColor]
                             size:self.followButton.frame.size]
         forState:UIControlStateNormal];
        self.profileImageView.borderType = UIImageViewBorderTypeNone;
    }
}
- (IBAction)followerButtonDidPress:(id)sender{
    if ([self.delegate respondsToSelector:@selector(followingCell:followerButtonDidPress:)]) {
        if ([self.followerModel valid]) {
            [self.delegate followingCell:self followerButtonDidPress:self.followerModel];
        }
        
        if ([self.agentContact valid]) {
            [self.delegate followingCell:self followerButtonDidPress:self.agentContact];
        }
    }
    
}

@end
