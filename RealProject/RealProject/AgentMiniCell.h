//
//  AgentMiniCell.h
//  productionreal2
//
//  Created by Alex Hung on 25/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgentMiniCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UIImageView *agentImageView;
@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UILabel *followerLabel;
@property (nonatomic,strong) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UIImageView *apartmentImageView;
@property (strong, nonatomic) IBOutlet UIButton *agentImageBtn;
-(void) configureCell:(AgentProfile*)profile;
@property (nonatomic,strong) AgentProfile *agentProfile;
@end
