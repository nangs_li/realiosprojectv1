//
//  AgentMiniCell.m
//  productionreal2
//
//  Created by Alex Hung on 25/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "AgentMiniCell.h"
#import "RealUtility.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation AgentMiniCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(AgentProfile *)profile{
    if (profile) {
        self.agentProfile=profile;
        AgentListing * listing= profile.AgentListing;
        NSString *name = profile.MemberName;
        GoogleAddress *googleAddress = [profile.AgentListing.GoogleAddress firstObject];
//        AddressUserInput *address = [profile.AgentListing.AddressUserInput firstObject];
        NSString *location = [profile agentAddress];
        NSString *follwerString =   [NSString stringWithFormat:@" %@",JMOLocalizedString(@"common__followers", nil)];
        NSString *follwerCount = [RealUtility getDecimalFormatByString:[NSString stringWithFormat:@"%d",profile.FollowerCount] withUnit:follwerString];
        self.nameLabel.text = name;
        self.locationLabel.text = location;
        self.followerLabel.text = follwerCount;
        self.agentImageView.layer.cornerRadius = self.agentImageView.frame.size.width / 2;
        [self.agentImageView loadImageURL:[NSURL URLWithString:profile.AgentPhotoURL] withIndicator:YES];
        
           if ([profile getIsFollowing]) {
               self.agentImageView.borderType = UIImageViewBorderTypeFollowing;
           }else{
               self.agentImageView.borderType = UIImageViewBorderTypeNone;
            
            
        }
         NSString *apartmentImage = [[SystemSettingModel shared]getMiniViewPropertyImageByPropertyType:listing.PropertyType spaceType:listing.SpaceType];
        
        self.apartmentImageView.image = [UIImage imageNamed:apartmentImage];
    }
}
@end
