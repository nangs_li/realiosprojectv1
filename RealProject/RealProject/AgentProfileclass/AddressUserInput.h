//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "JSONModel.h"
@protocol AddressUserInput
@end

@interface AddressUserInput : JSONModel
@property(assign, nonatomic) int LanguageIndex;
@property(strong, nonatomic) NSString* Street;
@property(strong, nonatomic) NSString* AptSuiteBldg;
@property(strong, nonatomic) NSString* Unit;
@end

//@implementation Photos

//@end