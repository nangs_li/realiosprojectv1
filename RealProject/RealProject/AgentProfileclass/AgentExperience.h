//
//  AgentExperience.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentExperience_h
//#define abc_AgentExperience_h

//#endif
#import "JSONModel.h"
@protocol AgentExperience
@end

@interface AgentExperience : JSONModel
@property(assign, nonatomic) int ID;
@property(strong, nonatomic) NSString* Company;
@property(strong, nonatomic) NSString* Title;
@property(strong, nonatomic) NSString* IsCurrentJob;
@property(strong, nonatomic) NSString* StartDate;
@property(strong, nonatomic) NSDate *dateOfStart;
@property(strong, nonatomic) NSDate *dateOfEnd;
@property(strong, nonatomic) NSString* EndDate;
@property(strong, nonatomic) NSString* StartDateString;
@property(strong, nonatomic) NSString* EndDateString;
-(NSString*)getPeriodString;
+(AgentExperience*)convertDictToAgentExperience:(NSDictionary*)dict;
@end

//@implementation AgentExperience

//@end