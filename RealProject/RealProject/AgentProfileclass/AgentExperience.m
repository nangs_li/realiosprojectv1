//
//  AgentExperience.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgentExperience.h"
@implementation AgentExperience
+ (BOOL)propertyIsOptional:(NSString*)propertyName {
  return YES;
}


-(NSString*)getPeriodString{
    NSDateFormatter *dateFormat = [NSDateFormatter localeDateFormatter];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [dateFormat dateFromString:self.StartDateString];
    NSDate *endDate = [dateFormat dateFromString:self.EndDateString];
    if ([self.IsCurrentJob boolValue]) {
        endDate = [NSDate date];
    }
    if ([[LanguagesManager sharedInstance].currentLanguage isEqualToString:@"zh-Hant"]||[[LanguagesManager sharedInstance].currentLanguage isEqualToString:@"zh-Hans"]) {
         [dateFormat setDateFormat:@"yyyy年MMM"];
    }else{
         [dateFormat setDateFormat:@"MMM yyyy"];
    }
   
    NSString *startDateString = [dateFormat stringFromDate:startDate];
    NSString *endDateString = [dateFormat stringFromDate:endDate];
    if ([self.IsCurrentJob boolValue]) {
        endDateString = JMOLocalizedString(@"agent_profile__to_present", nil);
    }
    NSString *compareDateString =    [NSString stringWithFormat:@"1 %@",JMOLocalizedString(@"agent_profile__mo", nil)];

    NSDateComponents *components;
    NSCalendarUnit unitFlags = NSCalendarUnitMonth |NSCalendarUnitYear;
    components = [[NSCalendar currentCalendar] components: unitFlags
                                                 fromDate: startDate toDate: endDate options: 0];
    int year = (int)[components year];
    int month = (int)[components month];
    NSString * yearString;
    if (year>1) {
        yearString=JMOLocalizedString(@"agent_profile__yrs", nil);
    }else{
        yearString=JMOLocalizedString(@"agent_profile__yr", nil);
    }
    NSString * monthString;
    if (month>1) {
        monthString=JMOLocalizedString(@"agent_profile__mos", nil);
    }else{
        monthString=JMOLocalizedString(@"agent_profile__mo", nil);

    }
    if (month >0 && year >0){
    compareDateString = [NSString stringWithFormat:@"%i%@ %i%@",year,yearString,month,monthString];
    }else if (year >0){
        if (year>1) {
            compareDateString = [NSString stringWithFormat:@"%i%@",year,yearString];
        }else{
        compareDateString = [NSString stringWithFormat:@"%i%@",year,yearString];
        }
    }else if (month >0){
        compareDateString = [NSString stringWithFormat:@"%i%@",month,monthString];
    }
    NSString *periodString = [NSString stringWithFormat:@"%@ - %@ (%@)",startDateString,endDateString,compareDateString];
    return periodString;
}

+(AgentExperience*)convertDictToAgentExperience:(NSDictionary *)dict{
    AgentExperience *exp = [[AgentExperience alloc]initWithDictionary:dict error:nil];
    return exp;
}
@end