//
//  AgentLanguage.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentLanguage_h
//#define abc_AgentLanguage_h

//#endif
#import "JSONModel.h"
@protocol AgentLanguage
@end

@interface AgentLanguage : JSONModel
@property(assign, nonatomic) int LangIndex;
@property(strong, nonatomic) NSString* Language;

@end

//@implementation AgentLanguage

//@end