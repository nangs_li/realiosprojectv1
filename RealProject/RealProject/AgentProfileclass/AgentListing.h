//
//  AgentListing.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentListing_h
//#define abc_AgentListing_h

//#endif

#import "JSONModel.h"
#import "Photos.h"
#import "Reasons.h"
#import "AddressUserInput.h"
#import "GoogleAddress.h"
@protocol AgentListing
@end

@interface AgentListing : JSONModel
@property (assign, nonatomic)BOOL isPreview;
@property (strong,nonatomic) NSArray *previewPhoto;
@property(assign, nonatomic) int BathroomCount;
@property(assign, nonatomic) int BedroomCount;
@property(strong, nonatomic) NSArray<Photos>* Photos;
@property(assign, nonatomic) int PropertyPrice;
@property(assign, nonatomic) int PropertySize;
@property(strong, nonatomic) NSMutableArray<Reasons>* Reasons;
@property(assign, nonatomic) int SloganIndex;
@property(assign, nonatomic) int PropertyType;
@property(assign, nonatomic) int SpaceType;
@property(strong, nonatomic) NSString* CurrencyUnit;
@property(assign, nonatomic) int AgentListingID;
@property(assign, nonatomic) int ListingFollowerCount;
@property(assign, nonatomic) int SizeUnitType;
@property(strong, nonatomic) NSMutableArray<GoogleAddress>* GoogleAddress;
@property(strong, nonatomic) NSString* PropertyPriceFormattedForRoman;
@property(strong, nonatomic) NSString* PropertyPriceFormattedForChinese;
@property(strong, nonatomic) NSString* PropertyPriceFormattedForIndian;
@property(strong,nonatomic) NSString* CreationDateString;
@property(strong, nonatomic) NSMutableArray<AddressUserInput>* AddressUserInput;
@property(assign, nonatomic) NSNumber *selectedLangIndex;

-(NSString*)getSlogan;
-(NSArray*)getReason;
-(NSDate*) getCreationDate;
@end

//@implementation AgentListing

//@end
