//
//  AgentListing.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgentListing.h"
@implementation AgentListing
+ (BOOL)propertyIsOptional:(NSString*)propertyName {
    return YES;
}

- (NSString*)getSlogan{
    Reasons *reason = [self.Reasons firstObject];
    return [[SystemSettingModel shared]getSloganStringByLangIndex:@(reason.LanguageIndex) position:@(reason.SloganIndex)];
}

-(NSArray*)getReason{
    NSMutableArray *reasonArray =[[NSMutableArray alloc]init];
    Reasons *firstReason = nil;
    if (!self.selectedLangIndex) {
        firstReason = [self.Reasons firstObject];
    }else{
        for (Reasons *reason in self.Reasons) {
            if (reason.LanguageIndex == [self.selectedLangIndex intValue]) {
                firstReason = reason;
                break;
            }
        }
    }
    NSString *reason1 = firstReason.Reason1;
    NSString *reason2 = firstReason.Reason2;
    NSString *reason3 = firstReason.Reason3;
    NSString *mediaURL1 = firstReason.MediaURL1;
    NSString *mediaURL2 = firstReason.MediaURL2;
    NSString *mediaURL3 = firstReason.MediaURL3;
    PHAsset *asset1 = firstReason.asset1;
    PHAsset *asset2 = firstReason.asset2;
    PHAsset *asset3 = firstReason.asset3;
    
    
    if (!mediaURL1) {
        mediaURL1 =@"";
    }
    
    if (!mediaURL2) {
        mediaURL2 =@"";
    }
    
    if (!mediaURL3) {
        mediaURL3 =@"";
    }
    
    if (!asset1) {
        asset1 =nil;
    }
    
    if (!asset2) {
        asset2 =nil;
    }
    
    if (!asset3) {
        asset3 =nil;
    }
    
    if (!reason1) {
        reason1 =@"";
    }
    if (!reason2) {
        reason2=@"";
    }
    
    if (!reason3) {
        reason3 =@"";
    }
    
    for (int i = 0; i<3; i++) {
        NSString *reason =@"";
        NSString *mediaURL =@"";
        PHAsset *asset = nil;
        if (i == 0) {
            reason = reason1;
            mediaURL =mediaURL1;
            asset = asset1;
        }else if (i == 1){
            reason = reason2;
            mediaURL =mediaURL2;
            asset = asset2;
        }else if (i == 2){
            reason = reason3;
            mediaURL =mediaURL3;
            asset = asset3;
        }
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setObject:reason forKey:@"reason"];
        [dict setObject:mediaURL forKey:@"mediaURL"];
        if (asset) {
        [dict setObject:asset forKey:@"asset"];
        }
        [reasonArray addObject:dict];
    }
    
    return reasonArray;
}

-(NSDate*)getCreationDate{
    NSDate *createDate = nil;
    if (self.CreationDateString) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        createDate = [dateFormat dateFromString:self.CreationDateString];
    }
    return createDate;
}
@end