//
//  AgentPastClosing.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentPastClosing_h
//#define abc_AgentPastClosing_h

//#endif
#import "JSONModel.h"

@protocol AgentPastClosing
@end

@interface AgentPastClosing : JSONModel
@property(assign, nonatomic) int ID;
@property(assign, nonatomic) int PropertyType;
@property(assign, nonatomic) int SpaceType;
@property(strong, nonatomic) NSString* Country;
@property(strong, nonatomic) NSString* State;
@property(strong, nonatomic) NSString* Address;
@property(strong, nonatomic) NSString* SoldPrice;
@property(strong, nonatomic) NSString* SoldDate;
@property(strong, nonatomic) NSString* SoldDateString;
@property(assign, nonatomic) int Position;

-(NSString*) getDateString;
@end

//@implementation AgentPastClosing

//@end