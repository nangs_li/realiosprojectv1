//
//  AgentPastClosing.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgentPastClosing.h"
@implementation AgentPastClosing
+ (BOOL)propertyIsOptional:(NSString*)propertyName {
  return YES;
}
-(NSString*) getDateString{
    NSDateFormatter *dateFormat = [NSDateFormatter localeDateFormatter];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *soldDate = [dateFormat dateFromString:self.SoldDateString];
    if ([[LanguagesManager sharedInstance].currentLanguage isEqualToString:@"zh-Hant"]||[[LanguagesManager sharedInstance].currentLanguage isEqualToString:@"zh-Hans"]) {
        [dateFormat setDateFormat:@"yyyy年MMM"];
    }else{
        [dateFormat setDateFormat:@"MMM yyyy"];
    }

    NSString *soldDateString = [dateFormat stringFromDate:soldDate];
    return [NSString stringWithFormat:@"%@: %@",JMOLocalizedString(@"agent_profile__closing_date", nil),soldDateString];;
}
@end