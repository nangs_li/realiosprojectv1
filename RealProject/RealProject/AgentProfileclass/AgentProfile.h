//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import "JSONModel.h"
#import "AgentListing.h"
#import "AgentLanguage.h"
#import "AgentExperience.h"
#import "AgentSpecialty.h"
#import "AgentPastClosing.h"
@protocol AgentProfile
@end
static NSString* newFeedDomainIdentifier = @"com.real.newsfeed";
static NSString* chatDomainIdentifier = @"com.real.chat";

@interface AgentProfile : JSONModel
@property(assign, nonatomic) int FollowingCount;
@property(assign, nonatomic) int FollowerCount;
@property(assign, nonatomic) int MemberID;
@property(strong, nonatomic) NSString* MemberName;
@property(strong, nonatomic) NSString* AgentLicenseNumber;
@property(strong, nonatomic) NSString* AgentPhotoURL;
@property(strong, nonatomic) NSString* QBID;
@property(assign, nonatomic) int MemberType;
@property(strong, nonatomic) AgentListing<Optional>* AgentListing;
@property(strong, nonatomic) NSMutableArray<AgentListing>* AgentListingHistory;
@property(strong, nonatomic) NSMutableArray<AgentLanguage>* AgentLanguage;
@property(strong, nonatomic) NSMutableArray<AgentExperience>* AgentExperience;
@property(strong, nonatomic) NSMutableArray<AgentSpecialty>* AgentSpecialty;
@property(strong, nonatomic) NSMutableArray<AgentPastClosing>* AgentPastClosing;
@property(assign, nonatomic) int IsFollowing;
@property(assign, nonatomic) AgentProfileStatus status;
@property(strong, nonatomic) NSNumber *meProfile;
@property(assign, nonatomic) BOOL profileImageDidLoad;
@property(assign, nonatomic) BOOL coverPhotoDidLoad;
@property(assign, nonatomic) BOOL readyToShow;
@property(assign, nonatomic) BOOL didShow;
- (AgentProfile*)initwithjsonstring:(NSString*)content:(int)error;
- (AgentProfile*)initwithNSDictionary:(NSDictionary*)content:(int)error;

-(CGFloat)getProfileCompletePercentage;
-(NSString*)getSlogan;
-(NSURL*)coverPhotoURL;

-(BOOL)isPreview;
-(BOOL)hasPhoto;
-(BOOL) isEqualToProfile:(AgentProfile*)profile;
-(BOOL) isRecenlyUpdate:(AgentProfile*)profile;
-(BOOL) haveAgentListing;
-(void) createSpotLightSearchNewFeedItem;
-(void) deleteSpotLightSearchNewFeedItem;

- (void)createSpotLightSearchChatItem;
- (void)deleteSpotLightSearchChatItem;
- (BOOL)isCSTeam;
- (BOOL)isSuperCS;
- (NSString*)agentAddress;
- (NSString*) languageTagString;
- (NSString*) specialtyTagString;

- (BOOL)getIsFollowing;
+ (NSNumber *)getIsFollowingMemberID:(NSInteger)agentMemberID;

@end
