//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AgentProfile.h"
#import "RealUtility.h"
#import "SystemSettingModel.h"
#import <CoreSpotlight/CoreSpotlight.h>
#import <QuartzCore/QuartzCore.h>
#import "DBFollowAgentRecord.h"
@implementation AgentProfile


typedef void(^searchItemBlock)(CSSearchableItemAttributeSet *attributeSet);
static AgentProfile *agentProfile;
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}


- (AgentProfile *)initwithjsonstring:(NSString *)content:(int)error {
    NSError *err = nil;
    agentProfile = [[AgentProfile alloc] initWithString:content error:&err];
    return agentProfile;
}
- (AgentProfile *)initwithNSDictionary:(NSDictionary *)content:(int)error {
    NSError *err = nil;
    agentProfile = [[AgentProfile alloc] initWithDictionary:content error:&err];
    return agentProfile;
}

-(NSString*)getSlogan{
    return [self.AgentListing getSlogan];
}

- (NSURL*)coverPhotoURL{
    NSURL *coverPhotoURL = nil;
    if ([RealUtility isValid:self.AgentListing] &&[RealUtility isValid:self.AgentListing.Photos]) {
        Photos *bgPhoto = [self.AgentListing.Photos firstObject];
        if (bgPhoto.URL) {
            coverPhotoURL = [NSURL URLWithString:bgPhoto.URL];
        }
    }
    return coverPhotoURL;
}
-(BOOL)isPreview{
    return self.status == AgentProfileStatusPreview;
}
-(BOOL)hasPhoto{
    return [RealUtility isValid:self.AgentListing] && [RealUtility isValid:self.AgentListing.Photos] && self.AgentListing.Photos.count >1;
}
-(CGFloat)getProfileCompletePercentage{
    BOOL hasLicense = [RealUtility isValid:self.AgentLicenseNumber];
    BOOL hasExperience = [RealUtility isValid:self.AgentExperience];
    BOOL hasLang = [RealUtility isValid:self.AgentLanguage];
    BOOL hasSpecail = [RealUtility isValid:self.AgentSpecialty];
    BOOL hasPastClosing = [RealUtility isValid:self.AgentPastClosing];
    
    return  (hasLicense + hasExperience + hasLang + hasSpecail + hasPastClosing)/5.0f;
}

- (BOOL)isEqual:(id)other {
    if (other == self){
        return YES;
    }if (!other || ![other isKindOfClass:[self class]]){
        return NO;
    }else{
        return [self isEqualToProfile:other];
    }
}

-(BOOL)isEqualToProfile:(AgentProfile *)profile{
    BOOL isSameAgent = self.MemberID == profile.MemberID;
    BOOL isSameListing = self.AgentListing.AgentListingID == profile.AgentListing.AgentListingID;
    
    return isSameAgent && isSameListing;
}
-(BOOL) isRecenlyUpdate:(AgentProfile*)profile{
    BOOL recenlyUpdate = YES;
    NSDate *selfCreationDate = [self.AgentListing getCreationDate];
    NSDate *otherCreationDate = [profile.AgentListing getCreationDate];
    recenlyUpdate = [selfCreationDate compare:otherCreationDate] == NSOrderedDescending;
    return recenlyUpdate ;
}


- (void)searchAttributeSet:(searchItemBlock)block{
    CSSearchableItemAttributeSet *attributeSet = [[CSSearchableItemAttributeSet alloc]initWithItemContentType:(NSString *)kUTTypeImage];
    AgentListing *listing = self.AgentListing;
    NSString *priceString = [NSString stringWithFormat:@"%@ %@",listing.CurrencyUnit,listing.PropertyPriceFormattedForRoman] ;
    NSString *sizeString = [NSString stringWithFormat:@"%d %@",listing.PropertySize,[[SystemSettingModel shared]getMetricNativeNameBySizeUnitType:listing.SizeUnitType]];
    NSString *bedCount = [NSString stringWithFormat:@"%d",listing.BedroomCount];
    NSString *bathroomCount = [NSString stringWithFormat:@"%d",listing.BathroomCount];
    NSString *apartmentType = [[SystemSettingModel shared]getSpaceTypeByPropertyType:listing.PropertyType spaceType:listing.SpaceType];
    GoogleAddress *googleAddress = [listing.GoogleAddress firstObject];
    NSString *address = googleAddress.FormattedAddress;
    NSString *name = googleAddress.Name;
    NSString *location = googleAddress.Location;
    // attributeSet.title =[NSString stringWithFormat:@"%@\n <Followers> %d",self.MemberName,self.FollowerCount];
    // attributeSet.contentDescription =[NSString stringWithFormat:@"<Price>:%@ <Size>:%@ \n<Bedroom>:%@ <Bathroom>:%@",priceString,sizeString,bedCount,bathroomCount];
    attributeSet.title =[NSString stringWithFormat:@"%@\n",self.MemberName];
    if (self.FollowerCount ==0|| self.FollowerCount ==1) {
        attributeSet.contentDescription =[NSString stringWithFormat:@"%d Follower",self.FollowerCount];
    }else{
        attributeSet.contentDescription =[NSString stringWithFormat:@"%d Followers",self.FollowerCount];
    }
    attributeSet.keywords = @[priceString, sizeString,apartmentType,location,name,address,self.MemberName];
    
    UIImageView * uiImageView =[[UIImageView alloc]init];
    [uiImageView setFrame:CGRectMake(0,0,50,50)];
    uiImageView.contentMode=UIViewContentModeScaleAspectFit;
    [uiImageView sd_setImageWithURL:[NSURL URLWithString:self.AgentPhotoURL]
                   placeholderImage:[UIImage imageNamed:@"noiamge.gif"]
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              UIImage * uiImage=  [self getRoundedRectImageFromImage:image onReferenceView:uiImageView withCornerRadius:1];
                              attributeSet.thumbnailData = UIImagePNGRepresentation(uiImage);
                              block(attributeSet);
                          }];
    //    [[SDImageCache sharedImageCache] queryDiskCacheForKey:self.AgentPhotoURL done:^(UIImage *image, SDImageCacheType cacheType){
    //        // image is not nil if image was found
    //        attributeSet.thumbnailData = UIImagePNGRepresentation(image);
    //        block(attributeSet);
    //    }];
    
}

-(NSString*) searchableItemIdentifer{
    return self.QBID;
}


- (void)createSpotLightSearchNewFeedItem{
    [self searchAttributeSet:^(CSSearchableItemAttributeSet *attributeSet) {
        
        CSSearchableItem *item = [[CSSearchableItem alloc]initWithUniqueIdentifier:self.searchableItemIdentifer domainIdentifier:newFeedDomainIdentifier attributeSet:attributeSet];
        if (item ) {
            
            
            [[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler: ^(NSError *  error) {
                if (!error)
                DDLogDebug(@"Search item indexed");
            }];
        }
    }];
    
}
- (void)deleteSpotLightSearchNewFeedItem{
    [[CSSearchableIndex defaultSearchableIndex] deleteSearchableItemsWithIdentifiers:@[self.searchableItemIdentifer] completionHandler:^(NSError *  error) {
        
        
        
        
        
    }];
}
- (void)createSpotLightSearchChatItem{
    
    
    [self searchAttributeSet:^(CSSearchableItemAttributeSet *attributeSet) {
        CSSearchableItem *item = [[CSSearchableItem alloc]initWithUniqueIdentifier:self.searchableItemIdentifer domainIdentifier:chatDomainIdentifier attributeSet:attributeSet];
        if (item ) {
            [[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler: ^(NSError *  error) {
                if (!error)
                DDLogDebug(@"Search item indexed");
            }];
        }
    }];
}
- (void)deleteSpotLightSearchChatItem{
    [[CSSearchableIndex defaultSearchableIndex] deleteSearchableItemsWithIdentifiers:@[self.searchableItemIdentifer] completionHandler:^(NSError *  error) {
        
        
        
        
        
        
    }];
}
- (UIImage *)getRoundedRectImageFromImage :(UIImage *)image onReferenceView :(UIImageView*)imageView withCornerRadius :(float)cornerRadius
{
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, 1.0);
    [[UIBezierPath bezierPathWithRoundedRect:imageView.bounds
                                cornerRadius:cornerRadius] addClip];
    [image drawInRect:imageView.bounds];
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return finalImage;
}


- (BOOL)getIsFollowing {
    if (![[LoginBySocialNetworkModel shared].myLoginInfo isLoggedIn])
    return NO;
    NSNumber *isFollowingInDB = [AgentProfile getIsFollowingMemberID:self.MemberID];
    
    if (isFollowingInDB) {
        
        return isFollowingInDB.boolValue;
        
    } else {
        
        return self.IsFollowing;
    }
}

+ (NSNumber *)getIsFollowingMemberID:(NSInteger)agentMemberID {
    if (!agentMemberID)
    return @(NO);
    NSNumber *isFollowingInDB = nil;
    
    DBResultSet * result =  [[[DBFollowAgentRecord query]
                              whereWithFormat:@"MemberID = %d AND FollowingMemberID = %d",[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue], (int)agentMemberID] fetch] ;
    
    for (DBFollowAgentRecord * dbFollowAgentRecord in result) {
        
        isFollowingInDB = @(dbFollowAgentRecord.IsFollowing);
    }
    
    return isFollowingInDB;
}

- (BOOL)isCSTeam{
    return self.MemberType == 4 || self.MemberType == 5;
}

- (BOOL)isSuperCS{
    return self.MemberType == 5;
}

-(BOOL) haveAgentListing{
    return self.AgentListing.AgentListingID !=0;
    
}

- (NSString*)agentAddress{
    GoogleAddress *googleAddress = [self.AgentListing.GoogleAddress firstObject];
    //        AddressUserInput *address = [profile.AgentListing.AddressUserInput firstObject];
    NSString *address = googleAddress.FormattedAddress;
    if (![address valid]) {
        address = @"";
    }
    return address;
}

- (BOOL)readyToShow{
    NSURL *profileImageURL = nil;
    if (self.AgentPhotoURL) {
        profileImageURL = [NSURL URLWithString:self.AgentPhotoURL];
    }
    NSURL *coverImageURL = [self coverPhotoURL];
    BOOL profileImageCacheExist = [UIImage imageExistForURL:profileImageURL];
    BOOL coverImageCacheExist = [UIImage imageExistForURL:coverImageURL];
    if (!profileImageURL) {
        profileImageCacheExist = YES;
    }
    if (![self coverPhotoURL]) {
        coverImageCacheExist = YES;
    }
    return (self.profileImageDidLoad && self.coverPhotoDidLoad) || (profileImageCacheExist && coverImageCacheExist);
}
#pragma mark check empty
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

- (NSString*) languageTagString{
    NSString *tagString = @"";
    NSMutableArray *tags = [[NSMutableArray alloc]init];
    for (AgentLanguage *lang in self.AgentLanguage) {
        if ([lang.Language valid]) {
            [tags addObject:lang.Language];
        }
    }
    tagString = [tags componentsJoinedByString:@"・"];
    return tagString;
}
- (NSString*) specialtyTagString{
    NSString *tagString = @"";
    NSMutableArray *tags = [[NSMutableArray alloc]init];
    for (AgentSpecialty *specialty in self.AgentSpecialty) {
        if ([specialty.specialty valid]) {
            [tags addObject:specialty.specialty];
        }
    }
    tagString = [tags componentsJoinedByString:@"・"];
    return tagString;
}
@end