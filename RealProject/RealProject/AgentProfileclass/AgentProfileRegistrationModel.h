//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
@interface AgentProfileRegistrationModel : BaseModel
@property(nonatomic, strong) NSMutableArray *ExperienceArray;
@property(nonatomic, strong) NSMutableArray *LanguagesArray;
@property(nonatomic, strong) NSMutableArray *Top5Array;
@property(nonatomic, strong) NSMutableArray *SpecialtyArray;
+ (AgentProfileRegistrationModel *)shared ;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 
 */
-(void)callAgentProfileRegistrationAPIAgentLicenseNo:(NSString *)agentLicenseNo  Success:(void (^)(AgentProfile *myAgentProfile))success
                      failure:(failureBlock) failure;

- (void)sortExperienceArray;
- (void)sortTop5Array;

- (BOOL)inputValid;
@end
