//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AgentProfileRegistrationModel.h"

#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "DBMyAgentProfile.h"
#import "AgentPastClosing.h"

@implementation AgentProfileRegistrationModel

static AgentProfileRegistrationModel *sharedAgentProfileRegistrationModel;




+ (AgentProfileRegistrationModel *)shared {
    @synchronized(self) {
        if (!sharedAgentProfileRegistrationModel) {
            sharedAgentProfileRegistrationModel = [[AgentProfileRegistrationModel alloc] init];
            sharedAgentProfileRegistrationModel.ExperienceArray = [[NSMutableArray alloc]init];
            sharedAgentProfileRegistrationModel.LanguagesArray = [[NSMutableArray alloc]init];
            sharedAgentProfileRegistrationModel.Top5Array = [[NSMutableArray alloc]init];
            sharedAgentProfileRegistrationModel.SpecialtyArray = [[NSMutableArray alloc]init];
            
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedAgentProfileRegistrationModel;
    }
}


-(void)callAgentProfileRegistrationAPIAgentLicenseNo:(NSString *)agentLicenseNo  Success:(void (^)(AgentProfile *myAgentProfile))success
                                             failure:(failureBlock ) failure{
   
    
    
    
       NSString *urlLink = [NSString
                        stringWithFormat:@"%@%@", kServerAddress,
                        @"/Admin.asmx/AgentProfileRegistration"];
    
  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
       failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      return;
  }

    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", [self
                                      agentProfileSaveJsonInputParameter:
                                      agentLicenseNo]);
   
    
  [manager POST:urlLink
     parameters:[self
                 agentProfileSaveJsonInputParameter:
                 agentLicenseNo]
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
          [[MyAgentProfileModel shared]
           callAgentProfileGetAPISuccess:^(AgentProfile *myAgentProfile) {
              
               success([MyAgentProfileModel shared].myAgentProfile);
               
           }
           failure:^(AFHTTPRequestOperation *operation, NSError *error,
                     NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                     NSString *ok) {
                failure(operation, error,errorMessage,errorStatus,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
               
           }];
          
          
              
      
         
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
       failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      }];
}


//// MePageAgentProfile----------------------------------------------------------------------------------------------------------
- (NSDictionary *)agentProfileSaveJsonInputParameter:
(NSString *)AgentLicenseNumber {
    NSString *AgentLicenseNumber2 = @" ";
    if (![self isEmpty:AgentLicenseNumber]) {
        AgentLicenseNumber2 = AgentLicenseNumber;
    }
    
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"AgentLicenseNumber\":\"%@\",\"AgentLanguage\":%@,"
                            @"\"AgentExperience\":%@,\"AgentPastClosing\":%@,"
                            @"\"AgentSpecialty\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            AgentLicenseNumber2,
                            [self nsDictionaryArrayToInputParametersString:self.LanguagesArray],
                            [self nsDictionaryArrayToInputParametersString:self.ExperienceArray],
                            [self nsDictionaryArrayToInputParametersString:self.Top5Array],
                            [self nsDictionaryArrayToInputParametersString:self.SpecialtyArray],
                            nil];
    
    NSDictionary *InputJson = @{ @"InputJson" : parameters };
    return InputJson;
}

//// nsDictionaryArrayToInputParametersString
- (NSString *)nsDictionaryArrayToInputParametersString:(NSMutableArray *)array {
    NSMutableArray *dictArray = [[NSMutableArray alloc]init];
    for (int i =0; i<array.count; i++) {
        JSONModel *model = array[i];
        [dictArray addObject:[model toDictionary]];
    }
    
    NSString *ArrayjsonString;
    NSError *error;
    NSData *jsonData =
    [NSJSONSerialization dataWithJSONObject:dictArray
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];
    
    if (!jsonData) {
        DDLogInfo(@"Got an error: %@", error);
    } else {
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        ArrayjsonString =
        [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    
    return ArrayjsonString;
}

- (void)sortExperienceArray{
    NSSortDescriptor *currentJobDescriptor = [[NSSortDescriptor alloc] initWithKey:@"IsCurrentJob" ascending:NO];
    NSSortDescriptor *endDateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateOfEnd" ascending:NO];
    NSSortDescriptor *startDateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateOfStart" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:currentJobDescriptor,endDateDescriptor, startDateDescriptor, nil];
    [self.ExperienceArray sortUsingDescriptors:sortDescriptors];
}

- (void)sortTop5Array{
    
}

- (BOOL)inputValid{
    return [self.ExperienceArray valid] || [self.LanguagesArray valid] || [self.Top5Array valid] || [self.SpecialtyArray valid];
}

@end