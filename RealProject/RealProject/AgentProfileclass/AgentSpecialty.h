//
//  AgentSpecialty.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentSpecialty_h
//#define abc_AgentSpecialty_h

//#endif
#import "JSONModel.h"
@protocol AgentSpecialty
@end

@interface AgentSpecialty : JSONModel
@property(strong, nonatomic) NSString* specialty;
@end

//@implementation AgentSpecialty

//@end