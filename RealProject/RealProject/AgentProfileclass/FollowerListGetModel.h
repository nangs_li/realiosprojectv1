//
//  FollowerListGetModel.h
//  productionreal2
//
//  Created by Alex Hung on 2/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"
#import "FollowerModel.h"

@interface FollowerListGetModel : JSONModel
@property (nonatomic,assign) NSInteger TotalCount;
@property (nonatomic,assign) NSInteger RecordCount;
@property (nonatomic,assign) NSInteger Page;
@property (nonatomic,strong) NSMutableArray<FollowerModel>* Followers;
@end
