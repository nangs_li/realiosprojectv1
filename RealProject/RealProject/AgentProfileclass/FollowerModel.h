//
//  FollowerModel.h
//  productionreal2
//
//  Created by Alex Hung on 2/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"
@protocol FollowerModel
@end
@class DBFollowerLog;
@interface FollowerModel : JSONModel
@property (nonatomic,assign) NSInteger MemberType;
@property (nonatomic,assign) NSInteger MemberID;
@property (nonatomic,assign) NSInteger ListingID;
@property (nonatomic,assign) BOOL IsFollowing;
@property (nonatomic,strong) NSString* MemberName;
@property (nonatomic,strong) NSString* AgentPhotoURL;
@property (nonatomic,strong) NSString* Location;

@property (nonatomic,strong) NSDate* FollowDate;

- (BOOL)didFollow;
+ (FollowerModel*)initWithDBFollowerLog:(DBFollowerLog*)log;
+ (NSArray*) initWithDBFollowerLogs:(NSArray*)dbLogs;
@end
