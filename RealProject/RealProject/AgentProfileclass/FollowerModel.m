//
//  FollowerModel.m
//  productionreal2
//
//  Created by Alex Hung on 2/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "FollowerModel.h"
#import "DBFollowerLog.h"

@implementation FollowerModel
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}
+ (FollowerModel*)initWithDBFollowerLog:(DBFollowerLog*)log{
    FollowerModel *model = [[FollowerModel alloc]init];
    model.MemberID = log.MemberID;
    model.ListingID = log.ListingID;
    model.MemberType = log.MemberType;
    model.MemberName = log.MemberName;
    model.AgentPhotoURL = log.AgentPhotoURL;
    model.IsFollowing = log.isFollowing;
    model.Location = log.Location;
    model.FollowDate = log.FollowDate;
    return model;
}

+ (NSArray*) initWithDBFollowerLogs:(NSArray*)dbLogs{
    NSMutableArray *followerLogs = [[NSMutableArray alloc]init];
    for (DBFollowerLog *log in dbLogs) {
        [followerLogs addObject:[[FollowerModel initWithDBFollowerLog:log] copy]];
    }
    return followerLogs;
}

- (BOOL)didFollow{
    NSNumber *isFollowingInDB = [AgentProfile getIsFollowingMemberID:self.MemberID];
    return isFollowingInDB ? [isFollowingInDB boolValue] : self.IsFollowing;
}
@end
