//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
#import "BaseModel.h"
@interface MyAgentProfileModel : BaseModel
@property(strong, nonatomic) AgentProfile *myAgentProfile;
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(strong, nonatomic) NSMutableArray *HaveProfileLanguagesArray;
+ (MyAgentProfileModel *)shared ;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 
 */
-(void)callAgentProfileGetAPISuccess:(void (^)(AgentProfile *myAgentProfile))success
                      failure:(failureBlock) failure;


//// delete

-(void)callAgentProfileExperienceDeleteAPIByIdString:(NSString *)idString  success:(void (^)(AgentProfile *myAgentProfile))success
                             failure:(failureBlock ) failure;




-(void)callAgentProfilePastClosingDeleteAPIByIdString:(NSString *)idString  success:(void (^)(AgentProfile *myAgentProfile))success
                                              failure:(failureBlock) failure;




-(void)callAgentProfileLanguageDeleteAPIByTitle:(NSString *)title  success:(void (^)(AgentProfile *myAgentProfile))success
                                          failure:(failureBlock ) failure;




-(void)callAgentProfileSpecialtyDeleteAPIByTitle:(NSString *)title  success:(void (^)(AgentProfile *myAgentProfile))success
                                           failure:(failureBlock ) failure;



//// add




-(void)callAgentProfileExperienceAddAPIByCompany:(NSString *)company title:(NSString *)title isCurrentJob:(NSString *)isCurrentJob startDate:(NSString *)startDate endDate:(NSString *)endDate success:(void (^)(AgentProfile *myAgentProfile))success
                                         failure:(failureBlock ) failure;

-(void)callAgentProfileLanguageAddAPIByLocalLangIndexarray:(NSMutableArray *)localLangIndexarray success:(void (^)(AgentProfile *myAgentProfile))success
                                                   failure:(failureBlock ) failure;

-(void)callAgentProfilePastClosingAddAPIByPropertyTypeNumber:(NSString *)propertyTypeNumber spaceTypeNumber:(NSString *)spaceTypeNumber soldPrice:(NSString *)soldPrice soldDate:(NSString *)soldDate address:(NSString *)address success:(void (^)(AgentProfile *myAgentProfile))success
                                                     failure:(failureBlock) failure;

-(void)callAgentProfileSpecialtyAddAPIBySpecialtyFieldArray:(NSMutableArray *)specialtyFieldArray success:(void (^)(AgentProfile *myAgentProfile))success
                                                    failure:(failureBlock) failure;




-(void)callAgentProfilePastClosingPositionUpdateByAgentPastClosingArray:(NSMutableArray<AgentPastClosing> *)AgentPastClosingArray success:(void (^)(AgentProfile *myAgentProfile))success
failure:(failureBlock) failure;



-(void)callAgentProfileAgentLicenseUpdateByAgentLicenseNumber:(NSString *)agentLicenseNumber success:(void (^)(AgentProfile *myAgentProfile))success
                                                    failure:(failureBlock ) failure;
-(void)uploadProfileImage:(UIImage*)profileImage withBlock:(CommonBlock)block;

-(void)saveMyAgentProfile:(AgentProfile*)agentProfile;
@end
