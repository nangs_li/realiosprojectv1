//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "MyAgentProfileModel.h"

#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "DBMyAgentProfile.h"
#import "AgentPastClosing.h"
#import "XMLReader.h"

@implementation MyAgentProfileModel

static MyAgentProfileModel *sharedMyAgentProfileModel;

+ (MyAgentProfileModel *)shared {
    @synchronized(self) {
        if (!sharedMyAgentProfileModel) {
            sharedMyAgentProfileModel = [[MyAgentProfileModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedMyAgentProfileModel;
    }
}
#pragma mark AgentProfileGet
- (void)
callAgentProfileGetAPISuccess:(void (^)(AgentProfile *myAgentProfile))success
failure:(failureBlock)failure {
    int closedListingCount = 9;
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"ClosedListingCount\":\"%d\"}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            closedListingCount,
                            nil];
    
    NSString *urlLink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress, @"/Admin.asmx/AgentProfileGet"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    [manager POST:urlLink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0 || error == 31) {
                  if (![self isEmpty:[self.HandleServerReturnString.Content
                                      objectForKey:@"AgentProfile"]]) {
                      self.myAgentProfile = [[AgentProfile alloc]
                                             initwithNSDictionary:[self.HandleServerReturnString.Content
                                                                   objectForKey:@"AgentProfile"
                                                                   ]:error];
                      [[[[DBMyAgentProfile query]whereWithFormat:@" MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID] fetch] removeAll];
                      DBMyAgentProfile *dbmyagentprofile = [DBMyAgentProfile new];
                      dbmyagentprofile.Date = [NSDate date];
                      dbmyagentprofile.MyAgentProfile = [NSKeyedArchiver
                                                         archivedDataWithRootObject:[self.myAgentProfile copy]];
                      dbmyagentprofile.MemberID = [[LoginBySocialNetworkModel shared]
                                                   .myLoginInfo.MemberID intValue];
                      [dbmyagentprofile commit];
                      success(self.myAgentProfile);
                  }
                  
                  // Not an agent
              }else{
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}
#pragma mark - Delete AgentProfile------------------------------------------------------------------------------------------------------------
- (void)callAgentProfileExperienceDeleteAPIByIdString:
(NSString *)
idString success:(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfileExperienceDelete"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"AgentExperience\":[{\"ID\":%@}]}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            idString, nil];
    
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

- (void)callAgentProfilePastClosingDeleteAPIByIdString:
(NSString *)
idString success:(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfilePastClosingDelete"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"AgentPastClosing\":[{\"ID\":%@}]}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            idString, nil];
    
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

- (void)callAgentProfileLanguageDeleteAPIByTitle:
(NSString *)
title success:
(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfileLanguageDelete"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"AgentLanguage\":[{\"Language\":\"%@\"}]}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            title, nil];
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

- (void)callAgentProfileSpecialtyDeleteAPIByTitle:
(NSString *)
title success:(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfileSpecialtyDelete"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"AgentSpecialty\":[{\"Specialty\":\"%@\"}]}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            title, nil];
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

#pragma mark Add AgentProfile------------------------------------------------------------------------------------------------------------

- (void)
callAgentProfileExperienceAddAPIByCompany:(NSString *)company
title:(NSString *)title
isCurrentJob:(NSString *)isCurrentJob
startDate:(NSString *)startDate
endDate:(NSString *)endDate
success:
(void (^)(AgentProfile *myAgentProfile))
success
failure:(failureBlock)failure {
    NSDictionary *dict = @{
                           @"Company" : company,
                           @"Title" : title,
                           @"IsCurrentJob" : isCurrentJob,
                           @"StartDate" : startDate,
                           @"EndDate" : endDate
                           };
    
    NSString *ExperienceArrayjsonString =
    [self nsDictionaryArrayToInputParametersString:dict.mutableCopy];
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfileExperienceAdd"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"AgentExperience\":[%@]}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            ExperienceArrayjsonString, nil];
    
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

- (void)
callAgentProfileLanguageAddAPIByLocalLangIndexarray:
(NSMutableArray *)
localLangIndexarray success:
(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfileLanguageAdd"];
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"DeleteAllFirst\":1,"
                            @"\"AgentLanguage\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            [self nsDictionaryArrayToInputParametersString:
                             [self haveProfileLanguagesArrayInitAndAddObject:
                              localLangIndexarray]],
                            nil];
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

- (void)
callAgentProfilePastClosingAddAPIByPropertyTypeNumber:
(NSString *)propertyTypeNumber spaceTypeNumber:(NSString *)spaceTypeNumber
soldPrice:(NSString *)soldPrice
soldDate:(NSString *)soldDate
address:(NSString *)address
success:
(void (^)(AgentProfile *
          myAgentProfile))
success
failure:
(failureBlock)failure {
    NSDictionary *PastClosing = [[NSDictionary alloc] init];
    NSMutableArray *Top5Array = [[NSMutableArray alloc] init];
    PastClosing = @{
                    @"PropertyType" : propertyTypeNumber,
                    @"SpaceType" : spaceTypeNumber,
                    @"SoldPrice" : soldPrice,
                    @"SoldDate" : soldDate,
                    @"Address" : address,
                    @"State" : @"",
                    @"Country" : @""
                    };
    [Top5Array addObject:PastClosing];
    
    NSString *Top5jsonString =
    [self nsDictionaryArrayToInputParametersString:Top5Array];
    
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfilePastClosingAdd"];
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"AgentPastClosing\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            Top5jsonString, nil];
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

- (void)callAgentProfileSpecialtyAddAPIBySpecialtyFieldArray:
(NSMutableArray *)
specialtyFieldArray success:
(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSMutableArray *SpecialtyArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *specialty in specialtyFieldArray) {
        NSString *string = [[specialty allValues] objectAtIndex:0];
        NSDictionary *dict = @{ @"specialty" : string };
        [SpecialtyArray addObject:dict];
    }
    
    NSString *SpecialtyjsonString =
    [self nsDictionaryArrayToInputParametersString:SpecialtyArray];
    
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfileSpecialtyAdd"];
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"AgentSpecialty\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            SpecialtyjsonString, nil];
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}

- (void)
callAgentProfileAgentLicenseUpdateByAgentLicenseNumber:
(NSMutableArray *)
agentLicenseNumber success:(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"AgentLicenseNumber\":\"%@\"}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            agentLicenseNumber, nil];
    
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentProfileAgentLicenseUpdate"];
    [self callServerAndAgentProfileGetByUrlLink:urlLink
                                     parameters:parameters
                                        success:^(AgentProfile *myAgentProfile) {
                                            success(self.myAgentProfile);
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                  NSString *errorMessage, RequestErrorStatus errorStatus,
                                                  NSString *alert, NSString *ok) {
                                            failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                                                    JMOLocalizedString(@"alert_ok", nil));
                                        }];
}
#pragma mark Support Method
- (NSMutableArray *)haveProfileLanguagesArrayInitAndAddObject:
(NSMutableArray *)localLangIndexarray {
    self.HaveProfileLanguagesArray = [[NSMutableArray alloc] init];
    for (NSString *LangIndex in localLangIndexarray) {
        for (SpokenLanguageList *spokenlanguage in [SystemSettingModel shared]
             .spokenlanguagelist) {
            if ([LangIndex
                 isEqualToString:[NSString stringWithFormat:@"%d", spokenlanguage
                                  .Index]]) {
                     NSDictionary *dict = @{
                                            @"Language" : spokenlanguage.NativeName,
                                            @"LangIndex" : [NSString stringWithFormat:@"%d", spokenlanguage.Index]
                                            };
                     [self.HaveProfileLanguagesArray addObject:dict];
                 }
        }
    }
    
    return self.HaveProfileLanguagesArray;
}

- (void)
callAgentProfilePastClosingPositionUpdateByAgentPastClosingArray:
(NSMutableArray<AgentPastClosing> *)
AgentPastClosingArray success:(void (^)(AgentProfile *myAgentProfile))
success failure:
(failureBlock)failure {
    NSMutableArray *Top5Position = [[NSMutableArray alloc] init];
    
    DDLogInfo(@"finishReorderingWithObject");
    // do any additional cleanup here
    int i = 0;
    for (AgentPastClosing *AgentPastClosing in [MyAgentProfileModel shared]
         .myAgentProfile.AgentPastClosing) {
        [Top5Position addObject:@{
                                  @"ID" : [NSString stringWithFormat:@"%d", AgentPastClosing.ID],
                                  @"Position" : [NSString stringWithFormat:@"%d", i]
                                  }];
        i++;
    }
    
    DDLogInfo(@"Top5Position-->%@", Top5Position);
    
    NSString *urlLink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/AgentProfilePastClosingPositionUpdate"];
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"AgentPastClosing\":%@"
                            @"}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            [self nsDictionaryArrayToInputParametersString:Top5Position], nil];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    [manager POST:urlLink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  [self callAgentProfileGetAPISuccess:^(AgentProfile *myAgentProfile) {
                      success(self.myAgentProfile);
                  } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                              NSString *errorMessage, RequestErrorStatus errorStatus,
                              NSString *alert, NSString *ok) {
                      failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                              RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
                  }];
              }else{
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                      RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
          }];
}

- (void)
callServerAndAgentProfileGetByUrlLink:(NSString *)urlLink
parameters:(NSString *)parameters
success:(void (^)(AgentProfile *myAgentProfile))
success
failure:
(failureBlock)failure {
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    [manager POST:urlLink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  [self callAgentProfileGetAPISuccess:^(AgentProfile *myAgentProfile) {
                      success(self.myAgentProfile);
                  } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                              NSString *errorMessage, RequestErrorStatus errorStatus,
                              NSString *alert, NSString *ok) {
                      failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                              RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
                      
                  }];
              }else{
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                      RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
          }];
}

//// nsDictionaryArrayToInputParametersString
- (NSString *)nsDictionaryArrayToInputParametersString:(NSMutableArray *)array {
    NSString *ArrayjsonString;
    NSError *error;
    NSData *jsonData =
    [NSJSONSerialization dataWithJSONObject:array
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];
    
    if (!jsonData) {
        DDLogInfo(@"Got an error: %@", error);
    } else {
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        ArrayjsonString =
        [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    
    return ArrayjsonString;
}


-(void) uploadProfileImage:(UIImage *)profileImage withBlock:(CommonBlock)block{
    NSDictionary *parameters = @{
                                 @"MemberID" : [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                                 @"AccessToken" : [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                                 @"ListingID" : @"0",
                                 @"Position" : @"0",
                                 @"FileType" : @"2",
                                 @"FileExt" : @"jpg"
                                 };
    
    
    NSString *string = [NSString stringWithFormat:@"%@%@", kServerAddress,
                        @"/Admin.asmx/FileUpload"];
    
    NSData *imageData = [UIImage compressImageToNSData:profileImage limitedDataSize:80];
    DDLogVerbose(@"imageData.length-->%lu",
                 (unsigned long)imageData.length);
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
                                    multipartFormRequestWithMethod:@"POST"
                                    URLString:string
                                    parameters:parameters
                                    constructingBodyWithBlock:^(id<AFMultipartFormData>
                                                                formData) {
                                        [formData appendPartWithFileData:imageData
                                                                    name:@"FileExt"
                                                                fileName:@"photo.jpg"
                                                                mimeType:@"image/jpeg"];
                                    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc]
                                    initWithSessionConfiguration:
                                    [NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSProgress *progress = nil;
    //    [kAppDelegate updateCurrentHudStatus:@"Uploading Photo"];
    NSURLSessionUploadTask *uploadTask = [manager
                                          uploadTaskWithStreamedRequest:request
                                          progress:&progress
                                          completionHandler:^(NSURLResponse *response,
                                                              id responseObject,
                                                              NSError *error) {
                                              NSDictionary *jsonResponse = nil;
                                              if (!error && responseObject) {
                                                  NSString *res = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                  NSError *parseError = nil;
                                                  NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:res error:&parseError];
                                                  if (!parseError && xmlDictionary) {
                                                      NSDictionary *rawDict = xmlDictionary[@"string"];
                                                      if(rawDict){
                                                          NSString *content = rawDict[@"text"];
                                                          NSString *decoded = [content stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                          NSData *data = [decoded dataUsingEncoding:NSUTF8StringEncoding];
                                                          jsonResponse= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                          
                                                          int errorCode = [jsonResponse[@"ErrorCode"]intValue ];
                                                          if (errorCode != 0) {
                                                              error =[[NSError alloc]init];
                                                          }
                                                          jsonResponse = jsonResponse[@"Content"];
                                                      }
                                                  }
                                              }
                                              if (block) {
                                                  block(jsonResponse,error);
                                              }
                                              
                                          }];
    
    [uploadTask resume];
    
}

- (void)saveMyAgentProfile:(AgentProfile *)agentProfile{
    self.myAgentProfile = agentProfile;
    [[[[DBMyAgentProfile query]whereWithFormat:@" MemberID = %@",agentProfile.MemberID] fetch] removeAll];
    DBMyAgentProfile *dbmyagentprofile = [DBMyAgentProfile new];
    dbmyagentprofile.Date = [NSDate date];
    dbmyagentprofile.MyAgentProfile = [NSKeyedArchiver
                                       archivedDataWithRootObject:[agentProfile copy]];
    dbmyagentprofile.MemberID = [[LoginBySocialNetworkModel shared]
                                 .myLoginInfo.MemberID intValue];
    [dbmyagentprofile commit];
}
@end