//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "JSONModel.h"
@protocol Photos
@end

@interface Photos : JSONModel
@property(assign, nonatomic) int PhotoID;
@property(assign, nonatomic) int Position;
@property(strong, nonatomic) NSString* URL;
@end

//@implementation Photos

//@end