//
//  Reasons.h
//  abc
//
//  Created by Li Nang Shing on 29/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_Reasons_h
#define abc_Reasons_h

#endif
#import "JSONModel.h"
#import <Photos/Photos.h>
@protocol Reasons
@end

@interface Reasons : JSONModel
@property(assign, nonatomic) int LanguageIndex;
@property(assign, nonatomic) int SloganIndex;
@property(strong, nonatomic) NSString* Reason1;
@property(strong, nonatomic) NSString* Reason2;
@property(strong, nonatomic) NSString* Reason3;
@property(strong, nonatomic) NSString* MediaURL1;
@property(strong, nonatomic) NSString* MediaURL2;
@property(strong, nonatomic) NSString* MediaURL3;
@property(strong, nonatomic) PHAsset *asset1;
@property(strong, nonatomic) PHAsset *asset2;
@property(strong, nonatomic) PHAsset *asset3;


@end