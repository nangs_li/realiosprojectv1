//
//  AppDelegate.h
//  FBLoginUIControlSample
//
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Ken All rights reserved.
//

// ash provide color
// Green - #14e2a0    20  226 160
// Grey - #adafb1    173  175 177

#import "LoginInfo.h"
#import <UIKit/UIKit.h>
#import "RDVTabBarController.h"
#import "SlidingViewManager.h"

//#import "WeiboSDK.h"
//#import "WBHttpRequest.h"
#import "HandleServerReturnString.h"
#import "AgentProfile.h"
#import "SearchAgentProfile.h"
#import "SystemSettings.h"
#import "SystemLanguageList.h"
#import "MetricList.h"
#import "PropertyTypeList.h"
#import "SpaceTypeList.h"
#import "MBProgressHUD.h"
#import <DBAccess/DBAccess.h>
#import "QBUUserCustomData.h"
#import "AFNetworking.h"
//#import "WeiboSDK.h"
#import "MWPhotoBrowser.h"
#import "BaseNavigationController.h"
#import "CTAssetsPickerController.h"
#import <Branch/Branch.h>
@class LoginUIViewController;
@class Finishprofile;
@class Chatroom;
@class BaseViewController;

@interface AppDelegate
    : UIResponder<UIApplicationDelegate, DBDelegate>
#pragma mark LoginPage-----------------------------------------------------------------------------------------------
@property(strong, nonatomic) LoginUIViewController *loginUIViewController;
@property(strong, nonatomic) UIWindow *window;
+ (AppDelegate *)getAppDelegate;
@property(strong, nonatomic) NSString *testServerAddress;
@property(strong, nonatomic)
    MWPhotoBrowser *onePageMwPhotoBrowserNavigationController;
#pragma mark device token
@property (strong, nonatomic) MBProgressHUD *currentHUD;
@property(nonatomic, strong) NSString *devicetoken;
@property (nonatomic,strong) NSString *weiboAccessToken;
@property (nonatomic,strong) NSString *languageCode;
@property (nonatomic,strong) NSMutableArray *hasBeenSelectedUploadPhoto;
@property(nonatomic, assign ) BOOL disableViewGesture;
#pragma mark QuickBlox (Chat) Login
//// quickbloxregisterpushnotification
- (void)quickBloxRegisterPushNotification;
- (void)registerForRemoteNotifications;
//// quickbloxtotalloginonly
- (void)quickBloxTotalLoginWithSuccessBlock:(void (^)(QBResponse *response, QBUUser *user))successBlock
                                 errorBlock:(QBRequestErrorBlock)errorBlock
                                      retry:(int)retry;
//// total logout
- (void)totalLogoutErrorMessage:(NSString *)errorMessage;
#pragma mark SpotLightSearch
@property(nonatomic, strong) NSString *spotLightSearchQBID;
#pragma mark TabBarController----------------------------------------------------------------------------------------
- (RDVTabBarController *)getTabBarController;
@property(strong, nonatomic) UIViewController *viewController;
@property (nonatomic,strong) BaseNavigationController *rootViewController;
@property (nonatomic, strong) Branch *branchInstance;
////customizeTabBarForController;
- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController;
- (void)setupViewControllers;
- (void)customizeInterface;
-(void)startLoginPageOrMainPage;
#pragma mark CallServer checkError-------------------------------------------------------------------------------------
////networkconnection
- (BOOL)networkConnection;
- (void)handleModelReturnErrorShowUIAlertViewByView:(UIView *)view
                                       errorMessage:(NSString *)errorMessage
                                        errorStatus:(int)errorStatus
                                              alert:(NSString *)alert
                                                 ok:(NSString *)ok;

- (void)handleModelReturnErrorShowUIAlertViewByView:(UIView *)view
                                       errorMessage:(NSString *)errorMessage
                                        errorStatus:(int)errorStatus;
////
/// CallServer------------------------------------------------------------------------------------
#pragma mark currentSwipeAgentProfile
@property(strong, nonatomic) AgentProfile *currentAgentProfile;
- (NSString*)getCurrentAgentProfileMemberName;

#pragma mark SupportMethod-------------------------------------------------------------------------------------------
//// Futura Font
- (UIFont *)Futurafont:(CGFloat)size;
//// CGcolor
- (UIColor *)Greycolor;
- (UIColor *)Greencolor;
//// Setborder
- (void)setborder:(UIView *)uiview width:(CGFloat)width;
//// TextFieldsetleftinset
- (void)textfieldsetleftinset:(UITextField *)textfield;
//// buttonsetleftinset
- (void)buttonsetleftinset:(UIButton *)button;
//// checkisnumber
- (BOOL)checkisnumber:(UITextField *)textfield;
- (NSString *)checkisnsnumberchangetonsstring:(id)number;
- (UIViewController *)topViewController;
//// hud
@property(strong, nonatomic) MBProgressHUD *hud;
//// checkIsEmpty
- (BOOL)isEmpty:(id)thing;
////setupcornerRadius
- (void)setupcornerRadius:(UIImageView *)personalimage;
-(void)overlayViewController:(UIViewController*)controller onViewController:(UIViewController*)onViewController;
-(void)dismissOverlayerViewController:(BaseViewController*)controller;
-(void)dismissOverlayerViewController:(BaseViewController*)controller animated:(BOOL)flag extraAnimations:(void (^)(void))extraAnimations extraCompletion:(void (^ __nullable)(BOOL finished))extraCompletion dismissCompletion:(void (^)(void))dismissCompletion;
-(void)updateCurrentHudStatus:(NSString*)status;

-(void)showImagePickerOn:(UIViewController*)viewcontroller;
-(BOOL)userNeedShowTutorial:(TutorialType)type;
-(void)userDidReadTutorial:(TutorialType)type;

-(void)setupLanguageFromSystemSettingSelectSystemLanguage;

-(void)showUpdateAlert:(NSString*)updateURL forceUpdate:(BOOL)forceUpdate;

-(void)deleteAllModelAndDataBaseData;
-(void)backToTopMainPageSelectedTabBarIndex:(int)selectedTabBarIndex;
-(void)changeToPreferredLanguage;
-(void)refreshAppLanguage;
-(void)reapplyQBAccountIfNeed;

- (void)setUpBlueAndWhiteTitleFontSizeUILabel:(UILabel*)label;

- (BOOL)checkTopViewControllerIsChatRoomForQBID:(NSString *)QBID;
@end
