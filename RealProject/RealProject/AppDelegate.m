
//
//  AppDelegate.m
//  FBLoginUIControlSample
//
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Ken All rights reserved.
//
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "AppDelegate.h"
#import "LoginUIViewController.h"
#import "Onepage.h"
#import "Twopage.h"
#import "Threepage.h"
#import "BaseViewController.h"
#import "Userprofile.h"
#import "Filterpage.h"
#import "DialogsViewController.h"
#import "Introductionpage/Introductionpage.h"
#import "Editprofile.h"
#import "UIView+Borders.h"
#import "Setting.h"
#import "Finishprofile.h"
#import "Finishprofilehavelisting.h"
#import "Editprofilehaveprofile.h"
#import "MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "PropertyTypeList.h"
#import "NewsFeedtwopage.h"
#import "DBLogInfo.h"
#import "DBMyAgentProfile.h"
#import "DBNewsFeedArray.h"
#import "DBSystemSettings.h"
#import "DBChatroomAgentProfiles.h"
#import "Reachability.h"
#import "DBQBChatDialog.h"
#import "DBQBChatMessage.h"
#import "RDVTabBarItem.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "DBChatSetting.h"
#import "NewsFeedAgentprofilesModel.h"
#import "DialogPageAgentProfilesModel.h"
#import "StringEncryption.h"
#import "NSData+Base64.h"
#import "SystemSettingModel.h"
#import "PressChatButtonModel.h"
#import "DeepLinkActionHanderModel.h"
#import "AgentProfileViewController.h"
#import "ChatDialogModel.h"
#import "ChatRelationModel.h"
#import "DBQBChatDialogDelete.h"
#import "MeProfileViewController.h"
#import <Slash/Slash.h>
#import "LanguagesManager.h"
#import "SearchAgentProfileModel.h"
#import "NewsFeedAgentprofilesModel.h"
#import "ChatRelationModel.h"
#import "DBQBChatDialog.h"
#import "DBQBUUser.h"
#import "BaseNavigationController.h"
#import "FollowAgentModel.h"
#import "DBRealNetworkMemberInfo.h"
#import "LoginByEmailModel.h"
#import "SDWebImageManager.h"
#import "DBChatRoomRelationArray.h"
#import <QuickbloxWebRTC/QuickbloxWebRTC.h>
#import "MyActivityLogModel.h"
#import "RealApiClient.h"
#import "NSObject+Utility.h"
#import "BaseViewController.h"
#import "DBVersion.h"
// Mixpanel
#import "Mixpanel.h"
#import "GetContryLocation.h"
// Fabric
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>

#import "DBSearchAddressArray.h"
#import "DialogPageAgentProfilesModel.h"
#import "Chatroom.h"
#import "REPhonebookManager.h"

@interface AppDelegate ()
@property (nonatomic,strong) UIAlertController *updateAlert;
@end
@implementation AppDelegate

#pragma mark Soical Network LoginPage-----------------------------------------------------------------------------------------------------------------------------
////quickbloxregisterpushnotification

- (void)registerForRemoteNotifications {
    //// push  Notification
    if ([[UIApplication sharedApplication]
         respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication]
         registerUserNotificationSettings:
         [UIUserNotificationSettings
          settingsForTypes:(UIUserNotificationTypeSound |
                            UIUserNotificationTypeAlert |
                            UIUserNotificationTypeBadge |
                            UIUserNotificationTypeNone)
          categories:nil]];
        
    } else {
        [[UIApplication sharedApplication]
         registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                             UIUserNotificationTypeSound |
                                             UIUserNotificationTypeAlert)];
    }
}

//// - openURL
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return ( [self.branchInstance handleDeepLink:url] ||
            [[FBSDKApplicationDelegate sharedInstance] application:application
                                                           openURL:url
                                                 sourceApplication:sourceApplication
                                                        annotation:annotation] );
}
#pragma mark QuickBlox Login Part
//// handle quickblox error
- (void (^)(QBResponse *))handleError {
    return ^(QBResponse *response) {
        
        DDLogError(@"[QBRequest handleError %@", response);
        
    };
}
- (void)quickBloxTotalLoginWithSuccessBlock:(void (^)(QBResponse *response, QBUUser *user))successBlock
                                 errorBlock:(QBRequestErrorBlock)errorBlock
                                      retry:(int)retry {
    int currentRetryCount = retry--;
    QBUUser *currentUser = [[LoginBySocialNetworkModel shared].myLoginInfo qbUser];
    // Login QB API Service
    [QBRequest logInWithUserLogin:currentUser.login
                         password:currentUser.password
                     successBlock:^(QBResponse *response, QBUUser *user) {
                         
                         if (successBlock) {
                             
                             successBlock(response, user);
                         }
                         
                         // Login QB Chat Service
                         [[ChatService shared] loginWithUser:currentUser completionBlock:^{
                             
                         }];
                         
                         [self quickBloxRegisterPushNotification];
                         
                     } errorBlock:^(QBResponse *response) {
                         
                         if (response.status == 422 || response.status == 401) {
                             
                             if (errorBlock) {
                                 
                                 errorBlock(response);
                             }
                             
                         } else if (currentRetryCount > 0) {
                             
                             [self quickBloxTotalLoginWithSuccessBlock:successBlock errorBlock:errorBlock retry:currentRetryCount];
                             
                         } else {
                             
                             [CrashlyticsKit recordError:response.error.error withAdditionalUserInfo:nil];
                             
                             if (errorBlock) {
                                 
                                 errorBlock(response);
                             }
                         }
                     }];
}

- (void)quickBloxRegisterPushNotification {
    [self registerForRemoteNotifications];
}

//// total logout
- (void)totalLogoutErrorMessage:(NSString *)errorMessage{
    //    [[self rootViewController] dismissViewControllerAnimated:YES completion:nil];
    //    if ([self.window.rootViewController  isKindOfClass:[LoginUIViewController class]]) {
    //        if (self.loginUIViewController == self.window.rootViewController)
    //            [self.loginUIViewController setApiDelegateToLoginUiViewController];
    //
    //    } else {
    [self changeToPreferredLanguage];
    LoginUIViewController *navVC =[[LoginUIViewController alloc]initWithNibName:@"LoginUIViewController" bundle:nil type:Login];
    self.rootViewController = navVC;
    [self.window setRootViewController:navVC];
    //    //    }
    //    //// popup Access Error  alert
    //    // [self.window makeKeyAndVisible];
    //
    //    NSString * message;
    //    if (hasAccessError) {
    //        message =JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil);
    //    }else{
    //        message =Yohaveeensuccessfullyloggedout;
    //
    //    }
    if ([errorMessage isEqualToString:@"logout"]) {
        
    }else{
        UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:JMOLocalizedString(@"alert_alert", nil) message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleCancel handler:nil];
        if ([errorMessage isEqualToString:JMOLocalizedString(@"alert_account_suspended", nil)]) {
            UIAlertAction *supportAction = [UIAlertAction actionWithTitle:ContactText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSString *mailURLString = [NSString stringWithFormat:@"mailto:%@?subject=%@",kSupportEmalAddress,SuspendEmailSubject];
                NSURL *mailURL = [NSURL URLWithString:[mailURLString stringByAddingPercentEscapesUsingEncoding:
                                                       NSUTF8StringEncoding]];
                if ([[UIApplication sharedApplication]canOpenURL:mailURL]) {
                    [[UIApplication sharedApplication] openURL:mailURL];
                }
            }];
            [alertViewController addAction:supportAction];
        }
        [alertViewController addAction:cancelAction];
        [navVC presentViewController:alertViewController animated:YES completion:nil];
    }
    [QBRequest unregisterSubscriptionForUniqueDeviceIdentifier:
     [[[UIDevice currentDevice] identifierForVendor] UUIDString]
                                                  successBlock:^(QBResponse *response) {
                                                      
                                                      [QBRequest logOutWithSuccessBlock:^(QBResponse *  response) {
                                                          
                                                      } errorBlock:^(QBResponse *  response) {
                                                          
                                                      }];
                                                      DDLogInfo(@"Unsubscribed successfully");
                                                      
                                                  }
                                                    errorBlock:^(QBError *error) {
                                                        DDLogError(@"QBError %@", error);
                                                        // Handle error
                                                        [QBRequest logOutWithSuccessBlock:^(QBResponse *  response) {
                                                            
                                                        } errorBlock:^(QBResponse *  response) {
                                                            
                                                        }];
                                                    }];
    
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel reset];
    NSString *uuid = [[NSUUID UUID] UUIDString];
    [mixpanel identify:uuid];
    [[[DBRealNetworkMemberInfo query]fetch]removeAll];
    [LoginByEmailModel shared].realNetworkMemberInfo =nil;
    [self deleteAllModelAndDataBaseData];
}

-(void)deleteAllModelAndDataBaseData{
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kNSUserDefaultKeyUpdateVersion];
    [[CSSearchableIndex defaultSearchableIndex] deleteSearchableItemsWithDomainIdentifiers:@[@"com.real.newsfeed",@"com.real.chat"] completionHandler:^(NSError *  error) {
        
    }];
    //// remove local data
    [[[DBLogInfo query] fetch] removeAll];
    [[[DBMyAgentProfile query] fetch] removeAll];
    //// remove memory data
    
    self.currentAgentProfile = nil;
    [[AgentListingModel shared]resetAllAgentListingPartOnlyLocalData];
    [MyAgentProfileModel shared].myAgentProfile = nil;
    [LoginBySocialNetworkModel shared].myLoginInfo = nil;
    [LoginBySocialNetworkModel shared].isApplyingQBAccount = NO;
    [AgentProfileRegistrationModel shared].ExperienceArray = [[NSMutableArray alloc]init];
    [AgentProfileRegistrationModel shared].Top5Array = [[NSMutableArray alloc]init];
    [AgentProfileRegistrationModel shared].SpecialtyArray = [[NSMutableArray alloc]init];
    [AgentProfileRegistrationModel shared].LanguagesArray = [[NSMutableArray alloc]init];
    [[AgentListingModel shared] resetAllAgentListingPartOnlyLocalData];
    [SearchAgentProfileModel shared].AgentProfiles= nil;
    [SearchAgentProfileModel shared].HandleServerReturnString= nil;
    [SearchAgentProfileModel shared].searchAgentProfilesHaveTotalCount= nil;
    [SearchAgentProfileModel shared].searchAgentProfilesTotalCount= 0;
    [SearchAgentProfileModel shared].location= nil;
    [SearchAgentProfileModel shared].locationManager =nil;
    [SearchAgentProfileModel shared].didHandleUserLocation =NO;
    [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles= nil;
    [NewsFeedAgentprofilesModel shared].HandleServerReturnString= nil;
    [NewsFeedAgentprofilesModel shared].newsfeedAgentProfilesHaveTotalCount= nil;
    [NewsFeedAgentprofilesModel shared].Page = 1;
    [ChatRelationModel shared].HandleServerReturnString= nil;
    [ChatRelationModel shared].chatRoomRelationArray= nil;
    [ChatRelationModel shared].chatRoomRelationArrayHaveTotalCount= nil;
    [ChatRelationModel shared].currentChatRoomRelationObject= nil;
    [ChatRelationModel shared].mySelfChatRoomRelation= nil;
    [ChatRelationModel shared].recipienerChatRoomRelation= nil;
    [ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty=YES;
    [ChatDialogModel shared].dialogs=nil;
    [DialogPageAgentProfilesModel shared].dialogPageSearchAgentProfiles=nil;
    [[[DBQBChatDialog query] fetch]removeAll ] ;
    [[[DBQBUUser query]fetch] removeAll];
    // [[[DBQBChatMessage query] fetch]removeAll ] ;
    [[NewsFeedAgentprofilesModel shared].getNewsFeedTimer invalidate];
    [LoginBySocialNetworkModel shared].myLoginInfo=nil;
    ////remove push notification
    //[[[DBChatroomAgentProfiles query]fetch]removeAll];
    
    [[Twitter sharedInstance]logOut];
    [SearchAgentProfileModel shared].locationManager.delegate=nil;
    [SearchAgentProfileModel shared].location=nil;
    
    // QuickBlox logout
    // Chat Service
    [[ChatService shared] logout];
    // API Service
    // [QBRequest logOutWithSuccessBlock:nil errorBlock:nil];
}
#pragma mark Application Method Start---------------------------------------------------------------------------------------------------
+ (AppDelegate *)getAppDelegate {
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[RealApiClient sharedClient]checkVersion];
    
    // Mixpanel
    [Mixpanel sharedInstanceWithToken:kMixpanelToken];
    
    // Fabric
    [Fabric with:@[[Crashlytics class], [Twitter class]]];
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    NSError *error;
    BOOL success = [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&error];
    // [QBRTCClient initializeRTC];
    [[SDWebImageManager sharedManager].imageDownloader setMaxConcurrentDownloads:NSOperationQueueDefaultMaxConcurrentOperationCount];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"firstLogin"]) {
        [[NSUserDefaults standardUserDefaults]setObject:@(0) forKey:@"firstLogin"];
    }else{
        [[NSUserDefaults standardUserDefaults]setObject:@(1) forKey:@"firstLogin"];
    }
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    //  self.testServerAddress = serveraddress;
    //// DDASLLogger tool
    setenv("XcodeColors", "YES", 0);
    
    [[DDASLLogger sharedInstance]
     setLogFormatter:[[MyCustomLogFormatter alloc] init]];
    [[DDTTYLogger sharedInstance]
     setLogFormatter:[[MyCustomLogFormatter alloc] init]];
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    UIColor *pink = [UIColor colorWithRed:(255 / 255.0)
                                    green:(58 / 255.0)
                                     blue:(159 / 255.0)
                                    alpha:1.0];
    [[DDTTYLogger sharedInstance] setForegroundColor:pink
                                     backgroundColor:nil
                                             forFlag:DDLogFlagDebug];
    UIColor *gray = [UIColor colorWithRed:(156 / 255.0)
                                    green:(158 / 255.0)
                                     blue:(161 / 255.0)
                                    alpha:1.0];
    [[DDTTYLogger sharedInstance] setForegroundColor:gray
                                     backgroundColor:nil
                                             forFlag:DDLogFlagVerbose];
    UIColor *yellow = [UIColor colorWithRed:(176 / 255.0)
                                      green:(178 / 255.0)
                                       blue:(16 / 255.0)
                                      alpha:1.0];
    [[DDTTYLogger sharedInstance] setForegroundColor:yellow
                                     backgroundColor:nil
                                             forFlag:DDLogFlagInfo];
    
    ////ORM DBAccess Database
    [DBAccess setDelegate:self];
    [DBAccess openDatabaseNamed:@"myApplication"];
    NSDictionary *remoteNotifiInfo = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    
    //Accept push notification when app is not open
    if (remoteNotifiInfo) {
        [self application:application didReceiveRemoteNotification:remoteNotifiInfo];
    }
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo) {
        [self handleRemoteNotifications:userInfo];
    }
    
    //    UILocalNotification *notification = [launchOptions
    //                                         objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    //    if (notification) {
    //        DDLogInfo(@"app recieved notification from remote%@", notification);
    //        [self application:application
    //                                didReceiveRemoteNotification:(NSDictionary *)notification];
    //
    //    } else {
    //        DDLogInfo(@"app did not recieve notification");
    //    }
    
    ////ORM Quickblox setup
    
    //
    //    [QBApplication sharedApplication].applicationId = 25583;
    //    [QBConnection registerServiceKey:@"ftZGNT7YOQeAywj"];
    //    [QBConnection registerServiceSecret:@"wD2TwzPrh979WVy"];
    //    [QBSettings setAccountKey:@"uKi6tB3ANZ1ivwG9cyzx"];
    
    [QBSettings setApplicationID:QBApplicationID];
    [QBSettings setAuthKey:QBAuthKey];
    [QBSettings setAuthSecret:QBAuthSecret];
    [QBSettings setAccountKey:QBAcctountKey];
    if(!kDebugMode){
        [QBSettings setApiEndpoint:QBAPIEndPoint chatEndpoint:QBChatEndPoint forServiceZone:QBConnectionZoneTypeProduction];
        
        
        [QBSettings setServiceZone:QBConnectionZoneTypeProduction];
    }
    [QBSettings setLogLevel:qbLogLevel];
    //    QBASession *session = [QBASession new];
    //    [QBSession currentSession];
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss z"];
    //    NSDate *expirationDate = [formatter dateFromString:@"2018-10-20 11:13:06 UTC"];
    //    [[QBSession currentSession] startSessionWithDetails:session expirationDate:expirationDate];
    BOOL isTokenValid=  [[QBSession currentSession] isTokenValid];
    
    
    [QBApplication sharedApplication].autoDetectEnvironment = YES;
    NSString * apiEndpoint=  [QBSettings apiEndpoint];
    
    
    
    
    [[GetContryLocation shared]getContryLocation];
    
    // twitter
    [[Twitter sharedInstance]
     startWithConsumerKey:@"iOixdOWc7H3kIzb9o8L5qrx2p"
     consumerSecret:
     @"PwsT8oruKKMquayanHyOTqFBsbgLqEO9eMANTBg6faXCTjCcj2"];
    
    //// setup loginfo from db
    DBResultSet *r;
    
    r = [[[[DBLogInfo query] orderByDescending:@"Date"] limit:1] fetch];
    
    for (DBLogInfo *p in r) {
        [LoginBySocialNetworkModel shared].myLoginInfo = [NSKeyedUnarchiver unarchiveObjectWithData:p.LogInfo];
    }
    //// setup DBrealNetworkMemberInfo from db
    
    
    r = [[[[DBRealNetworkMemberInfo query] orderByDescending:@"Date"] limit:1] fetch];
    
    for (DBRealNetworkMemberInfo *p in r) {
        [LoginByEmailModel shared].realNetworkMemberInfo = [NSKeyedUnarchiver unarchiveObjectWithData:p.RealNetworkMemberInfo];
    }
    [DBVersion checkVersion];
    [self setWindow:[[UIWindow alloc]initWithFrame:[RealUtility screenBounds]]];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self quickBloxRegisterPushNotification];
    r = [[[[DBChatRoomRelationArray query]
           whereWithFormat:
           @"MemberID = %@",
           [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
            .myLoginInfo.MemberID]]
          limit:1] fetch];
    
    for (DBChatRoomRelationArray *p in r) {
        
        [ChatRelationModel shared].chatRoomRelationArray = [NSKeyedUnarchiver unarchiveObjectWithData:p.ChatRoomRelationArray];
    }
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    NSString *memberIdString = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID.stringValue;
    if ([memberIdString length] > 0) {
        
        [mixpanel identify:memberIdString];
        
    }
    
    [mixpanel track:@"Opened App"];
    
    [self startLoginPageOrMainPage];
    
    //// - init MePageAgentProfile  array
    [AgentProfileRegistrationModel shared].ExperienceArray =
    [[NSMutableArray alloc] init];
    [AgentProfileRegistrationModel shared].LanguagesArray =
    [[NSMutableArray alloc] init];
    [AgentProfileRegistrationModel shared].Top5Array =
    [[NSMutableArray alloc] init];
    [AgentProfileRegistrationModel shared].SpecialtyArray =
    [[NSMutableArray alloc] init];
    //// - init MePageAgentCreateNewPosting  array
    [AgentListingModel shared].addnewlanguageindex =
    [[NSMutableArray alloc] init];
    [AgentListingModel shared].addnewonereason = [[NSMutableArray alloc] init];
    [AgentListingModel shared].addnewtworeason = [[NSMutableArray alloc] init];
    [AgentListingModel shared].addnewthreereason = [[NSMutableArray alloc] init];
    [AgentListingModel shared].googleGeoCodeApiAddressArray =
    [[NSMutableArray alloc] init];
    
    Branch *branch = self.branchInstance;
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        
        DDLogDebug(@"deep link data: %@", [params description]);
        
        if (!error) {
            
            [[DeepLinkActionHanderModel shared]handleLinkWithParameter:params];
        }
    }];
    
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = gaLogLevel;
    
    NSDictionary *activityDic = [launchOptions objectForKey:UIApplicationLaunchOptionsUserActivityDictionaryKey];
    
    if (activityDic) {
        
        NSUserActivity * userActivity = [activityDic valueForKey:@"UIApplicationLaunchOptionsUserActivityKey"];
        [self spotLightSearchHandlingWithUserActivity:userActivity];
    }
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    [self registerForRemoteNotifications];
    [[REPhonebookManager sharedManager]requestAccess:^(BOOL granted) {
        
    }];
    
    return YES;
}
-(void)startLoginPageOrMainPage{
    
    if (![self isEmpty:[LoginBySocialNetworkModel shared].myLoginInfo.MemberID]) {
        
        if (isEmpty([LoginBySocialNetworkModel shared].myLoginInfo.PhoneNumber)) {
            
            [self changeToPreferredLanguage];
            LoginUIViewController *navVC =[[LoginUIViewController alloc]initWithNibName:@"LoginUIViewController" bundle:nil type:Mirgate];
            self.rootViewController = navVC;
            [self.window setRootViewController:navVC];
            
        } else {
            
            //// setup systemsetting from db
            DBResultSet * r;
            r = [[[[[DBSystemSettings query]
                    whereWithFormat:
                    @"MemberID = %@",
                    [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
                     .myLoginInfo.MemberID]]
                   orderByDescending:@"Date"] limit:1] fetch];
            
            for (DBSystemSettings *p in r) {
                [SystemSettingModel shared].SystemSettings =
                [NSKeyedUnarchiver unarchiveObjectWithData:p.SystemSettings];
                
                if ([self isEmpty:p.selectSystemLanguageList]) {
                    [self changeToPreferredLanguage];
                    
                } else {
                    [SystemSettingModel shared].selectSystemLanguageList =
                    [NSKeyedUnarchiver
                     unarchiveObjectWithData:p.selectSystemLanguageList];
                    [self setupLanguageFromSystemSettingSelectSystemLanguage];
                }
            }
            //// setup DBMyAgentProfile from db
            r = [[[[[DBMyAgentProfile query]
                    whereWithFormat:
                    @"MemberID = %@",
                    [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
                     .myLoginInfo.MemberID]]
                   orderByDescending:@"Date"] limit:1] fetch];
            
            for (DBMyAgentProfile *p in r) {
                [MyAgentProfileModel shared].myAgentProfile =
                [NSKeyedUnarchiver unarchiveObjectWithData:p.MyAgentProfile];
            }
            
            //// DBNewsFeedArray from db
            
            r = [[[[[DBNewsFeedArray query]
                    whereWithFormat:
                    @"MemberID = %@",
                    [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
                     .myLoginInfo.MemberID]]
                   orderByDescending:@"Date"] limit:1] fetch];
            
            for (DBNewsFeedArray *p in r) {
                [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles = [[NSKeyedUnarchiver unarchiveObjectWithData:p.NewsFeedArray] mutableCopy];
            }
            
            
            
            
            
            
            //// DBQBChatDialog
            r = [[[[DBQBChatDialog query]
                   whereWithFormat:@"MemberID = %@",
                   [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]
                  orderByDescending:@"LastMessageDate"]
                 
                 fetch];
            NSMutableArray *dialogsarray = [[NSMutableArray alloc] init];
            QBChatDialog *chatDialog;
            for (DBQBChatDialog *p in r) {
                chatDialog = [NSKeyedUnarchiver unarchiveObjectWithData:p.QBChatDialog];
                if ([[[DBQBChatDialogDelete query]
                      whereWithFormat:@" DialogID = %@", chatDialog.ID] count] == 0) {
                    [dialogsarray addObject:chatDialog];
                }
                DDLogInfo(@"chatDialog.recipientID : %ld", (long)chatDialog.recipientID);
            }
            
            [ChatDialogModel shared].dialogs = [dialogsarray mutableCopy];
            [[DialogPageAgentProfilesModel shared] getAllDBDialogPageSearchAgentProfiles];
            
            [self setupViewControllers];
            [self customizeInterface];
            [[self getTabBarController] setSelectedIndex:1];
            
            [self quickBloxTotalLoginWithSuccessBlock:nil
                                           errorBlock:nil
                                                retry:3];
            
            //// setup Metric PropertyType SpokenLanguage
            [[SystemSettingModel shared] setupSpokenLanguageList];
            //// default English Language
            
            [[SystemSettingModel shared] setupMetricAccordingToLanguage];
            [[SystemSettingModel shared] setupPropertyTypeAccordingToLanguage];
            
            /*
             if (self.settotabbar2) {
             [[self getTabBarController] setSelectedIndex:2];
             self.settotabbar2 = NO;
             }
             */
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage object:nil];
            [self.window setRootViewController:[self rootViewController]];
            [[NewsFeedAgentprofilesModel shared] callNewsFeedApiTimer];
            [[NewsFeedAgentprofilesModel shared]callNewsFeedFromBackGround];
            [[DialogPageAgentProfilesModel shared]callUpdateDBDiaolgPageAgentProfileTimer];
            [[SearchAgentProfileModel shared] getDBSearchAddressArrayToSearchAgentProfileModel];
            
        }
        
    } else {
        [self changeToPreferredLanguage];
        LoginUIViewController *navVC =[[LoginUIViewController alloc]initWithNibName:@"LoginUIViewController" bundle:nil type:Login];
        self.rootViewController = navVC;
        [self.window setRootViewController:navVC];
    }
    [[DialogPageAgentProfilesModel shared] getAllDBDialogPageSearchAgentProfiles];
    [self.window makeKeyAndVisible];
    
}
- (void)applicationWillTerminate:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationChatDidAccidentallyDisconnect
     object:nil];
    
    [[ChatService shared] disconnectUser];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationChatDidAccidentallyDisconnect
     object:nil];
    
    [[ChatService shared] disconnectUser];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[ChatService shared] loginWithUser:[ChatService shared].currentUser
                        completionBlock:^{
                            
                        }];
    [[RealApiClient sharedClient]checkVersion];
    [[NewsFeedAgentprofilesModel shared] callNewsFeedApiTimer];
    [[NewsFeedAgentprofilesModel shared]callNewsFeedFromBackGround];
    [[DialogPageAgentProfilesModel shared]callUpdateDBDiaolgPageAgentProfileTimer];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationApplcationWillEnterBackGround object:nil];
}


- (void)applicationWillResignActive:(UIApplication *)application{
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationApplicationWillResignActive object:nil];
}


//// push Notification
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    self.devicetoken= [[[[deviceToken description]    stringByReplacingOccurrencesOfString:@"<"withString:@""]
                        stringByReplacingOccurrencesOfString:@">" withString:@""]
                       stringByReplacingOccurrencesOfString: @" " withString: @""];;
    
    QBMSubscription *subscription = [QBMSubscription subscription];
    subscription.notificationChannel = QBMNotificationChannelAPNS;
    subscription.deviceUDID = deviceIdentifier;
    subscription.deviceToken = deviceToken;
    
    [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
        
    } errorBlock:^(QBResponse *response) {
        
    }];
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people addPushDeviceToken:deviceToken];
}
- (void)application:(UIApplication *)application
didRegisterUserNotificationSettings:
(UIUserNotificationSettings *)notificationSettings {
    // register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)userInfo
  completionHandler:(void (^)())completionHandler {
    DDLogInfo(@"notificationopen");
    if ([identifier isEqualToString:@"declineAction"]) {
        
    } else if ([identifier isEqualToString:@"answerAction"]) {
        
    }
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    application.applicationIconBadgeNumber = 0;
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationApplcationDidBecomeActive object:nil];
    [MyActivityLogModel shared].newLogCount = [MyActivityLogModel shared].newLogCount;
    [[LoginBySocialNetworkModel shared] reapplyQBAccountIfNeed];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
  //    completionHandler(UIBackgroundFetchResultNewData);

  DDLogDebug(@"didReceiveRemoteNotification = %@", userInfo);

  if ([[LoginBySocialNetworkModel shared].myLoginInfo isLoggedIn]) {
    RealRemoteNotificationType pushType = [self getNotificationType:userInfo];
    if (pushType == RealRemoteNotificationTypeChat) {
      if (application && application.applicationState != UIApplicationStateActive) {
        DDLogDebug(@"application.applicationState != UIApplicationStateActive");
        [[ChatService shared] loginWithUser:[ChatService shared].currentUser
                            completionBlock:^{
                              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                                UIApplication *application = [UIApplication sharedApplication];

                                if (application && application.applicationState != UIApplicationStateActive) {
                                  [[ChatService shared] disconnectUser];
                                  completionHandler(UIBackgroundFetchResultNewData);
                                }
                              });
                            }];
      }
      if (application && application.applicationState == UIApplicationStateInactive) {
        NSNumber *qbID = [userInfo objectForKey:@"user_id"];
        //              NSString *dialogId   = [userInfo objectForKey:@"dialog_id"];
        NSString *qbIDString = [qbID stringValue];

        if (!isEmpty(qbIDString)) {
          if (![self checkTopViewControllerIsChatRoomForQBID:qbIDString]) {
            [[DialogPageAgentProfilesModel shared] agentProfileGetListFromQBID:qbIDString
                success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                  if (![self isEmpty:AgentProfiles]) {
                    [AppDelegate getAppDelegate].currentAgentProfile = [AgentProfiles firstObject];
                    [self moveToChatLobbyAndEnterChatRoomFrom:qbIDString];
                  }

                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok){

                }];
          }
        }
      }

    } else {
      [self handleRemoteNotifications:userInfo];
    }
  }
}
- (void)moveToChatLobbyAndEnterChatRoomFrom:(NSString *)QBID {
  [kAppDelegate backToTopMainPageSelectedTabBarIndex:2];
  NSDictionary *QBIDDict = [NSDictionary dictionaryWithObjectsAndKeys:QBID, @"QBID", nil];
  [[NSNotificationCenter defaultCenter] postNotificationName:kPushNotificationEnterChatRoom object:QBIDDict];

  [[ChatService shared] loginWithUser:[ChatService shared].currentUser
                      completionBlock:^{
                      }];
}
- (BOOL)checkTopViewControllerIsChatRoomForQBID:(NSString *)QBID {
  UINavigationController *navigationController = (UINavigationController *)[[self getTabBarController] selectedViewController];
  if ([navigationController.topViewController isKindOfClass:[JSQMessagesViewController class]]) {
    // Chatroom *chatroom = (JSQMessagesViewController *)navigationController.topViewController;
    if (isEmpty(self.currentAgentProfile.QBID)) {
      return NO;
    }
    if ([self.currentAgentProfile.QBID isEqualToString:QBID]) {
      UIViewController *presentedViewController = [self getTabBarController].selectedViewController.presentedViewController;
      if (presentedViewController) {
        if ([presentedViewController isKindOfClass:[UINavigationController class]]) {
          UINavigationController *navVC       = (UINavigationController *)presentedViewController;
          UIViewController *topViewController = navVC.topViewController;
          if (!isEmpty(topViewController)) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            [topViewController dismissViewControllerAnimated:YES completion:nil];
          }
        }
      }
      return YES;
    } else {
      return NO;
    }
  } else {
    return NO;
  }
}
- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
    
    DDLogDebug(@"performFetchWithCompletionHandler");
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    [self handleRemoteNotifications:userInfo];
}

- (void)handleRemoteNotifications:(NSDictionary *)userInfo{
    
    RealRemoteNotificationType pushType = [self getNotificationType:userInfo];
    
    switch (pushType) {
            
        case RealRemoteNotificationTypeFollow: {
            
            NSInteger newCount = [userInfo[@"NewCount"] integerValue];
            [MyActivityLogModel shared].newLogCount = newCount;
            [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationDidReceiveNewFollow object:nil];
            [UIApplication sharedApplication].applicationIconBadgeNumber ++;
            break;
        }
            
        case RealRemoteNotificationTypeChat: {
            
            DDLogDebug(@"RealRemoteNotificationTypeChat");
            break;
        }
            
        case RealRemoteNotificationTypeOther: {
            
            DDLogDebug(@"RealRemoteNotificationTypeOther");
            break;
        }
            
        default: {
            
            break;
        }
    }
}


- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DDLogInfo(@"didFailToRegisterForRemoteNotificationsWithError-->%@", error);
}
- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
 restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler{
    
    BOOL handledByBranch = [self.branchInstance continueUserActivity:userActivity];
    
    if (!handledByBranch) {
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstanceWithToken:kMixpanelToken];
        [mixpanel track:@"Spotlight Search"];
        
        [self spotLightSearchHandlingWithUserActivity:userActivity];
    }
    
    return YES;
}
#pragma mark TabBarController--------------------------------------------------------------------------------------------------------
//// - #getTabBarController
- (RDVTabBarController *)getTabBarController {
    return (RDVTabBarController *)self.viewController;
}
- (void)setupViewControllers {
    [[SearchAgentProfileModel shared]initLocationManager];
    //// setup addresssearch page
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"RealStoryboard"
                                                             bundle: nil];
    BaseNavigationController *nav3 = (BaseNavigationController*)[mainStoryboard
                                                                 instantiateViewControllerWithIdentifier: @"RDVTabBarController"];
    RDVTabBarController *tabBarController = (RDVTabBarController*) [nav3 topViewController];
    
    
    Twopage *twopage = [[Twopage alloc] init];
    AgentProfileViewController *onepage =
    [[AgentProfileViewController alloc] init];
    ApartmentDetailViewController *threepage =
    [[ApartmentDetailViewController alloc] init];
    
    MMDrawerController *drawerController =
    [[MMDrawerController alloc] initWithCenterViewController:twopage
                                    leftDrawerViewController:threepage
                                   rightDrawerViewController:onepage];
    twopage.MMDrawerController = drawerController;
    twopage.Onepageviewcontroller = onepage;
    twopage.Threepageviewcontroller = threepage;
    [drawerController setShowsShadow:YES];
    [drawerController setRestorationIdentifier:@"MMDrawer"];
    [drawerController setMaximumRightDrawerWidth:200.0];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    CGRect screenRect = [RealUtility screenBounds];
    CGFloat screenWidth = screenRect.size.width;
    [drawerController setMaximumLeftDrawerWidth:screenWidth];
    [drawerController setMaximumRightDrawerWidth:screenWidth];
    CGFloat parallaxFactor =4.0f;
    CGFloat alphaFactor = 3.0;
    MMDrawerControllerDrawerVisualStateBlock visualStateBlock = ^(MMDrawerController * drawerController, MMDrawerSide drawerSide, CGFloat percentVisible){
        //       NSParameterAssert(parallaxFactor >= 1.0);
        CATransform3D transform = CATransform3DIdentity;
        UIViewController * sideDrawerViewController;
        CGFloat navAlpha = percentVisible*alphaFactor;
        if (navAlpha <0) {
            navAlpha =0;
        }else if (navAlpha >1){
            navAlpha =1;
        }
        
        UIViewController *centerViewController = drawerController.centerViewController;
        if ([centerViewController isKindOfClass:[BaseViewController class]]) {
            BaseViewController* centerBaseViewController = (BaseViewController*)centerViewController;
            centerBaseViewController.navBarContentView.alpha = 1-navAlpha;
            CGFloat opactiy = navAlpha *2.0;
            if (opactiy < 0) {
                opactiy = 0;
            }else if(opactiy >0.8){
                opactiy =0.8;
            }
            DDLogDebug(@"opactiy:%f",opactiy);
            centerBaseViewController.spotlightLayer.opacity = opactiy;
        }
        
        if(drawerSide == MMDrawerSideLeft) {
            sideDrawerViewController = drawerController.leftDrawerViewController;
            if ([sideDrawerViewController isKindOfClass:[BaseViewController class]]) {
                BaseViewController* leftBaseViewController = (BaseViewController*)sideDrawerViewController;
                leftBaseViewController.navBarContentView.alpha = navAlpha;
            }
            CGFloat distance = MAX(drawerController.maximumLeftDrawerWidth,drawerController.visibleLeftDrawerWidth) -1;
            if (percentVisible <= 1.0) {
                CGFloat translation = (-distance)/parallaxFactor+(distance*percentVisible/parallaxFactor);
                transform = CATransform3DMakeTranslation(translation, 0.0, 0.0);
            }
            else{
                transform = CATransform3DMakeScale(percentVisible, 1.0, 1.0);
                transform = CATransform3DTranslate(transform, drawerController.maximumLeftDrawerWidth*(percentVisible-1.0)/2, 0.0, 0.0);
            }
        }
        else if(drawerSide == MMDrawerSideRight){
            sideDrawerViewController = drawerController.rightDrawerViewController;
            if ([sideDrawerViewController isKindOfClass:[BaseViewController class]]) {
                BaseViewController* rightBaseViewController = (BaseViewController*)sideDrawerViewController;
                rightBaseViewController.navBarContentView.alpha = navAlpha;
            }
            CGFloat distance = MAX(drawerController.maximumRightDrawerWidth,drawerController.visibleRightDrawerWidth) -1;
            if(percentVisible <= 1.0){
                CGFloat translation = (distance)/parallaxFactor-(distance*percentVisible)/parallaxFactor;
                transform = CATransform3DMakeTranslation(translation, 0.0, 0.0);
            }
            else{
                transform = CATransform3DMakeScale(percentVisible, 1.0, 1.0);
                transform = CATransform3DTranslate(transform, drawerController.maximumRightDrawerWidth*(percentVisible-1.0)/2, 0.0, 0.0);
            }
        }
        [sideDrawerViewController.view.layer setTransform:transform];
    };
    [drawerController
     setDrawerVisualStateBlock:visualStateBlock];
    
    //  [[MMExampleDrawerVisualStateManager sharedManager]
    //      setLeftDrawerAnimationType:MMDrawerAnimationTypeSlide];
    //  [[MMExampleDrawerVisualStateManager sharedManager]
    //      setRightDrawerAnimationType:MMDrawerAnimationTypeSlide];
    
    UIViewController *firstNavigationController = [[BaseNavigationController alloc]
                                                   initWithRootViewController:drawerController];
    //// setup newfeed page
    
    NewsFeedtwopage *twopage2 = [[NewsFeedtwopage alloc] init];
    AgentProfileViewController *onepage2 =
    [[AgentProfileViewController alloc] init];
    ApartmentDetailViewController *threepage2 =
    [[ApartmentDetailViewController alloc] init];
    
    MMDrawerController *drawerController2 =
    [[MMDrawerController alloc] initWithCenterViewController:twopage2
                                    leftDrawerViewController:threepage2
                                   rightDrawerViewController:onepage2];
    twopage2.MMDrawerController = drawerController2;
    twopage2.Onepageviewcontroller = onepage2;
    twopage2.Threepageviewcontroller = threepage2;
    
    [drawerController2 setShowsShadow:YES];
    [drawerController2 setRestorationIdentifier:@"MMDrawer"];
    [drawerController2 setMaximumRightDrawerWidth:200.0];
    [drawerController2 setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController2 setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [drawerController2 setMaximumLeftDrawerWidth:screenWidth];
    [drawerController2 setMaximumRightDrawerWidth:screenWidth];
    [drawerController2
     setDrawerVisualStateBlock:visualStateBlock];
    
    //  [[MMExampleDrawerVisualStateManager sharedManager]
    //      setLeftDrawerAnimationType:MMDrawerAnimationTypeSlide];
    //  [[MMExampleDrawerVisualStateManager sharedManager]
    //      setRightDrawerAnimationType:MMDrawerAnimationTypeSlide];
    UIViewController *secondNavigationController = [[BaseNavigationController alloc]
                                                    initWithRootViewController:drawerController2];
    
    //// setup DialogsViewController
    [ChatService shared];
    
    DialogsViewController *dialogsViewController =
    [[DialogsViewController alloc] init];
    AgentProfileViewController *dialogsonepage =
    [[AgentProfileViewController alloc] init];
    ApartmentDetailViewController *dialogsthreepage =
    [[ApartmentDetailViewController alloc] init];
    
    
    MMDrawerController *dialogsdrawerController = [[MMDrawerController alloc]
                                                   initWithCenterViewController:dialogsViewController
                                                   leftDrawerViewController:dialogsthreepage
                                                   rightDrawerViewController:dialogsonepage];
    dialogsViewController.MMDrawerController = dialogsdrawerController;
    dialogsViewController.Onepageviewcontroller = dialogsonepage;
    dialogsViewController.Threepageviewcontroller = dialogsthreepage;
    [dialogsdrawerController setShowsShadow:YES];
    [dialogsdrawerController setRestorationIdentifier:@"MMDrawer"];
    [dialogsdrawerController setMaximumRightDrawerWidth:200.0];
    [dialogsdrawerController
     setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [dialogsdrawerController
     setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [dialogsdrawerController setMaximumLeftDrawerWidth:screenWidth];
    [dialogsdrawerController setMaximumRightDrawerWidth:screenWidth];
    [dialogsdrawerController
     setDrawerVisualStateBlock:visualStateBlock];
    
    [[MMExampleDrawerVisualStateManager sharedManager]
     setLeftDrawerAnimationType:MMDrawerAnimationTypeSlide];
    [[MMExampleDrawerVisualStateManager sharedManager]
     setRightDrawerAnimationType:MMDrawerAnimationTypeSlide];
    
    UIViewController *thirdNavigationController = [[BaseNavigationController alloc]
                                                   initWithRootViewController:dialogsdrawerController];
    //// setup MePage
    NSString *meProfileNib = nil;
    AgentStatus status;
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        meProfileNib = @"MeProfileViewController";
        if ([RealUtility
             isValid:[MyAgentProfileModel shared].myAgentProfile.AgentListing]) {
            status = AgentStatusWithListing;
        } else {
            status = AgentStatusWithoutListing;
        }
    } else {
        meProfileNib = @"MeNotAgentProfileViewController";
        status = AgentStatusNotAgent;
    }
    
    MeProfileViewController *twopage3 =
    [[MeProfileViewController alloc] initWithNibName:meProfileNib bundle:nil];
    twopage3.agentStatus = status;
    Editprofilehaveprofile *onepage3 = [[Editprofilehaveprofile alloc] init];
    onepage3.viewType = EditProfilePreview;
    UIViewController *threepage3;
    if (status != AgentStatusWithListing) {
        threepage3 = nil;
    } else {
        threepage3 = [[ApartmentDetailViewController alloc] init];
        tabBarController.navigationController.delegate = nil;
    }
    
    MMDrawerController *drawerController3 =
    [[MMDrawerController alloc] initWithCenterViewController:twopage3
                                    leftDrawerViewController:threepage3
                                   rightDrawerViewController:onepage3];
    onepage3.MMDrawerController      = drawerController3;
    twopage3.MMDrawerController      = drawerController3;
    twopage3.Onepageviewcontroller   = onepage3;
    twopage3.Threepageviewcontroller = threepage3;
    [drawerController3 setShowsShadow:YES];
    [drawerController3 setRestorationIdentifier:@"MMDrawer"];
    [drawerController3 setMaximumRightDrawerWidth:200.0];
    [drawerController3 setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController3 setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    [drawerController3 setMaximumLeftDrawerWidth:screenWidth];
    [drawerController3 setMaximumRightDrawerWidth:screenWidth];
    [drawerController3
     setDrawerVisualStateBlock:visualStateBlock];
    
    [[MMExampleDrawerVisualStateManager sharedManager]
     setLeftDrawerAnimationType:MMDrawerAnimationTypeSlide];
    [[MMExampleDrawerVisualStateManager sharedManager]
     setRightDrawerAnimationType:MMDrawerAnimationTypeSlide];
    UIViewController *fourNavigationController = [[BaseNavigationController alloc]
                                                  initWithRootViewController:drawerController3];
    //// setup SettingPage
    BaseViewController *fireViewController = [[Setting alloc] init];
    // UIViewController *fireViewController = [[Filterpage alloc] init];
    UIViewController *fireNavigationController = [[UINavigationController alloc]
                                                  initWithRootViewController:fireViewController];
    [tabBarController setViewControllers:@[
                                           firstNavigationController,
                                           secondNavigationController,
                                           thirdNavigationController,
                                           fourNavigationController,
                                           fireNavigationController
                                           ]];
    self.rootViewController = nav3;
    self.viewController = tabBarController;
    
    [self customizeTabBarForController:tabBarController];
    [MyActivityLogModel shared].newLogCount = [MyActivityLogModel shared].newLogCount;
}
- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
    tabBarController.tabBar.translucent = YES;
    if (!tabBarController.shadowView) {
        tabBarController.shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, 1)];
        tabBarController.shadowView.backgroundColor =[UIColor colorWithWhite:0 alpha:0.1];
        [tabBarController.tabBar.backgroundView addSubview:tabBarController.shadowView];
    }
    
    tabBarController.tabBar.contentEdgeInsets = UIEdgeInsetsMake(-1, 0, 0, 0);
    UIImage *finishedImage = [UIImage imageWithColor:RealNavBlueColor];
    UIImage *unfinishedImage = [UIImage imageWithColor:[UIColor clearColor]];
    NSArray *tabBarItemImages = @[
                                  @"ico_tab_search",
                                  @"ico_tab_newsfeed",
                                  @"ico_tab_chats",
                                  @"ico_tab_me",
                                  @"ico_tab_setting"
                                  ];
    NSArray *tabBarItemName = @[
                                @"common__search",
                                @"tab_bar__news_feed",
                                @"tab_bar__chat",
                                @"tab_bar__me",
                                @"tab_bar__settings"
                                ];
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        NSString *tabString = tabBarItemImages[index];
        NSString *tabNameString = tabBarItemName[index];
        [item setBackgroundSelectedImage:finishedImage
                     withUnselectedImage:unfinishedImage];
        UIImage *selectedimage =
        [UIImage imageNamed:[NSString stringWithFormat:@"%@_on", tabString]];
        UIImage *unselectedimage =
        [UIImage imageNamed:[NSString stringWithFormat:@"%@_off", tabString]];
        NSString *titleKey = [NSString stringWithFormat:@"%@", tabNameString];
        NSString *title = JMOLocalizedString(titleKey, nil);
        item.title = title;
        item.titlePositionAdjustment = UIOffsetMake(0, 2);
        item.unselectedTitleAttributes = @{
                                           NSFontAttributeName : [UIFont systemFontOfSize:10],
                                           NSForegroundColorAttributeName : [UIColor lightGrayColor]
                                           };
        item.selectedTitleAttributes = @{
                                         NSFontAttributeName : [UIFont systemFontOfSize:10],
                                         NSForegroundColorAttributeName : [UIColor whiteColor]
                                         };
        
        [item setFinishedSelectedImage:selectedimage
           withFinishedUnselectedImage:unselectedimage];
        
        index++;
    }
    [[tabBarController tabBar]layoutSubviews];
}
- (void)customizeInterface {
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    
    UIImage *backgroundImage = nil;
    NSDictionary *textAttributes = nil;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        backgroundImage = [UIImage imageNamed:@"navigationbar_background_tall"];
        
        textAttributes = @{
                           NSFontAttributeName : [UIFont boldSystemFontOfSize:18],
                           NSForegroundColorAttributeName : [UIColor blackColor],
                           };
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
        backgroundImage = [UIImage imageNamed:@"navigationbar_background"];
        
        textAttributes = @{
                           UITextAttributeFont : [UIFont boldSystemFontOfSize:18],
                           UITextAttributeTextColor : [UIColor blackColor],
                           UITextAttributeTextShadowColor : [UIColor clearColor],
                           UITextAttributeTextShadowOffset :
                               [NSValue valueWithUIOffset:UIOffsetZero],
                           };
#endif
    }
    
    [navigationBarAppearance setBackgroundImage:backgroundImage
                                  forBarMetrics:UIBarMetricsDefault];
    [navigationBarAppearance setTitleTextAttributes:textAttributes];
}

#pragma mark SupportMethod---------------------------------------------------------------------------------------------------------
//// CGcolor
- (UIColor *)Greycolor {
    return [UIColor colorWithRed:(173 / 255.f)
                           green:(175 / 255.f)
                            blue:(177 / 255.f)
                           alpha:1.0];
}
- (UIColor *)Greencolor {
    return [UIColor colorWithRed:(20 / 255.f)
                           green:(226 / 255.f)
                            blue:(160 / 255.f)
                           alpha:1.0];
}
//// Futurafont
- (UIFont *)Futurafont:(CGFloat)size {
    return [UIFont fontWithName:@"Futura" size:size];
}
//// Setborder
- (void)setborder:(UIView *)uiview width:(CGFloat)width {
    [uiview addLeftBorderWithWidth:width andColor:[self Greycolor]];
    [uiview addTopBorderWithHeight:width andColor:[self Greycolor]];
    [uiview addRightBorderWithWidth:width andColor:[self Greycolor]];
    [uiview addBottomBorderWithHeight:width andColor:[self Greycolor]];
}
//// TextFieldsetleftinset
- (void)textfieldsetleftinset:(UITextField *)textfield {
    UIView *paddingView = [[UIView alloc]
                           initWithFrame:CGRectMake(0, 0, 5, textfield.frame.size.height)];
    textfield.leftView = paddingView;
    textfield.leftViewMode = UITextFieldViewModeAlways;
}
//// buttonsetleftinset
- (void)buttonsetleftinset:(UIButton *)button {
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
}
//// checkisnumber
- (BOOL)checkisnumber:(UITextField *)textfield {
    if ([[NSScanner scannerWithString:textfield.text] scanFloat:NULL]) {
        return true;
    } else {
        return false;
    }
}
- (NSString *)checkisnsnumberchangetonsstring:(id)number {
    if ([number isKindOfClass:[NSNumber class]]) {
        return [number stringValue];
    } else {
        return number;
    }
    return [number stringValue];
}
////setupcornerRadius
- (void)setupcornerRadius:(UIImageView *)personalimage {
    personalimage.layer.cornerRadius = personalimage.frame.size.width / 2;
    personalimage.clipsToBounds = YES;
    
    DDLogInfo(@"cornerRadius");
}

- (void)overlayViewController:(UIViewController *)controller onViewController:(UIViewController *)onViewController {
    UIViewController *topMostViewController = self.rootViewController;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        topMostViewController.definesPresentationContext = YES;
        controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    } else {
        topMostViewController.modalPresentationStyle =
        UIModalPresentationCurrentContext;
    }
    [topMostViewController presentViewController:controller
                                        animated:YES
                                      completion:nil];
}

- (void)dismissOverlayerViewController:(BaseViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)dismissOverlayerViewController:(BaseViewController*)controller animated:(BOOL)animated extraAnimations:(void (^)(void))extraAnimations extraCompletion:(void (^ __nullable)(BOOL finished))extraCompletion dismissCompletion:(void (^)(void))dismissCompletion{
    
    [controller dismissViewControllerAnimated:animated extraAnimations:extraAnimations extraCompletion:extraCompletion dismissCompletion:dismissCompletion];
}

-(void)showImagePickerOn:(UIViewController*)viewcontroller{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // init picker
            CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
            if (![self isEmpty:self.hasBeenSelectedUploadPhoto]) {
                picker.selectedAssets=[self.hasBeenSelectedUploadPhoto mutableCopy];
                self.hasBeenSelectedUploadPhoto=nil;
            }
            PHFetchOptions *fetchOptions = [PHFetchOptions new];
            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType == %d", PHAssetMediaTypeImage];
            picker.assetsFetchOptions = fetchOptions;
            //            picker.assetCollectionSubtypes = @[@(PHAssetCollectionSubtypeSmartAlbumUserLibrary),@(PHAssetCollectionSubtypeSmartAlbumScreenshots),@(PHAssetCollectionSubtypeAlbumCloudShared),@(PHAssetCollectionSubtypeSmartAlbumPanoramas)];
            picker.showsEmptyAlbums = NO;
            picker.delegate = viewcontroller;
            
            // to present picker as a form sheet in iPad
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                picker.modalPresentationStyle = UIModalPresentationFormSheet;
            
            // present picker
            UINavigationController *navVc = [[UINavigationController alloc]initWithRootViewController:picker];
            navVc.navigationBarHidden = YES;
            
            [viewcontroller presentViewController:navVc animated:YES completion:^(void){
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            }];
            
        });
    }];
}

////check empty
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

- (void)spotLightSearchHandlingWithUserActivity:(NSUserActivity *)userActivity {
    
    NSString *spotLightSearchQBID = userActivity.userInfo[@"kCSSearchableItemActivityIdentifier"];
    
    // isFromSpotLightSearch
    if (![self isEmpty:spotLightSearchQBID]) {
        
        // isLoggedIn
        if (![self isEmpty:[LoginBySocialNetworkModel shared].myLoginInfo.MemberID]) {
             if (![self checkTopViewControllerIsChatRoomForQBID:spotLightSearchQBID]) {
            
            [self backToTopMainPageSelectedTabBarIndex:2];
            self.spotLightSearchQBID = spotLightSearchQBID;
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSpotLightSearchOpenRightProfilePage object:nil];
             }
            
        }
    }
}

- (RealRemoteNotificationType)getNotificationType:(NSDictionary *)userInfo {
    
    NSInteger pushType = RealRemoteNotificationTypeOther;
    
    if (userInfo[@"category"]) {
        
        pushType = [userInfo[@"category"] integerValue];
        
    } else {
        
        NSDictionary *pushInfo = userInfo[@"aps"];
        
        if (pushInfo) {
            
            DDLogDebug(@"pushInfo = %@", pushInfo);
            
            if (pushInfo[@"category"]) {
                
                pushType = [pushInfo[@"category"] integerValue];
                
                DDLogDebug(@"pushType = %d", (int)pushType);
            }
        }
    }
    
    return pushType;
}

#pragma mark CallServer CheckError-------------------------------------------------------------------------------------------------------------------
- (BOOL)networkConnection {
    Reachability *networkReachability =
    [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        DDLogInfo(@"There IS NO internet connection");
        return NO;
    } else {
        DDLogInfo(@"There IS internet connection");
        return YES;
    }
}
- (void)handleModelReturnErrorShowUIAlertViewByView:(UIView *)view
                                       errorMessage:(NSString *)errorMessage
                                        errorStatus:(int)errorStatus
                                              alert:(NSString *)alert
                                                 ok:(NSString *)ok {
    [MBProgressHUD hideHUDForView:view animated:YES];
    self.disableViewGesture=NO;
    if (errorStatus == 5||errorStatus==40) {
        
        
    }else if (errorStatus == 4) {
        [self totalLogoutErrorMessage:JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil)];
        
    }else if (errorStatus ==26) {
        [self totalLogoutErrorMessage:JMOLocalizedString(@"alert_account_suspended", nil)];
        
    }else if (errorStatus==100) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:JMOLocalizedString(@"sign_up__email_exist", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
    }else if (errorStatus==101) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:JMOLocalizedString(@"login__wrong_password", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
    } else if (errorStatus==102) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:JMOLocalizedString(@"reset_password__email_not_found", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
    }else if (errorStatus==80) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:JMOLocalizedString(@"invite_to_chat__twillio_sms_error", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
    } else if (errorMessage&&errorStatus!=23) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    
}

-(void)handleModelReturnErrorShowUIAlertViewByView:(UIView *)view
                                      errorMessage:(NSString *)errorMessage
                                       errorStatus:(int)errorStatus {
    
    [self handleModelReturnErrorShowUIAlertViewByView:view errorMessage:errorMessage errorStatus:errorStatus alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
    
}

- (UIViewController *)topViewController {
    return[self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewControllerWithRootViewController:
(UIViewController *)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController =
        (UITabBarController *)rootViewController;
        return [self
                topViewControllerWithRootViewController:tabBarController
                .selectedViewController];
    } else if ([rootViewController
                isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController =
        (UINavigationController *)rootViewController;
        return [self
                topViewControllerWithRootViewController:navigationController
                .visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController *presentedViewController =
        rootViewController.presentedViewController;
        return
        [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}


-(void)updateCurrentHudStatus:(NSString *)status{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        
        self.currentHUD.labelText =status;
        
    }];
}
-(BOOL)userNeedShowTutorial:(TutorialType)type{
    BOOL needShow = YES;
    NSString *saveKey = [self getTutorialKeyByType:type];
    NSNumber *memberId = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    NSArray *recordArray = [[NSUserDefaults standardUserDefaults]objectForKey:saveKey];
    
    needShow = ![recordArray containsObject:memberId];
    if(![LoginBySocialNetworkModel shared].myLoginInfo.isNewUser){
        needShow=NO;
        
    }
    return needShow;
}
-(void)userDidReadTutorial:(TutorialType)type{
    NSString *saveKey = [self getTutorialKeyByType:type];
    NSNumber *memberId = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    NSMutableArray *recordArray = [[[NSUserDefaults standardUserDefaults]objectForKey:saveKey] mutableCopy];
    if (!recordArray) {
        recordArray =[[NSMutableArray alloc]init];
    }
    if (![recordArray containsObject:memberId]) {
        [recordArray addObject:memberId];
    }
    [[NSUserDefaults standardUserDefaults]setObject:recordArray forKey:saveKey];
}

-(NSString*)getTutorialKeyByType:(TutorialType)type{
    NSString *saveKey = @"";
    switch (type) {
        case TutorialTypeSearch:
            saveKey = @"Tutorial_Search";
            break;
        case TutorialTypeCreatePost:
            saveKey = @"Tutorial_CreatePost";
            break;
        case TutorialTypeInviteToFollow:
            saveKey = @"Tutorial_InviteToFollow";
            break;
        default:
            break;
    }
    return saveKey;
}
-(void)setupLanguageFromSystemSettingSelectSystemLanguage{
    NSString * languageCode=[[SystemSettingModel shared] serverReturnLanguageCodeChangeToAppleFormat:[SystemSettingModel shared].selectSystemLanguageList.Code];
    if ( [[LanguagesManager sharedInstance] isAnAvailableLanguage:languageCode]) {
        [[LanguagesManager sharedInstance] setLanguage:languageCode];
        NSArray* languages = [NSArray arrayWithObjects:languageCode, nil];
        [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    } else{
        
        
        [[LanguagesManager sharedInstance] setLanguage:@"en"];
        NSArray* languages = [NSArray arrayWithObjects:@"en", nil];
        [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
}

-(void)showUpdateAlert:(NSString*)updateURL forceUpdate:(BOOL)forceUpdate{
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    if (forceUpdate || ![currentVersion isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:kNSUserDefaultKeyUpdateVersion]]) {
        if (!self.updateAlert.presentingViewController) {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleCancel handler:nil];
            self.updateAlert = [UIAlertController alertControllerWithTitle:nil message:JMOLocalizedString(@"app_update__alert", nil) preferredStyle:UIAlertControllerStyleAlert];
            if (!forceUpdate) {
                [self.updateAlert addAction:ok];
            }
            if ([updateURL valid]) {
                UIAlertAction *update = [UIAlertAction actionWithTitle:JMOLocalizedString(@"common__update", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    NSURL *url = [NSURL URLWithString:updateURL];
                    if ([[UIApplication sharedApplication]canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL:url];
                    }
                }];
                [self.updateAlert addAction:update];
            }
            [self.topViewController presentViewController:self.updateAlert animated:YES completion:nil];
            [[NSUserDefaults standardUserDefaults]setObject:currentVersion forKey:kNSUserDefaultKeyUpdateVersion];
        }
    }
}
//-(void)handleSearchableItem:(id)item{
//    NSString *dominInde
//    if (<#condition#>) {
//        <#statements#>
//    }
//}

-(void)backToTopMainPageSelectedTabBarIndex:(int)selectedTabBarIndex{
    [[self getTabBarController]hideLoading];
    UIViewController *presentedViewController = [self getTabBarController].selectedViewController.presentedViewController;
    void (^completion)() = ^ {
        [[self getTabBarController] setSelectedIndex:selectedTabBarIndex];
        UINavigationController * navigationController= (UINavigationController *)[[self getTabBarController] selectedViewController];
        if ([navigationController.topViewController isKindOfClass:[MMDrawerController class]]) {
            MMDrawerController * mmDrawerViewController= (MMDrawerController *)navigationController.topViewController;
            [mmDrawerViewController closeDrawerAnimated:YES completion:^(BOOL finished) {
                [[[self getTabBarController].viewControllers objectAtIndex:selectedTabBarIndex]popToRootViewControllerAnimated:YES];
                [[self getTabBarController] setTabBarHidden:NO];
            }];
            
        }else if ([navigationController.topViewController isKindOfClass:[JSQMessagesViewController class]]) {
            [[[self getTabBarController].viewControllers objectAtIndex:selectedTabBarIndex]popToRootViewControllerAnimated:YES];
            [[self getTabBarController] setTabBarHidden:NO];
        }
    };
    if(presentedViewController){
        BOOL handled = NO;
        BaseNavigationController * navVC = nil;
        if ([presentedViewController isKindOfClass:[BaseNavigationController class]]) {
            navVC = (BaseNavigationController *) presentedViewController;
            if ([navVC.type isEqualToString:@"posting"]) {
                handled = YES;
                UIViewController *topViewController = navVC.topViewController;
                if ([topViewController isKindOfClass:[BaseViewController class]]) {
                    BaseViewController *baseVC = (BaseViewController*) topViewController;
                    [baseVC dismissPostingViewWithCompletionBlock:^(BOOL finished) {
                        if(finished){
                            completion();
                        }
                    }];
                }
            }
        }
        if (!handled) {
            if (presentedViewController.presentedViewController ) {
                [presentedViewController.presentedViewController dismissViewControllerAnimated:YES completion:^{
                    [presentedViewController dismissViewControllerAnimated:YES completion:completion];
                }];
            }
            [presentedViewController dismissViewControllerAnimated:YES completion:completion];
        }
    }else{
        completion();
    }
}

-(void)changeToPreferredLanguage{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *language = [locale objectForKey:NSLocaleLanguageCode];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    
    
    NSString *language2 = [locale objectForKey:NSLocaleIdentifier];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    if ([languageCode rangeOfString:@"zh"].location != NSNotFound)
    {
        if ([language2 rangeOfString:@"zh-Hans"].location != NSNotFound)
        {
            // Stuff happens
            languageCode=@"zh-Hans";
        }else{
            languageCode=@"zh-Hant";
            
            
        }
    }
    if ([languageCode isEqualToString:@"en"]||[languageCode isEqualToString:@"fr"]||[languageCode isEqualToString:@"de"]||[languageCode isEqualToString:@"ko"]||[languageCode isEqualToString:@"es"]||[languageCode isEqualToString:@"zh-Hant"]||[languageCode isEqualToString:@"zh-Hans"]) {
        
    }else{
        languageCode=@"en";
        
    }
    
    self.languageCode = languageCode;
    [SystemSettingModel shared].selectSystemLanguageList=[SystemLanguageList new];
    [SystemSettingModel shared].selectSystemLanguageList.Code =self.languageCode;
    //    [SystemSettingModel shared].selectSystemLanguageList.Code =@"en";
    for (SystemLanguageList *systemlanguage in[SystemSettingModel shared].SystemSettings.SystemLanguageList) {
        if ([[SystemSettingModel shared].selectSystemLanguageList.Code
             isEqualToString:systemlanguage.Code]) {
            [SystemSettingModel shared].selectSystemLanguageList =systemlanguage;
        }
        
    }
    
    [self refreshAppLanguage];
}

-(void)refreshAppLanguage{
    
    [[AppDelegate getAppDelegate] setupLanguageFromSystemSettingSelectSystemLanguage];
    [[AppDelegate getAppDelegate] customizeTabBarForController:(RDVTabBarController *)[AppDelegate getAppDelegate].viewController];
    [[SystemSettingModel shared] setupMetricAccordingToLanguage];
    [[SystemSettingModel shared] setupPropertyTypeAccordingToLanguage];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage object:nil];
}

#pragma mark - Accessors

- (Branch *)branchInstance {
    
    if (!_branchInstance) {
        
        _branchInstance = [Branch getInstance:kBranchKey];
    }
    
    return _branchInstance;
}
- (void)setUpBlueAndWhiteTitleFontSizeUILabel:(UILabel*)label{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *language = [locale objectForKey:NSLocaleLanguageCode];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *language2 = [locale objectForKey:NSLocaleIdentifier];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    if ([languageCode rangeOfString:@"zh"].location != NSNotFound)
    {
        if ([language2 rangeOfString:@"zh-Hans"].location != NSNotFound)
        {
            // Stuff happens
            languageCode=@"zh-Hans";
        }else{
            languageCode=@"zh-Hant";
            
            
        }
    }
    
    if ([languageCode isEqualToString:@"fr"]||[languageCode isEqualToString:@"de"]||[languageCode isEqualToString:@"es"]) {
        
        [label setFont:  [UIFont systemFontOfSize:29]];
    }else{
        [label setFont:  [UIFont systemFontOfSize:40]];
        
    }
    
}

- (NSString*)getCurrentAgentProfileMemberName {
    AgentProfile * agentProfile= [AppDelegate getAppDelegate].currentAgentProfile;
    NSString * memberName = @"";
    if (agentProfile.MemberName) {
        memberName=agentProfile.MemberName;
    }
    DDLogDebug(@"CurrentAgentProfilememberName %@",memberName);
    return memberName;
}

@end
