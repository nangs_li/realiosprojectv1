//
//  RealSelectTableCell.h
//  productionreal2
//
//  Created by Alex Hung on 2/11/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoReSizeTableCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UIImageView *indicatorImage;
@property (nonatomic,strong) IBOutlet UIView *disableView;
@property (nonatomic,assign) BOOL enable;
@property (nonatomic,assign) BOOL disableSelection;

-(void)configureWithTitle:(NSString*)title indicatorImage:(UIImage*)image;
@end
