//
//  BaseNavigationController.h
//  productionreal2
//
//  Created by Alex Hung on 23/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController
@property (nonatomic,assign) NSString *type;
@end
