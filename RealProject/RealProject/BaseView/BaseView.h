//
//  BaseView.h
//  productionreal2
//
//  Created by Alex Hung on 12/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseView : UIView
@property (nonatomic,strong) IBOutlet UIView *targetView;
@end
