//
//  BaseViewController.h
//  FWD
//
//  Created by Raymond Chan on 31/3/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "SCLAlertView.h"
#import "HandleServerReturnString.h"
#import "AgentProfile.h"
#import "FXBlurView.h"
#import "REPhonebookManager.h"

typedef enum {
    BaseViewActionEdit =0
}BaseViewAction;


@protocol BaseViewDelegate <NSObject>

- (void)BaseViewControllerDidAction:(BaseViewAction)action withRef:(id)ref;

@end

@interface BaseViewController
: UIViewController<MBProgressHUDDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property(weak
          , nonatomic) AppDelegate *delegate;

@property(weak, nonatomic) id<BaseViewDelegate>baseViewDelegate;
@property(nonatomic) BOOL showNavRightButton;
@property(strong, nonatomic) MBProgressHUD *hud;
#pragma mark d object, jsonstring
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(strong, nonatomic) AgentProfile *AgentProfile;
@property (strong, nonatomic) UIView *blurContainerView;
@property (strong, nonatomic) UIImageView *blurImageView;
@property (nonatomic, assign) BOOL visible;
@property (strong, nonatomic) FXBlurView *blurView;
@property (strong, nonatomic) UIView *blurBackgroundView;
@property (nonatomic, assign) BOOL isBlurring;
@property (nonatomic, assign) BOOL previousToolBarIsShown;
@property (nonatomic,assign) BOOL previousNavBarIsHidden;
@property (nonatomic,assign) BOOL previousPageControlIsHidden;
@property (nonatomic,weak) BaseViewController *underLayViewController;
@property (nonatomic,strong) IBOutlet UIView *navBarContentView;
@property (nonatomic,strong) IBOutlet UIView *spotlightContainerView;
@property (nonatomic,strong) IBOutlet UIView *spotlightView;
@property (nonatomic,strong) CAShapeLayer *spotlightLayer;
@property(nonatomic,assign) BOOL needCustomBackground;
@property (nonatomic,strong) NSMutableArray *imageOperations;

#pragma mark - Public Methods

- (void)showDXAlertView;
- (void)btnBackPressed:(id)sender;
- (void)btnNavRightPressed:(id)sender;
- (void)btnSharePressed:(id)sender;
- (void)btnEditPressed:(id)sender;
- (void)btnSavePressed:(id)sender;
- (void)listSubviewsOfView:(UIView *)view;
- (void)loadingAndStopLoadingAfterSecond:(int)second;
- (void)showLoading;
- (void)hideLoading;
- (void)showLoadingHUD;
- (void)showLoadingHUDWithProgress:(NSString*)progress;
- (void)showLoadingHUDWithTitleText:(NSString *)titleText titleTextFont:(UIFont *)titleTextFont titleTextColor:(UIColor *)titleTextColor detailText:(NSString *)detailText detailTextFont:(UIFont *)detailTextFont detailTextColor:(UIColor *)detailTextColor windowColor:(UIColor *)windowColor;
- (void)hideLoadingHUD;
- (void)updateHUD:(NSString*)progress;
- (BOOL)isEmpty:(id)thing;
- (void)dismissPostingViewWithCompletionBlock:(void (^ __nullable)(BOOL finished))completion;
- (void)setUpFixedLabelTextAccordingToSelectedLanguage;


- (void)dismissViewControllerAnimated:(BOOL)flag extraAnimations:(void (^)(void))extraAnimations extraCompletion:(void (^ __nullable)(BOOL finished))extraCompletion dismissCompletion:(void (^)(void))dismissCompletion;
- (void)showBlurView:(BOOL)show extraAnimations:(void (^)(void))animations extraCompletion:(void (^ __nullable)(BOOL finished))extraCompletion animated:(BOOL)animated dismissCompletion:(void (^)(void))dismissCompletion;
- (void)showBlurView:(BOOL)show withAnimation:(BOOL)animation;
- (void)showSpotlightInRect:(CGRect)spotlightRect;
- (void)hideSpotlight;

- (IBAction)dismissPostingViewWithOption;
- (IBAction)showOptionActionSheet:(id)sender;
- (void) showPageControl:(BOOL)show animated:(BOOL)animated;
- (void) showAlertWithTitle:(NSString*)title message:(NSString*)message;
- (void) showAlertWithTitle:(NSString *)title message:(NSString *)message withAction:(UIAlertAction *)action;
- (void) showAlertWithTitle:(NSString *)title message:(NSString *)message withActions:(NSArray *)actions;
- (void)moveToMainPage;
- (void)checkContactListRequestAccess;
- (void)registerQBWithInfo:(LoginInfo*)loginInfo;
- (void)setupTextFieldCheckError;
- (void)setupKeyBoardDoneBtnAction;
- (void)cancelImageOperation;
- (void)setupView;
- (BOOL)notError:(NSError *)error view:(UIView *)view;
@end
