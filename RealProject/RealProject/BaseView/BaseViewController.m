//
//  BaseViewController.m
//  FWD
//
//  Created by Raymond Chan on 31/3/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "BaseViewController.h"
#import "DXAlertView.h"
#import "RealUtility.h"
#import "TransparentNavControllerDelegate.h"
#import "Sharepage.h"
#import "ReportProblemModel.h"
#import "ApartmentDetailViewController.h"
#import "AgentProfileViewController.h"
#import "ShareViewController.h"
#import "Twopage.h"
#import "NewsFeedtwopage.h"
#import "Setting.h"
#import "Editprofilehaveprofile.h"
#import "DialogsViewController.h"
#import "Twopage.h"
#import "NewsFeedtwopage.h"
#import "MeProfileViewController.h"
#import "Setting.h"
#import "DBLogInfo.h"
#import "DBChatSetting.h"
#import "DeepLinkActionHanderModel.h"
#import "NewsFeedAgentprofilesModel.h"
#import "Uploadphoto.h"
#import "SearchTutorialViewController.h"
#import <Google/Analytics.h>
#import "SearchAgentProfileModel.h"
#import "ChatDialogModel.h"
#import "DialogPageAgentProfilesModel.h"
#import "RealPhoneBookShareViewController.h"
#import "RealAPIClient.h"

@interface BaseViewController ()
@end

@implementation BaseViewController
// init

- (AppDelegate*)delegate{
    return kAppDelegate;
}

#pragma mark - View lifecycle---------------------------------------------------------------------------------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _delegate = [AppDelegate getAppDelegate];
    _hud.labelText = JMOLocalizedString(@"alert__Initializing", nil);
    [self setShowNavRightButton:NO];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage
                                              object:nil];
    [self setupTextFieldCheckError];
    [self setupKeyBoardDoneBtnAction];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLayoutSubviews {
    DDLogInfo(@"viewDidLayoutSubviews currentview:-->[%@]", [self class]);
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[self className]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    self.visible = YES;
    if (self.underLayViewController && !self.underLayViewController.isBlurring) {
        if (!self.presentedViewController) {
            self.previousToolBarIsShown =[self.delegate getTabBarController].tabBarHidden;
        }
        [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
        [self.underLayViewController showBlurView:YES withAnimation:YES];
    }
    
    if ([self isKindOfClass:[Setting class]] || [self isKindOfClass:[DialogsViewController class]] || [self isKindOfClass:[DialogsViewController class]]) {
        [[self.delegate getTabBarController]setPageControlHidden:YES animated:NO];
    }else{
    }
    [self.navigationController setNavigationBarHidden:YES];
    if ([self isKindOfClass:[Twopage class]]||[self isKindOfClass:[NewsFeedtwopage class]]|| [self isKindOfClass:[DialogsViewController class]] || [self isKindOfClass:[MeProfileViewController class]] || [self isKindOfClass:[Setting class]] ) {
        [[self.delegate getTabBarController]setNavBarHidden:NO animated:NO];
    }else{
        //    [[self.delegate getTabBarController]setNavBarHidden:NO animated:NO];
    }
    
    DDLogInfo(@"viewWillAppear currentview:-->[%@]", [self class]);
    [self setUpFixedLabelTextAccordingToSelectedLanguage];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navBarContentView.alpha = 0;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self showTutorialIfNeed];
    self.visible = YES;
    DDLogInfo(@"viewDidAppear currentview:-->[%@]", [self class]);
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self showBlurView:NO withAnimation:NO];
    self.visible = NO;
    
}

-(void)showTutorialIfNeed{
    if ([self isKindOfClass:[Twopage class]] && [kAppDelegate userNeedShowTutorial:TutorialTypeSearch]) {
        
        SearchTutorialViewController *searchTutorial =[[SearchTutorialViewController alloc]init];
        [kAppDelegate overlayViewController:searchTutorial onViewController:self];
    }else if ([self isKindOfClass:[Uploadphoto class]] && [kAppDelegate userNeedShowTutorial:TutorialTypeCreatePost]){
        
    }
}

- (void)dismissViewControllerAnimated:(BOOL)animated extraAnimations:(void (^)(void))extraAnimations extraCompletion:(void (^ __nullable)(BOOL finished))extraCompletion dismissCompletion:(void (^)(void))dismissCompletion {
    if (self.underLayViewController && !self.presentedViewController) {
        [[self.delegate getTabBarController]setTabBarHidden:self.previousToolBarIsShown animated:YES];
        [self.underLayViewController showBlurView:NO extraAnimations:extraAnimations completion:extraCompletion animated:animated ];
    }
    [super dismissViewControllerAnimated:animated completion:dismissCompletion];
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion{
    [self dismissViewControllerAnimated:flag extraAnimations:nil extraCompletion:nil dismissCompletion:completion];
}
- (void)listSubviewsOfView:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    // Return if there are no subviews
    if ([subviews count] == 0) return;  // COUNT CHECK LINE
    
    for (UIView *subview in subviews) {
        // Do what you want to do with the subview
        DDLogDebug(@"subview-->%@", subview);
        
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}
#pragma mark - View lifecycle---------------------------------------------------------------------------------------------------------------------

#pragma mark - Form Actions---------------------------------------------------------------------------------------------------------------------
- (void)btnBackPressed:(id)sender {
    // previous page
    if(self.needCustomBackground){
        self.navigationController.delegate = self;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)btnNavRightPressed:(id)sender {
}
- (void)btnSharePressed:(id)sender {
    Sharepage *UIViewController =
    [[Sharepage alloc] initWithNibName:@"Sharepage" bundle:nil];
    CATransition *transition = [CATransition animation];
    transition.duration = ANIMATION_DURATION;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    
    [self.navigationController.view.layer addAnimation:transition
                                                forKey:kCATransition];
    [self.navigationController pushViewController:UIViewController animated:NO];
}
- (void)btnEditPressed:(id)sender {
}
- (void)btnSavePressed:(id)sender {
}
#pragma mark - Form Actions---------------------------------------------------------------------------------------------------------------------

#pragma mark - DXAlertView
- (void)showDXAlertView {
}
#pragma mark - loading
- (void)loadingAndStopLoadingAfterSecond:(int)second {
    [self showLoadingHUD];
    
    dispatch_time_t popTime =
    dispatch_time(DISPATCH_TIME_NOW, second * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        // Do something...
        [self hideLoadingHUD];
    });
}

-(void)showLoadingHUD{
    [self showLoadingHUDWithProgress:nil];
}

-(void)showLoadingHUDWithProgress:(NSString *)progress{
    [self hideLoadingHUD];
    self.delegate.currentHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.delegate.currentHUD.delegate = self;
    if (progress) {
        self.delegate.currentHUD.labelText = progress;
    }else{
        self.delegate.currentHUD.labelText =JMOLocalizedString(@"alert__Initializing", nil);
    }
    self.delegate.disableViewGesture=YES;
    [[[AppDelegate getAppDelegate]getTabBarController]customNavBarController].agentProfileDetailBackButton.userInteractionEnabled=NO;
}

- (void)showLoadingHUDWithTitleText:(NSString *)titleText titleTextFont:(UIFont *)titleTextFont titleTextColor:(UIColor *)titleTextColor detailText:(NSString *)detailText detailTextFont:(UIFont *)detailTextFont detailTextColor:(UIColor *)detailTextColor windowColor:(UIColor *)windowColor {
    
    [self hideLoadingHUD];
    self.delegate.currentHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.delegate.currentHUD.delegate = self;
    
    if (titleText) {
        
        self.delegate.currentHUD.labelText =titleText;
        
    }else{
        
        self.delegate.currentHUD.labelText =JMOLocalizedString(@"alert__Initializing", nil);
        
    }
    
    if (titleTextFont) {
        
        self.delegate.currentHUD.labelFont = titleTextFont;
        
    }
    
    if (titleTextColor) {
        
         self.delegate.currentHUD.labelColor = titleTextColor;
        
    }
    
    if (detailText) {
        
        self.delegate.currentHUD.detailsLabelText = detailText;
        
    }
    
    if (detailTextFont) {
        
        self.delegate.currentHUD.detailsLabelFont = detailTextFont;
        
    }
    
    if (detailTextColor) {
        
        self.delegate.currentHUD.detailsLabelColor = detailTextColor;
        
    }
    
    if (windowColor) {
        
        self.delegate.currentHUD.color = windowColor;
        
    }
    
    self.delegate.disableViewGesture=YES;
    
    [[[AppDelegate getAppDelegate]getTabBarController]customNavBarController].agentProfileDetailBackButton.userInteractionEnabled=NO;
    
}


-(void)hideLoadingHUD{
    if (self.delegate.currentHUD) {
        [self.delegate.currentHUD hide:YES];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    self.delegate.disableViewGesture=NO;
    [[[AppDelegate getAppDelegate]getTabBarController]customNavBarController].agentProfileDetailBackButton.userInteractionEnabled=YES;
}

-(void)updateHUD:(NSString *)progress{
    if (self.delegate.currentHUD) {
        self.delegate.currentHUD.labelText = progress;
    }
}

-(void)showLoading{
    self.delegate.disableViewGesture=YES;
    [[self.delegate getTabBarController]showLoading];
    
}

-(void)hideLoading{
    [[self.delegate getTabBarController]hideLoading];
    self.delegate.disableViewGesture=NO;
}
#pragma check empty
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    
}

-(void)showBlurView:(BOOL)show extraAnimations:(void (^)(void))addons completion:(void (^ __nullable)(BOOL finished))extraCompletion animated:(BOOL)animated{
    
    if (self.isBlurring == show) {
        return;
    }
    UIView *blurSuperView = nil;
    CGRect blurFrame = CGRectZero;
    if (self.navigationController) {
        blurFrame = self.navigationController.view.bounds;
        blurSuperView = self.navigationController.view;
    }else{
        blurFrame = self.view.bounds;
        blurSuperView = self.view;
    }
    
    blurFrame =self.delegate.window.bounds;
    if (!self.blurContainerView) {
        self.blurContainerView = [[UIView alloc]initWithFrame:blurFrame];
        self.blurContainerView.hidden = YES;
        self.blurContainerView.backgroundColor =[UIColor clearColor];
        [blurSuperView addSubview:self.blurContainerView];
    }else{
        self.blurContainerView.frame = blurFrame;
    }
    
    if (!self.blurImageView) {
        self.blurImageView = [[UIImageView alloc]initWithFrame:blurFrame];
        self.blurImageView.clipsToBounds = YES;
        self.blurImageView.backgroundColor =[UIColor clearColor];
        [self.blurContainerView addSubview:self.blurImageView];
    }
    
    if(!self.blurView){
        self.blurView = [[FXBlurView alloc]initWithFrame:blurFrame];
        self.blurView.iterations = 5;
        self.blurView.dynamic = NO;
        self.blurView.tintColor = [UIColor clearColor];
        
        [self.blurContainerView addSubview:self.blurView];
    }
    
    if (!self.blurBackgroundView) {
        self.blurBackgroundView = [[UIView alloc]initWithFrame:self.blurView.bounds];
        self.blurBackgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
        [self.blurView addSubview:self.blurBackgroundView];
    }
    
    if (!self.isBlurring) {
        self.blurImageView.image = [UIImage imageWithView:self.delegate.window];
    }
    
    if (show) {
        self.isBlurring = YES;
        [UIView performWithoutAnimation:^{
            self.previousNavBarIsHidden = [self.delegate getTabBarController].navBarHidden;
            self.previousPageControlIsHidden = [self.delegate getTabBarController].pageControlHidden;
            [[self.delegate getTabBarController]setNavBarHidden:YES animated:NO];
            [[self.delegate getTabBarController]setPageControlHidden:YES animated:NO];
        }];
        self.blurBackgroundView.alpha = 0.0f;
        self.blurView.blurRadius = 0;
        self.blurContainerView.hidden = NO;
    }else{
        self.isBlurring = NO;
        self.blurBackgroundView.alpha = 1.0f;
        self.blurView.blurRadius = MaxBlurRadius;
    }
    
    
    void (^animation)() = ^{
        if (show) {
            self.blurView.blurRadius = MaxBlurRadius;
            self.blurBackgroundView.alpha = 1.0f;
        }else{
            self.blurView.blurRadius = 0;
            self.blurBackgroundView.alpha = 0.0f;
        }
        if (addons) {
            addons();
        }
    };
    
    void (^completion)(BOOL) = ^(BOOL finished){
        if(finished){
            if (show) {
                
            }else{
                self.blurContainerView.hidden = YES;
                
                [[self.delegate getTabBarController]setNavBarHidden:self.previousNavBarIsHidden animated:NO];
                [[self.delegate getTabBarController]setPageControlHidden:self.previousPageControlIsHidden animated:NO];
                [self resetBlurView];
            }
            if (extraCompletion) {
                extraCompletion(finished);
            }
        }
    };
    if (animated) {
        [UIView animateWithDuration:0.3 animations:animation completion:completion];
    }else{
        animation();
        completion(YES);
    }
}
-(void)showBlurView:(BOOL)show withAnimation:(BOOL)animation{
    [self showBlurView:show extraAnimations:nil completion:nil animated:animation];
    //    if (self.isBlurring == show) {
    //        return;
    //    }
    //    UIView *blurSuperView = nil;
    //    CGRect blurFrame = CGRectZero;
    //    if (self.navigationController) {
    //        blurFrame = self.navigationController.view.bounds;
    //        blurSuperView = self.navigationController.view;
    //    }else{
    //        blurFrame = self.view.bounds;
    //        blurSuperView = self.view;
    //    }
    //
    //    blurFrame =self.delegate.window.bounds;
    //    if (!self.blurContainerView) {
    //        self.blurContainerView = [[UIView alloc]initWithFrame:blurFrame];
    //        self.blurContainerView.hidden = YES;
    //        self.blurContainerView.backgroundColor =[UIColor clearColor];
    //        [blurSuperView addSubview:self.blurContainerView];
    //    }else{
    //        self.blurContainerView.frame = blurFrame;
    //    }
    //
    //    if (!self.blurImageView) {
    //        self.blurImageView = [[UIImageView alloc]initWithFrame:blurFrame];
    //        self.blurImageView.clipsToBounds = YES;
    //        self.blurImageView.backgroundColor =[UIColor clearColor];
    //        [self.blurContainerView addSubview:self.blurImageView];
    //    }
    //
    //    if(!self.blurView){
    //        self.blurView = [[FXBlurView alloc]initWithFrame:blurFrame];
    //        self.blurView.iterations = 5;
    //        self.blurView.dynamic = NO;
    //        self.blurView.tintColor = [UIColor clearColor];
    //
    //        [self.blurContainerView addSubview:self.blurView];
    //    }
    //
    //    if (!self.blurBackgroundView) {
    //        self.blurBackgroundView = [[UIView alloc]initWithFrame:self.blurView.bounds];
    //        self.blurBackgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    //        [self.blurView addSubview:self.blurBackgroundView];
    //    }
    //
    //    if (!self.isBlurring) {
    //        self.blurImageView.image = [UIImage imageWithView:self.delegate.window];
    //    }
    //
    //    if (show) {
    //        self.isBlurring = YES;
    //        [UIView setAnimationsEnabled:NO];
    //        [[self.delegate getTabBarController]setNavBarHidden:YES animated:NO];
    //        [UIView setAnimationsEnabled:YES];
    //        if (animation) {
    //            self.blurBackgroundView.alpha = 0.0f;
    //            self.blurView.blurRadius = 0;
    //            self.blurContainerView.hidden = NO;
    //            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
    //                self.blurView.blurRadius = MaxBlurRadius;
    //                self.blurBackgroundView.alpha = 1.0f;
    //            }completion:^(BOOL finished) {
    //                if (finished) {
    //                }
    //            }];
    //        }else{
    //            self.blurView.blurRadius = MaxBlurRadius;
    //        }
    //    }else{
    //        self.isBlurring = NO;
    //        if (animation) {
    //            self.blurBackgroundView.alpha = 1.0f;
    //            self.blurView.blurRadius = MaxBlurRadius;
    //            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
    //                self.blurView.blurRadius = 0;
    //                self.blurBackgroundView.alpha = 0.0f;
    //            }completion:^(BOOL finished) {
    //                if (finished) {
    //                    self.blurContainerView.hidden = YES;
    //                    [[self.delegate getTabBarController]setNavBarHidden:NO animated:NO];
    //                    [self resetBlurView];
    //                }
    //            }];
    //        }else{
    //            self.blurView.blurRadius =0;
    //            self.blurContainerView.hidden = YES;
    //        }
    //    }
}

-(void)resetBlurView{
    if(self.blurImageView){
        self.blurImageView = nil;
        [self.blurImageView removeFromSuperview];
    }
    if(self.blurView){
        self.blurView = nil;
        [self.blurView removeFromSuperview];
    }
    
    if(self.blurBackgroundView){
        self.blurBackgroundView = nil;
        [self.blurBackgroundView removeFromSuperview];
    }
    
    if(self.blurContainerView){
        self.blurContainerView = nil;
        [self.blurContainerView removeFromSuperview];
    }
}
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC{
    TransparentNavControllerDelegate *transparentNav = [[TransparentNavControllerDelegate alloc]init];
    transparentNav.navigationControllerOperation = operation;
    return transparentNav;
}

- (void)BaseViewControllerDidAction:(BaseViewAction)action withRef:(id)ref{
    if (action == BaseViewActionEdit) {
        if ([ref isKindOfClass:[NSString class]]) {
            NSString *refString  = ref;
            if ([refString isEqualToString:@"experience"]) {
                
            }else if([refString isEqualToString:@"language"]){
                
            }else if([refString isEqualToString:@"specialty"]){
                
            }else if([refString isEqualToString:@"pastClosing"]){
                
            }
        }
    }
}


-(void)showSpotlightInRect:(CGRect)spotlightRect{
    [self hideSpotlight];
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) ];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRect:spotlightRect];
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    
    self.spotlightLayer= [CAShapeLayer layer];
    self.spotlightLayer.path = path.CGPath;
    self.spotlightLayer.rasterizationScale = [[UIScreen mainScreen] scale];
    self.spotlightLayer.shouldRasterize = YES;
    self.spotlightLayer.fillRule = kCAFillRuleEvenOdd;
    self.spotlightLayer.fillColor = [UIColor whiteColor].CGColor;
    self.spotlightLayer.opacity = 0.0;
    self.view.clipsToBounds = YES;
    [self.view.layer addSublayer:self.spotlightLayer];
}

-(void)hideSpotlight{
    if (self.spotlightLayer) {
        [self.spotlightLayer removeFromSuperlayer];
    }
}
- (void)dismissPostingViewWithCompletionBlock:(void (^ __nullable)(BOOL finished))completion{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = nil;
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                                destructiveButtonTitle:JMOLocalizedString(@"common__discard", nil)
                                     otherButtonTitles:JMOLocalizedString(@"common__save", nil),nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    actionSheet.didDismissBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            /* Discard */
            [[AgentListingModel shared]resetAllAgentListingPartOnlyLocalData];
            NSDictionary* userInfo = @{@"didEdit": @(0)};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateAgentListing" object:userInfo];
            [self dismissViewControllerAnimated:YES completion:^{
                if (completion) {
                    completion(YES);
                }
            }];
        }else if (buttonIndex == 1){
            /* Save */
            NSDictionary* userInfo = @{@"didEdit": @(0)};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateAgentListing" object:userInfo];
            [self dismissViewControllerAnimated:YES completion:^{
                if (completion) {
                    completion(YES);
                }
            }];
        }else if (buttonIndex == 2){
            /* Cancel */
            if (completion) {
                completion(NO);
            }        }
    };
    
    [actionSheet showInView:self.view];
}

- (IBAction)dismissPostingViewWithOption{
    [self dismissPostingViewWithCompletionBlock:nil];
}
- (IBAction)showOptionActionSheet:(id)sender {
    UIActionSheet *as = nil;
    if ([self isKindOfClass:[ApartmentDetailViewController class]]) {
        as = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:nil
                                cancelButtonTitle:JMOLocalizedString(@"reportpage__cancel", nil)
                           destructiveButtonTitle:JMOLocalizedString(@"reportpage__report", nil)
                                otherButtonTitles:nil];
    }else if([self isKindOfClass:[AgentProfileViewController class]]){
        as = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:nil
                                cancelButtonTitle:JMOLocalizedString(@"reportpage__cancel", nil)
                           destructiveButtonTitle:JMOLocalizedString(@"reportpage__report", nil)
                                otherButtonTitles:nil];
    }
    as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    as.didDismissBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if ([self.delegate networkConnection]) {
            if ([self isKindOfClass:[ApartmentDetailViewController class]]) {
                if (buttonIndex == 0) {
                    [self reportThisListingToServer];
                }else if(buttonIndex ==1){
                    //                    ShareViewController *shareVc = [[ShareViewController alloc]
                    //                                                    initWithNibName:@"ShareViewController"
                    //                                                    bundle:nil];
                    //                    shareVc.AgentProfile = self.AgentProfile;
                    //                    shareVc.shareType = ShareListing;
                    //                    shareVc.underLayViewController = self;
                    //                    [self.delegate overlayViewController:shareVc onViewController:self];
                }
            }else if([self isKindOfClass:[AgentProfileViewController class]]){
                if (buttonIndex == 0) {
                    [self reportThisAgentToServer];
                }
            }
            
        }
    };
    
    [as showInView:self.view];
}

- (void)reportThisAgentToServer {
    [self showLoadingHUD];
    [[ReportProblemModel shared] callReportProblemApiByProblemType:@"2"
                                                        toMemberID:[NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
                                                       toListingID:@""
                                                       description:@""
                                                           success:^(id responseObject) {
                                                               
                                                               [[[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"reportpage__thanks_for", nil)
                                                                                           message:nil
                                                                                          delegate:self
                                                                                 cancelButtonTitle:JMOLocalizedString(JMOLocalizedString(@"alert_ok", nil),nil)
                                                                                 otherButtonTitles:nil] show];
                                                               
                                                               [self hideLoadingHUD];
                                                               
                                                           }
                                                           failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                                     NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                                                                     NSString *ok) {
                                                               [self hideLoadingHUD];
                                                               [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                                                             errorMessage:errorMessage
                                                                                                              errorStatus:errorStatus
                                                                                                                    alert:alert
                                                                                                                       ok:ok];
                                                           }];
}

- (void)reportThisListingToServer {
    [self showLoadingHUD];
    [[ReportProblemModel shared] callReportProblemApiByProblemType:@"4"
                                                        toMemberID:[NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
                                                       toListingID:[NSString
                                                                    stringWithFormat:@"%d", self.AgentProfile.AgentListing
                                                                    .AgentListingID]
                                                       description:@""
                                                           success:^(id responseObject) {
                                                               
                                                               [[[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"reportpage__thanks_for", nil)
                                                                                           message:nil
                                                                                          delegate:self
                                                                                 cancelButtonTitle:JMOLocalizedString(JMOLocalizedString(@"alert_ok", nil),nil)
                                                                                 otherButtonTitles:nil] show];
                                                               
                                                               [self hideLoadingHUD];
                                                               
                                                           }
                                                           failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                                     NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                                                                     NSString *ok) {
                                                               [self hideLoadingHUD];
                                                               [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                                                             errorMessage:errorMessage
                                                                                                              errorStatus:errorStatus
                                                                                                                    alert:alert
                                                                                                                       ok:ok];
                                                           }];
}

-(void)showPageControl:(BOOL)show animated:(BOOL)animated{
    if (self.visible) {
        [[self.delegate getTabBarController]setPageControlHidden:!show animated:animated];
        [[self.delegate getTabBarController]adjustNavBarHeight:show ? RealNavBarHeightWithPageControl : RealNavBarHeight animated:animated];
        
    }
}

-(void) showAlertWithTitle:(NSString*)title message:(NSString*)message{
    UIAlertAction* ok = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:nil];
    [self showAlertWithTitle:title message:message withAction:ok];
}

- (void) showAlertWithTitle:(NSString *)title message:(NSString *)message withAction:(UIAlertAction *)action{
    [self showAlertWithTitle:title message:message withActions:@[action]];
}
- (void) showAlertWithTitle:(NSString *)title message:(NSString *)message withActions:(NSArray *)actions{
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    for (int i = 0; i<actions.count; i++) {
        id object= actions[i];
        if ([object isKindOfClass:[UIAlertAction class]]) {
            [alertViewController addAction:object];
        }
    }
    
    [self presentViewController:alertViewController animated:YES completion:nil];
}

-(void)moveToMainPage{
    
    [self.delegate setupViewControllers];
    [self.delegate customizeInterface];
    [[self.delegate getTabBarController] setSelectedIndex:1];
    
    NSString * language = [SystemSettingModel shared].selectSystemLanguageList.Code;
    DDLogDebug(@"[LanguagesManager sharedInstance].currentLanguage %@",language);
    
    for (SystemLanguageList * systemLanguageList in [SystemSettingModel shared].SystemSettings.SystemLanguageList) {
        NSString * languageCode=[[SystemSettingModel shared] serverReturnLanguageCodeChangeToAppleFormat:systemLanguageList.Code];
        if ([languageCode isEqualToString:language]) {
            [SystemSettingModel shared].selectSystemLanguageList =  systemLanguageList;
            break;
        }
    }
    [[AppDelegate getAppDelegate] setupLanguageFromSystemSettingSelectSystemLanguage];
    [[AppDelegate getAppDelegate] customizeTabBarForController:(RDVTabBarController *)[AppDelegate getAppDelegate].viewController];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage object:nil];
    [[SystemSettingModel shared] setupMetricAccordingToLanguage];
    [[SystemSettingModel shared] setupPropertyTypeAccordingToLanguage];
    [[SystemSettingModel shared] setupSpokenLanguageList];
    [[[DBChatSetting query] fetch] removeAll];
    DBChatSetting *dbChatSetting = [DBChatSetting new];
    dbChatSetting.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
    dbChatSetting.MemberID = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    dbChatSetting.ChatRoomPhotoAutoDownload = YES;
    [dbChatSetting commit];
    
    if ([[LoginBySocialNetworkModel shared].myLoginInfo hasQBAccount]) {
        [self.delegate quickBloxTotalLoginWithSuccessBlock:^(QBResponse *response, QBUUser *user) {
            
            
            
            [[ChatDialogModel shared] requestDialogsWithCompletionBlock:^{
                
                [[DialogPageAgentProfilesModel shared] AgentProfileGetListsuccess:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                    
                    [self hideLoadingHUD];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDialogsUpdated object:nil];
                    [[ChatDialogModel shared] updateChatBadgeNumber];
                    
                    [self getQBUserData];
                    
                    [[NewsFeedAgentprofilesModel shared] callNewsFeedApiTimer];
                    [[NewsFeedAgentprofilesModel shared] callNewsFeedFromBackGround];
                    [[DialogPageAgentProfilesModel shared]callUpdateDBDiaolgPageAgentProfileTimer];
                    
                    [kAppDelegate window].rootViewController = self.delegate.rootViewController;
                    
                    [[SearchAgentProfileModel shared] getDBSearchAddressArrayToSearchAgentProfileModel];
                }failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                    [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                }];
            }];
        } errorBlock:^(QBResponse * _Nonnull response) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:JMOLocalizedString(@"error_message__server_not_response", nil) errorStatus:2 alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
            
        } retry:3];
        
    }else{
        [self hideLoadingHUD];
        [[ChatDialogModel shared] updateChatBadgeNumber];
        [[NewsFeedAgentprofilesModel shared] callNewsFeedApiTimer];
        [[NewsFeedAgentprofilesModel shared] callNewsFeedFromBackGround];
        [kAppDelegate window].rootViewController = self.delegate.rootViewController;
        [[SearchAgentProfileModel shared] getDBSearchAddressArrayToSearchAgentProfileModel];
    }
}

- (void)getQBUserData {
    
    [QBRequest userWithID:[[LoginBySocialNetworkModel shared].myLoginInfo.QBID integerValue]
             successBlock:^(QBResponse *response, QBUUser *user) {
                 
                 DDLogDebug(@"userWithID success%@", response);
                 
                 [self.delegate quickBloxRegisterPushNotification];
                 
                 NSError *err = nil;
                 [ChatService shared].myQBUUserCustomData = [[QBUUserCustomData alloc] initWithString:user.customData error:&err];
                 if ([[ChatService shared].myQBUUserCustomData valid]) {
                     [[ChatService shared].myQBUUserCustomData checkCustomDataValidate:user.customData];
                 }else{
                     [ChatService shared].myQBUUserCustomData = [[QBUUserCustomData alloc] init];
                     [ChatService shared].myQBUUserCustomData.chatroomshowlastseen = YES;
                     [ChatService shared].myQBUUserCustomData.chatroomshowreadreceipts = YES;
                 }
                 
                 [LoginBySocialNetworkModel shared].myLoginInfo.chatroomshowlastseen = [ChatService shared].myQBUUserCustomData.chatroomshowlastseen;
                 [LoginBySocialNetworkModel shared].myLoginInfo.chatroomshowreadreceipts = [ChatService shared].myQBUUserCustomData.chatroomshowreadreceipts;
                 [[LoginBySocialNetworkModel shared].myLoginInfo updateCustomData];
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     
                     [self handleDeepLinkAction];
                 });
             } errorBlock:^(QBResponse *response) {
                 
                 DDLogError(@"[QBRequest userWithID%@", response);
             }];
}

- (void)handleDeepLinkAction {
    
    [[DeepLinkActionHanderModel shared]handleLinkWithCacheWithType:kDeepLinkActionChat];
    [[DeepLinkActionHanderModel shared]handleLinkWithCacheWithType:kDeepLinkActionFollow];
    [[DeepLinkActionHanderModel shared]handleLinkWithCacheWithType:kDeepLinkActionNone];
}

-(void)registerQBWithInfo:(LoginInfo*)loginInfo{
    [self showLoadingHUDWithProgress:JMOLocalizedString(@"alert__Initializing", nil)];
    [[LoginBySocialNetworkModel shared] loginByQuickBloxOrSignUpQuickBloxWithInfo:loginInfo Success:^(id responseObject){
        
        if ([loginInfo valid]) {
            
            [self checkContactListRequestAccess];
        }else{
            
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:JMOLocalizedString(@"error_message__server_not_response", nil) errorStatus:2 alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
            NSError *error = [NSError errorWithDomain:@"LoginInfo.Not.Valid" code:404 userInfo:nil];
            [CrashlyticsKit recordError:error withAdditionalUserInfo:nil];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
                NSString *ok) {
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
        [self hideLoadingHUD];
    }];
}

-(void)setNeedCustomBackground:(BOOL)needCustomBackground{
    _needCustomBackground = needCustomBackground;
    if (!needCustomBackground) {
        self.view.backgroundColor =[UIColor blackColor];
    }
}
-(NSString *) className
{
    return NSStringFromClass([self class]);
}
-(void)setupTextFieldCheckError{
    
}
-(void)setupKeyBoardDoneBtnAction{
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return YES;
}

- (void)cancelImageOperation{
    for (id<SDWebImageOperation> operation in self.imageOperations) {
        [operation cancel];
    }
    [self.imageOperations removeAllObjects];
}


- (void)checkContactListRequestAccess {
    
    if ([LoginBySocialNetworkModel shared].myLoginInfo.isNewUser) {
        
        [[REPhonebookManager sharedManager]requestAccess:^(BOOL granted) {
            
            if (granted&&[[REPhonebookManager sharedManager].contactsData valid]) {
                
                [[RealApiClient sharedClient] uploadContactListWithBlock:^(id response, NSError *error) {
                    
                    if (error) {
                        
                        [self moveToMainPage];
                        
                    }else{
                        
                        [[RealApiClient sharedClient] getAgentProfileFromContactListWithBlock:^(id response, NSError *error) {
                            
                            if (error) {
                                
                                [self moveToMainPage];
                                
                            }else{
                                
                                RealPhoneBookShareViewController *phoneBookShareVC = [RealPhoneBookShareViewController phoneBookShareVCWithShareType:RealPhoneBookShareTypeNewUser contact:[REPhonebookManager sharedManager].contacts joinedAgent:response];
                                
                                [self.navigationController pushViewController:phoneBookShareVC animated:YES];
                                
                            }
                            
                        }];
                        
                    }
                    
                }];
                
            }else{
                
                [self moveToMainPage];
            }
            
        }];
        
    }else{
        
        [self moveToMainPage];
        
    }
    
}

- (BOOL)notError:(NSError *)error view:(UIView *)view{
    
    BOOL notError = NO;
    NSString *errorMessage;
    
    [MBProgressHUD hideHUDForView:view animated:YES];
    
    if (isEmpty(error)) {
        
        notError = YES;
        
    } else if (error.code==4 || error.code==26) {
        
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:error.userInfo[NSLocalizedDescriptionKey] errorStatus:error.code];
        
    } else if (error.code == 105) {
        errorMessage = JMOLocalizedString(@"Old phone number not match", nil);
    } else if (error.code == 106) {
        errorMessage = JMOLocalizedString(@"New phone number = old phone number", nil);
    } else if (error.code==110) {
        
        errorMessage = JMOLocalizedString(@"phone_error_messsage__sms_pin_wrong", nil);
        
    } else if (error.code==111) {
        
        errorMessage = JMOLocalizedString(@"phone_error_messsage__sms_pin_expire", nil);
        
    } else if (error.code==112) {
        
        errorMessage = JMOLocalizedString(@"phone_error_messsage__phone_number_has_been_verified", nil);
        
    } else if (error.code==113) {
        
        errorMessage = JMOLocalizedString(@"phone_error_messsage__phone_number_has_been_verified_by_other", nil);
        
    } else {
        
        errorMessage = error.userInfo[NSLocalizedDescriptionKey];
        
    }
    
    if (!notError) {
        
        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:errorMessage];
        
    }
    
    return notError;
}

@end
