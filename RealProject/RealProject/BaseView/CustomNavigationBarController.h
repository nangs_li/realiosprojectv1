//
//  CustomNavigationBarController.h
//  productionreal2
//
//  Created by Alex Hung on 7/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentProfile.h"
@protocol MoveToCenterPage <NSObject>

- (void) moveToCenterPage;

@end

@interface CustomNavigationBarController : UIViewController
@property (nonatomic,strong) id<MoveToCenterPage> delegate;
@property (nonatomic,strong) IBOutletCollection(UIView) NSArray* navBars;



@property (nonatomic,strong) IBOutlet UIView *searchNavBar;
@property (nonatomic,strong) IBOutlet UITextField *searchNavBarTextField;
@property (nonatomic,strong) IBOutlet UIButton *searchNavBarFilterButton;

@property (nonatomic,strong) IBOutlet UIView *apartmentDetailNavBar;
@property (nonatomic,strong) IBOutlet UILabel *apartmentDetailTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *apartmentDetailMoreButton;
@property (nonatomic,strong) IBOutlet UIButton *apartmentDetailBackButton;
@property (nonatomic, strong) IBOutlet UIButton *rightArrow;


@property (nonatomic,strong) IBOutlet UIView *agentProfileDetailNavBar;
@property (nonatomic,strong) IBOutlet UILabel *agentProfileDetailTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *agentProfileDetailMoreButton;
@property (nonatomic,strong) IBOutlet UIButton *agentProfileDetailBackButton;

@property (nonatomic,strong) IBOutlet UIView *newsFeedNavBar;
@property (nonatomic,strong) IBOutlet UILabel *newsFeedTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *newsFeedMiniViewButton;
@property (nonatomic,strong) IBOutlet UIButton *newsFeedConnectAgentButton;

@property (nonatomic,strong) IBOutlet UIView *meNavBar;
@property (nonatomic,strong) IBOutlet UILabel *meTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *meInviteButton;
@property (nonatomic,strong) IBOutlet UIButton *meEditButton;

@property (nonatomic,strong) IBOutlet UIView *editProfileNavBar;
@property (nonatomic,strong) IBOutlet UILabel *editProfileTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *editProfileEditButton;
@property (strong, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic,strong) IBOutlet UIView *dialogNavBar;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *dialogLoadingView;
@property (nonatomic,strong) IBOutlet UILabel *dialogChatLabel;
@property (nonatomic,strong) IBOutlet UILabel *dialogConnectingLabel;
@property (nonatomic,strong) IBOutlet UIButton *dialogEditButton;
@property (nonatomic,strong) IBOutlet UIButton *dialogInviteButton;

@property (nonatomic,strong) IBOutlet UIView *settingNavBar;
@property (nonatomic,strong) IBOutlet UILabel *settinTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *settingBackButton;


@property (nonatomic,strong) IBOutlet UIView *simpleNavBar;
@property (nonatomic,strong) IBOutlet UILabel *simpleTitleLabel;

@end
