//
//  CustomNavigationBarController.m
//  productionreal2
//
//  Created by Alex Hung on 7/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "CustomNavigationBarController.h"

@interface CustomNavigationBarController ()

@end

@implementation CustomNavigationBarController

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage
                                              object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)moveToCenterPage:(id)sender {
    if ([self.delegate respondsToSelector:@selector(moveToCenterPage)]) {
        [self.delegate moveToCenterPage];
    }
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
//    self.searchNavBarTextField.text=JMOLocalizedString(@"common__agents", nil);
    self.agentProfileDetailTitleLabel.text=JMOLocalizedString(@"common__my_profile", nil);
    self.newsFeedTitleLabel.text=JMOLocalizedString(@"common__news_feed", nil);
    self.settinTitleLabel.text=JMOLocalizedString(@"common__settings", nil);
    self.meTitleLabel.text=JMOLocalizedString(@"common__me", nil);
    [self.dialogEditButton setTitle:JMOLocalizedString(@"common__edit", nil) forState:UIControlStateNormal];


}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
