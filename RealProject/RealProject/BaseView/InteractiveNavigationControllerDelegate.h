//
//  InteractiveNavigationControllerDelegate.h
//  productionreal2
//
//  Created by Alex Hung on 4/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InteractiveNavigationControllerDelegate : NSObject <UINavigationControllerDelegate>
@property (nonatomic,weak) IBOutlet UINavigationController *navigationController;
@property (nonatomic,strong) UIPercentDrivenInteractiveTransition * interactionController;
- (id)initWithNavgationController:(UINavigationController*)navVC;
@end
