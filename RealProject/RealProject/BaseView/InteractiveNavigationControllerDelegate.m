//
//  InteractiveNavigationControllerDelegate.m
//  productionreal2
//
//  Created by Alex Hung on 4/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "InteractiveNavigationControllerDelegate.h"
#import "SlideAnimatedTransitioning.h"
#import "MeProfileViewController.h"
@implementation InteractiveNavigationControllerDelegate

-(void)awakeFromNib{
    [super awakeFromNib];
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(pannded:)];
    panGesture.delegate = self;
    [self.navigationController.view addGestureRecognizer:panGesture];
}

- (id)initWithNavgationController:(UINavigationController*)navVC {
    self = [super init];
    if (self) {
        self.navigationController = navVC;
    }
    return self;
}


-(IBAction)pannded:(UIPanGestureRecognizer*) gesture{
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
            CGPoint test = [gesture translationInView:self.navigationController.view];
            if(test.x >0){
                self.interactionController = [[UIPercentDrivenInteractiveTransition alloc]init];
                if (self.navigationController.viewControllers.count <2) {
                      [self.navigationController.topViewController performSegueWithIdentifier:@"PushToAddress" sender:nil];
                }
            }
            break;
        }case UIGestureRecognizerStateChanged:{
            CGPoint translation = [gesture translationInView:self.navigationController.view];
            CGFloat completionProgress = -translation.x/CGRectGetWidth(self.navigationController.view.bounds);
            [self.interactionController updateInteractiveTransition:completionProgress];
            break;
        }case UIGestureRecognizerStateEnded:
            if (-[gesture velocityInView:self.navigationController.view].x >0) {
                [self.interactionController finishInteractiveTransition];
            }else{
                [self.interactionController cancelInteractiveTransition];
            }
            self.interactionController = nil;
            break;
        default:
            [self.interactionController cancelInteractiveTransition];
            self.interactionController = nil;
            break;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    AgentStatus status;
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        if ([RealUtility
             isValid:[MyAgentProfileModel shared].myAgentProfile.AgentListing]) {
            status = AgentStatusWithListing;
        } else {
            status = AgentStatusWithoutListing;
        }
    } else {
        status = AgentStatusNotAgent;
    }
    
    return [kAppDelegate getTabBarController].selectedIndex == 3 && status ==AgentStatusWithoutListing && NO;
}

- (nullable id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                                   interactionControllerForAnimationController:(id <UIViewControllerAnimatedTransitioning>) animationController NS_AVAILABLE_IOS(7_0){
    
    return self.interactionController;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                            animationControllerForOperation:(UINavigationControllerOperation)operation
                                                         fromViewController:(UIViewController *)fromVC
                                                           toViewController:(UIViewController *)toVC{
    SlideAnimatedTransitioning *transitioning = [[SlideAnimatedTransitioning alloc]init];
    transitioning.navigationControllerOperation = operation;
    return transitioning;
}



@end
