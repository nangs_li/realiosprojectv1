//
//  MutlipleStateButton.h
//  productionreal2
//
//  Created by Alex Hung on 12/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    MutipleStateAbove =0,
    MutipleStateAround,
    MutipleStateBelow
}MutipleState;

@interface MutlipleStateButton : UIButton
@property (nonatomic,assign) MutipleState selectState;
@property (nonatomic,strong) NSMutableDictionary *stateTitle;
-(void)commonSetup;
-(void)setState:(MutipleState)state withTitle:(NSString*)title;
@end
