//
//  MutlipleStateButton.m
//  productionreal2
//
//  Created by Alex Hung on 12/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "MutlipleStateButton.h"

@implementation MutlipleStateButton

-(void)awakeFromNib{
    
}

-(void)commonSetup{
    self.stateTitle = [[NSMutableDictionary alloc]init];
    self.selectState = 0;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setState:(MutipleState)state withTitle:(NSString*)title{
    if (!self.stateTitle) {
        self.stateTitle = [[NSMutableDictionary alloc]init];
    }
    if (title) {
        [self.stateTitle setObject:title forKey:@(state)];
    }
}

-(void)setSelected:(BOOL)selected{

    if (selected) {
        NSInteger currentStateIndex = self.selectState;
        currentStateIndex ++;
        if (currentStateIndex >=3) {
            currentStateIndex = 0;
        }
        self.selectState = (int)currentStateIndex;

    }
     [super setSelected:selected];
}
-(void)setSelectState:(MutipleState)selectState{
    _selectState = selectState;
    NSString *title = self.stateTitle[@(_selectState)];
    [self setTitle:title forState:UIControlStateNormal];
}
@end
