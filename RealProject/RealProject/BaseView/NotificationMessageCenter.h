//
//  NotificationMessageCenter.h
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationMessageView.h"
@interface NotificationMessageCenter : NSObject
+ (id)sharedCenter;
@property (nonatomic,strong) NotificationMessageView *messageView;
-(void)showfollowedMessageWithMemberName:(NSString*)memberName memberURL:(NSString*)memberURL withBlock:(NotificationMessageBlock)block;
@end
