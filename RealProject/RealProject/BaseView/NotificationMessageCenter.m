//
//  NotificationMessageCenter.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "NotificationMessageCenter.h"
#import "RealUtility.h"

@implementation NotificationMessageCenter

+ (id)sharedCenter{
    static NotificationMessageCenter *sharedCenter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCenter = [[self alloc] init];
    });
    return sharedCenter;
}

- (id)init {
    if (self = [super init]) {
       
    }
    return self;
}



-(void)showfollowedMessageWithMemberName:(NSString*)memberName memberURL:(NSString*)memberURL withBlock:(NotificationMessageBlock)block{
    
//    UIViewController *topViewController = [kAppDelegate topViewController];
    UIView *showView = [kAppDelegate window];
//    if ([topViewController isKindOfClass:[RDVTabBarController class]]) {
//        showView = ((RDVTabBarController*)topViewController).selectedViewController.view;
//    }
//    CGFloat tabBarHeight = [kAppDelegate getTabBarController].tabBarHidden?0: [kAppDelegate getTabBarController].tabBar.frame.size.height;
    CGRect messageFrame = showView.bounds;
    messageFrame.origin.y = messageFrame.size.height;
    messageFrame.size.height = 64;
    
    if (!self.messageView) {
        self.messageView= [[NotificationMessageView alloc]initWithFrame:messageFrame];
    }
    
    
    self.messageView.frame =messageFrame;
    if(self.messageView.superview){
        [self.messageView removeFromSuperview];
    }else{
        if (self.messageView.superview != showView) {
                [showView addSubview:self.messageView];
        }else{
        
        }
    }
    
    NSString* actionString = JMOLocalizedString(@"newsfeed_popup_label_1", nil);
    NSString* highlightString = memberName;
    NSString* timeString = JMOLocalizedString(@"newsfeed_popup_label_2", nil);
    NSString *message = [NSString stringWithFormat:JMOLocalizedString(@"newsfeed__youfollowed_justnow", nil),highlightString];
   
    [self.messageView showMessage:message withIcon:memberURL withBlock:block];
   
}




@end
