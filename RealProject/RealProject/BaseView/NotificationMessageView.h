//
//  NotificationMessageView.h
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^NotificationMessageBlock) ();

@interface NotificationMessageView : UIView
@property (nonatomic,strong) UIPanGestureRecognizer* panGestureRecognizer;
@property (nonatomic,strong) UIButton *dismissButton;
@property (nonatomic,strong) UIView *contentView;
@property (nonatomic,strong) UIButton *messageButton;
@property (nonatomic,strong) UIImageView *messageIconImageView;
@property (nonatomic,strong) UILabel *messageLabel;
@property (nonatomic,assign) NotificationMessageBlock messageBlock;

@property (nonatomic,strong) NSTimer* dimissTimer;
-(void)showMessage:(NSString*)message withIcon:(NSString*)iconURL withBlock:(NotificationMessageBlock)block;
@end
