//
//  NotificationMessageView.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "NotificationMessageView.h"
#import "UILabel+Utility.h"
#define NotificationMessageShowDuration        15.0
@implementation NotificationMessageView

-(instancetype)initWithFrame:(CGRect)frame{
    self=  [super initWithFrame:frame];
    
    [self commonInit];
    return self;
    
}

-(void)commonInit{
//    self.hidden = YES;
    CGRect frame = self.bounds;
    self.backgroundColor =[UIColor colorWithWhite:0 alpha:0.9];
    if (!self.panGestureRecognizer) {
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
        [self addGestureRecognizer:panRecognizer];
    }
    
    if (!self.dismissButton) {
        self.dismissButton =[UIButton buttonWithType:UIButtonTypeCustom];
        self.dismissButton.frame =CGRectMake(frame.size.width-44, (frame.size.height-44)/2, 44, 44);
        [self.dismissButton addTarget:self action:@selector(dismissButtonDidPress) forControlEvents:UIControlEventTouchUpInside];
        [self.dismissButton setImage:[UIImage imageNamed:@"btn_pop_icon_remove"] forState:UIControlStateNormal];
    }
    
    
    if (!self.contentView) {
        CGRect contentFrame= self.bounds;
        contentFrame.size.width =self.dismissButton.frame.origin.x;
        self.contentView =[[UIView alloc]initWithFrame:contentFrame];
        self.contentView.backgroundColor =[UIColor clearColor];
    }
    
    if (!self.messageButton) {
        self.messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.messageButton.frame = self.contentView.bounds;
        [self.messageButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:0.8]] forState:UIControlStateHighlighted];
        [self.messageButton addTarget:self action:@selector(messageButtonDidPress) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (!self.messageIconImageView) {
        self.messageIconImageView =[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 44, 44)];
        self.messageIconImageView.layer.cornerRadius = self.messageIconImageView.frame.size.width/2;
        self.messageIconImageView.clipsToBounds = YES;
        self.messageIconImageView.backgroundColor = [UIColor clearColor];
        self.messageIconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    if(!self.messageLabel){
        CGRect labelFrame = self.contentView.bounds;
        labelFrame.origin.x = self.messageIconImageView.frame.origin.x+self.messageIconImageView.frame.size.width +8;
        labelFrame.origin.y +=8;
        labelFrame.size.height -=labelFrame.origin.y*2;
        labelFrame.size.width = (self.dismissButton.frame.origin.x - labelFrame.origin.x );
        self.messageLabel = [[UILabel alloc]initWithFrame:labelFrame];
        self.messageLabel.numberOfLines =2;
        self.messageLabel.textColor = [UIColor whiteColor];
        self.messageLabel.backgroundColor = [UIColor clearColor];
    }
    
    
    if(!self.contentView.superview){
        [self addSubview:self.contentView];
    }
    
    if (!self.messageIconImageView.superview) {
        [self.contentView addSubview:self.messageIconImageView];
    }
    
    if (!self.messageLabel.superview) {
        [self.contentView addSubview:self.messageLabel];
    }
    
    if(!self.messageButton.superview){
        [self.contentView addSubview:self.messageButton];
    }
    
    
    if (!self.dismissButton.superview) {
        [self addSubview:self.dismissButton];
    }
}

-(void)move:(UIPanGestureRecognizer*)recognizer {
    CGPoint translation = [recognizer translationInView:self];
    CGRect containerRect = self.superview.bounds;
    CGFloat newY = self.frame.origin.y + translation.y;
    if (newY >= containerRect.size.height-self.frame.size.height) {
        self.center = CGPointMake(self.center.x, self.center.y + translation.y);
        [recognizer setTranslation:CGPointZero inView:self];
    }
    if(recognizer.state == UIGestureRecognizerStateEnded) {
        CGRect visibleRect = CGRectIntersection(self.frame, containerRect);
        if (visibleRect.size.height < self.frame.size.height*0.7) {
            [self dismissWithAnimation:YES];
        }else{
            [self showWithAnimation:YES];
        }
    }
}

-(void)dismissButtonDidPress{
    [self dismissWithAnimation:YES];
}

-(void)showWithAnimation:(BOOL)animated{
//    self.hidden = NO;
    CGRect containerRect = self.superview.bounds;
    CGRect visibleRect = CGRectIntersection(self.frame, containerRect);
    CGFloat duration = 0.3*((self.frame.size.height -visibleRect.size.height)/self.frame.size.height);
    [UIView animateWithDuration:duration animations:^{
        self.frame =CGRectMake(self.frame.origin.x, containerRect.size.height-self.frame.size.height, self.frame.size.width, self.frame.size.height);
    }];
}

-(void)dismissWithAnimation:(BOOL)animated{
    CGRect containerRect = self.superview.bounds;
    CGRect visibleRect = CGRectIntersection(self.frame, containerRect);
    CGFloat duration = 0.3*(visibleRect.size.height/self.frame.size.height);
    [UIView animateWithDuration:duration animations:^{
        self.frame =CGRectMake(self.frame.origin.x, containerRect.size.height+self.frame.size.height, self.frame.size.width, self.frame.size.height);
    }completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
//            self.hidden = YES;
        }
    }];
    
}

-(void)messageButtonDidPress{
    if (self.messageBlock) {
        self.messageBlock();
    }
    [self dismissButtonDidPress];
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer{
    CGPoint velocity = [panGestureRecognizer velocityInView:self];
    return fabs(velocity.y) > fabs(velocity.x);
}

-(void)showMessage:(NSString*)message withIcon:(NSString*)iconURL withBlock:(NotificationMessageBlock)block{
    self.messageBlock = block;
    if (self.dimissTimer) {
        [self.dimissTimer invalidate];
    }
    
    [self.messageLabel setAttributedTextWithMarkUp:message];
    [self.messageIconImageView loadImageURL:[NSURL URLWithString:iconURL] withIndicator:YES];
    self.dimissTimer =[NSTimer scheduledTimerWithTimeInterval:NotificationMessageShowDuration target:self
                                                     selector:@selector(dismissButtonDidPress) userInfo:nil repeats:NO];
    
    [self showWithAnimation:YES];
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
