//
//  RealImagePickerViewController.h
//  productionreal2
//
//  Created by Alex Hung on 9/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealImagePickerViewController : UIImagePickerController

@end
