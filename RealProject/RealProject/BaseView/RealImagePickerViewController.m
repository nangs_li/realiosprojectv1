//
//  RealImagePickerViewController.m
//  productionreal2
//
//  Created by Alex Hung on 9/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealImagePickerViewController.h"

@interface RealImagePickerViewController () <UIViewControllerPreviewingDelegate>

@end

@implementation RealImagePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerForPreviewingWithDelegate:self sourceView:self.view];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (nullable UIViewController *)previewingContext:(id <UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location{
    
    return nil;
}
- (void)previewingContext:(id <UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit{
    
}
-(void) pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [super pushViewController:viewController animated:animated];
    [viewController registerForPreviewingWithDelegate:self sourceView:[[viewController.view subviews] firstObject]];
    
}
@end
