//
//  RealImageTask.h
//  productionreal2
//
//  Created by Alex Hung on 4/3/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RealImageTask : NSObject
@property (nonatomic,strong) NSArray *imageURLs;
@property (nonatomic,strong) id key;
@property (nonatomic,strong) id object;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSOperation *operation;
@end
