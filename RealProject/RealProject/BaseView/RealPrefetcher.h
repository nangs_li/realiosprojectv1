//
//  RealPrefetcher.h
//  productionreal2
//
//  Created by Alex Hung on 10/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RealPrefetcherDelegate <NSObject>

-(void) prefeteherDidFinishFetching:(id)object;

@end


@interface RealPrefetcher : NSObject
@property (nonatomic,weak) id<RealPrefetcherDelegate> delegate;
+ (RealPrefetcher*)sharePrefetcher;
-(void)prefetchAgentProfile:(NSArray*)profiles clearQueue:(BOOL)clear;
-(void)prefetchAgentListings:(NSArray*)listings allContent:(BOOL)all clearQueue:(BOOL)clear top:(BOOL)top;

- (void)cancelListingImageTask;
- (void)cancelAgentImageTask;
- (void)cancelTopListingImageTask;
@end
