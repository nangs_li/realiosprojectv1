//
//  RealPrefetcher.m
//  productionreal2
//
//  Created by Alex Hung on 10/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealPrefetcher.h"
#import "SDWebImagePrefetcher.h"
#import "NSURL+Utility.h"
#import "RealImageTask.h"
#import "RealImageOperation.h"
#import "SDWebImageDownloaderOperation.h"
@interface RealPrefetcher ()
@property (nonatomic,strong) NSMutableDictionary *agentImageTasks;
@property (nonatomic,strong) NSMutableDictionary *listingImageTasks;
@property (nonatomic,strong) NSMutableDictionary *topListingImageTasks;

@property (nonatomic,strong) NSOperationQueue *agentTaskQueue;
@property (nonatomic,strong) NSOperationQueue *listingTaskQueue;
@property (nonatomic,strong) NSOperationQueue *topListingTaskQueue;


@end
@implementation RealPrefetcher
+ (RealPrefetcher*)sharePrefetcher{
    static RealPrefetcher *sharePrefetcher = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharePrefetcher = [[self alloc] init];
    });
    return sharePrefetcher;
}

- (id)init {
    if (self = [super init]) {
        self.agentImageTasks =[[NSMutableDictionary alloc]init];
        self.listingImageTasks = [[NSMutableDictionary alloc]init];
        self.topListingImageTasks = [[NSMutableDictionary alloc]init];
        
        self.agentTaskQueue = [[NSOperationQueue alloc]init];
        self.agentTaskQueue.name = @"AgentProfilePrefetchQueue";
        self.agentTaskQueue.qualityOfService = NSQualityOfServiceBackground;
        self.agentTaskQueue.maxConcurrentOperationCount = -1;
        
        self.listingTaskQueue = [[NSOperationQueue alloc]init];
        self.listingTaskQueue.name = @"AgentListingPrefetchQueue";
        self.listingTaskQueue.qualityOfService = NSQualityOfServiceBackground;
        self.listingTaskQueue.maxConcurrentOperationCount = 1;
        
        self.topListingTaskQueue = [[NSOperationQueue alloc]init];
        self.topListingTaskQueue.name = @"TopAgentListingPrefetchQueue";
        self.topListingTaskQueue.qualityOfService = NSQualityOfServiceBackground;
        self.topListingTaskQueue.maxConcurrentOperationCount = 2;
        
    }
    return self;
}

-(void)prefetchAgentListings:(NSArray*)listings allContent:(BOOL)all clearQueue:(BOOL)clear top:(BOOL)top{
    if(clear){
        if(top){
            [self cancelTopListingImageTask];
        }else{
            [self cancelListingImageTask];
        }
    }
    for (AgentListing *listing in listings) {
        RealImageTask *imageTask = [[RealImageTask alloc]init];
        
        NSNumber *key = @(listing.AgentListingID);
        NSMutableArray *urls = [[NSMutableArray alloc]init];
        
        if ([RealUtility isValid:listing]) {
            int photoCount = (int) listing.Photos.count;
            if (!all && photoCount >2) {
                photoCount =2;
            }
            for (int i = 1; i < photoCount; i++) {
                Photos *photo = listing.Photos[i];
                NSURL *listingPhotoURL = [NSURL URLWithString:photo.URL];
                if ( i == 1) {
                    if ([RealUtility isValid:listingPhotoURL]) {
                        [urls addObject:listingPhotoURL];
                    }
                    for (Reasons *reason in listing.Reasons) {
                        if ([RealUtility isValid:reason.MediaURL1]) {
                            NSURL *reasonMedia1URL = [NSURL URLWithString:reason.MediaURL1];
                            [urls addObject:reasonMedia1URL];
                        }
                        
                        if ([RealUtility isValid:reason.MediaURL2]) {
                            NSURL *reasonMedia2URL = [NSURL URLWithString:reason.MediaURL2];
                            [urls addObject:reasonMedia2URL];
                        }
                        
                        if ([RealUtility isValid:reason.MediaURL3]) {
                            NSURL *reasonMedia3URL = [NSURL URLWithString:reason.MediaURL3];
                            [urls addObject:reasonMedia3URL];
                        }
                    }
                }else{
                    if ([RealUtility isValid:listingPhotoURL]) {
                        [urls addObject:listingPhotoURL];
                    }
                }
                
            }
            
            
        }
        imageTask.key = key;
        imageTask.object = listing;
        imageTask.imageURLs = [NSArray arrayWithArray:urls];
        NSString *type = @"listing";
        if (top) {
            type = @"topListing";
        }
        imageTask.type = type;
        [self prefetchImageTask:imageTask type:type];
    }
    
}
-(void)prefetchAgentProfile:(NSArray*)profiles clearQueue:(BOOL)clear{
    if (clear) {
        [self cancelAgentImageTask];
    }
    int index = 0;
    for (AgentProfile *profile in profiles) {
        RealImageTask *imageTask = [[RealImageTask alloc]init];
        NSNumber *key = @(profile.MemberID);
        NSMutableArray *urls = [[NSMutableArray alloc]init];
        if ([RealUtility isValid:profile.AgentPhotoURL]) {
            NSURL *agentPhotoURL = [NSURL URLWithString:profile.AgentPhotoURL];
            if (agentPhotoURL) {
                [urls addObject:agentPhotoURL];
            }
        }
        if ([RealUtility isValid:profile.AgentListing]) {
            if (profile.coverPhotoURL) {
                [urls addObject:profile.coverPhotoURL];
            }
            if (index == 0) {
                [self prefetchAgentListings:@[profile.AgentListing] allContent:YES clearQueue:YES top:YES];
            }
        }
        index++;
        imageTask.imageURLs =[NSArray arrayWithArray:urls];
        imageTask.key = key;
        imageTask.object = profile;
        imageTask.type = @"profile";
        [self prefetchImageTask:imageTask type:@"profile"];
    }
}


-(void)prefetchImageTask:(RealImageTask*)task type:(NSString*)type{
    NSMutableDictionary *currentTask = nil;
    NSOperationQueue *taskQueue = nil;
    if ([type isEqualToString:@"listing"]) {
        currentTask = self.listingImageTasks;
        taskQueue = self.listingTaskQueue;
    }else if ([type isEqualToString:@"profile"]){
        currentTask = self.agentImageTasks;
        taskQueue = self.agentTaskQueue;
    }else if ([type isEqualToString:@"topListing"]){
        currentTask = self.topListingImageTasks;
        taskQueue = self.topListingTaskQueue;
    }
    if (!currentTask[task.key]) {
        if ([type isEqualToString:@"listing"] &&  self.topListingImageTasks[task.key] ) {
            return;
        }
        for (NSURL *url in task.imageURLs) {
            NSOperation *operation = [self sdImageperationForImageURL:url imageTask:task];
            task.operation = operation;
            [taskQueue addOperation:operation];
        }
        
        [currentTask setObject:task forKey:task.key];
    }else{
        
    }
}



- (RealImageOperation*)sdImageperationForImageURL:(NSURL*)url imageTask:(RealImageTask*)task{
    RealImageOperation *operation;
    NSMutableDictionary *HTTPHeaders = [@{@"Accept": @"image/webp,image/*;q=0.8"} mutableCopy];
    NSInteger downloadTimeout = 15.0;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:(0 & SDWebImageDownloaderUseNSURLCache ? NSURLRequestUseProtocolCachePolicy : NSURLRequestReloadIgnoringLocalCacheData) timeoutInterval:downloadTimeout];
    request.HTTPShouldHandleCookies = (0 & SDWebImageDownloaderHandleCookies);
    request.HTTPShouldUsePipelining = YES;
    request.allHTTPHeaderFields = HTTPHeaders;
    operation = [[RealImageOperation alloc] initWithRequest:request
                                                    options:0
                                                   progress:nil
                                                  completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                      if (finished) {
                                                          if(image){
                                                              NSString *taskType = task.type;
                                                              NSURL *responseURL = url;
                                                              NSString *cacheKey = [[SDWebImageManager sharedManager] cacheKeyForURL:responseURL];
                                                              if ([url.absoluteString isEqualToString:responseURL.absoluteString]) {
                                                                  [[SDImageCache sharedImageCache]storeImage:image recalculateFromImage:NO imageData:data forKey:cacheKey toDisk:YES];
                                                                  if ([self.delegate respondsToSelector:@selector(prefeteherDidFinishFetching:)]) {
                                                                      [self.delegate prefeteherDidFinishFetching:task.object];
                                                                  };
                                                              }
                                                          }
                                                      }
                                                  }
                                                  cancelled:nil];
    operation.shouldDecompressImages = NO;
    
    if([UIImage imageExistForURL:url]){
        operation.queuePriority = NSOperationQueuePriorityVeryLow;
    }else{
        operation.queuePriority = NSOperationQueuePriorityNormal;
    }
    return operation;
}


- (AFHTTPRequestOperation*)operationForImageURL:(NSURL*)url imageTask:(RealImageTask*)task{
    AFHTTPRequestOperation *operation = [UIImage operationWithURL:url completion:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject){
            NSURL *responseURL = [[operation response] URL];
            if ([url.absoluteString isEqualToString:responseURL.absoluteString]) {
                [[SDImageCache sharedImageCache] storeImage:responseObject forKey:[[SDWebImageManager sharedManager] cacheKeyForURL:responseURL]];
                if ([self.delegate respondsToSelector:@selector(prefeteherDidFinishFetching:)]) {
                    [self.delegate prefeteherDidFinishFetching:task.object];
                };
            }
        }
    }];
    if([UIImage imageExistForURL:url]){
        operation.queuePriority = NSOperationQueuePriorityVeryLow;
    }else{
        operation.queuePriority = NSOperationQueuePriorityNormal;
    }
    return operation;
}

- (void)cancelAgentImageTask{
    [self.agentTaskQueue cancelAllOperations];
    [self.agentImageTasks removeAllObjects];
}

- (void)cancelListingImageTask{
    [self.listingTaskQueue cancelAllOperations];
    [self.listingImageTasks removeAllObjects];
}

- (void)cancelTopListingImageTask{
    [self.topListingTaskQueue cancelAllOperations];
    [self.topListingImageTasks removeAllObjects];
}

- (void)cancelImageTasksOperation:(NSDictionary*)imageTasks{
    for (RealImageTask *imageTask in imageTasks.allValues) {
        [imageTask.operation cancel];
    }
}

@end
