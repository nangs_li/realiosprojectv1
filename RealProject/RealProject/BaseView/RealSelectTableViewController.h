//
//  RealSelectTableViewController.h
//  productionreal2
//
//  Created by Alex Hung on 2/11/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface RealSelectTableViewController : BaseViewController <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *titleArray;
@property (nonatomic,strong) NSArray *selectionArray;
@property (nonatomic,strong) NSString *indicatoImageName;
@property (nonatomic,strong) NSIndexPath *currentSelectIndexPath;
@property (nonatomic,assign) BOOL disableSelection;

-(void)configureWithTitles:(NSArray*)titles selection:(NSArray*)selections indicatorName:(NSString*)indicatorName;
@end
