//
//  RealSelectTableViewController.m
//  productionreal2
//
//  Created by Alex Hung on 2/11/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealSelectTableViewController.h"
#import "RealSelectTableCell.h"

@interface RealSelectTableViewController ()

@end

@implementation RealSelectTableViewController

-(void)awakeFromNib{
    [super awakeFromNib];
}

-(void)configureWithTitles:(NSArray*)titles selection:(NSArray*)selections indicatorName:(NSString*)indicatorName{
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.titleArray = [NSArray arrayWithArray:titles];
    self.selectionArray = [NSArray arrayWithArray:selections];
    self.indicatoImageName = indicatorName;
    self.currentSelectIndexPath = nil;
    [self.tableView reloadData];
}

-(void)registerSelectTableCell{
    [self.tableView registerNib:[UINib nibWithNibName:@"RealSelectTableCell" bundle:nil] forCellReuseIdentifier:@"RealSelectTableCell"];
}

-(NSString *)titleForIndexPath:(NSIndexPath*)indexPath{
    NSString *title = @"Please check the value is String or not";
    if (self.titleArray.count > indexPath.row) {
        id object =self.titleArray[indexPath.row];
        if ([object isKindOfClass:[NSString class]]) {
            title = object;
        }
    }else{
        title =@"out of bounds";
    }
    
    return title;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerSelectTableCell];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RealSelectTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RealSelectTableCell"];
    NSString *title = [self titleForIndexPath:indexPath];
    cell.disableSelection = self.disableSelection;
    cell.enable = ![self.selectionArray containsObject:title];
    UIImage *image = [UIImage imageWithColor:RealBlueColor];
    if ([RealUtility isValid:self.indicatoImageName ]) {
        image = [UIImage imageNamed:self.indicatoImageName];
    }
    [cell configureWithTitle:title indicatorImage:image];
//    if (self.currentSelectIndexPath.row == indexPath.row) {
//        cell.indicatorImage.hidden = NO;
//    }else{
//        cell.enable = YES;
//    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!self.disableSelection) {
        self.selectionArray = @[self.titleArray[indexPath.row]];
        [tableView reloadData];
    }
    self.currentSelectIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
}


@end
