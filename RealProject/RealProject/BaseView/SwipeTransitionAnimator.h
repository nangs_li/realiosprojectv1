//
//  SwipeTransitionAnimator.h
//  productionreal2
//
//  Created by Alex Hung on 4/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SwipeTransitionAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end
