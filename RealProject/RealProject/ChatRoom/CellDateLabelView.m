//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "CellDateLabelView.h"

#import <QuartzCore/QuartzCore.h>
#import "JSQMessage.h"
@implementation CellDateLabelView

- (void)baseInit {
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self baseInit];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if ((self = [super initWithCoder:aDecoder])) {
    [self baseInit];
  }
  return self;
}
#pragma mark Message SetUp Method
- (void)setupTickImageView:(JSQMessage *)jsqmessage {
 
    if (jsqmessage.IsBlockedSendMessage) {
        self.tickimageview.frame = CGRectMake(self.tickimageview.frame.origin.x, self.tickimageview.frame.origin.y, 10, self.tickimageview.frame.size.height);
        [self.tickimageview setImage:[UIImage imageNamed:@"ico_chat_tick_single_off"]];
        
    }else if (jsqmessage.ErrorStatus) {
        [self.tickimageview setImage:nil];
        
    }else if (jsqmessage.NumberofDeliverStatus > 1) {
 
    
    
        self.tickimageview.frame = CGRectMake(self.tickimageview.frame.origin.x, self.tickimageview.frame.origin.y, 10, self.tickimageview.frame.size.height);
        [self.tickimageview setImage:[UIImage imageNamed:@"ico_chat_tick_single_off"]];
        if (jsqmessage.DidDeliverStatus ) {
            self.tickimageview.frame = CGRectMake(self.tickimageview.frame.origin.x, self.tickimageview.frame.origin.y, 15, self.tickimageview.frame.size.height);
            [self.tickimageview setImage:[UIImage imageNamed:@"ico_chat_tick_off"]];
            }
#pragma mark DidDeliverStatus can be not receive
        if (jsqmessage.ReadStatus) {
            if (![self isEmpty:[jsqmessage.CustomParameters objectForKey:@"chatroomshowreadreceipts"]]) {
                if ([[jsqmessage.CustomParameters objectForKey:@"chatroomshowreadreceipts"]isEqualToString:@"true"]) {
                    
                    [self.tickimageview setImage:[UIImage imageNamed:@"ico_chat_tick_on"]];
                }
                if ([[jsqmessage.CustomParameters objectForKey:@"chatroomshowreadreceipts"]isEqualToString:@"false"]) {
                    
                    [self.tickimageview setImage:[UIImage imageNamed:@"ico_chat_tick_off"]];
                }
            }
            
            self.tickimageview.frame = CGRectMake(self.tickimageview.frame.origin.x, self.tickimageview.frame.origin.y, 15, self.tickimageview.frame.size.height);
            
        }
    }else{
        [self.tickimageview setImage:nil];
    }
    }
////check check isempty
- (BOOL)isEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end
