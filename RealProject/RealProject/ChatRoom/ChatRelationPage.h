//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

@interface ChatRelationPage : UIView
@property (strong, nonatomic) IBOutlet UIButton *muteButton;
@property (strong, nonatomic) IBOutlet UIButton *blockButton;
@property (strong, nonatomic) IBOutlet UIButton *exitChatButton;
@property(nonatomic, strong) UIActionSheet *exitChatSheet;
@property (strong, nonatomic) IBOutlet UIButton *clearChatButton;
@property (strong, nonatomic) IBOutlet UILabel *chatSettingLabel;
@property (strong, nonatomic) IBOutlet UIButton *yesOrNoBtn;

@property(nonatomic, strong) QBChatDialog *dialog;
- (void)getchatRoomRelationAndButtonSetup;
- (void)pressMuteButton;
- (void)pressBlockButton;
- (void)pressExitChatButton;
- (void)pressUnBlockButton;
- (void)buttonSetUp;
@end
