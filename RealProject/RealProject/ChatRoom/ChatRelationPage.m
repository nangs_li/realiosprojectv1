//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "ChatRelationPage.h"
#import "SpokenLanguagescell.h"
#import "ChatRelationModel.h"
#import "DBQBChatDialogClearMessage.h"
// Mixpanel
#import "Mixpanel.h"
#import "ChatDialogModel.h"
@implementation ChatRelationPage
-(void)awakeFromNib{
    [super awakeFromNib];
    [self getchatRoomRelationAndButtonSetup];
}




- (void)pressMuteButton {
    if ([ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty) {
        return;
    }
    NSString *muteStatus = [ChatRelationModel shared].recipienerChatRoomRelation.Muted ? @"0" : @"1";
    BOOL mute = [muteStatus boolValue];
    NSString *otherQBID = [self otherUserQBID];
    [[LoginBySocialNetworkModel shared].myLoginInfo mute:mute withQBID:otherQBID];
    [[ChatRelationModel shared] chatRoomMuteFromDialogId:self.dialog.ID
                                              muteStatus:muteStatus
                                                 Success:^(id responseObject) {
                                                     
                                                     // Mixpanel
                                                     NSString *mutedId = [ChatService shared].recipienterQBUUserCustomData.memberID;
                                                     if (![[AppDelegate getAppDelegate] isEmpty:mutedId]) {
                                                         Mixpanel *mixpanel = [Mixpanel sharedInstance];
                                                         [mixpanel track:@"Muted Chat"
                                                              properties:@{
                                                                           @"Muted": [ChatRelationModel shared].recipienerChatRoomRelation.Muted ? @"NO" : @"YES",
                                                                           @"Muted member ID": mutedId
                                                                           }];

                                                     }
                                                     
                                                     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRelationListUpdated object:nil];
                                                     [[ChatRelationModel shared] sendSystemMessageToRefreshChatRoomRelation];
                                                 }
                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                           NSString *errorMessage, RequestErrorStatus errorStatus,
                                                           NSString *alert, NSString *ok) {
                                                     [[AppDelegate getAppDelegate] handleModelReturnErrorShowUIAlertViewByView:self
                                                                                          errorMessage:errorMessage
                                                                                           errorStatus:errorStatus
                                                                                                 alert:alert
                                                                                                    ok:ok];
                                                 }];
}

- (void)pressBlockButton{
    if ([ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty) {
        return;
    }
    NSString *blockStatus = [ChatRelationModel shared].recipienerChatRoomRelation.Blocked ? @"0": @"1";
    BOOL block = [blockStatus boolValue];
    NSString *otherQBID = [self otherUserQBID];
    [[LoginBySocialNetworkModel shared].myLoginInfo block:block withQBID:otherQBID];
    [ChatRelationModel shared].recipienerChatRoomRelation.Blocked=![ChatRelationModel shared].recipienerChatRoomRelation.Blocked;
    [[ChatRelationModel shared] chatRoomBlockFromDialogId:self.dialog.ID
                                              blockStatus:blockStatus
                                                  Success:^(id responseObject) {
                                                      
                                                      // Mixpanel
                                                      Mixpanel *mixpanel = [Mixpanel sharedInstance];
                                                      
                                                      NSString *blockedId = [ChatService shared].recipienterQBUUserCustomData.memberID;
                                                      if (![[AppDelegate getAppDelegate] isEmpty:blockedId]) {
                                                          [mixpanel track:@"Blocked Contact"
                                                               properties:@{
                                                                            @"Blocked member ID": blockedId
                                                                            }];
                                                      }
                                                     
                                                      
                                                      
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRelationListUpdated object:nil];
                                                      [[ChatRelationModel shared] sendSystemMessageToRefreshChatRoomRelation];
                                                  }
                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                            NSString *errorMessage, RequestErrorStatus errorStatus,
                                                            NSString *alert, NSString *ok) {
                                                      [[AppDelegate getAppDelegate]  handleModelReturnErrorShowUIAlertViewByView:self
                                                                                           errorMessage:errorMessage
                                                                                            errorStatus:errorStatus
                                                                                                  alert:alert
                                                                                                     ok:ok];
                                                  }];
}
- (void)pressUnBlockButton{
    if ([ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty) {
        return;
    }
    NSString *blockStatus =
    [ChatRelationModel shared].recipienerChatRoomRelation.Blocked ? @"0" : @"1";
    BOOL block = [blockStatus boolValue];
    NSString *otherQBID = [self otherUserQBID];
    [[LoginBySocialNetworkModel shared].myLoginInfo block:block withQBID:otherQBID];
    [ChatRelationModel shared].recipienerChatRoomRelation.Blocked=![ChatRelationModel shared].recipienerChatRoomRelation.Blocked;
    [[ChatRelationModel shared] chatRoomBlockFromDialogId:self.dialog.ID
                                              blockStatus:blockStatus
                                                  Success:^(id responseObject) {
                                                      
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRelationListUpdated object:nil];
                                                      [[ChatRelationModel shared] sendSystemMessageToRefreshChatRoomRelation];}
                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                            NSString *errorMessage, RequestErrorStatus errorStatus,
                                                            NSString *alert, NSString *ok) {
                                                      [[AppDelegate getAppDelegate]  handleModelReturnErrorShowUIAlertViewByView:self
                                                                                           errorMessage:errorMessage
                                                                                            errorStatus:errorStatus
                                                                                                  alert:alert
                                                                                                     ok:ok];
                                                  }];
}

- (void)buttonSetUp {
    [self.chatSettingLabel setUpDefaultTitleFontSize];
    [self.muteButton setTitle:JMOLocalizedString(@"chatroom__mute", nil) forState:UIControlStateNormal];
    [self.muteButton setUpDefaultDetailTextFontSize];
    if ([ChatRelationModel shared].recipienerChatRoomRelation.Muted) {
        
        [self.yesOrNoBtn setTitle:JMOLocalizedString(@"chatroom__yes", nil) forState:UIControlStateNormal];
    } else {
        [self.yesOrNoBtn setTitle:JMOLocalizedString(@"chatroom__no", nil) forState:UIControlStateNormal];
    }
     [self.yesOrNoBtn setUpDefaultDetailTextFontSize];
     [self.blockButton setUpDefaultDetailTextFontSize];
    if ([ChatRelationModel shared].recipienerChatRoomRelation.Blocked) {
        [self.blockButton setTitle:JMOLocalizedString(@"chatroom__unblock_this_contact", nil) forState:UIControlStateNormal];
    } else {
        [self.blockButton setTitle:JMOLocalizedString(@"chatroom__block_this_contact", nil) forState:UIControlStateNormal];
    }
     [self.clearChatButton setUpDefaultDetailTextFontSize];
    [ self.clearChatButton setTitle:JMOLocalizedString(@"chatroom__clear_chat", nil) forState:UIControlStateNormal];
    self.chatSettingLabel.text=JMOLocalizedString(@"chatroom__chat_setting", nil) ;
}
- (void)getchatRoomRelationAndButtonSetup {
        [[ChatRelationModel shared]
         getCurrentChatRoomRelationObjectFromLocalForDialogId:self.dialog.ID];
        [self buttonSetUp];
}


-(NSString*) otherUserQBID{
    NSMutableArray *occupantsIDs = [self.dialog.occupantIDs mutableCopy];
    NSString *targetQBID = nil;
    NSNumber *myQBID = @([[LoginBySocialNetworkModel shared].myLoginInfo.QBID integerValue]);
    [occupantsIDs removeObject:myQBID];
    targetQBID =[occupantsIDs firstObject];
    return targetQBID;
}



@end