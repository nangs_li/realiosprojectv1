//
//  ApartmentDetailCardView.h
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentListing.h"

typedef NSUInteger ApartmentCardViewType;

@interface ChatRoomHeaderView : UIView

@property (nonatomic,strong) IBOutlet UILabel *apartmentTypeLabel;
@property (nonatomic,strong) IBOutlet UILabel *apartmentNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *apartmentAddressLabel;

@property (nonatomic,strong) IBOutlet UIView *contentView;
@property (nonatomic,strong) IBOutlet UIView *apartmentInfoView;

@property (nonatomic,strong) IBOutlet UIImageView *apartmentImageView;
@property (nonatomic,assign) BOOL  hidenStatus;
@property (nonatomic,assign) ApartmentCardViewType cardViewType;
@property (nonatomic,strong) AgentListing *agentListing;


-(id)initWithType:(ApartmentCardViewType)type withAgentListing:(AgentListing*)agent;
-(void)configureWithAgentListing:(AgentListing*)listing;
@end
