//
//  ApartmentDetailCardView.m
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "ChatRoomHeaderView.h"
#import "UIView+Utility.h"
#import "RealUtility.h"
#import "SystemSettingModel.h"
#import <QuartzCore/QuartzCore.h>

#define quickViewMaxHeight_568h 134.0f
#define quickViewMaxHeight_480h 80.0f
#define addressLabelMaxWidth  150.0f
#define maxButtonSize  CGSizeMake( 70.0f,28.0f)
@implementation ChatRoomHeaderView

-(id)initWithType:(ApartmentCardViewType)type withAgentListing:(AgentListing*)listing{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomHeaderView" owner:self options:nil];
    self = (ChatRoomHeaderView *)[nib objectAtIndex:0]; // or if it exists, (MCQView *)[nib objectAtIndex:0];
    self.cardViewType = type;
    self.agentListing = listing;
    self.apartmentTypeLabel.backgroundColor = RealDarkBlueColor;
       return self;
}

-(void)configureWithAgentListing:(AgentListing*)listing{
    if ([RealUtility isValid:listing]) {
//        if(self.agentListing.AgentListingID == listing.AgentListingID){
//            return;
//        }
        self.agentListing = listing;
              NSString *apartmentType = [[SystemSettingModel shared]getSpaceTypeByPropertyType:listing.PropertyType spaceType:listing.SpaceType];
        NSString *apartmentImage = [[SystemSettingModel shared]getPropertyImageByPropertyType:listing.PropertyType spaceType:listing.SpaceType state:nil];
        //        AddressUserInput *addressuserinput = [listing.AddressUserInput firstObject];
        GoogleAddress *googleAddress = [listing.GoogleAddress firstObject];
        NSString *address = googleAddress.FormattedAddress;
        NSString *name = googleAddress.Name;
        NSString *location = googleAddress.Location;
        
        self.apartmentImageView.image = [UIImage imageNamed:apartmentImage];
        [self.apartmentTypeLabel setText:apartmentType];
        [self.apartmentAddressLabel setText:location];
        self.apartmentNameLabel.text = name;
    }
}


-(void)fitTitle:(NSString*)title withButton:(UIButton*)button{
    CGRect buttonFrame = button.frame;
    buttonFrame.size =maxButtonSize;
    button.frame = buttonFrame;
    button.titleLabel.numberOfLines = 2;
    [button setTitle:title forState:UIControlStateNormal];
    CGSize buttonFitSize = [button sizeThatFits:maxButtonSize];
    buttonFitSize.width += 5;
    buttonFrame.size = buttonFitSize;
    button.frame = buttonFrame;
//    [button sizeToFit];
}


@end
