//
//  ApartmentDetailCardView.h
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentListing.h"
@interface ChatRoomHouseDetailView : UIView
@property (nonatomic,strong) IBOutlet UIButton *priceButton;
@property (nonatomic,strong) IBOutlet UIButton *sizeButton;
@property (nonatomic,strong) IBOutlet UIButton *bedButton;
@property (nonatomic,strong) IBOutlet UIButton *bathroomButton;
@property (nonatomic,strong) IBOutlet UILabel *apartmentTypeLabel;
@property (nonatomic,strong) IBOutlet UILabel *apartmentNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *apartmentAddressLabel;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint *contentViewTopSpacing;

@property (nonatomic,strong) IBOutlet UIView *contentView;
@property (nonatomic,strong) IBOutlet UIView *apartmentInfoView;

@property (nonatomic,strong) IBOutlet UIImageView *apartmentImageView;
@property (strong, nonatomic) IBOutlet UIImageView *houseImageView;

@property (nonatomic,assign) ApartmentCardViewType cardViewType;
@property (nonatomic,strong) AgentListing *agentListing;

@property (strong, nonatomic) IBOutlet UILabel *priceLabel;

@property (strong, nonatomic) IBOutlet UILabel *sizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *bedLabel;
@property (strong, nonatomic) IBOutlet UILabel *bathLabel;

-(id)initWithType:(ApartmentCardViewType)type withAgentListing:(AgentListing*)agent;
-(void)configureWithAgentListing:(AgentListing*)listing;
+(CGSize)calculateSizeWithAgentListing:(AgentListing*)listing;
@end
