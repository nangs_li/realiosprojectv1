//
//  ApartmentDetailCardView.m
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "ChatRoomHouseDetailView.h"
#import "UIView+Utility.h"
#import "RealUtility.h"
#import "SystemSettingModel.h"
#import <QuartzCore/QuartzCore.h>

#define quickViewMaxHeight_568h 134.0f
#define quickViewMaxHeight_480h 80.0f
#define addressLabelMaxWidth  150.0f
#define maxButtonSize  CGSizeMake( 70.0f,28.0f)
@implementation ChatRoomHouseDetailView

-(id)initWithType:(ApartmentCardViewType)type withAgentListing:(AgentListing*)listing{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomHouseDetailView" owner:self options:nil];
    self = (ChatRoomHouseDetailView *)[nib objectAtIndex:0]; // or if it exists, (MCQView *)[nib objectAtIndex:0];
    self.cardViewType = type;
    self.agentListing = listing;
    CGRect viewFrame = self.frame;
 
       return self;
}

-(void)configureWithAgentListing:(AgentListing*)listing{
    if ([RealUtility isValid:listing]) {
        self.layer.shadowOffset = CGSizeMake(0, 1);
        self.layer.shadowOpacity = 0.3;
        self.layer.shadowRadius = 1.0;
        self.layer.masksToBounds = NO;
        
        self.agentListing = listing;
        if (self.agentListing.Photos.count>0) {
            
            
            Photos *photo = [self.agentListing.Photos objectAtIndex:0];
            
            NSString *photocoverurl = photo.URL;
            
            NSURL *url =
            [NSURL URLWithString:[NSString stringWithFormat:@"%@", photocoverurl]];
            [self.houseImageView loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image) {
                    self.houseImageView.image =image;
                }
            }];
        }

        NSString *priceString = [NSString stringWithFormat:@"%@ %@",listing.CurrencyUnit,listing.PropertyPriceFormattedForRoman] ;
        NSString *sizeString = [NSString stringWithFormat:@"%d %@",listing.PropertySize,[[SystemSettingModel shared]getMetricNativeNameBySizeUnitType:listing.SizeUnitType+1]];
        NSString *bedCount = [NSString stringWithFormat:@"%d",listing.BedroomCount];
        
        NSString *bathroomCount = [NSString stringWithFormat:@"%d",listing.BathroomCount];
        NSString *apartmentType = [[SystemSettingModel shared]getSpaceTypeByPropertyType:listing.PropertyType spaceType:listing.SpaceType];
        NSString *apartmentImage = [[SystemSettingModel shared]getPropertyImageByPropertyType:listing.PropertyType spaceType:listing.SpaceType state:nil];
        //        AddressUserInput *addressuserinput = [listing.AddressUserInput firstObject];
        GoogleAddress *googleAddress = [listing.GoogleAddress firstObject];
        NSString *address = googleAddress.FormattedAddress;
        NSString *name = googleAddress.Name;
        NSString *location = googleAddress.Location;
//        [self fitTitle:priceString withButton:self.priceButton];
//        [self fitTitle:sizeString withButton:self.sizeButton];
        self.priceLabel.text=priceString;
        self.sizeLabel.text=sizeString;
        self.bedLabel.text=bedCount;
        self.bathLabel.text=bathroomCount;
        self.apartmentImageView.image = [UIImage imageNamed:apartmentImage];
        [self.apartmentTypeLabel setText:apartmentType];
        [self.apartmentAddressLabel setText:location];
        self.apartmentNameLabel.text = name;
        if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
            self.apartmentNameLabel.textAlignment=NSTextAlignmentRight;
            self.apartmentAddressLabel.textAlignment=NSTextAlignmentRight;
          
            self.bedButton.contentHorizontalAlignment  = UIControlContentHorizontalAlignmentRight;
            self.bathroomButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            self.sizeButton.contentHorizontalAlignment  = UIControlContentHorizontalAlignmentRight;
            self.priceButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
          
           
            [self setTitleInsetwithButton:self.bedButton];
             [self setTitleInsetwithButton:self.bathroomButton];
             [self setTitleInsetwithButton:self.sizeButton];
             [self setTitleInsetwithButton:self.priceButton];
            
        
        }
        
    }
}
-(void)setTitleInsetwithButton:(UIButton*)button{
    button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);

    
    
}
-(void)fitTitle:(NSString*)title withButton:(UIButton*)button{
    CGRect buttonFrame = button.frame;
    buttonFrame.size =maxButtonSize;
    button.frame = buttonFrame;
    button.titleLabel.numberOfLines = 2;
    [button setTitle:title forState:UIControlStateNormal];
    CGSize buttonFitSize = [button sizeThatFits:maxButtonSize];
    buttonFitSize.width += 5;
    buttonFrame.size = buttonFitSize;
    button.frame = buttonFrame;
//    [button sizeToFit];
}

-(CGRect)calculateFitFrame{
    CGRect fitFrame = self.frame;
    fitFrame.size.height = self.houseImageView.frame.origin.y + self.houseImageView.frame.size.height;
    return fitFrame;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
