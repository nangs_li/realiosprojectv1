//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationBarTitleView : UIView
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *agentPersonalPhoto;
@property (strong, nonatomic) IBOutlet UILabel *typingLabel;
@property (assign, nonatomic) BOOL typingLabelShouldShow;
- (void)showLastSeenLabel:(BOOL)show aniamted:(BOOL)animated;
@end
