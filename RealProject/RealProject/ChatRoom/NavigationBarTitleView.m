//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "NavigationBarTitleView.h"

#import <QuartzCore/QuartzCore.h>
#import "JSQMessage.h"
@implementation NavigationBarTitleView

- (void)baseInit {
    self.typingLabelShouldShow = YES;
    [self showLastSeenLabel:NO aniamted:NO];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self baseInit];
    }
    return self;
}

////check check isempty
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

- (void)showLastSeenLabel:(BOOL)show aniamted:(BOOL)animated{
    if (self.typingLabelShouldShow != show) {
        self.typingLabelShouldShow = show;
        void (^block)() = ^{
            if (show) {
                [self.titleLabel moveViewTo:self.typingLabel direction:RelativeDirectionTop padding:0];
            }else{
                [self.titleLabel align:self.agentPersonalPhoto direction:ViewAlignmentVerticalCenter padding:0];
            }
            self.typingLabel.alpha = show;
        };
        
        void (^completion)(BOOL) = ^(BOOL finished){
            if (finished) {
                self.typingLabel.hidden = !show;
            }
        };
        self.typingLabel.alpha = !show;
        self.typingLabel.hidden = NO;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            if (animated) {
                [UIView animateWithDuration:[CATransaction animationDuration] animations:block completion:completion];
            }else{
                block();
                completion(YES);
            }}];
    }
}
@end
