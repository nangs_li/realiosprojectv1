
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Ken All rights reserved.
//

#import "JSQMessages.h"

#import "MWPhotoBrowser.h"
#import "QBUUserCustomData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RDVTabBarController.h"
#import "DBQBChatMessage.h"
#import "ChatRoomSelectPhoto.h"
#import "DBQBChatDialog.h"
#import "ChatRoomSelectPhoto.h"
#import <AudioToolbox/AudioServices.h>
#import "ChatRoomHouseDetailView.h"
#import "DBChatSetting.h"
#import "CellDateLabelView.h"
#import "QBUUserCustomData.h"
#import "NavigationBarTitleView.h"
#import "NavigationBarConnectingServerViewView.h"
@class Chatroom;

@protocol JSQDemoViewControllerDelegate<NSObject>

- (void)didDismissJSQDemoViewController:(Chatroom *)vc;

@end

@interface Chatroom
    : JSQMessagesViewController<UIActionSheetDelegate, MBProgressHUDDelegate,
                                MWPhotoBrowserDelegate>
@property (nonatomic,strong) NSMutableDictionary *cachedAgentListingDict;
@property(strong, nonatomic) AppDelegate *delegate;
@property(nonatomic, strong) QBChatDialog *dialog;
@property(strong, nonatomic) MBProgressHUD *hud;
#pragma mark messageDateFormat
@property(nonatomic, strong) NSString *messageCellTopLabelDate;
@property(nonatomic, strong) NSMutableArray * messageCellTopLabelDateIndexPathArray;
@property(nonatomic, strong) NSMutableArray * houseDetailViewCellIndexPathArray;
@property(nonatomic, strong) UIRefreshControl *refreshControl;
@property(nonatomic, strong) NSDateFormatter *messageTopLabelDateFormatter;
@property(nonatomic, strong) NSDateFormatter *messageDateLabelFormatter;
@property(nonatomic, strong) NSDateFormatter *Onlydayformat;
#pragma mark Top Bar titleView
@property(nonatomic, strong) NavigationBarTitleView *titleView;

#pragma mark Top Bar connectingView
@property(nonatomic, strong) NavigationBarConnectingServerViewView *connectingServerView;

#pragma mark ExitChatSheet
@property(nonatomic, strong) UIActionSheet *sendPhotoMessageSheet;
@property(nonatomic, strong) UIActionSheet *unBlockSheet;
@property(nonatomic, strong) UIActionSheet *unExitChatSheet;
@property(nonatomic, strong) UIActionSheet *exitChatSheet;
@property(nonatomic, strong) UIActionSheet *blockSheet;
#pragma mark checkFirstCreateDialogbyDialogNotTargetAgentQBID
@property(nonatomic, assign)
    BOOL haveNotCreateDialog;
@property(nonatomic, assign)
BOOL secondTimeSendCoverViewMessage;
@property(strong, nonatomic) NSString *chatroomNameString;
@property(strong, nonatomic) NSString *recipienterMemberID;
#pragma mark photoBrowers
@property(assign, nonatomic) BOOL presentPhotoBrowers;
@property(strong, nonatomic) SDWebImageManager *sdWebImageManager;
@property(strong, nonatomic) MWPhotoBrowser *mwPhotoBrowser;
@property(nonatomic, strong) NSMutableArray *mwPhotosBrowerPhotoArray;
- (void)getImageFromDBQBChatMessageToMWPhotoBrower;
#pragma mark firstTimeEnterChatRoom
@property(nonatomic, assign) BOOL createDialogProcessing;
@property(nonatomic, assign) BOOL firstTimeEnterChatRoom;
#pragma mark selectederrorchatmessage
@property(strong, nonatomic) DBQBChatMessage *selectedErrorChatMessage;
#pragma mark getRecipienterMethod
@property(strong, nonatomic) NSURL *recipientPhotoUrl;
@property(strong, nonatomic) NSNumber *recipientID;
@property(strong, nonatomic) NSString *recipienterLastLoginTimeString;
@property(strong, nonatomic) NSDate *recipienterLastLoginTime;
- (void)getRecipienterFromQBServer;

#pragma mark ChatRoom TopBar Animation
@property (nonatomic) CGFloat lastContentOffset;
#pragma mark ChatRoom PresentType
@property(nonatomic, strong) NSString *chatRoomPresentType;
#pragma mark downloadPhotoWaitingListActive
@property(nonatomic, assign) BOOL downloadPhotoWaitingListActive;
#pragma mark Support Method
- (BOOL)isEmpty:(id)thing;
- (void)chatRoomCollectionViewReloadOnly;
- (void)chatRoomCollectionViewscrollToBottomYesAnimated;
- (void)readAllReceivceMessage:(int)totalReadReceivceMessageCount;
- (void)readFiveReceivceMessage;
- (void)getMessageCellTopLabelDateIndexPathArray;
@end
