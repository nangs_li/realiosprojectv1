#import "AgentListingGetListModel.h"
#import "ApartmentDetailCardView.h"
#import "BaseNavigationController.h"
#import "ChatDialogModel.h"
#import "ChatRelationModel.h"
#import "ChatRelationPage.h"
#import "ChatRoomSendMessageModel.h"
#import "ChatService.h"
#import "Chatroom.h"
#import "DBQBChatDialogClearMessage.h"
#import "DBQBChatDialogDelete.h"
#import "DBUnreadMessageLabel.h"
#import "DialogPageAgentProfilesModel.h"
#import "GetChatMessageToJsqMessageArrayModel.h"
#import "MWPhoto+Custom.h"
#import "NSDate+Utility.h"
#import "NSDateFormatter+Utility.h"
#import "NSString+JSQMessages.h"
#import "RealApiClient.h"
#import "RealUtility.h"
#import "RealUtility.h"
#import "ReceiveMessageCount.h"
#import "UIActionSheet+Blocks.h"
#import "UIViewController+Utility.h"
#import "UnreadMessageLabelView.h"
// Mixpanel
#import "Mixpanel.h"
#define kAgentListingHeaderPadding 20.0f
#define kAgentListingHeaderHeight 58.0f
#define kTopBarShowDistance 80.0f
#define kTopBarHideDistance 20.0f
@interface Chatroom ()<ChatServiceDelegate, UIScrollViewDelegate>
@property(nonatomic, strong) ChatRelationPage *chatSettingView;
@property(nonatomic, strong) UIView *dimView;
@property(nonatomic, assign) BOOL chatSettingWillShow;
@property(nonatomic, assign) BOOL didLayoutSubView;
//@property (nonatomic,strong) NSMutableArray *listingIndexPathArray;
@property(nonatomic, assign) NSInteger currentHeaderPosition;
@end

@implementation Chatroom

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}
- (void)viewDidLoad {
  [super viewDidLoad];
  self.chatRoomPresentType = @"None";
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage object:nil];
  self.currentHeaderPosition = -1;
  [self getAgentListingGetList];
  // Do any additional setup after loading the view.
  self.senderId          = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
  self.senderDisplayName = @"Peter";

  [ChatService shared];
  self.messageTopLabelDateFormatter = [NSDateFormatter localeDateFormatter];
  [self.messageTopLabelDateFormatter setDateFormat:@"EEE, d MMM"];

  self.messageDateLabelFormatter = [NSDateFormatter localeDateFormatter];
  [self.messageDateLabelFormatter setupDateFormatter];
  self.Onlydayformat = [NSDateFormatter localeDateFormatter];
  [self.Onlydayformat setDateFormat:@"yyyy-MM-dd"];

  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;

  _delegate                                           = [AppDelegate getAppDelegate];
  self.navigationController.navigationBar.translucent = NO;
  [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:RealNavBlueColor] forBarMetrics:UIBarMetricsDefault];

#warning DC - custom action for the texts, add it back when we need.
  //    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
  //    [UIMenuController sharedMenuController].menuItems = @[
  //                                                          [[UIMenuItem alloc] initWithTitle:@"Custom Action"
  //                                                                                     action:@selector(customAction:)]
  //                                                          ];
  self.firstTimeEnterChatRoom = YES;

  self.sdWebImageManager = [SDWebImageManager sharedManager];
  //  [self.sdWebImageManager.imageDownloader setMaxConcurrentDownloads:2];
  [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
  [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
  if ([[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ And ChatMessageType = %@", self.dialog.ID, kChatMessageTypeAgentListing] orderBy:@"Id"] count] == 0) {
    [[GetChatMessageToJsqMessageArrayModel shared] getChatMessageFromDBToJsqMessageArrayForDialog:self.dialog];
  } else {
    [[GetChatMessageToJsqMessageArrayModel shared] getChatMessageUntilAgentListingIDFromDBToJsqMessageArrayForDialog:self.dialog];
  }

  self.senderId = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
  [self setUpViewUI];
}

- (void)viewWillLayoutSubviews {
  [self autoCorrectFrameIfNeeded];
  [super viewWillLayoutSubviews];
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  DDLogDebug(@"viewWillAppear");
  [self getRecipienterFromQBServer];
  [ChatRelationModel shared].recipientID = self.recipientID;
  [self chatSettingBtnEnable];
  [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
  [ChatService shared].delegate = self;
  [[ChatService shared] startGetRecipienterTimer];
  [self.dialog setOnUserIsTyping:^(NSUInteger userID) {
    NSDate *timeAgoDate = [NSDate dateWithTimeIntervalSinceNow:0];
   // if ([self.recipienterLastLoginTimeString isEqualToString:JMOLocalizedString(@"chatroom__online", nil)] || [self.recipienterLastLoginTimeString isEqualToString:timeAgoDate.timeAgoSinceNow]) {
      self.titleView.typingLabel.text = JMOLocalizedString(@"chatroom__typing", nil);
      dispatch_time_t popTime         = dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC);
      dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        // Do something...
        self.titleView.typingLabel.text = self.recipienterLastLoginTimeString;
      });
   // }
  }];

  [self.dialog setOnUserStoppedTyping:^(NSUInteger userID) {
    self.titleView.typingLabel.text = self.recipienterLastLoginTimeString;

  }];
  [[self.delegate getTabBarController] setPageControlHidden:YES animated:YES];
  [[self.delegate getTabBarController] setNavBarHidden:YES animated:NO];

  [self setUpFixedLabelTextAccordingToSelectedLanguage];

  [self getMessageCellTopLabelDateIndexPathArray];
  [self.collectionView reloadData];
  [self.navigationController setNavigationBarHidden:NO];

  DBResultSet *r = [[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@", self.dialog.ID] orderBy:@"DateSent"]

      fetch];
  if (r.count == 0 && self.firstTimeEnterChatRoom) {
    [[GetChatMessageToJsqMessageArrayModel shared] whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:self.dialog];

  } else {
    [[GetChatMessageToJsqMessageArrayModel shared] addMessagesFromQBToJSQMessageArrayForDialog:self.dialog];
  }

  if (self.firstTimeEnterChatRoom) {
    [self getImageFromDBQBChatMessageToMWPhotoBrower];
    self.firstTimeEnterChatRoom                  = NO;
    self.automaticallyScrollsToMostRecentMessage = NO;
    //// readAllReceiveMessage
    [self readAllReceivceMessage:5];
    [self scrollToBottomAnimated:NO];
  }

  self.currentHeaderPosition                               = -1;
  UITapGestureRecognizer *stickyHeaderTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stickyHeaderTapGesture:)];
  [self.stickyHeaderView addGestureRecognizer:stickyHeaderTapGestureRecognizer];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatRoomConnectToServerSuccess) name:kNotificationChatRoomConnectToServerSuccess object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatDidAccidentallyDisconnectNotification) name:kNotificationChatDidAccidentallyDisconnect object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAgentListingGetList) name:kNotificationGetAgentListingGetList object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMessageCellTopLabelDateIndexPathArray) name:kNotificationGetMessageCellTopLabelDateIndexPathArray object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatRoomCollectionViewReloadOnly) name:kNotificationChatRoomReloadCollectionView object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatRoomCollectionViewscrollToBottomYesAnimated) name:kNotificationChatRoomReloadCollectionViewAndScrollToBottomYesAnimated object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getImageFromDBQBChatMessageToMWPhotoBrower) name:kNotificationChatRoomGetImageFromDBQBChatMessageToMWPhotoBrower object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readFiveReceivceMessage) name:kNotificationChatRoomReadAllReceivceMessage object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(knotificationChatRelationListUpdated) name:kNotificationChatRoomRelationPageButtonSetup object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(knotificationChatRelationListUpdated) name:kSystemMessageTypeRefreshChatPrivacySetting object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatRoomDownLoadPhotoWaitingListToReloadView) name:kChatRoomDownLoadPhotoWaitingListToReloadView object:nil];

  if ([ChatService shared].connecttoserversuccess) {
    [self chatRoomConnectToServerSuccess];
  } else {
    [self chatDidAccidentallyDisconnectNotification];
  }

  if (![self isEmpty:[ChatRoomSendMessageModel shared].chatroomchosenImages]) {
    if (self.haveNotCreateDialog) {
      [QBRequest createDialog:self.dialog
          successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {

            self.dialog = createdDialog;
            [[[[DBQBChatDialogDelete query] whereWithFormat:@" DialogID = %@", self.dialog.ID] fetch] removeAll];
            [[ChatRelationModel shared] markCreateChatRoomRecordToServerForDialogID:self.dialog.ID recipienterMemberID:self.recipienterMemberID];

            self.haveNotCreateDialog = NO;
            [self chatSettingBtnEnable];
            [self sendCoverViewMessageSaveIntoDatabaseAndGetNewestAgentProfileGetList];
            [[ChatRoomSendMessageModel shared] sendChatRoomChosenImagesForDialog:self.dialog];
          }
          errorBlock:^(QBResponse *response) {
            DDLogError(@"QBResponse %@", response);
          }];
    } else {
      if (self.secondTimeSendCoverViewMessage) {
        [self sendCoverViewMessageSaveIntoDatabaseAndGetNewestAgentProfileGetList];
        self.secondTimeSendCoverViewMessage = NO;
      }
      [[ChatRoomSendMessageModel shared] sendChatRoomChosenImagesForDialog:self.dialog];
    }
  }

  self.stickyHeaderView.hidenStatus = self.stickyHeaderView.hidden;
}
- (void)sendCoverViewMessageSaveIntoDatabaseAndGetNewestAgentProfileGetList {
    if ([[AppDelegate getAppDelegate].currentAgentProfile haveAgentListing]) {
  [[ChatRoomSendMessageModel shared] sendCoverViewMessageInsertIntoDataBaseForDialog:self.dialog];
  NSString *recipientQBID           = [self.dialog getRecipientIDFromQBChatDialog];
  NSMutableArray *chatlistQBidarray = [[NSMutableArray alloc] init];
  [chatlistQBidarray addObject:recipientQBID];
  [[DialogPageAgentProfilesModel shared] AgentProfileGetListtFromchatlistQBidarray:chatlistQBidarray
      numberofStartRecord:0
      success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
        [self.collectionView reloadData];
      //  [[ChatDialogModel shared] requestDialogsWithCompletionBlock:nil];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok){

      }];

  [self getAgentListingGetList];
        }
}
- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];

  DDLogDebug(@"viewWillDisappear");
  [[ChatService shared] stopGetRecipienterTimer];
  [self readAllReceivceMessage:1];
  if ([self.chatRoomPresentType isEqualToString:kChatRoomPresentPhoto] || [self.chatRoomPresentType isEqualToString:kChatRoomPresentSelectPhotoLibrary]) {
  } else {
    [ChatService shared].delegate = nil;
  }
  [self setDialogLastMessageFromChatroomLastMessage];
  [[[[ReceiveMessageCount query] whereWithFormat:@"DialogID = %@", self.dialog.ID] fetch] removeAll];
  [[[[DBUnreadMessageLabel query] whereWithFormat:@" DialogID = %@ And MemberID = %@", self.dialog.ID, [LoginBySocialNetworkModel shared].myLoginInfo.MemberID] fetch] removeAll];

  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [[self.delegate getTabBarController] setNavBarHidden:NO animated:NO];
}
#pragma mark Support Method
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
  self.messageTopLabelDateFormatter = [NSDateFormatter localeDateFormatter];
  [self.messageTopLabelDateFormatter setDateFormat:@"EEE, d MMM"];

  self.messageDateLabelFormatter = [NSDateFormatter localeDateFormatter];
  [self.messageDateLabelFormatter setupDateFormatter];
  self.Onlydayformat = [NSDateFormatter localeDateFormatter];
  [self.Onlydayformat setDateFormat:@"yyyy-MM-dd"];

  self.connectingServerView.connectingLabel.text = JMOLocalizedString(@"common_connecting", nil);

  NSString *sendString = JMOLocalizedString(@"chatroom__send_button", nil);

  // we hardcode the font size to 18 just to get the work done.  Need to work further on this.
  // Date: 2016-04-01
  // Author: Ken
  CGSize adjustedSize = [sendString getSizeWithFont:[UIFont systemFontOfSize:18.0f]];
  DDLogDebug(@"uifont %@", self.inputToolbar.contentView.rightBarButtonItem.titleLabel.font);

  [self.inputToolbar.contentView setRightBarButtonItemWidth:adjustedSize.width];
  [self.inputToolbar.contentView.textView setPlaceHolder:@""];
  [self.inputToolbar.contentView.rightBarButtonItem setTitle:JMOLocalizedString(@"chatroom__send_button", nil) forState:UIControlStateNormal];
}
- (void)setDialogLastMessageFromChatroomLastMessage {
  if (![self isEmpty:[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]]) {
    DBResultSet *rchatMessage = [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ And ErrorStatus = 0 And IsDeleteMessage = 0", self.dialog.ID] orderByDescending:@"DateSent"] limit:1] fetch];
    for (DBQBChatMessage *dbqbchatdialog in rchatMessage) {
      self.dialog.lastMessageText = [dbqbchatdialog getLastMessage];

      self.dialog.unreadMessagesCount = 0;
      self.dialog.lastMessageDate     = dbqbchatdialog.DateSent;
    }
    DBResultSet *r = [[[DBQBChatDialog query] whereWithFormat:@" DialogID = %@", self.dialog.ID] fetch];

    for (DBQBChatDialog *dbqbchatdialog in r) {
      dbqbchatdialog.LastMessageText = self.dialog.lastMessageText;
      dbqbchatdialog.QBChatDialog    = [NSKeyedArchiver archivedDataWithRootObject:[self.dialog copy]];
      dbqbchatdialog.LastMessageDate = self.dialog.lastMessageDate;

      [dbqbchatdialog commit];
    }
  } else {
    if (!isEmpty(self.dialog.ID)) {
      self.dialog.lastMessageText = @"";

      self.dialog.unreadMessagesCount = 0;
      self.dialog.lastMessageDate     = nil;
    }
  }
}
- (BOOL)isEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] && [(NSData *)thing length] == 0) || ([thing respondsToSelector:@selector(count)] && [(NSArray *)thing count] == 0);
}
- (void)chatSettingBtnEnable {
  if (self.haveNotCreateDialog)
    self.navigationItem.rightBarButtonItem.enabled = NO;
  else
    self.navigationItem.rightBarButtonItem.enabled = YES;
}
- (void)setUpViewUI {
  self.navigationItem.leftBarButtonItem = [self setupBackBarButton];

  // Set setting button
  if (![self.delegate.currentAgentProfile isCSTeam]) {
    self.navigationItem.rightBarButtonItem = [self setupSettingBarButtonItem];
  }

  //// titleView
  self.titleView            = [self setupTitleView];
  self.connectingServerView = [self setupConnectingServerView];

  // Set title

  [self setupContentViewLeftAndRightBarButtonEnable];

  self.refreshControl = [[UIRefreshControl alloc] init];
  [self.refreshControl addTarget:self action:@selector(loadMoreMessage) forControlEvents:UIControlEventValueChanged];
  [self.collectionView addSubview:self.refreshControl];
  NSMutableArray *jsqmessagsForDialogId2 = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];

  if ([[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", self.dialog.ID] orderByDescending:@"Id"] count] == jsqmessagsForDialogId2.count) {
    [self.refreshControl removeFromSuperview];
  }
  if ([self.delegate.currentAgentProfile isCSTeam]) {
    self.collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_conversation_real.png"]];
  } else {
    self.collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_conversation.png"]];
  }
}
- (UIBarButtonItem *)setupBackBarButton {
  UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_nav_bar_back"] style:UIBarButtonItemStylePlain target:self action:@selector(backPressed:)];
  backBarButtonItem.tintColor        = [UIColor whiteColor];
  return backBarButtonItem;
}
- (UIBarButtonItem *)setupSettingBarButtonItem {
  UIBarButtonItem *settingBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_nav_bar_chat_setting"] style:UIBarButtonItemStylePlain target:self action:@selector(openChatRelationPageButtonPressed:)];
  settingBarButtonItem.tintColor        = [UIColor whiteColor];
  return settingBarButtonItem;
}

- (NavigationBarTitleView *)setupTitleView {
  NavigationBarTitleView *titleView = [[[NSBundle mainBundle] loadNibNamed:@"NavigationBarTitleView" owner:self options:nil] objectAtIndex:0];
  //// connectingserverview
  titleView.agentPersonalPhoto.contentMode = UIViewContentModeScaleAspectFill;
  //    self.titleView.backgroundColor = [UIColor redColor];
  //    self.titleView.backgroundColor = [UIColor redColor];
  [titleView.agentPersonalPhoto loadImageURL:self.recipientPhotoUrl withIndicator:YES];
  [self.delegate setupcornerRadius:titleView.agentPersonalPhoto];
  if ([self.delegate.currentAgentProfile isCSTeam]) {
    titleView.agentPersonalPhoto.borderType = UIImageViewBorderTypeRealCSSmall;
  } else if ([self.delegate.currentAgentProfile getIsFollowing]) {
    titleView.agentPersonalPhoto.borderType = UIImageViewBorderTypeFollowingSmall;
  } else {
    titleView.agentPersonalPhoto.borderType = UIImageViewBorderTypeNone;
  }

  [titleView setFrame:CGRectMake(0, titleView.frame.origin.y, [[UIScreen mainScreen] bounds].size.width - self.navigationItem.rightBarButtonItem.image.size.width - self.navigationItem.leftBarButtonItem.image.size.width, titleView.frame.size.height)];
  titleView.titleLabel.text = self.chatroomNameString;
  return titleView;
}

- (NavigationBarConnectingServerViewView *)setupConnectingServerView {
  NavigationBarConnectingServerViewView *connectingServerView = [[[NSBundle mainBundle] loadNibNamed:@"NavigationBarConnectingServerViewView" owner:self options:nil] objectAtIndex:0];
  connectingServerView.agentPersonalPhoto.contentMode         = UIViewContentModeScaleAspectFill;
  [connectingServerView.agentPersonalPhoto loadImageURL:self.recipientPhotoUrl withIndicator:YES];
  [self.delegate setupcornerRadius:connectingServerView.agentPersonalPhoto];
  [connectingServerView.connectingLoading startAnimating];

  [connectingServerView setFrame:CGRectMake(0, connectingServerView.frame.origin.y, [[UIScreen mainScreen] bounds].size.width - self.navigationItem.rightBarButtonItem.image.size.width - self.navigationItem.leftBarButtonItem.image.size.width, connectingServerView.frame.size.height)];
  if ([self.delegate.currentAgentProfile isCSTeam]) {
    connectingServerView.agentPersonalPhoto.borderType = UIImageViewBorderTypeRealCS;
  } else if ([self.delegate.currentAgentProfile getIsFollowing]) {
    connectingServerView.agentPersonalPhoto.borderType = UIImageViewBorderTypeFollowingSmall;
  } else {
    connectingServerView.agentPersonalPhoto.borderType = UIImageViewBorderTypeNone;
  }

  return connectingServerView;
}
- (void)setupContentViewLeftAndRightBarButtonEnable {
  if ([ChatService shared].connecttoserversuccess) {
    [self.inputToolbar toggleSendButtonEnabled];
    self.inputToolbar.contentView.leftBarButtonItem.enabled = YES;

  } else {
    self.inputToolbar.contentView.rightBarButtonItem.enabled = NO;
    self.inputToolbar.contentView.leftBarButtonItem.enabled  = NO;
  }
}

#pragma mark MWPhotoBrowserDelegate-------------------------------------------------------------------------------------------------------------
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
  return self.mwPhotosBrowerPhotoArray.count;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
  if (index < self.mwPhotosBrowerPhotoArray.count) return [self.mwPhotosBrowerPhotoArray objectAtIndex:index];
  return nil;
}
- (void)getImageFromDBQBChatMessageToMWPhotoBrower {
  dispatch_queue_t backgroundQueue = dispatch_queue_create("com.mycompany.myqueue", 0);

  dispatch_barrier_async(backgroundQueue, ^{

    DBResultSet *r = [[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND PhotoType =%@", self.dialog.ID, @"1"] orderBy:@"Id"] fetch];

    self.mwPhotosBrowerPhotoArray = [[NSMutableArray alloc] init];
    for (DBQBChatMessage *p in r) {
      if (![self isEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
        NSURL *url = [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];
        [self.mwPhotosBrowerPhotoArray addObject:[MWPhoto photoWithURL:url]];
      }
    }

  });
}
- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser;
{
  self.chatRoomPresentType = @"None";
  [self.mwPhotoBrowser dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark TopBarLoginStatusView-----------------------------------------------------------------------------------------------------------------------
- (void)chatRoomConnectToServerSuccess {
  self.navigationItem.titleView = self.titleView;
     [self readAllReceivceMessage:1];
}
- (void)chatDidAccidentallyDisconnectNotification {
  self.navigationItem.titleView = self.connectingServerView;
}
////getRecipienterFromQBServer myloginstatus
- (void)getRecipienterFromQBServer {
  [QBRequest userWithID:[self.recipientID integerValue]
      successBlock:^(QBResponse *response, QBUUser *user) {

        //        DDLogInfo(@"getRecipienterFromQBServer recipienterlastlogintime-->%@",
        //                  user.lastRequestAt);

        self.recipienterLastLoginTime       = user.lastRequestAt;
        self.recipienterLastLoginTimeString = [user.lastRequestAt lastSeenDateString];

        NSError *err                                      = nil;
        [ChatService shared].recipienterQBUUserCustomData = [[QBUUserCustomData alloc] initWithString:user.customData error:&err];
        [[ChatService shared].recipienterQBUUserCustomData checkCustomDataValidate:user.customData];
        //        DDLogDebug(@"[ChatService shared].recipienterQBUUserCustomData %@",[ChatService shared].recipienterQBUUserCustomData);
        //       DDLogDebug(@"[ChatService shared].myQBUUserCustomData %@",[ChatService shared].myQBUUserCustomData);

        [[ChatRelationModel shared] getCurrentChatRoomRelationObjectFromLocalForDialogId:self.dialog.ID];

        if ((![ChatRelationModel shared].mySelfChatRoomRelation.Blocked && ![ChatRelationModel shared].recipienerChatRoomRelation.Blocked) || ![[ChatRelationModel shared].currentChatRoomRelationObject valid]) {
          if ([ChatService shared].myQBUUserCustomData.chatroomshowlastseen && [ChatService shared].recipienterQBUUserCustomData.chatroomshowlastseen) {
            [self.titleView showLastSeenLabel:YES aniamted:YES];
          } else {
            NSDate *timeAgoDate = [NSDate dateWithTimeIntervalSinceNow:0];

            if ([self.recipienterLastLoginTimeString isEqualToString:JMOLocalizedString(@"chatroom__online", nil)] || [self.recipienterLastLoginTimeString isEqualToString:timeAgoDate.timeAgoSinceNow]) {
              [self.titleView showLastSeenLabel:YES aniamted:YES];
            } else {
              [self.titleView showLastSeenLabel:NO aniamted:YES];
            }
          }
        } else {
          [self.titleView showLastSeenLabel:NO aniamted:YES];
        }
        self.titleView.typingLabel.text = self.recipienterLastLoginTimeString;

      }
      errorBlock:^(QBResponse *response) {
        DDLogError(@"QBResponse %@", response);
      }];
}
- (BOOL)hidesBottomBarWhenPushed {
  return YES;
}
- (void)backPressed:(UIBarButtonItem *)sender {
  self.messageCellTopLabelDate = nil;
  [self.inputToolbar.contentView.textView resignFirstResponder];
  [self.navigationController popViewControllerAnimated:YES];
}
- (void)stickyHeaderTapGesture:(UITapGestureRecognizer *)sender {
  NSArray *listingPathArray           = [self.houseDetailViewCellIndexPathArray copy];
  NSIndexPath *firstVisibleIndextPath = listingPathArray[[self lastHeaderPosition]];
  [self.collectionView scrollToItemAtIndexPath:firstVisibleIndextPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
}
- (void)showChatSettingView:(BOOL)show {
  self.chatSettingWillShow = show;
  if (show) {
    [self.chatSettingView setViewAlignmentInSuperView:ViewAlignmentTop padding:-self.chatSettingView.frame.size.height];
    self.dimView.alpha          = 0.0;
    self.dimView.hidden         = NO;
    self.chatSettingView.hidden = NO;
    [self.chatSettingView getchatRoomRelationAndButtonSetup];
    [UIView animateWithDuration:0.3
        animations:^{
          [self.chatSettingView setViewAlignmentInSuperView:ViewAlignmentTop padding:0];
          self.dimView.alpha = 1.0;
        }
        completion:^(BOOL finished) {
          if (finished) {
          }
        }];
  } else {
    [self.chatSettingView setViewAlignmentInSuperView:ViewAlignmentTop padding:0];
    self.dimView.alpha          = 1.0f;
    self.dimView.hidden         = NO;
    self.chatSettingView.hidden = NO;
    [UIView animateWithDuration:0.3
        animations:^{
          [self.chatSettingView setViewAlignmentInSuperView:ViewAlignmentTop padding:-self.chatSettingView.frame.size.height];
          self.dimView.alpha = 0.0f;
        }
        completion:^(BOOL finished) {
          if (finished) {
            self.dimView.hidden         = YES;
            self.chatSettingView.hidden = YES;
          }
        }];
  }
}

- (void)openChatRelationPageButtonPressed:(UIBarButtonItem *)sender {
    [self openChatRoomRelationPageHide:NO];
}
-(void)openChatRoomRelationPageHide:(BOOL)hide{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    if (!self.dimView) {
        self.dimView                                = [[UIView alloc] initWithFrame:self.view.frame];
        UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self.dimView addGestureRecognizer:letterTapRecognizer];
        self.dimView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.dimView.hidden          = hide;
        [self.view addSubview:self.dimView];
    }
    
    [self initChatSettingView];
    self.chatSettingView.hidden = YES;
    [self.view addSubview:self.chatSettingView];
    [self showChatSettingView:!self.chatSettingWillShow];
}
- (void)initChatSettingView {
  if (!self.chatSettingView) {
    self.chatSettingView = [[[NSBundle mainBundle] loadNibNamed:@"ChatRelationPage" owner:self options:nil] objectAtIndex:0];
    if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
      self.chatSettingView.muteButton.contentHorizontalAlignment  = UIControlContentHorizontalAlignmentRight;
      self.chatSettingView.blockButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    self.chatSettingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.chatSettingView.frame.size.height);
    //  [self.chatSettingView.exitChatButton addTarget:self action:@selector(exitChatButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.chatSettingView.blockButton addTarget:self action:@selector(blockButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.chatSettingView.muteButton addTarget:self action:@selector(muteButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.chatSettingView.clearChatButton addTarget:self action:@selector(clearChatButton:) forControlEvents:UIControlEventTouchUpInside];

    self.chatSettingView.dialog = self.dialog;
  }
}
- (void)knotificationChatRelationListUpdated {
  [self getRecipienterFromQBServer];
  //[self initChatSettingView];
  [[ChatRelationModel shared] getCurrentChatRoomRelationObjectFromLocalForDialogId:self.dialog.ID];
  [self.chatSettingView buttonSetUp];
}
- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
  [self openChatRelationPageButtonPressed:nil];
}
- (void)blockButton:(UIButton *)sender {
  if ([ChatRelationModel shared].recipienerChatRoomRelation.Blocked) {
    self.unBlockSheet = [[UIActionSheet alloc] initWithTitle:JMOLocalizedString(@"alert_confirm_unblock_title", nil) delegate:self cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil) destructiveButtonTitle:JMOLocalizedString(@"common__unblock", nil) otherButtonTitles:nil, nil];

    [self.unBlockSheet showFromToolbar:self.inputToolbar];
  } else {
    self.blockSheet =
        [[UIActionSheet alloc] initWithTitle:JMOLocalizedString(@"chatroom__block_confirmation_box", nil) delegate:self cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil) destructiveButtonTitle:JMOLocalizedString(@"chatroom__block_this_contact", nil) otherButtonTitles:nil, nil];

    [self.blockSheet showFromToolbar:self.inputToolbar];
  }
}
- (void)muteButton:(UIButton *)sender {
  if (!self.chatSettingView) {
      [self openChatRoomRelationPageHide:YES];
  }
  [self.chatSettingView pressMuteButton];
}
- (void)clearChatButton:(UIButton *)sender {
  UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:nil
                                         cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)

                                    destructiveButtonTitle:JMOLocalizedString(@"chatroom__clear_chat", nil)
                                         otherButtonTitles:nil, nil];

  as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;

  as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
    if (buttonIndex == 0) {
      // Mixpanel
      DBResultSet *r              = [[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@", self.dialog.ID] fetch];
      self.dialog.lastMessageDate = nil;
      self.dialog.lastMessageText = @"";
      for (DBQBChatMessage *dbQBChatMessage in r) {
        dbQBChatMessage.IsDeleteMessage = YES;
        [dbQBChatMessage commit];
      }
      [DBQBChatDialogClearMessage createDBQBChatDialogClearMessage:self.dialog];

      [[ChatService shared].chatroomJsqMessageArray setObject:[[NSMutableArray alloc] init] forKey:self.dialog.ID];

      [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRoomReloadCollectionView object:nil];
    };

  };
  [as showInView:self.view];
}
#pragma mark sendMessage-------------------------------------------------------------------------------------------------------------------------
- (void)pressSendButtonMethod {
  if (self.inputToolbar.contentView.textView.text.length == 0) {
    return;
  }

  if (self.haveNotCreateDialog) {  //use isDialogCreated
    if (self.createDialogProcessing) {  //use isDialogCreating
      //since dialog is being created, so I must return to avoid create double dialog.
      return;
    } else {
      self.createDialogProcessing = YES;
    }
    [QBRequest createDialog:self.dialog
        successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
          [[[[DBQBChatDialogDelete query] whereWithFormat:@" DialogID = %@", self.dialog.ID] fetch] removeAll];
          self.dialog = createdDialog;
          [[ChatRelationModel shared] markCreateChatRoomRecordToServerForDialogID:self.dialog.ID recipienterMemberID:self.recipienterMemberID];
          self.haveNotCreateDialog = NO;
          [self chatSettingBtnEnable];
          [JSQSystemSoundPlayer jsq_playMessageSentSound];
          self.createDialogProcessing = NO;
          [self sendCoverViewMessageSaveIntoDatabaseAndGetNewestAgentProfileGetList];
          [self sendMessageMethod];
          [self finishSendingMessageAnimated:YES];
        }
        errorBlock:^(QBResponse *response) {
          self.createDialogProcessing = NO;
          DDLogError(@"QBResponse %@", response);

        }];
  } else {
    if (self.secondTimeSendCoverViewMessage) {  //isSendTimeSendListingViewMessage   //isListingViewMessageSent
      [self sendCoverViewMessageSaveIntoDatabaseAndGetNewestAgentProfileGetList];

      self.secondTimeSendCoverViewMessage = NO;
    }

    [self sendMessageMethod];
    [JSQSystemSoundPlayer jsq_playMessageSentSound];

    [self finishSendingMessageAnimated:YES];
  }
}

- (void)sendMessageMethod {
  NSString *messageText = self.inputToolbar.contentView.textView.text;

  if (kDebugMode) {
    if ([messageText isEqualToString:@"//video8866"]) {
      [[ChatService shared] startConferenceWithType:QBRTCConferenceTypeVideo opponentIDs:self.dialog.occupantIDs];
    } else if ([messageText isEqualToString:@"//audio8866"]) {
      [[ChatService shared] startConferenceWithType:QBRTCConferenceTypeAudio opponentIDs:self.dialog.occupantIDs];
    } else if ([messageText isEqualToString:@"//audioAttachment8866"]) {
      /* [[ChatService shared]sendSystemIncompatibleMessage:ChatFeatureTypeAudioAttachment dialog:self.dialog];
       return;
       */
    } else if ([messageText isEqualToString:@"//videoAttachment8866"]) {
      /* [[ChatService shared]sendSystemIncompatibleMessage:ChatFeatureTypeVideoAttachment dialog:self.dialog];
       return;
       */
    } else if ([messageText isEqualToString:@"//systemMessageTest"]) {
      NSString *title             = @"System Error Message Test";
      NSString *message           = @"You Can customize the left and right button 's title and the url";
      NSString *goActionTitle     = @"Google";
      NSString *goActionURL       = @"http://google.com";
      NSString *cancelActionTitle = @"Apple";
      NSString *cancelActionURL   = @"http://www.apple.com/";

      [[ChatService shared] sendSystemErrorWithTitle:title message:message goActionTitle:goActionTitle goActionURL:goActionURL cancelActionTitle:cancelActionTitle cancelActionURL:cancelActionURL dialog:self.dialog];
    }
  }

  [[ChatRoomSendMessageModel shared] sendMessageInsertIntoDataBase:[self.inputToolbar.contentView.textView.text jsq_stringByTrimingWhitespace] forDialog:self.dialog];

  // Mixpanel
  NSString *myMemberId = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID.stringValue;
  NSString *receiverId = [ChatService shared].recipienterQBUUserCustomData.memberID;

#warning DC - temp fix for cannot get the member ID from QBUser, need to come back after db clean up
  if ([myMemberId length] > 0 && [receiverId length] > 0) {
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Sent Chat" properties:@{
                                              @"Receiver member ID" : receiverId,
                                              @"Agent name":[[AppDelegate getAppDelegate] getCurrentAgentProfileMemberName]
                                              }];

    [mixpanel track:@"Received Chat" properties:@{ @"Sender member ID" : myMemberId,
                                                   @"distinct_id" : receiverId,
                                                   @"Sender name" : [LoginBySocialNetworkModel shared].myLoginInfo.MemberName
                                                   }];
  }

  // Reload table
  [JSQSystemSoundPlayer jsq_playMessageSentSound];
  if ([self.delegate.currentAgentProfile isSuperCS]) {
    [[RealApiClient sharedClient] actionLogTypeWithChatMessage:messageText memberID:[receiverId integerValue]];
  }
  [self.collectionView reloadData];
  [self scrollToBottomAnimated:YES];
}

//// - JSQMessagesViewController method overrides didPressSendButton
- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
  if ([ChatRelationModel shared].recipienerChatRoomRelation.Blocked) {
    self.unBlockSheet = [[UIActionSheet alloc] initWithTitle:JMOLocalizedString(@"alert_confirm_unblock_title", nil) delegate:self cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil) destructiveButtonTitle:JMOLocalizedString(@"common__unblock", nil) otherButtonTitles:nil, nil];

    [self.unBlockSheet showFromToolbar:self.inputToolbar];
    [self.inputToolbar.contentView.textView resignFirstResponder];
    return;
  }

  [self pressSendButtonMethod];

  DDLogInfo(@"didPressSendButton");
}

- (void)didPressAccessoryButton:(UIButton *)sender {
  [self.inputToolbar.contentView.textView resignFirstResponder];
  if ([ChatRelationModel shared].recipienerChatRoomRelation.Blocked) {
    self.unBlockSheet = [[UIActionSheet alloc] initWithTitle:JMOLocalizedString(@"alert_confirm_unblock_title", nil) delegate:self cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil) destructiveButtonTitle:JMOLocalizedString(@"common__unblock", nil) otherButtonTitles:nil, nil];

    [self.unBlockSheet showFromToolbar:self.inputToolbar];
    [self.inputToolbar.contentView.textView resignFirstResponder];
    return;
  }
  self.chatRoomPresentType = kChatRoomPresentSelectPhotoLibrary;
  [kAppDelegate showImagePickerOn:self];
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
  if (buttonIndex == actionSheet.cancelButtonIndex) {
    return;
  }

  switch (buttonIndex) {
    case 0:
      if (actionSheet == self.sendPhotoMessageSheet) {
        [self.inputToolbar.contentView.textView resignFirstResponder];
        [kAppDelegate showImagePickerOn:self];
      }

      if (actionSheet == self.blockSheet) {
        if (!self.chatSettingView) {
            [self openChatRoomRelationPageHide:YES];
        }
        [self.chatSettingView pressBlockButton];
      }
      if (actionSheet == self.unBlockSheet) {
        if (!self.chatSettingView) {
          [self openChatRoomRelationPageHide:YES];
        }
        [self.chatSettingView pressUnBlockButton];
      }
      break;
  }
}
- (void)setUpBadSendMessageBarIfExitStatus {
  [[ChatRelationModel shared] getCurrentChatRoomRelationObjectFromLocalForDialogId:self.dialog.ID];

  if ([ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty) {
    return;
  }

  self.inputToolbar.contentView.badSendMessageView.hidden = [ChatRelationModel shared].recipienerChatRoomRelation.Exited ? NO : YES;
}

- (void)getAgentListingGetList {
  [AgentListingGetListModel shared].listingIDListArray = [[NSMutableArray alloc] init];
  DBResultSet *r                                       = [[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ And ChatMessageType = %@", self.dialog.ID, kChatMessageTypeAgentListing] orderBy:@"Id"] fetch];

  for (DBQBChatMessage *dbQBChatMessage in r) {
    NSString *agentlistingid = [dbQBChatMessage.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing];
    DDLogDebug(@"agentlistingid-->%@", agentlistingid);
    if (agentlistingid) {
      [[AgentListingGetListModel shared].listingIDListArray addObject:agentlistingid];
    }
  }
  [[AgentListingGetListModel shared] callAgentListingGetListModelSuccess:^(NSMutableArray<AgentListing> *AgentListings) {
    if (self.currentHeaderPosition < 0 && self.houseDetailViewCellIndexPathArray.count > 0) {
      NSArray *listingPathArray           = [self.houseDetailViewCellIndexPathArray copy];
      self.currentHeaderPosition          = [self lastHeaderPosition];
      NSIndexPath *firstVisibleIndextPath = listingPathArray[self.currentHeaderPosition];
      JSQMessage *message                 = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:firstVisibleIndextPath.row];
      AgentListing *listing               = [[AgentListingGetListModel shared] getAgentListByID:[[message.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing] intValue]];
      [self.stickyHeaderView configureWithAgentListing:listing];
    }
    [self.collectionView reloadData];

  }
      failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
      }];
}
- (void)getMessageCellTopLabelDateIndexPathArray {
  int indextPathrow                          = 0;
  self.messageCellTopLabelDateIndexPathArray = [[NSMutableArray alloc] init];
  self.houseDetailViewCellIndexPathArray     = [[NSMutableArray alloc] init];
  for (JSQMessage *jsqmessage in [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]) {
    if ([jsqmessage.messageType isEqualToString:kChatMessageTypeAgentListing]) {
      [self.houseDetailViewCellIndexPathArray addObject:[NSIndexPath indexPathForRow:indextPathrow inSection:0]];
    }

    if (self.messageCellTopLabelDateIndexPathArray.count == 0) {
      self.messageCellTopLabelDate = [self.Onlydayformat stringFromDate:jsqmessage.date];
      [self.messageCellTopLabelDateIndexPathArray addObject:[NSNumber numberWithInt:indextPathrow]];
    }

    if ([self.messageCellTopLabelDate isEqualToString:[self.Onlydayformat stringFromDate:jsqmessage.date]]) {
    } else {
      self.messageCellTopLabelDate = [self.Onlydayformat stringFromDate:jsqmessage.date];
      [self.messageCellTopLabelDateIndexPathArray addObject:[NSNumber numberWithInt:indextPathrow]];
    }
    indextPathrow++;
  }
}

- (void)loadMoreMessage {
  NSMutableArray *jsqmessagsForDialogId = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];

  NSUInteger jsqmessagesCount = jsqmessagsForDialogId.count;
  if ([[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ And ChatMessageType = %@", self.dialog.ID, kChatMessageTypeAgentListing] orderBy:@"Id"] count] == 0) {
    [[GetChatMessageToJsqMessageArrayModel shared] getEarlierChatMessageFromDBToJsqMessageArrayForDialog:self.dialog chatRoom:self];

  } else {
    [[GetChatMessageToJsqMessageArrayModel shared] getEarlierChatMessageeUntilAgentListingIDFromDBToJsqMessageArrayForDialog:self.dialog chatRoom:self];
  }
  //    [self getMessageCellTopLabelDateIndexPathArray];
  NSMutableArray *jsqmessagsForDialogId2 = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];

  NSIndexPath *whenLoadEarlierMessageCurrentPositionIndextPath = [NSIndexPath indexPathForRow:jsqmessagsForDialogId2.count - jsqmessagesCount inSection:0];

  [self.collectionView reloadData];
  [self.collectionView scrollToItemAtIndexPath:whenLoadEarlierMessageCurrentPositionIndextPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
  [self.refreshControl endRefreshing];
  if ([[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", self.dialog.ID] orderByDescending:@"Id"] count] == jsqmessagsForDialogId2.count) {
    [self.refreshControl removeFromSuperview];
  }
}

#pragma mark JSQMessages CollectionView DataSource----------------------------------------------------------------------------------------------
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
  NSMutableArray *mutableArray = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];
  if ([self isEmpty:mutableArray]) {
    return nil;
  }
  if (indexPath.item >= mutableArray.count) {
    return nil;
  }
  return [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  You may return nil here if you do not want bubbles.
   *  In this case, you should set the background color of your collection view
   *cell's textView.
   *
   *  Otherwise, return your previously created bubble image data objects.
   */

  JSQMessage *message = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];
  DDLogInfo(@"messageBubbleImageDataForItemAtIndexPath");

  if ([message.senderId isEqualToString:self.senderId]) {
    return [ChatService shared].outgoingBubbleImageData;
  }

  return [ChatService shared].incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
  return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  This logic should be consistent with what you return from
   *`heightForCellTopLabelAtIndexPath:`
   *  The other label text delegate methods should follow a similar pattern.
   *
   *  Show a timestamp for every 3rd message
   */
  JSQMessage *message = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];

  DDLogInfo(@"[_messageDateFormatter stringFromDate:message.date]-->%@", [_messageTopLabelDateFormatter stringFromDate:message.date]);
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:message.date];
    if (today) {
        return [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common__today", nil)];
    }
  if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"]) {
    return [[NSAttributedString alloc] initWithString:[_messageTopLabelDateFormatter stringFromDate:message.date]];
  } else {
    return [[NSAttributedString alloc] initWithString:[message.date formattedDateWithFormat:@"d/M/y"]];
  }
    
  return nil;
}
//// not use it.
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"attributedTextForMessageBubbleTopLabelAtIndexPath");

  JSQMessage *message = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];

  /**
   *  iOS7-style sender name labels
   */
  if ([message.senderId isEqualToString:self.senderId]) {
    return nil;
  }

  if (indexPath.item > 0) {
    JSQMessage *previousMessage = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];
    if ([[previousMessage senderId] isEqualToString:message.senderId]) {
      return nil;
    }
  }

  /**
   *  Don't specify attributes to use the defaults.
   */
  return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}
//// not use it.
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
  return nil;
}

//// - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  NSMutableArray *chatroomJsqMessageArray = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];
  DDLogDebug(@"numberOfItemsInSection %lu", (unsigned long)chatroomJsqMessageArray.count);
  return chatroomJsqMessageArray.count;
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  Override point for customizing cells
   */
  JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
  cell.textView.linkTextAttributes    = @{ NSForegroundColorAttributeName : [UIColor realBlueColor], NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
  DDLogInfo(@"cellForItemAtIndexPath-->%ld", (long)indexPath.item);
  JSQMessage *jsqmessage    = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];
  NSString *messageSenderId = [jsqmessage senderId];
  BOOL isOutgoingMessage    = [messageSenderId isEqualToString:self.senderId];

  [cell setBubbleContainerInset:UIEdgeInsetsMake(0, 20, 0, 20)];
  if (!cell.cellDateLabelView) {
    cell.cellDateLabelView = [[[NSBundle mainBundle] loadNibNamed:@"CellDateLabelView" owner:self options:nil] objectAtIndex:0];
    if ([jsqmessage.messageType isEqualToString:kChatMessageTypeAgentListing]) {
      cell.cellDateLabelView.hidden = YES;
    }
  }
#pragma mark if have agentlistingid
  if ([jsqmessage.messageType isEqualToString:kChatMessageTypeAgentListing]) {
    if (!cell.chatRoomHouseDetailView) {
      cell.chatRoomHouseDetailView = [[[NSBundle mainBundle] loadNibNamed:@"ChatRoomHouseDetailView" owner:self options:nil] objectAtIndex:0];

      [cell.chatRoomHouseDetailView setFrame:CGRectMake(cell.chatRoomHouseDetailView.frame.origin.x, 0, [UIScreen mainScreen].bounds.size.width, cell.chatRoomHouseDetailView.frame.size.height)];

      [cell addSubview:cell.chatRoomHouseDetailView];
    }

    cell.chatRoomHouseDetailView.contentViewTopSpacing.constant = 20;

    AgentListing *listing = [[AgentListingGetListModel shared] getAgentListByID:[[jsqmessage.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing] intValue]];

    [cell.chatRoomHouseDetailView configureWithAgentListing:listing];

    cell.chatRoomHouseDetailView.hidden    = NO;
    cell.messageBubbleContainerView.hidden = YES;
    cell.cellDateLabelView.hidden          = YES;

  } else {
    cell.chatRoomHouseDetailView.hidden    = YES;
    cell.cellDateLabelView.hidden          = NO;
    cell.messageBubbleContainerView.hidden = NO;
  }

  if ([jsqmessage.messageType isEqualToString:kChatMessageTypeRate]) {
    [cell.rateButton setHidden:NO];
  } else {
    [cell.rateButton setHidden:YES];
  }

#pragma mark DBUnreadMessageLabel
  if ([[[DBUnreadMessageLabel query] whereWithFormat:@" MessageID = %@", jsqmessage.MessageID] count] == 0) {
    cell.unreadMessageLabelView.hidden = YES;
  } else {
    DBResultSet *r = [[[DBUnreadMessageLabel query] whereWithFormat:@" MessageID = %@", jsqmessage.MessageID] fetch];

    for (DBUnreadMessageLabel *dbUnreadMessageLabel in r) {
      if (!cell.unreadMessageLabelView) {
        cell.unreadMessageLabelView = [[[NSBundle mainBundle] loadNibNamed:@"UnreadMessageLabelView" owner:self options:nil] objectAtIndex:0];

        [cell addSubview:cell.unreadMessageLabelView];
      }

      cell.unreadMessageLabelView.hidden = NO;
      if ([jsqmessage.messageType isEqualToString:kChatMessageTypeAgentListing]) {
        cell.unreadMessageLabelView.hidden = YES;
      }
      [cell.unreadMessageLabelView autoSetDimension:ALDimensionWidth toSize:[[UIScreen mainScreen] bounds].size.width];
      [cell.unreadMessageLabelView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:cell.cellTopLabel];
      NSString *unreadMessageLabelText;
      if (dbUnreadMessageLabel.UnReadMessageCount > 1) {
        unreadMessageLabelText = [NSString stringWithFormat:@"%d %@", dbUnreadMessageLabel.UnReadMessageCount, JMOLocalizedString(@"chatroom__unread_messages", nil)];
      } else {
        unreadMessageLabelText = [NSString stringWithFormat:@"%d %@", dbUnreadMessageLabel.UnReadMessageCount, JMOLocalizedString(@"chatroom__unread_message", nil)];
      }
      cell.unreadMessageLabelView.unReadMessageLabel.text = unreadMessageLabelText;
    }
  }
#pragma mark set Up cell DateLabel
  [cell.cellDateLabelView.datelabel setText:[self.messageDateLabelFormatter stringFromDate:jsqmessage.date]];
  CGSize cellSize = [self.collectionView.collectionViewLayout messageBubbleSizeForItemAtIndexPath:indexPath];
  if (cellSize.width <= [UIImage imageNamed:@"chat_bubble_blue"].size.width) {
    cell.textView.textAlignment = NSTextAlignmentCenter;
  } else {
    cell.textView.textAlignment = NSTextAlignmentLeft;
  }
  int errorStatusMoveToLeft;
#pragma mark Error Status Message
  if (jsqmessage.ErrorStatus) {
    [cell.errorMessageRetryButton setTag:indexPath.row];
    [cell.errorMessageRetryButton addTarget:self action:@selector(errorMessageRetryButton:) forControlEvents:UIControlEventTouchUpInside];

    cell.errorMessageRetryButton.hidden       = NO;
    cell.errorMessageRetryBtnWidth.constant   = 25;
    cell.errorMessageRetryBtnLeading.constant = 16;
    cell.errorMessageLabelTrailing.constant   = 20.0;
    errorStatusMoveToLeft                     = 25 + 6;
  } else {
    [cell.errorMessageRetryButton setTag:indexPath.row];
    [cell.errorMessageRetryButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    cell.errorMessageRetryButton.hidden       = YES;
    cell.errorMessageRetryBtnWidth.constant   = 14;
    cell.errorMessageRetryBtnLeading.constant = 6;
    cell.errorMessageLabelTrailing.constant   = 0.0;

    errorStatusMoveToLeft = 0;
  }
  //// set up rightsidemessagedatelabel
  if (isOutgoingMessage) {
    // optional:

    cell.textView.textColor = [UIColor colorWithRed:(78 / 255.0) green:(78 / 255.0) blue:(78 / 255.0) alpha:1.0];
    [cell.cellDateLabelView setupTickImageView:jsqmessage];
    //// add 0.1 white color overlay
    [cell.cellDateLabelView setFrame:CGRectMake(cell.frame.size.width - 105 - errorStatusMoveToLeft, cell.frame.size.height - 20, 60, 10)];
    if (cell.mediaView != nil) {
      [cell.cellDateLabelView.datelabel setTextColor:[UIColor blackColor]];
      [cell.cellDateLabelView setFrame:CGRectMake(cell.frame.size.width - 105 - errorStatusMoveToLeft, cell.frame.size.height - 40, 70, 10)];
      if (!cell.overlay) {
        cell.overlay = [[UIView alloc] initWithFrame:cell.cellDateLabelView.frame];

        [cell.overlay setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];

        [cell addSubview:cell.overlay];
        [cell.overlay addSubview:cell.cellDateLabelView];
      }
    }
  } else {
    cell.textView.textColor = [UIColor colorWithRed:(78 / 255.0) green:(78 / 255.0) blue:(78 / 255.0) alpha:1.0];
    //// set up leftidemessagedatelabel
    [cell.cellDateLabelView setFrame:CGRectMake(20, cell.frame.size.height - 20, 60, 10)];
    cell.datelabel.textAlignment = NSTextAlignmentRight;
    if (cell.mediaView != nil) {
      [cell.cellDateLabelView.datelabel setTextColor:[UIColor blackColor]];
      [cell.cellDateLabelView setFrame:CGRectMake(20, cell.frame.size.height - 40, 60, 10)];
      //// add 0.1 white color overlay

      if (!cell.overlay) {
        cell.overlay = [[UIView alloc] initWithFrame:cell.cellDateLabelView.frame];

        [cell.overlay setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];

        [cell addSubview:cell.overlay];
        [cell.overlay addSubview:cell.cellDateLabelView];
      }
    }
  }
  [cell addSubview:cell.cellDateLabelView];
  if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
    JSQMessagesCollectionViewLayoutAttributes *customAttributes = (JSQMessagesCollectionViewLayoutAttributes *)[self.collectionView layoutAttributesForItemAtIndexPath:indexPath];

    if (isOutgoingMessage) {
      DDLogDebug(@"cell.textView.frame.size.width  %f", cell.textView.frame.size.width);
      cell.bubbleContainerTrailing.constant = [UIScreen mainScreen].bounds.size.width - (customAttributes.messageBubbleContainerViewWidth + 20);

    } else {
      cell.bubbleContainerLeading.constant = [UIScreen mainScreen].bounds.size.width - (customAttributes.messageBubbleContainerViewWidth + 20);
    }
  }
  return cell;
}

#pragma mark JSQMessages collection view flow layout
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  for (NSNumber *messageCellTopLabelDateNumber in self.messageCellTopLabelDateIndexPathArray) {
    if (indexPath.row == [messageCellTopLabelDateNumber integerValue]) {
      return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
  }

  return 0;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  iOS7-style sender name labels
   */
  JSQMessage *jsqmessage = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];

  if ([[[DBUnreadMessageLabel query] whereWithFormat:@" MessageID = %@", jsqmessage.MessageID] count] == 0) {
    return 0;
  } else {
    return 40;
  }
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
  return 10;
}

#pragma mark Responding to collection view tap
- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
#warning DC - custom action for the texts, add it back when we need.
  //    if (action == @selector(customAction:)) {
  //        return YES;
  //    }

  return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
#warning DC - custom action for the texts, add it back when we need.
  //    if (action == @selector(customAction:)) {
  //        [self customAction:sender];
  //        return;
  //    }

  [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}
#pragma mark errorMessageRetryButton
- (void)errorMessageRetryButton:(UIButton *)sender {
  // NSIndexPath * buttonIndexPath = indexPath;
  int index = sender.tag;

  JSQMessage *jsqmessage = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:index];
  DBQBChatMessage *newestChatMessageFromDataBase;
  QBChatMessage *qbChatMessage;
  DBResultSet *r = [[[[DBQBChatMessage query] whereWithFormat:@" MessageID = %@", jsqmessage.MessageID] orderBy:@"Id"] fetch];
  for (DBQBChatMessage *dbqbChatMessage in r) {
    newestChatMessageFromDataBase = dbqbChatMessage;
    qbChatMessage                 = [NSKeyedUnarchiver unarchiveObjectWithData:newestChatMessageFromDataBase.QBChatHistoryMessage];
  }

  UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:nil
                                         cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)

                                    destructiveButtonTitle:JMOLocalizedString(@"chatroom__send_button", nil)
                                         otherButtonTitles:nil, nil];

  as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;

  as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
    if (buttonIndex == 0) {
      // Mixpanel
      [[ChatRoomSendMessageModel shared] sendMessageMethodDialog:self.dialog
                                                         message:qbChatMessage
                                                 completionBlock:^(id response, NSError *error) {

                                                   [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:(DBQBChatMessage *)response];
                                                   [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRoomReloadCollectionView object:nil];

                                                 }];
    }
  };

  [as showInView:self.view];
}

#warning DC - custom action for the texts, add it back when we need.

- (void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender {
  DDLogInfo(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"Tapped message bubble!");

  DDLogInfo(@"indexPath.row%ld", (long)indexPath.row);

  if ([self isEmpty:[[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item]]) {
    return;
  }

  [self.inputToolbar.contentView.textView resignFirstResponder];

  JSQMessage *jsqmessage = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];

  if ([self isEmpty:[jsqmessage.CustomParameters objectForKey:@"photourl"]]) {
    return;
  }

  id<JSQMessageMediaData> copyMediaData = jsqmessage.media;

  if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
    dispatch_queue_t backgroundQueue = dispatch_queue_create("com.mycompany.myqueue", 0);

    //        dispatch_barrier_async(backgroundQueue, ^{
    int imageCellIndex                          = 0;
    self.mwPhotoBrowser                         = [[MWPhotoBrowser alloc] initWithDelegate:self];
    self.mwPhotoBrowser.displayActionButton     = YES;
    self.mwPhotoBrowser.displayNavArrows        = NO;
    self.mwPhotoBrowser.displaySelectionButtons = NO;
    self.mwPhotoBrowser.zoomPhotosToFill        = YES;
    self.mwPhotoBrowser.alwaysShowControls      = NO;
    self.mwPhotoBrowser.autoPlayOnAppear        = NO;
    self.mwPhotoBrowser.enableGrid              = YES;
    self.mwPhotoBrowser.startOnGrid             = NO;

    for (MWPhoto *mwphoto in self.mwPhotosBrowerPhotoArray) {
      NSString *dbQBChatMessagePhotoUrlString = [jsqmessage.CustomParameters objectForKey:@"photourl"];

      if ([dbQBChatMessagePhotoUrlString isEqualToString:[mwphoto.photoURL absoluteString]]) {
        [self.mwPhotoBrowser setCurrentPhotoIndex:imageCellIndex];

        UINavigationController *nc           = [[UINavigationController alloc] initWithRootViewController:self.mwPhotoBrowser];
        nc.modalTransitionStyle              = UIModalTransitionStyleCrossDissolve;
        nc.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        self.presentPhotoBrowers             = YES;
        [self.delegate.rootViewController presentViewController:nc animated:YES completion:nil];
        self.chatRoomPresentType = kChatRoomPresentPhoto;
        break;
      }

      imageCellIndex++;
    }

    //        });
  }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation {
  [self.inputToolbar.contentView.textView resignFirstResponder];
  DDLogInfo(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark ChatServiceDelegate--------------------------------------------------------------------------------------------------------------
- (void)chatDidConnect {
  [ChatService shared].delegate               = self;
  [ChatService shared].connecttoserversuccess = YES;
  [self setupContentViewLeftAndRightBarButtonEnable];

  DBResultSet *r = [[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@", self.dialog.ID] orderBy:@"DateSent"]

      fetch];

  if (r.count == 0) {
    [[GetChatMessageToJsqMessageArrayModel shared] whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:self.dialog];

  } else {
    [[GetChatMessageToJsqMessageArrayModel shared] addMessagesFromQBToJSQMessageArrayForDialog:self.dialog];
  }
}
- (void)chatDidFailWithStreamError:(NSError *)error {
  [MBProgressHUD hideHUDForView:self.view animated:YES];
  self.delegate.disableViewGesture            = NO;
  [ChatService shared].connecttoserversuccess = NO;

  [self setupContentViewLeftAndRightBarButtonEnable];
}

- (void)viewDidLayoutSubviews {
  self.didLayoutSubView = YES;
}

- (BOOL)chatDidReceiveMessage:(QBChatMessage *)chatmessage {
  DDLogInfo(@"QBChatMessage *)message-->%@", chatmessage);

  if (chatmessage.senderID != self.dialog.recipientID) {
    return NO;
  }

    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        [[[[DBUnreadMessageLabel query] whereWithFormat:@" DialogID = %@ And MemberID = %@", self.dialog.ID, [LoginBySocialNetworkModel shared].myLoginInfo.MemberID] fetch] removeAll];
    }

  [[ChatService shared] whenReceiveMessageAddMessageToJsqmessageArray:chatmessage forDialogId:chatmessage.dialogID];
  //// photomessage  insert in to mwphotobrowerarray
  if (![self isEmpty:[chatmessage.customParameters objectForKey:@"photourl"]]) {
    [self.mwPhotosBrowerPhotoArray addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[chatmessage.customParameters objectForKey:@"photourl"]]]];
  }
  [self chatRoomCollectionViewscrollToBottomYesAnimated];

  if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
    [[QBChat instance] readMessage:chatmessage
                        completion:^(NSError *_Nullable error){

                        }];
  }

  if (![ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty && [ChatRelationModel shared].recipienerChatRoomRelation.Muted) {
  } else {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    AudioServicesPlaySystemSound(1003);
  }
  if ([[chatmessage.customParameters objectForKey:@"messageType"] valid]) {
    if ([[chatmessage.customParameters objectForKey:@"messageType"] isEqualToString:kChatMessageTypeAgentListing]) {
      [self getAgentListingGetList];
    }
  }
  return YES;
}
- (void)chatDidDeliverMessageWithID:(DBQBChatMessage *)message {
  if (![self isEmpty:[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]]) {
    NSMutableArray *messagearray = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];

    for (JSQMessage *jsqmessage in [messagearray reverseObjectEnumerator]) {
      if (!jsqmessage.DidDeliverStatus) {
        if ([jsqmessage.MessageID isEqualToString:message.MessageID]) {
          jsqmessage.DidDeliverStatus = YES;
        }
        if ([jsqmessage.DateSent compare:message.DateSent] == NSOrderedDescending) {
          DDLogInfo(@"date1 is later than date2");

        } else if ([jsqmessage.DateSent compare:message.DateSent] == NSOrderedAscending) {
          DDLogInfo(@"date1 is earlier than date2");
          jsqmessage.DidDeliverStatus = YES;
        } else {
          DDLogInfo(@"dates are the same");
          jsqmessage.DidDeliverStatus = YES;
        }
      }
    }
  }

  [self.collectionView reloadData];
}
- (void)chatRoomCollectionViewReloadOnly {
  [self.collectionView reloadData];
}
- (void)chatRoomCollectionViewscrollToBottomYesAnimated {
  [self.collectionView reloadData];
  if (self.presentPhotoBrowers) {
    self.presentPhotoBrowers = NO;
  } else {
    [self scrollToBottomAnimated:YES];
  }

  [MBProgressHUD hideHUDForView:self.view animated:YES];
  self.delegate.disableViewGesture = NO;
}
- (void)chatRoomDownLoadPhotoWaitingListToReloadView {
  if (!self.downloadPhotoWaitingListActive) {
    self.downloadPhotoWaitingListActive = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void) {
      [self.collectionView reloadData];
      self.downloadPhotoWaitingListActive = NO;
    });
  }
}
- (void)chatRoomGetImageFromDBQBChatMessageToMWPhotoBrower {
  [self getImageFromDBQBChatMessageToMWPhotoBrower];
}
- (void)chatDidReadMessageWithID:(DBQBChatMessage *)message {
  if (![self isEmpty:[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]]) {
    NSMutableArray *messagearray = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];

    for (JSQMessage *jsqmessage in [messagearray reverseObjectEnumerator]) {
      if (!jsqmessage.ReadStatus && jsqmessage.ErrorStatus == NO) {
        if ([jsqmessage.MessageID isEqualToString:message.MessageID]) {
          jsqmessage.ReadStatus = YES;
        }

        if ([jsqmessage.DateSent compare:message.DateSent] == NSOrderedDescending) {
          DDLogInfo(@"date1 is later than date2");

        } else if ([jsqmessage.DateSent compare:message.DateSent] == NSOrderedAscending) {
          DDLogInfo(@"date1 is earlier than date2");
          jsqmessage.ReadStatus = YES;

        } else {
          DDLogInfo(@"dates are the same");
          jsqmessage.ReadStatus = YES;
        }
      }
    }
  }

  [self.collectionView reloadData];
}
//// read all message
- (void)readAllReceivceMessage:(int)totalReadReceivceMessageCount {
  [QBRequest markMessagesAsRead:nil
                       dialogID:self.dialog.ID
                   successBlock:^(QBResponse *response) {

                   }
                     errorBlock:nil];

  DBResultSet *r = [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND ReadStatus =0 AND RecipientID = %@ ", self.dialog.ID, [LoginBySocialNetworkModel shared].myLoginInfo.QBID] orderByDescending:@"Id"] limit:totalReadReceivceMessageCount] fetch];
  for (DBQBChatMessage *dbchatmessage in r) {
    QBChatMessage *chatmessage = [NSKeyedUnarchiver unarchiveObjectWithData:dbchatmessage.QBChatHistoryMessage];
    DDLogDebug(@"chatmessage %@", chatmessage);

    [[QBChat instance] readMessage:chatmessage
                        completion:^(NSError *_Nullable error) {
                          if (!error) {
                            dbchatmessage.ReadStatus = YES;
                            [dbchatmessage commit];
                          }

                        }];
  }

  DBResultSet *r2 = [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND RecipientID = %@ ", self.dialog.ID, [LoginBySocialNetworkModel shared].myLoginInfo.QBID] orderByDescending:@"Id"] limit:1] fetch];
  for (DBQBChatMessage *dbchatmessage in r2) {
    QBChatMessage *chatmessage = [NSKeyedUnarchiver unarchiveObjectWithData:dbchatmessage.QBChatHistoryMessage];

    [[QBChat instance] readMessage:chatmessage
                        completion:^(NSError *_Nullable error){

                        }];
  }
}
- (void)readFiveReceivceMessage {
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        [self readAllReceivceMessage:5];
    }
}
- (NSInteger)lastHeaderPosition {
  NSInteger headerPosition = 0;
  CGFloat contentOffsetY   = self.collectionView.contentOffset.y;
  for (int i = (int)self.houseDetailViewCellIndexPathArray.count - 1; i >= 0; i--) {
    NSIndexPath *listIndexPath                                = self.houseDetailViewCellIndexPathArray[i];
    UICollectionViewLayoutAttributes *listIndexPathAttributes = [self.collectionView layoutAttributesForItemAtIndexPath:listIndexPath];
    if (contentOffsetY - kAgentListingHeaderPadding > listIndexPathAttributes.frame.origin.y) {
      headerPosition = i;
      break;
    }
  }
  return headerPosition;
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
  self.lastContentOffset = scrollView.contentOffset.y;
  //   DDLogDebug(@"self.lastContentOffset %f",self.lastContentOffset);
}
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//    self.lastContentOffset = scrollView.contentOffset.y;
//}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  NSMutableArray *mutableArray = [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID];
  if (self.didLayoutSubView && ![self isEmpty:mutableArray]) {
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    if (contentOffsetY == 0) {
      self.lastContentOffset = contentOffsetY;
      [self hideHeaderView];
    }
    // DDLogDebug(@"contentOffsetY %f",contentOffsetY);
    NSArray *listingPathArray = [self.houseDetailViewCellIndexPathArray copy];
    if (listingPathArray.count <= 0) {
      [self showStickyHeader:NO animated:YES contentOffsety:contentOffsetY];
      return;
    }
    if (self.currentHeaderPosition < 0) {
      self.currentHeaderPosition          = [self lastHeaderPosition];
      NSIndexPath *firstVisibleIndextPath = listingPathArray[self.currentHeaderPosition];
      JSQMessage *message                 = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:firstVisibleIndextPath.row];
      AgentListing *listing               = [[AgentListingGetListModel shared] getAgentListByID:[[message.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing] intValue]];
      [self.stickyHeaderView configureWithAgentListing:listing];
    }
    if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
      self.stickyHeaderView.apartmentNameLabel.textAlignment    = NSTextAlignmentRight;
      self.stickyHeaderView.apartmentAddressLabel.textAlignment = NSTextAlignmentRight;
    }

    if (self.lastContentOffset > scrollView.contentOffset.y) {
      if (scrollView.contentOffset.y + scrollView.frame.size.height > scrollView.contentSize.height) {
        //   self.lastContentOffset = scrollView.contentOffset.y;
        return;
      }
    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
      [self showStickyHeader:NO animated:YES contentOffsety:contentOffsetY];
      //  self.lastContentOffset = scrollView.contentOffset.y;
      return;
    }

    NSIndexPath *currentHeaderIndexPath                        = listingPathArray[self.currentHeaderPosition];
    NSIndexPath *previousHeadIndexPath                         = nil;
    NSIndexPath *nextHeaderIndexPath                           = nil;
    UICollectionViewLayoutAttributes *currentHeaderAttributes  = nil;
    UICollectionViewLayoutAttributes *previousHeaderAttributes = nil;
    UICollectionViewLayoutAttributes *nextHeaderAttributes     = nil;
    NSInteger previousIndex                                    = self.currentHeaderPosition - 1;
    NSInteger nextIndex                                        = self.currentHeaderPosition + 1;
    if (listingPathArray.count <= nextIndex) {
      nextIndex = listingPathArray.count - 1;
    }
    nextHeaderIndexPath  = listingPathArray[nextIndex];
    nextHeaderAttributes = [self.collectionView layoutAttributesForItemAtIndexPath:nextHeaderIndexPath];

    if (previousIndex < 0) {
      previousIndex = 0;
    }
    previousHeadIndexPath    = listingPathArray[previousIndex];
    previousHeaderAttributes = [self.collectionView layoutAttributesForItemAtIndexPath:previousHeadIndexPath];
    currentHeaderAttributes  = [self.collectionView layoutAttributesForItemAtIndexPath:currentHeaderIndexPath];
//    DDLogDebug(@"nextHeaderAttributesy %f",nextHeaderAttributes.frame.origin.y);
//   DDLogDebug(@"previousHeaderAttributes %f",previousHeaderAttributes.frame.origin.y);
//   DDLogDebug(@"currentHeaderAttributes %f",currentHeaderAttributes.frame.origin.y);

#pragma mark nexttListingHeader
#pragma mark AgentListing
    if (contentOffsetY - kAgentListingHeaderPadding + kAgentListingHeaderHeight >= nextHeaderAttributes.frame.origin.y) {
      CGFloat headerY = contentOffsetY - nextHeaderAttributes.frame.origin.y - kAgentListingHeaderPadding + kAgentListingHeaderHeight;

      //   self.headerViewTopSpaceConstraint.constant = - headerY;
      if (headerY > kAgentListingHeaderHeight) {
        if (self.lastContentOffset > scrollView.contentOffset.y) {
          [self showStickyHeader:YES animated:YES contentOffsety:contentOffsetY];
        }
        //  if (self.headerViewTopSpaceConstraint.constant != 0) {
        // self.headerViewTopSpaceConstraint.constant = 0;
        self.currentHeaderPosition = nextIndex;
        JSQMessage *message        = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:nextHeaderIndexPath.row];
        AgentListing *listing      = [[AgentListingGetListModel shared] getAgentListByID:[[message.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing] intValue]];
        [self.stickyHeaderView configureWithAgentListing:listing];
        //}
      } else {
        if (previousIndex != self.currentHeaderPosition) {
          NSInteger previousHeaderPosition = self.currentHeaderPosition;
          if (previousHeaderPosition == nextIndex) {
            previousHeaderPosition = previousIndex;
          }
          previousHeadIndexPath = listingPathArray[previousHeaderPosition];
          JSQMessage *message   = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:previousHeadIndexPath.row];
          AgentListing *listing = [[AgentListingGetListModel shared] getAgentListByID:[[message.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing] intValue]];
          [self.stickyHeaderView configureWithAgentListing:listing];
        } else if (previousIndex == 0 && self.currentHeaderPosition == 0 && self.houseDetailViewCellIndexPathArray.count <= 1) {
          [self showStickyHeader:NO animated:YES contentOffsety:contentOffsetY];
        }
      }
//      DDLogDebug(@"headerY:%f",-headerY);
#pragma mark currentagentListingHeader
#pragma mark Top

    } else if (contentOffsetY - kAgentListingHeaderPadding >= currentHeaderAttributes.frame.origin.y) {
      [self showStickyHeader:YES animated:YES contentOffsety:contentOffsetY];
      self.headerViewTopSpaceConstraint.constant = 0;
      currentHeaderIndexPath                     = listingPathArray[self.currentHeaderPosition];
      JSQMessage *message                        = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:currentHeaderIndexPath.row];
      AgentListing *listing                      = [[AgentListingGetListModel shared] getAgentListByID:[[message.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing] intValue]];
      [self.stickyHeaderView configureWithAgentListing:listing];
    } else if (contentOffsetY - kAgentListingHeaderPadding >= previousHeaderAttributes.frame.origin.y) {
      if (previousIndex == self.currentHeaderPosition) {
        [self showStickyHeader:NO animated:YES contentOffsety:contentOffsetY];
      } else {
        [self showStickyHeader:YES animated:YES contentOffsety:contentOffsetY];

        // self.headerViewTopSpaceConstraint.constant = 0;
        self.currentHeaderPosition = previousIndex;
        currentHeaderIndexPath     = listingPathArray[self.currentHeaderPosition];
        JSQMessage *message        = [[[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID] objectAtIndex:currentHeaderIndexPath.row];
        AgentListing *listing      = [[AgentListingGetListModel shared] getAgentListByID:[[message.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing] intValue]];
        //                [self.stickyHeaderView configureWithAgentListing:listing];
      }
#pragma mark Top
#pragma mark currentListingHeader
    } else if (currentHeaderAttributes.frame.origin.y > contentOffsetY - kAgentListingHeaderPadding && self.currentHeaderPosition == previousIndex) {
      [self showStickyHeader:NO animated:YES contentOffsety:contentOffsetY];

    }  //    DDLogDebug(@"lastContentOffset %f",self.lastContentOffset);
  }
}

- (void)showStickyHeader:(BOOL)show animated:(BOOL)animated contentOffsety:(CGFloat)contentOffsety {
  if (self.stickyHeaderView.hidenStatus == !show) {
    /* hidden state unchange */
  } else {
    //        self.stickyHeaderView.hidden =!show;
    if (show) {
      if (self.lastContentOffset - kTopBarShowDistance > contentOffsety) {
        self.lastContentOffset = contentOffsety;
        [self showHeaderView];
      }
    } else {
      if (self.lastContentOffset + kTopBarHideDistance < contentOffsety) {
        self.lastContentOffset = contentOffsety;

        [self hideHeaderView];
      }
    }

    /* animation show or hidden ,  self.headerViewTopSpaceConstraint*/
  }
}
- (void)showHeaderView {
  self.stickyHeaderView.hidenStatus          = NO;
  self.stickyHeaderView.hidden               = NO;
  self.headerViewTopSpaceConstraint.constant = -kAgentListingHeaderHeight;

  [self.stickyHeaderView layoutIfNeeded];
  [UIView animateWithDuration:0.15
      animations:^{
        self.headerViewTopSpaceConstraint.constant = 0;
        [self.stickyHeaderView layoutIfNeeded];
      }
      completion:^(BOOL finished){

      }];
}
- (void)hideHeaderView {
  self.stickyHeaderView.hidenStatus          = YES;
  self.headerViewTopSpaceConstraint.constant = 0;

  [self.stickyHeaderView layoutIfNeeded];
  [UIView animateWithDuration:0.15
      animations:^{
        self.headerViewTopSpaceConstraint.constant = -kAgentListingHeaderHeight;
        [self.stickyHeaderView layoutIfNeeded];

      }
      completion:^(BOOL finished) {
        self.stickyHeaderView.hidden = YES;

      }];
}
#pragma mark Text view
- (void)textViewDidBeginEditing:(UITextView *)textView {
  if (textView != self.inputToolbar.contentView.textView) {
    return;
  }

  [textView becomeFirstResponder];

  [self scrollToBottomAnimated:YES];
}
- (void)textViewDidChange:(UITextView *)textView {
  if (textView != self.inputToolbar.contentView.textView) {
    return;
  }
  [self setupContentViewLeftAndRightBarButtonEnable];
  [self.dialog sendUserIsTyping];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
  if (textView != self.inputToolbar.contentView.textView) {
    return;
  }

  [textView resignFirstResponder];
}

#pragma mark - <CTAssetsPickerControllerDelegate>

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
  // assets contains PHAsset objects.

  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

  PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
  requestOptions.resizeMode             = PHImageRequestOptionsResizeModeExact;
  requestOptions.deliveryMode           = PHImageRequestOptionsDeliveryModeHighQualityFormat;

  // this one is key
  requestOptions.synchronous = true;

  PHImageManager *manager = [PHImageManager defaultManager];
  NSMutableArray *images  = [NSMutableArray arrayWithCapacity:[assets count]];

  // assets contains PHAsset objects.
  __block UIImage *ima;

  for (PHAsset *asset in assets) {
    // Do something with the asset

    [manager requestImageForAsset:asset
                       targetSize:RealUploadPhotoSize
                      contentMode:PHImageContentModeDefault
                          options:requestOptions
                    resultHandler:^void(UIImage *image, NSDictionary *info) {
                      ima = image;
                    }];

    [images addObject:ima];
  }
  [ChatRoomSendMessageModel shared].chatroomchosenImages = images;
  self.chatRoomPresentType                               = @"None";
  [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker {
  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
  [picker.navigationController dismissViewControllerAnimated:YES completion:nil];
  self.chatRoomPresentType = @"None";
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(PHAsset *)asset {
  NSInteger max = 5;
  BOOL reachMax = picker.selectedAssets.count + 1 > max;
  if (reachMax) {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:JMOLocalizedString(@"alert_alert", nil) message:[NSString stringWithFormat:JMOLocalizedString(@"chatroom__max_photo", nil),  [NSString stringWithFormat: @"%ld", (long)max]] preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *action = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:nil];

    [alert addAction:action];

    [picker presentViewController:alert animated:YES completion:nil];
  }

  // limit selection to max
  return !reachMax;
}

@end
