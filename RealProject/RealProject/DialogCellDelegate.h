//
//  DialogCellDelegate.h
//  productionreal2
//
//  Created by Alex Hung on 11/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UITableViewCell,AgentProfile;
@protocol DialogCellDelegate <NSObject>

- (void) dialogcell:(UITableViewCell*)cell shouldFollow:(BOOL)follow agentProfile:(AgentProfile*)agentProfile;
@end
