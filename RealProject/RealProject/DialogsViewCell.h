//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>

// Views
#import "RealBouncyButton.h"
@class DialogsViewCell;

@protocol DialogsViewCellDelegate <NSObject>

@optional

- (void)cell:(DialogsViewCell *)cell followButtonDidPress:(UIButton *)button;
- (void)cell:(DialogsViewCell *)cell profileImageButtonDidPress:(UIButton *)button;
@end

@interface DialogsViewCell : UITableViewCell
@property(weak, nonatomic) IBOutlet UILabel *title;
@property(weak, nonatomic) IBOutlet UILabel *subtitle;
@property(weak, nonatomic) IBOutlet UILabel *date;
@property(weak, nonatomic) IBOutlet UILabel *numberOfLastMessageLabel;
@property(strong, nonatomic) IBOutlet UIView *numberOfLastMessageContainerView;
@property(strong, nonatomic) IBOutlet UIImageView *personalimageview;
@property (strong, nonatomic) IBOutlet UIButton *personalImageBtn;
@property(strong, nonatomic) IBOutlet UIImageView *muteicon;
@property(strong, nonatomic) IBOutlet UIImageView *blockicon;
@property (strong, nonatomic) IBOutlet RealBouncyButton *followButton;
@property (strong, nonatomic) IBOutlet UIView *bottomLine;
@property (strong, nonatomic) AgentProfile *agentProfile;
@property (weak, nonatomic) id<DialogsViewCellDelegate> delegate;
#pragma mark Message SetUp Method
- (void)setupcellmessageAgentProfile:(AgentProfile *)agentProfile chatDialog:(QBChatDialog *)chatDialog;
- (void)setupCellIconMuteAndBlockAndExitForDialogId:(NSString *)dialogId;
- (void)setupFollowButton:(AgentProfile *)AgentProfile;
-(void)setupUnReadMessageConutForchatDialog:(QBChatDialog *)chatDialog;


@end
