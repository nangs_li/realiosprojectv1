//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "DialogsViewCell.h"
#import "ChatRelationModel.h"
#import "UIImage+Utility.h"
#import "ReceiveMessageCount.h"
#import "UIImageView+Utility.h"
#import "ChatDialogModel.h"
@implementation DialogsViewCell

- (void)awakeFromNib {
    // Initialization code
    self.personalimageview.contentMode = UIViewContentModeScaleAspectFill;
    self.numberOfLastMessageContainerView.backgroundColor = [UIColor realBlueColor];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.numberOfLastMessageContainerView.layer.cornerRadius = self.numberOfLastMessageContainerView.frame.size.height/2;
    self.numberOfLastMessageContainerView.clipsToBounds = YES;
    
}
- (void)setupcellmessageAgentProfile:(AgentProfile *)agentProfile chatDialog:(QBChatDialog *)chatDialog{
    
    if ([agentProfile.MemberName valid])
        self.title.text = agentProfile.MemberName;
    
    
    DBResultSet *r = [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ AND IsDeleteMessage = 0", chatDialog.ID] orderByDescending:@"Id"] limit:1] fetch];
    if (isEmpty(r)) {
        [self.date setText:[self stringForTimeIntervalDateTime:chatDialog.lastMessageDate
                                                   NowDateTime:[NSDate date]]];
        self.subtitle.text=chatDialog.lastMessageText;
    }else{
        for (DBQBChatMessage * dbQBchatMessage in r) {
            
            
            [self.date setText:[self stringForTimeIntervalDateTime:dbQBchatMessage.DateSent
                                                       NowDateTime:[NSDate date]]];
            self.subtitle.text =  [[ChatDialogModel shared] getLastMessageFromQBChatDialog:chatDialog];
        }
        
        
    }
    
    
    
    if ([agentProfile.AgentPhotoURL valid])
        [self.personalimageview loadImageURL:[NSURL URLWithString:agentProfile.AgentPhotoURL] withIndicator:YES];
    
    
}
- (void)setupCellIconMuteAndBlockAndExitForDialogId:(NSString *)dialogId {
    [[ChatRelationModel shared]
     getCurrentChatRoomRelationObjectFromLocalForDialogId:dialogId];
    if ([ChatRelationModel shared].currentChatRoomRelationObjectIsEmpty) {
        if (!self.muteicon.hidden) {
            self.muteicon.hidden = YES;
        }
        if (!self.blockicon.hidden) {
            self.blockicon.hidden = YES;
        }
        
    }else{
    
    self.muteicon.hidden =
    [ChatRelationModel shared].recipienerChatRoomRelation.Muted ? NO : YES;
    self.blockicon.hidden =
    [ChatRelationModel shared].recipienerChatRoomRelation.Blocked ? NO : YES;
    }
    
    
//    if ( [ChatRelationModel shared].mySelfChatRoomRelation.Exited ||
//        [ChatRelationModel shared].recipienerChatRoomRelation.Exited) {
//        self.contentView.alpha=0.5;
//    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // super setSelected
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setupFollowButton:(AgentProfile *)agentProfile {
    
    _agentProfile = agentProfile;
    
    if ([self isEmpty: agentProfile.AgentListing] && ![agentProfile isCSTeam]) {
        self.followButton.hidden=YES;
        
    }else{
        [self.followButton addTarget:self
                              action:@selector(followPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
        self.followButton.hidden = NO;
    }
   CGSize followCGSize= [JMOLocalizedString(@"common__follow", nil) getSizeWithFont:[UIFont systemFontOfSize:18.0f]];
     CGSize followingCGSize= [JMOLocalizedString(@"common__following", nil) getSizeWithFont:[UIFont systemFontOfSize:18.0f]];
    CGSize finalFollowBtnCGSize;
    if (followCGSize.width>followingCGSize.width) {
        finalFollowBtnCGSize=followCGSize;
    }else{
        finalFollowBtnCGSize=followingCGSize;
    }
//    [self.followButton autoSetDimension:ALDimensionWidth toSize:finalFollowBtnCGSize.width];
    DDLogDebug(@"finalFollowBtnCGSize %f",finalFollowBtnCGSize.width);
//     [self.followButton setNeedsUpdateConstraints];
     DDLogDebug(@"self.followButton.frame.size.width %f",self.followButton.frame.size.width);
    self.followButton.titleLabel.adjustsFontSizeToFitWidth = true;
    self.followButton.titleLabel.minimumFontSize = 12;
    if ([agentProfile isCSTeam]) {
        [self.followButton setTitle:JMOLocalizedString(@"chat_lobby__real_team", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.followButton setBackgroundColor:[UIColor realGreenColor]];
        [self.followButton setImage:nil forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:nil forState:UIControlStateNormal];
        self.personalimageview.borderType =  UIImageViewBorderTypeRealCS;
        self.followButton.userInteractionEnabled = NO;
    }else if ([agentProfile getIsFollowing]) {
        [self.followButton setTitle:JMOLocalizedString(@"common__following", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.followButton setBackgroundColor:[UIColor colorWithRed:(0 / 255.0)
                                                              green:(153 / 255.0)
                                                               blue:(255 / 255.0)
                                                              alpha:1.0]];
        [self.followButton setImage:[UIImage imageNamed:@"btn_ico_follow_unfollow"]
                           forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:nil forState:UIControlStateNormal];
        self.personalimageview.borderType = UIImageViewBorderTypeFollowing;
        self.followButton.userInteractionEnabled = YES;
        
    } else {
        [self.followButton setTitle:JMOLocalizedString(@"common__follow", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor colorWithRed:(0 / 255.0)
                                                         green:(153 / 255.0)
                                                          blue:(255 / 255.0)
                                                         alpha:1.0]
                                forState:UIControlStateNormal];
        [self.followButton setBackgroundColor:[UIColor clearColor]];
        [self.followButton setImage:[UIImage imageNamed:@"btn_ico_follow_follow"]
                           forState:UIControlStateNormal];
        [self.followButton
         setBackgroundImage:
         [[UIImage imageWithBorder:[UIColor colorWithRed:(0 / 255.0)
                                                   green:(153 / 255.0)
                                                    blue:(255 / 255.0)
                                                   alpha:1.0]
                              size:CGSizeMake(20, 20)] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]
         forState:UIControlStateNormal];
        self.personalimageview.borderType = UIImageViewBorderTypeNone;
        self.followButton.userInteractionEnabled = YES;
    }
    if (agentProfile.MemberID ==
        [MyAgentProfileModel shared].myAgentProfile.MemberID) {
        [self.followButton
         setBackgroundColor:[AppDelegate getAppDelegate].Greycolor];
        self.followButton.userInteractionEnabled = NO;
    }
    
}
-(void)setupUnReadMessageConutForchatDialog:(QBChatDialog *)chatDialog{
    DBResultSet * r= [[[ReceiveMessageCount query]
                       whereWithFormat:@"DialogID = %@", chatDialog.ID] fetch];
    if (r.count==0) {
        self.numberOfLastMessageContainerView.hidden = YES;
    }else{
        for (ReceiveMessageCount * receiveMessageCount in r) {
            self.numberOfLastMessageContainerView.hidden = NO;
            self.numberOfLastMessageLabel.text = [NSString stringWithFormat:@"%d", receiveMessageCount.ReceiveMessageCount];
            [self layoutIfNeeded];
        }
    }
    
    
}
- (NSString *)stringForTimeIntervalDateTime:(NSDate *)dateTime
                                NowDateTime:(NSDate *)nowDateTime {
    //// only between day
    NSDateFormatter *Onlydayformat = [NSDateFormatter localeDateFormatter];
    [Onlydayformat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTimestring = [Onlydayformat stringFromDate:dateTime];
    NSString *nowDateTimestring = [Onlydayformat stringFromDate:nowDateTime];
    DDLogInfo(@"dateTimestring-->%@", dateTimestring);
    DDLogInfo(@"nowDateTimestring-->%@", nowDateTimestring);
    NSDate *dateTimeOnlydayformat = [Onlydayformat dateFromString:dateTimestring];
    NSDate *nowDateOnlydayformat =
    [Onlydayformat dateFromString:nowDateTimestring];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc]
                                     initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components =
    [gregorianCalendar components:NSCalendarUnitDay
                         fromDate:dateTimeOnlydayformat
                           toDate:nowDateOnlydayformat
                          options:NSCalendarWrapComponents];
    DDLogInfo(@"components.day-->%ld", (long)components.day);
    //// only between time
    
    //  NSInteger DayInterval;
    // NSInteger DayModules;
    if (components.day >= 7) {
        
        [Onlydayformat setDateFormat:@"d/M/y"];
        return [Onlydayformat stringFromDate:dateTime];
        
    } else if (components.day >= 2) {
        
        [Onlydayformat setDateFormat:@"EEEE"];
        return [Onlydayformat stringFromDate:dateTime];
        
    } else if (components.day == 1) {
        NSDate *timeAgoDate = [NSDate dateWithTimeIntervalSinceNow:-86400];
        
        return timeAgoDate.timeAgoSinceNow;
    }
    
    else {
        [Onlydayformat setupDateFormatter];
        return [Onlydayformat stringFromDate:dateTime];
    }
}

- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

- (void)followPressed:(UIButton *)sender {
    
    if ([_delegate respondsToSelector:@selector(cell:followButtonDidPress:)]) {
        
        [_delegate cell:self followButtonDidPress:sender];
    }
}

- (IBAction)profileImageButtonDidPress:(id)sender{
    if ([self.delegate respondsToSelector:@selector(cell:profileImageButtonDidPress:)]) {
        [self.delegate cell:self profileImageButtonDidPress:sender];
    }
}
@end
