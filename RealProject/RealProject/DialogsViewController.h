//
//  SecondViewController.h
//  sample-chat
//
//  Created by Igor Khomenko on 10/16/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Onepage.h"
#import "Threepage.h"
#import "MMDrawerController.h"
#import "AgentProfileViewController.h"
#import "ApartmentDetailViewController.h"
@interface DialogsViewController : BaseViewController

@property(strong, nonatomic) QBChatDialog *createdDialog;
#pragma mark swipe animation core
@property(nonatomic, assign) int currentddirection;
@property(nonatomic, strong) UIImageView *imageFromView;
@property(nonatomic, strong) AgentProfileViewController *Onepageviewcontroller;
@property(nonatomic, strong) ApartmentDetailViewController *Threepageviewcontroller;
@property(nonatomic, strong) NSIndexPath *indexPathForCell;
@property(nonatomic, assign) BOOL tableviewcellfade;
@property(nonatomic, retain) MMDrawerController *MMDrawerController;
@property (strong, nonatomic) IBOutlet UIButton *editButton;

@property (strong, nonatomic)  UIButton *inviteToChatButton;
#pragma mark UILabel
@property(strong, nonatomic) IBOutlet UILabel *connectinglabel;
@property(strong, nonatomic) IBOutlet UILabel *chatlabel;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property(strong, nonatomic) IBOutlet UIView *topBarView;
@property(strong, nonatomic) IBOutlet UIView *noAccountView;
@property(strong, nonatomic) IBOutlet UILabel *noAccountLabel;
#pragma mark dialogsTableViewOriginalFrame
@property(assign, nonatomic) CGRect dialogsTableViewOriginalFrame;
#pragma mark searchDialogResultArray
@property(nonatomic, strong) NSMutableArray *searchDialogResultArray;
@property(nonatomic, strong) NSMutableArray *searchDialogIDResultArray;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIView *tableViewSection1View;
@property (strong, nonatomic) IBOutlet UILabel *tableViewSection1ViewOtherNameLabel;
#pragma mark searchAgentTimer
@property(nonatomic, strong) NSTimer *searchAgentTimer;
@end
