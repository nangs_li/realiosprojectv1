
#import "DialogsViewController.h"
#import "Chatroom.h"
#import "DialogsViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "DBChatroomAgentProfiles.h"
#import "DBQBChatDialog.h"
#import "DBQBUUser.h"
#import "DBLogInfo.h"
#import "RDVTabBarItem.h"
#import "DBQBChatDialog.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "DialogPageAgentProfilesModel.h"
#import <Branch/Branch.h>
#import "ChatDialogModel.h"
#import "ChatRelationModel.h"
#import "DBQBChatDialogDelete.h"
#import "FollowAgentModel.h"
#import "UnFollowAgentModel.h"
#import "DialogPageSearchAgentProfilesModel.h"
#import "DialogsSearchAgentCell.h"
#import "RealUtility.h"
#import "DialogsInviteCell.h"
#import "DateTools.h"
#import "DeepLinkActionHanderModel.h"
#import "InviteAgentViewController.h"
#import "DBUnreadMessageLabel.h"
#import "PressChatButtonModel.h"
#import <CoreSpotlight/CoreSpotlight.h>
#import "UIScrollView+SVInfiniteScrolling.h"
// Controllers
#import "ViralViewController.h"

// Mixpanel
#import "Mixpanel.h"
#import "NSArray+SafeAccess.h"
@interface DialogsViewController ()<
UITableViewDelegate, UITableViewDataSource, MMDrawerTouchDelegate,MoveToCenterPage,
UISearchDisplayDelegate, UIActionSheetDelegate, DialogsViewCellDelegate, DialogsSearchAgentCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *dialogsTableView;
@end

@implementation DialogsViewController
//// init
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        // Custom initialization
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}
//// ViewController lyfe cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(spotLightSearchOpenRightProfilePage)
     name:kNotificationSpotLightSearchOpenRightProfilePage
     object:nil];
    self.searchDisplayController.searchResultsTableView.delegate = self;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(followOrUnfollowReloadTableView)
     name:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(chatRoomConnectToServerSuccess)
     name:kNotificationChatRoomConnectToServerSuccess
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(chatDidAccidentallyDisconnectNotification)
     name:kNotificationChatDidAccidentallyDisconnect
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dialogsUpdatedNotification)
     name:kNotificationDialogsUpdated
     object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didReapplyQBAccount) name:kNotificationDidReapplyQBAccount object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(knotificationChatRelationListUpdated)
     name:kNotificationChatRelationListUpdated
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(willEnterForegroundNotification)
     name:UIApplicationWillEnterForegroundNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(pushNotificationEnterChatRoom:)
     name:kPushNotificationEnterChatRoom
     object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(followOrUnfollowReloadTableView) name:kNotificationNewFeedNeedUpdate object:nil];
    [self.dialogsTableView setContentOffset:CGPointMake(0,44)];
    __weak DialogsViewController *weakSelf = self;
    [self tableViewReloadAnimationTableView:weakSelf.searchDisplayController.searchResultsTableView];
    [weakSelf.searchDisplayController.searchResultsTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[LoginBySocialNetworkModel shared]reapplyQBAccountIfNeed];
    if ([[DeepLinkActionHanderModel shared].action isEqualToString:kDeepLinkActionChat]) {
        [MBProgressHUD showHUDAddedTo:self.view  animated:NO];
        NSString *memberName = [DeepLinkActionHanderModel shared].MemberName;
        NSString *memberId = [DeepLinkActionHanderModel shared].MemberID;
        NSString *QBID = [DeepLinkActionHanderModel shared].QBID;
        [self.MMDrawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
            [self pressChatButtonFromQBID:QBID memberName:memberName memberId:memberId clearAllDeelLinkActionVar:YES];

        }];
        
    }
    //    [DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles=nil;
    [self.delegate getTabBarController].customNavBarController.delegate=self;
    if ([ChatService shared].connecttoserversuccess) {
        [self chatRoomConnectToServerSuccess];
    } else {
        [self chatDidAccidentallyDisconnectNotification];
    }
    if(self.MMDrawerController.openSide == MMDrawerSideNone){
        [self setupUpNavBar];
        [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
    }
    if(self.MMDrawerController.openSide == MMDrawerSideNone){
        [self showPageControl:[ChatDialogModel shared].dialogs.count>0 animated:self.MMDrawerController.openSide == MMDrawerSideNone];
    }
    if (self.MMDrawerController.openSide != MMDrawerSideNone) {
        [self.MMDrawerController openDrawerSide:self.MMDrawerController.openSide animated:NO completion:nil];
    }
    DDLogInfo(@"viewWillAppear:(BOOL)animateddialogsviewcontroller");
    self.MMDrawerController.touchDelegate = self;
    self.dialogsTableView.tableHeaderView =
    self.searchDisplayController.searchBar;
    //[self.dialogsTableView setContentOffset:CGPointMake(0,44)];
    
    //  self.searchDisplayController.searchBar.placeholder =
    //   JMOLocalizedString(@"chat_lobby__search_field_hint  ", nil);
    [self.searchDisplayController.searchResultsTableView
     setBackgroundColor:[UIColor clearColor]];
    [self.searchDisplayController.searchResultsTableView
     setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    //[self.dialogsTableView setContentOffset:CGPointMake(0, 44) animated:NO];
    
    ////get dialogs, dialogsagentprofile data from dbaccess
    
    [[ChatDialogModel shared] requestDialogsWithCompletionBlock:^{
       
        [self knotificationChatRelationListUpdated];
    }];
    
    [self.editButton setTitle: JMOLocalizedString(@"chatlobby__edit", nil) forState: UIControlStateNormal];
    [_dialogsTableView setEditing:NO animated:YES];
   // [self dialogsUpdatedNotification];
    self.dialogsTableView.userInteractionEnabled=YES;
     self.searchDisplayController.searchResultsTableView.userInteractionEnabled=YES;
    [self showNoAccountViewIfNeed];
}
-(void)pressChatButtonFromQBID:(NSString *)QBID
                    memberName:(NSString *)memberName  memberId:(NSString *)memberId clearAllDeelLinkActionVar:(BOOL)clearAllDeelLinkActionVar{
    [[PressChatButtonModel shared] pressChatButtonModelByQBID:QBID
                                                   MemberName:memberName
                                                     MemberID:memberId
                                                      success:^(Chatroom *chatRoom) {
                                                          
                                                          
                                                          [self.navigationController
                                                           pushViewController:chatRoom
                                                           animated:YES];
                                                          if (clearAllDeelLinkActionVar) {
                                                              [[DeepLinkActionHanderModel shared]clearAllDeelLinkActionVar];
                                                          }
                                                          [MBProgressHUD hideAllHUDsForView:self.view   animated:YES];
                                                      }
                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                                NSString *errorMessage, RequestErrorStatus errorStatus,
                                                                NSString *alert, NSString *ok) {
                                                          [[DeepLinkActionHanderModel shared]clearAllDeelLinkActionVar];
                                                          [MBProgressHUD hideAllHUDsForView:self.view   animated:YES];
                                                          [[AppDelegate getAppDelegate]
                                                           handleModelReturnErrorShowUIAlertViewByView:self.view
                                                           errorMessage:errorMessage
                                                           errorStatus:errorStatus
                                                           alert:alert
                                                           ok:ok];
                                                      }];
}
-(void)pushNotificationEnterChatRoom:(NSNotification *)notification{
    NSString * QBID = [notification.object objectForKey:@"QBID"];
    [MBProgressHUD showHUDAddedTo:self.view  animated:NO];
    [self.MMDrawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
          [self pressChatButtonFromQBID:QBID memberName:@""memberId:@"" clearAllDeelLinkActionVar:NO];
        
   
    }];
}
-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            navBarController.dialogChatLabel.text = JMOLocalizedString(@"chatlobby__title", nil);
            self.topBarView = navBarController.dialogNavBar;
            self.navBarContentView = self.topBarView;
            self.chatlabel = navBarController.dialogChatLabel;
            self.loading = navBarController.dialogLoadingView;
            self.connectinglabel = navBarController.dialogConnectingLabel;
            self.editButton = navBarController.dialogEditButton;
//            [self.editButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
//            [self.editButton addTarget:self action:@selector(btnEditPressed:) forControlEvents:UIControlEventTouchUpInside];
            self.inviteToChatButton=navBarController.dialogInviteButton;
            [navBarController.dialogInviteButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.dialogInviteButton addTarget:self action:@selector(invitePressed:) forControlEvents:UIControlEventTouchUpInside];
            if ([ChatService shared].connecttoserversuccess) {
                [self chatRoomConnectToServerSuccess];
            } else {
                [self chatDidAccidentallyDisconnectNotification];
            }
        }
    }];
    self.topBarView.alpha = 1.0;
    if ([ChatDialogModel shared].dialogs.count>0) {
        [self showPageControl:YES  animated:NO];
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(self.MMDrawerController.openSide == MMDrawerSideNone){
        [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
    }
    // [self removeallImageFromView];
    
    [_dialogsTableView setEditing:NO animated:YES];
  //  [self tableViewReloadAnimationTableView:self.dialogsTableView];
    [self showInfiniteRefreshIfNeed];
    [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
    [self.searchDisplayController.searchResultsTableView resetState];
    DDLogDebug(@"Debug %@",[SystemSettingModel shared].SystemSettings.GoogleAddressLanguageList);
    UIEdgeInsets tableInset = self.dialogsTableView.contentInset;
    tableInset.bottom = 49;
    [self.dialogsTableView setContentInset:tableInset];
    
    [self reloadEditCase];
    
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self showInfiniteRefreshIfNeed];
    [self tableViewReloadAnimationTableView:self.dialogsTableView];
    [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
    [self.searchDisplayController.searchResultsTableView resetState];
    
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showNoAccountViewIfNeed{
    BOOL hidden = [[LoginBySocialNetworkModel shared] qbAccountIsReady];
    if (!hidden) {
        self.chatlabel.hidden = NO;
        self.loading.hidden = YES;
        self.connectinglabel.hidden = YES;
        self.editButton.hidden = YES;
        self.inviteToChatButton.hidden = YES;
        self.noAccountView.hidden = NO;
         [self showPageControl:NO animated:YES];
    }else{
        self.noAccountView.hidden = YES;
        if ([ChatService shared].connecttoserversuccess) {
            [self chatRoomConnectToServerSuccess];
        } else {
            [self chatDidAccidentallyDisconnectNotification];
        }
    }
}
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    self.searchBar.placeholder =JMOLocalizedString(@"chat_lobby__search_agent", nil) ;
    //   [self.searchBar layoutSubviews];
    self.connectinglabel.text = JMOLocalizedString(@"common_connecting", nil);
    self.noAccountLabel.text = JMOLocalizedString(@"chatlobby__no_service", nil);
    self.connectinglabel.textAlignment = NSTextAlignmentCenter;
    if ([ChatService shared].connecttoserversuccess) {
        [self chatRoomConnectToServerSuccess];
    } else {
        [self chatDidAccidentallyDisconnectNotification];
    }
    id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
    [barButtonAppearanceInSearchBar setTitle:JMOLocalizedString(@"common__cancel", nil)];
}

#pragma mark searchDisplayController Delegate(Search Bar)-----------------------------------------------------------------------------------------------

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString {
    [self
     filterContentForSearchText:searchString
     scope:[[self.searchDisplayController
             .searchBar scopeButtonTitles]
            objectAtIndex:
            [self.searchDisplayController.searchBar
             selectedScopeButtonIndex]]];
    
    return YES;
}
- (void)searchDisplayControllerWillBeginSearch:
(UISearchDisplayController *)controller {
    [controller setValue:[NSNumber numberWithInt:UITableViewStyleGrouped]
                  forKey:@"_searchResultsTableViewStyle"];
    self.searchBar.userInteractionEnabled =YES;
    [self reloadEditCase];
}
- (void)searchDisplayControllerDidEndSearch:
(UISearchDisplayController *)controller {
    [self tableViewReloadAnimationTableView:self.dialogsTableView];
    self.dialogsTableView.hidden = NO;
    self.searchBar.userInteractionEnabled=YES;
     self.searchDisplayController.searchResultsTableView.userInteractionEnabled=YES;
    self.dialogsTableView.userInteractionEnabled=YES;
     [self reloadEditCase];
    
}
//// filterContentForSearchText
- (void)filterContentForSearchText:(NSString *)searchText
                             scope:(NSString *)scope {
    if (searchText.length > 0) {
        [self searchLocalLobbyForSearchText:searchText];
    }else{
        self.searchDialogResultArray = [[NSMutableArray alloc] init];
         [DialogPageSearchAgentProfilesModel shared].dialogPageSearchAgentProfiles = [[NSMutableArray alloc] init];
        [self.searchDisplayController.searchResultsTableView reloadData];
        return;
    }
    
    if (searchText.length > 1) {
        [self.searchAgentTimer invalidate];
        
        self.searchAgentTimer =
        [NSTimer scheduledTimerWithTimeInterval:0.3
                                         target:self
                                       selector:@selector(agentSearch)
                                       userInfo:nil
                                        repeats:NO];
    }
}
-(void)searchLocalLobbyForSearchText:(NSString *)searchText{
    self.searchDialogResultArray = [[NSMutableArray alloc] init];
    //  [DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles=nil;
    for (QBChatDialog *qbchatdialog in [ChatDialogModel shared].dialogs) {
        
        NSString * tableViewCellQBID =    [qbchatdialog getRecipientIDFromQBChatDialog];
        for (AgentProfile *agentprofile in [DialogPageAgentProfilesModel shared].dialogPageSearchAgentProfiles) {
            
            
            if ([agentprofile.QBID isEqualToString:[NSString stringWithFormat:@"%@", tableViewCellQBID]]) {
                if (isEmpty(agentprofile.MemberName)) {
                    qbchatdialog.name=@"";
                }else{
                    qbchatdialog.name = agentprofile.MemberName;
                }
                //  return agentprofile;
            }
            if ([DialogPageAgentProfilesModel shared].dialogPageSearchAgentProfiles.count==0) {
                qbchatdialog.name=@"";
            }
            //             for (DBQBUUser *user in r) {
            //            qbchatdialog.name = user.FullName;
        }
    }
    self.searchDialogIDResultArray =[[NSMutableArray alloc]init];
    for (QBChatDialog *qbchatdialog in [ChatDialogModel shared].dialogs) {
        NSArray *arr = [[searchText uppercaseString] componentsSeparatedByString:@" "];
        for (NSString * searchTextString in arr) {
            NSString *trimmedString = [searchTextString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (!([[qbchatdialog.name uppercaseString]
                   rangeOfString:trimmedString]
                  .location == NSNotFound)) {
                if (![self.searchDialogIDResultArray containsObject:qbchatdialog.ID]) {
                    [self.searchDialogIDResultArray addObject:qbchatdialog.ID];
                    
                }
                
            }
        }
    }
    for (QBChatDialog *qbchatdialog in [ChatDialogModel shared].dialogs) {
        
        if ([self.searchDialogIDResultArray containsObject:qbchatdialog.ID]) {
            [self.searchDialogResultArray addObject:qbchatdialog];
            
        }
    }

}
- (void)agentSearch {
    if (self.searchDisplayController.searchBar.text.length>0) {
        [[DialogPageSearchAgentProfilesModel shared]
         AgentSearch:self.searchDisplayController.searchBar.text
         success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
             
             
             
             [self showInfiniteRefreshIfNeed];
             [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
             [self.searchDisplayController.searchResultsTableView resetState];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorStatus,
                   NSString *alert, NSString *ok) {
             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                           errorMessage:nil
                                                            errorStatus:errorStatus
                                                                  alert:alert
                                                                     ok:ok];
             
             
             
             [self showInfiniteRefreshIfNeed];
             [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
             [self.searchDisplayController.searchResultsTableView resetState];
         }];
    }
    
}
- (void)searchDisplayController:(UISearchDisplayController *)controller
 willShowSearchResultsTableView:(UITableView *)tableView {
    [self.view bringSubviewToFront:self.topBarView];
    UIEdgeInsets tableInset = tableView.contentInset;
    tableInset.top = 64-20;
    tableInset.bottom = 49;
    [tableView setContentInset:tableInset];
    self.dialogsTableView.hidden = YES;
    [self reloadEditCase];
    self.chatlabel.text=JMOLocalizedString(@"chat_lobby_search__title", nil);
    
}
- (void)searchDisplayController:(UISearchDisplayController *)controller
  didShowSearchResultsTableView:(UITableView *)tableView {
    [self findAndHideSearchBarShadowInView:self.searchDisplayController
     .searchResultsTableView];
    [self.view bringSubviewToFront:self.topBarView];
}
- (void)searchDisplayController:(UISearchDisplayController *)controller
 willHideSearchResultsTableView:(UITableView *)tableView {
   [self reloadEditCase];
    self.chatlabel.text=JMOLocalizedString(@"chatlobby__title", nil);
    
}
- (void)searchDisplayController:(UISearchDisplayController *)controller
  didHideSearchResultsTableView:(UITableView *)tableView {
    //    [DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles=nil;
}
- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller{
    
    controller.searchBar.userInteractionEnabled=NO;
}
- (void)findAndHideSearchBarShadowInView:(UIView *)view {
    NSString *usc = @"_";
    NSString *sb = @"UISearchBar";
    NSString *sv = @"ShadowView";
    NSString *s = [[usc stringByAppendingString:sb] stringByAppendingString:sv];
    
    for (UIView *v in view.subviews) {
        if ([v isKindOfClass:NSClassFromString(s)]) {
            v.alpha = 0.0f;
        }
        [self findAndHideSearchBarShadowInView:v];
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (scrollView==self.searchDisplayController.searchResultsTableView) {
        
        UIEdgeInsets tableInset = self.searchDisplayController.searchResultsTableView.contentInset;
        if (tableInset.bottom!=49) {
            tableInset.top = 64-20;
            tableInset.bottom = 49;
            [self.searchDisplayController.searchResultsTableView setContentInset:tableInset];
        }
        
        
    }
}
#pragma mark Press Button
- (IBAction)invitePressed:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Invite to Chat @ Chat"];
    
    ViralViewController *viralViewController = [[ViralViewController alloc] initWithType:ViralViewControllerTypeChat];
    viralViewController.underLayViewController = self;
    [self.delegate overlayViewController:viralViewController onViewController:self];
}
- (void)presentUIActivityViewControllerSendDownRealUrlLinkAndThenOpenChatRoom:(NSString *)url {
    InviteAgentViewController *inviteVc =[[InviteAgentViewController alloc]initWithNibName:@"InviteAgentViewController" bundle:nil];
    inviteVc.underLayViewController = self;
    inviteVc.shareText = JMOLocalizedString(@"invite_to_chat__make_your_friends_smile", nil);
    inviteVc.shareURL = url;
    [self.delegate overlayViewController:inviteVc onViewController:self];
    //    NSArray *objectsToShare = @[ textToShare, [NSURL URLWithString:url] ];
    
    //    UIActivityViewController *activityVC =
    //    [[UIActivityViewController alloc] initWithActivityItems:objectsToShare
    //                                      applicationActivities:nil];
    //    [self presentViewController:activityVC animated:YES completion:nil];
}
#pragma mark dialogs Notifications(reload TableView)-----------------------------------------------------------------------------------------------------------------
- (void)dialogsUpdatedNotification {
    [self tableViewReloadAnimationTableView:self.dialogsTableView];
    [self createAllDialogSpotLightSearchChatItem];
    [[ChatDialogModel shared] updateChatBadgeNumber];
}

- (void)didReapplyQBAccount{
    [self showNoAccountViewIfNeed];
    if ([ChatService shared].connecttoserversuccess) {
        [self chatRoomConnectToServerSuccess];
    } else {
        [self chatDidAccidentallyDisconnectNotification];
    }
}

- (void)chatRoomConnectToServerSuccess {
    if ([[LoginBySocialNetworkModel shared] qbAccountIsReady]) {
        self.loading.hidden = YES;
        self.connectinglabel.hidden = YES;
        self.chatlabel.hidden = NO;
        [self reloadEditCase];
        self.inviteToChatButton.hidden = NO;
    }
}

- (void)chatDidAccidentallyDisconnectNotification {
    if ([[LoginBySocialNetworkModel shared] qbAccountIsReady]) {
     //   [self tableViewReloadAnimationTableView:self.dialogsTableView];
        self.loading.hidden = NO;
        self.connectinglabel.hidden = NO;
        self.chatlabel.hidden = YES;
       [self reloadEditCase];
        self.inviteToChatButton.hidden = NO;
    }
}

- (void)groupDialogJoinedNotification {
    [self tableViewReloadAnimationTableView:self.dialogsTableView];
}
- (void)willEnterForegroundNotification {
    [self tableViewReloadAnimationTableView:self.dialogsTableView];
}
- (IBAction)btnEditPressed:(id)sender {
    
    if (self.dialogsTableView.editing == NO) {
        [self editCase];
    } else {
        [self notEditCase];
    }
    
}
-(void)editCase{
    [self.editButton setTitle: JMOLocalizedString(@"error_message__done", nil) forState: UIControlStateNormal];
    [_dialogsTableView setEditing:YES animated:YES];
    self.delegate.disableViewGesture=YES;
    self.inviteToChatButton.hidden=YES;
    self.searchBar.userInteractionEnabled=NO;
}
-(void)notEditCase{
    [self.editButton setTitle: JMOLocalizedString(@"chatlobby__edit", nil) forState: UIControlStateNormal];
    [_dialogsTableView setEditing:NO animated:YES];
    self.delegate.disableViewGesture=NO;
    self.inviteToChatButton.hidden=NO;
    
    self.searchBar.userInteractionEnabled=YES;

}
-(void)reloadEditCase{
    if (self.searchDisplayController.active) {
        [self.editButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        self.editButton.hidden=YES;
        [self notEditCase];
    }else{
        [self.editButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [self.editButton addTarget:self action:@selector(btnEditPressed:) forControlEvents:UIControlEventTouchUpInside];
        self.editButton.hidden=NO;
        [self notEditCase];
    }

}
#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.searchDisplayController.active&&self.searchBar.text.length>0) {
        return 3;
    } else {
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (self.searchDisplayController.active&&self.searchBar.text.length>0) {
        if (section == 0) {
            return self.searchDialogResultArray.count;
        }
        
        
        if (section == 1) {
            return [DialogPageSearchAgentProfilesModel shared]
            .dialogPageSearchAgentProfiles.count;
        }
        if (section == 2) {
            return 1;
        }
    }
    ////    if ([self isEmpty:[DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles]) {
    return [ChatDialogModel shared].dialogs.count;
    ////    }else{
    //        return [DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles.count;
    //
    ////    }
}
- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // reuse table view cell  if exist or create new one(ClinicTableViewCell)
    DDLogInfo(@"cellForRowAtIndexPath-->%ld", (long)indexPath.row);
    //    if ([self isEmpty:[DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles]) {
    if (indexPath.section == 0) {
        DialogsViewCell *cell = (DialogsViewCell *)
        [tableView dequeueReusableCellWithIdentifier:@"DialogsViewCell"];
        if (cell == nil) {
            // table view cell setting
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DialogsViewCell"
                                                         owner:self
                                                       options:nil];
            
            cell = [nib objectAtIndex:0];
        }
        QBChatDialog *chatDialog = nil;
        
        if (self.searchDisplayController.active&&self.searchBar.text.length>0) {
            if (indexPath.row>=self.searchDialogResultArray.count||[self isEmpty:self.searchDialogResultArray]) {
                cell.hidden=YES;
                return cell;
            }else{
                cell.hidden = NO;
            }
            chatDialog = self.searchDialogResultArray[indexPath.row];
            
            
            if (self.searchDialogResultArray.count-1==indexPath.row) {
                cell.bottomLine.hidden=YES;
            }else{
                cell.bottomLine.hidden = NO;
            }
            
            
        }else{
            cell.bottomLine.hidden = NO;
            if (indexPath.row>=[ChatDialogModel shared].dialogs.count||[self isEmpty:[ChatDialogModel shared].dialogs]) {
                cell.hidden=YES;
                return cell;
            }else{
                cell.hidden = NO;
            }
            chatDialog = [ChatDialogModel shared].dialogs[indexPath.row];
            
            
        }
        
        
        [cell setupcellmessageAgentProfile:[self getAgentProfileFromTableViewCellIndexPath:indexPath.row] chatDialog:chatDialog];
        [cell setupCellIconMuteAndBlockAndExitForDialogId:chatDialog.ID];
        
        [cell setupFollowButton:[self getAgentProfileFromTableViewCellIndexPath:indexPath.row]];
        cell.delegate = self;
        cell.personalImageBtn.tag = indexPath.row;
        
        
        
        [self.delegate setupcornerRadius:cell.personalimageview];
        
        [cell setupUnReadMessageConutForchatDialog:chatDialog];
        
        //// create separator line
        
        
        return cell;
    }
    
    ////    }else{
    //        if (indexPath.section == 0) {
    //
    //
    //        DialogsSearchAgentCell *searchAgentCell = (DialogsSearchAgentCell *)
    //        [tableView dequeueReusableCellWithIdentifier:@"DialogsSearchAgentCell"];
    //        if (searchAgentCell == nil) {
    //            // table view cell setting
    //            NSArray *nib =
    //            [[NSBundle mainBundle] loadNibNamed:@"DialogsSearchAgentCell"
    //                                          owner:self
    //                                        options:nil];
    //
    //            searchAgentCell = [nib objectAtIndex:0];
    //        }
    //                   //// cell setting.
    //            AgentProfile *AgentProfile =
    //            [DialogPageAgentProfilesModel shared]
    //            .dialogPageSpotLightSearchAgentProfiles[indexPath.row];
    //
    //            if (![self isEmpty:AgentProfile.AgentListing.GoogleAddress]) {
    //                GoogleAddress *googleAddress =
    //                [AgentProfile.AgentListing.GoogleAddress objectAtIndex:0];
    //                NSString *streetstring = googleAddress.Location;
    //                searchAgentCell.agentlistingstreetname.text = streetstring;
    //            }
    //            [searchAgentCell setupcellmessage:AgentProfile.MemberName subtitle:@""];
    //            NSURL *url = [NSURL URLWithString:AgentProfile.AgentPhotoURL];
    //            [searchAgentCell.personalimageview sd_setImageWithURL:url
    //                                                 placeholderImage:nil];
    //            [self.delegate setupcornerRadius:searchAgentCell.personalimageview];
    //
    //            [searchAgentCell setupFollowButton:AgentProfile];
    //            searchAgentCell.selectionStyle = UITableViewCellSelectionStyleNone;
    //            [searchAgentCell.followButton addTarget:self
    //                                             action:@selector(pressFollowOrUnfollow:)
    //                                   forControlEvents:UIControlEventTouchUpInside];
    //            searchAgentCell.followButton.tag = indexPath.row;
    //            [searchAgentCell.agentImageBtn addTarget:self
    //                                              action:@selector(pressAgentImageBtn:)
    //                                    forControlEvents:UIControlEventTouchUpInside];
    //            searchAgentCell.agentImageBtn.tag = indexPath.row;
    //
    //            return searchAgentCell;
    //
    // }
    //
    ////    }
    
    
    if (indexPath.section == 1) {
        DialogsSearchAgentCell *searchAgentCell = (DialogsSearchAgentCell *)
        [tableView dequeueReusableCellWithIdentifier:@"DialogsSearchAgentCell"];
        if (searchAgentCell == nil) {
            // table view cell setting
            NSArray *nib =
            [[NSBundle mainBundle] loadNibNamed:@"DialogsSearchAgentCell"
                                          owner:self
                                        options:nil];
            
            searchAgentCell = [nib objectAtIndex:0];
        }
        if (![self isEmpty:[DialogPageSearchAgentProfilesModel shared]
              .dialogPageSearchAgentProfiles]) {
            //// cell setting.
            AgentProfile *AgentProfile =
            [[DialogPageSearchAgentProfilesModel shared]
            .dialogPageSearchAgentProfiles objectWithIndex:indexPath.row];
            [searchAgentCell configureWithAgentProfile:AgentProfile];
            [self.delegate setupcornerRadius:searchAgentCell.personalimageview];
            [searchAgentCell setupFollowButton:AgentProfile];
            searchAgentCell.delegate = self;
            [searchAgentCell.agentImageBtn addTarget:self
                                              action:@selector(pressAgentImageBtn:)
                                    forControlEvents:UIControlEventTouchUpInside];
            searchAgentCell.agentImageBtn.tag = indexPath.row;
            
            return searchAgentCell;
        }
    }
    if (indexPath.section == 2) {
        DialogsInviteCell *cell = (DialogsInviteCell *)
        [tableView dequeueReusableCellWithIdentifier:@"DialogsInviteCell"];
        if (cell == nil) {
            // table view cell setting
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DialogsInviteCell"
                                                         owner:self
                                                       options:nil];
            
            cell = [nib objectAtIndex:0];
        }
        [cell setupLanguage];
        
        [cell.inviteButton
         addTarget:self
         action:@selector(invitePressed:)
         forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.dialogsTableView.userInteractionEnabled=NO;
     self.searchDisplayController.searchResultsTableView.userInteractionEnabled=NO;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0) {
        self.delegate.currentAgentProfile=  [self getAgentProfileFromTableViewCellIndexPath:indexPath.row];
        QBChatDialog *chatDialog;
        if([ChatDialogModel shared].dialogs.count>indexPath.row){
       chatDialog = [ChatDialogModel shared].dialogs[indexPath.row];
        }else{
            chatDialog=nil;
        }
        
        if (self.searchDisplayController.active) {
            if(self.searchDialogResultArray.count>indexPath.row){
                chatDialog = self.searchDialogResultArray[indexPath.row];
            }else{
                chatDialog=nil;
            }

        }
        
        Chatroom *vc = [Chatroom messagesViewController];
        AgentProfile * agentprofile=    [self getAgentProfileFromTableViewCellIndexPath:indexPath.row];
        vc.chatroomNameString=agentprofile.MemberName;
        vc.recipientPhotoUrl=[NSURL URLWithString:agentprofile.AgentPhotoURL];
        vc.recipientID = [NSNumber numberWithInteger:chatDialog.recipientID];
        vc.dialog = chatDialog;
        [self showPageControl:NO  animated:NO];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    if (indexPath.section==1) {
        self.delegate.currentAgentProfile=
        [DialogPageSearchAgentProfilesModel shared]
        .dialogPageSearchAgentProfiles[indexPath.row];
        
        
        if ([self.delegate networkConnection]) {
            [self loadingAndStopLoadingAfterSecond:10];
        }
        
        [[PressChatButtonModel shared]
         pressChatButtonModelByQBID:self.delegate.currentAgentProfile.QBID MemberName:self.delegate.currentAgentProfile.MemberName  MemberID:[NSString stringWithFormat:@"%d",self.delegate.currentAgentProfile.MemberID]
         success:^(Chatroom *chatroom) {
             //[self showPageControl:NO animated:NO];
             
             [self.navigationController pushViewController:chatroom animated:YES];
             self.dialogsTableView.userInteractionEnabled=YES;
              self.searchDisplayController.searchResultsTableView.userInteractionEnabled=YES;
             [self hideLoadingHUD];
             
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                   NSString *ok) {
             [self hideLoading];
             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                           errorMessage:errorMessage
                                                            errorStatus:errorStatus
                                                                  alert:alert
                                                                     ok:ok];
             self.dialogsTableView.userInteractionEnabled=YES;
              self.searchDisplayController.searchResultsTableView.userInteractionEnabled=YES;
         }];
        
    }
    
}
//// stringForTimeIntervalSinceCreated
- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AgentProfile *agentProfile = [self getAgentProfileFromTableViewCellIndexPath:indexPath.row];
    
    if (_dialogsTableView.editing && !agentProfile.isCSTeam) {
        return YES;
    }else{
        return NO;
    }
    
}
//- (void)tableView:(UITableView *)tableView
//commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
//forRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView beginUpdates];
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        QBChatDialog *chatDialog = [ChatDialogModel shared].dialogs[indexPath.row];
//        
//        DBQBChatDialogDelete *dbQBChatDialogDelte = [DBQBChatDialogDelete new];
//        
//        dbQBChatDialogDelte.DialogID = chatDialog.ID;
//        
//        dbQBChatDialogDelte.QBChatDialog =
//        [NSKeyedArchiver archivedDataWithRootObject:[chatDialog copy]];
//        dbQBChatDialogDelte.MemberID =
//        [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
//        
//        dbQBChatDialogDelte.QBID =
//        [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
//        [dbQBChatDialogDelte commit];
//        [[ChatRelationModel shared] chatRoomDeleteFromDialogId:chatDialog.ID
//                                                  deleteStatus:@"1"
//                                                       Success:^(id responseObject) {
//                                                           
//                                                       }
//                                                       failure:^(AFHTTPRequestOperation *operation, NSError *error,
//                                                                 NSString *errorMessage, RequestErrorStatus errorStatus,
//                                                                 NSString *alert, NSString *ok) {
//                                                           [self.delegate
//                                                            handleModelReturnErrorShowUIAlertViewByView:self.view
//                                                            errorMessage:errorMessage
//                                                            errorStatus:errorStatus
//                                                            alert:alert
//                                                            ok:ok];
//                                                       }];
//        DBResultSet *r = [[[DBQBChatMessage query]
//                           whereWithFormat:@" DialogID = %@", chatDialog.ID] fetch];
//        
//        for (DBQBChatMessage *dbQBChatMessage in r) {
//            dbQBChatMessage.IsDeleteMessage = YES;
//            [dbQBChatMessage commit];
//        }
//        [[ChatDialogModel shared].dialogs removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil]
//                         withRowAnimation:UITableViewRowAnimationFade];
//    }
//    [tableView endUpdates];
//}
- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section {
    if (tableView == [[self searchDisplayController] searchResultsTableView]) {
        if (section==1) {
            return 30;
        }
        
        return 0;
    } else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView
viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        self.tableViewSection1ViewOtherNameLabel.text = [NSString
                                                         stringWithFormat:JMOLocalizedString(@"chat_lobby_search__other_agents", nil),
                                                         self.searchDisplayController.searchBar.text];
        self.tableViewSection1View.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.tableViewSection1View.frame.size.height);
        return self.tableViewSection1View;
    }
    
    
    return nil;
}

-(BOOL) tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:JMOLocalizedString(@"chat_lobby__delete", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        [tableView beginUpdates];
                                      
                                            QBChatDialog *chatDialog = [ChatDialogModel shared].dialogs[indexPath.row];
                                            
                                            DBQBChatDialogDelete *dbQBChatDialogDelte = [DBQBChatDialogDelete new];
                                            
                                            dbQBChatDialogDelte.DialogID = chatDialog.ID;
                                            
                                            dbQBChatDialogDelte.QBChatDialog =
                                            [NSKeyedArchiver archivedDataWithRootObject:[chatDialog copy]];
                                            dbQBChatDialogDelte.MemberID =
                                            [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
                                            
                                            dbQBChatDialogDelte.QBID =
                                            [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
                                            [dbQBChatDialogDelte commit];
                                            [[ChatRelationModel shared] chatRoomDeleteFromDialogId:chatDialog.ID
                                                                                      deleteStatus:@"1"
                                                                                           Success:^(id responseObject) {
                                                                                               
                                                                                           }
                                                                                           failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                                                                     NSString *errorMessage, RequestErrorStatus errorStatus,
                                                                                                     NSString *alert, NSString *ok) {
                                                                                               [self.delegate
                                                                                                handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                                                errorMessage:nil
                                                                                                errorStatus:errorStatus
                                                                                                alert:alert
                                                                                                ok:ok];
                                                                                           }];
                                            DBResultSet *r = [[[DBQBChatMessage query]
                                                               whereWithFormat:@" DialogID = %@", chatDialog.ID] fetch];
                                            
                                            for (DBQBChatMessage *dbQBChatMessage in r) {
                                                dbQBChatMessage.IsDeleteMessage = YES;
                                                [dbQBChatMessage commit];
                                            }
                                            [[ChatDialogModel shared].dialogs removeObjectAtIndex:indexPath.row];
                                            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil]
                                                             withRowAnimation:UITableViewRowAnimationFade];
                                        
                                        [tableView endUpdates];
                                    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    return @[button];
}
#pragma mark house swipe animation start---------------------------------------------------------------------------------------------------------------
- (void)openSideDidChange:(MMDrawerSide)drawerSide{
    [self changePage:drawerSide];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    if (self.Threepageviewcontroller.didChangeToImageTableView ||self.presentedViewController||self.delegate.disableViewGesture||![[LoginBySocialNetworkModel shared] qbAccountIsReady]) {
        return NO;
    }
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    UITableView *tableview = self.dialogsTableView;
    if (self.searchDisplayController.active) {
        tableview = self.searchDisplayController.searchResultsTableView;
    }
    [self.searchBar resignFirstResponder];
    CGPoint panLocation = [gestureRecognizer locationInView:tableview];
    //// if twopage then
    if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
        NSIndexPath *indexpath = [tableview indexPathForRowAtPoint:panLocation];
        
        if (indexpath) {
            //// get current cell QBID
            
            if (self.searchDisplayController.active) {
                
                if (indexpath.section == 0) {
                    if (  [self.searchDialogResultArray count] > indexpath.row) {
                        
                    }else{
                        return NO;
                    }
                   if ([self isEmpty:self.searchDialogResultArray[indexpath.row]]) {
                        return NO;
                    }
                    QBChatDialog *chatDialog =
                    self.searchDialogResultArray[indexpath.row];
                    
                    //// check current cell QBID   have agentprofile or not   to enable
                    /// animation
                    for (AgentProfile *agentprofile in
                         [DialogPageAgentProfilesModel shared]
                         .dialogPageSearchAgentProfiles) {
                        NSString *tableViewCellQBID = [chatDialog getRecipientIDFromQBChatDialog];
                        
                        if ([agentprofile.QBID
                             isEqualToString:
                             [NSString stringWithFormat:@"%@", tableViewCellQBID]]) {
                            
                            if ([agentprofile haveAgentListing]) {
                                
                                
                                self.delegate.currentAgentProfile = agentprofile;
                                self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                                self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                                return YES;
                            } else {
                                
                                
                                return NO;
                            }
                        }
                    }
                }
                
                if (indexpath.section == 1) {
                    self.delegate.currentAgentProfile =
                    [DialogPageSearchAgentProfilesModel shared]
                    .dialogPageSearchAgentProfiles[indexpath.row];
                    self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                    self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                    return YES;
                }
            }
            
            if (!self.searchDisplayController.active) {
                
                
                QBChatDialog *chatDialog =
                [ChatDialogModel shared].dialogs[indexpath.row];
                
                //// check current cell QBID   have agentprofile or not   to enable
                /// animation
                for (AgentProfile *agentprofile in [DialogPageAgentProfilesModel shared]
                     .dialogPageSearchAgentProfiles) {
                    NSString *tableViewCellQBID = [chatDialog getRecipientIDFromQBChatDialog];
                    if ([agentprofile.QBID
                         isEqualToString:
                         [NSString stringWithFormat:@"%@", tableViewCellQBID]]) {
                        self.delegate.currentAgentProfile = agentprofile;
                        self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                        self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                        
                        
                        if ([agentprofile haveAgentListing]) {
                            return YES;
                        }else{
                            return NO;
                        }
                    }
                }
            }
            
            return NO;
        }
        return NO;
    } else {
        ////one or three page animation.
        return YES;
    }
}

-(void) changePage:(MMDrawerSide)drawerSide{
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}



- (void)panGestureCallback:(UIPanGestureRecognizer *)panGesture {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
            DDLogInfo(@"UIGestureRecognizerStateBegan");
#pragma mark refresh after follow  or unfollow only one tableviewcell  threepage start pangesture
            if (screenRect.size.width > panLocation.x) {
                if (![self isEmpty:self.indexPathForCell]) {
                    /*  self.tableviewcellfade = NO;
                     [self.dialogsTableView beginUpdates];
                     [self.dialogsTableView reloadRowsAtIndexPaths:@[ self.indexPathForCell ]
                     withRowAnimation:UITableViewRowAnimationNone];
                     [self.dialogsTableView endUpdates];
                     self.tableviewcellfade = YES;
                     */
                    [self tableViewReloadAnimationTableView:self.dialogsTableView];
                }
            }
#pragma mark get self.view pangesture x   twopage start pangesture
            
            if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
                DDLogInfo(@"-panLocationstart%f", panLocation.x);
                
                
#pragma mark create imageivew
                UITableView *gestureTableView;
                if (self.searchDisplayController.isActive) {
                    gestureTableView = self.searchDisplayController.searchResultsTableView;
                }else{
                    gestureTableView = self.dialogsTableView;
                }
                CGPoint panLocation = [panGesture locationInView:gestureTableView];
                NSIndexPath *indexPath = [gestureTableView indexPathForRowAtPoint:panLocation];
                self.indexPathForCell = indexPath;
                
                CGRect rectInTableView = [gestureTableView rectForRowAtIndexPath:indexPath];
                CGRect rectInSuperview = [gestureTableView convertRect:rectInTableView toView:[gestureTableView superview]];
                CGRect searchBar = [gestureTableView convertRect:self.searchBar.frame toView:[gestureTableView superview]];
                
                if(searchBar.origin.y <0&&indexPath==0){
                    rectInSuperview.origin.y += searchBar.size.height;
                }
                [self showSpotlightInRect:rectInSuperview];
                
            }
            break;
            
        case UIGestureRecognizerStateChanged:
            if (self.MMDrawerController.openSide == MMDrawerSideNone) {
                
            }
            break;
            
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            break;
            
        default:
            break;
    }
}

- (void)finishPanGestureCallBack:(UIPanGestureRecognizer *)panGesture {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
        [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
        [self hideSpotlight];
    }
    MMDrawerSide drawerSide= self.MMDrawerController.openSide;
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
        [self tableViewReloadAnimationTableView:self.dialogsTableView];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}
//// imageFromView
- (UIImage *)imageFromView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO,
                                           [[UIScreen mainScreen] scale]);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
//
//- (void)removeallImageFromView {
//    NSArray *subViewArray = [self.delegate.window subviews];
//    for (id obj in subViewArray) {
//        if ([obj isKindOfClass:[UIImageView class]]) {
//            [obj removeFromSuperview];
//        }
//    }
//}
- (void)moveToCenterPage{
    
    [[self.delegate getTabBarController]setTabBarHidden:NO animated:YES];
    [self.MMDrawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        [self changePage:MMDrawerSideNone];
    }];
}

#pragma mark - <DialogsViewCellDelegate>
- (void)cell:(DialogsViewCell *)cell followButtonDidPress:(UIButton *)button
{
    if (self.dialogsTableView.editing == YES) {
        return;
    }
    
    [self followOrUnfollow:cell.agentProfile];
}

- (void)cell:(DialogsViewCell *)cell profileImageButtonDidPress:(UIButton *)button{
    if (self.dialogsTableView.editing == YES) {
        return;
    }
    [self pressPersonalImageBtn:button];
}

#pragma mark - <DialogsViewCellDelegate>
- (void)searchCell:(DialogsSearchAgentCell *)cell followButtonDidPress:(UIButton *)button
{
    if (self.dialogsTableView.editing == YES) {
        return;
    }
    
    [self followOrUnfollow:cell.agentProfile];
}

#pragma mark Follow ------------------------------------------------------------------------------------------------------------------
- (void)followOrUnfollow:(AgentProfile *)agentProfile {
    
    self.delegate.currentAgentProfile = agentProfile;
    if ([agentProfile getIsFollowing]) {
        [self.view endEditing:YES];
        [self showUnfollowActionSheet];
        
    } else {
        
        [self callServerFollowApi];
    }
}
- (void)pressPersonalImageBtn:(UIButton *)personalImageBtn {
    self.delegate.currentAgentProfile =
    [self getAgentProfileFromTableViewCellIndexPath:personalImageBtn.tag];
    if (![self.delegate.currentAgentProfile haveAgentListing] || [self.delegate.currentAgentProfile isCSTeam]) {
        return;
    }
    self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
    self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
    CGRect rectInTableView = [self.dialogsTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:personalImageBtn.tag]];
    CGRect rectInSuperview = [self.dialogsTableView convertRect:rectInTableView toView:[self.dialogsTableView superview]];
    rectInSuperview.size.height += 53;
    rectInSuperview.origin.y -=53;
    [self showSpotlightInRect:rectInSuperview];
    [[self.delegate getTabBarController]adjustNavBarHeight:RealNavBarHeightWithPageControl animated:NO];
    
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        if (finished) {
            [self changePage:MMDrawerSideRight];
        }
    }];
}

- (void)pressAgentImageBtn:(UIButton *)AgentImageBtn {
    self.delegate.currentAgentProfile=[DialogPageSearchAgentProfilesModel shared]
    .dialogPageSearchAgentProfiles[AgentImageBtn.tag];
    if (![self.delegate.currentAgentProfile haveAgentListing] || [self.delegate.currentAgentProfile isCSTeam]) {
        return;
    }
    self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
    self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
    CGRect rectInTableView = [self.dialogsTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:AgentImageBtn.tag]];
    CGRect rectInSuperview = [self.dialogsTableView convertRect:rectInTableView toView:[self.dialogsTableView superview]];
    rectInSuperview.size.height += 53;
    rectInSuperview.origin.y -=53;
    [self showSpotlightInRect:rectInSuperview];
    [[self.delegate getTabBarController]adjustNavBarHeight:RealNavBarHeightWithPageControl animated:NO];
    
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        if (finished) {
            [self changePage:MMDrawerSideRight];
        }
    }];
    
    
}
- (void)spotLightMoveToRightPageAgentProfile:(AgentProfile *)agentProfile{
    
    [self.dialogsTableView setContentOffset:CGPointMake(0,0)];
    
    
    [self.searchBar becomeFirstResponder];
    
    self.searchDisplayController.active = YES;
    self.searchDisplayController.searchBar.text = agentProfile.MemberName;
    //  DDLogDebug(@"DBUnreadMessageLabel %@",r);
    [self.searchBar resignFirstResponder];
    self.delegate.currentAgentProfile=agentProfile;
    self.Onepageviewcontroller.pendingAgentProfile = agentProfile;
    self.Threepageviewcontroller.pendingAgentProfile = agentProfile;
    if (![self.delegate.currentAgentProfile haveAgentListing]) {
        return;
    }
    [[self.delegate getTabBarController]adjustNavBarHeight:RealNavBarHeightWithPageControl animated:NO];
    
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        if (finished) {
            [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
            [self changePage:MMDrawerSideRight];
        }
    }];
    [DialogPageSearchAgentProfilesModel shared].dialogPageSearchAgentProfiles=nil;
    
}
- (void)showUnfollowActionSheet {
    UIActionSheet *followActionSheet =
    [[UIActionSheet alloc] initWithTitle:nil
                                delegate:self
                       cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                  destructiveButtonTitle:JMOLocalizedString(@"agent_profile__unfollow_button", nil)
                       otherButtonTitles:nil, nil];
    
    [followActionSheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        if ([self.delegate networkConnection]) {
            //            [self loadingAndStopLoadingAfterSecond:10];
            [self callServerUnFollowApi];
        }
    }
}

- (void)callServerFollowApi {
    //    if (![self.delegate networkConnection]) {
    //        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
    //    }
    [[FollowAgentModel shared]
     callFollowAgentApiByAgentProfileMemberID:
     [NSString stringWithFormat:@"%d",
      self.delegate.currentAgentProfile.MemberID]
     AgentProfileAgentListingAgentListingID:
     [NSString stringWithFormat:@"%d", self.delegate.currentAgentProfile
      .AgentListing.AgentListingID]
     success:^(id responseObject) {
         if (self.searchDisplayController.isActive) {
             [self tableViewReloadAnimationTableView:self.dialogsTableView];
             [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
         }else{
             [self tableViewReloadAnimationTableView:self.dialogsTableView];
         }
         //            [self hideLoadingHUD];
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,
               NSString *alert, NSString *ok) {
         
         //         [self hideLoading];
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                       errorMessage:errorMessage
                                                        errorStatus:errorStatus
                                                              alert:alert
                                                                 ok:ok];
         
     }];
}
- (void)callServerUnFollowApi {
    //    if (![self.delegate networkConnection]) {
    //        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
    //    }
    [[UnFollowAgentModel shared]
     callUnFollowAgentApiByAgentProfileMemberID:
     [NSString stringWithFormat:@"%d",
      self.delegate.currentAgentProfile.MemberID]
     success:^(id responseObject) {
         int const MYFollowerCount =
         self.delegate.currentAgentProfile.FollowerCount;
         self.delegate.currentAgentProfile.FollowerCount = MYFollowerCount + 1;
         if (self.searchDisplayController.isActive) {
             [self tableViewReloadAnimationTableView:self.dialogsTableView];
             [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
         }else{
             [self tableViewReloadAnimationTableView:self.dialogsTableView];
         }
         //            [self hideLoadingHUD];
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,
               NSString *alert, NSString *ok) {
         //         [self hideLoading];
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                       errorMessage:errorMessage
                                                        errorStatus:errorStatus
                                                              alert:alert
                                                                 ok:ok];
         
     }];
}
-(void)knotificationChatRelationListUpdated{
    
    
    
    [[ChatRelationModel shared] chatRoomGetRelationFromChatDialogIdArraysuccess:^(NSArray< ChatRoomRelationObject> *chatRoomRelationArray) {
        [self hideLoadingHUD];
        
        [self tableViewReloadAnimationTableView:self.dialogsTableView];
        [[ChatDialogModel shared] updateChatBadgeNumber];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatRoomRelationPageButtonSetup object:nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kNotificationDialogsUpdated
         object:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus,
                NSString *alert, NSString *ok){
        [self hideLoadingHUD];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                      errorMessage:nil
                                                       errorStatus:errorStatus
                                                             alert:alert
                                                                ok:ok];
        [self tableViewReloadAnimationTableView:self.dialogsTableView];
        [[ChatDialogModel shared] updateChatBadgeNumber];
        
    }];
    [[ChatDialogModel shared] updateChatBadgeNumber];
    
}
- (AgentProfile *)getAgentProfileFromTableViewCellIndexPath:
(NSInteger )IndexPathrow {
    
    //    if (![self isEmpty:[DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles]) {
    //        return [[DialogPageAgentProfilesModel shared].dialogPageSpotLightSearchAgentProfiles firstObject];
    //    }
    //// get current cell QBID
    QBChatDialog *chatDialog;
    if (self.searchDisplayController.active&&self.searchBar.text.length>0) {
        
        if (IndexPathrow < self.searchDialogResultArray.count) {
            
            chatDialog = self.searchDialogResultArray[IndexPathrow];
        }
        
    } else {
        
        if (IndexPathrow < [ChatDialogModel shared].dialogs.count) {
            
            chatDialog = [[ChatDialogModel shared].dialogs objectAtIndex:IndexPathrow];
            
        }
    }
    
    if(chatDialog) {
        
        for (AgentProfile *agentprofile in [DialogPageAgentProfilesModel shared].dialogPageSearchAgentProfiles) {
            
            NSString *tableViewCellQBID = [chatDialog getRecipientIDFromQBChatDialog];
            
            if ([agentprofile.QBID isEqualToString:[NSString stringWithFormat:@"%@", tableViewCellQBID]]) {
                
                return agentprofile;
            }
        }
    }
    return nil;
}
#pragma mark SpotLightSearch
-(void)spotLightSearchOpenRightProfilePage{
    
    NSString * QBID=[AppDelegate getAppDelegate].spotLightSearchQBID;
    [[DialogPageAgentProfilesModel shared]
     spotLightSearchFromQBID:QBID success:^(NSMutableArray<AgentProfile>
                                            *AgentProfiles) {
         [self.searchBar resignFirstResponder];
         [self.MMDrawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
             [self tableViewReloadAnimationTableView:self.dialogsTableView];
             [self spotLightMoveToRightPageAgentProfile:[AgentProfiles firstObject]];
         }];
         
         
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,
               NSString *alert, NSString *ok) {
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                       errorMessage:nil
                                                        errorStatus:errorStatus
                                                              alert:alert
                                                                 ok:ok];
         
     }];
}
-(void)createAllDialogSpotLightSearchChatItem{
    
    for (QBChatDialog * chatDialog in  [ChatDialogModel shared].dialogs) {
        for (AgentProfile *agentprofile in [DialogPageAgentProfilesModel shared]
             .dialogPageSearchAgentProfiles) {
            NSString *tableViewCellQBID = [chatDialog getRecipientIDFromQBChatDialog];
            if ([agentprofile.QBID
                 isEqualToString:[NSString
                                  stringWithFormat:@"%@", tableViewCellQBID]]) {
                     if (![self isEmpty:agentprofile.AgentListing]) {
                         [agentprofile deleteSpotLightSearchChatItem];
                         [agentprofile createSpotLightSearchChatItem];
                         
                     }
                     
                     
                 }
        }
    }
    
}

-(void)followOrUnfollowReloadTableView{
    [self tableViewReloadAnimationTableView:self.dialogsTableView];
}
#pragma mark SVPullToRefresh method
- (void)insertRowAtBottom {
    DDLogInfo(@"insertRowAtBottom");
    
    if ([[DialogPageSearchAgentProfilesModel shared]
         .dialogPageSearchAgentProfiles count] > 0) {
        __weak DialogsViewController *weakSelf = self;
        [[DialogPageSearchAgentProfilesModel shared] AgentSearchWithPage:self.searchDisplayController.searchBar.text success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
            
            
            
            
            [self showInfiniteRefreshIfNeed];
            [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
            [self.searchDisplayController.searchResultsTableView resetState];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:nil errorStatus:errorStatus alert:alert ok:ok];
            [self showInfiniteRefreshIfNeed];
            [self tableViewReloadAnimationTableView:self.searchDisplayController.searchResultsTableView];
            [self.searchDisplayController.searchResultsTableView resetState];
            
        }];
    }else{
        [DialogPageSearchAgentProfilesModel shared].tableViewNotLoading=YES;
        [self showInfiniteRefreshIfNeed];
        
        
    }
}

-(void)showInfiniteRefreshIfNeed{
    
    if ([DialogPageSearchAgentProfilesModel shared].tableViewNotLoading) {
        
        self.searchDisplayController.searchResultsTableView.showsInfiniteScrolling = NO;
        
    }else{
        self.searchDisplayController.searchResultsTableView.showsInfiniteScrolling = YES;
        
    }
}
-(void)tableViewReloadAnimationTableView:(UITableView *)tableivew{
    
    UIEdgeInsets tableInset = tableivew.contentInset;
    if (tableivew==self.searchDisplayController.searchResultsTableView) {
        tableInset.top = 64-20;
        tableInset.bottom = 49;
        [tableivew setContentInset:tableInset];
    }
    [tableivew reloadData];
    //    [UIView transitionWithView:tableivew
    //                      duration:0.3f
    //                       options:UIViewAnimationOptionTransitionCrossDissolve
    //                    animations:^(void)
    //     {
    //        [tableivew reloadData];
    //     }
    //                    completion:nil];
    
    
}
@end