//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TDDatePickerController.h"
#import "TDPicker.h"
@interface Filterpage : BaseViewController<TDPickerDelegate>
@property(strong, nonatomic) IBOutlet TDPicker *picker;
@property(strong, nonatomic) NSDate *selectedDate;
//// house information part2
@property(strong, nonatomic) IBOutlet UILabel *numberofbedroomlabel;
@property(strong, nonatomic) IBOutlet UILabel *numberofbathroomlabel;
@property(nonatomic) int numberofbedroom;
@property(nonatomic) int numberofbathroom;
@property(strong, nonatomic) IBOutlet UITextField *price;
@property(strong, nonatomic) IBOutlet UITextField *size;

@property(strong, nonatomic) IBOutlet UIButton *typeofspace;
//// house information part1 button press
- (IBAction)SpaceofTypePickerButtonClicked:(id)sender;

@end
