//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Filterpage.h"
#import "TDPicker.h"

@interface Filterpage ()

@end
@implementation Filterpage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}
- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
  [super viewDidLoad];
  self.picker.delegate = self;
}
//// -     - or + button
- (IBAction)increasebedroom:(id)sender {
  self.numberofbedroom++;
  [self.numberofbedroomlabel
      setText:[NSString stringWithFormat:@"%d", self.numberofbedroom]];
}

- (IBAction)decreasebedroom:(id)sender {
  if (self.numberofbedroom > 0) {
    self.numberofbedroom--;
    [self.numberofbedroomlabel
        setText:[NSString stringWithFormat:@"%d", self.numberofbedroom]];
  }
}
- (IBAction)increasebathroom:(id)sende {
  self.numberofbathroom++;
  [self.numberofbathroomlabel
      setText:[NSString stringWithFormat:@"%d", self.numberofbathroom]];
}

- (IBAction)decreasebathroom:(id)sende {
  if (self.numberofbathroom > 0) {
    self.numberofbathroom--;
    [self.numberofbathroomlabel
        setText:[NSString stringWithFormat:@"%d", self.numberofbathroom]];
  }
}

//// -TDPicker Delegate

- (IBAction)SpaceofTypePickerButtonClicked:(id)sender {
  _picker.propertyorspacepicker = @"space";

  if ([_picker.residentialorcommercial isEqualToString:@"Residential"])
    _picker.pickerData = @[
      @"Apartment",
      @"Unit",
      @"Townhouse",
      @"Loft",
      @"Houseboat",
      @"Villa",
      @"Land",
      @"Condos",
      @"Suburban Home",
      @"Others "
    ];
  else {
    _picker.pickerData = @[
      @"Office",
      @"Industrial",
      @"Warehouse",
      @"Flex Space",
      @"Retail",
      @"Land",
      @"Agricultural",
      @"Hotel & Motel",
      @"Health Care",
      @"Special Purpose",
      @"Others "
    ];
  }
  _picker.selectpickerdata = _picker.pickerData[0];
  [_picker.picker reloadAllComponents];
  [self presentSemiModalViewController:_picker];
}

- (void)picker:(UIPickerView *)picker
  didSelectRow:(NSInteger)row
   inComponent:(NSInteger)component {
  if ([_picker.propertyorspacepicker isEqualToString:@"property"]) {
    if (row == 0) {
      _picker.residentialorcommercial = @"Residential";
    } else {
      _picker.residentialorcommercial = @"Commercial";
    }
  }
  _picker.selectpickerdata = _picker.pickerData[row];
  DDLogDebug(@"_picker.selectpickerdata-->%@", _picker.pickerData[row]);
}

- (void)pickerSetDate:(TDPicker *)viewController {
  [self dismissSemiModalViewController:_picker];

  if ([_picker.propertyorspacepicker isEqualToString:@"space"]) {
    [self.typeofspace setTitle:_picker.selectpickerdata
                      forState:UIControlStateNormal];
  }
}

- (void)pickerClearDate:(TDPicker *)viewController {
  [self dismissSemiModalViewController:_picker];

  _selectedDate = nil;
}

- (void)pickerCancel:(TDPicker *)viewController {
  [self dismissSemiModalViewController:_picker];
}

@end