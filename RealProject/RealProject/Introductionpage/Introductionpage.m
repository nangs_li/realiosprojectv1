//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//


#import "Introductionpage.h"
#import "MYBlurIntroductionView.h"
#import "LoginUIViewController.h"
@interface Introductionpage ()
@property (nonatomic) CGPoint viewInputCenter;
@end
@implementation Introductionpage


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    //Calling this methods builds the intro and adds it to the screen. See below.
    [self buildIntro];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)buildIntro{
    //Create Stock Panel with header
    UIView *headerView = [[NSBundle mainBundle] loadNibNamed:@"TestHeader" owner:nil options:nil][0];
    MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50) title:nil description:nil image:[UIImage imageNamed:@"introductionphoto1.png"] header:headerView];
    UIView *headerView2 = [[NSBundle mainBundle] loadNibNamed:@"TestHeader" owner:nil options:nil][0];
    
    MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50) title:nil description:nil image:[UIImage imageNamed:@"introductionphoto2.png"] header:headerView2];
    UIView *headerView3 = [[NSBundle mainBundle] loadNibNamed:@"TestHeader" owner:nil options:nil][0];
    
    MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50) title:nil description:nil image:[UIImage imageNamed:@"introductionphoto3.png"] header:headerView3];
    UIView *headerView4 = [[NSBundle mainBundle] loadNibNamed:@"TestHeader" owner:nil options:nil][0];
    //Create custom panel with events
    MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50) title:nil description:nil image:[UIImage imageNamed:@"introductionphoto4.png"] header:headerView4];
    
    //Add panels to an array
    NSArray *panels = @[panel1, panel2, panel3, panel4];
    
    //Create the introduction view and set its delegate
    MYBlurIntroductionView *introductionView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50)];
    introductionView.delegate = self;
    //  introductionView.BackgroundImageView.image = [UIImage imageNamed:@"Toronto, ON.jpg"];
    
    //introductionView.LanguageDirection = MYLanguageDirectionRightToLeft;
    
    //Build the introduction with desired panels
    [introductionView buildIntroductionWithPanels:panels];
    
    //Add the introduction to your view
    [self.view addSubview:introductionView];
    
    // self.loginbutton.layer.zPosition=1;
}







//// - MYIntroduction Delegate

-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex{
    DDLogInfo(@"Introduction did change to panel %ld", (long)panelIndex);
    
    //You can edit introduction view properties right from the delegate method!
    //If it is the first panel, change the color to green!
    if (panelIndex == 0) {
        //  [introductionView setBackgroundColor:[UIColor colorWithRed:90.0f/255.0f green:175.0f/255.0f blue:113.0f/255.0f alpha:0.65]];
    }
    //If it is the second panel, change the color to blue!
    else if (panelIndex == 1){
        //   [introductionView setBackgroundColor:[UIColor colorWithRed:50.0f/255.0f green:79.0f/255.0f blue:133.0f/255.0f alpha:0.65]];
    }
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType {
    DDLogInfo(@"Introduction did finish");
}


- (IBAction)Loginpress:(id)sender {
    [self presentViewController: [[LoginUIViewController alloc]init] animated:YES completion:nil];
}




@end