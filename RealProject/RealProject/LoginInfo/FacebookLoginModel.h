//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
#import "BaseModel.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@interface FacebookLoginModel : BaseModel
#pragma mark Social Login User InFormation
@property(strong, nonatomic) NSString *userId;
@property(strong, nonatomic) NSString *username;
@property(strong, nonatomic) NSString *Email;
@property(strong, nonatomic) NSString *userphotolink;
#pragma mark FacebookToken
@property(strong, nonatomic) NSString *fbaccessToken;
+ (FacebookLoginModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 
 */

- (void)facebookLoginPressSuccess:
(void (^)(LoginInfo *myLoginInfo,
          SystemSettings *SystemSettings))success
                          failure:(failureBlock)failure fromViewController:(UIViewController*)viewController;

- (void)facebookLoginPressSuccessBlock:
(void (^)(id result,NSError *error))success
                          failureBlock:(failureBlock)failure fromViewController:(UIViewController*)viewController;
@end
