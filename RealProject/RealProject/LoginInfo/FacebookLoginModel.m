//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "FacebookLoginModel.h"

#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "DBLogInfo.h"
#import "DBSystemSettings.h"
#import "SystemSettingModel.h"
#import "StringEncryption.h"
#import "NSData+Base64.h"
#import "AppDelegate.h"

// Mixpanel
#import "Mixpanel.h"

@implementation FacebookLoginModel

static FacebookLoginModel *sharedFacebookLoginModel;

+ (FacebookLoginModel *)shared {
  @synchronized(self) {
    if (!sharedFacebookLoginModel) {
      sharedFacebookLoginModel = [[FacebookLoginModel alloc] init];

      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedFacebookLoginModel;
  }
}

- (void)facebookLoginPressSuccess:
(void (^)(LoginInfo *myLoginInfo,
          SystemSettings *SystemSettings))success
                          failure:(failureBlock)failure fromViewController:(UIViewController*)viewController{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorNative;
    [login logOut];
    [login logInWithReadPermissions:@[@"email"] fromViewController:viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            // Process error
#warning DC - KL, please help to replace a proper error message
            failure(nil, error, @"Error", NotUseStatus, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        } else if (result.isCancelled) {
            // Handle cancellations
#warning DC - KL, please help to replace a proper error message
            failure(nil, nil, @"Error", NotUseStatus, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        } else {
            // If you ask for multiple permissions at once,
            // you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {
                    DDLogInfo(@"Token is available : %@",
                              [[FBSDKAccessToken currentAccessToken] tokenString]);
                    
                    [[[FBSDKGraphRequest alloc]
                      initWithGraphPath:@"me"
                      parameters:@{
                                   @"fields" : @"id, name, link, first_name, last_name, "
                                   @"picture.type(large), email"
                                   }] startWithCompletionHandler:^(FBSDKGraphRequestConnection
                                                                   *connection,
                                                                   id result,
                                                                   NSError *error) {
                        if (!error) {
                            self.fbaccessToken =
                            [FBSDKAccessToken currentAccessToken].tokenString;
                            
                            DDLogInfo(@"resultis:%@", result);
                            
                            self.username = [result objectForKey:@"name"];
                            self.userId = [result objectForKey:@"id"];
                            self.Email = [result objectForKey:@"email"];
                            self.userphotolink =
                            [[[result objectForKey:@"picture"] objectForKey:@"data"]
                             objectForKey:@"url"];
                            
                            DDLogInfo(@"facebook-->email : %@", self.Email);
                            DDLogInfo(@"facebook-->username : %@", self.username);
                            DDLogInfo(@"facebook-->profilelink : %@", self.userphotolink);
                            DDLogInfo(@"facebook-->userId : %@", self.userId);
                            DDLogInfo(@"facebook-->fbaccessToken : %@", self.fbaccessToken);
                            
                            // Mixpanel
                            Mixpanel *mixpanel = [Mixpanel sharedInstance];
                            [mixpanel.people set:@"$email" to:self.Email];
                            
                            [[LoginBySocialNetworkModel shared]
                             callLoginBySocialNetworkModelApiDeviceType:@"1"
                             deviceToken:[AppDelegate getAppDelegate].devicetoken
                             socialNetworkType:@"1"
                             userName:self.username
                             userID:self.userId
                             email:self.Email
                             photoURL:self.userphotolink
                             accessToken:self.fbaccessToken
                             Success:^(LoginInfo *myLoginInfo,
                                       SystemSettings *SystemSettings) {
                                 if ([[AppDelegate getAppDelegate] isEmpty:myLoginInfo.MemberID.stringValue]) {
                                     return ;
                                 }
                                 if ([[AppDelegate getAppDelegate] isEmpty:myLoginInfo.MemberName]) {
                                     return ;
                                 }
                                 // Mixpanel
                                 Mixpanel *mixpanel = [Mixpanel sharedInstance];
                                 [mixpanel.people set:@"$name" to:myLoginInfo.MemberName];
                                 
                                 if (myLoginInfo.isNewUser) {
                                    
                                     [mixpanel createAlias:myLoginInfo.MemberID.stringValue forDistinctID:mixpanel.distinctId];
                                     [mixpanel identify:mixpanel.distinctId];
                                     [mixpanel track:@"Became User"];
                                     [mixpanel.people set:@"Member ID" to:myLoginInfo.MemberID.stringValue];
                                     [mixpanel.people set:@"Agent" to:@"NO"];
                                     [mixpanel.people set:@"# of listings" to:[NSNumber numberWithInt:0]];
                                     [mixpanel.people set:@"User since" to:[NSDate date]];
                                     
                                 } else {
                                     
                                     [mixpanel identify:myLoginInfo.MemberID.stringValue];
                                     [mixpanel track:@"Verified Facebook Login"];
                                 }
                                 
                                 success(myLoginInfo, SystemSettings);
                                 
                             }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                       NSString *errorMessage,
                                       RequestErrorStatus errorstatus, NSString *alert,
                                       NSString *ok) {
                                 failure(operation, error, errorMessage, errorstatus,
                                         alert, ok);
                             }];
                            
                        } else {
                            DDLogInfo(@"Error %@", error);
                        }
                    }];
                }
            }
        }
    }];
}


- (void)facebookLoginPressSuccessBlock:
(void (^)(id result,
          NSError *error))success
                          failureBlock:(failureBlock)failure fromViewController:(UIViewController*)viewController {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorNative;
    [login logOut];
    [login logInWithReadPermissions:@[@"email"] fromViewController:viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            // Process error
#warning DC - KL, please help to replace a proper error message
            failure(nil, error, @"Error", NotUseStatus, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        } else if (result.isCancelled) {
            // Handle cancellations
#warning DC - KL, please help to replace a proper error message
            failure(nil, nil, @"Error", NotUseStatus, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        } else {
            // If you ask for multiple permissions at once,
            // you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {
                    DDLogInfo(@"Token is available : %@",
                              [[FBSDKAccessToken currentAccessToken] tokenString]);
                    
                    [[[FBSDKGraphRequest alloc]
                      initWithGraphPath:@"me"
                      parameters:@{
                                   @"fields" : @"id, name, link, first_name, last_name, "
                                   @"picture.type(large), email"
                                   }] startWithCompletionHandler:^(FBSDKGraphRequestConnection
                                                                   *connection,
                                                                   id result,
                                                                   NSError *error) {
                        if (!error) {
                            self.fbaccessToken =
                            [FBSDKAccessToken currentAccessToken].tokenString;
                            
                            DDLogInfo(@"resultis:%@", result);
                            
                            self.username = [result objectForKey:@"name"];
                            self.userId = [result objectForKey:@"id"];
                            self.Email = [result objectForKey:@"email"];
                            self.userphotolink =
                            [[[result objectForKey:@"picture"] objectForKey:@"data"]
                             objectForKey:@"url"];
                            
                            DDLogInfo(@"facebook-->email : %@", self.Email);
                            DDLogInfo(@"facebook-->username : %@", self.username);
                            DDLogInfo(@"facebook-->profilelink : %@", self.userphotolink);
                            DDLogInfo(@"facebook-->userId : %@", self.userId);
                            DDLogInfo(@"facebook-->fbaccessToken : %@", self.fbaccessToken);
                            
                            success(result,error);
                            
                            
                        } else {
                            DDLogInfo(@"Error %@", error);
                        }
                    }];
                }
            }
        }
    }];
}
@end