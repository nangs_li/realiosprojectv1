//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
#import "BaseModel.h"
#import "RealNetworkMemberInfo.h"
@interface LoginByEmailModel : BaseModel
@property(strong, nonatomic) RealNetworkMemberInfo *realNetworkMemberInfo;
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
+ (LoginByEmailModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 errorStatus=100 Email already exist
 errorStatus=101 Real network login error
 errorStatus=102 Send email verification error - email not found
 */
- (void)callRealNetworkMemberPhoneRegistrationPhoneRegID:(NSString *)phoneRegID
                                            pinInput:(NSString *)pinInput
                                           firstName:(NSString *)firstName
                                            lastName:(NSString *)lastName
                                        profileImage:(UIImage*)profileImage
                                             Success:(void (^)(RealNetworkMemberInfo * realNetworkMemberInfo))success
                                                 failure:(failureBlock)failure;

- (void)callRealNetworkMemberLoginEmail:(NSString *)email
                            passwordMD5:(NSString *)passwordMD5
                                Success:(void (^)(RealNetworkMemberInfo * realNetworkMemberInfo))success
                                failure:(failureBlock)failure;

- (void)callRealNetworkMemberLoginPhoneRegID:(NSString *)phoneRegID
                                    pinInput:(NSString *)pinInput
                                     Success:(void (^)(RealNetworkMemberInfo * realNetworkMemberInfo))success
                                     failure:(failureBlock)failure;

- (void)callRealNetworkChangeEmailApiEmail:(NSString *)email
                               passwordMD5:(NSString *)passwordMD5
                                    userID:(NSString *)userID
                               accessToken:(NSString *)accessToken
                                   Success:(successBlock)success
                                   failure:(failureBlock)failure;

- (void)callRealNetworkChangePasswordApiNewPasswordMD5:(NSString *)newPasswordMD5 OldPasswordMD5:(NSString *)oldPasswordMD5
                                                userID:(NSString *)userID
                                           accessToken:(NSString *)accessToken
                                               Success:(successBlock)success
                                               failure:(failureBlock)failure;

- (void)callRealNetworkCheckEmailAvailability:(NSString *)email
                                      Success:(successBlock)success
                                      failure:(failureBlock)failure;

- (void)callRealNetworkSendResetPasswordEmailApiEmail:(NSString *)email
                                              Success:(successBlock)success
                                              failure:(failureBlock)failure;

@end
