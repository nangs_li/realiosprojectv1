//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LoginByEmailModel.h"
#import "XMLReader.h"
#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "DBLogInfo.h"
#import "DBSystemSettings.h"
#import "SystemSettingModel.h"
#import "StringEncryption.h"
#import "NSData+Base64.h"
#import "CocoaSecurity.h"
#import "RealUtility.h"
#import "DBRealNetworkMemberInfo.h"
@implementation LoginByEmailModel

static LoginByEmailModel *sharedLoginByEmailModel;

+ (LoginByEmailModel *)shared {
    @synchronized(self) {
        if (!sharedLoginByEmailModel) {
            sharedLoginByEmailModel = [[LoginByEmailModel alloc] init];
            
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedLoginByEmailModel;
    }
}

- (void)callRealNetworkMemberPhoneRegistrationPhoneRegID:(NSString *)phoneRegID
                                   pinInput:(NSString *)pinInput
                                     firstName:(NSString *)firstName
                                      lastName:(NSString *)lastName
                                  profileImage:(UIImage*)profileImage
                                       Success:(void (^)(RealNetworkMemberInfo * realNetworkMemberInfo))success
                                       failure:(failureBlock)failure {
    
    NSString *languageCode= [AppDelegate getAppDelegate].languageCode;
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"UniqueKey\":\"%f\",\"PhoneRegID\":\"%@\",\"PINInput\":\"%@\","
                            @"\"FirstName\":\"%@\",\"LastName\":\"%@\",\"Lang\":\"%@\"}",
                            [[NSDate date] timeIntervalSince1970] ,phoneRegID, pinInput, firstName, lastName,languageCode];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/RealNetwork.asmx/RealNetworkMemberPhoneRegistration"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogDebug(@"InputJson-->:%@", parameters);
    parameters =[self stringToAESEncrypt: parameters];
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *responseObjectString = [responseObject objectForKey:@"d"];
              NSString *dstring = responseObjectString;
              dstring = [self stringToAESDecrypt: responseObjectString];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              DDLogVerbose(@"Server Return String-->%@", dstring);
              NSError *err = nil;
              HandleServerReturnString *serverReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              int error = serverReturnString.ErrorCode;
              NSString *errorMsg = serverReturnString.ErrorMsg;
              
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  self.realNetworkMemberInfo = [[RealNetworkMemberInfo alloc]initwithNSDictionary:serverReturnString.Content error:error];
                  [self uploadProfileImage:profileImage memberId:self.realNetworkMemberInfo.UserID accessToken:self.realNetworkMemberInfo.AccessToken block:^(id response, NSError *error) {
                      if (!error && response) {
                          NSString *photoURL = response[@"PhotoURL"];
                          self.realNetworkMemberInfo.PhotoURL = photoURL;
                          
                          //// save DBLogInfo into database
                          [[[DBRealNetworkMemberInfo query] fetch] removeAll];
                          DBRealNetworkMemberInfo *dbRealNetworkMemberInfo = [DBRealNetworkMemberInfo new];
                          dbRealNetworkMemberInfo.Date = [NSDate date];
                          
                          dbRealNetworkMemberInfo.RealNetworkMemberInfo = [NSKeyedArchiver
                                               archivedDataWithRootObject:[self.realNetworkMemberInfo copy]];
                         
                          [dbRealNetworkMemberInfo commit];
                          success(self.realNetworkMemberInfo);
                      }else{
                          failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                                  JMOLocalizedString(@"alert_ok", nil));
                      }
                  }];
              }else{
                  
                  failure(operation, [self genError:error description:errorMsg], errorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}

- (void)uploadProfileImage:(UIImage*)profileImage memberId:(NSString*)memberId accessToken:(NSString*)accessToken block:(CommonBlock)block{
    
    NSDictionary *parameters = @{
                                 @"MemberID" : memberId,
                                 @"AccessToken" : accessToken,
                                 @"ListingID" : @"0",
                                 @"Position" : @"0",
                                 @"FileType" : @"4",
                                 @"FileExt" : @"jpg"
                                 };
    
    
    NSString *string = [NSString stringWithFormat:@"%@%@", kServerAddress,
                        @"/Admin.asmx/FileUpload"];
    

   NSData *imageData = [UIImage compressImageToNSData:profileImage limitedDataSize:80];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
                                    multipartFormRequestWithMethod:@"POST"
                                    URLString:string
                                    parameters:parameters
                                    constructingBodyWithBlock:^(id<AFMultipartFormData>
                                                                formData) {
                                        [formData appendPartWithFileData:imageData
                                                                    name:@"FileExt"
                                                                fileName:@"photo.jpg"
                                                                mimeType:@"image/jpeg"];
                                    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc]
                                    initWithSessionConfiguration:
                                    [NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSProgress *progress = nil;
    //    [kAppDelegate updateCurrentHudStatus:@"Uploading Photo"];
    NSURLSessionUploadTask *uploadTask = [manager
                                          uploadTaskWithStreamedRequest:request
                                          progress:&progress
                                          completionHandler:^(NSURLResponse *response,
                                                              id responseObject,
                                                              NSError *error) {
                                              NSDictionary *jsonResponse = nil;
                                              if (!error && responseObject) {
                                                  NSString *res = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                  NSError *parseError = nil;
                                                  NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:res error:&parseError];
                                                  if (!parseError && xmlDictionary) {
                                                      NSDictionary *rawDict = xmlDictionary[@"string"];
                                                      if(rawDict){
                                                          NSString *content = rawDict[@"text"];
                                                          NSString *decoded = [content stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                          NSData *data = [decoded dataUsingEncoding:NSUTF8StringEncoding];
                                                          jsonResponse= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                          
                                                          int errorCode = [jsonResponse[@"ErrorCode"]intValue ];
                                                          if (errorCode != 0) {
                                                              error =[[NSError alloc]init];
                                                          }
                                                          jsonResponse = jsonResponse[@"Content"];
                                                      }
                                                  }
                                              }
                                              if (block) {
                                                  block(jsonResponse,error);
                                              }
                                              
                                          }];
    
    [uploadTask resume];
    
}

- (void)
callRealNetworkMemberLoginEmail:(NSString *)email
passwordMD5:(NSString *)passwordMD5
Success:(void (^)(RealNetworkMemberInfo * realNetworkMemberInfo))success
failure:(failureBlock)failure {
    CocoaSecurityResult *md5 = [CocoaSecurity md5:passwordMD5];
    
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"Email\":\"%@\",\"PasswordMD5\":\"%@\"}",
                            email, md5.hex];
    
    NSString *urllink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/RealNetwork.asmx/RealNetworkMemberLogin"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    parameters =[self stringToAESEncrypt: parameters];
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *responseObjectString = [responseObject objectForKey:@"d"];
              NSString *dstring = responseObjectString;
              dstring = [self stringToAESDecrypt: responseObjectString];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              DDLogVerbose(@"Server Return String-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  self.realNetworkMemberInfo = [[RealNetworkMemberInfo alloc]
                                                initwithNSDictionary:self.HandleServerReturnString.
                                                Content error:error];
                  //// save DBLogInfo into database
                  [[[DBRealNetworkMemberInfo query] fetch] removeAll];
                  DBRealNetworkMemberInfo *dbRealNetworkMemberInfo = [DBRealNetworkMemberInfo new];
                  dbRealNetworkMemberInfo.Date = [NSDate date];
                  
                  dbRealNetworkMemberInfo.RealNetworkMemberInfo = [NSKeyedArchiver
                                                                   archivedDataWithRootObject:[self.realNetworkMemberInfo copy]];
                  
                  [dbRealNetworkMemberInfo commit];

                  success(self.realNetworkMemberInfo);
              }else{
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}

- (void)callRealNetworkMemberLoginPhoneRegID:(NSString *)phoneRegID
pinInput:(NSString *)pinInput
Success:(void (^)(RealNetworkMemberInfo * realNetworkMemberInfo))success
failure:(failureBlock)failure {

    NSString *parameters = [NSString
                            stringWithFormat:@"{\"PhoneRegID\":\"%@\",\"PINInput\":\"%@\"}",
                            phoneRegID, pinInput];
    
    NSString *urllink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/RealNetwork.asmx/RealNetworkMemberLogin"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    parameters =[self stringToAESEncrypt: parameters];
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *responseObjectString = [responseObject objectForKey:@"d"];
              NSString *dstring = responseObjectString;
              dstring = [self stringToAESDecrypt: responseObjectString];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              DDLogVerbose(@"Server Return String-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  self.realNetworkMemberInfo = [[RealNetworkMemberInfo alloc]
                                                initwithNSDictionary:self.HandleServerReturnString.
                                                Content error:error];
                  //// save DBLogInfo into database
                  [[[DBRealNetworkMemberInfo query] fetch] removeAll];
                  DBRealNetworkMemberInfo *dbRealNetworkMemberInfo = [DBRealNetworkMemberInfo new];
                  dbRealNetworkMemberInfo.Date = [NSDate date];
                  
                  dbRealNetworkMemberInfo.RealNetworkMemberInfo = [NSKeyedArchiver
                                                                   archivedDataWithRootObject:[self.realNetworkMemberInfo copy]];
                  
                  [dbRealNetworkMemberInfo commit];
                  
                  success(self.realNetworkMemberInfo);
              }else{
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}


- (void)
callRealNetworkChangeEmailApiEmail:(NSString *)email
 passwordMD5:(NSString *)passwordMD5
userID:(NSString *)userID
accessToken:(NSString *)accessToken
Success:(successBlock)success
failure:
(failureBlock)failure {
    NSString *languageCode= [AppDelegate getAppDelegate].languageCode;
    if (!isEmpty([SystemSettingModel shared].selectSystemLanguageList.Code)) {
        languageCode=[SystemSettingModel shared].selectSystemLanguageList.Code;
    }
     CocoaSecurityResult *oldmd5 = [CocoaSecurity md5:passwordMD5];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"Email\":\"%@\",\"PasswordMD5\":\"%@\",\"UserID\":\"%@\","
                            @"\"AccessToken\":\"%@\",\"Lang\":\"%@\"}",
                            email, oldmd5.hex,userID, accessToken,languageCode];
    
    NSString *urllink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/RealNetwork.asmx/RealNetworkChangeEmail"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    parameters =[self stringToAESEncrypt: parameters];
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *responseObjectString = [responseObject objectForKey:@"d"];
              NSString *dstring = responseObjectString;
              dstring = [self stringToAESDecrypt: responseObjectString];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              DDLogVerbose(@"Server Return String-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  
                  success(responseObject);
              }else{
                  
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}
- (void)
callRealNetworkChangePasswordApiNewPasswordMD5:(NSString *)newPasswordMD5
OldPasswordMD5:(NSString *)oldPasswordMD5
userID:(NSString *)userID
accessToken:(NSString *)accessToken
Success:(successBlock)success
failure:
(failureBlock)failure {
    CocoaSecurityResult *newmd5 = [CocoaSecurity md5:newPasswordMD5];
    CocoaSecurityResult *oldmd5 = [CocoaSecurity md5:oldPasswordMD5];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"NewPasswordMD5\":\"%@\",\"OldPasswordMD5\":\"%@\","
                            @"\"UserID\":\"%@\",\"AccessToken\":\"%@\"}",
                            newmd5.hex, oldmd5.hex, userID, accessToken];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/RealNetwork.asmx/RealNetworkChangePassword"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    parameters =[self stringToAESEncrypt: parameters];
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *responseObjectString = [responseObject objectForKey:@"d"];
              NSString *dstring = responseObjectString;
              dstring = [self stringToAESDecrypt: responseObjectString];
              
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              DDLogVerbose(@"Server Return String-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  
                  success(responseObject);
              }else{
                  
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}

- (void)callRealNetworkCheckEmailAvailability:(NSString *)email
                                      Success:(successBlock)success
                                      failure:(failureBlock)failure{

        
        NSString *parameters = [NSString
                                stringWithFormat:@"{\"Email\":\"%@\"}",
                                email];
        
        NSString *urllink = [NSString
                             stringWithFormat:@"%@%@", kServerAddress,
                             @"/RealNetwork.asmx/RealNetworkCheckEmailAvailability"];
        
        AFHTTPRequestOperationManager *manager =
        [self getAPiManager];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        if (![self networkConnection]) {
            failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
            return;
        }
        
        DDLogVerbose(@"apiurlstring-->:%@", urllink);
        DDLogVerbose(@"InputJson-->:%@", parameters);
        
        [manager POST:urllink
           parameters:@{
                        @"InputJson" : parameters
                        }
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  NSString *responseObjectString = [responseObject objectForKey:@"d"];
                  NSString *dstring = responseObjectString;
                  
                  
                  if ([self checkAccessError:dstring]) {
                      failure(operation, nil,
                              JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                              AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
                  }
                  
                  DDLogVerbose(@"Server Return String-->%@", dstring);
                  NSError *err = nil;
                  self.HandleServerReturnString =
                  [[HandleServerReturnString alloc] initWithString:dstring
                                                             error:&err];
                  int error = self.HandleServerReturnString.ErrorCode;
                  
                  
                  if (error == 0 || error == 20 || error == 21 || error == 22 ||
                      error == 23) {
                      
                      
                      success(responseObject);
                  }else{
                      
                      
                      failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                              JMOLocalizedString(@"alert_ok", nil));
                  }
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSString *newStr =
                  [[NSString alloc] initWithData:operation.request.HTTPBody
                                        encoding:NSUTF8StringEncoding];
                  
                  DDLogError(@"operation.request.HTTPBody-->%@", newStr);
                  DDLogError(@"response-->Error: %@", error);
                  failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }];

    
}

- (void)
callRealNetworkSendResetPasswordEmailApiEmail:(NSString *)email
Success:(successBlock)success
failure:
(failureBlock)failure {
     NSString *languageCode= [AppDelegate getAppDelegate].languageCode;
    if (!isEmpty([SystemSettingModel shared].selectSystemLanguageList.Code)) {
        languageCode=[SystemSettingModel shared].selectSystemLanguageList.Code;
    }
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"Email\":\"%@\",\"Lang\":\"%@\"}",
                            email,languageCode];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/RealNetwork.asmx/RealNetworkSendResetPasswordEmail"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *responseObjectString = [responseObject objectForKey:@"d"];
              NSString *dstring = responseObjectString;
              
              
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              DDLogVerbose(@"Server Return String-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              
              
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  success(self.HandleServerReturnString);
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}
@end