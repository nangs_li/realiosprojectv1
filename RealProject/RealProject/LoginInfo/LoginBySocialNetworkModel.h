//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
#import "BaseModel.h"
@interface LoginBySocialNetworkModel : BaseModel
@property(strong, nonatomic) LoginInfo *myLoginInfo;
@property(strong, nonatomic) SystemSettings *SystemSettings;
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(assign, nonatomic) BOOL isApplyingQBAccount;
+ (LoginBySocialNetworkModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find

 */
- (void)
callLoginBySocialNetworkModelApiDeviceType:(NSString *)deviceType
                               deviceToken:(NSString *)deviceToken
                         socialNetworkType:(NSString *)socialNetworkType
                                  userName:(NSString *)userName
                                    userID:(NSString *)userID
                                     email:(NSString *)email
                                  photoURL:(NSString *)photoURL
                               accessToken:(NSString *)accessToken
                                   Success:(void (^)(LoginInfo *myLoginInfo,
                                                     SystemSettings *
                                                         SystemSettings))success
                                   failure:
                                       (failureBlock)failure;

- (void)
callMemberProfileQBIDUpdateApiQBID:
    (NSString *)QBID Success:(void (^)(LoginInfo *myLoginInfo,
                                       SystemSettings *SystemSettings))success
                           failure:
                               (failureBlock)failure;

- (void) loginByQuickBloxOrSignUpQuickBloxWithInfo:(LoginInfo*)loginInfo
                                  Success:(successBlock)success
                                  failure:(failureBlock)failure;

- (void)reapplyQBAccountIfNeed;
- (BOOL)qbAccountIsReady;
@end
