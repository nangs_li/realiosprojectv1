//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LoginBySocialNetworkModel.h"

#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "DBLogInfo.h"
#import "DBSystemSettings.h"
#import "SystemSettingModel.h"
#import "StringEncryption.h"
#import "NSData+Base64.h"
#import "MyActivityLogModel.h"
#import "SearchAgentProfileModel.h"
#import "ChatDialogModel.h"
#import "DialogPageAgentProfilesModel.h"
#import "RealApiClient.h"
@implementation LoginBySocialNetworkModel

static LoginBySocialNetworkModel *sharedLoginBySocialNetworkModel;

+ (LoginBySocialNetworkModel *)shared {
    @synchronized(self) {
        if (!sharedLoginBySocialNetworkModel) {
            sharedLoginBySocialNetworkModel =[[LoginBySocialNetworkModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedLoginBySocialNetworkModel;
    }
}

- (void)setMyLoginInfo:(LoginInfo *)myLoginInfo{
    _myLoginInfo = myLoginInfo;
}

- (void)
callLoginBySocialNetworkModelApiDeviceType:(NSString *)deviceType
deviceToken:(NSString *)deviceToken
socialNetworkType:(NSString *)socialNetworkType
userName:(NSString *)userName
userID:(NSString *)userID
email:(NSString *)email
photoURL:(NSString *)photoURL
accessToken:(NSString *)accessToken
Success:(void (^)(LoginInfo *myLoginInfo,
                  SystemSettings *
                  SystemSettings))success
failure:
(failureBlock)failure {
    self.isApplyingQBAccount = NO;
    if ([self isEmpty:deviceToken]) {
        deviceToken = userID;
    }
    NSString *languageCode= [AppDelegate getAppDelegate].languageCode;
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"DeviceType\":%@,\"DeviceToken\":\"%@\","
                            @"\"SocialNetworkType\":%@,\"UserName\":\"%@\","
                            @"\"UserID\":\"%@\",\"Email\":\"%@\",\"PhotoURL\":\"%@"
                            @"\",\"AccessToken\":\"%@\",\"Lang\":\"%@\"}",
                            deviceType, deviceToken, socialNetworkType, userName,
                            userID, email, photoURL, accessToken,languageCode];
    
    NSString *urllink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/LoginBySocialNetwork"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    parameters=[self stringToAESEncrypt: parameters];
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *responseObjectString = [responseObject objectForKey:@"d"];
              NSString *dstring = responseObjectString;
              dstring=[self stringToAESDecrypt: responseObjectString];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              DDLogVerbose(@"Server Return String-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              [[AppDelegate getAppDelegate]deleteAllModelAndDataBaseData];
              if (error == 0 || error == 20 || error == 21 || error == 22 ||
                  error == 23) {
                  
                  
                  self.myLoginInfo = [[LoginInfo alloc]
                                      initwithNSDictionary:self.HandleServerReturnString.
                                      Content:error];
                  if([self.myLoginInfo isCSTeam]){
                      failure(operation, [self genError:500 description:@"Not authenticated"], @"Not authenticated", error, JMOLocalizedString(@"alert_alert", nil),
                              JMOLocalizedString(@"alert_ok", nil));
                      return ;
                  }
                  //// save DBLogInfo into database
                  [self.myLoginInfo saveInDataBase];
                  [SystemSettingModel shared].SystemSettings = [[SystemSettings alloc]
                                                                initwithNSDictionary:[self.HandleServerReturnString.Content
                                                                                      objectForKey:@"SystemSettings"
                                                                                      ]:error];;
                  if (![self isEmpty:[self.HandleServerReturnString.Content
                                      objectForKey:@"SystemSettings"]]) {
                      
                      DBResultSet *  r = [[[[DBSystemSettings query]
                                            whereWithFormat:@"MemberID = %@",
                                            [NSString
                                             stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
                                             .myLoginInfo.MemberID]]
                                           limit:1] fetch];
                      
                      if (r.count==0) {
                          //// save DBLogInfo into database
                          [DBSystemSettings createSearchAgentHistoryWithChatRoomRelationArray:[NSKeyedArchiver
                                                                                               archivedDataWithRootObject:[[SystemSettingModel shared]
                                                                                                                           .SystemSettings copy]]];
                          [[AppDelegate getAppDelegate] changeToPreferredLanguage];
                      }else{
                          for (DBSystemSettings *dbsystemsettings in r) {
                              [dbsystemsettings updateDBSystemSettings:[NSKeyedArchiver
                                                                        archivedDataWithRootObject:[[SystemSettingModel shared]
                                                                                                    .SystemSettings copy]]];
                              if ([self isEmpty:dbsystemsettings.selectSystemLanguageList]) {
                                  [[AppDelegate getAppDelegate] changeToPreferredLanguage];
                                  
                              } else {
                                  [SystemSettingModel shared].selectSystemLanguageList =
                                  [NSKeyedUnarchiver
                                   unarchiveObjectWithData:dbsystemsettings.selectSystemLanguageList];
                                  [[AppDelegate getAppDelegate]refreshAppLanguage];
                                  
                              }
                          }
                          
                          
                      }
                  }
                  success(self.myLoginInfo, self.SystemSettings);
                  
              }else if(error == 26){
                  failure(operation, [self genError:26 description:JMOLocalizedString(@"alert_account_suspended", nil)], JMOLocalizedString(@"alert_account_suspended", nil), 26, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}

- (void)
callMemberProfileQBIDUpdateApiQBID:
(NSString *)QBID Success:(void (^)(LoginInfo *myLoginInfo,
                                   SystemSettings *SystemSettings))success
failure:(failureBlock)failure {
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"QBID\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared]
                            .myLoginInfo.AccessToken,
                            QBID, nil];
    
    NSString *urllink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/MemberProfileQBIDUpdate"];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), 1, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    [manager POST:urllink
       parameters:@{
                    @"InputJson" : parameters
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  success(self.myLoginInfo, self.SystemSettings);
              }else{
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }];
}
- (void) loginByQuickBloxOrSignUpQuickBloxWithInfo:(LoginInfo*)loginInfo
                                           Success:(successBlock)success
                                           failure:(failureBlock)failure {
    QBUUser *user = [loginInfo qbUser];
    //// QBSignup
    if ([loginInfo hasQBAccount]) {
        [QBRequest userWithExternalID:user.externalUserID successBlock:^(QBResponse *response, QBUUser *user) {
            [[MyAgentProfileModel shared]
             callAgentProfileGetAPISuccess:^(AgentProfile *myAgentProfile) {
                 success(myAgentProfile);
                [[LoginBySocialNetworkModel shared].myLoginInfo updateCustomData];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error,
                       NSString *errorMessage, RequestErrorStatus errorStatus,
                       NSString *alert, NSString *ok){
                 failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil), 2, JMOLocalizedString(@"alert_alert", nil),
                         JMOLocalizedString(@"alert_ok", nil));
             }];
        } errorBlock:^(QBResponse *response) {
            success(nil);
        }];
    }else{
        success(nil);
    }
}

- (void)signUpQuickBlox:(QBUUser*)user Success:(successBlock)success failure:(failureBlock)failure{
    [QBRequest signUp:user
         successBlock:^(QBResponse *response, QBUUser *user) {
             // Success, do something
             [LoginBySocialNetworkModel shared].myLoginInfo.QBID =
             [NSString stringWithFormat:@"%li", (unsigned long)user.ID];
             [[[DBLogInfo query] fetch] removeAll];
             DBLogInfo *dblogInfo = [DBLogInfo new];
             dblogInfo.Date = [NSDate date];
             dblogInfo.LogInfo = [NSKeyedArchiver
                                  archivedDataWithRootObject:[[LoginBySocialNetworkModel shared]
                                                              .myLoginInfo copy]];
             dblogInfo.MemberID = [[LoginBySocialNetworkModel shared]
                                   .myLoginInfo.MemberID intValue];
             [dblogInfo commit];
             [[LoginBySocialNetworkModel shared]
              callMemberProfileQBIDUpdateApiQBID:
              [NSString stringWithFormat:@"%lu", (unsigned long)user.ID]
              Success:^(LoginInfo *myLoginInfo,
                        SystemSettings *SystemSettings) {
                  success(myLoginInfo.AgentProfile);
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error,
                        NSString *errorMessage, RequestErrorStatus errorStatus,
                        NSString *alert, NSString *ok) {
                  failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                          RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }];
             
         }
           errorBlock:^(QBResponse *response) {
               failure(nil, nil, JMOLocalizedString(@"error_message__server_not_response", nil),
                       RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
               
               // error handling
               DDLogError(@"QBRequest signUp: %@", response.error);
           }];
    
}

- (void)reapplyQBAccountIfNeed{
    if (self.myLoginInfo &&![self.myLoginInfo hasQBAccount] && !self.isApplyingQBAccount) {
        self.isApplyingQBAccount = YES;
        [[RealApiClient sharedClient]applyQBAccountWithCompletion:^(id response, NSError *error) {
            if (!error && [self.myLoginInfo hasQBAccount] && self.isApplyingQBAccount) {
                [kAppDelegate quickBloxTotalLoginWithSuccessBlock:^(QBResponse *response, QBUUser *user) {
                    if (self.isApplyingQBAccount) {
                        [[ChatDialogModel shared] requestDialogsWithCompletionBlock:^{
                            if (self.isApplyingQBAccount) {
                                [[DialogPageAgentProfilesModel shared] AgentProfileGetListsuccess:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                                    if (self.isApplyingQBAccount) {
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDialogsUpdated object:nil];
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDidReapplyQBAccount object:nil];
                                        [[ChatDialogModel shared] updateChatBadgeNumber];
                                        [[DialogPageAgentProfilesModel shared]callUpdateDBDiaolgPageAgentProfileTimer];
                                        self.isApplyingQBAccount = NO;
                                        
                                    }
                                }failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                                    self.isApplyingQBAccount = NO;
                                }];
                            }
                        }];
                    }
                
                 } errorBlock:^(QBResponse *  response) {
                        self.isApplyingQBAccount = NO;
                 } retry:3];
            }else{
                self.isApplyingQBAccount = NO;
            }
        }];
    }
}

- (BOOL)qbAccountIsReady{
    return [[LoginBySocialNetworkModel shared].myLoginInfo hasQBAccount] && ![LoginBySocialNetworkModel shared].isApplyingQBAccount;
}

@end