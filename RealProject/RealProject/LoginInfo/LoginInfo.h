//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"
@class AgentProfile;
@interface LoginInfo : JSONModel
//+ (d *)shared;

@property(strong, nonatomic) NSString* MemberName;
@property(strong, nonatomic) NSString* AccessToken;
@property(strong, nonatomic) NSString* PhotoURL;
@property(strong, nonatomic) NSNumber* MemberID;
@property(strong, nonatomic) NSNumber* AgentListingID;
@property(strong, nonatomic) NSString* QBID;
@property(strong, nonatomic) AgentProfile *AgentProfile;
@property(assign, nonatomic) BOOL isNewUser;
@property(strong, nonatomic) NSMutableArray *mute;
@property(strong, nonatomic) NSMutableArray *block;
@property(assign, nonatomic) NSInteger MemberType;
@property(assign, nonatomic) int FollowingCount;
@property(assign, nonatomic) int FollowerCount;
@property(strong, nonatomic) NSString *QBPwd;
@property(strong, nonatomic) NSString *CountryCode;
@property(strong, nonatomic) NSString *PhoneNumber;
@property(assign, nonatomic) BOOL chatroomshowlastseen;
@property(assign, nonatomic) BOOL chatroomshowreadreceipts;
- (LoginInfo*)initwithjsonstring:(NSString*)content:(int)error;
- (LoginInfo*)initwithNSDictionary:(NSDictionary*)content:(int)error;
- (NSArray*)tags;
- (QBUUser*)qbUser;
- (QBUUserCustomData*) customData;
- (void)saveInDataBase;
- (void)updateCustomData;
- (void)updateCustomDataWithSuccessBlock:(QB_NULLABLE void (^)(QBResponse * QB_NONNULL_S response, QBUUser * QB_NULLABLE_S user))successBlock errorBlock:(QB_NULLABLE void (^)(QBResponse * QB_NONNULL_S response))errorBlock;
- (void)mute:(BOOL)mute withQBID:(NSString*)QBID;
- (void)block:(BOOL)block withQBID:(NSString*)QBID;

- (BOOL)isCSTeam;
- (BOOL)showInviteViralAfterPublish;
- (void)didShowInviteViral;
- (void)didSuccessInviteViral;
- (void) updateFollowerCount:(int)newCount;
- (void) updateFollowingCount:(int)newCount;
- (NSInteger)followingCount;
- (BOOL)isLoggedIn;
- (BOOL)hasQBAccount;
@end
