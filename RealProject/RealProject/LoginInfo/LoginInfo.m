//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LoginInfo.h"
#import "NSObject+Utility.h"
#import "NSString+Utility.h"
#import "SDWebImagePrefetcher.h"
#import "DBLogInfo.h"
#import "DBFollowAgentRecord.h"
@implementation LoginInfo
static LoginInfo *sharedloginInfo;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

+ (LoginInfo *)shared {
    @synchronized(self) {
        if (!sharedloginInfo) {
            sharedloginInfo = [[LoginInfo alloc] init];
        }
        return sharedloginInfo;
    }
}

- (LoginInfo *)initwithjsonstring:(NSString *)content:(int)error {
    sharedloginInfo = nil;
    
    NSError *err = nil;
    sharedloginInfo = [[LoginInfo alloc] initWithString:content error:&err];
    if (!isEmpty(sharedloginInfo.PhotoURL)){
    [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:@[[NSURL URLWithString:sharedloginInfo.PhotoURL]]];
    }
    if (error == 22) {
        sharedloginInfo.isNewUser = YES;
    } else {
        sharedloginInfo.isNewUser = NO;
    }
    return sharedloginInfo;
}
- (LoginInfo *)initwithNSDictionary:(NSDictionary *)content:(int)error {
    sharedloginInfo = nil;
    
    NSError *err = nil;
    sharedloginInfo = [[LoginInfo alloc] initWithDictionary:content error:&err];
    if (!isEmpty(sharedloginInfo.PhotoURL)) {
         [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:@[[NSURL URLWithString:sharedloginInfo.PhotoURL]]];
           }else{
        NSError *error = [NSError errorWithDomain:@"LoginInfo.PhotoURL.EMPTY" code:404 userInfo:nil];
        if ([content valid]) {
            [CrashlyticsKit recordError:error withAdditionalUserInfo:content];
        }
    }
    if (error == 22) {
        sharedloginInfo.isNewUser = YES;
    } else {
        sharedloginInfo.isNewUser = NO;
    }
    return sharedloginInfo;
}


-(NSArray*)tags{
    NSMutableArray *tags = [[NSMutableArray alloc]init];
    NSMutableArray *nameTags = [[NSMutableArray alloc]init];
    NSMutableArray *locationTags = [[NSMutableArray alloc]init];
    NSString *memberName = self.MemberName;
    
    NSArray *memberNames = [memberName componentsSeparatedByString:@" "];
    for (NSString *names in memberNames) {
        [nameTags addObjectsFromArray:[names tags]];
    }
    if ([self.AgentProfile.AgentListing valid]) {
        GoogleAddress *googleAddress = [self.AgentProfile.AgentListing.GoogleAddress firstObject];
        NSString *location = googleAddress.Name;
        NSArray *locations = [location componentsSeparatedByString:@" "];
        for (NSString *names in locations) {
            [locationTags addObjectsFromArray:[names tags]];
        }
    }
    //    [tags addObjectsFromArray:nameTags];
    [tags addObjectsFromArray:locationTags];
    return tags;
}
- (QBUUser*)qbUser{
    NSString *fullName= self.MemberName;
    NSString *photoURL = self.PhotoURL;
    NSString *username = [NSString stringWithFormat:@"%010d", [self.MemberID intValue]];
    NSString *password = self.QBPwd;
    
    
    QBUUser *user = [QBUUser user];
    user.ID = [self.QBID integerValue];
    user.login = username;
    user.password = password;
    user.externalUserID = [self.MemberID integerValue];
    user.fullName = fullName;
    user.website = photoURL;
    //    user.tags = [self.tags mutableCopy];
    user.customData = [[self customData] toJSONString];
    return user;
}

-(void) saveInDataBase{
    [[[DBLogInfo query] fetch] removeAll];
    DBLogInfo *dblogInfo = [DBLogInfo new];
    dblogInfo.Date = [NSDate date];
    dblogInfo.LogInfo = [NSKeyedArchiver
                         archivedDataWithRootObject:[self copy]];
    dblogInfo.MemberID = [self.MemberID intValue];
    [dblogInfo commit];
}

-(QBUUserCustomData*) customData{
        QBUUserCustomData *customData = [QBUUserCustomData new];
        customData.chatroomshowlastseen = self.chatroomshowlastseen;
        customData.chatroomshowreadreceipts = self.chatroomshowreadreceipts;
        customData.mypersonalphotourl = self.PhotoURL;
        customData.photoURL =self.PhotoURL;
        customData.chatVersion = kChatVersion;
        if (self.mute) {
            customData.mute = [self.mute componentsJoinedByString:@","];
        }
        if (self.block) {
            customData.block = [self.block componentsJoinedByString:@","];
        }
        if ([self.AgentProfile.AgentListing valid]) {
            customData.agentListing = [self.AgentProfile.AgentListing toJSONString];
            customData.agentListingID = [NSString stringWithFormat:@"%d",self.AgentProfile.AgentListing.AgentListingID];
        }else{
            customData.agentListing = @"";
            customData.agentListingID = @"";
        }
        customData.memberID = [self.MemberID stringValue];
        customData.lastUpdateTimeStamp = kTimeStamp;
    return customData;
}

- (void)updateCustomData{
    [self updateCustomDataWithSuccessBlock:nil errorBlock:nil];
}

- (void)updateCustomDataWithSuccessBlock:(QB_NULLABLE void (^)(QBResponse * QB_NONNULL_S response, QBUUser * QB_NULLABLE_S user))successBlock
                              errorBlock:(QB_NULLABLE void (^)(QBResponse * QB_NONNULL_S response))errorBlock{
    [self saveInDataBase];
   // if ([ChatService shared].isConnected) {
        QBUpdateUserParameters *updateParameters = [QBUpdateUserParameters new];
        updateParameters.customData = [[self customData] toJSONString];
        updateParameters.website = self.PhotoURL;
        //    updateParameters.tags = [[self tags] mutableCopy];
        [QBRequest updateCurrentUser:updateParameters successBlock:^(QBResponse *response, QBUUser *user) {
            // User updated successfully
            if (successBlock) {
                successBlock(response,user);
            }
        } errorBlock:^(QBResponse *response) {
            if (errorBlock) {
                errorBlock(response);
            }
        }];
  //  }
}

-(void)mute:(BOOL)mute withQBID:(NSString *)QBID{
    [self setUserRelation:QBUUserRelationMute withQBID:QBID positive:mute];
}

-(void)block:(BOOL)block withQBID:(NSString *)QBID{
    [self setUserRelation:QBUUserRelationBlock withQBID:QBID positive:block];
}

-(void)setUserRelation:(QBUUserRelation)relation withQBID:(NSString*)QBID positive:(BOOL)positive{
    if ([QBID valid]) {
        BOOL needUpdate = NO;
        NSMutableArray *newRelation ;
        if (relation == QBUUserRelationBlock) {
            newRelation  = [self.block mutableCopy];
        }else if (relation == QBUUserRelationMute){
            newRelation = [self.mute mutableCopy];
        }
        
        if (![newRelation valid]) {
            newRelation = [[NSMutableArray alloc]init];
        }
        
        if (positive) {
            if (![newRelation containsObject:QBID]) {
                [newRelation addObject:QBID];
                needUpdate = YES;
            }
        }else{
            if ([newRelation containsObject:QBID]) {
                [newRelation removeObject:QBID];
                needUpdate = YES;
            }
        }
        
        if (needUpdate) {
            if (relation == QBUUserRelationBlock) {
                self.block = [newRelation mutableCopy];
            }else if (relation == QBUUserRelationMute){
                self.mute = [newRelation mutableCopy];
            }
            [self updateCustomData];
        }
    }
}

- (BOOL)isCSTeam{
    return self.MemberType == 4 ||self.MemberType == 5;
}

- (BOOL)showInviteViralAfterPublish{
    if ([self.MemberID valid]) {
        NSString *key = [NSString stringWithFormat:@"publishInviteViralKey_%@",self.MemberID];
        NSInteger showTime = [[[NSUserDefaults standardUserDefaults]objectForKey:key] integerValue];
        return showTime < 10;
    }else{
        return NO;
    }
}
- (void)didShowInviteViral{
    if ([self.MemberID valid]) {
        NSString *key = [NSString stringWithFormat:@"publishInviteViralKey_%@",self.MemberID];
        NSInteger showTime = [[[NSUserDefaults standardUserDefaults]objectForKey:key] integerValue];
        showTime++;
        [[NSUserDefaults standardUserDefaults]setObject:@(showTime) forKey:key];
    }
}
- (void)didSuccessInviteViral{
    if ([self.MemberID valid]) {
        NSString *key = [NSString stringWithFormat:@"publishInviteViralKey_%@",self.MemberID];
        [[NSUserDefaults standardUserDefaults]setObject:@(11) forKey:key];
    }
}


- (void) updateFollowerCount:(int)newCount{
    if(newCount < 0){
        newCount = 0;
    }
    self.FollowerCount = newCount;
    [self saveInDataBase];
}

- (void) updateFollowingCount:(int)newCount{
    if(newCount < 0){
        newCount = 0;
    }
    self.FollowingCount = newCount;
    [self saveInDataBase];
}
- (NSInteger)followingCount{
    NSInteger baseFollowingCount = self.FollowingCount;
    return baseFollowingCount;
}
- (BOOL)isLoggedIn{
    
    return [self.MemberID valid];
}

- (BOOL)hasQBAccount{
    return [self.QBID valid] && [self.QBPwd valid];
}
@end