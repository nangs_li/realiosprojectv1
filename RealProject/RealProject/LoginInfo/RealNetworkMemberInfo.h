//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@interface RealNetworkMemberInfo : JSONModel
//+ (d *)shared;

@property(strong, nonatomic) NSString* UserID;
@property(strong, nonatomic) NSString* FirstName;
@property(strong, nonatomic) NSString* LastName;
@property(strong, nonatomic) NSString* Email;
@property(strong, nonatomic) NSString* PhotoURL;
@property(strong, nonatomic) NSString* EmailVerified;
@property(strong, nonatomic) NSString* AccessToken;

- (RealNetworkMemberInfo*)initwithjsonstring:(NSString*)contentString error:(int)error;
- (RealNetworkMemberInfo*)initwithNSDictionary:(NSDictionary*)contentDict error:(int)error;
@end
