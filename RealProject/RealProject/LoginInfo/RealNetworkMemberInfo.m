//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "RealNetworkMemberInfo.h"

@implementation RealNetworkMemberInfo
static RealNetworkMemberInfo *sharedRealNetworkMemberInfo;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (RealNetworkMemberInfo *)shared {
  @synchronized(self) {
    if (!sharedRealNetworkMemberInfo) {
      sharedRealNetworkMemberInfo = [[RealNetworkMemberInfo alloc] init];
    }
    return sharedRealNetworkMemberInfo;
  }
}

- (RealNetworkMemberInfo *)initwithjsonstring:(NSString *)content error:(int)error {
  sharedRealNetworkMemberInfo = nil;

  NSError *err = nil;
  sharedRealNetworkMemberInfo = [[RealNetworkMemberInfo alloc] initWithString:content error:&err];
  return sharedRealNetworkMemberInfo;
}
- (RealNetworkMemberInfo *)initwithNSDictionary:(NSDictionary *)content error:(int)error {
  sharedRealNetworkMemberInfo = nil;

  NSError *err = nil;
  sharedRealNetworkMemberInfo = [[RealNetworkMemberInfo alloc] initWithDictionary:content error:&err];
  return sharedRealNetworkMemberInfo;
}

@end