//
//  LoginUIViewController.h
//  FBLoginUIControlSample
//
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import <MediaPlayer/MediaPlayer.h>

typedef NS_ENUM (NSInteger, LoginUiViewControllerType) {
    Login,
    Mirgate
};

@interface LoginUIViewController
: BaseNavigationController

@property(strong, nonatomic) MPMoviePlayerController * moviePlayer;
@property (assign, nonatomic) BOOL firstTimeEnterLoginUIViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil type:(LoginUiViewControllerType)type;
- (void)setApiDelegateToLoginUiViewController;



@end
