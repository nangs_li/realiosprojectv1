//
//  LoginUIViewController.m
//  FBLoginUIControlSample
//
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Ken All rights reserved.
//

/* This sample implements Login with Facebook using the standard Login button.
 It asks for the public_profile, email and user_likes permissions.
 You can see the tutorial that accompanies this sample here:
 https://developers.facebook.com/docs/ios/login-tutorial/#login-button
 
 For simplicity, this sample does limited error handling. You can read more
 about handling errors in our Error Handling guide:
 https://developers.facebook.com/docs/ios/errors
 */

#import "LoginUIViewController.h"
#import "RealLoginViewController.h"
// Mixpanel
#import "Mixpanel.h"
#import "PhoneNumberViewController.h"

@interface LoginUIViewController ()

@property (nonatomic, assign) PhoneNumberControllerType type;

@end

@implementation LoginUIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil type:(LoginUiViewControllerType)type {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.type =type;
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setupView];
    [self setupNotificationDelegate];
    [self setupValue];
    
    PhoneNumberViewController * phoneNumberViewController;
    
    if (self.type == Mirgate) {
        
        phoneNumberViewController = [[PhoneNumberViewController alloc] initWithType:PhoneNumberControllerTypeRegFromPhoneNumberMigrate];
        
    } else {
        
        phoneNumberViewController = [[PhoneNumberViewController alloc] initWithType:PhoneNumberControllerTypeReg];
        
    }
    
    [self pushViewController:phoneNumberViewController animated:YES];
    
}

#pragma mark - Setup
- (void)setupView {
    
    [self setupMoviePlayer];
    [self setupBlackView];
    
}

- (void)setupMoviePlayer {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"demo_video 2" ofType:@"mp4"];
    NSURL    *fileURL    =   [NSURL fileURLWithPath:path];
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    [self.view insertSubview:self.moviePlayer.view atIndex:0];
    self.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    [self.moviePlayer.view autoPinEdgesToSuperviewEdges];    self.moviePlayer.repeatMode = MPMovieRepeatModeOne;
    self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    [self.moviePlayer.view  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.moviePlayer prepareToPlay];
    [self.moviePlayer play];
    
}

- (void)setupBlackView {
    
    UIView * uiView = [[UIView alloc]init];
    [uiView setBackgroundColor:[UIColor blackColor]];
    uiView.alpha = 0.5;
    [self.view insertSubview:uiView atIndex:1];
    [uiView autoPinEdgesToSuperviewEdges];
    
}

#pragma mark - Support Method

- (void)setupNotificationDelegate {
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationApplcationDidBecomeActive object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         
         [self.moviePlayer play];
         
     }];
    
    
}

- (void)setupValue {
    
    self.firstTimeEnterLoginUIViewController=YES;
    [AppDelegate getAppDelegate].loginUIViewController = self;
    
}

- (void)setApiDelegateToLoginUiViewController {
    
    [AppDelegate getAppDelegate].loginUIViewController = self;
    
}

#pragma mark - App LifeCycle
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying) {
        
        [self.moviePlayer play];
        
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.firstTimeEnterLoginUIViewController) {
        
        self.firstTimeEnterLoginUIViewController=NO;
        
    }
    
    if (self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying) {
        
        [self.moviePlayer play];
        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.moviePlayer pause];
    
}

@end
