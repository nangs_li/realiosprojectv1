//
//  MWZoomingScrollView+Custom.m
//  productionreal2
//
//  Created by Alex Hung on 4/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "MWZoomingScrollView+Custom.h"
#import "MWTapDetectingImageView.h"
#import <DACircularProgress/DACircularProgressView.h>
@implementation MWZoomingScrollView (Custom)

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (CGFloat)initialZoomScaleWithMinScale {
    CGFloat zoomScale = self.minimumZoomScale;
    if ([self photoImageView] && [self photoBrowser].zoomPhotosToFill) {
        // Zoom image to fill if the aspect ratios are fairly similar
        CGSize boundsSize = self.bounds.size;
        CGSize imageSize = [self photoImageView].image.size;
        CGFloat boundsAR = boundsSize.width / boundsSize.height;
        CGFloat imageAR = imageSize.width / imageSize.height;
        CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
        CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
        // Zooms standard portrait images on a 3.5in screen but not on a 4in screen.
        if (ABS(boundsAR - imageAR) < 0.17) {
            zoomScale = MAX(xScale, yScale);
            // Ensure we don't zoom in or out too far, just in case
            zoomScale = MIN(MAX(self.minimumZoomScale, zoomScale), self.maximumZoomScale);
        }
    }
    return zoomScale;
}

- (void)setMaxMinZoomScalesForCurrentBounds {
    
    // Reset
    self.maximumZoomScale = 1;
    self.minimumZoomScale = 1;
    self.zoomScale = 1;
    
    // Bail if no image
    if ([self photoImageView].image == nil) return;
    
    // Reset position
    [self photoImageView].frame = CGRectMake(0, 0, [self photoImageView].frame.size.width, [self photoImageView].frame.size.height);
    
    // Sizes
    CGSize boundsSize = self.bounds.size;
    CGSize imageSize = [self photoImageView].image.size;
    
    // Calculate Min
    CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
    CGFloat minScale = MIN(xScale, yScale);                 // use minimum of these to allow the image to become fully visible
    
    // Calculate Max
    CGFloat maxScale = 3;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // Let them go a bit bigger on a bigger screen!
        maxScale = 4;
    }
    
    // Image is smaller than screen so no zooming!
    if (xScale >= 1 && yScale >= 1) {
        minScale = 1.0;
    }
    
    // Set min/max zoom
    self.maximumZoomScale = maxScale;
    self.minimumZoomScale = minScale;
    
    // Initial zoom
    self.zoomScale = [self initialZoomScaleWithMinScale];
    
    // If we're zooming to fill then centralise
    if (self.zoomScale != minScale) {
        
        // Centralise
        self.contentOffset = CGPointMake((imageSize.width * self.zoomScale - boundsSize.width) / 2.0,
                                         (imageSize.height * self.zoomScale - boundsSize.height) / 2.0);
        
    }
    [self updateZoomScrollView:self image:self.photoImageView.image ];
    // Disable scrolling initially until the first pinch to fix issues with swiping on an initally zoomed in photo
    self.scrollEnabled = NO;
    
    // If it's a video then disable zooming
    if ([self displayingVideo]) {
        self.maximumZoomScale = self.zoomScale;
        self.minimumZoomScale = self.zoomScale;
    }
    
    // Layout
    [self setNeedsLayout];
    
}

- (MWTapDetectingImageView*)photoImageView{
    return  [self valueForKey:@"_photoImageView"];
}

- (MWTapDetectingView*)tapView{
    return [self valueForKey:@"_tapView"];
}

- (DACircularProgressView*)loadingIndicator{
    return [self valueForKey:@"_loadingIndicator"];
}
- (MWPhotoBrowser*)photoBrowser{
    return [self valueForKey:@"_photoBrowser"];
}


- (UIImageView*) loadingError{
    return [self valueForKey:@"_loadingError"];
}

- (void)updateZoomScrollView:(UIScrollView *)scrollView image:(UIImage*)image{
    if (image) {
        scrollView.minimumZoomScale = MIN(scrollView.bounds.size.width / image.size.width, scrollView.bounds.size.height / image.size.height);
        
        if (scrollView.maximumZoomScale < scrollView.minimumZoomScale) {
            scrollView.maximumZoomScale = scrollView.minimumZoomScale;
        }
        if (scrollView.zoomScale < scrollView.minimumZoomScale){
            scrollView.zoomScale = scrollView.minimumZoomScale;
        }
    }
}
- (CGFloat)updateZoomScrollViewReturn:(UIScrollView *)scrollView image:(UIImage*)image{
    if (image) {
        scrollView.minimumZoomScale = MIN(scrollView.bounds.size.width / image.size.width, scrollView.bounds.size.height / image.size.height);
        
        if (scrollView.maximumZoomScale < scrollView.minimumZoomScale) {
            scrollView.maximumZoomScale = scrollView.minimumZoomScale;
        }
        if (scrollView.zoomScale < scrollView.minimumZoomScale){
            scrollView.zoomScale = scrollView.minimumZoomScale;
        }
        
        return scrollView.minimumZoomScale;
    }
     return 1.0f;
}

//
//-(void)updateZoomScrollView:(UIScrollView ​*)updateZoomScrollView imageView:(UIImageView *)imageView{
//    updateZoomScrollView.minimumZoomScale = MIN(updateZoomScrollView.bounds.size.width / imageView.image.size.width, updateZoomScrollView.bounds.size.height / imageView.image.size.height);
//
//    if (updateZoomScrollView.zoomScale < updateZoomScrollView.minimumZoomScale)
//        updateZoomScrollView.zoomScale = updateZoomScrollView.minimumZoomScale;
//}
@end
