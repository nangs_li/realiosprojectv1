//
//  ActivityLogGetModel.h
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"
#import "ActivityLogModel.h"
@interface ActivityLogGetModel : JSONModel

@property (nonatomic,assign) NSInteger TotalCount;
@property (nonatomic,assign) NSInteger RecordCount;
@property (nonatomic,strong) NSMutableArray<ActivityLogModel>* Log;
@end
