//
//  ActivityLogModel.h
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"
#import "ActivityLogPlaceModel.h"


@protocol ActivityLogModel
@end

@class DBActivityLog;
@interface ActivityLogModel : JSONModel
@property (nonatomic,assign) NSInteger ID;
@property (nonatomic,assign) NSInteger MemberID;
@property (nonatomic,strong) NSDate *CreationDate;
@property (nonatomic,strong) NSString *CreationDateString;
@property (nonatomic,assign) NSInteger LogType;
@property (nonatomic,strong) NSArray<ActivityLogPlaceModel> *Places;
@property (nonatomic,strong) NSString *BasedIn;
@property (nonatomic,strong) NSDate *DateOfJoin;
@property (nonatomic,strong) NSDate *DateOfFollow;
@property (nonatomic,strong) NSString *DateOfJoinString;
@property (nonatomic,strong) NSString *DateOfFollowString;
@property (nonatomic,assign) NSInteger FollowingCount;
@property (nonatomic,assign) NSInteger ActiveChatCount;

- (NSString*)logMarkUp;
- (UIImage*)logIconImage;
- (NSString*) joinDateString;
- (NSString*) followingCountString;
- (NSString*) followedTimeString;
- (NSString*) locationString;
- (NSString*) activeChatString;
- (NSDate*)joinDate;
- (NSDate*)followDate;

+ (ActivityLogModel*)initWithDBActivityLog:(DBActivityLog*)activityLog;

+ (NSArray*) initWithDBActivityLogs:(NSArray*)dbLogs;
@end
