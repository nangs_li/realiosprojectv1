//
//  ActivityLogModel.m
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ActivityLogModel.h"
#import "DBActivityLog.h"
#import "NSDateFormatter+Utility.h"
@implementation ActivityLogModel
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

- (NSString*)logMarkUp{
    NSString *logString = @"";
    ActivityLogPlaceModel *topPlaceModel = [self.Places firstObject];
    NSString *location = [NSString stringWithFormat:@"<realBlueBold>%@</realBlueBold>",topPlaceModel.Location];
    NSString *dateString = [NSString stringWithFormat:@"<paleLightGraySmall>%@</paleLightGraySmall>",[RealUtility timeDifferenceStringFromDate:self.createDate toDate:[NSDate date]]];
    switch (self.LogType) {
        case ActivityLogTypeFollow:{
            NSString * someOneFollowingYouString = [NSString stringWithFormat:JMOLocalizedString(@"me__someone_following_you", nil),location];
            logString = [NSString stringWithFormat:@"%@ %@",someOneFollowingYouString,dateString];
            break;
        }case ActivityLogTypeUnfollow:{
            NSString * someOneFollowingYouString = [NSString stringWithFormat:JMOLocalizedString(@"me__someone_unfollowing_you", nil),location];
            logString = [NSString stringWithFormat:@"%@ %@",someOneFollowingYouString,dateString];
            break;
        }case ActivityLogTypeInviteToFollow:{
            logString = [NSString stringWithFormat:@"%@ %@",JMOLocalizedString(@"me__someone_invited_following_you", nil),dateString];
        }default:{
            break;
        }
    }

    return logString;
}

- (UIImage*) logIconImage{
    NSString *iconImage = @"";
    switch (self.LogType) {
        case ActivityLogTypeFollow:{
            iconImage = @"activity_log_icon_follow";
            break;
        }case ActivityLogTypeUnfollow:{
            iconImage = @"activity_log_icon_unfollow";
            break;
        }case ActivityLogTypeInviteToFollow:{
            iconImage = @"activity_log_icon_invitefollow";
            break;
        }default:{
            break;
        }
    }
    return [UIImage imageNamed:iconImage];
}

- (NSString*) joinDateString{   
    NSString *dateString = [RealUtility timeDifferenceStringFromDate:self.joinDate toDate:[NSDate date]];
    return dateString;
}
- (NSString*) followingCountString{
    NSString *countString = @"";
    if (self.FollowingCount>1) {
        countString =[NSString stringWithFormat:@"%ld %@ ", (long)self.FollowingCount,JMOLocalizedString(@"common__agents", nil)];
    }else{
        countString =[NSString stringWithFormat:@"%ld %@ ", (long)self.FollowingCount,JMOLocalizedString(@"common__agent", nil)];
    }
    return countString;
}
- (NSString*) followedTimeString{
    NSString *dateString = [RealUtility timeDifferenceStringFromDate:self.followDate toDate:[NSDate date]];
    return dateString;
}
- (NSString*) locationString{
    NSString *locationString = self.BasedIn;
    return locationString;
}
- (NSString*) activeChatString{
    NSString *countString = [NSString stringWithFormat:@"%d",(int)self.ActiveChatCount];
    return countString;
}

- (NSDate*)joinDate{
    NSDateFormatter *localeFormatter = [NSDateFormatter localeDateFormatter];
    [localeFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [localeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [localeFormatter dateFromString:self.DateOfJoinString];
}
- (NSDate*)followDate{
    NSDateFormatter *localeFormatter = [NSDateFormatter localeDateFormatter];
    [localeFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        [localeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [localeFormatter dateFromString:self.DateOfFollowString];
}

- (NSDate*)createDate{
    NSDateFormatter *localeFormatter = [NSDateFormatter localeDateFormatter];
    [localeFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        [localeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [localeFormatter dateFromString:self.CreationDateString];
}

+ (ActivityLogModel*)initWithDBActivityLog:(DBActivityLog*)activityLog{
    ActivityLogModel *logModel = [[ActivityLogModel alloc]init];
    logModel.ID = activityLog.ID;
    logModel.CreationDateString = activityLog.CreationDateString;
    logModel.LogType = activityLog.LogType;
    logModel.Places = [NSKeyedUnarchiver unarchiveObjectWithData:activityLog.Places];
    logModel.BasedIn = activityLog.BasedIn;
    logModel.DateOfFollow = activityLog.DateOfFollow;
    logModel.DateOfJoin = activityLog.DateOfJoin;
    logModel.DateOfJoinString = activityLog.DateOfJoinString;
    logModel.DateOfFollowString = activityLog.DateOfFollowString;
    logModel.FollowingCount = activityLog.FollowingCount;
    logModel.ActiveChatCount = activityLog.ActiveChatCount;
    return logModel;
}
+ (NSArray*) initWithDBActivityLogs:(NSArray*)dbLogs{
    NSMutableArray *activityLogs = [[NSMutableArray alloc]init];
    for (DBActivityLog *log in dbLogs) {
        [activityLogs addObject:[[ActivityLogModel initWithDBActivityLog:log] copy]];
    }
    return activityLogs;
}
@end
