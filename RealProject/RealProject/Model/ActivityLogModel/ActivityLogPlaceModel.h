//
//  ActivityLogPlaceModel.h
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"
@protocol ActivityLogPlaceModel
@end
@interface ActivityLogPlaceModel : JSONModel
@property (nonatomic,strong) NSString *Location;
@property (nonatomic,strong) NSString *Percentage;

- (CGFloat )percentageFloat;
- (NSString*) percentageString;
@end
