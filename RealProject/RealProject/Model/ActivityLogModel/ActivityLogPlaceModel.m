//
//  ActivityLogPlaceModel.m
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ActivityLogPlaceModel.h"

@implementation ActivityLogPlaceModel
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

- (CGFloat )percentageFloat{
    CGFloat percentage = [self.Percentage floatValue];
    return percentage/100.0;
}

- (NSString*) percentageString{
    CGFloat percentage = [self.Percentage floatValue];
    return [NSString stringWithFormat:@"%.0f%%",percentage];
}
@end
