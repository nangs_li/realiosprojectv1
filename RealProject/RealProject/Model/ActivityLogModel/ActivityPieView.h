//
//  ActivityPieView.h
//  productionreal2
//
//  Created by Alex Hung on 8/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PieLayer, PieElement;
@interface ActivityPieView : UIView

- (void)setSlices:(NSArray*)slices;
- (void)clearAllSlices;
@end

@interface ActivityPieView (ex)
@property(nonatomic,readonly,retain) PieLayer *layer;
@end