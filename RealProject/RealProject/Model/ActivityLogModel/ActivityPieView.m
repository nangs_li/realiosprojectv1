//
//  ActivityPieView.m
//  productionreal2
//
//  Created by Alex Hung on 8/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ActivityPieView.h"
#import "MagicPieLayer.h"
#import "PieChatSlice.h"
@implementation ActivityPieView
+ (Class)layerClass
{
    return [PieLayer class];
}

- (id)init
{
    self = [super init];
    if(self){
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setup];
    }
    return self;
}

- (void)setup{
    self.layer.maxRadius = CGRectGetWidth(self.bounds)/2 -4;
    self.layer.minRadius = 20;
    self.layer.animationDuration = 0.6;
    self.layer.showTitles = ShowTitlesNever;
    self.layer.startAngle = 0;
    self.layer.endAngle = self.layer.startAngle+360;
    if ([self.layer.self respondsToSelector:@selector(setContentsScale:)])
    {
        self.layer.contentsScale = [[UIScreen mainScreen] scale];
    }
    self.layer.transform = CATransform3DScale(CATransform3DMakeRotation(M_PI / 2.0f, 0, 0, 1),
                                                    -1, 1, 1);

}

-(void)layoutSubviews{
    [super layoutSubviews];
}

- (void)setSlices:(NSArray *)slices{
    [self clearAllSlices];
    NSMutableArray *pieElements = [[NSMutableArray alloc]init];
    for (PieChatSlice *slice in slices) {
        PieElement *element = [PieElement pieElementWithValue:slice.value color:slice.color];
        element.centrOffset = 2.0;
        [pieElements addObject:element];
    }
    [self.layer addValues:pieElements animated:YES];
}

-(void)clearAllSlices{
    [self.layer deleteValues:self.layer.values animated:NO];
}


@end
