//
//  MyActivityLogModel.h
//  productionreal2
//
//  Created by Alex Hung on 11/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyActivityLogModel : NSObject
+ (MyActivityLogModel *)shared;
@property (nonatomic,assign) NSInteger newLogCount;
@end
