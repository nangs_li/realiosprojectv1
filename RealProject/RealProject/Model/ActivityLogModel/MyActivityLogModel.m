//
//  MyActivityLogModel.m
//  productionreal2
//
//  Created by Alex Hung on 11/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "MyActivityLogModel.h"

@implementation MyActivityLogModel
+ (MyActivityLogModel *)shared{
    static MyActivityLogModel *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
         shared = [MyActivityLogModel loadFromUserDefault];
        if (!shared) {
             shared = [[self alloc] init];
        }
        shared.newLogCount = shared.newLogCount;
       
    });
    return shared;
}

+(MyActivityLogModel*)loadFromUserDefault{
    NSString *key = [NSString stringWithFormat:@"activity_log_%@",[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue]];
    MyActivityLogModel* savedModel = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults]objectForKey:key]];
    return savedModel;
}

-(void)saveInUserDefault{
    NSString *key = [NSString stringWithFormat:@"activity_log_%@",[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue]];
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self] forKey:key];
}

-(void)setNewLogCount:(NSInteger)newLogCount{
    _newLogCount = newLogCount;
    if (_newLogCount > 0) {
        [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
          objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%d",(int)_newLogCount]];
    }else{
        [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items] objectAtIndex:3] setBadgeValue:nil];
    }
    
    [self saveInUserDefault];
}

- (void)encodeWithCoder:(NSCoder *)coder;{
    [coder encodeObject:@(self.newLogCount) forKey:@"newLogCount"];
}

- (id)initWithCoder:(NSCoder *)coder;
{
    self = [[MyActivityLogModel alloc] init];
    if (self != nil)
    {
        self.newLogCount = [[coder decodeObjectForKey:@"newLogCount"] integerValue];
    }
    return self;
}
@end
