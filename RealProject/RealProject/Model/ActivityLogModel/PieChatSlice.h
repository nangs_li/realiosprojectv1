//
//  PieChatSlice.h
//  productionreal2
//
//  Created by Alex Hung on 8/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PieChatSlice : NSObject
@property (nonatomic,strong) NSString *title;
@property (nonatomic,assign) CGFloat value;
@property (nonatomic,strong) UIColor *color;

+ (PieChatSlice*)sliceWithTitle:(NSString*)title value:(CGFloat)value color:(UIColor*)color;
@end
