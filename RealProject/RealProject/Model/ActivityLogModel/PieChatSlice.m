//
//  PieChatSlice.m
//  productionreal2
//
//  Created by Alex Hung on 8/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "PieChatSlice.h"

@implementation PieChatSlice
+ (PieChatSlice*)sliceWithTitle:(NSString*)title value:(CGFloat)value color:(UIColor*)color{
    PieChatSlice *slice = [[PieChatSlice alloc]init];
    slice.title = title;
    slice.value = value;
    slice.color = color;
    return slice;
}
@end
