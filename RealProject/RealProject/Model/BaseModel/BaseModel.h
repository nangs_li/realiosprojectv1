//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"

typedef NS_ENUM(NSUInteger, RequestErrorStatus) {
    NotUseStatus= 0 ,
    NotNetWorkConnection ,
    error,
    RealServerNotReponse,
    AccessErrorOtherUserLoginThisAccount,
    NotRecordFind,
    GoogleServerNotReponse,
    GoogleReturnEmptyResult,
};

typedef void (^successBlock) (id response);
/**
 *  failureBlock
 *
 *  @param operation    AFHTTPRequestOperation
 *  @param error        Server Return Error Code and errorMessage
 *  @param errorMessage Server Return errorMessage
 *  @param errorStatus  RequestErrorStatus
 *  @param alert        String Alert
 *  @param ok           String Alert
 */
typedef void (^failureBlock) (AFHTTPRequestOperation *operation,NSError *error,NSString *errorMessage,RequestErrorStatus errorStatus,NSString *alert,NSString *ok);


@interface BaseModel : NSObject
#pragma mark check empty
- (BOOL)isEmpty:(id)thing;
#pragma mark AES encrypt
- (NSString *)stringToAESEncrypt:(NSString *)string;
- (NSString *)stringToAESDecrypt:(NSString *)string ;
#pragma mark   CallServer chec,Error-------------------------------------------------------------------------------------
- (BOOL)checkAccessError:(NSString *)serverReturnString ;
- (BOOL)networkConnection ;
- (NSError*)genError:(int)errorCode description:(NSString*)description;

- (DBQBChatMessage *)fillDBQBChatMessageNullValue:(DBQBChatMessage*)dbqbchatmessage;
- (QBChatMessage *)fillQBChatMessageNullValue:(QBChatMessage*)qbchatmessage;
-(NSString *)stringByReplacingSpaceAndByAddingPercentEscapesUsingEncoding:(NSString *)urlString;
- (NSString *)nsDictionaryArrayToJsonString:(NSMutableArray *)array;
- (AFHTTPRequestOperationManager *)getAPiManager;
- (void)checkLoginWithBlock:(CommonBlock)block;
- (void)checkNotNetwork:(CommonBlock)block;
@end
