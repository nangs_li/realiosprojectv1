//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "BaseModel.h"
#import "StringEncryption.h"
#import "NSData+Base64.h"
#import "Reachability.h"
@interface BaseModel ()
@end

@implementation BaseModel

#pragma mark check empty
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}
#pragma mark AES encrypt
- (NSString *)stringToAESEncrypt:(NSString *)string {
    NSData *encryptedData = [[StringEncryption alloc]
                             encrypt:[string dataUsingEncoding:NSUTF8StringEncoding]
                             key:@"03ac674216f3e15c761ee1a5e255f067"
                             iv:@"mS++sv+0xEL0YwM="];
    
    return [encryptedData base64EncodingWithLineLength:0];
}
//// AES decrypt
- (NSString *)stringToAESDecrypt:(NSString *)string {
    NSData *encryptedData = [[StringEncryption alloc]
                             decrypt:[[NSData alloc] initWithBase64EncodedString:string options:0]
                             key:@"03ac674216f3e15c761ee1a5e255f067"
                             iv:@"mS++sv+0xEL0YwM="];
    
    return [[NSString alloc] initWithData:encryptedData
                                 encoding:NSUTF8StringEncoding];
}
- (BOOL)networkConnection {
    Reachability *networkReachability =
    [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        /*
         UIAlertView *alert =
         [[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
         message:@"Not Network,Please Turn On Wifi"
         delegate:nil
         cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
         otherButtonTitles:nil];
         
         [alert show];
         */
        DDLogInfo(@"There IS NO internet connection");
        return NO;
    } else {
        DDLogInfo(@"There IS internet connection");
        return YES;
    }
}

- (BOOL)checkAccessError:(NSString *)serverReturnString {
    if ([serverReturnString rangeOfString:@"\"ErrorCode\":4,"].location != NSNotFound) {
        return YES;
    }
    return NO;
}

-(NSError*)genError:(int)errorCode description:(NSString*)description{
    NSError *error = nil;
    if (errorCode != 0) {
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
        if ([RealUtility isValid:description]) {
            [userInfo setObject:description forKey:@"description"];
        }
        error = [[NSError alloc]initWithDomain:@"com.real.network" code:errorCode userInfo:userInfo];
    }
    return error;
}
-(DBQBChatMessage *)fillDBQBChatMessageNullValue:(DBQBChatMessage*)dbqbchatmessage{
    
    
    if ([self isEmpty:dbqbchatmessage.Text]) {
        dbqbchatmessage.Text = @"";
    }
    if ([self isEmpty:dbqbchatmessage.DateSent]) {
        dbqbchatmessage.DateSent=[NSDate date];
    }
    return dbqbchatmessage;
}
-(QBChatMessage *)fillQBChatMessageNullValue:(QBChatMessage*)qbchatmessage{
    
    
    if ([self isEmpty:qbchatmessage.text]) {
        qbchatmessage.text = @"";
    }
    if ([self isEmpty:qbchatmessage.dateSent]) {
        qbchatmessage.dateSent=[NSDate date];
    }
    return qbchatmessage;
}
-(NSString *)stringByReplacingSpaceAndByAddingPercentEscapesUsingEncoding:(NSString *)urlString{
    urlString= [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return urlString;
}

//// nsDictionaryArrayToInputParametersString
- (NSString *)nsDictionaryArrayToJsonString:(NSMutableArray *)array {
    NSString *ArrayjsonString;
    NSError *error;
    NSData *jsonData =
    [NSJSONSerialization dataWithJSONObject:array
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];
    
    if (!jsonData) {
        DDLogInfo(@"Got an error: %@", error);
    } else {
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        ArrayjsonString =
        [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    
    return ArrayjsonString;
}
-(AFHTTPRequestOperationManager *)getAPiManager{
    AFHTTPRequestOperationManager * manager= [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setTimeoutInterval:12];
    return manager;
}

- (void)checkLoginWithBlock:(CommonBlock)block {
    
    if ([LoginBySocialNetworkModel shared].myLoginInfo.MemberID==0) {
        
        NSError *error = [NSError errorWithDomain:@"com.real.notLogin" code:-1012 userInfo:nil];
        block(nil,error);
        
    }
    
}

- (void)checkNotNetwork:(CommonBlock)block {
    
    if (![self networkConnection]) {
        
        NSError *error = [NSError errorWithDomain:JMOLocalizedString(NotNetWorkConnectionText, nil) code:NotNetWorkConnection userInfo:nil];
        block(nil,error);

    }
    
}
@end