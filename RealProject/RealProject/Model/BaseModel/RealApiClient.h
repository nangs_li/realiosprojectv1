//
//  RealApiClient.h
//  productionreal2
//
//  Created by Alex Hung on 18/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

// Model
#import "GoogleTranslateModel.h"
#import "PhoneNumberRegModel.h"

typedef void (^DetectLanguageSuccessBlock) (GoogleTranslateModel *response);

@interface RealApiClient : BaseModel

@property (nonatomic,assign) BOOL needForceUpdate;
@property (nonatomic,strong) NSString *updateURL;
@property (nonatomic,strong) NSMutableArray *matchList;
@property (nonatomic,assign) NSInteger contactListPage;

+ (id)sharedClient;

- (void)addViewdListing:(NSNumber*)viewedMemberId;
- (void)activityLogGetWithPage:(NSInteger)page WithBlock:(CommonBlock)block;
- (void)cachedActivityLogGetWithPage:(NSInteger)page withBlock:(CommonBlock)block;
- (void)cachedActivityLogGetWithOffset:(int)offset limit:(int)limit withBlock:(CommonBlock)block;
- (void)checkVersion;
- (void)followInfoGetWithBlock:(CommonBlock)block;
- (void)followingListGetWithPage:(NSInteger)page block:(CommonBlock)block;
- (void)cachedFollowerLogGetWithPage:(NSInteger)page withBlock:(CommonBlock)block;
- (void)cachedFollowerLogGetWithOffset:(int)offset limit:(int)limit withBlock:(CommonBlock)block;
- (void)viralLogWriteWithType:(NSInteger)vType inviteBy:(NSInteger)inviteBy block:(CommonBlock)block;
- (void)actionLogTypeWithChatMessage:(NSString*)message memberID:(NSInteger)memberID;
- (void)detectLanguageWithString:(NSString *)inputString success:(DetectLanguageSuccessBlock)success failure:(CommonBlock)failure;
- (void)applyQBAccountWithCompletion:(CommonBlock)block;
- (AFHTTPRequestOperationManager *)getAPiManager;
- (void)getAgentProfileFromContactListWithBlock:(CommonBlock)block;
- (void)uploadContactListWithBlock:(CommonBlock)block;
- (void)uploadInviteList:(NSData *)data withBlock:(CommonBlock)block;
- (void)registerphoneNumberFromCountryCode:(NSString *)countryCode phoneNumber:(NSString *)phoneNumber block:(CommonBlock)block;
- (void)verifyPhoneNumberSMSFromPhoneRegID:(NSString *)phoneRegID pinInput:(NSString *)pinInput block:(CommonBlock)block;
- (void)changePhoneNumberWithDict:(NSDictionary *)dict completion:(CommonBlock)completion;
- (void)getCountryCodeBlock:(CommonBlock)block;
- (void)migratePhoneNumberFromCountryCode:(NSString *)countryCode phoneNumber:(NSString *)phoneNumber block:(CommonBlock)block;
- (void)migratePhoneForPhoneNumberSMSVerifyFromPhoneRegID:(NSString *)phoneRegID pinInput:(NSString *)pinInput block:(CommonBlock)block;
- (void)verifyPhoneNumberSMSForChangingPhoneFromPhoneRegID:(NSString *)phoneRegID pinInput:(NSString *)pinInput block:(CommonBlock)block;
@end

