//
//  RealApiClient.m
//  productionreal2
//
//  Created by Alex Hung on 18/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealApiClient.h"
#import "LoginBySocialNetworkModel.h"
#import "NSDictionary+Utility.h"
#import "Reachability.h"
#import "ActivityLogGetModel.h"
#import "DBActivityLog.h"
#import "DBFollowerLog.h"
#import "FollowerListGetModel.h"
#import "FollowerModel.h"
#import "GoogleTranslateModel.h"
#import "DBSearchAddressArray.h"
// Mixpanel
#import "Mixpanel.h"
#import "AgentListingModel.h"
#import "GetContryLocation.h"
#import "REPhoneBookManager.h"
#import "XMLReader.h"
#import "MatchList.h"
#import "SystemSettingModel.h"
#import "SMSCountryCodeList.h"

@implementation RealApiClient
+ (id)sharedClient {
    static RealApiClient *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[self alloc] init];
    });
    return sharedClient;
}

- (BOOL)networkConnection {
    Reachability *networkReachability =
    [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        /*
         UIAlertView *alert =
         [[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
         message:@"Not Network,Please Turn On Wifi"
         delegate:nil
         cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
         otherButtonTitles:nil];
         
         [alert show];
         */
        DDLogInfo(@"There IS NO internet connection");
        return NO;
    } else {
        DDLogInfo(@"There IS internet connection");
        return YES;
    }
}
-(NSDictionary*)getCommonParameter{
    
    NSNumber *memberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    
    if (![memberID valid]) {
        
        memberID = [NSNumber numberWithInt:0];
    }
    
    return @{
             @"MemberID":memberID,
             @"UniqueKey":[@([[NSDate date] timeIntervalSince1970])stringValue],
             @"AccessToken":[LoginBySocialNetworkModel shared].myLoginInfo.AccessToken
             };
}

- (NSDictionary*)getUniqueKey {
    
    return @{@"UniqueKey":[@([[NSDate date] timeIntervalSince1970])stringValue]};
    
}

-(void)addViewdListing:(NSNumber *)viewedMemberId{
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/AddViewedListing"];
    NSDictionary * GooglePackDict=[[NSDictionary alloc] init];
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    DBResultSet * dbSearchAddressArrayResult = [[[[DBSearchAddressArray query]
                                                  whereWithFormat:@"MemberID               = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] limit:1]fetch];
    
    for (DBSearchAddressArray * dbSearchAddressArray in dbSearchAddressArrayResult) {
        if (![[AppDelegate getAppDelegate]isEmpty:dbSearchAddressArray.GoogleAddressPackDict]) {
            GooglePackDict = @{@"GoogleAddressPack":dbSearchAddressArray.GoogleAddressPackDict};
            [parameter addEntriesFromDictionary:GooglePackDict];
        }
        
    }
    
    
    [parameter setObject:viewedMemberId forKey:@"ViewedMemberID"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              
          }];
    
}

- (void)cachedActivityLogGetWithPage:(NSInteger)page withBlock:(CommonBlock)block{
    int offset = ((int)page-1)*20;
    int limit = 20;
    [self cachedActivityLogGetWithOffset:offset limit:limit withBlock:block];
}
- (void)cachedActivityLogGetWithOffset:(int)offset limit:(int)limit withBlock:(CommonBlock)block{
    DBQuery *activityLogQuery = [[DBActivityLog query] whereWithFormat:@"MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID ];
    DBQuery *pagingQuery =[[[activityLogQuery limit:limit]offset:offset]orderByDescending:@"ID"];
    DBResultSet* r = [pagingQuery fetch];
    int totalCount = [activityLogQuery count];
    if (block) {
        ActivityLogGetModel *getModel =[[ActivityLogGetModel alloc]init];
        getModel.TotalCount = totalCount;
        getModel.RecordCount = limit;
        getModel.Log = [[ActivityLogModel initWithDBActivityLogs:r] mutableCopy];
        block(getModel,nil);
    }
}

- (void)cachedFollowerLogGetWithPage:(NSInteger)page withBlock:(CommonBlock)block{
    int offset = ((int)page-1)*20;
    int limit = 20;
    [self cachedFollowerLogGetWithOffset:offset limit:limit withBlock:block];
}
- (void)cachedFollowerLogGetWithOffset:(int)offset limit:(int)limit withBlock:(CommonBlock)block{
    DBQuery *followerLogQuery = [[DBFollowerLog query] whereWithFormat:@"SenderID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID ];
    DBQuery *pagingQuery =[[[followerLogQuery limit:limit]offset:offset]orderByDescending:@"ID"];
    DBResultSet* r = [pagingQuery fetch];
    int totalCount = [followerLogQuery count];
    if (block) {
        FollowerListGetModel *getModel =[[FollowerListGetModel alloc]init];
        getModel.TotalCount = totalCount;
        getModel.RecordCount = limit;
        getModel.Followers = [[FollowerModel initWithDBFollowerLogs:r] mutableCopy];
        block(getModel,nil);
    }
}
- (void)activityLogGetWithPage:(NSInteger)page WithBlock:(CommonBlock)block{
    
    [self checkLoginWithBlock:block];
    
    [self checkNotNetwork:block];
    
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/ActivityLogGet"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    [parameter setObject:@(page) forKey:@"Page"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc]initwithstring:dstring];
              ActivityLogGetModel *logModel;
              NSError *error = nil;
              if (handleServerReturnString.ErrorCode == 0) {
                  logModel =[[ActivityLogGetModel alloc]initWithDictionary:handleServerReturnString.Content error:&error];
                  for (ActivityLogModel*model  in logModel.Log) {
                      if (model.LogType == ActivityLogTypeInviteToFollow) {
                          [[LoginBySocialNetworkModel shared].myLoginInfo didSuccessInviteViral];
                      }
                      DBActivityLog *dbLog = [DBActivityLog initWithActivityLogModel:model];
                      [dbLog commit];
                  }
              }else{
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = handleServerReturnString.ErrorMsg;
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
              }
              if (block) {
                  block(logModel,error);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  block(nil,error);
              }
          }];
}


- (void)checkVersion{
    
    if (self.needForceUpdate && self.updateURL) {
        [kAppDelegate showUpdateAlert:self.updateURL forceUpdate:YES];
    }else{
        NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress, @"/Operation.asmx/Version"];
        AFHTTPRequestOperationManager *manager = [self getAPiManager];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONReadingAllowFragments];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
        [parameter setObject:currentVersion forKey:@"MyVersion"];
        [parameter setObject:@(1) forKey:@"DeviceType"];
        [manager POST:urlLink
           parameters:@{@"InputJson":[parameter jsonPresentation]}
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  NSString *dstring = [responseObject objectForKey:@"d"];
                  HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc]initwithstring:dstring];
                  NSDictionary *dictionary = [NSDictionary fromJsonString:dstring];
                  NSDictionary *verDictionary = dictionary[@"Ver"];
                  NSString *latestVersion = verDictionary[@"iOSVer"];
                  NSString *updateURL = dictionary[@"URL"];
                  self.updateURL = updateURL;
                  BOOL forceUpdate = [verDictionary[@"ForceUpdate"] boolValue];
                  self.needForceUpdate = forceUpdate;
                  
                  NSDecimalNumber *latestVersionDecimal = [NSDecimalNumber decimalNumberWithString:latestVersion];
                  NSDecimalNumber *currentVersionDecimal = [NSDecimalNumber decimalNumberWithString:currentVersion];
                  if ([currentVersionDecimal compare:latestVersionDecimal] == NSOrderedAscending || forceUpdate) {
                      [kAppDelegate showUpdateAlert:updateURL forceUpdate:forceUpdate];
                  }
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
              }];
    }
}

- (void)followInfoGetWithBlock:(CommonBlock)block{
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/FollowInfoGet"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc]initwithstring:dstring];
              NSDictionary *dict = handleServerReturnString.Content;
              NSError *error = nil;
              if (handleServerReturnString.ErrorCode == 0) {
                  
              }else{
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = handleServerReturnString.ErrorMsg;
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
              }
              if (block) {
                  block(dict,error);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  block(nil,error);
              }
          }];
}

- (void)followingListGetWithPage:(NSInteger)page block:(CommonBlock)block{
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/FollowingListGet"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    [parameter setObject:@(page) forKey:@"Page"];
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc]initwithstring:dstring];
              FollowerListGetModel *getModel;
              NSError *error = nil;
              if (handleServerReturnString.ErrorCode == 0) {
                  getModel =[[FollowerListGetModel alloc]initWithDictionary:handleServerReturnString.Content error:&error];
                  for (FollowerModel*model  in getModel.Followers) {
                      DBFollowerLog *dbLog = [DBFollowerLog initWithFollowerModel:model];
                      [dbLog commit];
                  }
              }else{
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = handleServerReturnString.ErrorMsg;
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
              }
              if (block) {
                  block(getModel,error);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              if (block) {
                  block(nil,error);
              }
          }];
}

- (void)viralLogWriteWithType:(NSInteger)vType inviteBy:(NSInteger)inviteBy block:(CommonBlock)block {
    
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/ViralLogWrite"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    [parameter setObject:@(vType) forKey:@"vType"];
    [parameter setObject:@(inviteBy) forKey:@"InviteBy"];
    [parameter setObject:[LoginBySocialNetworkModel shared].myLoginInfo.MemberID forKey:@"InviteTo"];
    
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              NSError *error = nil;
              
              if (block) {
                  
                  block(responseObject,error);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              
              if (block) {
                  
                  block(nil,error);
              }
          }];
}

- (void)actionLogTypeWithChatMessage:(NSString*)message memberID:(NSInteger)memberID{
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/CSPortal.asmx/ActionLogWrite"];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:@(8) forKey:@"ActionType"];
    [parameter setObject:@(0) forKey:@"CaseID"];
    [parameter setObject:[LoginBySocialNetworkModel shared].myLoginInfo.MemberID forKey:@"ToMemberID"];
    [parameter setObject:@(memberID) forKey:@"MemberID"];
    if ([message valid]) {
        [parameter setObject:message forKey:@"ChatMessage"];
    }
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager = [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
          }];
}

- (void)detectLanguageWithString:(NSString *)inputString
                         success:(DetectLanguageSuccessBlock)success
                         failure:(CommonBlock)failure {
    
    NSString *urlLink = @"https://www.googleapis.com/language/translate/v2/detect";
    
    GoogleTranslateModel *model = [[GoogleTranslateModel alloc] init];
    model.key = kGoogleAPIKey;
    model.q = inputString;
    
    if ([[GetContryLocation shared]contryLocationISChina]) {
        [[AgentListingModel shared] callGoogleLanguageDetectionLangToDetect:inputString success:^(id responseObject) {
            if (success) {
                
                GoogleTranslateModel *response = [[GoogleTranslateModel alloc] initWithDictionary:responseObject error:nil];
                response.key = kGoogleAPIKey;
                response.q = inputString;
                success(response);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            if (failure) {
                
                failure(nil,error);
            }
        }];
    }else{
        
        AFHTTPRequestOperationManager *manager = [self getAPiManager];
        [manager.requestSerializer setTimeoutInterval:5];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        DDLogVerbose(@"apiurlstring-->:%@", urlLink);
        [manager GET:urlLink
          parameters:[model dictionaryWithValuesForKeys:[GoogleTranslateModel requestKeys]]
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                 if (success) {
                     
                     GoogleTranslateModel *response = [[GoogleTranslateModel alloc] initWithDictionary:responseObject error:nil];
                     
                     success(response);
                 }
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 
                 if (failure) {
                     
                     failure(nil,error);
                 }
             }];
    }
}

- (void)applyQBAccountWithCompletion:(CommonBlock)block{
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/MemberProfileQBApply"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    AFHTTPRequestOperationManager *manager = [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *jsonString = [responseObject objectForKey:@"d"];
              NSDictionary *jsonDict = [NSDictionary fromJsonString:jsonString];
              NSInteger errorCode = [jsonDict[@"ErrorCode"] integerValue];
              if (errorCode == 0) {
                  NSDictionary *content = jsonDict[@"Content"];
                  NSString *qbid = content[@"QBID"];
                  NSString *qbpw = content[@"QBPwd"];
                  [LoginBySocialNetworkModel shared].myLoginInfo.QBID = qbid;
                  [LoginBySocialNetworkModel shared].myLoginInfo.QBPwd = qbpw;
                  [[LoginBySocialNetworkModel shared] loginByQuickBloxOrSignUpQuickBloxWithInfo:[LoginBySocialNetworkModel shared].myLoginInfo Success:^(id response) {
                      block(response,nil);
                  } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                      block(nil,error);
                  }];
              }else{
                  block(nil,[NSError errorWithDomain:@"" code:errorCode userInfo:nil]);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              block(nil,error);
          }];
}
-(AFHTTPRequestOperationManager *)getAPiManager{
    AFHTTPRequestOperationManager * manager= [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setTimeoutInterval:12];
    return manager;
}

- (void)getAgentProfileFromContactListWithBlock:(CommonBlock)block{
    
    [self checkLoginWithBlock:block];
    
    [self checkNotNetwork:block];
    
    // init agentProfileArrayFromContactList and page.
    self.matchList = [[NSMutableArray alloc]init];
    self.contactListPage = 0;
    
    [self getAgentProfileFromContactList:self.contactListPage WithBlock:^(id response, NSError *error) {
        
        block(response,error);
        
    }];
    
}

- (void)getAgentProfileFromContactList:(NSInteger)page WithBlock:(CommonBlock)block{
    //add page count.
    page++;
    
    // make api call from url and parameter
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/MatchContactGet"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    [parameter setObject:@(page) forKey:@"Page"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              // change return json to object
              NSString *dstring = [responseObject objectForKey:@"d"];
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc]initwithstring:dstring];
              MatchList *matchList;
              NSError *error = nil;
              
              // check errorcode success status or failure status
              if (handleServerReturnString.ErrorCode == 0) {
                  
                  NSError *error;
                  matchList = [[MatchList alloc]initWithDictionary:handleServerReturnString.Content error:&error];
                  
                  if ([matchList.RecordCount intValue]!=0 && matchList.RecordCount!=nil) {
                      
                      [self.matchList addObjectsFromArray:matchList.MatchList];
                      __weak RealApiClient *weakSelf = self;
                      [weakSelf getAgentProfileFromContactList:page WithBlock:^(id response, NSError *error) {
                          
                          block(response,error);
                          
                      }];
                      
                  }else{
                      
                      block(self.matchList,nil);
                      
                  }
                  
              }else{
                  
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = handleServerReturnString.ErrorMsg;
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
                  
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  
                  block(nil,error);
                  
              }
              
          }];
}



- (void)uploadContactListWithBlock:(CommonBlock)block {    
    [self uploadContactListData:[REPhonebookManager sharedManager].contactsData fileType:@"5" block:block];
}

- (void)uploadInviteList:(NSData *)data withBlock:(CommonBlock)block {
    [self uploadContactListData:data fileType:@"6" block:block];
}

- (void)uploadContactListData:(NSData *)data fileType:(NSString *)fileType block:(CommonBlock)block {
    
    NSDictionary *parameters = @{
                                 @"MemberID" : [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                                 @"AccessToken" : [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                                 @"ListingID" : @"0",
                                 @"FileType" : fileType,
                                 @"FileExt" : @"txt"
                                 };
    
    
    NSString *string = [NSString stringWithFormat:@"%@%@", kServerAddress,
                        @"/Admin.asmx/FileUpload"];
    //    NSString *txtFilePath = [REPhonebookManager sharedManager].txtFilePath;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
                                    multipartFormRequestWithMethod:@"POST"
                                    URLString:string
                                    parameters:parameters
                                    constructingBodyWithBlock:^(id<AFMultipartFormData>
                                                                formData) {
                                        
                                        [formData appendPartWithFileData:data
                                                                    name:@"ContactList"
                                                                fileName:@"ContactList.txt"
                                                                mimeType:@"text/plain"];
                                    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc]
                                    initWithSessionConfiguration:
                                    [NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    DDLogDebug(@"parameters-->%@",parameters);
    
    NSProgress *progress = nil;
    //    [kAppDelegate updateCurrentHudStatus:@"Uploading Photo"];
    NSURLSessionUploadTask *uploadTask = [manager
                                          uploadTaskWithStreamedRequest:request
                                          progress:&progress
                                          completionHandler:^(NSURLResponse *response,
                                                              id responseObject,
                                                              NSError *error) {
                                              
                                              NSDictionary *jsonResponse = nil;
                                              
                                              if (!error && responseObject) {
                                                  
                                                  NSString *res = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                  NSError *parseError = nil;
                                                  NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:res error:&parseError];
                                                  
                                                  if (!parseError && xmlDictionary) {
                                                      
                                                      NSDictionary *rawDict = xmlDictionary[@"string"];
                                                      
                                                      if(rawDict){
                                                          
                                                          NSString *content = rawDict[@"text"];
                                                          NSString *decoded = [content stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                          NSData *data = [decoded dataUsingEncoding:NSUTF8StringEncoding];
                                                          jsonResponse= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                          
                                                          int errorCode = [jsonResponse[@"ErrorCode"]intValue ];
                                                          
                                                          if (errorCode != 0) {
                                                              
                                                              error =[[NSError alloc]init];
                                                              
                                                          }
                                                          jsonResponse = jsonResponse[@"Content"];
                                                          
                                                      }
                                                      
                                                  }
                                                  
                                              }
                                              
                                              if (block) {
                                                  
                                                  block(jsonResponse,error);
                                                  
                                              }
                                              
                                          }];
    
    [uploadTask resume];
    
}

- (void)registerphoneNumberFromCountryCode:(NSString *)countryCode phoneNumber:(NSString *)phoneNumber block:(CommonBlock)block {
    
    [self checkNotNetwork:block];
    
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/PhoneNumberRegEX"];
    
    NSMutableDictionary *parameter = [[self getUniqueKey]mutableCopy];
    [parameter setObject:countryCode forKey:@"CountryCode"];
    [parameter setObject:phoneNumber forKey:@"PhoneNumber"];
    [parameter setObject:[LanguagesManager sharedInstance].currentLanguage forKey:@"Lang"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              NSError *error = nil;
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&error];
              PhoneNumberRegModel *phoneNumberRegModel = [[PhoneNumberRegModel alloc] initWithDictionary:handleServerReturnString.Content error:&error];
              
              if (handleServerReturnString.ErrorCode == 0) {
                  
              }else{
                  
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = [handleServerReturnString.ErrorMsg copy];
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
                  phoneNumberRegModel = nil;
                  
              }
              
              if (block) {
                  
                  block(phoneNumberRegModel,error);
                  
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  
                  block(nil,error);
              }
              
          }];
    
}

- (void)verifyPhoneNumberSMSFromPhoneRegID:(NSString *)phoneRegID pinInput:(NSString *)pinInput block:(CommonBlock)block {
    
    [self checkNotNetwork:block];
    
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/PhoneNumberSMSVerifyEX"];
    
    NSMutableDictionary *parameter = [[self getUniqueKey]mutableCopy];
    [parameter setObject:phoneRegID forKey:@"PhoneRegID"];
    [parameter setObject:pinInput forKey:@"PINInput"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              
              NSError *error = nil;
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&error];
              NSString * pinInput;
              if (handleServerReturnString.ErrorCode == 0) {
                  
                  pinInput = pinInput;
                  
              }else{
                  
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = [handleServerReturnString.ErrorMsg copy];
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
                  handleServerReturnString = nil;
                  pinInput = nil;
              }
              
              if (block) {
                  
                  block(pinInput,error);
                  
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  
                  block(nil,error);
              }
              
          }];
}

- (void)changePhoneNumberWithDict:(NSDictionary *)dict completion:(CommonBlock)completion {
    NSString *url = [NSString stringWithFormat:@"%@%@",kServerAddress,@"/Admin.asmx/PhoneNumberChange"];
    NSMutableDictionary *parameter = [[self getCommonParameter] mutableCopy];
    [parameter addEntriesFromDictionary:dict];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", url);
    
    [manager POST:url
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc]initwithstring:dstring];
              PhoneNumberRegModel *phoneNumberRegModel = nil;
              NSError *error = nil;
              if (handleServerReturnString.ErrorCode != 0) {
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = handleServerReturnString.ErrorMsg;
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
              } else {
                  HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&error];
                  phoneNumberRegModel = [[PhoneNumberRegModel alloc] initWithDictionary:handleServerReturnString.Content error:&error];
              }
              
              if (completion) {
                  completion(phoneNumberRegModel,error);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              if (completion) {
                  completion(nil,error);
              }
          }];

}

- (void)getCountryCodeBlock:(CommonBlock)block{
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/CountryCodeGet"];
    
    NSMutableDictionary *parameter = [[self getUniqueKey]mutableCopy];
    
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc]initwithstring:dstring];
              
              NSError *error = nil;
              if (handleServerReturnString.ErrorCode == 0) {
                  
                  [SystemSettingModel shared].SystemSettings = [[SystemSettings alloc]initWithDictionary:handleServerReturnString.Content error:&error];
                  
                  
              }else{
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = handleServerReturnString.ErrorMsg;
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
              }
              if (block) {
                  block([SystemSettingModel shared].SystemSettings,error);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              if (block) {
                  block(nil,error);
              }
          }];
}

- (void)migratePhoneNumberFromCountryCode:(NSString *)countryCode phoneNumber:(NSString *)phoneNumber block:(CommonBlock)block {
    
    [self checkNotNetwork:block];
    
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/PhoneNumberMigrate"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter]mutableCopy];
    [parameter setObject:countryCode forKey:@"CountryCode"];
    [parameter setObject:phoneNumber forKey:@"PhoneNumber"];
    [parameter setObject:[LanguagesManager sharedInstance].currentLanguage forKey:@"Lang"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              NSError *error = nil;
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&error];
              PhoneNumberRegModel *phoneNumberRegModel = [[PhoneNumberRegModel alloc] initWithDictionary:handleServerReturnString.Content error:&error];
              
              if (handleServerReturnString.ErrorCode == 0) {
                  
              }else{
                  
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = [handleServerReturnString.ErrorMsg copy];
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
                  phoneNumberRegModel = nil;
                  
              }
              
              if (block) {
                  
                  block(phoneNumberRegModel,error);
                  
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  
                  block(nil,error);
              }
              
          }];
    
}

- (void)migratePhoneForPhoneNumberSMSVerifyFromPhoneRegID:(NSString *)phoneRegID pinInput:(NSString *)pinInput block:(CommonBlock)block {
    
    [self checkNotNetwork:block];
    
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/PhoneNumberSMSVerifyForMigratingPhone"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter]mutableCopy];
    [parameter setObject:phoneRegID forKey:@"PhoneRegID"];
    [parameter setObject:pinInput forKey:@"PINInput"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              
              NSError *error = nil;
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&error];
              NSString * pinInput;
              if (handleServerReturnString.ErrorCode == 0) {
                  
                  pinInput = pinInput;
                  
              }else{
                  
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = [handleServerReturnString.ErrorMsg copy];
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
                  handleServerReturnString = nil;
                  pinInput = nil;
              }
              
              if (block) {
                  
                  block(pinInput,error);
                  
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  
                  block(nil,error);
              }
              
          }];
}

- (void)verifyPhoneNumberSMSForChangingPhoneFromPhoneRegID:(NSString *)phoneRegID pinInput:(NSString *)pinInput block:(CommonBlock)block {
    
    [self checkNotNetwork:block];
    
    NSString *urlLink = [NSString stringWithFormat:@"%@%@", kServerAddress,
                         @"/Admin.asmx/PhoneNumberSMSVerifyForChangingPhone"];
    
    NSMutableDictionary *parameter = [[self getCommonParameter]mutableCopy];
    [parameter setObject:phoneRegID forKey:@"PhoneRegID"];
    [parameter setObject:pinInput forKey:@"PINInput"];
    
    NSDictionary *inputJson = @{@"InputJson":[parameter jsonPresentation]};
    
    AFHTTPRequestOperationManager *manager =[self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", inputJson);
    [manager POST:urlLink
       parameters:inputJson
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              
              NSError *error = nil;
              HandleServerReturnString *handleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&error];
              NSString * pinInput;
              if (handleServerReturnString.ErrorCode == 0) {
                  
                  pinInput = pinInput;
                  
              }else{
                  
                  NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                  userInfo[NSLocalizedDescriptionKey] = [handleServerReturnString.ErrorMsg copy];
                  error =[NSError errorWithDomain:@"com.real.server" code:handleServerReturnString.ErrorCode userInfo:userInfo];
                  handleServerReturnString = nil;
                  pinInput = nil;
              }
              
              if (block) {
                  
                  block(pinInput,error);
                  
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if (block) {
                  
                  block(nil,error);
              }
              
          }];
    
}

@end
