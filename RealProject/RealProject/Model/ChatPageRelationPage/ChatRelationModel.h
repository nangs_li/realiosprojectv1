//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AFHTTPRequestOperation.h"
#import "HandleServerReturnString.h"
#import "ChatRelationModel.h"
#import "ChatRoomRelationObject.h"
#import "ChatRoomRelationArrayHaveTotalCount.h"
@interface ChatRelationModel : BaseModel
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(strong, nonatomic)
    NSMutableArray<ChatRoomRelationObject> *chatRoomRelationArray;
@property(strong, nonatomic)
    ChatRoomRelationArrayHaveTotalCount *chatRoomRelationArrayHaveTotalCount;
@property(strong, nonatomic)
    ChatRoomRelationObject *currentChatRoomRelationObject;
@property(strong, nonatomic) ChatRoomMember *mySelfChatRoomRelation;
@property(strong, nonatomic) ChatRoomMember *recipienerChatRoomRelation;
@property(assign, nonatomic) BOOL currentChatRoomRelationObjectIsEmpty;
#pragma mark Block List
@property(strong, nonatomic) QBPrivacyList *blockList;
@property(strong, nonatomic) NSNumber *recipientID;
+ (ChatRelationModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find

 */
- (void)chatRoomCreateToMemberID:(NSString *)ToMemberID
                        dialogID:(NSString *)dialogID
                         Success:(successBlock)success
                         failure:
                             (failureBlock)failure;

//- (void)chatRoomRelationArrayGetSuccess:
//            (void (^)(NSArray<ChatRoomRelationObject> *chatRoomRelationArray))
//                success failure:
//                (failureBlock)failure;
- (void)chatRoomGetRelationFromChatDialogIdArraysuccess:(void (^)(NSMutableArray* AgentProfiles))success
                                                failure:(failureBlock ) failure;
- (void)chatRoomGetRelationFromChatDialogIdArray:(NSMutableArray *)chatDialogIdArray numberofStartRecord:(int)numberofStartRecord  success:(void (^)(NSMutableArray<ChatRoomRelationObject>* chatRoomRelationArray))success
                                         failure:(failureBlock ) failure;
- (void)getCurrentChatRoomRelationObjectFromLocalForDialogId:
    (NSString *)dialogID;

- (void)chatRoomMuteFromDialogId:(NSString *)dialogID
                      muteStatus:(NSString *)muteStatus
                         Success:(successBlock)success
                         failure:
                             (failureBlock)failure;
- (void)chatRoomDeleteFromDialogId:(NSString *)dialogID
                      deleteStatus:(NSString *)deleteStatus
                           Success:(successBlock)success
                           failure:
                               (failureBlock)failure;
- (void)chatRoomBlockFromDialogId:(NSString *)dialogID
                      blockStatus:(NSString *)blockStatus
                          Success:(successBlock)success
                          failure:
                              (failureBlock)failure;
- (void)chatRoomExitFromDialogId:(NSString *)dialogID
                      exitStatus:(NSString *)exitStatus
                         Success:(successBlock)success
                         failure:
                             (failureBlock)failure;
- (void)markCreateChatRoomRecordToServerForDialogID:(NSString *)dialogID
                                recipienterMemberID:
                                    (NSString *)recipienterMemberID;
- (void)sendSystemMessageToRefreshChatRoomRelation;
- (void)sendSystemMessageToRefreshChatPrivacyRecipientID:(NSString *)recipientID;
//- (void)UnBlockForDialogID:(NSString *)dialogID;
//- (void)UnExitForDialogID:(NSString *)dialogID;
@end
