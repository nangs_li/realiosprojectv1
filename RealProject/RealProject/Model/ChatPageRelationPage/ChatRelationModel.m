//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ChatRelationModel.h"

#import "HandleServerReturnString.h"
#import "DBChatRoomRelationArray.h"
#import "NSDictionary+Utility.h"
#import "ChatDialogModel.h"
 #define chatRoomGetDialogNumberOfRecord 10
@implementation ChatRelationModel

static ChatRelationModel *sharedChatRelationModel;
+ (ChatRelationModel *)shared {
  @synchronized(self) {
    if (!sharedChatRelationModel) {
      sharedChatRelationModel = [[ChatRelationModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedChatRelationModel;
  }
}
#pragma mark chatRoomCreateToMemberID
- (void)chatRoomCreateToMemberID:(NSString *)ToMemberID
                        dialogID:(NSString *)dialogID
                         Success:(successBlock)success
                         failure:
                             (failureBlock)failure {
  NSString *urlLink =
      [NSString stringWithFormat:@"%@%@", kServerAddress,
                                 @"/Chat.asmx/ChatRoomCreate"];
  NSString *parameters = [NSString
      stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                       @"\"ToMemberID\":\"%@\",\"DialogID\":\"%@\"}",
                       [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                       [LoginBySocialNetworkModel shared]
                           .myLoginInfo.AccessToken,
                       ToMemberID, dialogID, nil];

  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotNetWorkConnection, JMOLocalizedString(@"alert_alert", nil),
            JMOLocalizedString(@"alert_ok", nil));
      return;
  }

  DDLogVerbose(@"apiurlstring-->:%@", urlLink);

  [manager POST:urlLink
      parameters:@{
        @"InputJson" : parameters
      }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
        failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
      }];
}
#pragma mark ChatRoomGet
- (void)chatRoomGetRelationFromChatDialogIdArraysuccess:(void (^)(NSMutableArray* AgentProfiles))success
                           failure:(failureBlock ) failure{
    
    if ([LoginBySocialNetworkModel shared].myLoginInfo.MemberID ==0 ) {
        return;
    }
    NSMutableArray *chatDialogIdArray = [[NSMutableArray alloc] init];
    for (QBChatDialog *chatDialog in [ChatDialogModel shared].dialogs) {
        NSString *dialogID = chatDialog.ID ;
        [chatDialogIdArray
         addObject:[NSString stringWithFormat:@"%@", dialogID]];
    }
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    

    self.chatRoomRelationArray =[[NSMutableArray alloc]init];
    
    [self chatRoomGetRelationFromChatDialogIdArray:chatDialogIdArray numberofStartRecord:0  success:success failure:failure];
    
}
- (void)chatRoomGetRelationFromChatDialogIdArray:(NSMutableArray *)chatDialogIdArray numberofStartRecord:(int)numberofStartRecord  success:(void (^)(NSMutableArray<ChatRoomRelationObject>* chatRoomRelationArray))success
                                          failure:(failureBlock ) failure{
    if (numberofStartRecord>=chatDialogIdArray.count) {
        
        DDLogDebug(@"self.chatDialogIdArray.count %lu",chatDialogIdArray.count);
        
        if (success) {
            
            success(self.chatRoomRelationArray);
        }
        return;
    }
    int numberOfRecord=chatRoomGetDialogNumberOfRecord;
    
    if (numberofStartRecord+chatRoomGetDialogNumberOfRecord > chatDialogIdArray.count) {
        
        numberOfRecord=(int)chatDialogIdArray.count - numberofStartRecord;
    }
    
    NSArray *  selfChatlistQBidarray =[chatDialogIdArray subarrayWithRange:NSMakeRange(numberofStartRecord, numberOfRecord)];
    DDLogDebug(@"selfChatlistQBidarray.count%lu",(unsigned long)selfChatlistQBidarray.count);
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"DialogIDList\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            [self nsDictionaryArrayToJsonString:[selfChatlistQBidarray mutableCopy]],
                            nil];
    
    NSString *urlLink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress, @"/Chat.asmx/ChatRoomGet"];
    __weak typeof(self) weakSelf = self;
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"TestInputJson-->%@", parameters);
    
    
    
    [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,
                          JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                          AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
              }
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring
                                                         error:&err];
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  NSError *err = nil;
                  self.HandleServerReturnString =
                  [[HandleServerReturnString alloc] initWithString:dstring
                                                             error:&err];
                  
                  self.chatRoomRelationArrayHaveTotalCount =
                  [[ChatRoomRelationArrayHaveTotalCount alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  
                  [self.chatRoomRelationArray addObjectsFromArray:self.chatRoomRelationArrayHaveTotalCount.ChatRoom];
                  [[[[DBChatRoomRelationArray query]
                     whereWithFormat:
                     @"MemberID = %@",
                     [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
                      .myLoginInfo.MemberID]]
                    fetch] removeAll];
                  [DBChatRoomRelationArray createSearchAgentHistoryWithChatRoomRelationArray:self.chatRoomRelationArray];
                 
                  
                  [ weakSelf chatRoomGetRelationFromChatDialogIdArray:chatDialogIdArray numberofStartRecord:numberofStartRecord+chatRoomGetDialogNumberOfRecord success:success failure:failure];
                  
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));          }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}


#pragma mark getCurrentChatRoomRelationObjectFromLocalForDialogId

- (void)getCurrentChatRoomRelationObjectFromLocalForDialogId:
    (NSString *)dialogID {
    self.currentChatRoomRelationObjectIsEmpty = YES;
    for (ChatRoomRelationObject *chatRoomRelationObject in self
           .chatRoomRelationArray) {
    if ([chatRoomRelationObject.DialogID isEqualToString:dialogID]) {
      self.currentChatRoomRelationObject = chatRoomRelationObject;
      self.currentChatRoomRelationObjectIsEmpty = NO;

      for (ChatRoomMember *chatRoomMember in self.currentChatRoomRelationObject
               .ChatRoomMember) {
        if ([chatRoomMember.QBID
                isEqualToString:[LoginBySocialNetworkModel shared]
                                    .myLoginInfo.QBID]) {
          self.mySelfChatRoomRelation = chatRoomMember;

        } else {
          self.recipienerChatRoomRelation = chatRoomMember;
        }
      }
      self.currentChatRoomRelationObjectIsEmpty = NO;
     
       
    }
  }
   
    if (self.currentChatRoomRelationObjectIsEmpty) {
        self.recipienerChatRoomRelation=nil;
    }
  
}
#pragma mark Mute,Delete ,Block,Exit
- (void)chatRoomMuteFromDialogId:(NSString *)dialogID
                      muteStatus:(NSString *)muteStatus
                         Success:(successBlock)success
                         failure:
                             (failureBlock)failure {
  NSString *urlLink = [NSString
      stringWithFormat:@"%@%@", kServerAddress, @"/Chat.asmx/ChatRoomMute"];
  NSString *parameters = [NSString
      stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                       @"\"MuteStatus\":\"%@\",\"DialogID\":\"%@\"}",
                       [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                       [LoginBySocialNetworkModel shared]
                           .myLoginInfo.AccessToken,
                       muteStatus, dialogID, nil];

  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotNetWorkConnection, JMOLocalizedString(@"alert_alert", nil),
            JMOLocalizedString(@"alert_ok", nil));
      return;
  }

  DDLogVerbose(@"apiurlstring-->:%@", urlLink);
  DDLogVerbose(@"parameters-->:%@", parameters);
  [manager POST:urlLink
      parameters:@{
        @"InputJson" : parameters
      }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *dstring = [responseObject objectForKey:@"d"];
        if ([self checkAccessError:dstring]) {
          failure(operation, nil,
                  JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                  AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        }
        NSError *err = nil;
        self.HandleServerReturnString =
            [[HandleServerReturnString alloc] initWithString:dstring
                                                       error:&err];
        int error = self.HandleServerReturnString.ErrorCode;

        if (error == 0) {
          success(responseObject);
        }else{
            
            failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                    JMOLocalizedString(@"alert_ok", nil));
        }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
        failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
      }];
}
- (void)
chatRoomDeleteFromDialogId:(NSString *)dialogID
              deleteStatus:(NSString *)deleteStatus
                   Success:(successBlock)success
                   failure:(failureBlock)failure {
  NSString *urlLink =
      [NSString stringWithFormat:@"%@%@", kServerAddress,
                                 @"/Chat.asmx/ChatRoomDelete"];
  NSString *parameters = [NSString
      stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                       @"\"DeleteStatus\":\"%@\",\"DialogID\":\"%@\"}",
                       [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                       [LoginBySocialNetworkModel shared]
                           .myLoginInfo.AccessToken,
                       deleteStatus, dialogID, nil];

  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotNetWorkConnection, JMOLocalizedString(@"alert_alert", nil),
            JMOLocalizedString(@"alert_ok", nil));
      return;
  }

  DDLogVerbose(@"apiurlstring-->:%@", urlLink);
  DDLogVerbose(@"parameters-->:%@", parameters);
  [manager POST:urlLink
      parameters:@{
        @"InputJson" : parameters
      }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *dstring = [responseObject objectForKey:@"d"];
        if ([self checkAccessError:dstring]) {
          failure(operation, nil,
                  JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                  AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        }
        NSError *err = nil;
        self.HandleServerReturnString =
            [[HandleServerReturnString alloc] initWithString:dstring
                                                       error:&err];
        int error = self.HandleServerReturnString.ErrorCode;
        if (error == 0) {
          success(responseObject);
        }else{
            
            failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                    JMOLocalizedString(@"alert_ok", nil));
        }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
        failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
      }];
}
- (void)chatRoomBlockFromDialogId:(NSString *)dialogID
                      blockStatus:(NSString *)blockStatus
                          Success:(successBlock)success
                          failure:
                              (failureBlock)failure {
  NSString *urlLink =
      [NSString stringWithFormat:@"%@%@", kServerAddress,
                                 @"/Chat.asmx/ChatRoomBlock"];
  NSString *parameters = [NSString
      stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                       @"\"BlockStatus\":\"%@\",\"DialogID\":\"%@\"}",
                       [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                       [LoginBySocialNetworkModel shared]
                           .myLoginInfo.AccessToken,
                       blockStatus, dialogID, nil];

  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotNetWorkConnection, JMOLocalizedString(@"alert_alert", nil),
            JMOLocalizedString(@"alert_ok", nil));
      return;
  }

  DDLogVerbose(@"apiurlstring-->:%@", urlLink);
  DDLogVerbose(@"parameters-->:%@", parameters);
  [manager POST:urlLink
      parameters:@{
        @"InputJson" : parameters
      }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *dstring = [responseObject objectForKey:@"d"];
        if ([self checkAccessError:dstring]) {
          failure(operation, nil,
                  JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                  AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        }
        NSError *err = nil;
        self.HandleServerReturnString =
            [[HandleServerReturnString alloc] initWithString:dstring
                                                       error:&err];
        int error = self.HandleServerReturnString.ErrorCode;
        if (error == 0) {
          success(responseObject);
        }else{
            
            failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                    JMOLocalizedString(@"alert_ok", nil));
        }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
        failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
      }];
}
- (void)chatRoomExitFromDialogId:(NSString *)dialogID
                      exitStatus:(NSString *)exitStatus
                         Success:(successBlock)success
                         failure:
                             (failureBlock)failure {
  NSString *urlLink = [NSString
      stringWithFormat:@"%@%@", kServerAddress, @"/Chat.asmx/ChatRoomExit"];
  NSString *parameters = [NSString
      stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                       @"\"ExitStatus\":\"%@\",\"DialogID\":\"%@\"}",
                       [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                       [LoginBySocialNetworkModel shared]
                           .myLoginInfo.AccessToken,
                       exitStatus, dialogID, nil];

  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotNetWorkConnection, JMOLocalizedString(@"alert_alert", nil),
            JMOLocalizedString(@"alert_ok", nil));
      return;
  }

  DDLogVerbose(@"apiurlstring-->:%@", urlLink);
  DDLogVerbose(@"parameters-->:%@", parameters);
  [manager POST:urlLink
      parameters:@{
        @"InputJson" : parameters
      }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *dstring = [responseObject objectForKey:@"d"];
        if ([self checkAccessError:dstring]) {
          failure(operation, nil,
                  JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),
                  AccessErrorOtherUserLoginThisAccount, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        }
        NSError *err = nil;
        self.HandleServerReturnString =
            [[HandleServerReturnString alloc] initWithString:dstring
                                                       error:&err];
        int error = self.HandleServerReturnString.ErrorCode;
        if (error == 0) {
          success(responseObject);
        }else{
            
            failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                    JMOLocalizedString(@"alert_ok", nil));
        }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
        failure(operation, error, JMOLocalizedString(@"error_message__server_not_response", nil),
                RealServerNotReponse, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
      }];
}

- (void)markCreateChatRoomRecordToServerForDialogID:(NSString *)dialogID
                                recipienterMemberID:
                                    (NSString *)recipienterMemberID {
  [[ChatRelationModel shared] chatRoomCreateToMemberID:recipienterMemberID
      dialogID:dialogID
      Success:^(NSArray<ChatRoomRelationObject> *chatRoomRelationArray) {
        [[ChatDialogModel shared]requestDialogUpdateWithId:dialogID completionBlock:^{
            [self chatRoomRelationArrayGetForDialogID:dialogID];
        }];
          //  [self sendSystemMessageToRefreshChatRoomRelation];
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus,
                NSString *alert, NSString *ok){

      }];
}

- (void)chatRoomRelationArrayGetForDialogID:(NSString *)dialogID {
  [[ChatRelationModel shared] chatRoomGetRelationFromChatDialogIdArraysuccess:^(NSArray<
      ChatRoomRelationObject> *chatRoomRelationArray) {

    [[ChatRelationModel shared]
        getCurrentChatRoomRelationObjectFromLocalForDialogId:dialogID];
       [self sendSystemMessageToRefreshChatRoomRelation];

  } failure:^(AFHTTPRequestOperation *operation, NSError *error,
              NSString *errorMessage, RequestErrorStatus errorStatus,
              NSString *alert, NSString *ok){

  }];
}
- (void)sendSystemMessageToRefreshChatRoomRelation{
    QBChatMessage *message = [QBChatMessage message];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"isChatSetting"] = @"1";
    params[@"type"] = kSystemMessageTypeRefreshChatSetting;
    [message setCustomParameters:params];
    if ([self.recipientID valid])
        [message setRecipientID:[self.recipientID integerValue]];
    
    [[QBChat instance] sendSystemMessage:message completion:^(NSError * _Nullable error) {
        
    }];
}
- (void)sendSystemMessageToRefreshChatPrivacyRecipientID:(NSString *)recipientID{
    QBChatMessage *message = [QBChatMessage message];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"isChatSetting"] = @"1";
    params[@"type"] = kSystemMessageTypeRefreshChatPrivacySetting;
    [message setCustomParameters:params];
    if ([recipientID valid])
        [message setRecipientID:[recipientID integerValue]];
    
    [[QBChat instance] sendSystemMessage:message completion:^(NSError * _Nullable error) {
        
    }];
}
 /*
- (void)UnBlockForDialogID:(NSString *)dialogID {
 
    NSMutableArray *newBlockList = [[NSMutableArray alloc] init];
  for (QBPrivacyItem *qbPrivacyItem in [ChatRelationModel shared]
           .blockList.items) {
    if ([[ChatRelationModel shared]
                .recipienerChatRoomRelation.QBID
            isEqualToString:[NSString
                                stringWithFormat:@"%li",
                                                 (unsigned long)qbPrivacyItem.valueForType]]) {
    } else {
      [newBlockList addObject:qbPrivacyItem];
    }
  }
  if (newBlockList.count == 0) {
    QBPrivacyItem *item = [[QBPrivacyItem alloc] initWithType:USER_ID
                                                 valueForType:12345678
                                                       action:ALLOW];
    [newBlockList addObject:item];
  }

  [ChatRelationModel shared].blockList.items = newBlockList;
    
  [[QBChat instance] setPrivacyList:[ChatRelationModel shared].blockList];
  [[QBChat instance] setDefaultPrivacyListWithName:@"BlockList"];
  [[QBChat instance] setActivePrivacyListWithName:@"BlockList"];
  [[QBChat instance] retrievePrivacyListWithName:@"BlockList"];
  
  [[ChatRelationModel shared] chatRoomBlockFromDialogId:dialogID
      blockStatus:@"0"
      Success:^(id responseObject) {
        [[ChatRelationModel shared]
            chatRoomRelationArrayGetForDialogID:dialogID];
           [self sendSystemMessageToRefreshChatRoomRelation];
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus,
                NSString *alert, NSString *ok) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
      }];
}

- (void)UnExitForDialogID:(NSString *)dialogID {
  [[ChatRelationModel shared] chatRoomExitFromDialogId:dialogID
      exitStatus:@"0"
      Success:^(id responseObject) {
        [[ChatRelationModel shared]
            chatRoomRelationArrayGetForDialogID:dialogID];
          [self sendSystemMessageToRefreshChatRoomRelation];
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus,
                NSString *alert, NSString *ok) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
      }];
}
    */
@end