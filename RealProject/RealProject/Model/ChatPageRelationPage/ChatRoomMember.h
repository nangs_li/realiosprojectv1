//
//  AgentExperience.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentExperience_h
//#define abc_AgentExperience_h

//#endif
#import "JSONModel.h"
@protocol ChatRoomMember
@end

@interface ChatRoomMember : JSONModel
@property(strong, nonatomic) NSString* MemberID;
@property(strong, nonatomic) NSString* QBID;
@property(strong, nonatomic) NSString* Name;
@property(strong, nonatomic) NSString* PhotoURL;
@property(assign, nonatomic) BOOL Blocked;
@property(assign, nonatomic) BOOL Muted;
@property(assign, nonatomic) BOOL Deleted;
@property(assign, nonatomic) BOOL Exited;



@end

//@implementation AgentExperience

//@end