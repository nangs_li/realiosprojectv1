//
//  AgentExperience.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatRoomMember.h"
@implementation ChatRoomMember
static ChatRoomMember *chatRoomMember;
+ (BOOL)propertyIsOptional:(NSString*)propertyName {
  return YES;
}


- (ChatRoomMember *)initwithjsonstring:(NSString *)content:(int)error {
    NSError *err = nil;
    chatRoomMember = [[ChatRoomMember alloc] initWithString:content error:&err];
    return chatRoomMember;
}
- (ChatRoomMember *)initwithNSDictionary:(NSDictionary *)content:(int)error {
    NSError *err = nil;
    chatRoomMember = [[ChatRoomMember alloc] initWithDictionary:content error:&err];
    return chatRoomMember;
}

@end