//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import "JSONModel.h"
#import "ChatRoomRelationObject.h"
@interface ChatRoomRelationArrayHaveTotalCount : JSONModel
@property(strong, nonatomic) NSNumber* TotalCount;
@property(strong, nonatomic) NSNumber* RecordCount;
@property(strong, nonatomic) NSMutableArray<ChatRoomRelationObject>* ChatRoom;

- (ChatRoomRelationArrayHaveTotalCount*)initwithjsonstring:(NSString*)content:(int)error;
- (ChatRoomRelationArrayHaveTotalCount*)initwithNSDictionary:(NSDictionary*)content:(int)error;

@end
