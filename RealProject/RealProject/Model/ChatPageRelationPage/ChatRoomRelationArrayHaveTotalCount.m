//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ChatRoomRelationArrayHaveTotalCount.h"
@implementation ChatRoomRelationArrayHaveTotalCount
static ChatRoomRelationArrayHaveTotalCount *chatRoomRelationArrayHaveTotalCount;
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

- (ChatRoomRelationArrayHaveTotalCount *)initwithjsonstring:(NSString *)content:(int)error {
  NSError *err = nil;
  chatRoomRelationArrayHaveTotalCount =
      [[ChatRoomRelationArrayHaveTotalCount alloc] initWithString:content error:&err];
  return chatRoomRelationArrayHaveTotalCount;
}
- (ChatRoomRelationArrayHaveTotalCount *)initwithNSDictionary:(NSDictionary *)
                                     content:(int)error {
  NSError *err = nil;
  chatRoomRelationArrayHaveTotalCount =
      [[ChatRoomRelationArrayHaveTotalCount alloc] initWithDictionary:content error:&err];
  return chatRoomRelationArrayHaveTotalCount;
}

@end