//
//  AgentExperience.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentExperience_h
//#define abc_AgentExperience_h

//#endif
#import "JSONModel.h"
#import "ChatRoomMember.h"
@protocol ChatRoomRelationObject
@end

@interface ChatRoomRelationObject : JSONModel
@property(assign, nonatomic) int ChatRoomID;
@property(strong, nonatomic) NSString* DialogID;
@property(strong, nonatomic) NSArray<ChatRoomMember>* ChatRoomMember;


@end

//@implementation AgentExperience

//@end