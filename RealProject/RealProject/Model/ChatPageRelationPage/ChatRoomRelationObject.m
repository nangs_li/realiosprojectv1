//
//  AgentExperience.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatRoomRelationArrayHaveTotalCount.h"
@implementation ChatRoomRelationObject
static ChatRoomRelationObject *chatRoomRelationObject;
+ (BOOL)propertyIsOptional:(NSString*)propertyName {
  return YES;
}


- (ChatRoomRelationObject *)initwithjsonstring:(NSString *)content:(int)error {
    NSError *err = nil;
    chatRoomRelationObject = [[ChatRoomRelationArrayHaveTotalCount alloc] initWithString:content error:&err];
    return chatRoomRelationObject;
}
- (ChatRoomRelationObject *)initwithNSDictionary:(NSDictionary *)content:(int)error {
    NSError *err = nil;
    chatRoomRelationObject = [[ChatRoomRelationObject alloc] initWithDictionary:content error:&err];
    return chatRoomRelationObject ;
}

@end