//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "CrashlyticsModel.h"

@implementation CrashlyticsModel

static CrashlyticsModel *sharedCrashlyticsModel;

+ (CrashlyticsModel *)shared {
  @synchronized(self) {
    if (!sharedCrashlyticsModel) {
      sharedCrashlyticsModel = [[CrashlyticsModel alloc] init];
    }
    return sharedCrashlyticsModel;
  }
}

- (void)recordErrorUniqueID:(NSString *)errorUniqueID errorType:(NSString *)errorType errorTypeCode:(int)errorTypeCode errorDesc:(NSString *)errorDesc {
  NSString *memberID;
  if ([self isEmpty:[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue]]) {
    memberID = @"Empty";
  } else {
    memberID = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue];
  }
  NSDictionary *errorDictionary = @{ @"errorUniqueID" : errorUniqueID, @"memberID" : memberID, @"errorDesc" : errorDesc };

  NSError *error = [NSError errorWithDomain:errorType code:errorTypeCode userInfo:nil];
  [CrashlyticsKit recordError:error withAdditionalUserInfo:errorDictionary];
}

- (void)checkPlaceIDVaildateFromGoogleGeoCodePlaceIDArray:(NSMutableArray *)googleGeoCodePlaceIDArray {
  NSString *textPlaceID;
  for (NSString *placeID in googleGeoCodePlaceIDArray) {
    if ([self isEmpty:textPlaceID]) {
      textPlaceID = placeID;
    } else {
      if ([textPlaceID isEqualToString:placeID]) {
        textPlaceID = placeID;
      } else {
        [[CrashlyticsModel shared] recordErrorUniqueID:@"fsajfjisajfsaj" errorType:kFalseData errorTypeCode:kFalseDataCode errorDesc:@"placeIDFalse"];
      }
    }
  }
}
@end