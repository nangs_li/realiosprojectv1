//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"
#import <Branch/Branch.h>

// Deep Link Attributes
// Common
static NSString *kDeepLinkAttributeFromMemberId = @"member_id";
static NSString *kDeepLinkAttributeFromMemberName = @"member_name";
static NSString *kDeepLinkAttributeAction = @"action";
// "Invite to chat" only
static NSString *kDeepLinkAttributeFromMemberQBID = @"member_qbid";
// "Invite to follow" only
static NSString *kDeepLinkAttributeFromMemberImageUrl = @"member_image_url";
static NSString *kDeepLinkAttributeAgentListingId = @"agent_listing_id";
// eDM only
static NSString *kDeepLinkAttributeOriginalEmail = @"email";
static NSString *kDeepLinkAttributeCompany = @"company";
static NSString *kDeepLinkAttributeName = @"name";

// Installion Channels
static NSString *kInstallionChannelChat = @"Invite To Chat";
static NSString *kInstallionChannelFollow = @"Invite To Follow";
static NSString *kInstallionChannelDownload = @"Invite To Download";
static NSString *kInstallionChannelEDM = @"eDM";

// Deep Link Actions
static NSString *kDeepLinkActionChat = @"DeepLinkActionChat";
static NSString *kDeepLinkActionFollow = @"DeepLinkActionFollow";
static NSString *kDeepLinkActionNone = @"DeepLinkActionNone";

@interface DeepLinkActionHanderModel : BaseModel
+ (DeepLinkActionHanderModel *)shared;
@property(strong, nonatomic) NSString *QBID;
@property(strong, nonatomic) NSString *MemberName;
@property(strong, nonatomic) NSString *MemberID;
@property(strong, nonatomic) NSString *MemberImageURL;
@property(strong, nonatomic) NSString *AgentListingID;
@property(strong, nonatomic) NSString *action;
@property(nonatomic, assign) BOOL isFirstLogin;

-(void)cachedParameterFromLink:(NSDictionary*)params;
-(void)cachedParameterFromPushNotificationQBID:(NSString*)QBID;
-(void)inviteToChatWithQBID:(NSString*)QBID memberId:(NSString*)memberId memberName:(NSString*)memberName block:(callbackWithUrl)block;
-(void)inviteToFollowWithMemberId:(NSString*)memberId memberName:(NSString*)memberName imageURL:(NSString*)imageURL listingId:(NSString*)listingId block:(callbackWithUrl)block;
-(void)inviteToDownloadWithMemberId:(NSString*)memberId memberName:(NSString*)memberName block:(callbackWithUrl)block;


-(void)handleLinkWithParameter:(NSDictionary*)params;
-(void)handleLinkWithCacheWithType:(NSString*)type;
-(void)clearAllDeelLinkActionVar;
@end

//@implementation Photos

//@end