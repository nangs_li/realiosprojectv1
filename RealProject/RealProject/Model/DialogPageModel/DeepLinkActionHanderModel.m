//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RealUtility.h"
#import "DeepLinkActionHanderModel.h"
#import "FollowAgentModel.h"
#import "NotificationMessageCenter.h"
#import "DialogPageAgentProfilesModel.h"
#import "RealApiClient.h"
// Mixpanel
#import "Mixpanel.h"
#import "PressChatButtonModel.h"
#import "dbFollowAgentRecord.h"
@implementation DeepLinkActionHanderModel


static DeepLinkActionHanderModel *sharedDeepLinkActionHanderModel;

+ (DeepLinkActionHanderModel *)shared {
    @synchronized(self) {
        if (!sharedDeepLinkActionHanderModel) {
            sharedDeepLinkActionHanderModel = [[DeepLinkActionHanderModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedDeepLinkActionHanderModel;
    }
}

-(void)cachedParameterFromLink:(NSDictionary*)params{
    NSString *deepLinkAction = [params objectForKey:kDeepLinkAttributeAction];
    if (deepLinkAction) {
        NSString *QBID =  [params objectForKey:kDeepLinkAttributeFromMemberQBID];
        NSString *memberName = [params objectForKey:kDeepLinkAttributeFromMemberName];
        NSString *memberId = [params objectForKey:kDeepLinkAttributeFromMemberId];
        NSString *listingId =[params objectForKey:kDeepLinkAttributeAgentListingId];
        NSString *memberImageURL = [params objectForKey:kDeepLinkAttributeFromMemberImageUrl];
        BOOL firstLogin =  [[[NSUserDefaults standardUserDefaults]objectForKey:@"firstLogin"]boolValue];
        [DeepLinkActionHanderModel shared].isFirstLogin = firstLogin;
        [DeepLinkActionHanderModel shared].action =deepLinkAction;
        [DeepLinkActionHanderModel shared].QBID = QBID;
        [DeepLinkActionHanderModel shared].MemberName = memberName;
        [DeepLinkActionHanderModel shared].MemberID = memberId;
        [DeepLinkActionHanderModel shared].AgentListingID = listingId;
        [DeepLinkActionHanderModel shared].MemberImageURL = memberImageURL;
    }
}
-(void)cachedParameterFromPushNotificationQBID:(NSString*)QBID{
    if (!isEmpty(QBID)) {
        BOOL firstLogin =  [[[NSUserDefaults standardUserDefaults]objectForKey:@"firstLogin"]boolValue];
        [DeepLinkActionHanderModel shared].isFirstLogin = firstLogin;
        [DeepLinkActionHanderModel shared].action =kDeepLinkActionChat;
        [DeepLinkActionHanderModel shared].QBID = QBID;
    
    }
}

-(void)inviteToChatWithQBID:(NSString*)QBID memberId:(NSString*)memberId memberName:(NSString*)memberName block:(callbackWithUrl)block {
    
    NSMutableDictionary *params = [self getCommonParams:kDeepLinkActionChat];
    params[kDeepLinkAttributeFromMemberId] = memberId;
    params[kDeepLinkAttributeFromMemberName] = memberName;
    params[kDeepLinkAttributeFromMemberQBID] = QBID;
    params[kDeepLinkAttributeAction] = kDeepLinkActionChat;
    
    [[AppDelegate getAppDelegate].branchInstance getShortURLWithParams:params andChannel:kInstallionChannelChat andFeature:BRANCH_FEATURE_TAG_INVITE andCallback:block];
}


-(void)inviteToFollowWithMemberId:(NSString*)memberId memberName:(NSString*)memberName imageURL:(NSString*)imageURL listingId:(NSString*)listingId block:(callbackWithUrl)block {
    
    NSMutableDictionary *params = [self getCommonParams:kDeepLinkActionFollow];
    params[kDeepLinkAttributeFromMemberId] = memberId;
    params[kDeepLinkAttributeFromMemberName] = memberName;
    params[kDeepLinkAttributeFromMemberImageUrl] = imageURL;
    params[kDeepLinkAttributeAgentListingId] = listingId;
    params[kDeepLinkAttributeAction] = kDeepLinkActionFollow;
    
    [[AppDelegate getAppDelegate].branchInstance getShortURLWithParams:params andChannel:kInstallionChannelFollow andFeature:BRANCH_FEATURE_TAG_INVITE andCallback:block];
    
}

- (void)inviteToDownloadWithMemberId:(NSString *)memberId memberName:(NSString *)memberName block:(callbackWithUrl)block {
    
    NSMutableDictionary *params = [self getCommonParams:kDeepLinkActionNone];
    params[kDeepLinkAttributeFromMemberId] = memberId;
    params[kDeepLinkAttributeFromMemberName] = memberName;
    params[kDeepLinkAttributeAction] = kDeepLinkActionNone;
    
    [[AppDelegate getAppDelegate].branchInstance getShortURLWithParams:params andChannel:kInstallionChannelDownload andFeature:BRANCH_FEATURE_TAG_INVITE andCallback:block];
}


-(NSMutableDictionary*)getCommonParams:(NSString*)type{
    NSString *title = @"";
    NSString *description = @"";
    if ([type isEqualToString:kDeepLinkActionChat]) {
        title = JMOLocalizedString(@"invite_to_chat__subject", nil);
        description = JMOLocalizedString(@"invite_to_chat__content", nil);
    }else if ([type isEqualToString:kDeepLinkActionFollow]){
        title = JMOLocalizedString(@"invite_to_follow__subject", nil);
        description = JMOLocalizedString(@"invite_to_follow__content", nil);
    }else {
        title = JMOLocalizedString(@"invite_to_download__subject", nil);
        description = JMOLocalizedString(@"invite_to_download__content", nil);
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"$og_title"] = title;
    params[@"$og_description"] = description;
    return params;
}

-(void)handleLinkWithParameter:(NSDictionary*)params{
    
    BOOL isFreshInstall = [[params objectForKey:BRANCH_INIT_KEY_IS_FIRST_SESSION] boolValue];
    
    if (isFreshInstall) {
        
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        MixpanelPeople *people = mixpanel.people;
        
        // Media source
        NSString *channel = [params objectForKey:BRANCH_INIT_KEY_CHANNEL];
        
        if (channel == nil) { // Install from Appstore
            
            channel = @"Organic";
            
        } else if ([channel isEqualToString:kInstallionChannelChat] || [channel isEqualToString:kInstallionChannelDownload] || [channel isEqualToString:kInstallionChannelFollow]) { // Install from Viral
            
            [people set:@{@"Viral type": channel}];
            [mixpanel registerSuperProperties:@{@"Viral type": channel}];
            channel = @"Viral";
            
            // Set Referrer ID
            NSString *referrerId = [params objectForKey:kDeepLinkAttributeFromMemberId];
            if (referrerId) {
                
                [people set:@{@"Referrer ID": referrerId}];
            }
            
            // Set Referrer Name
            NSString *referrerName = [params objectForKey:kDeepLinkAttributeFromMemberName];
            if (referrerName) {
                
                [people set:@{@"Referrer Name": referrerName}];
            }
            
        } else if ([channel isEqualToString:kInstallionChannelEDM]) { // Install from eDM
            
            // Set Original Email
            NSString *originalEmail = [params objectForKey:kDeepLinkAttributeOriginalEmail];
            if (originalEmail) {
                
                [people set:@{@"Original Email": originalEmail}];
            }
            
            // Set Company
            NSString *company = [params objectForKey:kDeepLinkAttributeCompany];
            if (company) {
                
                [people set:@{@"Company": company}];
            }
        }
        
        // Set Campaign
        NSString *campaign = [params objectForKey:BRANCH_INIT_KEY_CAMPAIGN];
        if (campaign) {
            
            [people set:@{@"Campaign": campaign}];
        }
        
        [people set:@{@"Media source": channel}];
        [mixpanel registerSuperProperties:@{@"Media source": channel}];
        
        [mixpanel track:@"Installed App"];
    }
    
    
    [[DeepLinkActionHanderModel shared]cachedParameterFromLink:params];
    if ([RealUtility isValid:[LoginBySocialNetworkModel shared].myLoginInfo.MemberID]) {
        [self handleLinkWithCacheWithType:[DeepLinkActionHanderModel shared].action];
    }
}
-(void)handleLinkWithCacheWithType:(NSString*)type{
    NSString *deepLinkAction  = [DeepLinkActionHanderModel shared].action;
    NSString *memberName = [DeepLinkActionHanderModel shared].MemberName;
    NSString *memberId = [DeepLinkActionHanderModel shared].MemberID;
    NSString *listingId = [DeepLinkActionHanderModel shared].AgentListingID;
    NSString *memberImageURL = [DeepLinkActionHanderModel shared].MemberImageURL;
    NSString *QBID = [DeepLinkActionHanderModel shared].QBID;
    BOOL firstLogin = [DeepLinkActionHanderModel shared].isFirstLogin;
    
    if (![type isEqualToString:deepLinkAction]) {
        return;
    }
    
    if ([deepLinkAction isEqualToString:kDeepLinkActionChat]) {
          if (![[AppDelegate getAppDelegate] checkTopViewControllerIsChatRoomForQBID:QBID]) {
        if ([RealUtility isValid:QBID]) {
            [[DialogPageAgentProfilesModel shared]agentProfileGetListFromQBID:QBID success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                if (![self isEmpty:AgentProfiles]) {
                    [AppDelegate getAppDelegate].currentAgentProfile=   [AgentProfiles firstObject];
                }
                [kAppDelegate backToTopMainPageSelectedTabBarIndex:2];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                
            }];
            
            [[RealApiClient sharedClient] viralLogWriteWithType:2 inviteBy:memberId.integerValue block:nil];
        }
          }else{
               [[DeepLinkActionHanderModel shared]clearAllDeelLinkActionVar];
          }
    } else if ([deepLinkAction isEqualToString:kDeepLinkActionFollow]) {
        if ([RealUtility isValid:memberId]) {
            if (firstLogin) {
                [[NSUserDefaults standardUserDefaults]setObject:@(0) forKey:@"firstLogin"];
                [kAppDelegate backToTopMainPageSelectedTabBarIndex:1];
            }
            if (![self getIsFollowingMemberID:[memberId intValue]]) {
            [RealUtility prefetchPhotoURL:@[[NSURL URLWithString:memberImageURL]]];
            [[kAppDelegate getTabBarController]showLoading];
            [[RealApiClient sharedClient] viralLogWriteWithType:3 inviteBy:memberId.integerValue block:^(id response ,NSError *error){
                if (!error) {
                    [[FollowAgentModel shared]callFollowAgentApiByAgentProfileMemberID:memberId
                                                AgentProfileAgentListingAgentListingID:listingId
                                                                               success:^(id responseObject) {
                                                                                    [[DeepLinkActionHanderModel shared] clearAllDeelLinkActionVar];
                                                                                   [[NotificationMessageCenter sharedCenter]showfollowedMessageWithMemberName:memberName memberURL:memberImageURL withBlock:^{
                                                                                       [kAppDelegate backToTopMainPageSelectedTabBarIndex:1];
                                                                                       [[kAppDelegate getTabBarController]hideLoading];
                                                                                       
                                                                                   }];
                                                                               }failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                                                          NSString *errorMessage, RequestErrorStatus errorStatus,
                                                                                          NSString *alert, NSString *ok) {
                                                                                   [[kAppDelegate getTabBarController]hideLoading];
                                                                                   [[DeepLinkActionHanderModel shared] clearAllDeelLinkActionVar];
                                                                               }];

                }
              
            }];
            }else{
                [[DeepLinkActionHanderModel shared] clearAllDeelLinkActionVar];
            }
                  }
    } else {
        
        if ([RealUtility isValid:memberId]) {
            
            [[RealApiClient sharedClient] viralLogWriteWithType:1 inviteBy:memberId.integerValue block:nil];
        }
    }
}

-(void)clearAllDeelLinkActionVar{
    [DeepLinkActionHanderModel shared].isFirstLogin = nil;
    [DeepLinkActionHanderModel shared].action =nil;
    [DeepLinkActionHanderModel shared].QBID = nil;
    [DeepLinkActionHanderModel shared].MemberName = nil;
    [DeepLinkActionHanderModel shared].MemberID = nil;
    [DeepLinkActionHanderModel shared].AgentListingID = nil;
    [DeepLinkActionHanderModel shared].MemberImageURL = nil;
}

- (BOOL)getIsFollowingMemberID:(int)agentMemberID {
    
    DBResultSet * result =  [[[DBFollowAgentRecord query]
                              whereWithFormat:@"MemberID = %d AND FollowingMemberID = %d",[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue], agentMemberID] fetch] ;
    if (result.count>0) {
        for (DBFollowAgentRecord * dbFollowAgentRecord in result) {
            
            if (dbFollowAgentRecord.IsFollowing>0) {
                return YES;
            }else{
                return NO;
            }
        }
    }else{
        return NO;
    }
    return NO;
}

@end