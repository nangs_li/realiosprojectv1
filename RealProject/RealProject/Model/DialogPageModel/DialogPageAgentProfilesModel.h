//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
@interface DialogPageAgentProfilesModel : BaseModel
@property(strong, nonatomic)NSMutableArray<AgentProfile> *dialogPageSearchAgentProfiles;
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(strong, nonatomic)SearchAgentProfile *dialogPageSearchAgentProfilesHaveTotalCount;
@property(strong, nonatomic)NSMutableArray<AgentProfile> *serverReturnDialogPageSearchAgentProfiles;
@property(strong, nonatomic)SearchAgentProfile *serverReturnDialogPageSearchAgentProfilesHaveTotalCount;
@property(strong, nonatomic)NSMutableArray<AgentProfile> *dialogPageSpotLightSearchAgentProfiles;
@property(strong, nonatomic)SearchAgentProfile *dialogPageSpotLightSearchAgentProfilesHaveTotalCount;
@property(strong, nonatomic)NSMutableArray<AgentProfile> *inviteToChatGetAgentProfile;
@property(strong, nonatomic)SearchAgentProfile *inviteToChatGetAgentProfileHaveTotalCount;
@property(nonatomic, strong) NSTimer *updateDBDiaolgPageAgentProfileTimer;
+ (DialogPageAgentProfilesModel *)shared ;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 */
-(void)AgentProfileGetListsuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                      failure:(failureBlock) failure;


- (void)AgentProfileGetListtFromchatlistQBidarray:(NSMutableArray *)chatlistQBidarray numberofStartRecord:(int)numberofStartRecord  success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                          failure:(failureBlock ) failure;
- (void)agentProfileGetListFromQBID:(NSString *)QBID  success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                            failure:(failureBlock ) failure;
- (void)spotLightSearchFromQBID:(NSString *)QBID success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                        failure:(failureBlock ) failure;

-(void)callUpdateDBDiaolgPageAgentProfileTimer;

-(AgentProfile *)getAgentProfileFromQBID:(NSString *)QBID;
-(void)getAllDBDialogPageSearchAgentProfiles;
@end
