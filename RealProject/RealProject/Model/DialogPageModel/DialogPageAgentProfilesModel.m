//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "DialogPageAgentProfilesModel.h"

#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "DBChatroomAgentProfiles.h"
#import "ChatDialogModel.h"
@implementation DialogPageAgentProfilesModel
 #define AgentProfileGetListNumberOfRecord 5
static DialogPageAgentProfilesModel *sharedDialogPageAgentProfilesModel;




+ (DialogPageAgentProfilesModel *)shared {
    @synchronized(self) {
        if (!sharedDialogPageAgentProfilesModel) {
            sharedDialogPageAgentProfilesModel = [[DialogPageAgentProfilesModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedDialogPageAgentProfilesModel;
    }
}


- (void)AgentProfileGetListsuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                       failure:(failureBlock ) failure{
    
    if ([LoginBySocialNetworkModel shared].myLoginInfo.MemberID ==0 ) {
        return;
    }
    NSMutableArray *chatlistQBidarray = [[NSMutableArray alloc] init];
    for (QBChatDialog *chatDialog in [ChatDialogModel shared].dialogs) {
        NSString *tableViewCellQBID = [chatDialog getRecipientIDFromQBChatDialog];
              [chatlistQBidarray
         addObject:[NSString stringWithFormat:@"%@", tableViewCellQBID]];
    }
  if (![self networkConnection]) {
       failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      return;
  }
 
  [[[DBChatroomAgentProfiles query] fetch] removeAll];
   
    
    [self AgentProfileGetListtFromchatlistQBidarray:chatlistQBidarray numberofStartRecord:0  success:success failure:failure];

}
- (void)AgentProfileGetListtFromchatlistQBidarray:(NSMutableArray *)chatlistQBidarray numberofStartRecord:(int)numberofStartRecord  success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                           failure:(failureBlock ) failure{
    if (numberofStartRecord>=chatlistQBidarray.count) {
        self.dialogPageSearchAgentProfiles =[[NSMutableArray<AgentProfile> alloc]init];
       [self getAllDBDialogPageSearchAgentProfiles];
        
        DDLogDebug(@"self.dialogPageSearchAgentProfiles.count %lu",self.dialogPageSearchAgentProfiles.count);
        
        if (success) {
            
            success(self.dialogPageSearchAgentProfiles);
        }
        return;
    }
    int numberOfRecord=AgentProfileGetListNumberOfRecord;
    
    if (numberofStartRecord+AgentProfileGetListNumberOfRecord > chatlistQBidarray.count) {
        
        numberOfRecord=(int)chatlistQBidarray.count - numberofStartRecord;
    }
    
    NSArray *  selfChatlistQBidarray =[chatlistQBidarray subarrayWithRange:NSMakeRange(numberofStartRecord, numberOfRecord)];
    DDLogDebug(@"selfChatlistQBidarray.count%lu",(unsigned long)selfChatlistQBidarray.count);
       NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"QBIDList\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            [self nsDictionaryArrayToInputParametersString:[selfChatlistQBidarray mutableCopy]],
                            nil];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/AgentProfileGetList_EX"];
      __weak typeof(self) weakSelf = self;
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"TestInputJson-->%@", parameters);
    
    
    
    [manager POST:urllink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
        
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  self.serverReturnDialogPageSearchAgentProfilesHaveTotalCount =
                  [[SearchAgentProfile alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  
                  self.serverReturnDialogPageSearchAgentProfiles =
                  self.serverReturnDialogPageSearchAgentProfilesHaveTotalCount.AgentProfiles;
                  
                  for (AgentProfile *  agentProfile in self.serverReturnDialogPageSearchAgentProfiles) {
                      [DBChatroomAgentProfiles removeDBLobbyAgentProfile:agentProfile];
                      [DBChatroomAgentProfiles createDBLobbyAgentProfile:agentProfile];
                  }
                  
                  
                  [ weakSelf AgentProfileGetListtFromchatlistQBidarray:chatlistQBidarray numberofStartRecord:numberofStartRecord+AgentProfileGetListNumberOfRecord success:success failure:failure];
              
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));          }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}


- (void)agentProfileGetListFromQBID:(NSString *)QBID  success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                           failure:(failureBlock ) failure{
    
    
    NSMutableArray *chatlistQBidarray = [[NSMutableArray alloc] init];
           [chatlistQBidarray
         addObject:[NSString stringWithFormat:@"%@", QBID]];
    
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"QBIDList\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            [self nsDictionaryArrayToInputParametersString:chatlistQBidarray],
                            nil];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/AgentProfileGetList_EX"];
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->%@", parameters);
    
    
    
    [manager POST:urllink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              DDLogVerbose(@"self.HandleServerReturnString to string-->%@",
                           [self.HandleServerReturnString toJSONString]);
              DDLogVerbose(@"self.HandleServerReturnString.ErrorCode-->%d",
                           self.HandleServerReturnString.ErrorCode);
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  self.inviteToChatGetAgentProfileHaveTotalCount =
                  [[SearchAgentProfile alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  
                  self.inviteToChatGetAgentProfile =
                  self.inviteToChatGetAgentProfileHaveTotalCount.AgentProfiles;
                  DDLogVerbose(@"dialogPageSearchAgentProfiles-->%@",
                               self.dialogPageSearchAgentProfiles);
                  
                  //// store DBQBUUser into local
                 
                  
                  success(self.inviteToChatGetAgentProfile);
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));          }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}

- (void)spotLightSearchFromQBID:(NSString *)QBID success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                                 failure:(failureBlock ) failure{
    
    
    NSMutableArray *chatlistQBidarray = [[NSMutableArray alloc] init];
   
        [chatlistQBidarray addObject:QBID];
    
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"QBIDList\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            [self nsDictionaryArrayToInputParametersString:chatlistQBidarray],
                            nil];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/AgentProfileGetList_EX"];
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->%@", parameters);
    
    
    
    [manager POST:urllink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              DDLogVerbose(@"self.HandleServerReturnString to string-->%@",
                           [self.HandleServerReturnString toJSONString]);
              DDLogVerbose(@"self.HandleServerReturnString.ErrorCode-->%d",
                           self.HandleServerReturnString.ErrorCode);
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  self.dialogPageSpotLightSearchAgentProfilesHaveTotalCount =
                  [[SearchAgentProfile alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  
                  self.dialogPageSpotLightSearchAgentProfiles =
                  self.dialogPageSpotLightSearchAgentProfilesHaveTotalCount.AgentProfiles;
                  DDLogVerbose(@"dialogPageSearchAgentProfiles-->%@",
                               self.dialogPageSearchAgentProfiles);
                  
                 
                  success(self.dialogPageSpotLightSearchAgentProfiles);
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));          }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}

//// nsDictionaryArrayToInputParametersString
- (NSString *)nsDictionaryArrayToInputParametersString:(NSMutableArray *)array {
    NSString *ArrayjsonString;
    NSError *error;
    NSData *jsonData =
    [NSJSONSerialization dataWithJSONObject:array
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];
    
    if (!jsonData) {
        DDLogInfo(@"Got an error: %@", error);
    } else {
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        ArrayjsonString =
        [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    
    return ArrayjsonString;
}
-(void)callUpdateDBDiaolgPageAgentProfileTimer{
    
    [self.updateDBDiaolgPageAgentProfileTimer invalidate];
    
    self.updateDBDiaolgPageAgentProfileTimer =
    [NSTimer scheduledTimerWithTimeInterval:kUpdateDBDialogPageAgentProfileTimerInterval
                                     target:self
                                   selector:@selector(updateDBDiaolgPageAgentProfile)
                                   userInfo:nil
                                    repeats:YES];
    
    
}
-(void)updateDBDiaolgPageAgentProfile{
    
  DBResultSet *  r = [[[[[DBChatroomAgentProfiles query]
          whereWithFormat:
          @"MemberID = %@",
          [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
           .myLoginInfo.MemberID]] orderByDescending:@"Id"] limit:kUpdateDBDialogPageAgentProfileNumberofRecord]
         fetch];
    NSMutableArray *chatlistQBidarray =[[NSMutableArray<AgentProfile> alloc]init];
    for (DBChatroomAgentProfiles *p in r) {
        DDLogDebug(@"[p.Date timeIntervalSince1970] %f",[p.Date timeIntervalSince1970]);
          DDLogDebug(@"[[NSDate date]timeIntervalSince1970 ] %f",[[NSDate date]timeIntervalSince1970 ]);
        if ([p.Date timeIntervalSince1970]+kUpdateDBDialogPageAgentProfileBetweenTime <=[[NSDate date]timeIntervalSince1970 ]) {
           
            AgentProfile * agentprofile  =[NSKeyedUnarchiver unarchiveObjectWithData:p.ChatroomAgentProfile];
            [chatlistQBidarray addObject:agentprofile.QBID];
          
        }
    }
    if (![chatlistQBidarray valid]) {
      
        return;
    }
    [[DialogPageAgentProfilesModel shared]AgentProfileGetListtFromchatlistQBidarray:chatlistQBidarray numberofStartRecord:0 success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
        [r removeAll];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
        
    }];
    
}

-(AgentProfile *)getAgentProfileFromQBID:(NSString *)QBID{
    
    
    for (AgentProfile *agentprofile in self.dialogPageSearchAgentProfiles) {
        
        
        if ([agentprofile.QBID isEqualToString:[NSString stringWithFormat:@"%@", QBID]]) {
           
            return agentprofile;
            //  return agentprofile;
        }
      
}
      return nil;
}

-(void)getAllDBDialogPageSearchAgentProfiles{
    self.dialogPageSearchAgentProfiles =[[NSMutableArray<AgentProfile> alloc]init];
    DBResultSet *    resultDBChatroomAgentProfiles = [[DBChatroomAgentProfiles query]fetch];
    for (DBChatroomAgentProfiles *dbChatroomAgentProfiles in resultDBChatroomAgentProfiles) {
        if (!isEmpty(dbChatroomAgentProfiles.ChatroomAgentProfile)) {
            [self.dialogPageSearchAgentProfiles addObject:
             [NSKeyedUnarchiver unarchiveObjectWithData:dbChatroomAgentProfiles.ChatroomAgentProfile]];
        }
       
    }

}
@end