//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
@interface DialogPageSearchAgentProfilesModel : BaseModel
@property(strong, nonatomic) NSMutableArray<AgentProfile>* dialogPageSearchAgentProfiles;
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(strong, nonatomic)SearchAgentProfile *dialogPageSearchAgentProfilesHaveTotalCount;
@property(assign, nonatomic) int page;
@property(assign, nonatomic) int searchAgentProfilesTotalCount;
@property(assign, nonatomic) BOOL tableViewNotLoading;
+ (DialogPageSearchAgentProfilesModel *)shared ;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 
 */
-(void)AgentSearch:(NSString *)searchBarText        success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                      failure:(failureBlock) failure;
- (void)AgentSearchWithPage:(NSString *)searchBarText  success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                    failure:(failureBlock) failure;






@end
