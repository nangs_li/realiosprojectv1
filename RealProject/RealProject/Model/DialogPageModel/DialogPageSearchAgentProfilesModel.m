//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "DialogPageSearchAgentProfilesModel.h"

#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "ChatDialogModel.h"
#import "DBQBChatDialog.h"
@implementation DialogPageSearchAgentProfilesModel

static DialogPageSearchAgentProfilesModel *sharedDialogPageSearchAgentProfilesModel;




+ (DialogPageSearchAgentProfilesModel *)shared {
    @synchronized(self) {
        if (!sharedDialogPageSearchAgentProfilesModel) {
            sharedDialogPageSearchAgentProfilesModel = [[DialogPageSearchAgentProfilesModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedDialogPageSearchAgentProfilesModel;
    }
}


- (void)AgentSearch:(NSString *)searchBarText        success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                       failure:(failureBlock) failure{
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"Page\":%@,"
                            @"\"Pattern\":\"%@\"}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            @"1", searchBarText, nil];
    
    NSString *urllink = [NSString
                        stringWithFormat:@"%@%@", kServerAddress,
                        @"/Operation.asmx/AgentSearch"];
    
    
    
  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
       failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      return;
  }

    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
   

    
  [manager POST:urllink
      parameters:@{ @"InputJson" : parameters }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

          NSString *dstring = [responseObject objectForKey:@"d"];
          if ([self checkAccessError:dstring]) {
              failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              
          }
          DDLogVerbose(@"serverreturnstring-->%@", dstring);
          
          NSError *err = nil;
          self.HandleServerReturnString =
          [[HandleServerReturnString alloc] initWithString:dstring error:&err];
          // DDLogInfo(@"self.HandleServerReturnString to string-->%@",
          // [self.HandleServerReturnString toJSONString]);
          // DDLogInfo(@"self.HandleServerReturnString.ErrorCode-->%d",
          // self.HandleServerReturnString.ErrorCode);
          int error = self.HandleServerReturnString.ErrorCode;
          if (error == 0) {
              if ([self.HandleServerReturnString.APIName
                   isEqualToString:@"AgentSearch"]) {
                  self.dialogPageSearchAgentProfilesHaveTotalCount =
                  [[SearchAgentProfile alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  NSMutableArray<AgentProfile> * agentProfileMutableArray = [[NSMutableArray<AgentProfile> alloc]init];
               
                  for (AgentProfile * agentProfile in self.dialogPageSearchAgentProfilesHaveTotalCount.AgentProfiles) {
                      if ( ![self checkAgentProfileHaveDialogPage:agentProfile]) {
                          [agentProfileMutableArray addObject:agentProfile];
                      }
                   
                                         }
                  self.searchAgentProfilesTotalCount=[self.dialogPageSearchAgentProfilesHaveTotalCount.TotalCount intValue];
                  self.dialogPageSearchAgentProfiles = agentProfileMutableArray;
              }else{
                  
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              self.page=1;
              if (self.dialogPageSearchAgentProfilesHaveTotalCount.AgentProfiles.count==0) {
                  self.tableViewNotLoading=YES;
              }else{
                  self.tableViewNotLoading=NO;
              }
              success(self.dialogPageSearchAgentProfiles);
          }else{
              failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
       failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      }];
}
- (void)AgentSearchWithPage:(NSString *)searchBarText  success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
            failure:(failureBlock) failure{
    if (self.page==0) {
        self.page=2;
    }else{
        self.page++;
    }
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"Page\":%d,"
                            @"\"Pattern\":\"%@\"}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            self.page, searchBarText, nil];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/AgentSearch"];
    
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    
    
    [manager POST:urllink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              // DDLogInfo(@"self.HandleServerReturnString to string-->%@",
              // [self.HandleServerReturnString toJSONString]);
              // DDLogInfo(@"self.HandleServerReturnString.ErrorCode-->%d",
              // self.HandleServerReturnString.ErrorCode);
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  if ([self.HandleServerReturnString.APIName
                       isEqualToString:@"AgentSearch"]) {
                      self.dialogPageSearchAgentProfilesHaveTotalCount =
                      [[SearchAgentProfile alloc]
                       initwithNSDictionary:self.HandleServerReturnString.
                       Content:error];
                      if (self.dialogPageSearchAgentProfilesHaveTotalCount.AgentProfiles.count==0) {
                          self.tableViewNotLoading=YES;
                      }else{
                           self.tableViewNotLoading=NO;
                      }
                      
                      NSMutableArray<AgentProfile> * agentProfileMutableArray = [[NSMutableArray<AgentProfile> alloc]init];
                      
                      for (AgentProfile * agentProfile in self.dialogPageSearchAgentProfilesHaveTotalCount.AgentProfiles) {
                          if ( ![self checkAgentProfileHaveDialogPage:agentProfile]) {
                              if ( ![self isDialogPageSearchAgentProfilesAlreadyHaveAgentProfile:agentProfile]) {
                              [agentProfileMutableArray addObject:agentProfile];
                              }
                          }
                          
                      }
                      if (![self isEmpty:self.dialogPageSearchAgentProfiles]) {
                          [self.dialogPageSearchAgentProfiles addObjectsFromArray:agentProfileMutableArray];
                      }
                  }else{
                      
                      failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                              JMOLocalizedString(@"alert_ok", nil));
                  }
                  success(self.dialogPageSearchAgentProfiles);
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}
-(BOOL)checkAgentProfileHaveDialogPage:(AgentProfile *)agentProfile{
      BOOL SameAgentInDailogPage =NO;
    for (QBChatDialog * dialog in [ChatDialogModel shared].dialogs) {
      //  DDLogError(@"dialog.recipientID %ld ",(long)dialog.recipientID);
        NSString *tableViewCellQBID = [dialog getRecipientIDFromQBChatDialog];
            if ([tableViewCellQBID isEqualToString:agentProfile.QBID]) {
            SameAgentInDailogPage=YES;
        }
        
    }
    
    if ([[LoginBySocialNetworkModel shared].myLoginInfo.QBID isEqualToString:agentProfile.QBID]) {
         SameAgentInDailogPage=YES;
    }
    return SameAgentInDailogPage;
    
}
-(BOOL)isDialogPageSearchAgentProfilesAlreadyHaveAgentProfile:(AgentProfile *)agentProfile{
    BOOL SameAgentInDailogPage =NO;
    for (AgentProfile * dialogAgentProfile in self.dialogPageSearchAgentProfiles) {
        if ( [dialogAgentProfile.QBID isEqualToString:agentProfile.QBID]) {
            return YES;
        }
    }
    return SameAgentInDailogPage;
    
}
@end