//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SendSMSViaTwilioModel.h"
#import "NSDictionary+Utility.h"
#import "HandleServerReturnString.h"

@implementation SendSMSViaTwilioModel

static SendSMSViaTwilioModel *sharedSendSMSViaTwilioModel;




+ (SendSMSViaTwilioModel *)shared {
    @synchronized(self) {
        if (!sharedSendSMSViaTwilioModel) {
            sharedSendSMSViaTwilioModel = [[SendSMSViaTwilioModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedSendSMSViaTwilioModel;
    }
}


- (void)SendSMSViaTwilio:(NSString *)toCountryCode ToPhoneNumber:(NSString *)toPhoneNumber url:(NSString*)urlString success:(successBlock)success
                 failure:(failureBlock) failure{
    NSString *languageCode= [AppDelegate getAppDelegate].languageCode;
    if (!isEmpty([SystemSettingModel shared].selectSystemLanguageList.Code)) {
        languageCode=[SystemSettingModel shared].selectSystemLanguageList.Code;
    }
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setObject:[LoginBySocialNetworkModel shared].myLoginInfo.MemberID forKey:@"MemberID"];
    [parameters setObject:[LoginBySocialNetworkModel shared].myLoginInfo.AccessToken forKey:@"AccessToken"];
    [parameters setObject:toCountryCode forKey:@"ToCountryCode"];
    [parameters setObject:toPhoneNumber forKey:@"ToPhoneNumber"];
    [parameters setObject:urlString forKey:@"URL"];
    [parameters setObject:languageCode forKey:@"Lang"];
//    NSString *parameters = [NSString
//                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"ToCountryCode\":\"%@\","
//                            @"\"ToPhoneNumber\":\"%@\"}",
//                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
//                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
//                            toCountryCode, toPhoneNumber, nil];
    
    NSString *urllink = [NSString
                        stringWithFormat:@"%@%@", kServerAddress,
                        @"/Operation.asmx/SendSMSViaTwilio"];
    
    
    
  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
       failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      return;
  }

    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
   

    
  [manager POST:urllink
      parameters:@{ @"InputJson" : [parameters jsonPresentation] }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

          NSString *dstring = [responseObject objectForKey:@"d"];
          if ([self checkAccessError:dstring]) {
              failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              
          }
          DDLogVerbose(@"serverreturnstring-->%@", dstring);
          
          NSError *err = nil;
          self.HandleServerReturnString =
          [[HandleServerReturnString alloc] initWithString:dstring error:&err];
          // DDLogInfo(@"self.HandleServerReturnString to string-->%@",
          // [self.HandleServerReturnString toJSONString]);
          // DDLogInfo(@"self.HandleServerReturnString.ErrorCode-->%d",
          // self.HandleServerReturnString.ErrorCode);
          int error = self.HandleServerReturnString.ErrorCode;
          if (error == 0) {
              
           success(responseObject);
          }else{
              failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));          }

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
       failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      }];
}

@end