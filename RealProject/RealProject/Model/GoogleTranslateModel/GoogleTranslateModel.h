//
//  GoogleTranslateModel.h
//  productionreal2
//
//  Created by Derek Cheung on 24/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"

// Model
#import "LanguageDetectionModel.h"

@interface GoogleTranslateModel : JSONModel

#pragma mark - Request

@property (nonatomic, strong) NSString *key;        // Google API Key
@property (nonatomic, strong) NSString *q;          // Source text

+ (NSArray *)requestKeys;

#pragma mark - Response

@property (nonatomic, strong) NSDictionary *data;   // Response data
@property (nonatomic, strong) LanguageDetectionModel *languageDetection;

@end
