//
//  GoogleTranslateModel.m
//  productionreal2
//
//  Created by Derek Cheung on 24/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "GoogleTranslateModel.h"

@interface GoogleTranslateModel ()

@property (nonatomic, strong) NSArray *detections;

@end

@implementation GoogleTranslateModel

#pragma mark - Override

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    
    return YES;
}

+ (JSONKeyMapper*)keyMapper {
    
    JSONKeyMapper *mapper = [[JSONKeyMapper alloc] initWithDictionary:@{
                                                                        @"data.detections": @"detections"
                                                                        }];
    return mapper;
}

#pragma mark - Public

+ (NSArray *)requestKeys {
    
    return [NSArray arrayWithObjects:
            @"key",
            @"q",
            nil];
}

#pragma mark - Getters

- (LanguageDetectionModel *)languageDetection {
    
    if (!_languageDetection) {
        
        NSArray *tempArray = [self.detections firstObject];
        
        if (tempArray) {
            
            NSDictionary *tempDict = [tempArray firstObject];
            
            if (tempDict) {
                
                _languageDetection = [[LanguageDetectionModel alloc] initWithDictionary:tempDict error:nil];
            }
        }
    }
    
    return _languageDetection;
}

@end
