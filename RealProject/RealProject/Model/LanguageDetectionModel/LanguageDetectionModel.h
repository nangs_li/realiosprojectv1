//
//  LanguageDetectionModel.h
//  productionreal2
//
//  Created by Derek Cheung on 24/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"

@interface LanguageDetectionModel : JSONModel

#pragma mark - Response

@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSNumber *confidence;

- (NSString *)mappedLanguage;

@end
