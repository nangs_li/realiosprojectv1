//
//  LanguageDetectionModel.m
//  productionreal2
//
//  Created by Derek Cheung on 24/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "LanguageDetectionModel.h"

@implementation LanguageDetectionModel

#pragma mark - Override

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

#pragma mark - Public

+ (NSArray *)requestKeys {
  return [NSArray arrayWithObjects:@"key", @"q", nil];
}

- (NSString *)mappedLanguage {
    if (self.confidence.floatValue < 0.5) {
        return @"en";
    }
    
    if ([self.language isEqualToString:@"zh-CN"]) {
        return @"zh-TW";
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", self.language];
    NSArray *filteredData  = [[LanguageDetectionModel supportedLanguage] filteredArrayUsingPredicate:predicate];
    
    if (filteredData.count > 0) {
        return self.language;
    }
    
    return @"en";
}

+ (NSArray<NSString *> *)supportedLanguage {
  return @[@"ar" , @"bg" , @"bn" , @"ca" , @"cs", @"da" , @"de" , @"el" , @"en" , @"es" , @"eu" , @"fa" , @"fi" , @"fil" , @"fr" , @"gl" , @"gu" , @"hi" , @"hr" , @"hu" , @"id" , @"it" , @"iw" , @"ja" , @"kn" , @"ko" , @"lt" , @"lv" , @"ml" , @"mr" , @"nl" , @"no" , @"pl" , @"pt" , @"ro" , @"ru" , @"sk" , @"sl" , @"sr" , @"sv" , @"ta" , @"te" , @"th" , @"tl" , @"tr" , @"uk" , @"vi" , @"zh-CN" , @"zh-TW"];
}

@end
