//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
@interface AgentListingModel : BaseModel
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
//// posting  add Descriptions
@property (nonatomic,strong) NSMutableDictionary *reasonDict;

@property(nonatomic, strong) NSMutableArray *addnewlanguageindex;
@property(nonatomic, strong) NSMutableArray *addnewonereason;
@property(nonatomic, strong) NSMutableArray *addnewtworeason;
@property(nonatomic, strong) NSMutableArray *addnewthreereason;
@property(nonatomic, strong) NSString *currentaddnewlanguage;
@property(nonatomic, strong) NSString *userInputTextLanguage;
//// posting  add Descritptions
@property(nonatomic, assign) NSInteger languageindex;
//// posting googleGeoCode Api  Address Array
@property(nonatomic, strong) NSMutableArray *googleGeoCodeApiAddressArray;
//// storage google API Return Address  Memory Storage Only.
@property(nonatomic, strong) NSString *googleapireturnaddress;
//// posting Info
@property(nonatomic, strong) NSString *street;
@property(nonatomic, strong) NSString *apt;
@property(nonatomic, strong) NSString *numberofbedroom;
@property(nonatomic, strong) NSString *numberofbathroom;
@property(nonatomic, strong) NSString *price;
@property(nonatomic, strong) NSString *size;
@property(nonatomic, strong) NSString *followercount;
@property(nonatomic, retain) NSMutableArray *chosenImages;
@property(nonatomic, assign) int totalSelectedImageCount;
@property(nonatomic, strong) NSString *selectcurrencytype;
@property(nonatomic, strong) MetricList *selectmetric;
@property(nonatomic, strong) SpaceTypeList *selectspacetypelist;
@property(nonatomic, strong) PropertyTypeList *selectpropertytypelist;

@property(nonatomic, retain) NSMutableArray *reasonImageArray;
@property(nonatomic, retain) NSMutableArray *reasonLanguageIndexArray;
@property (nonatomic,assign) int imageUploadSuccessCount;
@property (nonatomic,assign) int totatImageUploadCount;
@property (nonatomic,assign) BOOL imageArrayCompressFinish;
@property (nonatomic,strong) NSArray * compressFinishURLRequestImageArray;
@property (nonatomic,strong) UIScrollView *coverPhotoScrollView;
+ (AgentListingModel *)shared ;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 
 */

- (void)resetAllAgentListingPartOnlyLocalData;
- (BOOL)haveHistory;
- (NSNumber*) firstReasonLangIndex;
-(void)callAgentListingAddApisuccess:(successBlock)success
                      failure:(failureBlock ) failure;

- (void)uploadAgentListingImageFromResponseObject:(id)responseObject success:(void (^)(id responseObject))success
failure:(failureBlock) failure;

//-(NSArray*)uploadAgentListingChoseImageToServerFromResponseObject:(id)responseObject  success:(void (^)(id responseObject))success
//                                                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error,
//                                                                        NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) ) failure;
//
//
//-(NSArray*)uploadReasonImageToServerFromResponseObject:(id)responseObject  success:(void (^)(id responseObject))success
//                                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error,
//                                                             NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) ) failure;
- (void)geoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLink:
(NSString *)urllink languageString:(NSString *)languageString placeId:(NSString *)placeId success:(successBlock)success
                                                                failure:(failureBlock) failure;
- (void)geoCodeJsonApiFromAddress:(NSString *)address language:(NSString *)language success:(successBlock)success failure:(failureBlock) failure;
-(AgentProfile*)convertToPreviewAgentProfile;
- (void)callChinaGoogleMapPlaceAPIGeocodeBySearchAddressUserInputSearchAddress:(NSString *)userInputSearchAddress
                                                                      language:(NSString *)language
                                                                       success:(void (^)(id responseObject))success
                                                                       failure:(failureBlock) failure;
- (void)callGoogleLanguageDetectionLangToDetect:(NSString *)LangToDetect success:(successBlock)success failure:(failureBlock) failure;

-(void)resetAllAgentListingPartOnlyLocalData;

-(void)removeReasonDictByLangIndex:(NSNumber*)langIndex;
-(void)compressImageArraywithBlock:(CommonBlock)block;
- (BOOL) canAddMoreLanguage;
@end
