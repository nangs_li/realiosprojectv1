//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AgentListingModel.h"
#import "RealUtility.h"
#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "RealUtility.h"
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "XMLReader.h"
#import "GetContryLocation.h"
#import "HandleGoogleAutoCompleteJson.h"
#import "HandleServerReturnGoogleString.h"
#import "HandleGoogleAutoCompleteJson.h"
#import "UIImage+Utility.h"
@implementation AgentListingModel

static AgentListingModel *sharedAgentListingModel;




+ (AgentListingModel *)shared {
    @synchronized(self) {
        if (!sharedAgentListingModel) {
            sharedAgentListingModel = [[AgentListingModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedAgentListingModel;
    }
}


- (void)callAgentListingAddApisuccess:(successBlock)success
                              failure:(failureBlock) failure{
    NSMutableArray *addressuserinputarray = [[NSMutableArray alloc] init];
    
    NSDictionary *addressuserinput = @{
                                       @"LanguageIndex" : @"1",
                                       @"Street" : self.street,
                                       @"AptSuite" : self.apt,
                                       @"Unit" : @""
                                       };
    [addressuserinputarray addObject:addressuserinput];
    
    NSMutableArray *reasoninputarray = [[NSMutableArray alloc] init];
    
    for (NSNumber *langNumber in self.reasonDict.allKeys) {
        NSDictionary *langDict = self.reasonDict[langNumber];
        NSString *langIndex = [langNumber stringValue];
        
        NSDictionary *reasonOneDict = langDict[@(0)];
        NSDictionary *reasonTwoDict = langDict[@(1)];
        NSDictionary *reasonThreeDict = langDict[@(2)];
        NSString *reasonOneString =@"";
        NSString *reasonTwoString =@"";
        NSString *reasonThreeString =@"";
        NSNumber *sloganIndex = @(0);
        if ([RealUtility isValid:reasonOneDict]) {
            if (reasonOneDict[@"reasonString"]) {
                reasonOneString = reasonOneDict[@"reasonString"];
            }
        }
        
        if ([RealUtility isValid:reasonTwoDict]) {
            if (reasonTwoDict[@"reasonString"]) {
                reasonTwoString =reasonTwoDict[@"reasonString"];
            }
        }
        
        if([RealUtility isValid:reasonThreeDict]){
            if (reasonThreeDict[@"reasonString"]) {
                reasonThreeString = reasonThreeDict[@"reasonString"];
            }
        }
        
        if ([RealUtility isValid:langDict[@"sloganIndex"]]) {
            sloganIndex = langDict[@"sloganIndex"];
        }
        
        [reasoninputarray addObject:@{
                                      @"LanguageIndex" : langIndex,
                                      @"Reason1" : reasonOneString,
                                      @"Reason2" : reasonTwoString,
                                      @"Reason3" : reasonThreeString,
                                      @"SloganIndex": sloganIndex
                                      }];
    }
    
    int selectmeticindex = 1;
    if (![self isEmpty:self.selectmetric]) {
        selectmeticindex = self.selectmetric.Index;
    }
    NSString *selectcurrencytype = @"USD";
    
    if (![self isEmpty:self.selectcurrencytype]) {
        selectcurrencytype = self.selectcurrencytype;
    }
    
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"UniqueKey\":\"%f\",\"MemberID\":%@,\"AccessToken\":\"%@\",\"PropertyType\":%d,\"SpaceType\":%d,"
                            @"\"PropertyPrice\":%@, "
                            @"\"CurrencyUnit\":\"%@\",\"PropertySize\":%@,\"SizeUnitType\":%d,"
                            @"\"BedroomCount\":%@,\"BathroomCount\":%@,\"Reasons\":%@,"
                            @"\"AddressUserInput\":%@,\"GoogleAddressPack\":%@}",
                            [[NSDate date] timeIntervalSince1970],
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            self.selectpropertytypelist.Position,
                            self.selectspacetypelist.Position, self.price,
                            selectcurrencytype, self.size, selectmeticindex,
                            self.numberofbedroom, self.numberofbathroom,
                            [self
                             nsDictionaryArrayToInputParametersString:reasoninputarray],
                            [self
                             nsDictionaryArrayToInputParametersString:addressuserinputarray],
                            [self nsDictionaryArrayToInputParametersString:
                             self.googleGeoCodeApiAddressArray],
                            nil];
    
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Admin.asmx/AgentListingAdd"];
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    
    
    
    [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  [[AgentListingModel shared] uploadAgentListingImageFromResponseObject:responseObject success:^(id responseObject) {
                      success(responseObject);
                  } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                      failure(operation, error,errorMessage,errorStatus,alert,ok);
                  }];
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}

-(void)uploadAgentListingImageFromResponseObject:(id)responseObject success:(successBlock)success
                                         failure:(failureBlock) failure{
    
    __block int uploadedPhotoCount = 0;
    __block int totalPhotoCount = 0;
    __block NSMutableArray *photoURLArray = [[NSMutableArray alloc]init];
    [self uploadReasonImageToServerFromResponseObject:responseObject withBlock:^(id response, NSError *error) {
        if (!error) {
            NSArray *reasonRequest = [NSArray arrayWithArray:response];
            NSArray *listImageRequest = [self uploadAgentListingChoseImageToServerFromResponseObject:responseObject];
            NSMutableArray *uploadRequest = [NSMutableArray arrayWithArray:reasonRequest];
            [uploadRequest addObjectsFromArray:listImageRequest];
            totalPhotoCount = (int)uploadRequest.count;
            for (NSURLRequest *request in uploadRequest) {
                AFURLSessionManager *manager = [[AFURLSessionManager alloc]
                                                initWithSessionConfiguration:
                                                [NSURLSessionConfiguration defaultSessionConfiguration]];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                NSProgress *progress = nil;
                [kAppDelegate updateCurrentHudStatus:JMOLocalizedString(@"ready_to_publish__uploading_photo", nil)];
                NSURLSessionUploadTask *uploadTask = [manager
                                                      uploadTaskWithStreamedRequest:request
                                                      progress:&progress
                                                      completionHandler:^(NSURLResponse *response,
                                                                          id responseObject,
                                                                          NSError *error) {
                                                          if (error) {
                                                              DDLogVerbose(@"completionHandler: %@", error);
                                                          }
                                                          NSDictionary *jsonResponse = nil;
                                                          if (!error && responseObject) {
                                                              NSString *res = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                              NSError *parseError = nil;
                                                              NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:res error:&parseError];
                                                              if (!parseError && xmlDictionary) {
                                                                  NSDictionary *rawDict = xmlDictionary[@"string"];
                                                                  if(rawDict){
                                                                      NSString *content = rawDict[@"text"];
                                                                      NSString *decoded = [content stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                                      NSData *data = [decoded dataUsingEncoding:NSUTF8StringEncoding];
                                                                      jsonResponse= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                      
                                                                      int errorCode = [jsonResponse[@"ErrorCode"]intValue ];
                                                                      if (errorCode != 0) {
                                                                          error =[[NSError alloc]init];
                                                                      }
                                                                      jsonResponse = jsonResponse[@"Content"];
                                                                  }
                                                              }
                                                              NSString *photoURL = jsonResponse[@"PhotoURL"];
                                                              if ([photoURL valid] && !error) {
                                                                  [photoURLArray addObject:[NSURL URLWithString:photoURL]];
                                                              }
                                                          }
                                                          
                                                          uploadedPhotoCount ++;
                                                          NSString *uploadingStatus = [NSString stringWithFormat:@"%@ %d/%d",JMOLocalizedString(@"ready_to_publish__uploading_photo", nil),uploadedPhotoCount,totalPhotoCount];
                                                          [kAppDelegate updateCurrentHudStatus:uploadingStatus];
                                                          DDLogDebug(@"uploadePhotoCount:%d",uploadedPhotoCount);
                                                          if (uploadedPhotoCount >= totalPhotoCount) {
                                                              DDLogDebug(@"upload succes, callAgentProfileGetAPISuccess");
                                                              [[MyAgentProfileModel shared]
                                                               callAgentProfileGetAPISuccess:
                                                               ^(AgentProfile *myAgentProfile) {
                                                                   [kAppDelegate updateCurrentHudStatus:JMOLocalizedString(@"invite_to_chat__success", nil)];
                                                                   [[AgentListingModel shared]resetAllAgentListingPartOnlyLocalData];
                                                                   success(myAgentProfile);
                                                                   
                                                               }
                                                               failure:^(AFHTTPRequestOperation *operation,
                                                                         NSError *error,
                                                                         NSString *errorMessage,
                                                                         RequestErrorStatus errorStatus, NSString *alert,
                                                                         NSString *ok) {
                                                                   failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                                                                   
                                                               }];
                                                          }
                                                      }];
                
                [uploadTask resume];
            }
        }
    }];
    
}


-(NSArray*)uploadAgentListingChoseImageToServerFromResponseObject:(id)responseObject{
    NSMutableArray *uploadRequest = [[NSMutableArray alloc]init];
    NSString *string = [responseObject objectForKey:@"d"];
    NSError *err = nil;
    self.HandleServerReturnString =
    [[HandleServerReturnString alloc] initWithString:string error:&err];
    if (![self isEmpty:[self.HandleServerReturnString.Content
                        objectForKey:@"AgentListingID"]]) {
        NSString *subString = [self.HandleServerReturnString.Content
                               objectForKey:@"AgentListingID"];
        DDLogVerbose(@"AgentListingID-->%@", subString);
        for (int i =0; i<self.compressFinishURLRequestImageArray.count; i++) {
            DDLogVerbose(@"callmultipartfileupload");
            NSDictionary *parameters = @{
                                         @"MemberID" : [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                                         @"AccessToken" : [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                                         @"ListingID" : subString,
                                         @"Position" : [NSString stringWithFormat:@"%d", i],
                                         @"FileType" : @"1",
                                         @"FileExt" : @"jpg"
                                         };
            
            
            NSString *string =
            [NSString stringWithFormat:@"%@%@", kServerAddress,
             @"/Admin.asmx/FileUpload"];
            NSData * imageData =self.compressFinishURLRequestImageArray[i];
            
            
            
            DDLogVerbose(@"imageData.length-->%lu",
                         (unsigned long)imageData.length);
            
            
            NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
                                            multipartFormRequestWithMethod:@"POST"
                                            URLString:string
                                            parameters:parameters
                                            constructingBodyWithBlock:^(id<AFMultipartFormData>
                                                                        formData) {
                                                [formData appendPartWithFileData:imageData
                                                                            name:@"FileExt"
                                                                        fileName:@"photo.jpg"
                                                                        mimeType:@"image/jpeg"];
                                            } error:nil];
            
            
            [uploadRequest addObject:request];
            
        }
    }
    return uploadRequest;
}
-(void)compressImageArraywithBlock:(CommonBlock)block{
    NSMutableArray *imageArray = [[NSMutableArray alloc]init];
    NSMutableArray *compressFinishURLRequestImageArray = [[NSMutableArray alloc]init];
    DDLogVerbose(@"self.chosenImages-->%@", [AgentListingModel shared].chosenImages);
    for (int i = 0; i < [[AgentListingModel shared].chosenImages count]; i++) {
        id object = [AgentListingModel shared].chosenImages[i];
        if ([object isKindOfClass:[PHAsset class]]){
            [RealUtility getImageBy:object size:RealUploadPhotoSize handler:^(UIImage *result, NSDictionary *info) {
                if (!isEmpty(result)) {
                    [imageArray addObject:result];
                }else{
                    NSError *error = [NSError errorWithDomain:kEmptyData code:404 userInfo:nil];
                        [CrashlyticsKit recordError:error withAdditionalUserInfo:info];
                }
            }];
        }else if([object isKindOfClass:[UIImage class]]){
            [imageArray addObject:object];
        }
    }
    
    for (int i =0; i<imageArray.count; i++) {
        DDLogVerbose(@"callmultipartfileupload");
        UIImage * beforeCompressImage=imageArray[i];
        NSData * imageData = [UIImage compressImageToNSData:beforeCompressImage limitedDataSize:600];
        DDLogVerbose(@"imageData.length-->%lu",
                     (unsigned long)imageData.length);
        [compressFinishURLRequestImageArray addObject:imageData];
    }
    
    self.compressFinishURLRequestImageArray=[compressFinishURLRequestImageArray copy];
    self.imageArrayCompressFinish=YES;
    block(self.compressFinishURLRequestImageArray,nil);
    
    
}

-(void)uploadReasonImageToServerFromResponseObject:(id)responseObject withBlock:(CommonBlock)block{
    //    __block int uploadedPhotoCount = 0;
    //    __block int totalPhotoCount = 0;
    NSMutableArray * uploadRequest =[[NSMutableArray alloc]init];
    NSString *string = [responseObject objectForKey:@"d"];
    NSError *err = nil;
    self.HandleServerReturnString =
    [[HandleServerReturnString alloc] initWithString:string error:&err];
    int totalCount = 0;
    __block int successCount =0;
    if (![self isEmpty:[self.HandleServerReturnString.Content
                        objectForKey:@"AgentListingID"]]) {
        NSString *subString = [self.HandleServerReturnString.Content
                               objectForKey:@"AgentListingID"];
        DDLogVerbose(@"AgentListingID-->%@", subString);
        
        DDLogVerbose(@"self.reasonImageArray-->%@", [AgentListingModel shared].reasonImageArray);
        
        for (NSNumber *langNumber in self.reasonDict.allKeys) {
            NSDictionary *langDict = self.reasonDict[langNumber];
            NSString *langIndex = [langNumber stringValue];
            for (NSNumber *reasonNumber in langDict.allKeys) {
                if (![reasonNumber isKindOfClass:[NSString class]]) {
                    NSDictionary *reasonDict = langDict[reasonNumber];
                    PHAsset *imageAsset = reasonDict[@"reasonImageAsset"];
                    if(imageAsset){
                        totalCount++;
                    }
                }
            }
        }
        if(totalCount ==0){
            block(@[],nil);
            return;
        }
        for (NSNumber *langNumber in self.reasonDict.allKeys) {
            NSDictionary *langDict = self.reasonDict[langNumber];
            NSString *langIndex = [langNumber stringValue];
            for (NSNumber *reasonNumber in langDict.allKeys) {
                if (![reasonNumber isKindOfClass:[NSString class]]) {
                    NSDictionary *reasonDict = langDict[reasonNumber];
                    int reasonPosition = [reasonNumber intValue];
                    PHAsset *imageAsset = reasonDict[@"reasonImageAsset"];
                    if (imageAsset) {
                        [RealUtility getImageBy:imageAsset size:RealUploadPhotoSize handler:^(UIImage *result, NSDictionary *info) {
                            if (result) {
                                successCount++;
                                NSDictionary *parameters = @{
                                                             @"MemberID" : [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                                                             @"AccessToken" : [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                                                             @"ListingID" : subString,
                                                             @"Position" : [NSString stringWithFormat:@"%d", reasonPosition+1],
                                                             @"FileType" : @"3",
                                                             @"FileExt" : @"jpg",
                                                             @"LanguageIndex" : langIndex
                                                             };
                                
                                
                                NSString *string =
                                [NSString stringWithFormat:@"%@%@", kServerAddress,
                                 @"/Admin.asmx/FileUpload"];
                                
                                NSData *imageData =  [UIImage compressImageToNSData:result limitedDataSize:400];
                                
                                
                                NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
                                                                multipartFormRequestWithMethod:@"POST"
                                                                URLString:string
                                                                parameters:parameters
                                                                constructingBodyWithBlock:^(id<AFMultipartFormData>
                                                                                            formData) {
                                                                    [formData appendPartWithFileData:imageData
                                                                                                name:@"FileExt"
                                                                                            fileName:@"photo.jpg"
                                                                                            mimeType:@"image/jpeg"];
                                                                } error:nil];
                                
                                [uploadRequest addObject:request];
                                if (successCount >= totalCount) {
                                    if (block) {
                                        block(uploadRequest,nil);
                                    }
                                }
                                
                            }
                        }];
                    }
                }
            }
        }
    }
}


- (void)geoCodeJsonApiFromAddress:(NSString *)address language:(NSString *)language success:(successBlock)success failure:(failureBlock) failure{
    
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    if ([[GetContryLocation shared] contryLocationISChina]){
        [self callChinaGoogleMapPlaceAPIGeocodeBySearchAddressUserInputSearchAddress:address language:language success:^(id responseObject) {
            success(responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                    NSString *errorMessage, RequestErrorStatus errorStatus,
                    NSString *alert, NSString *ok) {
            failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
            
            
        }];
        
        
        
        
    }else{
        NSDictionary *parameters = @{ @"address" : address, @"language":language ,@"key":kGoogleAPIKey};
        
        AFHTTPRequestOperationManager *manager =
        [self getAPiManager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager GET:@"https://maps.googleapis.com/maps/api/geocode/json"
          parameters:parameters
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                 
                 NSString *newStr =
                 [[NSString alloc] initWithData:operation.request.HTTPBody
                                       encoding:NSUTF8StringEncoding];
                 DDLogVerbose(@"operation.request.HTTPBody-->%@", newStr);
                 DDLogVerbose(@"operation.responseString-->%@",
                              operation.responseString);
                 
                 success(responseObject);
                 
             }
         
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                 NSString *newStr =
                 [[NSString alloc] initWithData:operation.request.HTTPBody
                                       encoding:NSUTF8StringEncoding];
                 DDLogVerbose(@"operation.request.HTTPBody-->%@", newStr);
                 DDLogVerbose(@"responseObject-->Server problem&&Error: %@", error);
             }];
        
    }
}
- (void)callGoogleLanguageDetectionLangToDetect:(NSString *)LangToDetect success:(successBlock)success failure:(failureBlock) failure{
    
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    if ([[GetContryLocation shared] contryLocationISChina]){
        
        NSString *urlLink =
        [NSString stringWithFormat:@"%@%@", kServerAddress,
         @"/Map.asmx/GoogleLanguageDetection"];
        NSString *parameters = [NSString
                                stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                                @"\"LangToDetect\":\"%@\"}",
                                [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                                [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                                LangToDetect, nil];
        
        AFHTTPRequestOperationManager *manager =
        [self getAPiManager];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        if (![self networkConnection]) {
            failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
            return;
        }
        DDLogVerbose(@"apiurlstring-->:%@", urlLink);
        DDLogVerbose(@"InputJson-->:%@", parameters);
        [manager POST:urlLink
           parameters:@{ @"InputJson" : parameters }
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  
                  NSString *dstring = [responseObject objectForKey:@"d"];
                  
                  if ([self checkAccessError:dstring]) {
                      failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                      
                  }
                  NSError *err = nil;
                  HandleServerReturnGoogleString* returnString =[[HandleServerReturnGoogleString alloc] initWithString:dstring error:&err];
                  NSString *   googleAutoCompleteString = [returnString.GoogleResult stringByReplacingOccurrencesOfString:@"\n"
                                                                                                               withString:@""];
                  NSData *data = [googleAutoCompleteString dataUsingEncoding:NSUTF8StringEncoding];
                  
                  
                  id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                  
                  
                 
                  
                  
                 
                  success(json);
                  
                  
                  
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  NSString *newStr =
                  [[NSString alloc] initWithData:operation.request.HTTPBody
                                        encoding:NSUTF8StringEncoding];
                  
                  DDLogError(@"operation.request.HTTPBody-->%@", newStr);
                  DDLogError(@"response-->Error: %@", error);
                  
              }];
    }
}

- (void)geoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLink:
(NSString *)urllink languageString:(NSString *)languageString placeId:(NSString *)placeId success:(successBlock)success
                                                                failure:(failureBlock) failure{
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    if ([[GetContryLocation shared] contryLocationISChina]){
        
        NSString *urlLink =
        [NSString stringWithFormat:@"%@%@", kServerAddress,
         @"/Map.asmx/GoogleMapPlaceAPIGeocodeByPlaceID"];
        NSString *parameters = [NSString
                                stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                                @"\"Language\":\"%@\",\"PlaceID\":\"%@\"}",
                                [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                                [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                                languageString,
                                placeId, nil];
        
        AFHTTPRequestOperationManager *manager =
        [self getAPiManager];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        if (![self networkConnection]) {
            failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
            return;
        }
        DDLogVerbose(@"apiurlstring-->:%@", urlLink);
        DDLogVerbose(@"InputJson-->:%@", parameters);
        [manager POST:urlLink
           parameters:@{ @"InputJson" : parameters }
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  
                  NSString *dstring = [responseObject objectForKey:@"d"];
                  
                  if ([self checkAccessError:dstring]) {
                      failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                      
                  }
                  NSError *err = nil;
                  HandleServerReturnGoogleString* returnString =[[HandleServerReturnGoogleString alloc] initWithString:dstring error:&err];
                  NSString *   googleAutoCompleteString = [returnString.GoogleResult stringByReplacingOccurrencesOfString:@"\n"
                                                                                                               withString:@""];
                  NSData *data = [googleAutoCompleteString dataUsingEncoding:NSUTF8StringEncoding];
                  
                  
                  id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                  
                  
                  NSArray *results = [json objectForKey:@"results"];
                  NSDictionary *resultsFirstObject = [results firstObject];
                  
                  if (isEmpty(resultsFirstObject)||isEmpty(languageString)) {
                      success(nil);
                      return ;
                  }
                  
                  NSDictionary *googleaddress = @{
                                                  @"GoogleAddressLang" : languageString,
                                                  @"GoogleAddress" : resultsFirstObject
                                                  };
                  
                  [self.googleGeoCodeApiAddressArray addObject:googleaddress];
                  success(nil);
                  
                  
                  
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  NSString *newStr =
                  [[NSString alloc] initWithData:operation.request.HTTPBody
                                        encoding:NSUTF8StringEncoding];
                  
                  DDLogError(@"operation.request.HTTPBody-->%@", newStr);
                  DDLogError(@"response-->Error: %@", error);
                  
              }];
    }else{
        
        
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        
        DDLogVerbose(@"apiurlstring-->:%@", urllink);
        DDLogVerbose(@"InputJson-->:%@", languageString);
        
        [manager POST:urllink
           parameters:languageString
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  NSString *newStr =
                  [[NSString alloc] initWithData:operation.request.HTTPBody
                                        encoding:NSUTF8StringEncoding];
                  
                  DDLogVerbose(@"operation.request.HTTPBody-->%@", newStr);
                  DDLogVerbose(@"response-->Success");
                  
                  NSArray *results = [responseObject objectForKey:@"results"];
                  NSDictionary *resultsFirstObject = [results firstObject];
                  if (isEmpty(resultsFirstObject)||isEmpty(languageString)) {
                       success(nil);
                      return ;
                  }
                  NSDictionary *googleaddress = @{
                                                  @"GoogleAddressLang" : languageString,
                                                  @"GoogleAddress" : resultsFirstObject
                                                  };
                  
                  [self.googleGeoCodeApiAddressArray addObject:googleaddress];
                  success(nil);
                  return;
                  
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  NSString *newStr =
                  [[NSString alloc] initWithData:operation.request.HTTPBody
                                        encoding:NSUTF8StringEncoding];
                  
                  DDLogError(@"operation.request.HTTPBody-->%@", newStr);
                  DDLogError(@"response-->Error: %@", error);
                  
              }];
        
    }
    
}
//// nsDictionaryArrayToInputParametersString
- (NSString *)nsDictionaryArrayToInputParametersString:(NSMutableArray *)array {
    NSString *ArrayjsonString;
    NSError *error;
    NSData *jsonData =
    [NSJSONSerialization dataWithJSONObject:array
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];
    
    if (!jsonData) {
        DDLogInfo(@"Got an error: %@", error);
    } else {
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        ArrayjsonString =
        [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    
    return ArrayjsonString;
}



- (void)callChinaGoogleMapPlaceAPIGeocodeBySearchAddressUserInputSearchAddress:(NSString *)userInputSearchAddress
                                                                      language:(NSString *)language
                                                                       success:(void (^)(id responseObject))success
                                                                       failure:(failureBlock) failure{
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Map.asmx/GoogleMapPlaceAPIGeocodeBySearchAddress"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"SearchAddress\":\"%@\",\"Language\":\"%@\"}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            userInputSearchAddress,
                            language, nil];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              NSError *err = nil;
              HandleServerReturnGoogleString* returnString =[[HandleServerReturnGoogleString alloc] initWithString:dstring error:&err];
              
              if (returnString.ErrorCode == 0) {
                  NSString * googleAutoCompleteString = returnString.GoogleResult;
                  
                  
                  googleAutoCompleteString = [googleAutoCompleteString stringByReplacingOccurrencesOfString:@"\n"
                                                                                                 withString:@""];
                  
                  
                  NSData *data = [googleAutoCompleteString dataUsingEncoding:NSUTF8StringEncoding];
                  id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                  
                  //    self.handleGoogleAutoCompleteJson =[[HandleGoogleAutoCompleteJson alloc] initWithString:googleAutoCompleteString error:&err];
                  
                  //   DDLogDebug(@"googleAutoCompleteJsonRow  %@",googleAutoCompleteJsonRow);
                  
                  success(json );
              }else{
                  failure(operation, [self genError:returnString.ErrorCode description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
    
    
    
}


- (BOOL)haveHistory{
    return self.street|| self.selectspacetypelist || self.selectspacetypelist || self.apt;
}
//// set delegate store posting data to nil
- (void)resetAllAgentListingPartOnlyLocalData {
    self.street = nil;
    self.apt = nil;
    self.numberofbedroom = nil;
    self.numberofbathroom = nil;
    self.price = nil;
    self.size = nil;
    self.followercount = nil;
    self.chosenImages = nil;
    self.selectcurrencytype = nil;
    self.selectmetric = nil;
    self.selectspacetypelist = nil;
    self.selectpropertytypelist = nil;
    self.googleapireturnaddress = nil;
    self.googleGeoCodeApiAddressArray = nil;
    self.addnewlanguageindex = nil;
    self.reasonDict = nil;
    self.addnewonereason = nil;
    self.addnewtworeason = nil;
    self.addnewthreereason = nil;
    self.totalSelectedImageCount =0;
    self.totatImageUploadCount = 0;
    self.coverPhotoScrollView = nil;
}

-(AgentProfile*)convertToPreviewAgentProfile{
    AgentProfile *myProfile = [[MyAgentProfileModel shared].myAgentProfile copy];
    AgentListing *previewListing = [[AgentListing alloc]init];
    previewListing.isPreview = YES;
    previewListing.BathroomCount =[self.numberofbathroom intValue];
    previewListing.BedroomCount = [self.numberofbedroom intValue];
    NSMutableArray *imageArray= [[NSMutableArray alloc]init];
    for (int i = 0; i < 2; i++) {
        id object =[AgentListingModel shared].chosenImages[i];
        if ([object isKindOfClass:[UIImage class]]) {
            [imageArray addObject:object];
        }else if ([object isKindOfClass:[PHAsset class]]){
            [RealUtility getImageBy:object size:CGSizeMake(500, 500) handler:^(UIImage *result, NSDictionary *info) {
                if (!isEmpty(result)) {
                    [imageArray addObject:result];
                }else{
                    NSError *error = [NSError errorWithDomain:kEmptyData code:404 userInfo:nil];
                    [CrashlyticsKit recordError:error withAdditionalUserInfo:info];
                }
            }];
        }
    }
    previewListing.PropertyType = self.selectpropertytypelist.Position;
    previewListing.SpaceType = self.selectspacetypelist.Position;
    previewListing.previewPhoto = [NSArray arrayWithArray:imageArray];
    previewListing.PropertyPrice=[self.price intValue];
    previewListing.PropertySize =[self.size intValue];
    
    NSMutableArray *reasonArray = [[NSMutableArray alloc]init];
    for (NSNumber *langIndex in self.reasonDict.allKeys) {
        NSDictionary *langDict =self.reasonDict [langIndex];
        NSDictionary *reasonOneDict = langDict[@(0)];
        NSDictionary *reasonTwoDict = langDict[@(1)];
        NSDictionary *reasonThreeDict = langDict[@(2)];
        NSString *reasonOneString = nil;
        NSString *reasonTwoString = nil;
        NSString *reasonThreeString = nil;
        PHAsset *assetOne = nil;
        PHAsset *assetTwo = nil;
        PHAsset *assetThree = nil;
        NSNumber *slogaIndexNumber = langDict[@"sloganIndex"];
        
        if(reasonOneDict){
            reasonOneString = reasonOneDict[@"reasonString"];
            assetOne = reasonOneDict[@"reasonImageAsset"];
        }
        
        if(reasonTwoDict){
            reasonTwoString = reasonTwoDict[@"reasonString"];
            assetTwo = reasonTwoDict[@"reasonImageAsset"] ;
        }
        
        if(reasonThreeDict){
            reasonThreeString = reasonThreeDict[@"reasonString"];
            assetThree = reasonThreeDict[@"reasonImageAsset"];
        }
        Reasons *reason =[[Reasons alloc]init];
        reason.LanguageIndex = [langIndex intValue];
        reason.Reason1 =reasonOneString;
        reason.Reason2 = reasonTwoString;
        reason.Reason3 = reasonThreeString;
        reason.asset1 = assetOne;
        reason.asset2 = assetTwo;
        reason.asset3 = assetThree;
        reason.SloganIndex = [slogaIndexNumber intValue];
        [reasonArray addObject:reason];
    }
    previewListing.Reasons = [reasonArray mutableCopy];
    previewListing.CurrencyUnit = self.selectcurrencytype;
    previewListing.AgentListingID = 100;
    previewListing.ListingFollowerCount = 0;
    previewListing.SizeUnitType = self.selectmetric.Index;
    NSMutableArray<GoogleAddress> *googleAddressArray =[[NSMutableArray<GoogleAddress> alloc ]init];
    for (NSDictionary *dict in self.googleGeoCodeApiAddressArray) {
        NSDictionary *googleAddress = dict[@"GoogleAddress"];
        NSArray *addressComponents = googleAddress[@"address_components"];
        NSDictionary *firstComponent = [addressComponents firstObject];
        NSString *name = firstComponent[@"long_name"];
        NSString *formatedAddress = googleAddress[@"formatted_address"];
        NSString *placeId = googleAddress[@"place_id"];
        NSString *lang = dict[@"GoogleAddressLang"];
        GoogleAddress *tempAddress = [[GoogleAddress alloc]init];
        tempAddress.Name = name;
        tempAddress.FormattedAddress = formatedAddress;
        tempAddress.PlaceID = placeId;
        tempAddress.Lang = lang;
        [googleAddressArray addObject:tempAddress];
    }
    previewListing.GoogleAddress = googleAddressArray;
    previewListing.PropertyPriceFormattedForRoman =[RealUtility abbreviateNumber:[self.price doubleValue]];
    myProfile.AgentListing = previewListing;
    myProfile.status = AgentProfileStatusPreview;
    return myProfile;
}

-(void)removeReasonDictByLangIndex:(NSNumber*)langIndex{
    if (!isEmpty(langIndex)) {
        [self.reasonDict removeObjectForKey:langIndex];
    }
    
}

- (NSNumber*)firstReasonLangIndex{
    return self.reasonDict.allKeys.firstObject;
}

- (BOOL) canAddMoreLanguage{
    return self.reasonDict.allKeys.count < kMaxReasonLanugageCount;
}
@end