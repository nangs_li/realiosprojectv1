//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
#import <CoreSpotlight/CoreSpotlight.h>
@interface NewsFeedAgentprofilesModel : BaseModel
@property(strong, nonatomic) NSMutableArray<AgentProfile>* newsfeedAgentProfiles;
@property(strong, nonatomic) NSMutableArray<AgentProfile>* pendingNewsfeedAgentProfiles;
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(strong, nonatomic) SearchAgentProfile *newsfeedAgentProfilesHaveTotalCount;
@property(strong, nonatomic) SearchAgentProfile *newsfeedAgentRetrieveProfilesHaveTotalCount;
@property(assign, nonatomic) int Page;
@property(nonatomic, strong) NSTimer *getNewsFeedTimer;
+ (NewsFeedAgentprofilesModel *)shared ;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 
 */
-(void)callNewsFeedApisuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                      failure:(failureBlock ) failure;







-(void)callNewsFeedApiPage:(int )Page   success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                      failure:(failureBlock ) failure;
-(void)callNewsFeedFromBackGround;
-(void)callNewsFeedApiTimer;
-(void)checkbadgeCountToHideOrShowUpdateNewsFeedView;
@end
