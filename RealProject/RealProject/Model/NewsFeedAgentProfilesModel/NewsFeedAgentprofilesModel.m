//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "NewsFeedAgentprofilesModel.h"

#import "HandleServerReturnString.h"
#import "DBNewsFeedArray.h"
#import "SDWebImagePrefetcher.h"
#import "DBNewsFeedLastReadDate.h"
@implementation NewsFeedAgentprofilesModel

static NewsFeedAgentprofilesModel *sharedNewsFeedAgentprofilesModel;




+ (NewsFeedAgentprofilesModel *)shared {
    @synchronized(self) {
        if (!sharedNewsFeedAgentprofilesModel) {
            sharedNewsFeedAgentprofilesModel = [[NewsFeedAgentprofilesModel alloc] init];
            [NewsFeedAgentprofilesModel shared].Page = 1;
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedNewsFeedAgentprofilesModel;
    }
}
-(void)callNewsFeedApiTimer{
    
    [self.getNewsFeedTimer invalidate];
    
    self.getNewsFeedTimer =
    [NSTimer scheduledTimerWithTimeInterval:kNewFeedUpdateTimerInterval
                                     target:self
                                   selector:@selector(callNewsFeedFromBackGround)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)callNewsFeedFromBackGround{
    
    
    [[NewsFeedAgentprofilesModel shared]callNewsFeedApisuccess:^(NSMutableArray<AgentProfile> *AgentProfiles) {
        //        [NewsFeedAgentprofilesModel shared].Page=1;
        self.pendingNewsfeedAgentProfiles = [NSMutableArray<AgentProfile> arrayWithArray:AgentProfiles];
        
        
        NSDate *newFeedLastUpdateTime = [NSDate date];/*TO - DO */
        
        if ([[[[DBNewsFeedLastReadDate query] whereWithFormat:@" MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]
              fetch]count]==0) {
            DBNewsFeedLastReadDate * dbNewsFeedLastReadDate= [DBNewsFeedLastReadDate new];
            dbNewsFeedLastReadDate.MemberID =[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ;
            dbNewsFeedLastReadDate.QBID =[LoginBySocialNetworkModel shared].myLoginInfo.QBID ;
            AgentProfile * agentProfile =[[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles firstObject];
            dbNewsFeedLastReadDate.LastReadDate = [agentProfile.AgentListing  getCreationDate];
            [dbNewsFeedLastReadDate commit];
        }
        
        
        DBResultSet * r=     [[[DBNewsFeedLastReadDate query] whereWithFormat:@" MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]fetch];
        
        for (DBNewsFeedLastReadDate * dBNewsFeedLastReadDate in r) {
            newFeedLastUpdateTime=  dBNewsFeedLastReadDate.LastReadDate;
        }
        
        int unreadCount = 0;
        for (AgentProfile *pendingProfile in self.pendingNewsfeedAgentProfiles) {
            
            NSDate *pendingCreattionDate=  [pendingProfile.AgentListing getCreationDate];
            if ([pendingCreattionDate compare:newFeedLastUpdateTime] == NSOrderedDescending) {
                unreadCount ++;
            }
        }
        
        if (unreadCount > 0) {
          
            [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
              objectAtIndex:1] setBadgeValue:[NSString stringWithFormat:@"%d",unreadCount]];
           
        }else{
            [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
              objectAtIndex:1] setBadgeValue:nil];
           
        }
        [self checkbadgeCountToHideOrShowUpdateNewsFeedView];
        
        NSMutableArray *urlArrays = [[NSMutableArray alloc]init];
        for (AgentProfile *agentProfile in [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles  copy]) {
            if ([RealUtility isValid:agentProfile.AgentPhotoURL]) {
                [urlArrays addObject:[NSURL URLWithString:agentProfile.AgentPhotoURL]];
            }
            for (Photos *photos in agentProfile.AgentListing.Photos) {
                if ([RealUtility isValid:photos.URL]) {
                    [urlArrays addObject:[NSURL URLWithString:photos.URL]];
                }
            }
            
            for (Reasons *reasons in agentProfile.AgentListing.Reasons) {
                if ([RealUtility isValid:reasons.MediaURL1]) {
                    [urlArrays addObject:[NSURL URLWithString:reasons.MediaURL1]];
                }
                if ([RealUtility isValid:reasons.MediaURL2]) {
                    [urlArrays addObject:[NSURL URLWithString:reasons.MediaURL2]];
                }
                if ([RealUtility isValid:reasons.MediaURL3]) {
                    [urlArrays addObject:[NSURL URLWithString:reasons.MediaURL3]];
                }
            }
        }
                
    }failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
        
    }];
}
- (void)callNewsFeedApisuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                       failure:(failureBlock) failure{
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/NewsFeed"];
    
    NSString *parameters =
    [NSString stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
     @"\"UniqueKey\":\"%f\",\"Page\":%d,\"LanguageIndex\":%d}",
     [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
     [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
     [[NSDate date] timeIntervalSince1970], 1,[SystemSettingModel shared]
     .selectSystemLanguageList.Index , nil];
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urllink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  self.newsfeedAgentProfilesHaveTotalCount =
                  [[SearchAgentProfile alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  
                  self.pendingNewsfeedAgentProfiles =
                  self.newsfeedAgentProfilesHaveTotalCount.AgentProfiles;
                  DDLogVerbose(@"self.newsfeedAgentProfiles-->%@",
                               self.newsfeedAgentProfiles);
                  
                  if ([self.pendingNewsfeedAgentProfiles valid]) {
                      if ([self isEmpty:self.newsfeedAgentProfiles]) {
                          self.newsfeedAgentProfiles = [NSMutableArray<AgentProfile> arrayWithArray:self.pendingNewsfeedAgentProfiles];
                      }
                  }
                  //// save DBNewsFeedArray into database
                  [[[[DBNewsFeedArray query]
                     whereWithFormat:@" MemberID = %@",
                     [LoginBySocialNetworkModel shared]
                     .myLoginInfo.MemberID] fetch] removeAll];
                  DBNewsFeedArray *dbnewsfeed = [DBNewsFeedArray new];
                  dbnewsfeed.Date = [NSDate date];
                  
                  dbnewsfeed.NewsFeedArray = [NSKeyedArchiver
                                              archivedDataWithRootObject:[self.pendingNewsfeedAgentProfiles copy]];
                  dbnewsfeed.MemberID =
                  [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
                  [dbnewsfeed commit];
                  
                  if ([self.pendingNewsfeedAgentProfiles count] == 0) {
                      
                      failure(operation, nil,@"Not Record Find",NotRecordFind,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  }else{
                      
                      
                      
                      for (AgentProfile * agentProfile in self.newsfeedAgentProfiles) {
                          //   [agentProfile deleteSpotLightSearchNewFeedItem];
                          [agentProfile createSpotLightSearchNewFeedItem];
                          
                      }
                      
                      
                      
                  }
                  
                  success(self.pendingNewsfeedAgentProfiles);
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}


-(void)callNewsFeedApiPage:(int )Page success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success failure:(failureBlock) failure{
    
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/NewsFeed"];
    
    NSString *parameters =
    [NSString stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
     @"\"UniqueKey\":\"%f\",\"Page\":%d,\"LanguageIndex\":%d}",
     [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
     [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
     [[NSDate date] timeIntervalSince1970], Page,[SystemSettingModel shared]
     .selectSystemLanguageList.Index, nil];
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urllink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  self.newsfeedAgentRetrieveProfilesHaveTotalCount =
                  [[SearchAgentProfile alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  if ([self.newsfeedAgentRetrieveProfilesHaveTotalCount.AgentProfiles valid]) {
                      [self.newsfeedAgentProfiles addObjectsFromArray:self.newsfeedAgentRetrieveProfilesHaveTotalCount.AgentProfiles];
                      [self cleanDuplicate];
                  }
                  //// save DBNewsFeedArray into database
                  [[[[DBNewsFeedArray query]
                     whereWithFormat:@" MemberID = %@",
                     [LoginBySocialNetworkModel shared]
                     .myLoginInfo.MemberID] fetch] removeAll];
                  DBNewsFeedArray *dbnewsfeed = [DBNewsFeedArray new];
                  dbnewsfeed.Date = [NSDate date];
                  
                  dbnewsfeed.NewsFeedArray = [NSKeyedArchiver
                                              archivedDataWithRootObject:[self
                                                                          .newsfeedAgentProfiles copy]];
                  dbnewsfeed.MemberID =
                  [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
                  [dbnewsfeed commit];
                  
                  if ([self.newsfeedAgentProfiles count] == 0) {
                      
                      failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                              JMOLocalizedString(@"alert_ok", nil));
                  }else{
                      
                      
                      for (AgentProfile * agentProfile in self.newsfeedAgentProfiles) {
                          //  [agentProfile deleteSpotLightSearchNewFeedItem];
                          [agentProfile createSpotLightSearchNewFeedItem];
                          
                      }

                  }
                  success(self.newsfeedAgentProfiles);
              }else{
                  failure(operation, [[NSError alloc]init],self.HandleServerReturnString.ErrorMsg ,error,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
    
    
}

- (void)cleanDuplicate{
    NSMutableArray<AgentProfile> *unqiueArray = [[NSMutableArray<AgentProfile> alloc]init];
    for (AgentProfile *profile in [self.newsfeedAgentProfiles copy]) {
        if (![unqiueArray containsObject:profile]) {
            [unqiueArray addObject:profile];
        }
    }
    self.newsfeedAgentProfiles = [NSMutableArray<AgentProfile> arrayWithArray:unqiueArray];
}
-(void)checkbadgeCountToHideOrShowUpdateNewsFeedView{
   UITabBarItem  *newsfeed= [[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
                            objectAtIndex:1];
    if ( isEmpty(newsfeed.badgeValue)) {
         [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHideUpdateNewsFeedView object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShowUpdateNewsFeedView object:nil];

        
    }
    
                            
}
@end