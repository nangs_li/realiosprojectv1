//
//  REPhonebookManager.h
//  productionreal2
//
//  Created by Derek Cheung on 31/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

// Framework
#import "APAddressBook.h"

// Model
#import "REContact.h"

// Constant
typedef NS_ENUM(NSUInteger, REPhonebookAccess)
{
    REPhonebookAccessUnknown = 0,
    REPhonebookAccessGranted = 1,
    REPhonebookAccessDenied  = 2
};

typedef void(^REPhonebookRequestAccessBlock)(BOOL granted);

@interface REPhonebookManager : NSObject

//Usage:
//
//[[REPhonebookManager sharedManager] requestAccess:^(BOOL granted) {
//    
//    if (granted) {
//        
//        NSArray *contacts = [[REPhonebookManager sharedManager] contacts];
//        NSData *contactsData = manager.contactsData;
//        DDLogDebug(@"Get contacts succeed");
//    }
//}];

@property (nonatomic, strong, readonly) NSArray<REContact *> * _Nullable contacts;
@property (nonatomic, strong, readonly) NSData * _Nullable contactsData;

+ (nonnull instancetype)sharedManager;

- (void)requestAccess:(nullable REPhonebookRequestAccessBlock)completionBlock;

- (REPhonebookAccess)access;

+ (nonnull UIAlertController *)noPermissionAlertWithCancelBlock:(void (^ __nullable)(UIAlertAction *action))cancelBlock;

@end
