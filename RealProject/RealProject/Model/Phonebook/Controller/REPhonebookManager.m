//
//  REPhonebookManager.m
//  productionreal2
//
//  Created by Derek Cheung on 31/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REPhonebookManager.h"

// Framework
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"

// Models
#import "APContact.h"
#import "GetContryLocation.h"

typedef void(^REPhonebookLoadedContactsBlock)();

@interface REPhonebookManager ()

@property (nonatomic, strong) APAddressBook *addressBook;
@property (nonatomic, strong) NSArray<REContact *> *contacts;
@property (nonatomic, strong) NSDictionary *dictForData;

@end

@implementation REPhonebookManager

#pragma mark - Singleton

+ (instancetype)sharedManager {
    
    static REPhonebookManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

#pragma mark - Initialization

- (instancetype)init {
    
    if (self = [super init]) {
        
        _addressBook = [[APAddressBook alloc] init];
        _addressBook.fieldsMask = APContactFieldDefault;
        _addressBook.filterBlock = ^BOOL(APContact *contact) {
            return contact.phones.count > 0;
        };
        _addressBook.sortDescriptors = @[
                                         [NSSortDescriptor sortDescriptorWithKey:@"name.compositeName" ascending:YES]
                                         ];
    }
    
    return self;
}

#pragma mark - Pubilc

- (void)requestAccess:(nullable REPhonebookRequestAccessBlock)completionBlock {
    
    __weak typeof(self) weakSelf = self;
    [_addressBook requestAccess:^(BOOL granted, NSError * _Nullable error) {
        
        if (granted && weakSelf.contacts == nil) {
            
            [weakSelf loadContactsCompletion:^{
                
                if (completionBlock) {
                    completionBlock(granted);
                }
            }];
            
        } else {
            
            if (completionBlock) {
                completionBlock(granted);
            }
        }
    }];
}

- (REPhonebookAccess)access {
    
    REPhonebookAccess access;
    
    switch ([APAddressBook access]) {
            
        case APAddressBookAccessGranted:
            access = REPhonebookAccessGranted;
            break;
            
        case APAddressBookAccessDenied:
            access = REPhonebookAccessDenied;
            break;
            
        default:
            access = REPhonebookAccessUnknown;
            break;
    }
    
    return access;
}

#pragma mark - Helpers

- (void)loadContactsCompletion:(REPhonebookLoadedContactsBlock)block {
    
    __weak typeof(self) weakSelf = self;
    [_addressBook loadContacts:^(NSArray<APContact *> * _Nullable contacts, NSError * _Nullable error) {
        
        NSMutableArray<REContact *> *contactArray = [NSMutableArray new];
        NSMutableArray *contactsToTxtFile = [NSMutableArray new];
        
        for (APContact *contact in contacts) {
            
            NSString *name = contact.name.compositeName;
            
            for (int i =0; i <contact.phones.count; i++) {
                
                APPhone *phone = contact.phones[i];
                APEmail *email = contact.emails[i];
                NSString *emailAddress = email.address;
                
                if (![emailAddress valid]) {
                    emailAddress = @"";
                }
                
                NSString *originalNumber = phone.number;
                
                REContact *temp = [REPhonebookManager contactWithName:name rawInput:originalNumber];
                temp.email = emailAddress;
                if (temp) {
                    
                    if (![temp.name valid]) {
                        temp.name = @"";
                    }
                    
                    [contactArray addObject:temp];
                    
                    NSDictionary *tempDict = @{
                                               @"cc": temp.countryCode,
                                               @"pn": temp.phoneNumber,
                                               @"nm": temp.name,
                                               @"em": emailAddress
                                               };
                    [contactsToTxtFile addObject:tempDict];
                    
                    
                }
            }
        }
        weakSelf.contacts = [NSArray arrayWithArray:contactArray];
        
        if ([contactsToTxtFile count] > 0) {
            
            weakSelf.dictForData = @{
                                     @"contactList": contactsToTxtFile
                                     };
            
            [weakSelf writeToData];
        }
        
        if (block) {
            
            block();
        }
    }];
}

+ (REContact *)contactWithName:(NSString *)name rawInput:(NSString *)rawInput {
    
    REContact *temp = [REContact new];
    temp.name = name;
    temp.originalNumber = rawInput;
    
    NSString *region = [[GetContryLocation shared] getCountryNameFromSimCard];
    NSError *error;
    
    NBPhoneNumber *pn = [[NBPhoneNumberUtil sharedInstance] parse:rawInput defaultRegion:region error:&error];
    
    if (!error) {
        
        temp.countryCode = [NSString stringWithFormat:@"+%@", [pn.countryCode stringValue]];
        temp.phoneNumber = [pn.nationalNumber stringValue];
        
    } else {
        
        temp = nil;
    }
    
    return temp;
}

- (void)writeToData {
    
    if (_dictForData) {
        
        NSString *stringToWrite = [REPhonebookManager jsonStringWithString:_dictForData];
        NSString *bom = @"\357\273\277";
        stringToWrite = [NSString stringWithFormat:@"%@%@",bom,stringToWrite];
        _contactsData = [stringToWrite dataUsingEncoding:NSUTF8StringEncoding];
    }
}

+ (NSString *)jsonStringWithString:(NSDictionary *)input {
    
    NSError *error;
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:input options:0 error:&error];
    
    if (error) {
        DDLogError(@"jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        jsonString = @"{}";
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return jsonString;
}

+ (nonnull UIAlertController *)noPermissionAlertWithCancelBlock:(void (^ __nullable)(UIAlertAction *action))cancelBlock {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:JMOLocalizedString(@"phonebook__no_permission_message", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *settingAction = [UIAlertAction actionWithTitle:JMOLocalizedString(@"phonebook__no_permission_redirect_button", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              
                                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                          }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:JMOLocalizedString(@"common__cancel", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:cancelBlock];
    [alert addAction:cancelAction];
    [alert addAction:settingAction];
    
    return alert;
}

@end
