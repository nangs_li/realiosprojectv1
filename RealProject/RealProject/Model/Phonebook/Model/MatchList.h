//
//  MatchList.h
//  productionreal2
//
//  Created by Li Ken on 1/6/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "REJoinedAgentContact.h"
@interface MatchList : JSONModel

@property(strong, nonatomic) NSNumber* TotalCount;
@property(strong, nonatomic) NSNumber* RecordCount;
@property(strong, nonatomic) NSMutableArray<REJoinedAgentContact>* MatchList;

@end
