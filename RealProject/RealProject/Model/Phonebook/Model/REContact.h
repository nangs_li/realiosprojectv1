//
//  REContact.h
//  productionreal2
//
//  Created by Derek Cheung on 31/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"

@interface REContact : JSONModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *originalNumber;

- (NSString *)phone;

+ (NSMutableDictionary *)invitedContactDict;

+ (void)saveInvitedContactDict:(NSDictionary*)invitedContactDict;

+ (NSMutableDictionary *)boardcastDict;

+ (void)saveBoardcastDict:(NSDictionary*)boardcastict;

@end
