//
//  REContact.m
//  productionreal2
//
//  Created by Derek Cheung on 31/5/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REContact.h"
#import "LoginBySocialNetworkModel.h"

@implementation REContact

#pragma mark - Override

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    
    return YES;
}

- (NSString *)phone {
    return [NSString stringWithFormat:@"%@ %@",self.countryCode,self.phoneNumber];
}

- (BOOL)isEqual:(id)object {
    BOOL equal = YES;
    
    if ([object isKindOfClass:[self class]]) {
        REContact *contact = object;
        equal = [contact.countryCode isEqual:self.countryCode] && [contact.phoneNumber isEqual:self.phoneNumber];
    } else {
        equal = NO;
    }
    
    return equal;
}

- (NSUInteger)hash {
    return [self.countryCode hash] ^ [self.phoneNumber hash];
}

- (NSString *) userDefaultKey {
    NSString *key = [NSString stringWithFormat:@"%@%@",self.countryCode, self.phoneNumber];
    
    return key;
}

+ (NSMutableDictionary *)invitedContactDict {
    NSMutableDictionary *invitedContactDict = [[NSMutableDictionary alloc]init];
    NSNumber *memberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    NSData *savedContactData = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"InvitedContact_%@",memberID]];
    if (savedContactData) {
        NSDictionary *savedContactDict =  (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:savedContactData];;
        invitedContactDict = [savedContactDict mutableCopy];
    }
    
    return invitedContactDict;
}

+ (void)saveInvitedContactDict:(NSDictionary*)invitedContactDict {
    NSData *savedContactData = [NSKeyedArchiver archivedDataWithRootObject:invitedContactDict];
    NSNumber *memberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    [[NSUserDefaults standardUserDefaults]setObject:savedContactData forKey:[NSString stringWithFormat:@"InvitedContact_%@",memberID]];
}

+ (NSMutableDictionary *)boardcastDict {
    NSMutableDictionary *boardcastDict = [[NSMutableDictionary alloc]init];
    NSNumber *memberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    NSData *savedContactData = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"Boardcast_%@",memberID]];
    if (savedContactData) {
        NSDictionary *savedBoardcastDict =  (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:savedContactData];;
        boardcastDict = [savedBoardcastDict mutableCopy];
    }
    
    return boardcastDict;
}

+ (void)saveBoardcastDict:(NSDictionary*)boardcastDict {
    NSData *boardcastData = [NSKeyedArchiver archivedDataWithRootObject:boardcastDict];
    NSNumber *memberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    [[NSUserDefaults standardUserDefaults]setObject:boardcastData forKey:[NSString stringWithFormat:@"Boardcast_%@",memberID]];
}

@end
