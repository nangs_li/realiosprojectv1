//
//  REJoinedAgentContact.h
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"
@protocol REJoinedAgentContact

@end

@interface REJoinedAgentContact : JSONModel

@property(strong, nonatomic) NSNumber *MemberID;
@property(strong, nonatomic) NSString *Name;
@property(strong, nonatomic) NSString *PhotoURL;
@property(strong, nonatomic) NSString *Location;
@property(strong, nonatomic) NSString *CountryCode;
@property(strong, nonatomic) NSString *PhoneNumber;
@property(strong, nonatomic) NSString *ListingID;
@property (nonatomic,assign) BOOL IsFollowing;

- (BOOL)didFollow;

@end
