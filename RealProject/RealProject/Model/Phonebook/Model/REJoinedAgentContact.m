//
//  REJoinedAgentContact.m
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "REJoinedAgentContact.h"

@implementation REJoinedAgentContact

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    
    return YES;
}

- (BOOL)didFollow {
    NSNumber *isFollowingInDB = [AgentProfile getIsFollowingMemberID:self.MemberID];
    return isFollowingInDB ? [isFollowingInDB boolValue] : self.IsFollowing;
}


@end
