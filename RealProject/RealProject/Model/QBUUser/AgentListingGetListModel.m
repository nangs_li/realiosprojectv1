//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AgentListingGetListModel.h"
#import "SearchAgentListing.h"
#import "HandleServerReturnString.h"

@implementation AgentListingGetListModel

static AgentListingGetListModel *sharedAgentListingGetListModel;




+ (AgentListingGetListModel *)shared {
    @synchronized(self) {
        if (!sharedAgentListingGetListModel) {
            sharedAgentListingGetListModel = [[AgentListingGetListModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedAgentListingGetListModel;
    }
}


-(void)callAgentListingGetListModelSuccess:(void (^)(NSMutableArray<AgentListing>* AgentListings))success
                                   failure:(failureBlock ) failure{
       NSString *urllink = [NSString
                        stringWithFormat:@"%@%@", kServerAddress,
                        @"/Operation.asmx/AgentListingGetList"];
    
    NSString *parameters =
    [NSString stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"ListingIDlist\":%@}",
     [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
       [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,[self nsDictionaryArrayToInputParametersString:self.listingIDListArray], nil];
    
    
  AFHTTPRequestOperationManager *manager =
      [self getAPiManager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
       failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      return;
  }

    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urllink
      parameters:@{ @"InputJson" : parameters }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

          NSString *dstring = [responseObject objectForKey:@"d"];
          if ([self checkAccessError:dstring]) {
              failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              
          }
       //   DDLogDebug(@"serverreturnstring-->%@", dstring);
          
          NSError *err = nil;
          self.HandleServerReturnString = [[HandleServerReturnString alloc] initWithString:dstring error:&err];
          
          int error = self.HandleServerReturnString.ErrorCode;
          if (error == 0) {
             self.agentListingGetListArrayHaveTotalCount =
              [[SearchAgentListing alloc]
               initwithNSDictionary:self.HandleServerReturnString.
               Content:error];
              
             self.agentListingGetListArray = self.agentListingGetListArrayHaveTotalCount.AgentListings;
              DDLogVerbose(@"self.agentListingGetListArray-->%@",self.agentListingGetListArray);
              success(self.agentListingGetListArray);
          }else{
              
              failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                      JMOLocalizedString(@"alert_ok", nil));
          }
          

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *newStr =
            [[NSString alloc] initWithData:operation.request.HTTPBody
                                  encoding:NSUTF8StringEncoding];

        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
        DDLogError(@"response-->Error: %@", error);
       failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
      }];
}

//// nsDictionaryArrayToInputParametersString
- (NSString *)nsDictionaryArrayToInputParametersString:(NSMutableArray *)array {
    NSString *ArrayjsonString;
    NSError *error;
    NSData *jsonData =
    [NSJSONSerialization dataWithJSONObject:array
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];
    
    if (!jsonData) {
        DDLogInfo(@"Got an error: %@", error);
    } else {
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        ArrayjsonString =
        [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    
    return ArrayjsonString;
}

-(AgentListing*)getAgentListByID:(int)listingId{
    AgentListing *listing = [self getCachedAgentListing:listingId];
    if (!listing) {
        for (AgentListing * agentListing in self.agentListingGetListArray) {
            if (agentListing.AgentListingID == listingId) {
                listing = agentListing;
                [self cacheAgentList:listing];
            }
        }
    }
    return listing;
}

-(void)cacheAgentList:(AgentListing*)listing{
    if (!self.cachedAgentListDict) {
        self.cachedAgentListDict = [[NSMutableDictionary alloc]init];
    }
    
    [self.cachedAgentListDict setObject:listing forKey:@(listing.AgentListingID)];
}

-(AgentListing*)getCachedAgentListing:(int)listingID{
   return self.cachedAgentListDict[@(listingID)];
}
@end