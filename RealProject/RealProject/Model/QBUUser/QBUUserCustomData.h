//
//  AgentLanguage.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentLanguage_h
//#define abc_AgentLanguage_h

//#endif
#import "JSONModel.h"
@protocol QBUUserCustomData
@end

@interface QBUUserCustomData : JSONModel
@property(strong, nonatomic) NSString *mypersonalphotourl;
@property(assign, nonatomic) BOOL chatroomshowlastseen;
@property(assign, nonatomic) BOOL chatroomshowreadreceipts;
@property(strong, nonatomic) NSString *agentProfile;
@property(strong, nonatomic) NSString *agentListing;
@property(strong, nonatomic) NSString *memberID;
@property(strong, nonatomic) NSString *agentListingID;
@property(strong, nonatomic) NSString* photoURL;
@property(strong, nonatomic) NSString* chatVersion;
@property(strong, nonatomic) NSString* lastUpdateTimeStamp;
@property(strong, nonatomic) NSString* mute;
@property(strong, nonatomic) NSString* block;

-(void)checkCustomDataValidate:(NSString*)customData;
/*
"agentProfile":	 "... Agent Profile JSON String...",
"agentListing":	 "... Agent Listing JSON String...",
"agentProfileID": "... Agent Profile ID...",
"agentListingID": "...Agent Listing ID...",
"photoURL": "...Member Photo URL...",
"chatVersion": "...Chat Version Code..."
 */
@end

//@implementation AgentLanguage

//@end