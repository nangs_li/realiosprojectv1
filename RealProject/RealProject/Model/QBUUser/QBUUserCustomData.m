//
//  AgentLanguage.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QBUUserCustomData.h"

@implementation QBUUserCustomData
+ (BOOL)propertyIsOptional:(NSString*)propertyName {
  return YES;
}
-(void)checkCustomDataValidate:(NSString*)customData{
    NSData *data = [customData dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    DDLogDebug(@"json success%@", json);
    if ([[AppDelegate getAppDelegate]isEmpty:[json objectForKey:@"chatroomshowlastseen"]]) {
        self.chatroomshowlastseen=YES;
    }
    if ([[AppDelegate getAppDelegate]isEmpty:[json objectForKey:@"chatroomshowreadreceipts"]]) {
         self.chatroomshowreadreceipts=YES;
    }
}
@end