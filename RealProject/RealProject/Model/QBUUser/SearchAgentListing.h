//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import "JSONModel.h"
#import "AgentListing.h"

@interface SearchAgentListing : JSONModel
@property(strong, nonatomic) NSNumber* TotalCount;
@property(strong, nonatomic) NSNumber* RecordCount;
@property(strong, nonatomic) NSArray* FullResultSet;
@property(strong, nonatomic) NSMutableArray<AgentListing>* AgentListings;

- (SearchAgentListing*)initwithjsonstring:(NSString*)content:(int)error;
- (SearchAgentListing*)initwithNSDictionary:(NSDictionary*)content:(int)error;

@end
