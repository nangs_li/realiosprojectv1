//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SearchAgentListing.h"
@implementation SearchAgentListing
static SearchAgentListing *searchAgentListing;
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

- (SearchAgentListing *)initwithjsonstring:(NSString *)content:(int)error {
  NSError *err = nil;
  searchAgentListing =
      [[SearchAgentListing alloc] initWithString:content error:&err];
  return searchAgentListing;
}
- (SearchAgentListing *)initwithNSDictionary:(NSDictionary *)
                                     content:(int)error {
  NSError *err = nil;
  searchAgentListing =
      [[SearchAgentListing alloc] initWithDictionary:content error:&err];
  return searchAgentListing;
}

@end