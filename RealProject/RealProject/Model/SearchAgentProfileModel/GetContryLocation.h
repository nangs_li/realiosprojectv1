//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
@interface GetContryLocation : BaseModel

+ (GetContryLocation *)shared;

@property(strong, nonatomic) NSString* locationCode;
@property(strong, nonatomic) NSString* locationConuntryName;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Findd

 */
- (void)getContryLocation;
- (BOOL)contryLocationISChina;
-(NSString *)getCountryName;
-(NSString *)getCountryNameFromSimCard;
@end
