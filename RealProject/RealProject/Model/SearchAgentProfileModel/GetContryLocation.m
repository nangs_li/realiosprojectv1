//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "GetContryLocation.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "HandleServerReturnString.h"

@implementation GetContryLocation

static GetContryLocation *sharedGetContryLocation;




+ (GetContryLocation *)shared {
    @synchronized(self) {
        if (!sharedGetContryLocation) {
            sharedGetContryLocation = [[GetContryLocation alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedGetContryLocation;
    }
}


- (void)getContryLocation{
    //    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    //    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    //    NSString *cc = [carrier mobileCountryCode];
    //    DDLogDebug(@"cc %@",cc);
    //    self.locationCode=cc;
    //
    //    if ([self isEmpty:cc]) {
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    
    [manager.requestSerializer setTimeoutInterval:5];
    
    [manager GET:@"http://www.google.com"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             
             self.locationCode=@"310";
             
             
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             self.locationCode=@"460";
             
         }];
    
    
    
    
    //    }
    
}
- (BOOL)contryLocationISChina{
    if ([self.locationCode isEqualToString:@"460"]||isEmpty(self.locationCode)) {
        return YES;
    }else{
        return NO;
        //return YES;
    }
}
-(NSString *)getCountryName{
    NSLocale *CurrentCountry = [NSLocale currentLocale];
    // Get the country Code
    NSString *Country = [CurrentCountry objectForKey:NSLocaleCountryCode];
    // Check if it returned anything
    if (Country == nil || Country.length <= 0) {
        // No country found
        return @" ";
    }
    // Return the country
    return Country;
}
#pragma check empty
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}
-(NSString *)getCountryNameFromSimCard{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    NSString *countryCode = [carrier isoCountryCode];
    countryCode = [countryCode uppercaseString];
    if (!countryCode) {
        countryCode = @"US";
    }
    return countryCode;
}


@end