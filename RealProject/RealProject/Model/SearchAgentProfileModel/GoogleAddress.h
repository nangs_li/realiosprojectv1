//
//  AgentExperience.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_AgentExperience_h
//#define abc_AgentExperience_h

//#endif
#import "JSONModel.h"
@protocol GoogleAddress
@end

@interface GoogleAddress : JSONModel
@property(strong, nonatomic) NSString* Lang;
@property(strong, nonatomic) NSString* Location;
@property(strong, nonatomic) NSString* Name;
@property(strong, nonatomic) NSString* FormattedAddress;
@property(strong, nonatomic) NSString* PlaceID;
@end

//@implementation AgentExperience

//@end