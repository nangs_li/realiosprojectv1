//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"
#import "HandleGoogleAutoCompleteJsonRow.h"
@interface HandleGoogleAutoCompleteJson : JSONModel
//+ (d *)shared;
@property(strong, nonatomic) NSArray<HandleGoogleAutoCompleteJsonRow> * predictions;
@property(strong, nonatomic) NSString *status;
+ (HandleServerReturnString *)shared;
- (HandleServerReturnString *)initwithstring:(NSString *)json;
@end
