//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "HandleGoogleAutoCompleteJson.h"

@implementation HandleGoogleAutoCompleteJson
static HandleGoogleAutoCompleteJson *sharedHandleGoogleAutoCompleteJson;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (HandleGoogleAutoCompleteJson *)shared {
  @synchronized(self) {
    if (!sharedHandleGoogleAutoCompleteJson) {
      sharedHandleGoogleAutoCompleteJson = [[HandleGoogleAutoCompleteJson alloc] init];
    }
    return sharedHandleGoogleAutoCompleteJson;
  }
}
- (HandleGoogleAutoCompleteJson *)initwithstring:(NSString *)json {
  NSError *err = nil;

  sharedHandleGoogleAutoCompleteJson =
      [[HandleGoogleAutoCompleteJson alloc] initWithString:json error:&err];
  return sharedHandleGoogleAutoCompleteJson;
}

@end