//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import "JSONModel.h"

@protocol HandleGoogleAutoCompleteJsonRow
@end

@interface HandleGoogleAutoCompleteJsonRow : JSONModel

@property(strong, nonatomic) NSString *googleDescription;
@property(strong, nonatomic) NSString *place_id;
@property(strong, nonatomic) NSString *reference;


@end
