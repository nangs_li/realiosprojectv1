//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "HandleGoogleAutoCompleteJsonRow.h"
#import "RealUtility.h"

@implementation HandleGoogleAutoCompleteJsonRow
static HandleGoogleAutoCompleteJsonRow *sharedHandleGoogleAutoCompleteJsonRow;
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"description": @"googleDescription",
                                                                                                          }];
}

@end