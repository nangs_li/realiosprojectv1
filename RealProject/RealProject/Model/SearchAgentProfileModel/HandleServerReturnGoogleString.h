//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@interface HandleServerReturnGoogleString : JSONModel
//+ (d *)shared;
@property(assign, nonatomic) int ErrorCode;
@property(strong, nonatomic) NSString *ErrorMsg;
@property(strong, nonatomic) NSString *APIName;
@property(strong, nonatomic) NSString *GoogleResult;
+ (HandleServerReturnGoogleString *)shared;
- (HandleServerReturnGoogleString *)initwithstring:(NSString *)json;
@end
