//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "HandleServerReturnGoogleString.h"

@implementation HandleServerReturnGoogleString
static HandleServerReturnGoogleString *sharedHandleServerReturnGoogleString;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (HandleServerReturnGoogleString *)shared {
  @synchronized(self) {
    if (!sharedHandleServerReturnGoogleString) {
      sharedHandleServerReturnGoogleString = [[HandleServerReturnGoogleString alloc] init];
    }
    return sharedHandleServerReturnGoogleString;
  }
}
- (HandleServerReturnGoogleString *)initwithstring:(NSString *)json {
  NSError *err = nil;

  sharedHandleServerReturnGoogleString =
      [[HandleServerReturnGoogleString alloc] initWithString:json error:&err];
  return sharedHandleServerReturnGoogleString;
}

@end