//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
@interface PressChatButtonModel : BaseModel

+ (PressChatButtonModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=20 Agent have not chat account.
 errorStatus=21 This account have not chat account.
 errorStatus=23 Chat Server Not Response

 */
- (void)pressChatButtonModelByQBID:(NSString *)QBID
                        MemberName:(NSString *)MemberName
                        MemberID:(NSString *)MemberID
                           success:(void (^)(Chatroom *chatRoom))success
                           failure:
                               (failureBlock)failure;

@end
