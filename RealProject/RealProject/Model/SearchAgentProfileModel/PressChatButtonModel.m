//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "PressChatButtonModel.h"
#import "ChatRelationModel.h"
#import "HandleServerReturnString.h"
#import "ChatRoom.h"
#import "ChatDialogModel.h"
#import "DBQBChatDialogDelete.h"
#import "DBQBUUser.h"
#import "ChatRoomSendMessageModel.h"
#import "QBRequest+Timeout.h"
@implementation PressChatButtonModel

static PressChatButtonModel *sharedPressChatButtonModel;

+ (PressChatButtonModel *)shared {
    @synchronized(self) {
        if (!sharedPressChatButtonModel) {
            sharedPressChatButtonModel = [[PressChatButtonModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedPressChatButtonModel;
    }
}

- (void)pressChatButtonModelByQBID:(NSString *)QBID MemberName:(NSString *)MemberName MemberID:(NSString *)MemberID success:(void (^)(Chatroom *chatRoom))success failure:(failureBlock)failure {
    
    if (![self networkConnection]) {
        failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotRecordFind, JMOLocalizedString(@"alert_alert", nil),
                JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    if ([self isEmpty:QBID]) {
        failure(nil, nil, @"Agent have not chat account.", 20, JMOLocalizedString(@"alert_alert", nil), JMOLocalizedString(@"alert_ok", nil));
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                                                        message:@"Agent have not chat account."
                                                       delegate:nil
                                              cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
                                              otherButtonTitles:nil];
        
        [alert show];
        return;
    }
    
    if ([self isEmpty:[LoginBySocialNetworkModel shared].myLoginInfo.QBID]) {
        failure(nil, nil, @"This account have not chat account..", 21, JMOLocalizedString(@"alert_alert", nil),
                JMOLocalizedString(@"alert_ok", nil));
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                                                        message:@"This account have not chat account."
                                                       delegate:nil
                                              cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([[LoginBySocialNetworkModel shared].myLoginInfo.QBID isEqualToString:QBID]) {
        failure(nil, nil,@"You cannot chat with yourself!",RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
    }
    QBChatDialog *chatDialog = [[QBChatDialog alloc] initWithDialogID:NULL type:QBChatDialogTypePrivate];
    
    NSMutableArray *selectedUsersIDs = [NSMutableArray array];
    [selectedUsersIDs addObject:QBID];
    chatDialog.occupantIDs = selectedUsersIDs;
    
    Chatroom *vc = [Chatroom messagesViewController];
    
   // vc.chatroomNameString = MemberName;
    NSNumberFormatter *NumberFormatter = [[NSNumberFormatter alloc] init];
    NumberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    vc.recipientID = [NumberFormatter numberFromString:QBID];
  //  vc.recipienterMemberID = MemberID;
        vc.recipienterMemberID =[NSString stringWithFormat:@"%d",[AppDelegate getAppDelegate].currentAgentProfile.MemberID];
        vc.chatroomNameString=[AppDelegate getAppDelegate].currentAgentProfile.MemberName;
        vc.recipientPhotoUrl =
        [NSURL URLWithString:[AppDelegate getAppDelegate].currentAgentProfile.AgentPhotoURL];
    
    
  NSMutableDictionary *extendedRequest = [[NSMutableDictionary alloc] init];
  DDLogDebug(@"MemberName %@", MemberName);
  DDLogDebug(@"occupants_ids[all] %@",
             [NSString stringWithFormat:@"%@,%@", QBID,
                                        [LoginBySocialNetworkModel shared]
                                            .myLoginInfo.QBID]);
  extendedRequest[@"occupants_ids[all]"] = [NSString
      stringWithFormat:@"%@,%@", QBID,
                       [LoginBySocialNetworkModel shared].myLoginInfo.QBID];

 __block QBRequest *countDialogRequest =  [QBRequest countOfDialogsWithExtendedRequest:extendedRequest
      successBlock:^(QBResponse *response, NSUInteger count) {
          [countDialogRequest stopTimeoutTimer];
        DDLogInfo(@"countOfDialogsWithExtendedRequestcount-->%lu",
                  (unsigned long)count);

        if (count >= 1) {
          //// Have Create Dialog   getDialog again  through creatDialog Method;
          vc.haveNotCreateDialog = NO;
            
        __block QBRequest *createDialogRequest  =  [QBRequest createDialog:chatDialog
              successBlock:^(QBResponse *response,
                             QBChatDialog *createdDialog) {
                   [createDialogRequest stopTimeoutTimer];
                  [[[[DBQBChatDialogDelete query]
                     whereWithFormat:@" DialogID = %@", createdDialog.ID] fetch] removeAll];
                DDLogInfo(@"dialog@@@%@ StopStop", createdDialog);
                vc.dialog = createdDialog;
                                 

                  DBResultSet * r =  [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@ And ChatMessageType = %@", createdDialog.ID,kChatMessageTypeAgentListing]
                                        orderByDescending:@"Id"] limit:1]fetch];
                  
                  for (DBQBChatMessage * dbQBChatMessage in r) {
                      NSString * agentlistingid=    [dbQBChatMessage.CustomParameters objectForKey:kChatMessageCustomParameterAgentListing];
                      if ([agentlistingid intValue] !=[AppDelegate getAppDelegate].currentAgentProfile.AgentListing.AgentListingID) {
                          vc.secondTimeSendCoverViewMessage=YES;
                      }
                  }
                success(vc);

              }
              errorBlock:^(QBResponse *response) {
                DDLogError(@"QBResponse %@", response);
                  failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotNetWorkConnection, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));

              }];
            [createDialogRequest startTimeoutTimer];
        } else {
          //// Have NO Create Dialog;
          vc.haveNotCreateDialog = YES;
          vc.dialog = chatDialog;
          success(vc);
        }

      }
      errorBlock:^(QBResponse *response) {
          [countDialogRequest stopTimeoutTimer];
        DDLogError(@"QBResponse %@", response);
          failure(nil, nil, JMOLocalizedString(NotNetWorkConnectionText, nil), NotNetWorkConnection, JMOLocalizedString(@"alert_alert", nil),
                  JMOLocalizedString(@"alert_ok", nil));
      }];
    
    [countDialogRequest startTimeoutTimer];
    
//    [createDialogRequest startTimeoutTimer];
}

@end