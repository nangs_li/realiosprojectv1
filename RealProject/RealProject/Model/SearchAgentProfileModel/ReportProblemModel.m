//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ReportProblemModel.h"

#import "HandleServerReturnString.h"

@implementation ReportProblemModel

static ReportProblemModel *sharedReportProblemModel;




+ (ReportProblemModel *)shared {
    @synchronized(self) {
        if (!sharedReportProblemModel) {
            sharedReportProblemModel = [[ReportProblemModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedReportProblemModel;
    }
}


- (void)callReportProblemApiByProblemType:(NSString *)problemType toMemberID:(NSString *)toMemberID
                              toListingID:(NSString *)toListingID
                              description:(NSString *)description
                                  success:(successBlock)success
                                  failure:(failureBlock)failure{
    
    NSString *urlLink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/ReportProblem"];
    NSString *parameters =
    [NSString stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
     @"\"ProblemType\":\"%@\",\"ToMemberID\":%@,"
     @"\"ToListingID\":%@,\"Description\":\"%@\"}",
     [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
     [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken, problemType, toMemberID, toListingID,
     description, nil];
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              success(responseObject);
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
              
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
               failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              
          }];
}

@end