//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import "JSONModel.h"
#import "AgentProfile.h"

@interface SearchAgentProfile : JSONModel
@property(strong, nonatomic) NSNumber* TotalCount;
@property(strong, nonatomic) NSNumber* RecordCount;
@property(strong, nonatomic) NSArray* FullResultSet;
@property(strong, nonatomic) NSMutableArray<AgentProfile>* AgentProfiles;

- (SearchAgentProfile*)initwithjsonstring:(NSString*)content:(int)error;
- (SearchAgentProfile*)initwithNSDictionary:(NSDictionary*)content:(int)error;

@end
