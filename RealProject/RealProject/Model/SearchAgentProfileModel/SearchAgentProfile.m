//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SearchAgentProfile.h"
@implementation SearchAgentProfile
static SearchAgentProfile *searchagentprofile;
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

- (SearchAgentProfile *)initwithjsonstring:(NSString *)content:(int)error {
  NSError *err = nil;
  searchagentprofile =
      [[SearchAgentProfile alloc] initWithString:content error:&err];
  return searchagentprofile;
}
- (SearchAgentProfile *)initwithNSDictionary:(NSDictionary *)
                                     content:(int)error {
  NSError *err = nil;
  searchagentprofile =
      [[SearchAgentProfile alloc] initWithDictionary:content error:&err];
  return searchagentprofile;
}

@end