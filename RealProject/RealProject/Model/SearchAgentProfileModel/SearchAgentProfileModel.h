//
//  AgentProfile.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AgentProfile.h"
#import "AFHTTPRequestOperation.h"
#import "HandleGoogleAutoCompleteJson.h"
#import "HandleGoogleAutoCompleteJsonRow.h"
@interface SearchAgentProfileModel : BaseModel<CLLocationManagerDelegate>
@property(strong, nonatomic) NSMutableArray<AgentProfile>* AgentProfiles;
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;
@property(strong, nonatomic)SearchAgentProfile *searchAgentProfilesHaveTotalCount;
@property(assign, nonatomic)int searchAgentProfilesTotalCount;
@property(strong, nonatomic)SearchAgentProfile *searchAgentRetrieveProfilesHaveTotalCount;
@property(strong, nonatomic) HandleGoogleAutoCompleteJson *handleGoogleAutoCompleteJson;
@property(strong, nonatomic) CLLocationManager *locationManager;
@property(strong, nonatomic) CLLocation *location;
@property(strong, nonatomic) CLPlacemark *placemark;
@property(strong, nonatomic) NSString *subAdministrativeAreaLocation;
@property(strong, nonatomic) NSString *cityLocation;
@property(strong, nonatomic) NSString *countryLocation;
@property(assign, nonatomic) BOOL didHandleUserLocation;
@property(strong, nonatomic) NSTimer *locationTimer;
@property(strong, nonatomic) NSString *userInputSearchAddress;
@property(strong, nonatomic) NSString *dbPlaceID;
+ (SearchAgentProfileModel *)shared ;
/**
 *  callGoogleGeoCodeApiConvertFromPlaceIDAndThenAddressSearch
 *
 *  @param placeID                 didselect tableViewCell Google placeID
 *  @param userInputSearchAddress  userInputSearchAddress
 *  @param success                 NSMutableArray<AgentProfile>* AgentProfiles
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Real Server Not Reponse
 errorStatus=3 checkAccessError Other User Login This Account
 errorStatus=4 Not Record Find
 errorStatus=10 Google Server Not Reponse
 errorStatus=11 Google Return Empty Result
 
 */
- (void)callGoogleGeoCodeApiConvertFromPlaceIDAndThenAddressSearch:(NSString *)placeID userInputSearchAddress:(NSString *)userInputSearchAddress
           success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                                           failure:(failureBlock) failure;


/**
 *  see RealAPi pdf page 52
 
 */
- (void)callGoogleGeoCodeApiConvertFromPlaceIDAndThenAddressSearchWithFilter:(NSString *)placeID userInputSearchAddress:(NSString *)userInputSearchAddress  PropertyTypeSet:(NSArray * )PropertyTypeSet PriceSet:(NSArray * )PriceSet SizeMax:(int )SizeMax SizeMin:(int)SizeMin BedroomCount:(int )BedroomCount BathroomCount:(int )BathroomCount SpokenLanguageIndex:(int )SpokenLanguageIndex CurrencyUnit:(NSString *)CurrencyUnit SizeUnitType:(int)SizeUnitType
                                                                     success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                                                     failure:(failureBlock) failure;
-(void)callGeoCodeapiConverFromPlaceIDAndAddressSearch:(NSString*)placeID addressSearch:(NSString*)addressSearch succes:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))block failure:(failureBlock)failureBlock;
-(void)callAddressSearchRetrieveAgentProfileSuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
failure:(failureBlock) failure;
- (void)initLocationManager;
- (void)getGoogleLocationFromCityName:(NSString *)cityName
                              success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                              failure:(failureBlock) failure;

- (void)callChinaGoogleMapPlaceAPIAutoCompleteUserInputSearchAddress:(NSString *)userInputSearchAddress
                                                             success:(void (^)(HandleGoogleAutoCompleteJson * handleGoogleAutoCompleteJson))success
                                                             failure:(failureBlock) failure;
- (void)callChinaGoogleMapPlaceAPIGeocodeByPlaceID:(NSString *)placeID
                                          language:(NSString *)language
                            userInputSearchAddress:(NSString *)userInputSearchAddress
                                           success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                           failure:(failureBlock) failure;

-(void)getDBSearchAddressArrayToSearchAgentProfileModel;
-(void)postNotificationWithLandingAgentProfiles:(NSArray*)profile success:(BOOL)success;

@end
