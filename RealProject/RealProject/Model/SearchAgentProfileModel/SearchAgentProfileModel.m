//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SearchAgentProfileModel.h"
#import "GetContryLocation.h"
#import "HandleServerReturnGoogleString.h"
#import "RealPrefetcher.h"
#import "SPGooglePlacesAutocompleteUtilities.h"
#import "DBSearchAddressArray.h"
#import "DBHaveShowLanding.h"
#import "NSDictionary+Utility.h"
@implementation SearchAgentProfileModel

static SearchAgentProfileModel *sharedSearchAgentProfileModel;




+ (SearchAgentProfileModel *)shared {
    @synchronized(self) {
        if (!sharedSearchAgentProfileModel) {
            sharedSearchAgentProfileModel = [[SearchAgentProfileModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
            
        }
        return sharedSearchAgentProfileModel;
    }
}

-(void)callGeoCodeapiConverFromPlaceIDAndAddressSearch:(NSString*)placeID addressSearch:(NSString*)addressSearch succes:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))block failure:(failureBlock)failureBlock{
    NSString *lang = @"en";
    if ([[GetContryLocation shared] contryLocationISChina]){
        [self callChinaGoogleMapPlaceAPIGeocodeByPlaceID:placeID language:lang userInputSearchAddress:addressSearch success:block failure:failureBlock];
    }else{
        [self callGoogleGeoCodeApiConvertFromPlaceIDAndThenAddressSearch:placeID userInputSearchAddress:addressSearch success:block failure:failureBlock];
    }
}

- (void)callGoogleGeoCodeApiConvertFromPlaceIDAndThenAddressSearch:(NSString *)placeID userInputSearchAddress:(NSString *)userInputSearchAddress
                                                           success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                                           failure:(failureBlock) failure{
    NSString *googleurlstring = [NSString
                                 stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/"
                                 @"json?place_id=%@&key="
                                 @"%@&language=en",
                                 placeID,kGoogleAPIKey];
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogDebug(@"apiurlstring-->:%@", googleurlstring);
    googleurlstring = [self stringByReplacingSpaceAndByAddingPercentEscapesUsingEncoding:googleurlstring];
    [manager POST:googleurlstring
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *urllink =
              [NSString stringWithFormat:@"%@%@", kServerAddress,
               @"/Operation.asmx/AddressSearch"];
              NSArray *results = [responseObject objectForKey:@"results"];
              NSDictionary *resultsFirstObject = [results firstObject];
              
              if ([self isEmpty:resultsFirstObject]) {
                  
                  failure(operation, nil,@"Google Return Empty Result",GoogleReturnEmptyResult,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
                  return;
              }
              NSDictionary *googleaddress = @{
                                              @"GoogleAddressLang" : @"en",
                                              @"GoogleAddress" : resultsFirstObject
                                              };
              
              NSString *ArrayjsonString;
              NSError *error;
              NSData *jsonData =
              [NSJSONSerialization dataWithJSONObject:googleaddress
                                              options:NSJSONWritingPrettyPrinted
                                                error:&error];
              if (!jsonData) {
                  DDLogError(@"Got an error: %@", error);
              } else {
                  NSString *jsonString =
                  [[NSString alloc] initWithData:jsonData
                                        encoding:NSUTF8StringEncoding];
                  
                  ArrayjsonString =
                  [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                        withString:@""];
              }
              
              NSString *parameters =
              [NSString stringWithFormat:
               @"{\"MemberID\":%@,\"AccessToken\":\"%@\","
               @"\"UserAddress\":\"%@\",\"GoogleAddressPack\":%@,\"LanguageIndex\":%d}",
               [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
               [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
               userInputSearchAddress, ArrayjsonString,[SystemSettingModel shared]
               .selectSystemLanguageList.Index ,nil];
              
              
              
              
              DDLogVerbose(@"apiurlstring-->:%@", urllink);
              DDLogVerbose(@"InputJson-->:%@", parameters);
              
              [manager POST:urllink
                 parameters:@{ @"InputJson" : parameters }
                    success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSString *newStr =
                        [[NSString alloc] initWithData:operation.request.HTTPBody
                                              encoding:NSUTF8StringEncoding];
                        DDLogVerbose(@"operation.request.HTTPBody-->%@", newStr);
                        DDLogVerbose(@"response-->Success");
                        
                        NSString *dstring = [responseObject objectForKey:@"d"];
                        if ([self checkAccessError:dstring]) {
                            failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                            
                        }
                        //// check serverReturnStringError
                        NSError *err = nil;
                        self.HandleServerReturnString =
                        [[HandleServerReturnString alloc] initWithString:dstring
                                                                   error:&err];
                        
                        int error = self.HandleServerReturnString.ErrorCode;
                        
                        if (error == 0 || error == 20 || error == 21 || error == 22 ||
                            error == 23) {
                            NSError *err = nil;
                            self.HandleServerReturnString =
                            [[HandleServerReturnString alloc] initWithString:dstring error:&err];
                            
                            int error = self.HandleServerReturnString.ErrorCode;
                            if (error == 0) {
                                self.searchAgentProfilesHaveTotalCount =
                                [[SearchAgentProfile alloc]
                                 initwithNSDictionary:self.HandleServerReturnString.
                                 Content:error];
                                
                                self.AgentProfiles=self.searchAgentProfilesHaveTotalCount.AgentProfiles;
                                self.searchAgentProfilesTotalCount=[self.searchAgentProfilesHaveTotalCount.TotalCount intValue];
                             
                                [[[[DBSearchAddressArray query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] fetch] removeAll];
                                [DBSearchAddressArray createDBSearchAddressArrayHaveCount:self.searchAgentProfilesHaveTotalCount placeID:placeID userInputSearchAddress:userInputSearchAddress googleAddressPackDict:googleaddress];
                               
                                success(self.AgentProfiles);
                            }
                            
                            
                            
                        } else {
                            
                            if (error == 40) {
                                
                                self.searchAgentProfilesHaveTotalCount = nil;
                                self.AgentProfiles = nil;
                                
                                failure(operation, nil,JMOLocalizedString(@"search_result_detail__result_not_find", nil),NotRecordFind,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                                
                            }else{
                                
                                failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                                        JMOLocalizedString(@"alert_ok", nil));
                            }
                            
                        }
                        
                        
                    }
                    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSString *newStr =
                        [[NSString alloc] initWithData:operation.request.HTTPBody
                                              encoding:NSUTF8StringEncoding];
                        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
                        DDLogError(@"response-->Error: %@", error);
                        failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                    }];
              
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,nil,GoogleServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}

- (void)callGoogleGeoCodeApiConvertFromPlaceIDAndThenAddressSearchWithFilter:(NSString *)placeID userInputSearchAddress:(NSString *)userInputSearchAddress  PropertyTypeSet:(NSArray * )PropertyTypeSet PriceSet:(NSArray * )PriceSet SizeMax:(int )SizeMax SizeMin:(int)SizeMin BedroomCount:(int )BedroomCount BathroomCount:(int )BathroomCount SpokenLanguageIndex:(int )SpokenLanguageIndex CurrencyUnit:(NSString *)CurrencyUnit SizeUnitType:(int)SizeUnitType
                                                                     success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                                                     failure:(failureBlock) failure{
    NSString *googleurlstring = [NSString
                                 stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/"
                                 @"json?place_id=%@&key="
                                 @"%@&language=en",
                                 placeID,kGoogleAPIKey];
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", googleurlstring);
    DDLogVerbose(@"InputJson-->:%@", nil);
     googleurlstring = [self stringByReplacingSpaceAndByAddingPercentEscapesUsingEncoding:googleurlstring];
    [manager POST:googleurlstring
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *urllink =
              [NSString stringWithFormat:@"%@%@", kServerAddress,
               @"/Operation.asmx/AddressSearch"];
              NSArray *results = [responseObject objectForKey:@"results"];
              NSDictionary *resultsFirstObject = [results firstObject];
              
              if ([self isEmpty:resultsFirstObject]) {
                  
                  failure(operation, nil,@"Google Return Empty Result",GoogleReturnEmptyResult,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
                  return;
              }
              NSDictionary *googleaddress = @{
                                              @"GoogleAddressLang" : @"en",
                                              @"GoogleAddress" : resultsFirstObject
                                              };
              
              NSString *ArrayjsonString;
              NSError *error;
              NSData *jsonData =
              [NSJSONSerialization dataWithJSONObject:googleaddress
                                              options:NSJSONWritingPrettyPrinted
                                                error:&error];
              if (!jsonData) {
                  DDLogError(@"Got an error: %@", error);
              } else {
                  NSString *jsonString =
                  [[NSString alloc] initWithData:jsonData
                                        encoding:NSUTF8StringEncoding];
                  
                  ArrayjsonString =
                  [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                        withString:@""];
              }
              
              NSString *parameters =
              [NSString stringWithFormat:
               @"{\"MemberID\":%@,\"AccessToken\":\"%@\","
               @"\"UserAddress\":\"%@\",\"GoogleAddressPack\":%@ ,\"LanguageIndex\":%d,\"Filter\": {",
               [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
               [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
               userInputSearchAddress, ArrayjsonString,[SystemSettingModel shared]
               .selectSystemLanguageList.Index, nil];
              
              NSString *filterParameters = nil;
              if (! ([self isEmpty:PropertyTypeSet])) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"PropertyTypeSet\":%@", filterParameters, [self nsDictionaryArrayToInputParametersString:PropertyTypeSet.mutableCopy]];
                  
              }
              if (!([self isEmpty:PriceSet])) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"PriceSet\":%@", filterParameters, [self nsDictionaryArrayToInputParametersString:PriceSet.mutableCopy]];
                  
              }
              if ((0 <= SizeMax)) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"SizeMax\":%d", filterParameters, SizeMax];
                  
              }
              if ((0 <= SizeMin)) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"SizeMin\":%d", filterParameters, SizeMin];
                  
              }
              if (!(0 == BedroomCount)) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"BedroomCount\":%d", filterParameters, BedroomCount];
                  
              }
              if (!(0 == BathroomCount)) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"BathroomCount\":%d", filterParameters, BathroomCount];
                  
              }
              if (!(0 == SpokenLanguageIndex)) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"SpokenLanguageIndex\":%d", filterParameters, SpokenLanguageIndex];
                  
              }
              if (!( [self isEmpty:CurrencyUnit])) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"CurrencyUnit\":\"%@\"", filterParameters, CurrencyUnit];
                  
              }  if ((0 <= SizeUnitType)) {
                  if (filterParameters) {
                      filterParameters = [filterParameters stringByAppendingString:@","];
                  }else{
                      filterParameters = @"";
                  }
                  filterParameters = [NSString stringWithFormat: @"%@\"SizeUnitType\":%d", filterParameters, SizeUnitType];
                  
              }
              if (filterParameters) {
                  parameters =[parameters stringByAppendingString:filterParameters];
              }
              
              
              parameters = [NSString stringWithFormat: @"%@}}", parameters];
              DDLogVerbose(@"apiurlstring-->:%@", urllink);
              DDLogVerbose(@"InputJson-->:%@", parameters);
              
              DDLogDebug(@"InputJson:%@",parameters);
              [manager POST:urllink
                 parameters:@{ @"InputJson" : parameters }
                    success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSString *newStr =
                        [[NSString alloc] initWithData:operation.request.HTTPBody
                                              encoding:NSUTF8StringEncoding];
                        DDLogVerbose(@"operation.request.HTTPBody-->%@", newStr);
                        DDLogVerbose(@"response-->Success");
                        
                        NSString *dstring = [responseObject objectForKey:@"d"];
                        if ([self checkAccessError:dstring]) {
                            failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                            
                        }
                        //// check serverReturnStringError
                        NSError *err = nil;
                        self.HandleServerReturnString =
                        [[HandleServerReturnString alloc] initWithString:dstring
                                                                   error:&err];
                        
                        int error = self.HandleServerReturnString.ErrorCode;
                        
                        if (error == 0 || error == 20 || error == 21 || error == 22 ||
                            error == 23) {
                            NSError *err = nil;
                            self.HandleServerReturnString =
                            [[HandleServerReturnString alloc] initWithString:dstring error:&err];
                            
                            int error = self.HandleServerReturnString.ErrorCode;
                            if (error == 0) {
                                self.searchAgentProfilesHaveTotalCount =
                                [[SearchAgentProfile alloc]
                                 initwithNSDictionary:self.HandleServerReturnString.
                                 Content:error];
                                
                                self.AgentProfiles=self.searchAgentProfilesHaveTotalCount.AgentProfiles;
                                self.searchAgentProfilesTotalCount=[self.searchAgentProfilesHaveTotalCount.TotalCount intValue];
                                     [[[[DBSearchAddressArray query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] fetch] removeAll];
                                [DBSearchAddressArray createDBSearchAddressArrayHaveCount:self.searchAgentProfilesHaveTotalCount placeID:placeID userInputSearchAddress:userInputSearchAddress googleAddressPackDict:googleaddress];
                                success(self.AgentProfiles);
                            }
                            
                            
                            
                        } else {
                            
//                            if (error == 40) {
                            
                                self.searchAgentProfilesHaveTotalCount = nil;
                                self.AgentProfiles = nil;
                                
                                failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                                        JMOLocalizedString(@"alert_ok", nil));
                                
//                            }
                            
                        }
                        
                        
                    }
                    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSString *newStr =
                        [[NSString alloc] initWithData:operation.request.HTTPBody
                                              encoding:NSUTF8StringEncoding];
                        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
                        DDLogError(@"response-->Error: %@", error);
                        failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                    }];
              
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,nil,GoogleServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
}

-(void)callAddressSearchRetrieveAgentProfileSuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success failure:(failureBlock) failure{
    NSMutableArray *agentProfileRetrieveList = [[NSMutableArray alloc] init];
    int i=0;
    for (NSString * MemberID in self.searchAgentProfilesHaveTotalCount.FullResultSet) {
        i++;
        if (self.AgentProfiles.count<i) {
            [agentProfileRetrieveList
             addObject:MemberID];
        }
        if (i==self.AgentProfiles.count+20||self.AgentProfiles.count==self.searchAgentProfilesHaveTotalCount.FullResultSet.count) {
            break;
        }
    }
    DBResultSet * r=     [[[[DBSearchAddressArray query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] limit:1] fetch] ;
    NSDictionary * googlePackDict;
    for (DBSearchAddressArray * dbSearchAddressArray in r) {
        googlePackDict=dbSearchAddressArray.GoogleAddressPackDict;
    }

    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"MemberID\":%@,\"AccessToken\":\"%@\",\"RetrieveList\":%@,\"LanguageIndex\":%d ,\"GoogleAddressPack\":%@}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            [self nsDictionaryArrayToInputParametersString:agentProfileRetrieveList],
                            [SystemSettingModel shared]
                            .selectSystemLanguageList.Index, [self nsDictionaryArrayToInputParametersString:googlePackDict],
                            nil];
    
    NSString *urllink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/AddressSearchRetrieveAgentProfile"];
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogVerbose(@"apiurlstring-->:%@", urllink);
    DDLogVerbose(@"InputJson-->%@", parameters);
    
    
    
    [manager POST:urllink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              DDLogVerbose(@"serverreturnstring-->%@", dstring);
              NSError *err = nil;
              self.HandleServerReturnString =
              [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              DDLogVerbose(@"self.HandleServerReturnString to string-->%@",
                           [self.HandleServerReturnString toJSONString]);
              DDLogVerbose(@"self.HandleServerReturnString.ErrorCode-->%d",
                           self.HandleServerReturnString.ErrorCode);
              int error = self.HandleServerReturnString.ErrorCode;
              if (error == 0) {
                  self.searchAgentRetrieveProfilesHaveTotalCount =
                  [[SearchAgentProfile alloc]
                   initwithNSDictionary:self.HandleServerReturnString.
                   Content:error];
                  for (AgentProfile * agentProfile in self.searchAgentRetrieveProfilesHaveTotalCount.AgentProfiles) {
                      [self.AgentProfiles addObject:agentProfile];
                  }
                  self.searchAgentProfilesHaveTotalCount.AgentProfiles=self.AgentProfiles;
                  DBResultSet * r=     [[[[DBSearchAddressArray query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] limit:1] fetch] ;
                  for (DBSearchAddressArray * dbSearchAddressArray in r) {
                      dbSearchAddressArray.SearchAddressArrayHaveCount  = [NSKeyedArchiver
                                                                           archivedDataWithRootObject:[self.searchAgentProfilesHaveTotalCount copy]];
                      [dbSearchAddressArray commit];
                  }
                
                  success(self.AgentProfiles);
              }else{
                  failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
              
              
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
          }];
    
    
    
    
}
-(void)initLocationManager{
#pragma mark locationmanager init
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
   //   self.locationTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(fetchAgentProfileWithCountyLocation) userInfo:nil repeats:NO];
}
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    self.location=nil;
    [self.locationManager startUpdatingLocation];
    if (!self.locationTimer) {
        [self.locationTimer invalidate];
    }

}
- (void)getGoogleLocationFromCityName:(NSString *)cityName
                                            success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                            failure:(failureBlock) failure{

   

    
    NSString *googleurlstring = [NSString
                                 stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&language=en",
                                 cityName];
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    
    DDLogDebug(@"apiurlstring-->:%@", googleurlstring);
    googleurlstring = [self stringByReplacingSpaceAndByAddingPercentEscapesUsingEncoding:googleurlstring];
    
   
    [manager GET:googleurlstring
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSArray *results = [responseObject objectForKey:@"results"];
              DDLogDebug(@"results %@",results);
             NSDictionary *resultsFirstObject = [results firstObject];
              DDLogDebug(@"resultsFirstObject %@",resultsFirstObject);
             NSString *place_id = [resultsFirstObject objectForKey:@"place_id"];
           
              DDLogDebug(@"place_id %@",place_id);
             
             [self callGeoCodeapiConverFromPlaceIDAndAddressSearch:place_id addressSearch:kLocationSearchType succes:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                 [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:AgentProfiles clearQueue:YES];
                 success(self.AgentProfiles);
             } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                 if (errorStatus==NotRecordFind) {
                      failure(operation, error,JMOLocalizedString(@"search_result_detail__result_not_find", nil),NotRecordFind,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                 }else{
                 failure(operation, error,nil,GoogleServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                 }
             }];

         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSString *newStr =
             [[NSString alloc] initWithData:operation.request.HTTPBody
                                   encoding:NSUTF8StringEncoding];
             
             DDLogError(@"operation.request.HTTPBody-->%@", newStr);
             DDLogError(@"response-->Error: %@", error);
             failure(operation, error,nil,GoogleServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
         }];
}

- (void) stopUpdatingLocation{
    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;
}

#pragma mark locationmanger delegate---------------------------------------------------------------------------------------------------------------
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    if ([self isEmpty:self.location]) {
        self.location = [locations lastObject];
        if (!self.didHandleUserLocation) {
            [self.locationTimer invalidate];
            if ([DBHaveShowLanding queryMyDBHaveShowLandingRecordCount]!=0) {
           
            }else{
                  [self getReverseGeocode];
            }

        }
    }
    [self stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    DDLogError(@"didFailWithError: %@", error);
    // [errorAlert show];
    if (error) {
        if ([DBHaveShowLanding queryMyDBHaveShowLandingRecordCount]!=0) {
        
        }else{
            [self fetchAgentProfileWithCountyLocation];
        }
    }
     [self stopUpdatingLocation];
}

-(void) fetchAgentProfileWithCountyLocation{
    self.didHandleUserLocation = YES;
    [[GetContryLocation shared] getContryLocation];
    if (![self isEmpty: [[GetContryLocation shared] getCountryName]]) {
        
        NSString * Country=[[GetContryLocation shared] getCountryName];
        
        [self  getGoogleLocationFromCityName: Country success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
            
            if (AgentProfiles.count==0) {
                [self notLocationOrHaveLocationNotResultFindCallUSAAddress];
            }else {
                [self postNotificationWithLandingAgentProfiles:AgentProfiles success:YES];
            }
            return ;
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error,NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert, NSString *ok) {
            
            [self notLocationOrHaveLocationNotResultFindCallUSAAddress];
            
            return ;
            
            
        }];
    }else{
        
        [self notLocationOrHaveLocationNotResultFindCallUSAAddress];
    }
}
-(void)notLocationOrHaveLocationNotResultFindCallUSAAddress{
    
    NSString *googleurlstring = [NSString
                                 stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=USA&language=en"];
    AFHTTPRequestOperationManager *httpManager =
    [self getAPiManager];
    
    
    httpManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    
    DDLogDebug(@"apiurlstring-->:%@", googleurlstring);
  googleurlstring = [self stringByReplacingSpaceAndByAddingPercentEscapesUsingEncoding:googleurlstring];
    [httpManager GET:googleurlstring
          parameters:nil
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSArray *results = [responseObject objectForKey:@"results"];
                 DDLogDebug(@"results %@",results);
                 NSDictionary *resultsFirstObject = [results firstObject];
                 DDLogDebug(@"resultsFirstObject %@",resultsFirstObject);
                 NSString *place_id = [resultsFirstObject objectForKey:@"place_id"];
                 
                 DDLogDebug(@"place_id %@",place_id);
                 
                 [self callGeoCodeapiConverFromPlaceIDAndAddressSearch:place_id addressSearch:kLocationSearchType succes:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                     [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:AgentProfiles clearQueue:YES];
                     [self postNotificationWithLandingAgentProfiles:AgentProfiles success:YES];
                   

                 } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                     [[AppDelegate getAppDelegate]handleModelReturnErrorShowUIAlertViewByView:[AppDelegate getAppDelegate].rootViewController.visibleViewController.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                     [self postNotificationWithLandingAgentProfiles:@[] success:NO];
                    

                 
                 }];
        
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSString *newStr =
                 [[NSString alloc] initWithData:operation.request.HTTPBody
                                       encoding:NSUTF8StringEncoding];
                 
               [self postNotificationWithLandingAgentProfiles:@[] success:NO];
                 
             }];
    

}
#pragma mark locationmanger delegate---------------------------------------------------------------------------------------------------------------
- (void)callChinaGoogleMapPlaceAPIAutoCompleteUserInputSearchAddress:(NSString *)userInputSearchAddress
                                                             success:(void (^)(HandleGoogleAutoCompleteJson * handleGoogleAutoCompleteJson))success
                                                             failure:(failureBlock) failure{
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Map.asmx/GoogleMapPlaceAPIAutoComplete"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"SearchAddress\":\"%@\"}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            userInputSearchAddress,
                            nil];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              NSError *err = nil;
              HandleServerReturnGoogleString* returnString =[[HandleServerReturnGoogleString alloc] initWithString:dstring error:&err];
              
              if (returnString.ErrorCode == 0) {
                  NSString * googleAutoCompleteString = returnString.GoogleResult;
                  
                  
                  googleAutoCompleteString = [googleAutoCompleteString stringByReplacingOccurrencesOfString:@"\n"
                                                                                                 withString:@""];
                  
                  
                  
                  
                  self.handleGoogleAutoCompleteJson =[[HandleGoogleAutoCompleteJson alloc] initWithString:googleAutoCompleteString error:&err];
                  
                  //   DDLogDebug(@"googleAutoCompleteJsonRow  %@",googleAutoCompleteJsonRow);
                  
                  success(self.handleGoogleAutoCompleteJson);
              }else{
                  failure(operation, [self genError:returnString.ErrorCode description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              
          }];
    
    
    
}

- (void)callChinaGoogleMapPlaceAPIGeocodeByPlaceID:(NSString *)placeID
                                          language:(NSString *)language
                            userInputSearchAddress:(NSString *)userInputSearchAddress
                                           success:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                           failure:(failureBlock) failure{
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Map.asmx/GoogleMapPlaceAPIGeocodeByPlaceID"];
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":%@,\"AccessToken\":\"%@\","
                            @"\"Language\":\"%@\",\"PlaceID\":\"%@\",\"LanguageIndex\":%d}",
                            [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
                            language,
                            placeID,[SystemSettingModel shared]
                            .selectSystemLanguageList.Index, nil];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *urllink =
              [NSString stringWithFormat:@"%@%@", kServerAddress,
               @"/Operation.asmx/AddressSearch"];
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              NSError *err = nil;
              HandleServerReturnGoogleString* returnString =[[HandleServerReturnGoogleString alloc] initWithString:dstring error:&err];
              NSString *   googleAutoCompleteString = [returnString.GoogleResult stringByReplacingOccurrencesOfString:@"\n"
                                                                                                           withString:@""];
              NSData *data = [googleAutoCompleteString dataUsingEncoding:NSUTF8StringEncoding];
              
              if (isEmpty(data)) {
                   failure(operation, nil,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              }
              id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
              
              
              NSArray *results = [json objectForKey:@"results"];
              NSDictionary *resultsFirstObject = [results firstObject];
              
              if ([self isEmpty:resultsFirstObject]) {
                  
                  failure(operation, nil,@"Google Return Empty Result",GoogleReturnEmptyResult,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
                  return;
              }
              NSDictionary *googleaddress = @{
                                              @"GoogleAddressLang" : @"en",
                                              @"GoogleAddress" : resultsFirstObject
                                              };
              
              NSString *ArrayjsonString;
              NSError *error;
              NSData *jsonData =
              [NSJSONSerialization dataWithJSONObject:googleaddress
                                              options:NSJSONWritingPrettyPrinted
                                                error:&error];
              if (!jsonData) {
                  DDLogError(@"Got an error: %@", error);
              } else {
                  NSString *jsonString =
                  [[NSString alloc] initWithData:jsonData
                                        encoding:NSUTF8StringEncoding];
                  
                  ArrayjsonString =
                  [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                        withString:@""];
              }
              
              NSString *parameters =
              [NSString stringWithFormat:
               @"{\"MemberID\":%@,\"AccessToken\":\"%@\","
               @"\"UserAddress\":\"%@\",\"GoogleAddressPack\":%@,\"LanguageIndex\":%d}",
               [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
               [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken,
               userInputSearchAddress, ArrayjsonString,[SystemSettingModel shared]
               .selectSystemLanguageList.Index ,nil];
              
              
              
              
              DDLogVerbose(@"apiurlstring-->:%@", urllink);
              DDLogVerbose(@"InputJson-->:%@", parameters);
              
              [manager POST:urllink
                 parameters:@{ @"InputJson" : parameters }
                    success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSString *newStr =
                        [[NSString alloc] initWithData:operation.request.HTTPBody
                                              encoding:NSUTF8StringEncoding];
                        DDLogVerbose(@"operation.request.HTTPBody-->%@", newStr);
                        DDLogVerbose(@"response-->Success");
                        
                        NSString *dstring = [responseObject objectForKey:@"d"];
                        if ([self checkAccessError:dstring]) {
                            failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                            
                        }
                        //// check serverReturnStringError
                        NSError *err = nil;
                        self.HandleServerReturnString =
                        [[HandleServerReturnString alloc] initWithString:dstring
                                                                   error:&err];
                        
                        int error = self.HandleServerReturnString.ErrorCode;
                        
                        if (error == 0 || error == 20 || error == 21 || error == 22 ||
                            error == 23) {
                            NSError *err = nil;
                            self.HandleServerReturnString =
                            [[HandleServerReturnString alloc] initWithString:dstring error:&err];
                            
                            int error = self.HandleServerReturnString.ErrorCode;
                            if (error == 0) {
                                self.searchAgentProfilesHaveTotalCount =
                                [[SearchAgentProfile alloc]
                                 initwithNSDictionary:self.HandleServerReturnString.
                                 Content:error];
                                
                                self.AgentProfiles=self.searchAgentProfilesHaveTotalCount.AgentProfiles;
                                self.searchAgentProfilesTotalCount=[self.searchAgentProfilesHaveTotalCount.TotalCount intValue];
                                
                                     [[[[DBSearchAddressArray query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] fetch] removeAll];
                                [DBSearchAddressArray createDBSearchAddressArrayHaveCount:self.searchAgentProfilesHaveTotalCount placeID:placeID userInputSearchAddress:userInputSearchAddress googleAddressPackDict:googleaddress];

                                
                                
                                success(self.AgentProfiles);
                            }
                            
                            
                            
                        } else {
                            
                            if (error == 40) {
                                
                                self.searchAgentProfilesHaveTotalCount = nil;
                                self.AgentProfiles = nil;
                                
                                failure(operation, nil,JMOLocalizedString(@"search_result_detail__result_not_find", nil),NotRecordFind,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                                
                            }else{
                                
                                failure(operation, [self genError:error description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                                        JMOLocalizedString(@"alert_ok", nil));
                            }
                            
                        }
                        
                        
                    }
                    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSString *newStr =
                        [[NSString alloc] initWithData:operation.request.HTTPBody
                                              encoding:NSUTF8StringEncoding];
                        DDLogError(@"operation.request.HTTPBody-->%@", newStr);
                        DDLogError(@"response-->Error: %@", error);
                        failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                    }];
              
              
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              
          }];
    
    
    
}

- (NSString *)nsDictionaryArrayToInputParametersString:(NSMutableArray *)array {
    NSString *ArrayjsonString;
    NSError *error;
    NSData *jsonData =
    [NSJSONSerialization dataWithJSONObject:array
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];
    
    if (!jsonData) {
        DDLogInfo(@"Got an error: %@", error);
    } else {
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        ArrayjsonString =
        [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    
    return ArrayjsonString;
}

- (void) getReverseGeocode
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    if(![self isEmpty:self.location])
    {
        CLLocationCoordinate2D myCoOrdinate;
        
        myCoOrdinate.latitude = self.location.coordinate.latitude;
        myCoOrdinate.longitude = self.location.coordinate.longitude;
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:myCoOrdinate.latitude longitude:myCoOrdinate.longitude];
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error)
             {
                 [self notLocationOrHaveLocationNotResultFindCallUSAAddress];
                 DDLogDebug(@"failed with error: %@", error);
                 return;
             }
             if(placemarks.count > 0)
             {
                 NSString *MyAddress = @"";
                 NSString *city = @"";
                  self.placemark = [placemarks objectAtIndex:0];
                 if([self.placemark.addressDictionary objectForKey:@"FormattedAddressLines"] != NULL)
                     MyAddress = [[self.placemark.addressDictionary objectForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                 else
                     MyAddress = @"Address Not founded";
                 
                 
                 [self addressSearchFromSubAdministrativeAreaWithSuccess:^(NSMutableArray<AgentProfile> *AgentProfiles) {
                  
                     [self postNotificationWithLandingAgentProfiles:AgentProfiles success:YES];
                     

                 } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                    [self notLocationOrHaveLocationNotResultFindCallUSAAddress];
                     
                 }];
                
                 
                 
                 
                
                 
                 DDLogDebug(@"city %@",city);
                 DDLogDebug(@"MyAddress %@", MyAddress);
             }
         }];
    }
}
-(void)addressSearchFromSubAdministrativeAreaWithSuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                      failure:(failureBlock) failure{
    
    if([self.placemark.addressDictionary objectForKey:@"SubAdministrativeArea"] != NULL){
        NSString *   city = [self.placemark.addressDictionary objectForKey:@"SubAdministrativeArea"];
        [self getGoogleLocationFromCityName:city success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
            if (AgentProfiles.count >= 20) {
                success(AgentProfiles);
                return ;
            }else{
                [self addressSearchFromCityWithSuccess:success failure:failure];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error,NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert, NSString *ok) {
            if ([self isNotResultsFoundAgentProfileInLocationSearchNSError:error errorMessage:errorMessage errorStatus:errorStatus]) {
                 [self addressSearchFromCityWithSuccess:success failure:failure];
            }else{
            failure(operation,error,errorMessage,errorStatus,alert,ok);
            }
        }];
    }else{
        [self addressSearchFromCityWithSuccess:success failure:failure];
        
    }

    
}
-(void)addressSearchFromCityWithSuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                failure:(failureBlock) failure{
    if([self.placemark.addressDictionary objectForKey:@"City"] != NULL){
        
        NSString *   city = [self.placemark.addressDictionary objectForKey:@"City"];
        [self  getGoogleLocationFromCityName:city success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
            
            if (AgentProfiles.count>=20) {
                success(AgentProfiles);
                return ;
            }else{
                [self addressSearchFromCountryWithSuccess:success failure:failure];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error,NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert, NSString *ok) {
            if ([self isNotResultsFoundAgentProfileInLocationSearchNSError:error errorMessage:errorMessage errorStatus:errorStatus]) {
                [self addressSearchFromCountryWithSuccess:success failure:failure];
            }else{
                failure(operation,error,errorMessage,errorStatus,alert,ok);
            }

        }];
    }else{
        [self addressSearchFromCountryWithSuccess:success failure:failure];
    }
    
    
}
-(void)addressSearchFromCountryWithSuccess:(void (^)(NSMutableArray<AgentProfile>* AgentProfiles))success
                                   failure:(failureBlock) failure{
    if([self.placemark.addressDictionary objectForKey:@"Country"] != NULL){
        NSString * city = [self.placemark.addressDictionary objectForKey:@"Country"];
        [self  getGoogleLocationFromCityName:city success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
            
            
            if (AgentProfiles.count>=20) {
                success(AgentProfiles);
                return ;
            }else{
                   failure(nil, [self genError:error description:JMOLocalizedString(@"error_message__server_not_response", nil)],JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error,NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert, NSString *ok) {
             failure(operation,error,errorMessage,errorStatus,alert,ok);
        }];
    }

    
    
}



-(void)postNotificationWithLandingAgentProfiles:(NSArray*)profile success:(BOOL)success{
    NSMutableDictionary * userInfo = [[NSMutableDictionary alloc]init];
    
    if (![self isEmpty:profile]) {
    [userInfo setObject:profile forKey:@"agentProfile"];
         [DBHaveShowLanding createDBHaveShowLanding];
        
    }
    if ([self.searchAgentProfilesHaveTotalCount.TotalCount valid])
        [userInfo setObject:[self.searchAgentProfilesHaveTotalCount.TotalCount stringValue] forKey:@"totalCount"];
   
    [userInfo setObject:@(success) forKey:@"success"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAddressSearchUpdate object:self userInfo:userInfo];
}
-(void)getDBSearchAddressArrayToSearchAgentProfileModel{
    
    DBResultSet * dbSearchAddressArrayResult = [[[[DBSearchAddressArray query]
    whereWithFormat:@"MemberID               = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] limit:1]fetch];

    for (DBSearchAddressArray * dbSearchAddressArray in dbSearchAddressArrayResult) {
    self.searchAgentProfilesHaveTotalCount   = [NSKeyedUnarchiver unarchiveObjectWithData:dbSearchAddressArray.SearchAddressArrayHaveCount];

        [self setAgentProfileFromSearchAgentProfilesTotalCount];
    NSMutableDictionary * userInfo           = [[NSMutableDictionary alloc]init];

        if (![self isEmpty:self.AgentProfiles])
            [userInfo setObject:self.AgentProfiles forKey:@"agentProfile"];
        if ([self.searchAgentProfilesHaveTotalCount.TotalCount valid])
            [userInfo setObject:[self.searchAgentProfilesHaveTotalCount.TotalCount stringValue] forKey:@"totalCount"];
        if ([dbSearchAddressArray.UserInputSearchAddress valid])
            self.userInputSearchAddress=dbSearchAddressArray.UserInputSearchAddress;
        if ([dbSearchAddressArray.PlaceID valid])
            self.dbPlaceID=dbSearchAddressArray.PlaceID;
         [userInfo setObject:[NSNumber numberWithBool:YES] forKey:@"success"];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAddressSearchUpdate object:self userInfo:userInfo];
    }
    
    if (dbSearchAddressArrayResult.count==0) {
        [self postNotificationWithLandingAgentProfiles:nil success:NO];
    }
    
}

-(void)setAgentProfileFromSearchAgentProfilesTotalCount{
    self.AgentProfiles                       = self.searchAgentProfilesHaveTotalCount.AgentProfiles;
    self.searchAgentProfilesTotalCount   =[self.searchAgentProfilesHaveTotalCount.TotalCount intValue];
}

-(BOOL)isNotResultsFoundAgentProfileInLocationSearchNSError:(NSError*)error errorMessage:(NSString*)errorMessage errorStatus:(RequestErrorStatus)errorStatus{
    if ([errorMessage isEqualToString:JMOLocalizedString(@"search_result_detail__result_not_find", nil)]) {
        return YES;
    }
    return NO;
}
@end