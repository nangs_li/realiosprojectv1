//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "UnFollowAgentModel.h"
#import "DBFollowAgentRecord.h"
#import "DBFollowerLog.h"
#import "HandleServerReturnString.h"
#import "Mixpanel.h"

@implementation UnFollowAgentModel

static UnFollowAgentModel *sharedUnFollowAgentModel;




+ (UnFollowAgentModel *)shared {
    @synchronized(self) {
        if (!sharedUnFollowAgentModel) {
            sharedUnFollowAgentModel = [[UnFollowAgentModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedUnFollowAgentModel;
    }
}


-(void)callUnFollowAgentApiByAgentProfileMemberID:(NSString *)agentProfileMemberID success:(successBlock)success
                                          failure:(failureBlock ) failure{
    NSString *urlLink =
    [NSString stringWithFormat:@"%@%@", kServerAddress,
     @"/Operation.asmx/Unfollow"];
    NSString *parameters = [NSString
                            stringWithFormat:
                            @"{\"UniqueKey\":\"%f\",\"MemberID\":%@,\"AccessToken\":\"%@\",\"ToRealMemberID\":%@}",
                            [[NSDate date] timeIntervalSince1970],[LoginBySocialNetworkModel shared].myLoginInfo.MemberID,
                            [LoginBySocialNetworkModel shared].myLoginInfo.AccessToken, agentProfileMemberID,
                            nil];
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
      [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              
              NSError *err = nil;
              HandleServerReturnString *returnString = [[HandleServerReturnString alloc] initWithString:dstring error:&err];
              if (returnString.ErrorCode == 0) {
                  
                  // Mixpanel
                  Mixpanel *mixpanel = [Mixpanel sharedInstance];
                  [mixpanel track:@"Unfollowed Agent"
                       properties:@{
                                    @"Agent member ID":agentProfileMemberID,
                                    @"Agent name":[[AppDelegate getAppDelegate] getCurrentAgentProfileMemberName]
                                    }];
                  
                  [mixpanel track:@"Lost Follower"
                       properties:@{
                                    @"Unfollower member ID":[LoginBySocialNetworkModel shared].myLoginInfo.MemberID.stringValue,
                                    @"distinct_id":agentProfileMemberID,
                                    @"Unfollower name":[LoginBySocialNetworkModel shared].myLoginInfo.MemberName
                                    }];
                  
                  [[LoginBySocialNetworkModel shared].myLoginInfo updateFollowingCount:[LoginBySocialNetworkModel shared].myLoginInfo.FollowingCount - 1];
                  [DBFollowerLog removeLogByMemberID:agentProfileMemberID];
                  [DBFollowAgentRecord removeDBFollowAgentRecordFollowingMemberID:[agentProfileMemberID intValue]];
                  [DBFollowAgentRecord createDBFollowAgentRecordFromFollowingMemberID:[agentProfileMemberID intValue] isFollowing:NO];

                  success(responseObject);
                  [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationNewFeedNeedUpdate object:nil];
              }else{
                  failure(operation, [self genError:returnString.ErrorCode description:returnString.ErrorMsg], returnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              
          }];
    
}

@end