//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

#import "Onepage.h"
#import "Threepage.h"
#import "AgentProfile.h"
#import "TableViewCell.h"
#import "MMDrawerController.h"
@class SPGooglePlacesAutocompleteQuery;

@interface NewsFeedtwopage
    : BaseViewController<UIScrollViewDelegate, UITableViewDataSource,
                         UITableViewDelegate, UISearchDisplayDelegate,
                         UISearchBarDelegate, MMDrawerTouchDelegate,UITextFieldDelegate,UIScrollViewDelegate> {
  NSArray *searchResultPlaces;
  SPGooglePlacesAutocompleteQuery *searchQuery;
  BOOL shouldBeginEditing;
}

@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) IBOutlet UIView *noRecordView;
@property(strong, nonatomic) IBOutlet UIView *searchHeaderView;
@property (strong,nonatomic) IBOutlet UIButton *miniViewButton;
@property(strong, nonatomic) IBOutlet UITextField *searchTextField;
@property(nonatomic, retain) NSMutableArray *agentListingSloganarray;
#pragma mark  swipe animation core
@property(nonatomic, strong) UIImageView *imageFromView;
@property(nonatomic, retain) AgentProfileViewController *Onepageviewcontroller;
@property(nonatomic, retain) ApartmentDetailViewController *Threepageviewcontroller;
@property(nonatomic, strong) NSIndexPath *afterFollowOrUnFollowReloadindexPathForCell;
@property(nonatomic, retain) MMDrawerController *MMDrawerController;
- (void)removeAllImageFromView;
#pragma mark  googlereturnplaceapi;
@property(nonatomic, retain) NSArray *googlereturnplaceapi;
@property(nonatomic, assign) int  tapCounter;
@property(nonatomic, assign) BOOL insertRowAtBottomEnable;
@property(nonatomic, strong) UIViewController * previousHandledViewController;
@property (strong, nonatomic) IBOutlet UILabel *newsFeedNotStartLabel;

#pragma mark  - server return server data array
@property(nonatomic, strong) NSMutableArray *agentProfileArray;
@property (strong, nonatomic) IBOutlet UIView *updateNewsFeedView;

@property (strong, nonatomic) IBOutlet UIButton *updateNewsFeedBtn;

////// - server otherpagepanHandler
//-(void) otherpagepanHandler: (UIPanGestureRecognizer *)gesture;
//// only one language pickerData
@property(strong, nonatomic) NSArray *pickerData;

@end
