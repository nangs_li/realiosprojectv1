//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "NewsFeedtwopage.h"
#import "TableViewCell.h"

#import "Onepage.h"
#import "Threepage.h"
#import <QuartzCore/QuartzCore.h>
#import "UIScrollView+SVPullToRefresh.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import <CoreLocation/CoreLocation.h>
#import "HandleServerReturnString.h"
#import "AgentProfile.h"
#import "AgentListing.h"
#import "Onepage.h"
#import "Threepage.h"
#import "DBNewsFeedArray.h"
#import "NewsFeedAgentprofilesModel.h"
#import "SystemSettingModel.h"
#import "AgentCell.h"
#import "AgentMiniCell.h"
#import "AgentListSectionHeaderView.h"
#import "BaseView.h"
#import "ODRefreshControl.h"
#import "DBNewsFeedLastReadDate.h"
#import <PureLayout/PureLayout.h>
#import "RealPrefetcher.h"
#import "RealPhoneBookShareViewController.h"
// Mixpanel
#import "Mixpanel.h"

#define maxCardSize 80.0f+[RealUtility screenBounds].size.width

@interface NewsFeedtwopage ()<CLLocationManagerDelegate,RDVTabBarControllerDelegate,AgentCellDelegate,MoveToCenterPage,AgentListSectionHeaderDelegate,RealPrefetcherDelegate> {
    CLLocationManager *locationManager;
}
@property(nonatomic) CGPoint viewInputCenter;
@property(nonatomic, assign) CGRect preferCardFrame;
@property (nonatomic, assign) BOOL isMiniView;
@property (nonatomic, assign) BOOL detectScrollAction;
@property (nonatomic, strong) NSMutableDictionary *sectionHeaderDict;
@property (nonatomic, strong) NSMutableDictionary *sectionHeaderOffsetDict;
@property (nonatomic, strong) ODRefreshControl *topRefreshControl;
@property (nonatomic, assign) BOOL isFirstShown;
@end
@implementation NewsFeedtwopage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //// locaiton init
        /* not using
         searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
         searchQuery.radius = 5000.0;
         shouldBeginEditing = YES;
         */
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadTableView];
    self.insertRowAtBottomEnable=YES;
    //    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.delegate getTabBarController].delegate = self;
    
    //// setupthreepagefollowbutton and currentscrollCGPoint
    
    // must back to 0,0
    //// locationmanager init
    //    locationManager = [[CLLocationManager alloc] init];
    //    locationManager.delegate = self;
    //    locationManager.distanceFilter = kCLDistanceFilterNone;
    //    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //
    //    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    //        [locationManager requestWhenInUseAuthorization];
    //
    //    [locationManager startUpdatingLocation];
    //
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        //        [self showPageControl:NO animated:NO];
        //        [[self.delegate getTabBarController]setNavBarHidden:YES animated:NO];
        self.navBarContentView.alpha =0;
    }
    
    DDLogInfo(@"Center will disappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    DDLogInfo(@"Center did disappear");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [RealPrefetcher sharePrefetcher].delegate = self;
    [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles clearQueue:YES];
    [self loadImagesForOnscreenRows];
    [self.delegate getTabBarController].customNavBarController.delegate=self;
    self.detectScrollAction = NO;
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]setNavBarHidden:NO animated:NO];
        [[self.delegate getTabBarController]setTabBarHidden:NO animated:NO];
        [self.view layoutIfNeeded];
        [self setupNavBar];
        [UIView performWithoutAnimation:^{
            self.navBarContentView.alpha =1;
        }];
        self.MMDrawerController.touchDelegate = self;
        [self changePage:MMDrawerSideNone];
    }
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        [self showPageControl:![self isEmpty:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles] animated:self.MMDrawerController.openSide == MMDrawerSideNone];
    }
    if (self.MMDrawerController.openSide != MMDrawerSideNone) {
        [self.MMDrawerController openDrawerSide:self.MMDrawerController.openSide animated:NO completion:nil];
    }
    self.tableView.scrollEnabled = [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count > 0;
    if ([NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count<=0) {
        [self showNoRecordView];
    }else{
        [self hideNoRecordView];
    }
    
    [self tableViewShowInfiniteScrollIfNeed];
    
    [self reloadTableView];
    
    //     [self refreshTableView:YES];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.detectScrollAction = YES;
}

- (void)openSideDidChange:(MMDrawerSide)drawerSide{
    [self changePage:drawerSide];
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.newsFeedNotStartLabel.text=JMOLocalizedString(@"newsfeed__follow_agent_start_receiving", nil);
    // [self.updateNewsFeedBtn setUpAutoScaleButton];
    [self.updateNewsFeedBtn setImageEdgeInsets:UIEdgeInsetsMake(0,-3,0,0)];
    [self setupUpdateNewsFeedBtnTitle];
}
-(void)setupUpdateNewsFeedBtnTitle{
    UITabBarItem * tabBarItem=  [[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items] objectAtIndex:1];
    if (  [tabBarItem.badgeValue isEqualToString:@"1"]) {
        [self.updateNewsFeedBtn setTitle:JMOLocalizedString(@"newsfeed__new_story", nil) forState:UIControlStateNormal];
    }else{
        [self.updateNewsFeedBtn setTitle:JMOLocalizedString(@"newsfeed__new_stories", nil) forState:UIControlStateNormal];
    }
    
}
-(void)setupNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.newsFeedNavBar;
            navBarController.newsFeedTitleLabel.text=JMOLocalizedString(@"newsfeed__title", nil);
            self.miniViewButton = navBarController.newsFeedMiniViewButton;
            [self.miniViewButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [self.miniViewButton addTarget:self action:@selector(switchViewMode:) forControlEvents:UIControlEventTouchUpInside];
            [navBarController.newsFeedConnectAgentButton addTarget:self action:@selector(connectAgentButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isFirstShown = YES;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(followOrUnfollowReloadTableView)
     name:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
     object:nil];
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationShowUpdateNewsFeedView object:nil]
     subscribeNext:^(id x) {
         self.updateNewsFeedView.hidden=NO;
         [self setupUpdateNewsFeedBtnTitle];
     }];
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationHideUpdateNewsFeedView object:nil]
     subscribeNext:^(id x) {
         self.updateNewsFeedView.hidden=YES;
         if (self.isFirstShown) {
             self.isFirstShown = NO;
             [self refreshTableView:NO];
         }
     }];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshTableViewWithoutLoading) name:kNotificationNewFeedNeedUpdate object:nil];
    NSAttributedString *search = [[NSAttributedString alloc]
                                  initWithString:@"Search Agent"
                                  attributes:@{
                                               NSForegroundColorAttributeName : [UIColor lightGrayColor]
                                               }];
    self.searchTextField.attributedPlaceholder = search;
    UIImageView *searchIconImageView =
    [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 18.0f)];
    searchIconImageView.contentMode = UIViewContentModeScaleAspectFit;
    searchIconImageView.image = [UIImage imageNamed:@"icon-search"];
    [self.searchTextField setLeftView:searchIconImageView];
    [self.searchTextField setLeftViewMode:UITextFieldViewModeAlways];
    
    
    CGRect cardFrame =  CGRectMake(0, 0, [RealUtility screenBounds].size.width, [RealUtility screenBounds].size.height - 64-44);
    if (cardFrame.size.height >maxCardSize) {
        cardFrame.size.height = maxCardSize;
    }
    self.preferCardFrame = cardFrame;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    __weak NewsFeedtwopage *weakSelf = self;
    self.tableView.contentInset =UIEdgeInsetsMake(0, 0, 49+sectionHeaderHeight, 0);
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    if (!self.topRefreshControl) {
        self.topRefreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
        [self.topRefreshControl addTarget:self action:@selector(refreshTableViewWithoutLoading) forControlEvents:UIControlEventValueChanged];
    }
    
    
    [self tableViewShowInfiniteScrollIfNeed];
    self.agentProfileArray = [[NSMutableArray alloc] init];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil]
     setTextColor:[AppDelegate getAppDelegate].Greycolor];
    UIGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self.updateNewsFeedView addGestureRecognizer:gesture];
    self.updateNewsFeedView.layer.cornerRadius = 15;
    self.updateNewsFeedView.layer.masksToBounds = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    DDLogInfo(@"didReceiveMemoryWarning");
}
- (void)moveToCenterPage{
    
    [[self.delegate getTabBarController]setTabBarHidden:NO animated:YES];
    [self.MMDrawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        [self changePage:MMDrawerSideNone];
    }];
}

//-(void)showPullToRefrestIfNeed{
//    int totalCount = [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfilesHaveTotalCount.TotalCount intValue];
//    int currentCount = (int)[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count;
//    if (totalCount >currentCount) {
//        self.tableView.showsInfiniteScrolling = YES;
//    }else{
//        self.tableView.showsInfiniteScrolling = NO;
//    }
//}

-(void)refreshTableViewWithLoading{
    [self refreshTableView:YES];
}

-(void)refreshTableViewWithoutLoading{
    [self refreshTableView:NO];
}

-(void)refreshTableView:(BOOL) showLoading{
    if (showLoading) {
        [self showLoading];
    }
    [[NewsFeedAgentprofilesModel shared]callNewsFeedApisuccess:^(NSMutableArray<AgentProfile> *AgentProfiles) {
        [self hideLoading];
        [self.topRefreshControl endRefreshing];
        
        self.tableView.scrollEnabled = AgentProfiles.count > 0;
        if (AgentProfiles.count <=0) {
            [self showNoRecordView];
        }else{
            [self hideNoRecordView];
        }
        [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles= [[NewsFeedAgentprofilesModel shared].pendingNewsfeedAgentProfiles mutableCopy];
        
        [[[[DBNewsFeedLastReadDate query] whereWithFormat:@" MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]
          fetch]removeAll];
        DBNewsFeedLastReadDate * dbNewsFeedLastReadDate= [DBNewsFeedLastReadDate new];
        dbNewsFeedLastReadDate.MemberID =[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ;
        dbNewsFeedLastReadDate.QBID =[LoginBySocialNetworkModel shared].myLoginInfo.QBID ;
        AgentProfile * agentProfile =[[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles firstObject];
        dbNewsFeedLastReadDate.LastReadDate = [agentProfile.AgentListing  getCreationDate];
        [dbNewsFeedLastReadDate commit];
        [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
          objectAtIndex:1] setBadgeValue:nil];
        [[NewsFeedAgentprofilesModel shared] checkbadgeCountToHideOrShowUpdateNewsFeedView];
        [self tableViewShowInfiniteScrollIfNeed];
        [self resetSectionHeaderView];
        [NewsFeedAgentprofilesModel shared].Page = 1;
        self.afterFollowOrUnFollowReloadindexPathForCell = nil;
        [self reloadTableView];
        [self showPageControl:![self isEmpty:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles] animated:YES];
        [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles clearQueue:YES];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:nil errorStatus:errorStatus alert:alert ok:ok];
        [self showPageControl:![self isEmpty:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles] animated:YES];
        [self.topRefreshControl endRefreshing];
        [self hideLoading];
    }];
}

-(void)showPullToRefresh{
    //    if (!self.topRefreshControl.superview) {
    //        [self.tableView addSubview:self.topRefreshControl];
    //    }
}

-(void)hidePullToRefresh{
    //    [self.topRefreshControl removeFromSuperview];
}
#pragma mark house swipe animation start---------------------------------------------------------------------------------------------------------
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.Threepageviewcontroller.didChangeToImageTableView ||self.presentedViewController||self.delegate.disableViewGesture) {
        return NO;
    }
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CGPoint panLocation = [gestureRecognizer locationInView:self.tableView];
    //// if twopage then
    if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
        NSIndexPath *indexPath =
        [self.tableView indexPathForRowAtPoint:panLocation];
        if (indexPath) {
            AgentProfile *profile = [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles[indexPath.section];
            return profile.readyToShow;
        }
        return NO;
    } else {
        return YES;
    }
}
- (void)panGestureCallback:(UIPanGestureRecognizer *)panGesture {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
            DDLogInfo(@"UIGestureRecognizerStateBegan");
            //// refresh after follow  or unfollow only one tableviewcell  threepage start pangesture
            if (screenRect.size.width < panLocation.x) {
                if (![self isEmpty:self.afterFollowOrUnFollowReloadindexPathForCell]) {
                    
                    //                    [self.tableView beginUpdates];
                    //                    [self.tableView reloadRowsAtIndexPaths:@[ self.afterFollowOrUnFollowReloadindexPathForCell ]
                    //                                          withRowAnimation:UITableViewRowAnimationNone];
                    //                    [self.tableView endUpdates];
                    [self reloadTableView];
                }
                
            }
            //// get self.view pangesture x   twopage start pangesture
            
            if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
                DDLogInfo(@"-panLocationstart%f", panLocation.x);
                CGPoint panLocation = [panGesture locationInView:self.tableView];
                //[[[AppDelegate getAppDelegate]getTabBarController]
                // setTabBarHidden:YES];
                NSIndexPath *indexPath =
                [self.tableView indexPathForRowAtPoint:panLocation];
                
                self.afterFollowOrUnFollowReloadindexPathForCell = indexPath;
                
                if (![self isEmpty:indexPath]) {
                    if (![self isEmpty:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles[indexPath.section]]) {
                        self.delegate.currentAgentProfile =
                        [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles[indexPath.section];
                        
                        DDLogInfo(@"indexPathindexPathindexPath%@", indexPath);
                        self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                        self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
                    }
                    
                }
                
                //// create imageivew
                //
                
                CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:indexPath];
                CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
                if (!self.isMiniView) {
                    rectInSuperview.size.height += 53;
                    rectInSuperview.origin.y -=53;
                }
                [self showSpotlightInRect:rectInSuperview];
                DDLogDebug(@"cellRect:%@",NSStringFromCGRect(rectInTableView));
            }
            break;
            
        case UIGestureRecognizerStateChanged:
            break;
            
        case UIGestureRecognizerStateEnded:
            
            // setTabBarHidden:NO have bug when only not move to other page
            
        case UIGestureRecognizerStateCancelled:
            DDLogInfo(@"-panLocationend%f", panLocation.x);
            DDLogInfo(@"UIGestureRecognizerStateCancelled-");
            if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
                DDLogInfo(@"UIGestureRecognizerState- two page");
                
                DDLogInfo(@"self.MMDrawerController.centerContainerView-->%ld",
                          (long)self.MMDrawerController.openSide);
                
                //                [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
                
            } else if (screenRect.size.width < panLocation.x) {
                DDLogInfo(@"UIGestureRecognizerState- three page");
                
            } else if (0 > panLocation.x) {
                DDLogInfo(@"UIGestureRecognizerState- one page");
            }
            break;
            
        default:
            break;
    }
}

- (void)finishPanGestureCallBack:(UIPanGestureRecognizer *)panGesture {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
        [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
        [self hideSpotlight];
    }
    MMDrawerSide drawerSide= self.MMDrawerController.openSide;
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}
//// imageFromView
- (UIImage *)imageFromView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO,
                                           [[UIScreen mainScreen] scale]);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (void)removeAllImageFromView {
    NSArray *subViewArray = [self.delegate.window subviews];
    for (id obj in subViewArray) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            [obj removeFromSuperview];
        }
    }
}




-(IBAction)switchViewMode:(UIButton*)sender{
    self.isMiniView = !self.isMiniView;
    sender.selected =self.isMiniView;
    [self resetSectionHeaderView];
    [self reloadTableView];
}

- (IBAction)connectAgentButtonDidPress:(id)sender {
    [[REPhonebookManager sharedManager] requestAccess:^(BOOL granted) {
        
        if (granted) {
            
            RealPhoneBookShareViewController *shareVC = [RealPhoneBookShareViewController phoneBookShareVCWithShareType:RealPhoneBookShareTypeConnectAgent];
            shareVC.underLayViewController = self;
            BaseNavigationController *navController = [[BaseNavigationController alloc]initWithRootViewController:shareVC];
            [self.delegate overlayViewController:navController onViewController:self];
            
        } else {
            __weak typeof(self) weakSelf = self;
            UIAlertController *alert = [REPhonebookManager noPermissionAlertWithCancelBlock:^(UIAlertAction *action) {
                
            }];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
    


}



//
//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
//    DDLogDebug(@"contentOffset:%@",NSStringFromCGPoint(scrollView.contentOffset));
//    if(scrollView.contentOffset.y > self.searchHeaderView.frame.size.height/2 && targetContentOffset ->y == 0){
//        targetContentOffset->x =0;
//        targetContentOffset->y = self.searchHeaderView.frame.size.height;
//    }
//}

#pragma mark UITableViewDelegate & UITableViewDataSource start-------------------------------------------------------------------------------------
-(UIView*)getSectionHeaderViewWithProfile:(AgentProfile*)profile withSection:(NSInteger)section{
    if (!self.sectionHeaderDict) {
        self.sectionHeaderDict =[[NSMutableDictionary alloc]init];
    }
    
    BaseView* headerContainerView = self.sectionHeaderDict[@(section)];
    AgentListSectionHeaderView *headerView = nil;
    if (!headerContainerView) {
        headerContainerView =[[BaseView alloc]initWithFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, sectionHeaderHeight)];
        headerView =[[AgentListSectionHeaderView alloc]initFromXib:headerContainerView.bounds];
        headerView.clipsToBounds = NO;
        [headerContainerView addSubview:headerView];
        [headerView autoPinEdgesToSuperviewEdges];
        headerContainerView.targetView = headerView.targetView;
        AgentProfile *agentProfile = [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles objectAtIndex:section];
        [headerView configureWithAgentProfile:agentProfile];
        headerContainerView.clipsToBounds = NO;
        [self.sectionHeaderDict setObject:headerContainerView forKey:@(section)];
    }
    
    if (!headerView) {
        headerView = [headerContainerView.subviews firstObject];
    }else{
    }
    CGFloat offset = [self getHeaderOffset:section];
    [headerView adjustHeaderOffset:offset];
    headerView.delegate = self;
    if ([RealUtility isValid:profile]) {
        [headerView configureWithAgentProfile:profile];
    }
    return headerContainerView;
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.tableView && !self.isMiniView) {
        AgentProfile *agentProfile = [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles objectAtIndex:section];
        return [self getSectionHeaderViewWithProfile:agentProfile withSection:section];
    }else{
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.tableView && !self.isMiniView) {
        return sectionHeaderHeight;
    }else{
        return 0;
    }
}

-(void)showNoRecordView{
    self.noRecordView.hidden = NO;
    self.tableView.hidden = YES;
}

-(void)hideNoRecordView{
    self.noRecordView.hidden = YES;
    self.tableView.hidden = NO;
}

- (void)recordHeaderOffset:(CGFloat)offset section:(NSInteger)section{
    if (!self.sectionHeaderOffsetDict) {
        self.sectionHeaderOffsetDict = [[NSMutableDictionary alloc]init];
    }
    
    [self.sectionHeaderOffsetDict setObject:@(offset) forKey:@(section)];
}

-(CGFloat)getHeaderOffset:(NSInteger)section{
    NSNumber *offsetNumber =self.sectionHeaderOffsetDict[@(section)];
    return [offsetNumber floatValue];
}


- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    if ( self.detectScrollAction && scrollView == self.tableView && [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count> 0 && !self.isMiniView) {
        NSIndexPath *firstVisibleIndexPath = [[self.tableView indexPathsForVisibleRows] objectAtIndex:0];
        CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:firstVisibleIndexPath];
        CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
        CGFloat cellOffset = rectInSuperview.origin.y - sectionHeaderHeight;
        CGFloat infoViewOffset = fabs(cellOffset) - [RealUtility screenBounds].size.width;
        UIView *headerContainerView;
        AgentListSectionHeaderView *topHeaderView;
        headerContainerView =  [self getSectionHeaderViewWithProfile:nil withSection:firstVisibleIndexPath.section];
        topHeaderView = [headerContainerView.subviews firstObject];
        [topHeaderView adjustHeaderOffset:infoViewOffset];
        [self recordHeaderOffset:infoViewOffset section:firstVisibleIndexPath.section];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //    self.detectScrollAction = NO;
    [self loadImagesForOnscreenRows];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        //        self.detectScrollAction = NO;
        [self loadImagesForOnscreenRows];
    }
}

- (void)loadImagesForOnscreenRows{
    if (self.visible) {
        if ([NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count > 0){
            NSMutableArray *visiblePaths = [[self.tableView indexPathsForVisibleRows] mutableCopy];
            NSMutableArray *listings = [[NSMutableArray alloc]init];
            for (NSIndexPath *indexPath in visiblePaths){
                if ([NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count > indexPath.section) {
                    AgentProfile *agentProfile =[[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles objectAtIndex:indexPath.section];
                    if (agentProfile.readyToShow && [agentProfile.AgentListing valid]) {
                        [listings addObject:agentProfile.AgentListing];
                    }
                }
            }
            [[RealPrefetcher sharePrefetcher]prefetchAgentListings:listings allContent:NO clearQueue:YES top:NO];
        }
    }
}


- (void)shouldShowAgentProfile:(AgentProfile*)agentProfile atIndexPath:(NSIndexPath*)indexPath{
    if (self.visible) {
        NSMutableArray *visiblePaths = [[self.tableView indexPathsForVisibleRows] mutableCopy];
        if (agentProfile.readyToShow && [visiblePaths containsObject:indexPath]) {
            if ([NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count > indexPath.section) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.tableView reloadData];
                    [self getSectionHeaderViewWithProfile:agentProfile withSection:indexPath.section];
                }];
                if (agentProfile.AgentListing) {
                    [[RealPrefetcher sharePrefetcher]prefetchAgentListings:@[agentProfile.AgentListing] allContent:NO clearQueue:NO top:NO];
                }
            }
        }
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.tableView) {
        return [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count;
    }else{
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    // set number of rows
    DDLogInfo(@"numberOfRowsInSection");
    if (tableView == self.tableView) {
        if (![self isEmpty:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles]) {
            return 1;
        } else {
            return 0;
        }
    } else {
        return [searchResultPlaces count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set height to 116
    if (tableView == self.tableView) {
        if (self.isMiniView) {
            if (indexPath.section ==0) {
                return 88;
            }
            return 88;
        }else{
            return self.preferCardFrame.size.height;
        }
        
    } else {
        return 44;
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    if ([cell isKindOfClass:[AgentCell class]]) {
        AgentCell *agentCell = (AgentCell*)cell;
        [agentCell willEndDisplay];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[AgentCell class]]) {
        AgentCell *agentCell = (AgentCell*)cell;
        [agentCell resetLoadingStatus];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    [self checkMoveToTopForNSIndexPath:indexPath];
    if (![self isEmpty:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles]) {
        if (self.isMiniView) {
            AgentProfile *agentProfile =
            [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles objectAtIndex:indexPath.section];
            NSString *CellIdentifier = @"AgentMiniCell";
            AgentMiniCell *miniCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (miniCell == nil) {
                // table view cell setting
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AgentMiniCell"
                                                             owner:self
                                                           options:nil];
                
                miniCell = [nib objectAtIndex:0];
            }
            //            miniCell.pageControl.hidden = indexPath.row != 0;
            [miniCell configureCell:agentProfile];
            miniCell.tag=indexPath.row;
            miniCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [miniCell.agentImageBtn addTarget:self action:@selector(minCellAgentProfileDidPress:) forControlEvents:UIControlEventTouchUpInside];
            
            return miniCell;
        }else{
            static NSString *CellIdentifier = @"AgentCell";
            
            AgentProfile *agentProfile =
            [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles objectAtIndex:indexPath.section];
            
            AgentCell *agentCell =
            [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!agentCell) {
                agentCell = [[AgentCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:CellIdentifier
                                                   withFrame:self.preferCardFrame
                                                 withProfile:agentProfile];
                agentCell.delegate = self;
                agentCell.agentProfileView.backgroundButton.hidden = NO;
            }
            agentCell.selectionStyle = UITableViewCellSelectionStyleNone;
            agentCell.backgroundView = nil;
            [agentCell configureWithAgentProfile:agentProfile];
            if (!agentProfile.readyToShow) {
                [agentCell willEndDisplay];
            }else{
                [agentCell resetLoadingStatus];
            }
            return agentCell;
        }
    } else {
        DDLogInfo(@"cellForRowAtIndexPath-->%ld", (long)indexPath.section);
        static NSString *CellIdentifier = @"TableViewCell";
        
        TableViewCell *cell = (TableViewCell *)
        [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableViewCell"
                                                         owner:self
                                                       options:nil];
            cell = (TableViewCell *)[nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        DDLogInfo(@"self.delegate=cell;");
        [cell.imageview setImage:[UIImage imageNamed:@"noiamge.gif"]];
        [cell setUpPeronalImagelLayerToCircule];
        
        return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableView) {
        
        DDLogInfo(@"indexPathgfd%ld", (long)indexPath.section);
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        [self minCellDidSelectRowAtIndexPath:indexPath];
    }
}

-(void)resetSectionHeaderView{
    for (UIView *containerView in self.sectionHeaderDict.allValues) {
        [containerView removeFromSuperview];
    }
    
    self.sectionHeaderDict = [[NSMutableDictionary alloc]init];
    self.sectionHeaderOffsetDict = [[NSMutableDictionary alloc]init];
}
#pragma mark SVPullToRefresh method
- (void)insertRowAtBottom {
    DDLogInfo(@"insertRowAtBottom");
    __weak NewsFeedtwopage *weakSelf = self;
    if ([[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles count] > 0 && self.insertRowAtBottomEnable) {
        
        self.insertRowAtBottomEnable=NO;
        [[NewsFeedAgentprofilesModel shared]callNewsFeedApiPage: ++[NewsFeedAgentprofilesModel shared].Page success:^(NSMutableArray<AgentProfile> *AgentProfiles) {
            
            self.afterFollowOrUnFollowReloadindexPathForCell = nil;
            [[RealPrefetcher sharePrefetcher] prefetchAgentProfile:[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles clearQueue:NO];
            [self reloadTableView];
            [self tableViewShowInfiniteScrollIfNeed];
            [self tableViewShowInfiniteScrollIfNeed];
            self.insertRowAtBottomEnable=YES;
            [self.tableView resetState];
        }failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:nil errorStatus:errorStatus alert:alert ok:ok];
            self.insertRowAtBottomEnable=YES;
            [self.tableView resetState];
        }];
        
    }
}

- (void)tableViewShowInfiniteScrollIfNeed{
    NSInteger totalCount = [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfilesHaveTotalCount.TotalCount integerValue];
    if (totalCount==0) {
        self.tableView.showsInfiniteScrolling = YES;
    }
    if (totalCount > [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles.count) {
        self.tableView.showsInfiniteScrolling = YES;
    }else{
        self.tableView.showsInfiniteScrolling = NO;
    }
}








/* Not Using
 #pragma mark locationmanger delegate-----------------------------------------------------------------------------------------------------------
 - (void)locationManager:(CLLocationManager *)manager
 didUpdateLocations:(NSArray *)locations {
 static dispatch_once_t oncePredicate;
 
 // 3
 dispatch_once(&oncePredicate, ^{
 CLLocation *location = [locations lastObject];
 DDLogInfo(@"location.coordinate.latitude-->%f",
 location.coordinate.latitude);
 DDLogInfo(@"location.coordinate.latitude-->%f",
 location.coordinate.longitude);
 searchQuery.location = CLLocationCoordinate2DMake(
 location.coordinate.latitude, location.coordinate.longitude);
 DDLogInfo(@"searchQuery.locationlocation.coordinate.longitude-->%f",
 searchQuery.location.longitude);
 [manager stopUpdatingLocation];
 });
 }
 
 - (void)locationManager:(CLLocationManager *)manager
 didFailWithError:(NSError *)error {
 // Delegate of the location manager, when you have an error
 [manager stopUpdatingLocation];
 DDLogError(@"didFailWithError: %@", error);
 }
 */

-(void)AgentListSectionHeaderProfileDidPress:(AgentProfile *)selectedProfile{
    int section = (int)[[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles indexOfObject:selectedProfile];
    BOOL haveData=  [self setupPagesWithIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    if (!haveData) {
        return;
    }
    
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        [[self.delegate getTabBarController]setTabBarHidden:YES];
        if (finished) {
            [self changePage:MMDrawerSideRight];
        }
    }];
    
}
-(void)minCellAgentProfileDidPress:(UIButton *)sender{
    NSInteger section= sender.tag;
    BOOL haveData=  [self setupPagesWithIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    if (!haveData) {
        return;
    }
    
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        [[self.delegate getTabBarController]setTabBarHidden:YES];
        if (finished) {
            [self changePage:MMDrawerSideRight];
        }
    }];
    
}
- (void)minCellDidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BOOL haveData=  [self setupPagesWithIndexPath:indexPath];
    if (!haveData) {
        return;
    }
    
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        [[self.delegate getTabBarController]setTabBarHidden:YES];
        if (finished) {
            [self changePage:MMDrawerSideLeft];
        }
    }];
}
- (void)AgentCellApartmentBackgroundImageView:(AgentProfile *)profile{
    int section = (int)[[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles indexOfObject:profile];
    BOOL haveData=  [self setupPagesWithIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    if (!haveData) {
        return;
    }
    
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        [[self.delegate getTabBarController]setTabBarHidden:YES];
        if (finished) {
            [self changePage:MMDrawerSideLeft];
        }
    }];
}

-(void) changePage:(MMDrawerSide)drawerSide{
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}

#pragma mark tabBarControllerDelegate (Move To Up)-----------------------------------------------------------------------------------------------------------
- (BOOL)tabBarController:(RDVTabBarController *)tabBarController
shouldSelectViewController:(UIViewController *)viewController {
    
    self.tapCounter++;
    
    
    // rule out possibility when the user taps on two different tabs very fast
    BOOL hasTappedTwiceOnOneTab = NO;
    
    if(self.previousHandledViewController == viewController) {
        
        hasTappedTwiceOnOneTab = YES;
    }
    
    self.previousHandledViewController = viewController;
    
    
    
    
    
    if(self.tapCounter == 2 && hasTappedTwiceOnOneTab) {
        
        [self.tableView setContentOffset:CGPointZero animated:YES];
        [self refreshTableViewWithoutLoading];
        // do something when tapped twice
        self.previousHandledViewController=nil;
        self.tapCounter = 0;
        return NO; // or YES when you want the default engine process the event
        
    } else if(self.tapCounter == 1) {
        
        if (viewController == self.navigationController) {
            DDLogInfo(@"viewController==self%@", viewController);
            if (tabBarController.selectedIndex == 1) {
                [self.tableView setContentOffset:CGPointZero animated:YES];
            }
        }
        dispatch_time_t popTime =
        dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            self.tapCounter = 0;
            self.previousHandledViewController=nil;
        });
        
        
        return YES; // or YES when you want the default engine process the event
    }
    return YES;
}

- (BOOL)setupPagesWithIndexPath:(NSIndexPath*)indexPath{
    self.afterFollowOrUnFollowReloadindexPathForCell = indexPath;
    
    if (![self isEmpty:indexPath]) {
        self.delegate.currentAgentProfile = [NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles[indexPath.section];
        if (![self.delegate.currentAgentProfile haveAgentListing]) {
            return NO;
        }
        DDLogInfo(@"indexPathindexPathindexPath%@", indexPath);
        self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
        self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
    }else{
        return NO;
    }
    //
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    rectInSuperview.size.height += 53;
    rectInSuperview.origin.y -=53;
    [self showSpotlightInRect:rectInSuperview];
    return YES;
    DDLogDebug(@"cellRect:%@",NSStringFromCGRect(rectInSuperview));
}

- (void)reloadTableView{
    [self.tableView reloadData];
    [self loadImagesForOnscreenRows];
}

-(void)followOrUnfollowReloadTableView{
    [self reloadTableView];
}


-(void) prefeteherDidFinishFetching:(id)object{
    if([object isKindOfClass:[AgentProfile class]]){
        AgentProfile *profile = object;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"MemberID == %d",profile.MemberID];
        NSInteger index = [[NewsFeedAgentprofilesModel shared].newsfeedAgentProfiles indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return [predicate evaluateWithObject:obj];
        }];
        NSMutableArray *visiblePaths = [[self.tableView indexPathsForVisibleRows] mutableCopy];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
        if ([visiblePaths containsObject:indexPath]) {
            [self shouldShowAgentProfile:object atIndexPath:indexPath];
        }
        
    }else{
        
    }
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    [self.tableView setContentOffset:CGPointZero animated:YES];
    [self refreshTableViewWithoutLoading];
    // do something when tapped twice
    self.previousHandledViewController=nil;
    self.tapCounter = 0;
    self.updateNewsFeedView.hidden=YES;
}
-(void)checkMoveToTopForNSIndexPath:(NSIndexPath *)nsindexPath{
    if (nsindexPath.row==0&&nsindexPath.section==0) {
        self.tableView.showsInfiniteScrolling = YES;
    }
    
}
@end
