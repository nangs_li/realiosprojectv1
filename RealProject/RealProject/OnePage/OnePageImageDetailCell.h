//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnePageImageDetailCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UIImageView *zoomimage;
@property(strong, nonatomic) IBOutlet UIImageView *imageview;
@property(strong, nonatomic) IBOutlet UIImageView *bottomGapImageView;
@property(strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property(strong, nonatomic) NSString *urlString;

-(void) configureCell:(NSDictionary*)dict;
@end
