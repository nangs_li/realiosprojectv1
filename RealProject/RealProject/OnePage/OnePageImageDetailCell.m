//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "OnePageImageDetailCell.h"
#import "UIImage+Utility.h"

#define shadowColors @[[UIColor colorWithRed:180/255.0f green:180/255.0f blue:180/255.0f alpha:1.0],[UIColor colorWithRed:219/255.0f green:219/255.0f blue:219/255.0f alpha:1.0]]

@implementation OnePageImageDetailCell

- (void)awakeFromNib {
  // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  // super setSelected

  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

-(void) configureCell:(NSDictionary*)dict{
    NSString *photocoverurl = dict[@"imageURL"];
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"%@", photocoverurl]];
    self.urlString = photocoverurl;
    self.imageview.contentMode = UIViewContentModeScaleAspectFill;
    self.imageview.clipsToBounds = YES;
    [self.imageview loadImageURL:url withIndicator:YES];
}

- (void)layoutSubviews{
    [super layoutSubviews];
         self.bottomGapImageView.image =[UIImage add_imageWithGradient:shadowColors size:self.bottomGapImageView.frame.size direction:ADDImageGradientDirectionVertical];
}
@end
