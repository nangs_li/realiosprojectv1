//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "SlidingViewManager.h"
#import "MWPhotoBrowser.h"
#import "MWPhoto+Custom.h"

@interface Onepage
    : BaseViewController<UIScrollViewDelegate, UIGestureRecognizerDelegate,
                         UITableViewDataSource, UITableViewDelegate,
                         UIActionSheetDelegate, MWPhotoBrowserDelegate>

@property(nonatomic, strong) UITableView *imagetableview;
@property(strong, nonatomic) SlidingViewManager *svm;
@property(nonatomic, assign) BOOL hidepersonalimage;
@property(nonatomic, assign) BOOL presentToMWPhotoBrowser;
@property(strong, nonatomic) IBOutlet UIScrollView *scrollview;
#pragma mark posting detail
@property(strong, nonatomic) IBOutlet UILabel *perviewnumberofbedroomlabel;
@property(strong, nonatomic) IBOutlet UILabel *perviewnumberofbathroomlabel;
@property(strong, nonatomic) IBOutlet UILabel *perviewtypeofspace;
@property(strong, nonatomic) IBOutlet UILabel *perviewprice;
@property(strong, nonatomic) IBOutlet UILabel *perviewsize;
@property(strong, nonatomic) IBOutlet UIImageView *perviewcoverphoto;
@property(strong, nonatomic) IBOutlet UILabel *perviewunqiue;
@property(strong, nonatomic) IBOutlet UILabel *perviewreason1;
@property(strong, nonatomic) IBOutlet UILabel *perviewreason2;
@property(strong, nonatomic) IBOutlet UILabel *perviewreason3;
@property(strong, nonatomic) IBOutlet UIImageView *personalimage;
@property(strong, nonatomic) IBOutlet UILabel *membername;
@property(strong, nonatomic) IBOutlet UILabel *followercount;
@property(strong, nonatomic) IBOutlet UILabel *typeofspace;
@property(strong, nonatomic) IBOutlet UILabel *street;

#pragma mark only one language pickerData
@property(strong, nonatomic) NSArray *pickerData;

@property(nonatomic, retain) NSMutableArray *tableData;

#pragma mark mwPhotoBrowser
@property(strong, nonatomic) NSMutableArray *mwPhotosBrowerPhotoArray;
@property(strong, nonatomic) MWPhotoBrowser *mwPhotoBrowser;
#pragma mark setupOnePage
- (void)setupOnePage:(BOOL)fade;

@end
