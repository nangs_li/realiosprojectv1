//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Onepage.h"
#import "UIScrollView+TwitterCover.h"

#import "Twopage.h"

#import <QuartzCore/QuartzCore.h>
//#import "SlidingViewManager.h"
#import "OnePageImageDetailCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ReportProblemModel.h"
#import "SystemSettingModel.h"
#import "BaseNavigationController.h"
@interface Onepage ()

@end
@implementation Onepage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:NO];
  // dispatch_async(
  //    dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
  //  [self.scrollview addTwitterCoverWithImage:self.perviewcoverphoto.image];
  // 耗时的操作

  //   });
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.imagetableview.separatorStyle = UITableViewCellSeparatorStyleNone;
  DDLogInfo(@"OnepageOnepage");
}

- (void)viewDidLoad {
  [super viewDidLoad];
}
- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [self.scrollview removeTwitterCoverView];
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  if (self.presentToMWPhotoBrowser) {
    self.presentToMWPhotoBrowser = NO;
  } else {
    [self.scrollview setContentOffset:CGPointMake(0.00, -70) animated:YES];
    [self.svm slideViewOut];
  }
}
- (void)setupOnePage:(BOOL)fade {
  DDLogInfo(@"setupOnePage-");
    self.delegate.onePageMwPhotoBrowserNavigationController=self.mwPhotoBrowser;
  self.scrollview = [[UIScrollView alloc]
      initWithFrame:CGRectMake(0, 102, self.view.bounds.size.width, 210)];

  [self.scrollview setScrollEnabled:YES];
  self.scrollview.delegate = self;
  [self.scrollview setContentSize:CGSizeMake(320, 800)];

  [self.view addSubview:self.scrollview];

  UIView *notificationView = [[UIView alloc]
      initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 468)];

  self.imagetableview =
      [[UITableView alloc] initWithFrame:notificationView.frame
                                   style:UITableViewStylePlain];
  self.imagetableview.delegate = self;
  self.imagetableview.dataSource = self;
  self.imagetableview.userInteractionEnabled = YES;

  [notificationView addSubview:self.imagetableview];

  self.svm = [[SlidingViewManager alloc] initWithInnerView:notificationView
                                             containerView:self.view];
  // Add a left swipe gesture recognizer

  ////personal image

  self.AgentProfile = self.delegate.currentAgentProfile;
  [self.imagetableview reloadData];
  DDLogInfo(@"self.AgentProfile.AgentListing toJSONString-->%@",
            [self.AgentProfile.AgentListing toJSONString]);

  self.membername.text = self.AgentProfile.MemberName;
  NSURL *url = [NSURL URLWithString:self.AgentProfile.AgentPhotoURL];
  if (fade) {
 
      [self.personalimage loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
          if (image) {
              self.personalimage.alpha = 0.0;
              [UIView animateWithDuration:1.0
                               animations:^{
                                   self.personalimage.alpha = 1.0;
                               }];
          }
      }];
    DDLogInfo(@"setupOnePageFade");
  } else {
   [self.personalimage loadImageURL:url withIndicator:nil withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
       
   }];
    DDLogInfo(@"setupOnePageNotFade");
  }
  [self.delegate setupcornerRadius:self.personalimage];

  ////postindetail setting
  self.tableData = [[NSMutableArray alloc]
      initWithObjects:@"Amazing potential", @"The only one", @"Fire sale",
                      @"Super high yield", @"Final call", @"Nothing to lose",
                      @"With good investment potential",
                      @"The only opportunity in market", @"Below market price",
                      @"High return / High yield", @"Last call offer",
                      @"Immediate sale", nil];
  [self.perviewprice
      setText:self.AgentProfile.AgentListing.PropertyPriceFormattedForRoman];
  MetricList *metric;
  if (![self isEmpty:[SystemSettingModel shared].metricaccordingtolanguage]) {
    if (self.AgentProfile.AgentListing.SizeUnitType > 1) {
      metric = [[SystemSettingModel shared]
                    .metricaccordingtolanguage objectAtIndex:1];
    } else {
      metric = [[SystemSettingModel shared]
                    .metricaccordingtolanguage
          objectAtIndex:self.AgentProfile.AgentListing.SizeUnitType];
    }
  }

  self.perviewsize.text = [NSString
      stringWithFormat:@"%d %@", self.AgentProfile.AgentListing.PropertySize,
                       metric.NativeName];

  [self.perviewnumberofbathroomlabel
      setText:[NSString stringWithFormat:@"%d", self.AgentProfile.AgentListing
                                                    .BathroomCount]];
  [self.perviewnumberofbedroomlabel
      setText:[NSString stringWithFormat:@"%d", self.AgentProfile.AgentListing
                                                    .BedroomCount]];
  self.followercount.text =
      [NSString stringWithFormat:@"%d", self.AgentProfile.FollowerCount];

  Reasons *reasons = [self.AgentProfile.AgentListing.Reasons objectAtIndex:0];
  [self.perviewreason1 setText:reasons.Reason1];
  [self.perviewreason2 setText:reasons.Reason2];
  [self.perviewreason3 setText:reasons.Reason3];

  SpaceTypeList *spacetype;
  if (self.AgentProfile.AgentListing.PropertyType == 0) {
    spacetype = [[SystemSettingModel shared]
                     .typeofspaceoneaccordingtolanguage
        objectAtIndex:self.AgentProfile.AgentListing.SpaceType];
  } else if (self.AgentProfile.AgentListing.PropertyType == 1) {
    spacetype = [[SystemSettingModel shared]
                     .typeofspacetwoaccordingtolanguage
        objectAtIndex:self.AgentProfile.AgentListing.SpaceType];
  }

  self.typeofspace.text = spacetype.SpaceTypeName;

  [self.perviewunqiue
      setText:self.tableData[self.AgentProfile.AgentListing.SloganIndex]];

  AddressUserInput *addressuserinput =
      [self.AgentProfile.AgentListing.AddressUserInput objectAtIndex:0];
  self.street.text = addressuserinput.Street;

  ////posting photo setting
  if ([self.AgentProfile.AgentListing.Photos count] > 0) {
    Photos *photo = [self.AgentProfile.AgentListing.Photos objectAtIndex:0];

    NSString *photocoverurl = photo.URL;

    NSURL *url =
        [NSURL URLWithString:[NSString stringWithFormat:@"%@", photocoverurl]];
    DDLogInfo(@"photocoverurl-->%@",
              [NSString stringWithFormat:@"%@", photocoverurl]);
    self.perviewcoverphoto.contentMode = UIViewContentModeScaleAspectFill;
    self.perviewcoverphoto.clipsToBounds = YES;

    if (fade) {
      //&& cacheType == SDImageCacheTypeNone

        [self.personalimage loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                self.personalimage.alpha = 0.0;
                [UIView animateWithDuration:1.0
                                 animations:^{
                                     self.personalimage.alpha = 1.0;
                                 }];
            }
        }];


    } else {
        [self.personalimage loadImageURL:url withIndicator:nil withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];

    }
  }

  [self getImageFromAgentListingPhotosToMWPhotoBrower];
}

- (void)dealloc {
  [self.scrollview removeTwitterCoverView];
}

#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  // set number of rows

  DDLogInfo(
      @"[self.delegate.currentAgentProfile.AgentListing.Photos count]-->%lu",
      (unsigned long)[self.AgentProfile.AgentListing.Photos count]);
  return [self.AgentProfile.AgentListing.Photos count];
}
- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  // set height to 116

  return 210;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"OnepageImageDetailCell";

  OnePageImageDetailCell *cell = (OnePageImageDetailCell *)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    // table view cell setting
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OnePageImageDetailCell"
                                                 owner:self
                                               options:nil];

    cell = (OnePageImageDetailCell *)[nib objectAtIndex:0];
  }

  DDLogInfo(@"indexPath.rowindexPath.row-->%ld", (long)indexPath.row);
  //*** Terminating app due to uncaught exception 'NSRangeException'
  //-[__NSArrayM objectAtIndex:]: index 1 beyond bounds [0 .. 0]'

  DDLogInfo(@"self.AgentProfile.AgentListing.Photos%@",
            self.AgentProfile.AgentListing.Photos);
  Photos *photo =
      [self.AgentProfile.AgentListing.Photos objectAtIndex:indexPath.row];

  NSString *photocoverurl = photo.URL;

  NSURL *url =
      [NSURL URLWithString:[NSString stringWithFormat:@"%@", photocoverurl]];

  cell.urlString = photocoverurl;
  cell.imageview.contentMode = UIViewContentModeScaleAspectFill;
  cell.imageview.clipsToBounds = YES;

    [self.personalimage loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            self.personalimage.alpha = 0.0;
            [UIView animateWithDuration:1.0
                             animations:^{
                                 self.personalimage.alpha = 1.0;
                             }];
        }
    }];

  return cell;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"didSelectRowAtIndexPath%ld", (long)indexPath.row);
  int imageCellIndex = 0;
  self.mwPhotoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
  self.mwPhotoBrowser.displayActionButton = YES;
  self.mwPhotoBrowser.displayNavArrows = NO;
  self.mwPhotoBrowser.displaySelectionButtons = NO;
  self.mwPhotoBrowser.zoomPhotosToFill = YES;
  self.mwPhotoBrowser.alwaysShowControls = NO;
  self.mwPhotoBrowser.autoPlayOnAppear = NO;
  self.mwPhotoBrowser.enableGrid = YES;
  self.mwPhotoBrowser.startOnGrid = NO;

  for (MWPhoto *mwphoto in self.mwPhotosBrowerPhotoArray) {
    OnePageImageDetailCell *cell =
        (OnePageImageDetailCell *)[tableView cellForRowAtIndexPath:indexPath];

    if ([cell.urlString isEqualToString:[mwphoto.photoURL absoluteString]]) {
      [self.mwPhotoBrowser setCurrentPhotoIndex:imageCellIndex];

      BaseNavigationController *nc = [[BaseNavigationController alloc]
          initWithRootViewController:self.mwPhotoBrowser];
      nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
      nc.navigationBar.titleTextAttributes =
          @{NSForegroundColorAttributeName : [UIColor whiteColor]};
      self.presentToMWPhotoBrowser = YES;
        self.delegate.onePageMwPhotoBrowserNavigationController=nc;
      [self presentViewController:nc animated:YES completion:nil];

      break;
    }

    imageCellIndex++;
  }
}

#pragma mark - MWPhotoBrowserDelegate---------------------------------------------------------------------------------------------------
- (void)getImageFromAgentListingPhotosToMWPhotoBrower {
  dispatch_queue_t backgroundQueue =
      dispatch_queue_create("com.mycompany.myqueue", 0);
  dispatch_barrier_async(backgroundQueue, ^{
    self.mwPhotosBrowerPhotoArray = [[NSMutableArray alloc] init];
    for (Photos *photo in self.AgentProfile.AgentListing.Photos) {
      NSString *photocoverurl = photo.URL;
      NSURL *url = [NSURL
          URLWithString:[NSString stringWithFormat:@"%@", photocoverurl]];
      [self.mwPhotosBrowerPhotoArray addObject:[MWPhoto photoWithURL:url]];
    }

  });
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
  return self.mwPhotosBrowerPhotoArray.count;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
               photoAtIndex:(NSUInteger)index {
  if (index < self.mwPhotosBrowerPhotoArray.count)
    return [self.mwPhotosBrowerPhotoArray objectAtIndex:index];
  return nil;
}

#pragma mark -  Drop Down scroll Animation--------------------------------------------------------------------------------------------------------
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  //  DDLogInfo(@"scrollViewDidScroll-->%f", scrollView.contentOffset.y);
  //*****here
  if (self.scrollview.contentOffset.y > 0) {
    [self.scrollview setContentOffset:CGPointMake(0, 0) animated:NO];
  }

  if (self.scrollview.contentOffset.y < -50 && [self.svm visible]) {
    [self.scrollview setContentOffset:CGPointMake(0.00, -70) animated:YES];
    DDLogInfo(@"[self.scrollview setContentOffset:CGPointMake(0.00, -70)]");
  }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)aScrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset {
  DDLogInfo(@"scrollViewWillEndDragging");
  DDLogInfo(@"ScrollView.contentOffset.y-->%f", aScrollView.contentOffset.y);
  //  [self.scrollview setContentOffset:CGPointMake(0.00, -70) animated:YES];

  if (aScrollView == self.imagetableview) {
    DDLogInfo(@"aScrollView == self.imagetableview");
    if (aScrollView.contentOffset.y < -50) {
      DDLogInfo(@"load more rows top");
      DDLogInfo(@"aScrollView.contentOffset.y-->%f",
                aScrollView.contentOffset.y);

      [self.scrollview setContentOffset:CGPointMake(0.00, -70)];

      [self.scrollview setContentOffset:CGPointMake(0.00, 0) animated:YES];
      [self.svm slideViewOut];
      DDLogInfo(@"aScrollView == self.imagetableview");
      self.personalimage.layer.zPosition = 1;
      [self.delegate setupcornerRadius:self.personalimage];
    }
  } else {
    if (self.scrollview.contentOffset.y < -50) {
      DDLogInfo(@"load more rows top");
      DDLogInfo(@"aScrollView == self.tableview");

      [self.svm slideViewIn];
      [self.delegate setupcornerRadius:self.personalimage];
      self.personalimage.layer.zPosition = 1;
    }
  }
}

#pragma mark -   ReportProblem--------------------------------------------------------------------------------------------
- (void)actionSheet:(UIActionSheet *)actionSheet
    clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (buttonIndex == 0) {
    if ([self.delegate networkConnection]) {
      [self loadingAndStopLoadingAfterSecond:10];
    }

    [self reportThisListingProblemToServer];
  }
}
- (IBAction)reportListingProblem:(id)sender {
  UIActionSheet *sheet =
      [[UIActionSheet alloc] initWithTitle:nil
                                  delegate:self
                         cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                    destructiveButtonTitle:nil
                         otherButtonTitles:@"Report Problem", nil];

  [sheet showInView:self.view];
}

- (void)reportThisListingProblemToServer {
  [[ReportProblemModel shared] callReportProblemApiByProblemType:@"4"
      toMemberID:[NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
      toListingID:[NSString
                      stringWithFormat:@"%d", self.AgentProfile.AgentListing
                                                  .AgentListingID]
      description:@""
      success:^(id responseObject) {

        [[[UIAlertView alloc] initWithTitle:@"Notice"
                                    message:@"Report Problem Successful"
                                   delegate:self
                          cancelButtonTitle:@"OK!"
                          otherButtonTitles:nil] show];

           [self hideLoadingHUD];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                NSString *ok) {
           [self hideLoading];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                      errorMessage:errorMessage
                                                       errorStatus:errorStatus
                                                             alert:alert
                                                                ok:ok];
      }];
}

@end