//
//  RealChangeEmailViewController.h
//  productionreal2
//
//  Created by Alex Hung on 30/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealChangeEmailViewController : BaseViewController
@property (nonatomic,strong) IBOutlet UILabel *oldEmail;
@property (nonatomic,strong) IBOutlet UITextField *passwordTextField;
@property (nonatomic,strong) IBOutlet UITextField *emailTextField;

@property (nonatomic,strong) IBOutlet UIButton *changeButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *textFieldErrorLabel;
@end
