//
//  RealChangeEmailViewController.m
//  productionreal2
//
//  Created by Alex Hung on 30/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealChangeEmailViewController.h"
#import "LoginByEmailModel.h"

// Mixpanel
#import "Mixpanel.h"

#define textFieldPlaceHolderColor [UIColor colorWithRed:193/255.0 green:193/255.0 blue:193/255.0 alpha:1.0]
@interface RealChangeEmailViewController () <UITextFieldDelegate>

@end

@implementation RealChangeEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    self.oldEmail.text =  [NSString stringWithFormat:@"%@:\n%@",JMOLocalizedString(@"change_email__current_email", nil),[LoginByEmailModel shared].realNetworkMemberInfo.Email];
    
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common_password", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    self.passwordTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.passwordTextField.frame.size];
    self.passwordTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.passwordTextField.delegate = self;
    
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"change_email__new_email", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    self.emailTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.emailTextField.frame.size];
    self.emailTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.emailTextField.delegate = self;
    [self.changeButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    [self.changeButton setTitle:JMOLocalizedString(@"common__change", nil)
                       forState:UIControlStateNormal];
}
-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"change_email__title", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closeButton:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (BOOL)emailFormatIsValid:(NSString*)email{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/va ... l-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(IBAction)changeButtonDidPress:(id)sender{
    if ([self checkInputIsValidNotAlertTitle]) {
        if([[AppDelegate getAppDelegate]isEmpty:[LoginByEmailModel shared].realNetworkMemberInfo.Email])
            return;
        if([[AppDelegate getAppDelegate]isEmpty:self.emailTextField.text])
            return;
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Changed Email Address"
             properties:@{
                          @"Old Email" : [LoginByEmailModel shared].realNetworkMemberInfo.Email,
                          @"New Email" : self.emailTextField.text
                          }];
        [mixpanel.people set:@"$email" to:self.emailTextField.text];
        
        NSString *email = self.emailTextField.text;
        NSString *password = self.passwordTextField.text;
        NSString *userId = [LoginByEmailModel shared].realNetworkMemberInfo.UserID;
        NSString *accessToken = [LoginByEmailModel shared].realNetworkMemberInfo.AccessToken;
        [[LoginByEmailModel shared]callRealNetworkChangeEmailApiEmail:email passwordMD5:password userID:userId accessToken:accessToken Success:^(id response) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self closeButton:nil];
                        [LoginByEmailModel shared].realNetworkMemberInfo.Email=self.emailTextField.text;
            }];
            [self showAlertWithTitle:nil message:JMOLocalizedString(Emailhasbeenchangedsuccessfully, nil) withAction:action];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            if (errorStatus==AccessErrorOtherUserLoginThisAccount) {
                [self showAlertWithTitle:nil message:JMOLocalizedString(@"error_message__password_incorrect", nil)];
            }else{
                [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                              errorMessage:errorMessage
                                                               errorStatus:errorStatus
                                                                     alert:alert
                                                                        ok:ok];

            }
 
        }];
    }else{
           [self showAlertWithTitle:nil message:self.textFieldErrorLabel.text];
        
    }
}

-(IBAction)closeButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];	
}
-(void)setupTextFieldCheckError{
    [[self.passwordTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];
    
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];
    
}
-(BOOL)checkInputIsValidNotAlertTitle{
    BOOL valid = YES;
    NSString *password = self.passwordTextField.text;
    NSString *email = self.emailTextField.text;
    
    if (![RealUtility isValid:password]) {
         self.textFieldErrorLabel.text=JMOLocalizedString(Passwordisrequired, nil);
        valid= NO;
    }else if (![RealUtility isValid:email]){
       self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteravalidemailaddress, nil);
        valid= NO;
    }else if(![self emailFormatIsValid:email]){
        valid = NO;
       self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteravalidemailaddress, nil);
    }
    
    return valid;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setupKeyBoardDoneBtnAction{
     [self.emailTextField setReturnKeyType:UIReturnKeyDone];
    
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x){
        
        [self changeButtonDidPress:self];
    }];
}
@end
