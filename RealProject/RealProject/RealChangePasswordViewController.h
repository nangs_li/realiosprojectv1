//
//  RealChangePasswordViewController.h
//  productionreal2
//
//  Created by Alex Hung on 30/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealChangePasswordViewController : BaseViewController <UITextFieldDelegate>
@property (nonatomic,strong) IBOutlet UILabel *changePasswordTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *changePasswordDescriptionLabel;
@property (nonatomic,strong) IBOutlet UITextField *oldPasswordTextField;
@property (nonatomic,strong) IBOutlet UITextField *passwordTextField;
@property (nonatomic,strong) IBOutlet UITextField *confirmPasswordTextField;
@property (nonatomic,strong) IBOutlet UIButton *changeButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *textFieldErrorLabel;
@property (strong, nonatomic) IBOutlet UIView *resetPasswordContainerView;
@property (strong, nonatomic) IBOutlet UIView *backdropView;
@end
