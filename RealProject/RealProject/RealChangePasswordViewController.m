//
//  RealChangePasswordViewController.m
//  productionreal2
//
//  Created by Alex Hung on 30/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealChangePasswordViewController.h"
#import "LoginByEmailModel.h"
#define textFieldPlaceHolderColor [UIColor colorWithRed:193/255.0 green:193/255.0 blue:193/255.0 alpha:1.0]

// Views
#import "RealArrowCell.h"

// Controllers
#import "SettingResetPassword.h"

@interface RealChangePasswordViewController ()

@end

@implementation RealChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    NSString *changeTitle =[NSString stringWithFormat:@"<blue>CHANGE</blue><white>PASSWORD</white>"];
    [self.changePasswordTitleLabel setAttributedTextWithMarkUp:changeTitle];
    self.oldPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common_password", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    self.oldPasswordTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.oldPasswordTextField.frame.size];
    self.oldPasswordTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.oldPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.oldPasswordTextField.delegate = self;
    
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"change_password__new_password", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    self.passwordTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.passwordTextField.frame.size];
    self.passwordTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.passwordTextField.delegate = self;
    
    self.confirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"change_password__confirm_new_password", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    self.confirmPasswordTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.confirmPasswordTextField.frame.size];
    self.confirmPasswordTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.confirmPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.confirmPasswordTextField.delegate = self;
    [self.changeButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
    
    [self setupResetPasswordContainerView];
    [self setupBackdropView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    [self.changeButton setTitle:JMOLocalizedString(@"common__change", nil)
                      forState:UIControlStateNormal];
}

- (void)setupResetPasswordContainerView
{
    RealArrowCell *cell = [[RealArrowCell alloc] init];
    cell.contentView.backgroundColor = [UIColor settingsRowBlueColor];
    cell.leftLabel.text = JMOLocalizedString(@"reset_password__title", nil);
    cell.separatorView.hidden = YES;
    cell.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_resetPasswordContainerView addSubview:cell.contentView];
    [cell.contentView autoPinEdgesToSuperviewEdges];
    
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetDidPress)];
    gr.numberOfTapsRequired = 1;
    
    [cell.contentView addGestureRecognizer:gr];
}

- (void)resetDidPress
{
    SettingResetPassword *vc = [[SettingResetPassword alloc]initWithNibName:@"SettingResetPassword" bundle:nil];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setupBackdropView
{
    _backdropView.backgroundColor = [UIColor settingsRowBlueColor];
    UIDotImageView *dotView = [UIDotImageView newAutoLayoutView];
    [_backdropView addSubview:dotView];
    [dotView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [dotView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:25.0];
    [dotView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:25.0];
    [dotView autoSetDimension:ALDimensionHeight toSize:1.0];
}

-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"change_password__title", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closeButton:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

-(BOOL)inputIsValid{
    int passwordMinLength = 4;
    int passwordMaxLength = 20;
    BOOL valid = YES;
    NSString *oldPassword = self.oldPasswordTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *confirmPassword = self.confirmPasswordTextField.text;
    
    if (![RealUtility isValid:oldPassword]) {
        [self showAlertWithTitle:nil message:JMOLocalizedString(Passwordisrequired, nil)];
        valid= NO;
    }else if (![RealUtility isValid:password]){
        [self showAlertWithTitle:nil message:JMOLocalizedString(Pleaseenteranewpassword, nil)];
        valid= NO;
    }else if([RealUtility isValid:password]){
        
        if (password.length < passwordMinLength || password.length > passwordMaxLength ) {
            [self showAlertWithTitle:nil message:JMOLocalizedString(PasswordlengthmustbebetweenCharacters, nil)];
            valid = NO;
        } else if (![RealUtility isValid:confirmPassword]){
            [self showAlertWithTitle:nil message:JMOLocalizedString(Pleaseconfirmyourpassword, nil)];
            valid= NO;
        }else if([RealUtility isValid:confirmPassword]){
            if (![confirmPassword isEqualToString:password]) {
                [self showAlertWithTitle:nil message:JMOLocalizedString(Passworddoesnotmatch, nil)];
                valid = NO;
            }else if([oldPassword isEqualToString:password]){
                [self showAlertWithTitle:nil message:JMOLocalizedString(Newpasswordcantbethesameastheoldpassword, nil)];
                 valid = NO;
            }
        }
    }
    
    return valid;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.passwordTextField == textField || self.confirmPasswordTextField == textField ||self.oldPasswordTextField == textField) {
        return [self passwordFormatIsValid:string];
    }else{
        return YES;
    }
}
-(BOOL)passwordFormatIsValid:(NSString*)password{
    NSString *passwordRegex = @"[A-Z0-9a-z_!]*"; ;
    NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordPredicate evaluateWithObject:password];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeButtonDidPress:(id)sender{
    if ([self inputIsValid]) {
        NSString *oldPassword = self.oldPasswordTextField.text;
        NSString *password = self.passwordTextField.text;
        NSString *userID = [LoginByEmailModel shared].realNetworkMemberInfo.UserID;
        NSString *accessToken = [LoginByEmailModel shared].realNetworkMemberInfo.AccessToken;
        [self showLoadingHUD];
        [[LoginByEmailModel shared]callRealNetworkChangePasswordApiNewPasswordMD5:password OldPasswordMD5:oldPassword userID:userID accessToken:accessToken Success:^(id response) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self closeButton:nil];
            }];
            [self showAlertWithTitle:nil message:JMOLocalizedString(Passwordhasbeechangedsuccessfully, nil) withAction:action];
            [self hideLoadingHUD];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            if (errorStatus==AccessErrorOtherUserLoginThisAccount) {
                 [self showAlertWithTitle:nil message:JMOLocalizedString(@"change_password__alert_message_old_password", nil)];
            }else{
            [self showAlertWithTitle:nil message:errorMessage];
            }
            [self hideLoadingHUD];
        }];
    }
}

-(IBAction)closeButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setupTextFieldCheckError{
    [[self.passwordTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];
    
    [[self.oldPasswordTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];
    [[self.confirmPasswordTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];

    
}

-(BOOL)checkInputIsValidNotAlertTitle{
    int passwordMinLength = 4;
    int passwordMaxLength = 20;
    BOOL valid = YES;
    NSString *oldPassword = self.oldPasswordTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *confirmPassword = self.confirmPasswordTextField.text;
    
    if (![RealUtility isValid:oldPassword]) {
         self.textFieldErrorLabel.text=JMOLocalizedString(Passwordisrequired, nil);
        valid= NO;
    }else if (![RealUtility isValid:password]){
          self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteranewpassword, nil);
        valid= NO;
    }else if([RealUtility isValid:password]){
        
        if (password.length < passwordMinLength || password.length > passwordMaxLength ) {
              self.textFieldErrorLabel.text=JMOLocalizedString(PasswordlengthmustbebetweenCharacters, nil);
            valid = NO;
        } else if (![RealUtility isValid:confirmPassword]){
            self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseconfirmyourpassword, nil);
            valid= NO;
        }else if([RealUtility isValid:confirmPassword]){
            if (![confirmPassword isEqualToString:password]) {
                 self.textFieldErrorLabel.text=JMOLocalizedString(Passworddoesnotmatch, nil);
                valid = NO;
            }else if([oldPassword isEqualToString:password]){
               self.textFieldErrorLabel.text=JMOLocalizedString(Newpasswordcantbethesameastheoldpassword, nil);
                 valid = NO;
            }
        }
    }
    
    return valid;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)setupKeyBoardDoneBtnAction{
    
     [self.confirmPasswordTextField setReturnKeyType:UIReturnKeyDone];
    [[self.confirmPasswordTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x){
        
        [self changeButtonDidPress:self];
    }];
}
@end
