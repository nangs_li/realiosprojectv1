//
//  RealChangePhoneIntroViewController.h
//  productionreal2
//
//  Created by Alex Hung on 15/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"

@interface RealChangePhoneIntroViewController : BaseViewController

@property (nonatomic, strong) IBOutlet UILabel *introTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *introContentLabel;
@property (nonatomic, strong) IBOutlet UIButton *nextButton;

- (IBAction)nextButtonDidPress:(id)sender;

+ (RealChangePhoneIntroViewController *)changePhoneIntroViewController;

@end
