//
//  RealChangePhoneIntroViewController.m
//  productionreal2
//
//  Created by Alex Hung on 15/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealChangePhoneIntroViewController.h"
#import "RealChangePhoneViewController.h"

@interface RealChangePhoneIntroViewController ()

@end

@implementation RealChangePhoneIntroViewController

+ (RealChangePhoneIntroViewController *)changePhoneIntroViewController {
    RealChangePhoneIntroViewController *introVC = [[RealChangePhoneIntroViewController alloc]initWithNibName:@"RealChangePhoneIntroViewController" bundle:nil];
    return introVC;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavBar];
    self.introTitleLabel.textColor = [UIColor whiteColor];
    self.introContentLabel.textColor = [UIColor whiteColor];
    [self.nextButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    
    [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
    self.introTitleLabel.text = JMOLocalizedString(@"Change Number", nil);
    self.introContentLabel.text = JMOLocalizedString(@"Use Change Number to mirgrate your account info, groups and settings from your current phone number to new phone number. You can't undo this change.\n\nTo proceed, confirm that your new number can receive SMS or calls and tap Next to Verify that number", nil);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupNavBar];
}


- (void) setupNavBar {
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"change_phone__title", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}


- (IBAction)nextButtonDidPress:(id)sender {
    RealChangePhoneViewController *phoneVC = [RealChangePhoneViewController changePhoneViewController];
    phoneVC.view.backgroundColor = self.view.backgroundColor;
    [self.navigationController pushViewController:phoneVC animated:YES];
}

@end
