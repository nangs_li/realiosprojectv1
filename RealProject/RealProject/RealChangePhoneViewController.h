//
//  RealChangePhoneViewController.h
//  productionreal2
//
//  Created by Alex Hung on 15/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"

@interface RealChangePhoneViewController : BaseViewController

@property (nonatomic, strong) IBOutlet UILabel *oldNumberTitle;
@property (nonatomic, strong) IBOutlet UILabel *changeNumberTitle;

@property (nonatomic, strong) IBOutlet UITextField *oldCountryCodeTextField;
@property (nonatomic, strong) IBOutlet UITextField *oldNumberTextField;

@property (nonatomic, strong) IBOutlet UITextField *changeCountryCodeTextField;
@property (nonatomic, strong) IBOutlet UITextField *changeNumberTextField;

@property (nonatomic, strong) IBOutlet UIButton *doneButton;


+ (RealChangePhoneViewController *)changePhoneViewController;

@end
