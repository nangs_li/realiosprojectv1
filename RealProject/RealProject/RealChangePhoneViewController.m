//
//  RealChangePhoneViewController.m
//  productionreal2
//
//  Created by Alex Hung on 15/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealChangePhoneViewController.h"
#import "UITextField+Utility.h"
#import "PhoneNumberViewController.h"
#import "PhoneNumberRegModel.h"
#import "RealApiClient.h"

// Framework
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"

@interface RealChangePhoneViewController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) NSArray *countryCode;

@end

@implementation RealChangePhoneViewController

+ (RealChangePhoneViewController *)changePhoneViewController {
    RealChangePhoneViewController *changePhoneVC = [[RealChangePhoneViewController alloc]initWithNibName:@"RealChangePhoneViewController" bundle:nil];
    return changePhoneVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.countryCode = [NSArray arrayWithArray:[[SystemSettingModel shared]getNotDuplicateSMSCountryCodeList]];
    
    self.oldNumberTitle.textColor = [UIColor whiteColor];
    self.changeNumberTitle.textColor = [UIColor whiteColor];
    
    self.changeNumberTextField.delegate = self;
    self.oldNumberTextField.delegate = self;
    
    [self setupCountryCodePicker:self.oldCountryCodeTextField];
    [self setupCountryCodePicker:self.changeCountryCodeTextField];
    
    [self setupKeyobardToolBar:self.oldCountryCodeTextField];
    [self setupKeyobardToolBar:self.changeCountryCodeTextField];
    [self setupKeyobardToolBar:self.oldNumberTextField];
    [self setupKeyobardToolBar:self.changeNumberTextField];
    
    [self.oldCountryCodeTextField setupCountryCodeTextField];
    [self.changeCountryCodeTextField setupCountryCodeTextField];
    [self.oldNumberTextField setUpPhoneNumberType];
    [self.changeNumberTextField setUpPhoneNumberType];
    
    self.oldNumberTitle.text = JMOLocalizedString(@"Your old country code and phone number", nil);
    self.changeNumberTitle.text = JMOLocalizedString(@"Your new country code and phone number", nil);
    [self.doneButton setTitle:JMOLocalizedString(@"Done", nil) forState:UIControlStateNormal];
    
    [self.doneButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    
    [self checkInput];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupNavBar];
}

- (void)prefillCountryCode:(UITextField *)textField pickerView:(UIPickerView *)pickerView{
    
    int selectedIndex= [[SystemSettingModel shared] getIndexFromNotDuplicateSMSCountryCodeListAccordingToCountry];
    
    if (selectedIndex == NSNotFound) {
        
        selectedIndex = 0;
    }
    
    if ([self.countryCode valid]) {
        
        [pickerView selectRow:selectedIndex inComponent:0 animated:NO];
        [self updateTextField:textField countryCode:self.countryCode[selectedIndex]];
        
    } else {
        [self updateTextField:textField countryCode:@"852"];
    }
    
}
- (void)updateTextField:(UITextField *)textField countryCode:(NSString *)countryCode {
    textField.text = [NSString stringWithFormat:@"+%@",countryCode];
}


- (void)setupKeyobardToolBar:(UITextField *)textField {
    UIToolbar *keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *textFieldDoneButton = [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(Done, nil)
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:textField
                                                                           action:@selector(resignFirstResponder)];
    
    [keyboardToolbar setItems:[NSArray arrayWithObjects:textFieldDoneButton, nil]];
    textField.inputAccessoryView = keyboardToolbar;
}

- (void)setupCountryCodePicker:(UITextField *)textField {
    UIPickerView *countryCodePickerView = [[UIPickerView alloc] init];
    countryCodePickerView.delegate = self;
    countryCodePickerView.dataSource = self;
    countryCodePickerView.tag = textField.hash;
    textField.inputView = countryCodePickerView;
    
    [self prefillCountryCode:textField pickerView:countryCodePickerView];
}

- (BOOL)checkInput {
    NSString *oldCountryCode = self.oldCountryCodeTextField.text;
    NSString *oldNumber = self.oldNumberTextField.text;
    NSString *changeCountryCode = self.changeCountryCodeTextField.text;
    NSString *changeNumber = self.changeNumberTextField.text;
    
    BOOL valid = oldCountryCode.length > 0 && oldNumber.length > 0 && changeNumber.length > 0 &&changeCountryCode.length > 0;
    
    self.doneButton.enabled = valid;
    return valid;
}

- (void) setupNavBar {
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"change_phone__title", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

- (void)goToVerfiyViewController:(PhoneNumberRegModel *)regModel{
    
    NSString *oldCountryCode = self.oldCountryCodeTextField.text;
    NSString *oldNumber = self.oldNumberTextField.text;
    NSString *changeCountryCode = self.changeCountryCodeTextField.text;
    NSString *changeNumber = self.changeNumberTextField.text;

    regModel.RegType = PhoneNumberControllerTypeSMSVerifyViewSettingPage;
    regModel.oldCountryCode = oldCountryCode;
    regModel.oldPhoneNumber = oldNumber;
    regModel.countryCode = changeCountryCode;
    regModel.phoneNumber = changeNumber;

    ResendButtonDisabledTimer *resentButtonDisabledTimer = [ResendButtonDisabledTimer shared];
    PhoneNumberViewController *phoneVC = [[PhoneNumberViewController alloc]initWithType:PhoneNumberControllerTypeSMSVerifyViewSettingPage phoneNumberRegModel:regModel resentButtonDisabledTimer:resentButtonDisabledTimer];
    [resentButtonDisabledTimer callResendButtonDisabledTimer];
    phoneVC.view.backgroundColor = self.view.backgroundColor;
    [self.navigationController pushViewController:phoneVC animated:YES];
}

- (IBAction)doneButtonDidPress:(id)sender {
    
    NSString *oldCountryCode = self.oldCountryCodeTextField.text;
    NSString *oldNumber = self.oldNumberTextField.text;
    NSString *changeCountryCode = self.changeCountryCodeTextField.text;
    NSString *changeNumber = self.changeNumberTextField.text;
    NSString *lang = [LanguagesManager sharedInstance].currentLanguage;
    NSString *changePhone = [NSString stringWithFormat:@"%@%@",changeCountryCode, changeNumber];
    __weak RealChangePhoneViewController *weakSelf = self;
    NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];
    
    NSError *error = nil;
    NBPhoneNumber *newNumber = [phoneUtil parse:changePhone defaultRegion:nil error:&error];
    
    if (!error) {
        if ([phoneUtil isValidNumber:newNumber]) {
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:JMOLocalizedString(@"alert_ok", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action) {
                                     
                                     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                                     [dict setObject:oldCountryCode forKey:@"CountryCodeOld"];
                                     [dict setObject:oldNumber forKey:@"PhoneNumberOld"];
                                     [dict setObject:changeNumber forKey:@"PhoneNumber"];
                                     [dict setObject:changeCountryCode forKey:@"CountryCode"];
                                     [dict setObject:lang forKey:@"Lang"];
                                     
                                     [weakSelf showLoading];
                                     [[RealApiClient sharedClient]changePhoneNumberWithDict:dict completion:^(PhoneNumberRegModel *response, NSError *error) {
                                         if (!error) {
                                             [weakSelf goToVerfiyViewController:response];
                                         } else {
                                             [weakSelf notError:error view:weakSelf.view];
                                         }
                                         [weakSelf hideLoading];
                                     }];
                                     
                                 }];
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:JMOLocalizedString(@"common__cancel", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         
                                         
                                     }];
            
            NSString *phoneNumberText = [NSString stringWithFormat:@"%@ %@" ,changeCountryCode ,changeNumber];
            NSString * title = [NSString stringWithFormat:@"%@\n\n%@\n\n%@" ,@"Number Confirmation" ,phoneNumberText ,@"Is your phone number above correct?"];
            [self showAlertWithTitle:title message:nil withActions:@[cancel,ok]];
        } else {
            NSString *errorTitle = @"Phone Number Error";
            NSString *errorMessage = @"Number %@ is not valid not %@";
            errorMessage = [NSString stringWithFormat:errorMessage,changeNumber,changeCountryCode];
            NSString *title = [NSString stringWithFormat:@"%@\n\n%@",errorTitle,errorMessage];
            [self showAlertWithTitle:title message:nil];
        }
    } else {
        [self showAlertWithTitle:error.localizedDescription message:nil];
    }
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self checkInput];
    return YES;
}

#pragma mark - <UIPickerViewDelegate>

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSString *countryCode = [self.countryCode objectAtIndex:row];
    
    UITextField *textField = nil;
    if (pickerView.tag == self.oldCountryCodeTextField.hash) {
        textField = self.oldCountryCodeTextField;
    } else if (pickerView.tag == self.changeCountryCodeTextField.hash) {
        textField = self.changeCountryCodeTextField;
    }
    
    [self updateTextField:textField countryCode:countryCode];
    
}

#pragma mark - <UIPickerViewDataSource>

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.countryCode.count;
    
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *countryCode = [self.countryCode objectAtIndex:row];
    return countryCode;
    
}



@end
