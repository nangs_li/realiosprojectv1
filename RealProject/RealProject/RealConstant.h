//
//  RealConstant.h
//  productionreal2
//
//  Created by Alex Hung on 28/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RealConstant : NSObject
FOUNDATION_EXPORT BOOL const kDebugMode;

FOUNDATION_EXPORT NSString *const kServerAddress;

/* Mixpanel */
FOUNDATION_EXPORT NSString *const kMixpanelToken;

/* Branck.io */
FOUNDATION_EXPORT NSString *const kBranchKey;

/* QuickBlox */
FOUNDATION_EXPORT NSInteger const QBApplicationID;
FOUNDATION_EXPORT NSString *const QBAuthKey;
FOUNDATION_EXPORT NSString *const QBAuthSecret;
FOUNDATION_EXPORT NSString *const QBAcctountKey;
FOUNDATION_EXPORT NSString *const QBAPIEndPoint;
FOUNDATION_EXPORT NSString *const QBChatEndPoint;
FOUNDATION_EXPORT int const QBSendMessageRetryCount;
FOUNDATION_EXPORT int const QBSendMessageRetryCountInterval;
FOUNDATION_EXPORT NSTimeInterval const kQBReconnectTimerInterval;
FOUNDATION_EXPORT NSTimeInterval const kQBRecipientTimerInterval;
FOUNDATION_EXPORT NSTimeInterval const kQBPresenceTimerInterval;
FOUNDATION_EXPORT NSTimeInterval const kQBCheckOnlineTimePeriod;
FOUNDATION_EXPORT int const qbLogLevel;
FOUNDATION_EXPORT int const QBRequestTimeOutInterval;
/* NewFeed*/
FOUNDATION_EXPORT NSTimeInterval const kNewFeedUpdateTimerInterval;

/* UpdateDBDiaolgPageAgentProfile*/
FOUNDATION_EXPORT NSTimeInterval const kUpdateDBDialogPageAgentProfileTimerInterval;
FOUNDATION_EXPORT NSTimeInterval const kUpdateDBDialogPageAgentProfileBetweenTime;
FOUNDATION_EXPORT int const kUpdateDBDialogPageAgentProfileNumberofRecord;

/* DDLog */
FOUNDATION_EXPORT int const ddLogLevel;

/* GA */
FOUNDATION_EXPORT int const gaLogLevel;

/* NewFeed*/
FOUNDATION_EXPORT NSTimeInterval const kNewFeedUpdateTimerInterval;

/* Create Listing*/
FOUNDATION_EXPORT NSInteger const kMaxReasonLanugageCount;
FOUNDATION_EXPORT NSInteger const kMaxAgentSpecialtyCount;
FOUNDATION_EXPORT NSInteger const kMaxAgentExperienceCount;
FOUNDATION_EXPORT NSInteger const kMaxBedroomCount;
FOUNDATION_EXPORT NSInteger const kMaxBathroomCount;
FOUNDATION_EXPORT CGFloat const kMaxGeneralTextFieldDataSizeInByte;
FOUNDATION_EXPORT CGFloat const kMaxReasonTextFieldDataSizeInByte;


FOUNDATION_EXPORT int const CurrentDBVersion;

/* Suppport Email*/
FOUNDATION_EXPORT NSString *const kSupportEmalAddress;


//auto Check Error type
FOUNDATION_EXPORT NSString *const kEmptyData ;
FOUNDATION_EXPORT int const kEmptyDataCode;
FOUNDATION_EXPORT NSString *const kFalseData ;
FOUNDATION_EXPORT int const kFalseDataCode;
FOUNDATION_EXPORT NSString *const kDuplicateData ;
FOUNDATION_EXPORT int const kDuplicateDataCode;

/* Google API key */
FOUNDATION_EXPORT NSString *const  kGoogleAPIKey;


/**
 *  Location Search Type
 */
FOUNDATION_EXPORT NSString *const kLocationSearchType;
FOUNDATION_EXPORT NSString *const kAddressSearchType;

/**
 *  kChatRoomPresentPhoto
 */
FOUNDATION_EXPORT NSString *const kChatRoomPresentPhoto;
FOUNDATION_EXPORT NSString *const kChatRoomPresentSelectPhotoLibrary;
@end
