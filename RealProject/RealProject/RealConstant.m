//
//  RealConstant.m
//  productionreal2
//
//  Created by Alex Hung on 28/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealConstant.h"

@implementation RealConstant
#ifdef  DEBUG

BOOL const kDebugMode = YES;

#ifdef PREDEPLOY
NSString *const kServerAddress = @"https://api.real.co/v1-dev";
#else
NSString *const kServerAddress = @"http://dev.real.co:9000/V1";
#endif

/* Mixpanel */
NSString *const kMixpanelToken = @"153f3a3a064a234dcd7c5587153b85ff";

/* Branck.io */
NSString *const kBranchKey = @"key_test_bgoUYNJXb43XMZT01dJYiliavqk5EXBd";

/* QuickBlox  */
NSInteger const QBApplicationID = 25583;
NSString *const QBAuthKey = @"ftZGNT7YOQeAywj";
NSString *const QBAuthSecret = @"wD2TwzPrh979WVy";
NSString *const QBAcctountKey = @"uKi6tB3ANZ1ivwG9cyzx";
NSString *const QBAPIEndPoint = @"";
NSString *const QBChatEndPoint = @"" ;
const int qbLogLevel = QBLogLevelInfo;

/* DDLog */
const int ddLogLevel = DDLogLevelAll;

/* GA */
const int gaLogLevel = kGAILogLevelVerbose;

#else

BOOL const kDebugMode = NO;

NSString *const kServerAddress = @"https://api.real.co/v1";

/* Mixpanel */
NSString *const kMixpanelToken = @"103f8fc687e6610385b933a3cec18019";

/* Branck.io */
NSString *const kBranchKey = @"key_live_knoLWISWfYY2N4UYZdK7bhkbECfWrXww";

/* QuickBlox  */
NSInteger const QBApplicationID = 3;
NSString *const QBAuthKey = @"5seV5xSrdH2JnOw";
NSString *const QBAuthSecret = @"8wQXPKu93GNz6JO";
NSString *const QBAcctountKey = @"arSwdkX6jXZGxPUNnSHX";
NSString *const QBAPIEndPoint = @"https://apiforreal.quickblox.com";
NSString *const QBChatEndPoint = @"chatforreal.quickblox.com" ;
const int qbLogLevel = QBLogLevelNothing;

/* DDLog */
const int ddLogLevel = DDLogLevelOff;

/* GA */
const int gaLogLevel = kGAILogLevelNone;

#endif

/* QuickBlox  */
NSTimeInterval const kQBReconnectTimerInterval = 10;
NSTimeInterval const kQBRecipientTimerInterval = 10;  //Get friend's last seen,  this makes friend's online status around 10 to 15 seconds appear after I enter the chatroom.  30 to 40 second to determine my friend's offline status.
NSTimeInterval const kQBPresenceTimerInterval = 30;   //Send out my presence
NSTimeInterval const kQBCheckOnlineTimePeriod = 30;   //The time period to determine the 'Online' status

/* NewFeed*/
NSTimeInterval const kNewFeedUpdateTimerInterval = 180;

/* UpdateDBDiaolgPageAgentProfile*/
NSTimeInterval const kUpdateDBDialogPageAgentProfileTimerInterval = 180;
NSTimeInterval const kUpdateDBDialogPageAgentProfileBetweenTime =24*60*60*1;
int const kUpdateDBDialogPageAgentProfileNumberofRecord =5;

/* Create Listing*/
NSInteger const kMaxReasonLanugageCount = 3;
NSInteger const kMaxAgentSpecialtyCount = 10;
NSInteger const kMaxAgentExperienceCount = 20;
NSInteger const kMaxBedroomCount = 99;
NSInteger const kMaxBathroomCount = 99;
CGFloat const kMaxGeneralTextFieldDataSizeInByte = 0.3 * 1024;
CGFloat const kMaxReasonTextFieldDataSizeInByte = 2 * 1024;

const int QBSendMessageRetryCount = 3;
const int QBSendMessageRetryCountInterval=1;
const int CurrentDBVersion = 1;
const int QBRequestTimeOutInterval = 8;
/* Suppport Email*/
NSString *const kSupportEmalAddress = @"team@real.co";

NSString *const kEmptyData = @"kEmptyData";
const int kEmptyDataCode = 1001;
NSString *const kFalseData = @"kFalseData";
const int kFalseDataCode = 1002;
NSString *const kDuplicateData = @"kDuplicateData";
const int kDuplicateDataCode = 1003;

/* Google API key */
NSString *const  kGoogleAPIKey = @"AIzaSyAUvXzefUL7vdyqiYpDOkJ2TJzbOm-2R_I";

/**
 *  location Search Type
 */
NSString *const kLocationSearchType = @"kLocationSearchType56e243527d713a72d0000d97";
NSString *const kAddressSearchType = @"kAddressSearchType56e243527d713a72d0000d97";



NSString *const kChatRoomPresentPhoto = @"kchatRoomPresentPhoto";
NSString *const kChatRoomPresentSelectPhotoLibrary = @"kchatRoomPresentSelectPhotoLibrary";
@end
