//
//  RealPhoneBookShareViewController.h
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"
@class BaseNavigationController;

typedef NS_ENUM(NSUInteger, RealPhoneBookShareType) {
    RealPhoneBookShareTypeConnectAgent = 0,
    RealPhoneBookShareTypeNewUser,
    RealPhoneBookShareTypeBoardcast
};

@interface RealPhoneBookShareViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic ,strong) IBOutlet UIImageView *loginBackgroundImageView;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIButton *inviteButton;
@property (nonatomic, strong) IBOutlet UIButton *otherInviteButton;
@property (nonatomic, strong) IBOutlet UIImageView *titleImageView;
@property (nonatomic, strong) IBOutlet UIView *searchContainerView;
@property (nonatomic, strong) IBOutlet UITextField *searchTextField;

@property (nonatomic, assign) RealPhoneBookShareType shareType;
@property (nonatomic, strong) NSArray *contacts;
@property (nonatomic, strong) NSArray *joinedAgents;

- (IBAction)closeButtonDidPress:(id)sender;
- (IBAction)inviteButtonDidPress:(id)sender;
- (IBAction)otherInviteButtonDidPress:(id)sender;

+ (RealPhoneBookShareViewController *)phoneBookShareVCWithShareType:(RealPhoneBookShareType)shareType;
+ (RealPhoneBookShareViewController *)phoneBookShareVCWithShareType:(RealPhoneBookShareType)shareType contact:(NSArray *)contact joinedAgent:(NSArray *)joinedAgent;


@end
