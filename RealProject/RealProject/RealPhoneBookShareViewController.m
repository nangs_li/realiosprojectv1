//
//  RealPhoneBookShareViewController.m
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealPhoneBookShareViewController.h"
#import "REPhonebookManager.h"
#import "REContact.h"
#import "RealPhoneBookTableViewCell.h"
#import "UIViewController+Hint.h"
#import <MessageUI/MessageUI.h>
#import "BaseNavigationController.h"
#import "RealApiClient.h"
#import "UILabel+Utility.h"
#import "NSDictionary+Utility.h"
#import "SLSMarkupParser.h"
#import "ActivityLogFollowingCell.h"
#import "RealPhonebookShareSectionHeaderView.h"
#import "RealFadingTableViewCell.h"
#import "FollowAgentModel.h"
#import "REJoinedAgentContact.h"
#import "DeepLinkActionHanderModel.h"

static CGFloat tableSectionHeaderHeight = 56.0;

@interface RealPhoneBookShareViewController () <MFMessageComposeViewControllerDelegate, ActivityLogFollowingCellDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *selectedContacts;
@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;
@property (nonatomic, strong) NSDictionary *sectionDict;
@property (nonatomic, strong) NSMutableDictionary *invitedContactDict;

@property (nonatomic, strong) RealPhonebookShareSectionHeaderView *agentHeaderView;
@property (nonatomic, strong) RealPhonebookShareSectionHeaderView *contactHeaderView;

@property (nonatomic, strong) NSArray *originalJoinedAgents;
@property (nonatomic, strong) NSArray *originalContacts;

@property (nonatomic, strong) NSString *filterString;
@property (nonatomic, strong) NSMutableArray *pendToFollowAgentIDs;

@end

@implementation RealPhoneBookShareViewController

+ (RealPhoneBookShareViewController *)phoneBookShareVCWithShareType:(RealPhoneBookShareType)shareType {
    return [self phoneBookShareVCWithShareType:shareType contact:nil joinedAgent:nil];
}

+ (RealPhoneBookShareViewController *)phoneBookShareVCWithShareType:(RealPhoneBookShareType)shareType contact:(NSArray *)contact joinedAgent:(NSArray *)joinedAgent{
    NSString *nibName = @"RealPhoneBookConnectAgentViewController";
    if(shareType == RealPhoneBookShareTypeBoardcast){
        nibName = @"RealPhoneBookBoardcastViewController";
    }else if (shareType == RealPhoneBookShareTypeConnectAgent || shareType == RealPhoneBookShareTypeNewUser){
        nibName = @"RealPhoneBookConnectAgentViewController";
    }
    
    RealPhoneBookShareViewController *phoneBookShareVC = [[RealPhoneBookShareViewController alloc]initWithNibName:nibName bundle:nil];
    phoneBookShareVC.shareType = shareType;
    phoneBookShareVC.contacts = [contact copy];
    phoneBookShareVC.originalContacts = [contact copy];
    phoneBookShareVC.originalJoinedAgents = [joinedAgent copy];
    phoneBookShareVC.joinedAgents = [joinedAgent copy];
    return phoneBookShareVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.shareType == RealPhoneBookShareTypeNewUser) {
        self.loginBackgroundImageView.hidden = NO;
    }else {
        self.loginBackgroundImageView.hidden = YES;
    }
    [self setupTextField];
    [self setupTableView];
    [self setupTitleLabel];
    
    if (self.shareType == RealPhoneBookShareTypeBoardcast) {
        self.invitedContactDict = [REContact boardcastDict];
    } else {
        self.invitedContactDict = [REContact invitedContactDict];
    }
    self.pendToFollowAgentIDs = [[NSMutableArray alloc]init];
    self.selectedContacts = [[NSMutableArray alloc]init];
    self.selectedIndexPaths = [[NSMutableArray alloc]init];
    [self updateRecipientCount:0];
    [self showLoadingHUD];
    __weak typeof(self) weakSelf = self;
    if (self.contacts.count <= 0) {
        [[REPhonebookManager sharedManager] requestAccess:^(BOOL granted) {
            
            if (granted) {
                
                if ([REPhonebookManager sharedManager].contacts.count > 0) {
                    weakSelf.contacts = [NSArray arrayWithArray:[REPhonebookManager sharedManager].contacts];
                } else {
                    weakSelf.contacts = [[NSArray alloc]init];
                }
                weakSelf.originalContacts = [self.contacts copy];
                
                if (weakSelf.shareType == RealPhoneBookShareTypeConnectAgent || weakSelf.shareType == RealPhoneBookShareTypeNewUser) {
                    [weakSelf findJoinedAgent];
                } else {
                    [weakSelf reloadTableViewWithFilter:nil];
                }
                
            }else{
                [self hideLoadingHUD];
                __weak typeof(self) weakSelf = self;
                UIAlertController *alert = [REPhonebookManager noPermissionAlertWithCancelBlock:^(UIAlertAction *action) {
                    
                    [weakSelf closeButtonDidPress:nil];
                }];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }else{
        [self reloadTableViewWithFilter:nil];
    }
}

- (void)setupTextField {
    NSAttributedString *search = [[NSAttributedString alloc]  initWithString:JMOLocalizedString(@"connectagent__search", nil) attributes:@{NSForegroundColorAttributeName : [UIColor lightGrayColor]}];
    
    self.searchTextField.attributedPlaceholder = search;
    UIView *searchIconContainerView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 26.0f, 18.0f)];
    searchIconContainerView.backgroundColor = [UIColor clearColor];
    UIImageView *searchIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8.0f, 0.0f, 18.0f, 18.0f)];
    searchIconImageView.contentMode = UIViewContentModeScaleAspectFit;
    searchIconImageView.image = [UIImage imageNamed:@"icon-search"];
    [searchIconContainerView addSubview:searchIconImageView];
    
    if (!self.searchTextField.leftView) {
        [self.searchTextField setLeftView:searchIconContainerView];
        [self.searchTextField setLeftViewMode:UITextFieldViewModeAlways];
    }
    
    if (!self.searchTextField.rightView) {
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor realBlueColor] forState:UIControlStateHighlighted];
        [cancelButton setTitle:JMOLocalizedString(@"common__cancel", nil) forState:UIControlStateNormal];
        cancelButton.titleLabel.font = self.searchTextField.font;
        cancelButton.contentEdgeInsets = UIEdgeInsetsMake(4, 8, 4, 8);
        [cancelButton addTarget:self action:@selector(resetSearch) forControlEvents:UIControlEventTouchUpInside];
        [cancelButton sizeToFit];
        
        [self.searchTextField setRightView:cancelButton];
        [self.searchTextField setRightViewMode:UITextFieldViewModeWhileEditing];
    }
}

- (void)setupTitleLabel {
    
    NSString *markUpStringNotColor;
    
    if (self.shareType == RealPhoneBookShareTypeConnectAgent || self.shareType == RealPhoneBookShareTypeNewUser) {
        markUpStringNotColor = JMOLocalizedString(@"connectagent__title", nil);
    }else if (self.shareType == RealPhoneBookShareTypeBoardcast) {
        markUpStringNotColor = [NSString stringWithFormat:@"<yellow>%@</yellow>",JMOLocalizedString(@"broadcast__title", nil)];
    }
    
    NSString *markUpString = [NSString stringWithFormat:@"<title>%@</title>", markUpStringNotColor];
    NSAttributedString *attributedString = [self attributedTextWithMarkUp:markUpString];
    [self.titleLabel setAttributedText:attributedString];
}

-(NSAttributedString *)attributedTextWithMarkUp:(NSString*)markup{
    
    NSMutableParagraphStyle *styleDefault = [[NSMutableParagraphStyle alloc] init];
    styleDefault.alignment = NSTextAlignmentCenter;
    
    NSDictionary *style = @{
                            @"$default":@{
                                    NSFontAttributeName : [UIFont systemFontOfSize:12],
                                    NSParagraphStyleAttributeName:styleDefault,
                                    NSForegroundColorAttributeName :[UIColor white]
                                    },
                            @"title":@{
                                    NSFontAttributeName:[UIFont systemFontOfSize:20]
                                    },
                            @"subtitle":@{
                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:14]
                                    },
                            @"yellow":@{
                                    NSForegroundColorAttributeName : [UIColor alexYellow]
                                    },
                            @"white":@{
                                    NSForegroundColorAttributeName : [UIColor white]
                                    },
                            };
    
    NSError *error = nil;
    
    NSAttributedString *attributedString = [SLSMarkupParser attributedStringWithMarkup:markup style:style error:&error];
    
    return attributedString;
}

- (void)findJoinedAgent {
    [[RealApiClient sharedClient] uploadContactListWithBlock:^(id response, NSError *error) {
        
        if (error) {
            [self reloadTableViewWithFilter:nil];
        }else{
            [[RealApiClient sharedClient] getAgentProfileFromContactListWithBlock:^(id response, NSError *error) {
                
                if (error) {
                    
                } else {
                    self.joinedAgents = [response copy];
                    self.originalJoinedAgents = [self.joinedAgents copy];
                    [self cleanArray];
                }
                [self reloadTableViewWithFilter:nil];
            }];
        }
    }];
}

- (void) resetSearch {
    self.searchTextField.text = nil;
    [self.searchTextField resignFirstResponder];
    [self reloadTableViewWithFilter:nil];
}

- (void)cleanArray {
    NSMutableArray *cleanContact = [self.contacts mutableCopy];
    for (REJoinedAgentContact *agentContact in self.originalJoinedAgents) {
        for (REContact *contact in [cleanContact copy]) {
            if ([agentContact.CountryCode isEqualToString:contact.countryCode] && [agentContact.PhoneNumber isEqualToString:contact.phoneNumber]) {
                [cleanContact removeObject:contact];
            }
        }
    }
    self.originalContacts = [NSArray arrayWithArray:cleanContact];
}

- (void)reloadTableViewWithFilter:(NSString *)filter{
    
    self.filterString = filter;
    
    NSArray *filteredAgentArray = [self.originalJoinedAgents copy];
    NSArray *filteredContactArray = [self.originalContacts copy];
    
    if (self.filterString.length > 0) {
        NSPredicate *agentPredicate = [NSPredicate predicateWithFormat:@"SELF.Name contains[cd] %@",self.filterString];
        filteredAgentArray = [self.originalJoinedAgents filteredArrayUsingPredicate:agentPredicate];
        
        NSPredicate *contactPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@",self.filterString];
        filteredContactArray = [self.originalContacts filteredArrayUsingPredicate:contactPredicate];
    }
    
    self.joinedAgents = [filteredAgentArray copy];
    self.contacts = [filteredContactArray copy];
    
    BOOL hasJoinedAgents = self.joinedAgents.count > 0;
    BOOL hasContacts = self.contacts.count > 0;
    
    NSMutableDictionary *sectionDict = [[NSMutableDictionary alloc]init];
    
    if (hasJoinedAgents) {
        [sectionDict setObject:self.joinedAgents forKey:@(sectionDict.allKeys.count)];
    }
    
    if(hasContacts){
        [sectionDict setObject:self.contacts forKey:@(sectionDict.allKeys.count)];
    }
    
    self.sectionDict = [NSDictionary dictionaryWithDictionary:sectionDict];
    
    [self.tableView reloadData];
    
    [self scrollViewDidScroll:self.tableView];
    [self hideLoadingHUD];
}

- (void)updateRecipientCount:(NSInteger)count {
    
    NSString *inviteString ;
    
    if (count > 0) {
        if (self.shareType == RealPhoneBookShareTypeBoardcast) {
            inviteString = [NSString stringWithFormat:JMOLocalizedString(@"broadcast__send_invitations", nil),count];
        }else {
            inviteString = [NSString stringWithFormat:JMOLocalizedString(@"connectagent__invite", nil),count];
        }
        
    }else {
        inviteString = JMOLocalizedString(@"broadcast__Invite", nil);
    }
    if (self.shareType == RealPhoneBookShareTypeConnectAgent || self.shareType == RealPhoneBookShareTypeNewUser) {
        self.contactHeaderView.sectionActionButton.enabled = count > 0;
        self.contactHeaderView.sectionActionButton.alpha = count > 0 ? 1 : 0.8;
        [self.contactHeaderView.sectionActionButton setTitle:inviteString forState:UIControlStateNormal];
    }else if (self.shareType == RealPhoneBookShareTypeBoardcast) {
        [self.inviteButton setTitle:inviteString forState:UIControlStateNormal];
    }
}

- (void)saveSuccessRecipients {
    for (REContact *contact in self.selectedContacts) {
        NSString *key = [NSString stringWithFormat:@"%@%@",contact.countryCode, contact.phoneNumber];
        NSInteger previousInvitedTime = [self invitedTime:contact];
        [self.invitedContactDict setObject:@(previousInvitedTime+1) forKey:key];
    }
    [self sendSuccessRecipientToServer];
    [REContact saveInvitedContactDict:self.invitedContactDict];
    self.selectedContacts = [[NSMutableArray alloc]init];
    [self updateRecipientCount:0];
    [self resetSelectedContact];
    [self reloadTableViewWithFilter:self.filterString];
    
}

- (void)sendSuccessRecipientToServer {
    NSMutableArray *contactsToTxtFile = [[NSMutableArray alloc]init];
    for (REContact *contact in self.selectedContacts) {
            NSDictionary *tempDict = @{
                                       @"cc": contact.countryCode,
                                       @"pn": contact.phoneNumber,
                                       @"nm": contact.name,
                                       @"em": contact.email
                                       };
        [contactsToTxtFile addObject:tempDict];
    }
    NSDictionary *dictForData = @{@"contactList":contactsToTxtFile};
    NSString *stringToWrite = [dictForData jsonPresentation];
    NSString *bom = @"\357\273\277";
    stringToWrite = [NSString stringWithFormat:@"%@%@",bom,stringToWrite];
    NSData *contactData = [stringToWrite dataUsingEncoding:NSUTF8StringEncoding];
    [[RealApiClient sharedClient]uploadInviteList:contactData withBlock:^(id response, NSError *error) {
        
    }];
}

- (void)resetSelectedContact {
    for (NSIndexPath *indexPath in self.selectedIndexPaths) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    self.selectedIndexPaths = [[NSMutableArray alloc]init];
}

- (NSInteger)invitedTime:(REContact *)contact {
    NSInteger invitedTime = 0;
    NSString *key = [NSString stringWithFormat:@"%@%@",contact.countryCode, contact.phoneNumber];
    if (self.invitedContactDict[key]) {
        invitedTime = [self.invitedContactDict[key] integerValue];
    }
    
    return invitedTime;
}

#pragma mark - Setup

- (void)setupTableView {
    [self.inviteButton setBackgroundImage:[UIImage imageWithColor:[UIColor alexYellow]] forState:UIControlStateNormal];
    [self.inviteButton setTitleColor:RealDarkBlueColor forState:UIControlStateNormal];
    [self.otherInviteButton setBackgroundImage:[UIImage imageWithBorder:[UIColor alexYellow] size:self.otherInviteButton.frame.size fillcolor:[UIColor clearColor]] forState:UIControlStateNormal];
    [self.otherInviteButton setTitleColor:[UIColor alexYellow] forState:UIControlStateNormal];
    //TODO: Replace Localized String
    [self.otherInviteButton setTitle:JMOLocalizedString(@"broadcast__other", nil) forState:UIControlStateNormal];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pop_dottedline"]]];
    [self.tableView registerNib:[UINib nibWithNibName:@"ActivityLogFollowingCell" bundle:nil] forCellReuseIdentifier:@"ActivityLogFollowingCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RealPhoneBookTableViewCell" bundle:nil] forCellReuseIdentifier:@"RealPhoneBookTableViewCell"];
}

#pragma mark - IBAction

- (IBAction)closeButtonDidPress:(id)sender {
    if(self.shareType == RealPhoneBookShareTypeNewUser || self.shareType == RealPhoneBookShareTypeConnectAgent){
        [self followPendingMemberId];
    }
    if(self.shareType == RealPhoneBookShareTypeNewUser) {
        [self showLoadingHUD];
        [self moveToMainPage];
    } else if (self.shareType == RealPhoneBookShareTypeConnectAgent){
        __block UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.image = [UIImage imageNamed:@"ico_pop_viral_follow"];
        iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [[kAppDelegate window] addSubview:iconImageView];
        NSLayoutConstraint *topSpacing = [iconImageView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.titleImageView];
        NSLayoutConstraint *alignCenter = [iconImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [[kAppDelegate window]layoutIfNeeded];
        UIImage *targetImage = [UIImage imageNamed:@"btn_nav_bar_follow"];
        CGSize finalImageSize = targetImage.size;
        CGSize rightButtonSize = CGSizeMake(45, 44);
        CGFloat statusBarHeight = 20;
        CGFloat finalTopInset = statusBarHeight + (rightButtonSize.height - finalImageSize.height)/2;
        CGFloat finalRightInset = (rightButtonSize.width - finalImageSize.width)/2;
        [self.delegate dismissOverlayerViewController:self animated:YES
                                      extraAnimations:^{
                                          topSpacing.active = NO;
                                          alignCenter.active = NO;
                                          [iconImageView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:finalTopInset];
                                          [iconImageView autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:finalRightInset];
                                          [iconImageView autoSetDimensionsToSize:finalImageSize];
                                          [[kAppDelegate window]layoutIfNeeded];
                                      } extraCompletion:^(BOOL finished) {
                                          [iconImageView removeFromSuperview];
                                          iconImageView = nil;
                                      } dismissCompletion:^{
                                          
                                      }];
    } else {
        [self.delegate dismissOverlayerViewController:self];
    }
}

- (IBAction)otherInviteButtonDidPress:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    [self getInviteUrlWithBlock:^(NSString *url, NSError *error) {
        [weakSelf hideLoadingHUD];
        if(!error){
            //TODO: Replace Localized String
            NSString *subject = JMOLocalizedString(@"invite_to_follow__subject", nil);
            NSString *textToShare = JMOLocalizedString(@"invite_to_follow__content", nil);
            NSArray *objectsToShare;
            NSURL *shareUrl = [NSURL URLWithString:url];
            objectsToShare = @[textToShare, shareUrl];
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            [activityVC setValue:subject forKey:@"subject"];
            [weakSelf presentViewController:activityVC animated:YES completion:nil];
        }
    }];
    
}

- (void)getInviteUrlWithBlock:(callbackWithUrl)block{
    NSString *memberId = [NSString stringWithFormat:@"%d",[MyAgentProfileModel shared].myAgentProfile.MemberID];
    NSString *listingId =[NSString stringWithFormat:@"%d",[MyAgentProfileModel shared].myAgentProfile.AgentListing.AgentListingID];
    NSString *memberName = [MyAgentProfileModel shared].myAgentProfile.MemberName;
    NSString *imageURL =[MyAgentProfileModel shared].myAgentProfile.AgentPhotoURL;
    
    if (![self.delegate networkConnection]) {
        
        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
    }
    
    [self showLoadingHUD];
    [[DeepLinkActionHanderModel shared]inviteToFollowWithMemberId:memberId memberName:memberName imageURL:(NSString*)imageURL listingId:listingId block:block];
}

- (IBAction)inviteButtonDidPress:(id)sender {
    
    if([self selectedRecipients].count > 0){
        __weak typeof(self) weakSelf = self;
        [self getInviteUrlWithBlock:^(NSString *url, NSError *error) {
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            NSString *message = @"";
            
            //TODO: Replace Localized String
            if (self.shareType == RealPhoneBookShareTypeNewUser || self.shareType == RealPhoneBookShareTypeConnectAgent){
                message = JMOLocalizedString(@"invite_to_follow__content", nil);
            }else {
                message = JMOLocalizedString(@"invite_to_follow__content", nil);
            }
            
            [weakSelf hideLoadingHUD];
            
            if([MFMessageComposeViewController canSendText]) {
                weakSelf.navigationController.navigationBar.hidden = NO;
                controller.body = [message stringByAppendingFormat:@"\n%@",url];
                controller.recipients = [weakSelf selectedRecipients];
                controller.messageComposeDelegate = weakSelf;
                [weakSelf presentViewController:controller animated:YES completion:nil];
            }
        }];
    }
}

- (IBAction)followAllButtonDidPress:(id)sender {
    for (REJoinedAgentContact *agentContact in self.joinedAgents) {
        NSString *memberID = [agentContact.MemberID stringValue];
        if(![self.pendToFollowAgentIDs containsObject:memberID]){
            [self.pendToFollowAgentIDs addObject:memberID];
        }
    }
    [self reloadTableViewWithFilter:self.filterString];
    self.agentHeaderView.sectionActionButton.enabled = NO;
    self.agentHeaderView.sectionActionButton.alpha = 0.8;
}


- (NSArray *)selectedRecipients {
    NSMutableArray *recipients = [[NSMutableArray alloc]init];
    
    for (REContact *contact in self.selectedContacts) {
        NSString *phone = [contact phone];
        [recipients addObject:phone];
    }
    
    return recipients;
}

- (void)followPendingMemberId {
    if (self.pendToFollowAgentIDs.count > 0) {
        [[FollowAgentModel shared]callAllFollowAgentApiByMemberIDArray:[self.pendToFollowAgentIDs mutableCopy] success:nil failure:nil];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)replacementString {
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:replacementString];
    [self reloadTableViewWithFilter:proposedNewString];
    return YES;
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    if (result == MessageComposeResultSent) {
        [self saveSuccessRecipients];
    }
    
    [self dismissViewControllerAnimated:controller completion:nil];
    
}

#pragma mark - ActivityLogFollowingCellDelegate

- (void)followingCell:(ActivityLogFollowingCell *)cell followerButtonDidPress:(REJoinedAgentContact *)model {
    NSString *memberID = [model.MemberID stringValue];
    if (cell.isFollowing) {
        if([self.pendToFollowAgentIDs containsObject:memberID]){
            [self.pendToFollowAgentIDs removeObject:memberID];
        }
        [cell didFollow:NO];
        self.agentHeaderView.sectionActionButton.enabled = YES;
        self.agentHeaderView.sectionActionButton.alpha = 1;
    }else {
        if(![self.pendToFollowAgentIDs containsObject:memberID]){
            [self.pendToFollowAgentIDs addObject:memberID];
        }
        [cell didFollow:YES];
    }
    
    
}

#pragma mark - UIScollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == self.tableView && (self.shareType == RealPhoneBookShareTypeConnectAgent || self.shareType == RealPhoneBookShareTypeNewUser)) {
        
        for (RealFadingTableViewCell *cell in self.tableView.visibleCells) {
            CGFloat hiddenFrameHeight = scrollView.contentOffset.y + tableSectionHeaderHeight - cell.frame.origin.y;
            if (hiddenFrameHeight >= 0 || hiddenFrameHeight <= cell.frame.size.height) {
                [cell maskCellFromTop:hiddenFrameHeight];
            }
        }
    }
}


#pragma mark - UITableViewDataSoure & Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *sectionItems = self.sectionDict[@(indexPath.section)];
    CGFloat height = 0;
    
    if (sectionItems == self.joinedAgents) {
        height = 63;
    }else if (sectionItems == self.contacts) {
        height = 58;
    }
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat headerHeight = 0;
    
    if (self.shareType == RealPhoneBookShareTypeConnectAgent || self.shareType == RealPhoneBookShareTypeNewUser) {
        headerHeight = tableSectionHeaderHeight;
    }
    
    return headerHeight;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    CGFloat headerHeight = 12;
    
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sectionDict.allKeys.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [UIView new];
    if (self.shareType == RealPhoneBookShareTypeConnectAgent || self.shareType == RealPhoneBookShareTypeNewUser) {
        NSArray *sectionItems = self.sectionDict[@(section)];
        
        RealPhonebookShareSectionHeaderView *shareHeaderView;
        NSString *title;
        NSString *buttonTitle;
        UIColor *themeColor;
        if (sectionItems == self.joinedAgents) {
            if (!self.agentHeaderView) {
                self.agentHeaderView = [[NSBundle mainBundle] loadNibNamed:@"RealPhonebookShareSectionHeaderView" owner:self options:nil].firstObject;
                [self.agentHeaderView.sectionActionButton addTarget:self action:@selector(followAllButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
            }
            themeColor = [UIColor realBlueColor];
            shareHeaderView = self.agentHeaderView;
            //TODO: Replace Localized String
            title = JMOLocalizedString(@"connectagent__agents_you_may_know", nil);
            buttonTitle = JMOLocalizedString(@"connectagent__follow_all", nil);
        }else if (sectionItems == self.contacts){
            if (!self.contactHeaderView) {
                self.contactHeaderView = [[NSBundle mainBundle] loadNibNamed:@"RealPhonebookShareSectionHeaderView" owner:self options:nil].firstObject;
                [self.contactHeaderView.sectionActionButton addTarget:self action:@selector(inviteButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
            }
            themeColor = [UIColor alexYellow];
            shareHeaderView = self.contactHeaderView;
            title = JMOLocalizedString(@"connectagent__favourite_agents", nil);
        }
        
        [shareHeaderView configureWithTitle:title buttonTitle:buttonTitle color:themeColor];
        [self updateRecipientCount:self.selectedContacts.count];
        headerView = shareHeaderView;
    }
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionItems = self.sectionDict[@(section)];
    return sectionItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *sectionItems = self.sectionDict[@(indexPath.section)];
    UITableViewCell *cell;
    
    if (sectionItems == self.joinedAgents) {
        REJoinedAgentContact *agentContact = self.joinedAgents[indexPath.row];
        ActivityLogFollowingCell *followingCell = [tableView dequeueReusableCellWithIdentifier:@"ActivityLogFollowingCell"];
        followingCell.delegate = self;
        [followingCell configureWithAgentContact:agentContact];
        followingCell.dotImageView.hidden = YES;
        [followingCell didFollow:[self.pendToFollowAgentIDs containsObject:[agentContact.MemberID stringValue]]];
        cell = followingCell;
        
    }else if (sectionItems == self.contacts) {
        
        RealPhoneBookTableViewCell *phoneBookCell = [tableView dequeueReusableCellWithIdentifier:@"RealPhoneBookTableViewCell"];
        REContact *contact = self.contacts[indexPath.row];
        [phoneBookCell configureWithContact:contact invitedTime:[self invitedTime:contact]];
        cell = phoneBookCell;
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSArray *sectionItems = self.sectionDict[@(indexPath.section)];
    if (sectionItems == self.contacts) {
        
        RealPhoneBookTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (self.selectedContacts.count >= 10) {
            [self showHint:JMOLocalizedString(@"broadcast__recommend_sending", nil) offset:20];
            cell.selected = NO;
        }else{
            
            [self hideHint];
            
            REContact *selectedContact = self.contacts[indexPath.row];
            
            if(![self.selectedContacts containsObject:selectedContact]) {
                [self.selectedContacts addObject:selectedContact];
            }
            
            if (![self.selectedIndexPaths containsObject:indexPath]) {
                [self.selectedIndexPaths addObject:indexPath];
            }
            
            [self updateRecipientCount:self.selectedContacts.count];
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *sectionItems = self.sectionDict[@(indexPath.section)];
    
    if (sectionItems == self.contacts) {
        
        [self hideHint];
        
        REContact *selectedContact = self.contacts[indexPath.row];
        
        if ([self.selectedContacts containsObject:selectedContact]) {
            [self.selectedContacts removeObject:selectedContact];
        }
        
        if ([self.selectedIndexPaths containsObject:indexPath]) {
            [self.selectedIndexPaths removeObject:indexPath];
        }
        
        [self updateRecipientCount:self.selectedContacts.count];
    }
    
}

@end
