//
//  RealPhonebookShareSectionHeaderView.h
//  productionreal2
//
//  Created by Alex Hung on 6/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealPhonebookShareSectionHeaderView : UIView

@property (nonatomic,strong) IBOutlet UILabel *sectionTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *sectionActionButton;
@property (nonatomic,strong) IBOutlet UIView *sectionDividerView;

- (void)configureWithTitle:(NSString *)title buttonTitle:(NSString *)buttonTitle color:(UIColor *)color;

@end
