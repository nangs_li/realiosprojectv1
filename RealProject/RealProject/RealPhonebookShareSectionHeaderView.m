//
//  RealPhonebookShareSectionHeaderView.m
//  productionreal2
//
//  Created by Alex Hung on 6/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealPhonebookShareSectionHeaderView.h"

@implementation RealPhonebookShareSectionHeaderView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (void)configureWithTitle:(NSString *)title buttonTitle:(NSString *)buttonTitle color:(UIColor *)color {
    self.sectionTitleLabel.text = title;
    [self.sectionActionButton setTitle:buttonTitle forState:UIControlStateNormal];
    
    self.sectionDividerView.backgroundColor = color;
    self.sectionTitleLabel.textColor = color;
    [self.sectionActionButton setBackgroundImage:[UIImage imageWithBorder:color size:self.sectionActionButton.frame.size fillcolor:[UIColor clearColor]] forState:UIControlStateNormal];
    [self.sectionActionButton setTitleColor:color forState:UIControlStateNormal];
}

@end
