//
//  RealSignUpDetalViewController.h
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealSignUpDetalViewController : BaseViewController
@property (nonatomic,strong) NSString *email;

@property (nonatomic,strong) IBOutlet NSLayoutConstraint *profileImageViewWidthConstraint;
@property (nonatomic,strong) IBOutlet UIButton *addPhotoButton;
@property (nonatomic,strong) IBOutlet UITextField *firstNameTextField;
@property (nonatomic,strong) IBOutlet UITextField *lastNameTextField;
@property (nonatomic,strong) IBOutlet UITextView *termTextView;
@property (nonatomic,strong) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *textFieldErrorLabel;
@property (strong, nonatomic) IBOutlet UILabel *signUpLable;
@property (strong, nonatomic) IBOutlet UIButton *createBtn;
@property (assign, nonatomic) BOOL pressCreateButton;
@property (assign, nonatomic) BOOL hasCompressImage;
@property (strong, nonatomic) IBOutlet UIButton *facebookLoginButton;
@property (strong, nonatomic) IBOutlet UILabel *loginWithFacebookLabel;

//phoneNumber
@property (nonatomic,strong) PhoneNumberRegModel * phoneNumberRegModel;
@property (nonatomic,strong) NSString *countryCode;
@property (nonatomic,strong) NSString *phoneNumber;
@property (nonatomic,strong) NSString *pinInput;

@end
