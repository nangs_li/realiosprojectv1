//
//  RealSignUpDetalViewController.m
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealSignUpDetalViewController.h"
#import "UIImage+Utility.h"
#import "Termsofservice.h"
#import "Privatepolicy.h"
#import <DZNPhotoPickerController/UIImagePickerController+Edit.h>
#import "LoginByEmailModel.h"
#import <PureLayout/PureLayout.h>
#import "RSKImageCropViewController.h"
#define textFieldPlaceHolderColor [UIColor colorWithRed:193/255.0 green:193/255.0 blue:193/255.0 alpha:1.0]

// Mixpanel
#import "Mixpanel.h"
#import "FacebookLoginModel.h"

@interface RealSignUpDetalViewController () <UITextFieldDelegate>
@property (nonatomic,strong) UIImage *profileImage;
@property (nonatomic,strong) NSMutableAttributedString *termText;
@end

@implementation RealSignUpDetalViewController

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage
                                              object:nil];
    
    [super viewDidLoad];
    [self.createBtn setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self.createBtn setBackgroundImage:[UIImage imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateDisabled];
    
    DDLogDebug(@"screen:%f",[RealUtility screenBounds].size.height);
    if ([RealUtility screenBounds].size.height <= 568) {
        self.profileImageViewWidthConstraint.constant = 70;
        UIFont *currentFont =self.addPhotoButton.titleLabel.font;
        self.addPhotoButton.titleLabel.font = [currentFont fontWithSize:14];
    }
    self.firstNameTextField.background =[UIImage imageWithBorder:textFieldPlaceHolderColor size:self.firstNameTextField.frame.size];
    self.firstNameTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.firstNameTextField.leftViewMode = UITextFieldViewModeAlways;
    self.firstNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"sign_up__create_a_first_name", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    //    self.firstNameTextField.delegate = self;
    
    self.lastNameTextField.background =[UIImage imageWithBorder:textFieldPlaceHolderColor size:self.lastNameTextField.frame.size];
    self.lastNameTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.lastNameTextField.leftViewMode = UITextFieldViewModeAlways;
    self.lastNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"sign_up__last_name", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    //    self.lastNameTextField.delegate = self;
    
    //    self.confirmPasswordTextField.delegate = self;
    
    self.profileImageView.layer.cornerRadius = self.profileImageViewWidthConstraint.constant/2;
    
    NSDictionary *termNormalAttributeDict =  @{ NSForegroundColorAttributeName : textFieldPlaceHolderColor };
    
    NSMutableAttributedString *termString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",JMOLocalizedString(@"sign_up__tnc_1", nil)] attributes:termNormalAttributeDict];
    NSMutableAttributedString *policyLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"common__privacy_policy", nil) attributes:@{ @"privacyPolicy" : @(YES),NSForegroundColorAttributeName: RealLightBlueColor}];
    [termString appendAttributedString:policyLinkString];
    
    NSMutableAttributedString *termString2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",JMOLocalizedString(@"sign_up__tnc_2", nil)] attributes:termNormalAttributeDict];
    [termString appendAttributedString:termString2];
    NSMutableAttributedString *serviceLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"common__terms_of_service", nil) attributes:@{ @"termOfService" : @(YES),NSForegroundColorAttributeName: RealLightBlueColor }];
    [termString appendAttributedString:serviceLinkString];
    self.termText = termString;
    [self.termTextView setAttributedText:self.termText];
    UITapGestureRecognizer * textViewTapGesureeRecognizer =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textTapped:)];
    [self.termTextView addGestureRecognizer:textViewTapGesureeRecognizer];
    // Do any additional setup after loading the view from its nib.
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
         [self.scrollView setContentInset:UIEdgeInsetsMake(-60, 0, 0, 0)];
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
    
    self.firstNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    self.lastNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    NSString * signUpLable= JMOLocalizedString(@"sign_up_create_an_account", nil);
    self.signUpLable.text =signUpLable;
    if (isEmpty(self.profileImage)) {
        [self.addPhotoButton setTitle:JMOLocalizedString(@"sign_up__photo", nil) forState:UIControlStateNormal];
    }
    [self.createBtn setTitle:JMOLocalizedString(@"common__create", nil) forState:UIControlStateNormal];
    self.loginWithFacebookLabel.text=JMOLocalizedString(@"sign_up__label_login_facebook", nil);
    [self resignAllTextField];
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id didTapPrivacyPolicy = [self.termText attribute:@"privacyPolicy" atIndex:characterIndex effectiveRange:&range];
        id didTapTermOfService = [self.termText attribute:@"termOfService" atIndex:characterIndex effectiveRange:&range];
        // Handle as required...
        if (didTapPrivacyPolicy) {
            [self showPrivacyPolicy];
        }else if(didTapTermOfService){
            [self showTermsOfService];
        }
        
    }
}

-(void)showPrivacyPolicy{
    [self.view endEditing:YES];
    Privatepolicy *policyVC = [[Privatepolicy alloc] initWithNibName:@"Privatepolicy" bundle:nil];
    policyVC.needShowTitleView = YES;
    [self presentViewController:policyVC animated:YES completion:nil];
}

-(void)showTermsOfService{
    [self.view endEditing:YES];
    Termsofservice *termVC = [[Termsofservice alloc] initWithNibName:@"Termsofservice" bundle:nil];
    termVC.needShowTitleView = YES;
    [self presentViewController:termVC animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)createButtonDidPress:(id)sender{
    
    if (self.createBtn.enabled) {
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Saved User Profile Details"];
        
        if ([self checkInputIsValidAlertTitle:YES]) {
            [self showLoadingHUD];
            
            NSString *firstName = self.firstNameTextField.text;
            NSString *lastName = self.lastNameTextField.text;
            
            if([[AppDelegate getAppDelegate]isEmpty:firstName])
                return;
            if([[AppDelegate getAppDelegate]isEmpty:lastName])
                return;
            RealApiClient * realAPiClient = [RealApiClient sharedClient];
            
            [[LoginByEmailModel shared] callRealNetworkMemberPhoneRegistrationPhoneRegID:[self.phoneNumberRegModel getStringPhoneRegID] pinInput:self.pinInput firstName:firstName lastName:lastName profileImage:self.profileImage Success:^(RealNetworkMemberInfo *realNetworkMemberInfo)
             {
                 // Mixpanel
                 Mixpanel *mixpanel = [Mixpanel sharedInstance];
                 [mixpanel track:@"Became User"];
                 [mixpanel.people set:@"$first_name" to:firstName];
                 [mixpanel.people set:@"$last_name" to:lastName];
                 NSString *name = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
                 [mixpanel.people set:@"$name" to:name];
                 [mixpanel.people set:@"Agent" to:@"NO"];
                 [mixpanel.people set:@"# of listings" to:[NSNumber numberWithInt:0]];
                 [mixpanel.people set:@"User since" to:[NSDate date]];
                 
                 [self loginBySocialNetwork:realNetworkMemberInfo];
             }
                                                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok)
             {
                 //   [self showAlertWithTitle:@"" message:errorMessage];
                 [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                               errorMessage:errorMessage
                                                                errorStatus:errorStatus
                                                                      alert:alert
                                                                         ok:ok];
                 [self hideLoadingHUD];
                 if(error.code == 100){
                     [self backButtonDidPress:nil];
                 }
             }];
        }
    }
}

-(void)loginBySocialNetwork:(RealNetworkMemberInfo*)memberInfo{
    
    NSString *name = [NSString stringWithFormat:@"%@ %@",memberInfo.FirstName,memberInfo.LastName];
    [[LoginBySocialNetworkModel shared]
     callLoginBySocialNetworkModelApiDeviceType:@"1"
     deviceToken:self.delegate.devicetoken
     socialNetworkType:@"6"
     userName:name
     userID:memberInfo.UserID
     email:memberInfo.Email
     photoURL:memberInfo.PhotoURL
     accessToken:memberInfo.AccessToken
     Success:^(LoginInfo *myLoginInfo, SystemSettings *SystemSettings) {
         
         // Mixpanel
         Mixpanel *mixpanel = [Mixpanel sharedInstance];
         [mixpanel createAlias:myLoginInfo.MemberID.stringValue forDistinctID:mixpanel.distinctId];
         [mixpanel identify:mixpanel.distinctId];
         [mixpanel.people set:@"Member ID" to:myLoginInfo.MemberID.stringValue];
         
         UIAlertAction *action = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
             [self registerQBWithInfo:myLoginInfo];
         }];
         [self showAlertWithTitle:JMOLocalizedString(@"error_message__reset_email_sent", nil) message:JMOLocalizedString(VerifyYourEmailAddress, nil) withAction:action];
         [self hideLoadingHUD];
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
               NSString *ok) {
         
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
         [self hideLoadingHUD];
         
     }];
    
}

- (IBAction)facebookBtnPress:(id)sender {
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Logged in with Facebook"];
    
    if ([self.delegate networkConnection]) {
        [self showLoadingHUD];
        [[FacebookLoginModel shared] facebookLoginPressSuccessBlock:^(id result, NSError *error) {
            
            [self hideLoadingHUD];
            NSString *username = [result objectForKey:@"name"];
            NSString *userphotolink =
            [[[result objectForKey:@"picture"] objectForKey:@"data"]
             objectForKey:@"url"];
            
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[NSURL URLWithString:userphotolink]
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // do something with image
                                        
                                        [self updateProfile:image];
                                    }
                                }];
            
            
            if (username) {
                NSArray *userNameArray = [username componentsSeparatedByString: @" "];
                self.firstNameTextField.text = [userNameArray objectAtIndex: 0];
                NSString *lastName = [username stringByReplacingOccurrencesOfString:self.firstNameTextField.text
                                                                         withString:@""];
                if (lastName) {
                    
                    if ([[lastName substringWithRange:NSMakeRange(0,1)] isEqualToString:@" "]) {
                        
                        lastName = [lastName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:@""];
                        
                    }
                    
                }
                
                self.lastNameTextField.text = lastName;
                
            }
            
        } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            
            [self hideLoadingHUD];
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
            
            
        } fromViewController:self.delegate.rootViewController];
    }else{
        
        
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:JMOLocalizedString(NotNetWorkConnectionText, nil) errorStatus:NotNetWorkConnection alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
    }
    
}

-(IBAction)backButtonDidPress:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)updateProfile:(UIImage*)image{
    
    if (image) {
        
        self.profileImage = image;
        self.profileImageView.image = image;
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
        self.profileImageView.clipsToBounds = YES;
        //        self.addPhotoButton.hidden = YES;
        [self.addPhotoButton setTitle:@"" forState:UIControlStateNormal];
        
    }
    
}

-(IBAction)addPhotoButtonDidPress:(id)sender{
    
    [kAppDelegate showImagePickerOn:self];
    [self resignAllTextField];
    
}

-(void)resignAllTextField{
    
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

-(BOOL)passwordFormatIsValid:(NSString*)password{
    NSString *passwordRegex = @"[A-Z0-9a-z_!]*"; ;
    NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordPredicate   evaluateWithObject:password];
}

#pragma mark -UIImagePickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    // assets contains PHAsset objects.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    
    // this one is key
    requestOptions.synchronous = true;
    PHAsset *asset = [assets firstObject];
    if (asset) {
        [RealUtility getImageBy:asset size:RealUploadPhotoSize handler:^(UIImage *result, NSDictionary *info) {
            RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:result];
            imageCropVC.delegate = self;
            [picker.navigationController pushViewController:imageCropVC animated:YES];
        }];
    }else{
        
    }
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    if (picker.selectedAssets.count >0){
        [picker deselectAsset:[picker.selectedAssets firstObject]];
    }
    return picker.selectedAssets.count<1;
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [controller.navigationController popViewControllerAnimated:YES];
}

// The original image has been cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect{
    [self updateProfile:croppedImage];
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
}

// The original image has been cropped. Additionally provides a rotation angle used to produce image.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
                  rotationAngle:(CGFloat)rotationAngle{
    [self updateProfile:croppedImage];
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
}

// The original image will be cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                  willCropImage:(UIImage *)originalImage{
    // Use when `applyMaskToCroppedImage` set to YES.
    //    [SVProgressHUD show];
}

-(void)setupTextFieldCheckError{
    
    [[self.firstNameTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     subscribeNext:^(id x) {
         if (([self checkInputIsValidAlertTitle:NO])) {
             self.textFieldErrorLabel.text=@"";
         }
     }];
    [[self.lastNameTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     subscribeNext:^(id x) {
         if (([self checkInputIsValidAlertTitle:NO])) {
             self.textFieldErrorLabel.text=@"";
         }
     }];
    
    [[RACObserve(self, profileImageView.image) ignore:nil] subscribeNext : ^(id x) {
        if (([self checkInputIsValidAlertTitle:NO])) {
            self.textFieldErrorLabel.text=@"";
        }
    }];
}

-(BOOL)checkInputIsValidAlertTitle:(BOOL)show{
    
    
    BOOL valid = YES;
    if(![RealUtility isValid:self.profileImage]){
        self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseuploadaprofilephoto, nil);
        valid = NO;
    }else if (![RealUtility isValid:self.firstNameTextField.text ]) {
        self.textFieldErrorLabel.text=JMOLocalizedString(Firstnameisrequired, nil);
        valid = NO;
    }else if (![RealUtility isValid:self.lastNameTextField.text]){
        self.textFieldErrorLabel.text=JMOLocalizedString(Lastnameisrequired, nil);
        valid = NO;
    }
    
    self.createBtn.enabled = valid;
    return valid;
}

-(void)setupKeyBoardDoneBtnAction{
    
    [self.lastNameTextField setReturnKeyType:UIReturnKeyDone];
    [[self.lastNameTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x){
        [self createButtonDidPress:self];
    }];
    
}

@end
