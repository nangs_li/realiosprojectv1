//
//  RealSignUpViewController.h
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealSignUpViewController : BaseViewController <UITextFieldDelegate>
@property (nonatomic,strong) IBOutlet UILabel *signupTitleLabel;
@property (nonatomic,strong) IBOutlet UITextField *emailTextField;
@property (nonatomic,strong) IBOutlet UIButton *loginButton;
@property (nonatomic,strong) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UILabel *textFieldErrorLabel;
@property (strong, nonatomic) IBOutlet UILabel *orLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *topTitleCreateAccount;
@property (strong, nonatomic) IBOutlet UILabel *loginWithFacebookLabel;

@end
