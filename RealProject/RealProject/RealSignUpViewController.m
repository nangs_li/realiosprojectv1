//
//  RealSignUpViewController.m
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealSignUpViewController.h"
#import "RealSignUpDetalViewController.h"
#import "RealLoginViewController.h"
#import "LoginByEmailModel.h"
#import "FacebookLoginModel.h"
// Mixpanel
#import "Mixpanel.h"

@interface RealSignUpViewController ()
@property (nonatomic ,strong) RealSignUpDetalViewController *signUpDetialVC;
@end

@implementation RealSignUpViewController

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage
                                              object:nil];
    
    [super viewDidLoad];
    [self.nextButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self.nextButton setBackgroundImage:[UIImage imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateDisabled];
    [self.loginButton setBackgroundImage:[UIImage imageWithBorder:self.loginButton.titleLabel.textColor
                                                             size:self.loginButton.frame.size] forState:UIControlStateNormal];
    UIImageView *leftImageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 41, 44)];
    leftImageView.contentMode = UIViewContentModeCenter;
    leftImageView.image = [UIImage imageNamed:@"ico_pop_invite_email"];
    NSString *shareTitle =JMOLocalizedString(@"sign_up__createanaccount", nil);
    [self.signupTitleLabel setAttributedTextWithMarkUp: shareTitle];
    [[AppDelegate getAppDelegate]setUpBlueAndWhiteTitleFontSizeUILabel:self.signupTitleLabel];
    self.emailTextField.delegate = self;
    self.emailTextField.leftView =leftImageView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    self.emailTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.emailTextField.frame.size];
    // Do any additional setup after loading the view from its nib.
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
    [self checkInputIsValid];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.topTitleCreateAccount.text=JMOLocalizedString(@"sign_up__create_an_account", nil);
    self.loginWithFacebookLabel.text=JMOLocalizedString(@"sign_up__label_login_facebook", nil);
    self.orLabel.text=JMOLocalizedString(@"sign_up__or", nil);
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common__email", nil) attributes:@{NSForegroundColorAttributeName: [UIColor textFieldPlaceholderColor]}];
    [self.nextButton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    
}

- (IBAction)loginButtonDidPress:(id)sender{
    RealLoginViewController *loginVc = [[RealLoginViewController alloc]initWithNibName:@"RealLoginViewController" bundle:nil];
    [self.navigationController pushViewController:loginVc animated:YES];
}

- (IBAction)nextButtonDidPress:(id)sender{
    
    // Mixpanel
    if (self.nextButton.enabled) {
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Chose Email Signup"];
        
        if ([self checkInputIsValid]) {
            
            NSString *email = self.emailTextField.text;
            [self showLoadingHUDWithProgress:JMOLocalizedString(@"alert__checking", nil)];
            [[LoginByEmailModel shared]callRealNetworkCheckEmailAvailability:email Success:^(id response) {
                
                // Mixpanel
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel.people set:@"$email" to:email];
                
                [self hideLoadingHUD];
                if(!self.signUpDetialVC){
                    self.signUpDetialVC = [[RealSignUpDetalViewController alloc]initWithNibName:@"RealSignUpDetalViewController" bundle:nil];
                }
                self.signUpDetialVC.email = email;
                [self.navigationController pushViewController: self.signUpDetialVC animated:YES];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                [self hideLoadingHUD];
                //[self showAlertWithTitle:nil message:errorMessage];
                [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                              errorMessage:errorMessage
                                                               errorStatus:errorStatus
                                                                     alert:alert
                                                                        ok:ok];
            }];
        }
    }
}

- (IBAction)backButtonDidPress:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkInputIsValid{
    BOOL valid = YES;
    NSString *email = self.emailTextField.text;
    if (![RealUtility isValid:email]) {
        valid = NO;
    }else if(![self emailFormatIsValid:email]){
        valid = NO;
    }
    self.nextButton.enabled = valid;
    return valid;
}

- (BOOL)emailFormatIsValid:(NSString*)email{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/va ... l-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)facebookLoginPress:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Chose Facebook Signup"];
    
    if ([self.delegate networkConnection]) {
        [self showLoadingHUD];
        [[FacebookLoginModel shared]
         facebookLoginPressSuccess:^(LoginInfo *myLoginInfo, SystemSettings *SystemSettings) {
             [[LoginBySocialNetworkModel shared] loginByQuickBloxOrSignUpQuickBloxWithInfo:myLoginInfo Success:^(id responseObject){
                 [self checkContactListRequestAccess];
             } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                         NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
                         NSString *ok) {
                 [self hideLoadingHUD];
                 [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
                 
             }];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
                   NSString *ok) {
             [self hideLoadingHUD];
             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
             
         } fromViewController:self.delegate.rootViewController];
        
    }else{
        
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:JMOLocalizedString(NotNetWorkConnectionText, nil) errorStatus:NotNetWorkConnection alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
        
    }
}

-(void)setupTextFieldCheckError{
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text = @"";
         }
     }];
    
}
- (BOOL)checkInputIsValidNotAlertTitle{
    BOOL valid = YES;
    NSString *email = self.emailTextField.text;
    if (![RealUtility isValid:email]) {
        valid = NO;
        self.textFieldErrorLabel.text=JMOLocalizedString(Emailisrequried, nil);
    }else if(![self emailFormatIsValid:email]){
        valid = NO;
        self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteravalidemailaddress, nil);
    }
    
    self.nextButton.enabled = valid;
    return valid;
}
-(void)setupKeyBoardDoneBtnAction{
    
    [self.emailTextField setReturnKeyType:UIReturnKeyDone];
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x){
        [self nextButtonDidPress:self];
    }];
}

@end
