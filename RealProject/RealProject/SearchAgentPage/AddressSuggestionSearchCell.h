//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressSuggestionSearchCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *suggestAddress;
@property (strong, nonatomic) IBOutlet UILabel *agentCount;
@end
