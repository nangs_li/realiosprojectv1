//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "AddressSuggestionSearchCell.h"
#import "UIImage+Utility.h"
@implementation AddressSuggestionSearchCell

- (void)awakeFromNib {
  // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  // super setSelected

  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}
@end
