//
//  AgentCell.h
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentDetailProfile.h"
#import "ApartmentDetailCardView.h"
#import "AgentProfile.h"
#import "FBShimmeringView.h"

@protocol AgentCellDelegate <NSObject>

- (void) AgentCellApartmentBackgroundImageView:(AgentProfile*)profile;

@end


@interface AgentCell : UITableViewCell <AgentDetailProfileDelegate>

@property (nonatomic,strong) id<AgentCellDelegate> delegate;
@property (nonatomic, strong) FBShimmeringView *shimmeringView;
@property (nonatomic,strong) AgentDetailProfile* agentProfileView;
@property (nonatomic,strong) ApartmentDetailCardView* apartmentDetailCardView;
@property (nonatomic,strong) UIImageView *gradientMask;
@property (nonatomic,strong) AgentProfile *agentProfile;
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withFrame:(CGRect)frame withProfile:(AgentProfile*)profile;
-(void)configureWithAgentProfile:(AgentProfile*)profile;
-(void)adjustViewWithOffset:(CGFloat)offset;
-(void)updateImage;
-(void)showSlogan:(BOOL)show animated:(BOOL)animated;
-(void)setShimmering:(BOOL)shimmering;
-(void)willEndDisplay;
-(void)resetLoadingStatus;
@end
