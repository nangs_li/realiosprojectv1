//
//  AgentDetailCell.h
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentDetailCardView.h"
@interface AgentDetailCell : UICollectionViewCell
@property (nonatomic,strong) AgentDetailCardView *cardView;
-(void)configureCell:(AgentProfile*)profile withMinSize:(CGSize)size;
@end
