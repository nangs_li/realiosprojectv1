//
//  AgentDetailProfile.m
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentDetailProfile.h"
#import "RealUtility.h"
#import "UIView+Utility.h"
#import "UIImage+Utility.h"
#import "UIImageView+Utility.h"
#define DesiredCommonPadding  8.0
#define DesiredSloganPadding 26.0
#define DesiredScaleFactor 0.3

@implementation AgentDetailProfile

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)awakeFromNib{
    [super awakeFromNib];
    [self saveOriginalFrames];
    [self saveOriginalFonts];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage
                                              object:nil];
    
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.followTitleLabel.text=JMOLocalizedString(@"common__followers", nil);
    [self.followButton setTitle:JMOLocalizedString(@"common__follow", nil) forState:UIControlStateNormal];
}
- (void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
+(AgentDetailProfile*)initWithType:(AgentProfileType)profileType{
    AgentDetailProfile *profileView;
    NSString *nibName;
    switch (profileType) {
        case AgentProfileDetail:{
            nibName = @"AgentDetailProfile";
            break;
        }case AgentProfileAction:{
            nibName = @"AgentActionProfile";
            break;
        }
        default:
            break;
    }
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    profileView = [subviewArray objectAtIndex:0];
    profileView.profileType = profileType;
    profileView.backgroundImageView.frame = CGRectMake(0, 0, profileView.frame.size.width, profileView.frame.size.width);
    [profileView.backgroundButton setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [profileView.backgroundButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.0 alpha:0.7]] forState:UIControlStateHighlighted];
    if (profileType == AgentProfileAction) {
        [profileView.followButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
        [profileView.followButton setBackgroundImage:[UIImage imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateSelected];
        [profileView.chatButton setBackgroundImage:[UIImage imageWithBorder:RealBlueColor size:profileView.chatButton.frame.size] forState:UIControlStateNormal];
        profileView.sloganView.hidden =YES;
    }else if (profileType == AgentProfileDetailWithoutSlogan){
        profileView.sloganView.hidden = YES;
    }
    return profileView;
}
-(void)configureProfile:(AgentProfile *)profile{
    [self cancelImageOperation];
    self.followTitleLabel.text=JMOLocalizedString(@"common__followers", nil);
    [self.followButton setTitle:JMOLocalizedString(@"common__follow", nil) forState:UIControlStateNormal];
    [self.followButton setTitle:JMOLocalizedString(@"common__following", nil) forState:UIControlStateSelected];
    NSString *profileUrl = profile.AgentPhotoURL;
    NSString *bgImageURL = @"";
    NSString *name = profile.MemberName;
    BOOL isFolowing = [profile getIsFollowing];
    
    NSString *follwerCount = [NSString stringWithFormat:@"%d",profile.FollowerCount];
    NSString *slogan = [profile getSlogan];
    NSURL *profilePicUrl = [NSURL
                            URLWithString:profileUrl];
    
    if ([RealUtility isValid:profile.AgentListing] &&[RealUtility isValid:profile.AgentListing.Photos]) {
        Photos *bgPhoto =[profile.AgentListing.Photos firstObject];
        bgImageURL = bgPhoto.URL;
    }
    BOOL notSelf = profile.MemberID != [MyAgentProfileModel shared].myAgentProfile.MemberID;
    self.followButton.hidden = !notSelf;
    self.chatButton.hidden = !notSelf;
    self.chatButton.enabled = [[LoginBySocialNetworkModel shared] qbAccountIsReady];
    self.followButton.selected = isFolowing;
    
    self.agentProfileImageView.layer.cornerRadius = self.profileWidthConstraint.constant / 2;
    self.agentProfileImageView.clipsToBounds = YES;
    [self.agentProfileImageView loadImageURL:profilePicUrl withIndicator:YES];
    if (([profile getIsFollowing] && !profile.meProfile) || profile.isPreview) {
        self.agentProfileImageView.borderType = UIImageViewBorderTypeFollowing;
    }else{
        self.agentProfileImageView.borderType = UIImageViewBorderTypeNone;
    }
    
    
    if (!self.loadCustomBackground) {
        NSURL *bgurl = [NSURL URLWithString:bgImageURL];
        [self.backgroundImageView loadImageURL:bgurl withIndicator:YES];
    }
    
    self.agentNameLabel.text = name;
    self.followCountLabel.text = [RealUtility getDecimalFormatByString:follwerCount withUnit:nil];;
    //    [self.followCountLabel sizeToFit];
    //    [self.followTitleLabel sizeToFit];
            self.followCountLabel.textColor = RealBlueColor;
    if (self.profileType == AgentProfileDetail) {
        [self showLoadingPlaceholder:![profile readyToShow]];
        [self changeSlogan:slogan];
    }else if(self.profileType == AgentProfileAction){
        [self.followCountLabel setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter padding:0];
        [self.followTitleLabel setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter padding:0];
        
    }
    
}
-(void)changeSlogan:(NSString*)slogan{
    if([RealUtility isValid:slogan]){
        //        self.sloganView.hidden =  NO;
        self.sloganLabel.text = slogan;
        self.sloganLabel.numberOfLines = 0;
        self.sloganImageView.image =[UIImage imageNamed:@"bubble"];
    }else{
        self.sloganView.hidden = YES;
    }
}

-(void)changePageIndex:(int)index{
    self.pageControl.currentPage =index;
}

-(void)showLoadingPlaceholder:(BOOL)show{
    //    if (self.loadingView.hidden != !show) {
    self.loadingView.hidden = !show;
    //    }
}

-(void)cancelImageOperation{
    [self.agentProfileImageView cancelImageOperation];
    [self.backgroundImageView cancelImageOperation];
}

-(void) saveOriginalFrames{
    self.infoView.frame = CGRectMake(self.infoView.frame.origin.x, self.infoView.frame.origin.y, [RealUtility screenBounds].size.width - self.infoView.frame.origin.x *2, self.infoView.frame.size.height);
    self.agentNameLabel.frame = CGRectMake(self.agentNameLabel.frame.origin.x, self.agentNameLabel.frame.origin.y, [RealUtility screenBounds].size.width - self.agentNameLabel.frame.origin.x *2, self.agentNameLabel.frame.size.height);
    [self saveOriginalViewFrame:self.infoView key:@"infoView"];
    [self saveOriginalViewFrame:self.agentProfileImageView key:@"agentProfileImageView"];
    [self saveOriginalViewFrame:self.agentNameLabel key:@"agentNameLabel"];
    [self saveOriginalViewFrame:self.followerView key:@"followerView"];
    [self saveOriginalViewFrame:self.followCountLabel key:@"followCountLabel"];
    [self saveOriginalViewFrame:self.followTitleLabel key:@"followTitleLabel"];
    [self saveOriginalViewFrame:self.sloganView key:@"sloganView"];
}

-(void)saveOriginalFonts{
    [self saveOriginalFont:self.agentNameLabel.font key:@"agentNameLabel"];
    [self saveOriginalFont:self.followCountLabel.font key:@"followCountLabel"];
    [self saveOriginalFont:self.followTitleLabel.font key:@"followTitleLabel"];
}

-(void)saveOriginalViewFrame:(UIView*)view key:(NSString*)key{
    if (!view || !key) {
        return;
    }
    if (!self.originalSizeDict) {
        self.originalSizeDict = [[NSMutableDictionary alloc]init];
    }
    [self.originalSizeDict setObject:NSStringFromCGRect(view.frame) forKey:key];
}

-(CGRect)getOriginalFrame:(NSString*)key{
    CGRect frame = CGRectZero;
    NSString* frameString = self.originalSizeDict[key];
    
    if ([RealUtility isValid:frameString]) {
        frame = CGRectFromString(frameString);
    }
    
    return frame;
}

-(void)saveOriginalFont:(UIFont*)font key:(NSString*)key{
    if (!font || !key) {
        return;
    }
    if (!self.originalFontDict) {
        self.originalFontDict = [[NSMutableDictionary alloc]init];
    }
    [self.originalFontDict setObject:font forKey:key];
}

-(UIFont *)getOriginalFont:(NSString*)key{
    UIFont *font = self.originalFontDict[key];
    if (![RealUtility isValid:font]) {
        font =[UIFont systemFontOfSize:[UIFont systemFontSize]];
    }
    return font;
}

-(void)resetAgentProfileScale{
    CGRect profileImageFrame = [self getOriginalFrame:@"agentProfileImageView"];
    self.profileWidthConstraint.constant = profileImageFrame.size.width;
    self.profileHeightConstraint.constant = profileImageFrame.size.height;
    
    self.agentProfileImageView.layer.cornerRadius = self.profileWidthConstraint.constant/2.0f;
    
}
-(void)adjustAgentProfileInfoByContentOffset:(CGPoint)contentOffset withAlphaEffect:(BOOL)needAlphaEffect{
    
    UIView *mostBottomView = nil;
    
    
    CGFloat scalePercentage = (contentOffset.y /profileHeaderMinHeight);
    CGFloat followerCountPadding = [self getScaledValueWithOriginal:DesiredCommonPadding scale:DesiredScaleFactor percent:scalePercentage*8];
    CGRect profileImageFrame = [self getOriginalFrame:@"agentProfileImageView"];
    
    
    profileImageFrame.origin.y = 0.0f;
    CGFloat profileImageWidth = [self getScaledValueWithOriginal:profileImageFrame.size.width scale:DesiredScaleFactor percent:scalePercentage];
    CGFloat profileImageHeight = [self getScaledValueWithOriginal:profileImageFrame.size.height scale:DesiredScaleFactor percent:scalePercentage];
    self.profileWidthConstraint.constant = profileImageWidth;
    self.profileHeightConstraint.constant = profileImageWidth;
    
    self.agentProfileImageView.layer.cornerRadius = profileImageWidth/2.0f;
    //    [self.agentProfileImageView setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter padding:0];
    //    DDLogDebug(@"followerCountPadding:%f",followerCountPadding);
    mostBottomView = self.agentProfileImageView;
    
    self.followerCountTopSpacingConstraint.constant = followerCountPadding;
    if(self.profileType == AgentProfileDetail){
        if (needAlphaEffect) {
            CGFloat alphaScale = 1.0f * scalePercentage *1.5f;
            if (alphaScale >1.0f) {
                alphaScale = 1.0f;
            }
            self.sloganView.alpha = 1.0f  -alphaScale;
        }
    }else if (self.profileType == AgentProfileAction){
        if (needAlphaEffect) {
            CGFloat alphaScale = 1.0f * scalePercentage *1.5f;
            if (alphaScale >1.0f) {
                alphaScale = 1.0f;
            }
            self.followCountLabel.alpha = 1.0f  -alphaScale;
            self.followTitleLabel.alpha = 1.0f -alphaScale;
            self.actionView.alpha = 1.0f -alphaScale;
        }
    }
}

-(CGFloat)getScaledValueWithOriginal:(CGFloat)original scale:(CGFloat)scale percent:(CGFloat)percent{
    int roundMaxScaledValue = roundf(original*scale);
    CGFloat scaledValue = roundMaxScaledValue *percent;
    return original - scaledValue;
}

-(IBAction)backgroundImageButtonDidPress:(id)sender{
    if ([self.delegate respondsToSelector:@selector(AgentDetailProfileBackgroundImageButtonDidPress)]){
        [self.delegate AgentDetailProfileBackgroundImageButtonDidPress];
    }
}

-(void)prefetchImage:(AgentProfile*)profile{
    if(![RealUtility isValid:profile]){
        return;
    }
    NSString *profileUrl = profile.AgentPhotoURL;
    NSString *bgImageURL = @"";
    NSURL *profilePicUrl = [NSURL
                            URLWithString:profileUrl];
    
    if ([RealUtility isValid:profile.AgentListing] &&[RealUtility isValid:profile.AgentListing.Photos]) {
        Photos *bgPhoto =[profile.AgentListing.Photos firstObject];
        bgImageURL = bgPhoto.URL;
    }
    [self.agentProfileImageView loadImageURL:profilePicUrl withIndicator:YES];
    
    if (!self.loadCustomBackground) {
        NSURL *bgurl = [NSURL URLWithString:bgImageURL];
        [self.backgroundImageView loadImageURL:bgurl withIndicator:YES];
    }
}
@end
