//
//  AgentExperienceItemView.m
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentExperienceItemView.h"
@implementation AgentExperienceItemView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(id)initWithAgentExperience:(AgentExperience*)experience size:(CGSize)size withStyle:(ItemViewStyle)style{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AgentExperienceItemView" owner:self options:nil];
    self = (AgentExperienceItemView *)[nib objectAtIndex:0]; // or if it exists, (MCQView *)[nib objectAtIndex:0];
    [self changeStyle:style];
    self.preferedSize = size;
    self.frame= CGRectMake(0, 0, size.width, size.height);
    [self configureWithExperience:experience];
    return self;
}

-(void)changeStyle:(ItemViewStyle)style{
    self.viewStyle = style;
    if (style == ItemViewStyleWhite) {
        self.positionLabel.textColor = [UIColor whiteColor];
        self.companyNameLabel.textColor = [UIColor lightGrayColor];
        self.workingPeriodLabel.textColor = [UIColor lightGrayColor];
    }else if (style == ItemViewStyleBlack){
        self.positionLabel.textColor = [UIColor colorWithRed:50/255.0f green:59/255.0f blue:72/255.0f alpha:1.0f];
        self.companyNameLabel.textColor =[UIColor darkGrayColor];
        self.workingPeriodLabel.textColor = [UIColor darkGrayColor];
    }
}

-(void)configureWithExperience:(AgentExperience *)experience{
    NSString *companyName = experience.Company;
    NSString *position = experience.Title;
    NSString *periodString = [experience getPeriodString];
    CGRect companyRect = self.companyNameLabel.frame;
    CGRect positionRect = self.positionLabel.frame;
    CGRect periodRect = self.workingPeriodLabel.frame;
    
    self.companyNameLabel.text = companyName;
    self.positionLabel.text = position;
    self.workingPeriodLabel.text = periodString;
    CGFloat padding = 29;
    CGFloat deleteButtonWidth = 44;
    CGSize maxLabelSize = CGSizeMake(self.preferedSize.width, CGFLOAT_MAX);
    if (self.viewStyle == ItemViewStyleWhite) {
        maxLabelSize = CGSizeMake(self.preferedSize.width - deleteButtonWidth - padding*2, CGFLOAT_MAX);
    }else{
        maxLabelSize = CGSizeMake(self.preferedSize.width - padding*2, CGFLOAT_MAX);
    }
    companyRect.size = [self.companyNameLabel sizeThatFits:maxLabelSize];
    if (self.viewStyle == ItemViewStyleWhite) {
        maxLabelSize = CGSizeMake(self.preferedSize.width - deleteButtonWidth - padding*2 , CGFLOAT_MAX);
    }else{
        maxLabelSize = CGSizeMake(self.preferedSize.width - padding*2, CGFLOAT_MAX);
    }
    positionRect.size =[self.positionLabel sizeThatFits:maxLabelSize];
    periodRect.size =[self.workingPeriodLabel sizeThatFits:maxLabelSize];
    
    self.companyNameLabel.frame = companyRect;
    self.positionLabel.frame = positionRect;
    self.workingPeriodLabel.frame =periodRect;
    
    UIView *mostBottomView = self.positionLabel;
    
    [self.companyNameLabel moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:0];
    [self.companyNameLabel align:mostBottomView direction:RelativeDirectionLeft padding:0];
    mostBottomView = self.companyNameLabel;
    
    [self.workingPeriodLabel moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:0];
    [self.workingPeriodLabel align:mostBottomView direction:RelativeDirectionLeft padding:0];
    mostBottomView = self.workingPeriodLabel;
    
    [self.dividerView moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:12];
    [self.dividerView align:mostBottomView direction:RelativeDirectionLeft padding:0];
    mostBottomView = self.dividerView;
    
    self.frame = CGRectMake(0, 0, self.preferedSize.width, mostBottomView.frame.origin.y +mostBottomView.frame.size.height);
    
}


+(CGSize)calViewSizeWithExperience:(AgentExperience *)experience withInitSize:(CGSize)size withStyle:(ItemViewStyle)style{
    AgentExperienceItemView* tempItemView = [[AgentExperienceItemView alloc]initWithAgentExperience:experience size:size withStyle:style];
    return tempItemView.frame.size;
}
@end
