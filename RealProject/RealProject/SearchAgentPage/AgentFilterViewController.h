//
//  AgentFilterViewController.h
//  productionreal2
//
//  Created by Alex Hung on 28/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterStepCell.h"
#import "FilterTypeCell.h"

#import "FilterTextInputTableViewCell.h"
@protocol AgentFilterViewControllerDelegate <NSObject>
-(void)agentFilterDidDismissWithFilterDict:(NSDictionary*)dict applyFilter:(BOOL)apply;
@end

@interface AgentFilterViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,FilterStepCellDelagate,FilterTypeCellDelegate,FilterTextInputCellDelagate>
@property (nonatomic,strong) id<AgentFilterViewControllerDelegate> delegate;
@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIView *tableFooterView;
@property (nonatomic,strong) IBOutlet UIPickerView *pickerView;
@property (nonatomic,strong) IBOutlet UIView *pickerContainerView;
@property (nonatomic,strong) IBOutlet UIButton *applyButton;
@property (nonatomic,strong) IBOutlet UIButton *resetButton;
@property (nonatomic,strong) IBOutlet UILabel *typeOfSpace;
@property (nonatomic,strong) IBOutlet UIBarButtonItem *doneItem;
-(IBAction)dismissButtonDidPress:(id)sender;
-(IBAction)pickerDoneButtonDidPress:(id)sender;
-(IBAction)applyButtonDidPress:(id)sender;
-(IBAction)resetButtonDidPress:(id)sender;

@end
