//
//  AgentFilterViewController.m
//  productionreal2
//
//  Created by Alex Hung on 28/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentFilterViewController.h"
#import "FilterTypeCell.h"
#import "FilterRangeCell.h"
#import "FilterStepCell.h"
#import "RealUtility.h"
#import "UIView+Utility.h"
#import "UIImage+Utility.h"
@interface AgentFilterViewController ()
@property(nonatomic, strong) NSArray *priceMinArray;
@property(nonatomic, strong) NSArray *priceMaxArray;
@property(nonatomic, strong) NSArray *sizeMinArray;
@property(nonatomic, strong) NSArray *sizeMaxArray;
@property(nonatomic, strong) NSArray *spokenLangArray;
@property(nonatomic, strong) NSString *pickerType;
@property(nonatomic, strong) NSDictionary *cachedFilterDict;
@property(nonatomic, assign) BOOL isShowingPickerView;
@end

@implementation AgentFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.cachedFilterDict = [[SystemSettingModel shared].filterDict copy];
    [self.applyButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor]
                                forState:UIControlStateNormal];
    [self.resetButton
     setBackgroundImage:[UIImage
                         imageWithColor:[UIColor colorWithRed:42 / 255.0f
                                                        green:42 / 255.0f
                                                         blue:42 / 255.0f
                                                        alpha:1.0f]]
     forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setUpFixedLabelTextAccordingToSelectedLanguage) name:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage
                                              object:nil];
    [self showPickerView:nil];
    [self registerCell];
    [self initFilterArray];
    // Do any additional setup after loading the view from its nib.
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.tableView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.tableView.scrollEnabled=YES;
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.doneItem.title=JMOLocalizedString(@"error_message__done", nil);
    self.typeOfSpace.text=JMOLocalizedString(@"common__filter", nil);
    [self.applyButton setTitle:JMOLocalizedString(@"search_filter__apply_filter", nil) forState:UIControlStateNormal];
    
    [self.resetButton setTitle:JMOLocalizedString(@"search_filter__reset", nil)
                      forState:UIControlStateNormal];
    
    [self.tableView reloadData];
}
- (void) viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self setUpFixedLabelTextAccordingToSelectedLanguage];
}
- (void)registerCell {
    [self.tableView
     registerNib:[UINib nibWithNibName:@"FilterTypeCell" bundle:nil]
     forCellReuseIdentifier:@"FilterTypeCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FilterRangeCell"
                                               bundle:nil]
         forCellReuseIdentifier:@"FilterRangeCell"];
    [self.tableView
     registerNib:[UINib nibWithNibName:@"FilterStepCell" bundle:nil]
     forCellReuseIdentifier:@"FilterStepCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FilterTextInputTableViewCell" bundle:nil] forCellReuseIdentifier:@"FilterTextInputTableViewCell"];
}

- (IBAction)dismissButtonDidPress:(id)sender {
    [SystemSettingModel shared].filterDict = [NSMutableDictionary dictionaryWithDictionary:self.cachedFilterDict];
    [self cleanFilterDict];
    if ([self.delegate
         respondsToSelector:@selector(agentFilterDidDismissWithFilterDict:applyFilter:)]) {
        [self.delegate
         agentFilterDidDismissWithFilterDict:[SystemSettingModel shared]
         .filterDict applyFilter:NO];
    }
}

- (IBAction)pickerDoneButtonDidPress:(id)sender {
    [self showPickerView:nil];
}

- (IBAction)applyButtonDidPress:(id)sender {
    [self cleanFilterDict];
    if ([self.delegate
         respondsToSelector:@selector(agentFilterDidDismissWithFilterDict:applyFilter:)]) {
        [self.delegate
         agentFilterDidDismissWithFilterDict:[SystemSettingModel shared]
         .filterDict applyFilter:YES];
    }
}

-(void)cleanFilterDict{
    if ([SystemSettingModel shared].filterDict) {
        NSMutableDictionary *cleanDict = [[SystemSettingModel shared].filterDict mutableCopy];
        for (NSString *dictKey in [cleanDict copy]) {
            if ([dictKey isEqualToString:@"bathroom"]) {
                int bathroom = [cleanDict[dictKey]intValue];
                if (bathroom == 0) {
                    [cleanDict removeObjectForKey:dictKey];
                }
            }else if ([dictKey isEqualToString:@"bedroom"]){
                int bedroom = [cleanDict[dictKey]intValue];
                if (bedroom == 0) {
                    [cleanDict removeObjectForKey:dictKey];
                }
            }else if ([dictKey isEqualToString:@"price"]){
                NSDictionary *priceDict = cleanDict[dictKey];
                NSString *priceMin = priceDict[@"min"];
                NSString *priceMax = priceDict[@"max"];
                NSInteger state = [priceDict[@"state"] integerValue];
                BOOL haveMax = (![priceMin isEqualToString:@"0"] && priceMin);
                //                BOOL haveMin = (![priceMin isEqualToString:@"0"] && priceMin);
                BOOL haveState  = state != 0 ;
                if (!haveMax && !haveState) {
                    [cleanDict removeObjectForKey:dictKey];
                }
            }else if ([dictKey isEqualToString:@"size"]){
                NSDictionary *sizeDict = cleanDict[dictKey];
                NSString *sizeMin = sizeDict[@"min"];
                NSString *sizeMax = sizeDict[@"max"];
                NSInteger state = [sizeDict[@"state"] integerValue];
                BOOL haveMax = (![sizeMin isEqualToString:@"0"] && sizeMin);
                //                BOOL haveMin = (![sizeMin isEqualToString:@"0"] && sizeMin);
                BOOL haveState  = state != 0 ;
                if (!haveMax && !haveState) {
                    [cleanDict removeObjectForKey:dictKey];
                }
            }else if ([dictKey isEqualToString:@"space"]){
                NSArray *spaceArray = cleanDict[dictKey];
                if (spaceArray.count <= 0) {
                    [cleanDict removeObjectForKey:dictKey];
                }
            }else if ([dictKey isEqualToString:@"spokenLanguage"]){
                NSString *spokenLanguage = cleanDict[dictKey];
                
                if ([spokenLanguage isEqualToString:JMOLocalizedString(@"search_filter__any", nil)]) {
                    [cleanDict removeObjectForKey:dictKey];
                }
            }
        }
        [SystemSettingModel shared].filterDict = [cleanDict mutableCopy];
    }
}

- (IBAction)resetButtonDidPress:(id)sender {
    [self showPickerView:nil];
    [SystemSettingModel shared].filterDict = [[NSMutableDictionary alloc] init];
    [self.tableView reloadData];
}
#pragma mark -
#pragma mark - Table View Datasoure And Delegate

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self filterItemArray].count;
}
- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *filterItem = [self filterItemArray][indexPath.row];
    if ([filterItem isEqualToString:@"space"]) {
        return 130;
    } else {
        return 88;
    }
}

// Row display. Implementers should *always* try to reuse cells by setting each
// cell's reuseIdentifier and querying for available reusable cells with
// dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators)
// and data source (accessory views, editing controls)
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *filterItem = [self filterItemArray][indexPath.row];
    NSDictionary *filterItemDict = [self getFilterItemDict:filterItem];
    if ([filterItem isEqualToString:@"space"]) {
        FilterTypeCell *typeCell = (FilterTypeCell*) cell;
        [typeCell initialAnimation:0.6];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *filterItem = [self filterItemArray][indexPath.row];
    UITableViewCell *cell;
    NSDictionary *filterItemDict = [self getFilterItemDict:filterItem];
    if ([filterItem isEqualToString:@"space"]) {
        FilterTypeCell *typeCell =
        [tableView dequeueReusableCellWithIdentifier:@"FilterTypeCell"];
        typeCell.selectionStyle = UITableViewCellSelectionStyleNone;
        typeCell.delegate = self;
        [typeCell configureCell:filterItemDict];
        cell = typeCell;
        
    } else if ([filterItem isEqualToString:@"spokenLanguage"]) {
        FilterRangeCell *rangeCell =
        [tableView dequeueReusableCellWithIdentifier:@"FilterRangeCell"];
        [rangeCell configureCell:filterItemDict];
        cell = rangeCell;
        if([filterItem isEqualToString:@"spokenLanguage"])
            [rangeCell.filterTitleButton setTitle:JMOLocalizedString(@"search_filter__agent_language_skills", nil) forState:UIControlStateNormal];
    }else if([filterItem isEqualToString:@"price"] ||
             [filterItem isEqualToString:@"size"] ){
        FilterTextInputTableViewCell *inputCell = [tableView dequeueReusableCellWithIdentifier:@"FilterTextInputTableViewCell"];
        [inputCell configureCell:filterItemDict];
        inputCell.delegate = self;
        cell = inputCell;
        if([filterItem isEqualToString:@"price"])
            [inputCell.filterTitleButton setTitle:JMOLocalizedString(@"filters__price", nil) forState:UIControlStateNormal];
        if([filterItem isEqualToString:@"size"])
            [inputCell.filterTitleButton setTitle:JMOLocalizedString(@"filters__size", nil) forState:UIControlStateNormal];
    }else {
        FilterStepCell *stepCell =
        [tableView dequeueReusableCellWithIdentifier:@"FilterStepCell"];
        [stepCell configureCell:filterItemDict];
        stepCell.delagate = self;
        cell = stepCell;
        if([filterItemDict[@"filterTitle"] isEqualToString:@"Bedroom"])
            [stepCell.filterTitleButton setTitle:JMOLocalizedString(@"filters__bedroom", nil) forState:UIControlStateNormal];
        if([filterItemDict[@"filterTitle"] isEqualToString:@"Bathroom"])
            [stepCell.filterTitleButton setTitle:JMOLocalizedString(@"filters__bathroom", nil) forState:UIControlStateNormal];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *filterItem = [self filterItemArray][indexPath.row];
    if ([filterItem isEqualToString:@"price"]) {
        self.pickerType = nil;
    } else if ([filterItem isEqualToString:@"size"]) {
        self.pickerType = nil;
    } else if ([filterItem isEqualToString:@"spokenLanguage"]) {
        self.pickerType = filterItem;
    } else {
        self.pickerType = nil;
    }
    
    [self showPickerView:self.pickerType];
}

#pragma mark -
#pragma mark Picker View Datasoure And Delegate
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    NSInteger component = 0;
    if ([self.pickerType isEqual:@"price"] || [self.pickerType isEqual:@"size"]) {
        component = 2;
    } else if ([self.pickerType isEqualToString:@"spokenLanguage"]) {
        component = 1;
    }
    return component;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    NSInteger row = 0;
    if ([self.pickerType isEqualToString:@"price"]) {
        if (component == 0) {
            row = self.priceMinArray.count;
        } else if (component == 1) {
            row = self.priceMaxArray.count;
        }
    } else if ([self.pickerType isEqualToString:@"size"]) {
        if (component == 0) {
            row = self.sizeMinArray.count;
        } else if (component == 1) {
            row = self.sizeMaxArray.count;
        }
    } else if ([self.pickerType isEqualToString:@"spokenLanguage"]) {
        row = self.spokenLangArray.count;
    }
    return row;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    NSString *title = nil;
    if ([self.pickerType isEqualToString:@"price"]) {
        if (component == 0) {
            title = self.priceMinArray[row];
        } else if (component == 1) {
            title = self.priceMaxArray[row];
        }
    } else if ([self.pickerType isEqualToString:@"size"]) {
        if (component == 0) {
            title = self.sizeMinArray[row];
        } else if (component == 1) {
            title = self.sizeMaxArray[row];
        }
    } else if ([self.pickerType isEqualToString:@"spokenLanguage"]) {
        title = self.spokenLangArray[row];
    }
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    id value = @"";
    if ([self.pickerType isEqualToString:@"price"]) {
        NSMutableDictionary *filterDict = [self getFilterByFilterType:@"price"];
        if (!filterDict) {
            filterDict = [[NSMutableDictionary alloc] init];
        }
        NSString *price = @"";
        NSString *key = @"";
        if (component == 0) {
            key = @"min";
            price = self.priceMinArray[row];
        } else if (component == 1) {
            key = @"max";
            price = self.priceMaxArray[row];
        }
        [filterDict setObject:price forKey:key];
        value = filterDict;
    } else if ([self.pickerType isEqualToString:@"size"]) {
        NSMutableDictionary *filterDict = [self getFilterByFilterType:@"size"];
        if (!filterDict) {
            filterDict = [[NSMutableDictionary alloc] init];
        }
        NSString *size = @"";
        NSString *key = @"";
        if (component == 0) {
            key = @"min";
            size = self.sizeMinArray[row];
        } else if (component == 1) {
            key = @"max";
            size = self.sizeMaxArray[row];
        }
        [filterDict setObject:size forKey:key];
        value = filterDict;
    } else if ([self.pickerType isEqualToString:@"spokenLanguage"]) {
        NSString *lang = self.spokenLangArray[row];
        value = lang;
    }
    
    [self createFilter:self.pickerType value:value];
    [self.tableView reloadData];
}

- (NSArray *)filterItemArray {
    return @[
             @"space",
             @"price",
             @"size",
             @"bedroom",
             @"bathroom",
             @"spokenLanguage"
             ];
}

- (void)showPickerView:(NSString *)filterType {
    self.pickerType = filterType;
    
    if ([RealUtility isValid:filterType]) {
        [self.pickerView reloadAllComponents];
        if (!self.isShowingPickerView) {
            [self.pickerContainerView setViewAlignmentInSuperView:ViewAlignmentBottom padding:-self.pickerContainerView.frame.size.height];
            self.pickerContainerView.hidden = NO;
            [UIView animateWithDuration:0.3f
                             animations:^{
                                 [self.pickerContainerView
                                  setViewAlignmentInSuperView:ViewAlignmentBottom
                                  padding:0];
                             }];
        }
        
        //        if ([filterType isEqualToString:@"price"] ||
        //            [filterType isEqualToString:@"size"]) {
        //            NSDictionary *filterDict = [self getFilterByFilterType:filterType];
        //            NSString *minString = filterDict[@"min"];
        //            NSString *maxString = filterDict[@"max"];
        //            int minRow = 0;
        //            int maxRow = 0;
        //            if ([filterType isEqualToString:@"price"]) {
        //                minRow = (int)[self.priceMinArray indexOfObject:minString];
        //                maxRow = (int)[self.priceMaxArray indexOfObject:maxString];
        //            } else if ([filterType isEqualToString:@"size"]) {
        //                minRow = (int)[self.sizeMinArray indexOfObject:minString];
        //                maxRow = (int)[self.sizeMaxArray indexOfObject:maxString];
        //            }
        //
        //            minRow = minRow != NSNotFound && minRow != NAN ? minRow : 0;
        //            maxRow = maxRow != NSNotFound && maxRow != NAN ? maxRow : 0;
        //
        //            [self.pickerView selectRow:minRow inComponent:0 animated:NO];
        //            [self.pickerView selectRow:maxRow inComponent:1 animated:NO];
        //        } else
        //
        if ([filterType isEqualToString:@"spokenLanguage"]) {
            NSString *lang = [self getFilterByFilterType:filterType];
            int row ;
            if (isEmpty(lang)) {
                row =0;
            }else{
            row = (int)[self.spokenLangArray indexOfObject:lang];
            }
            row = row != NSNotFound ? row : 0;
            [self.pickerView selectRow:row inComponent:0 animated:NO];
        }
        
        self.isShowingPickerView = YES;
    } else {
        if (self.isShowingPickerView) {
            self.isShowingPickerView = NO;
            [UIView animateWithDuration:0.3f
                             animations:^{
                                 [self.pickerContainerView
                                  setViewAlignmentInSuperView:ViewAlignmentBottom
                                  padding:-self.pickerContainerView
                                  .frame.size.height];
                             }];
        }
    }
}

- (void)closeAllKeyBoard{
    [self.view endEditing:YES];
    
}
- (void)initFilterArray {
    NSArray *priceMinIntArray =
    @[ @(-1), @(10000), @(30000), @(50000), @(1000000) ];
    NSArray *priceMaxIntArray =
    @[ @(-1), @(20000), @(40000), @(60000), @(2000000) ];
    NSArray *sizeMinIntArray =
    @[ @(-1), @(100), @(300), @(500), @(700), @(1000) ];
    NSArray *sizeMaxIntArray =
    @[ @(-1), @(200), @(400), @(600), @(800), @(2000) ];
    NSMutableArray *spokenLangArray = [[[SystemSettingModel shared] getSpokenLanguageStringList] mutableCopy];
    
    [spokenLangArray insertObject:JMOLocalizedString(@"search_filter__any", nil) atIndex:0];
    self.priceMaxArray = [self convertIntToString:priceMaxIntArray type:@"price"];
    self.priceMinArray = [self convertIntToString:priceMinIntArray type:@"price"];
    self.sizeMinArray = [self convertIntToString:sizeMinIntArray type:@"size"];
    self.sizeMaxArray = [self convertIntToString:sizeMaxIntArray type:@"size"];
    self.spokenLangArray = spokenLangArray;
}

- (NSArray *)convertIntToString:(NSArray *)array type:(NSString *)type {
    NSMutableArray *stringArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < array.count; i++) {
        NSNumber *number = array[i];
        NSString *string = nil;
        double value = [number doubleValue];
        if (value > 0) {
            if ([type isEqual:@"price"]) {
                string = [RealUtility abbreviateNumber:value];
            } else if ([type isEqual:@"size"]) {
                string = [RealUtility getDecimalFormatByString:[number stringValue]
                                                      withUnit:nil];
            }
            
        } else {
            
            string = JMOLocalizedString(@"search_filter__any", nil);
        }
        if (![stringArray containsObject:string]) {
            [stringArray addObject:string];
        }
    }
    return stringArray;
}

- (NSDictionary *)getFilterItemDict:(NSString *)filterItem {
    NSDictionary *filterDict = nil;
    if ([filterItem isEqualToString:@"space"]) {
        NSMutableArray *selectedIndexArray =[[NSMutableArray alloc]init];
        NSArray *selectedArray = [SystemSettingModel shared].filterDict[@"space"];
        if (!selectedArray) {
            selectedArray = @[];
        }
        NSArray *typeArray = [[SystemSettingModel shared] getSpaceFilterArray];
        if (!typeArray) {
            typeArray = @[];
        }
        for (NSDictionary *selectedDict in selectedArray) {
            int selectedSpaceType = [selectedDict[@"spaceType"]intValue];
            int selectedPropertyType =[selectedDict[@"propertyType"]intValue];
            for (NSDictionary *typeDict in typeArray) {
                int spaceType=  [typeDict[@"spaceType"] intValue];
                int propertyType = [typeDict[@"propertyType"]intValue];
                int index = 0;
                if (propertyType == selectedPropertyType && spaceType == selectedSpaceType) {
                    if ([typeArray indexOfObject:typeDict] != NSNotFound) {
                        index = (int)[typeArray indexOfObject:typeDict];
                        if(index != NSNotFound && ![selectedIndexArray containsObject:@(index)]){
                            [selectedIndexArray addObject:@(index)];
                        }
                    }
                }
            }
        }
        filterDict = @{
                       @"typeArray" : typeArray,
                       @"selectedArray" : selectedIndexArray
                       };
    } else if ([filterItem isEqualToString:@"price"]) {
        NSDictionary *priceFilterDict = [self getFilterByFilterType:filterItem];
        NSString *min = [priceFilterDict[@"state"] stringValue];
        NSString *max = priceFilterDict[@"value"];
        NSString *priceUnit = [[SystemSettingModel shared] getUserCurrencyUnit];
        if (priceFilterDict[@"unitString"]) {
            priceUnit = priceFilterDict[@"unitString"];
        }
        filterDict = [self createFilterDictWithType:FilterTypePrice
                                              image:@"ico_pop_content_price"
                                              title:@"Price"
                                         filterUnit:priceUnit
                                                min:min
                                                max:max];
    } else if ([filterItem isEqualToString:@"size"]) {
        NSDictionary *sizeFilterDict = [self getFilterByFilterType:filterItem];
        NSString *min = [sizeFilterDict[@"state"] stringValue];
        NSString *max = sizeFilterDict[@"value"];
        NSString *unit = @"";
        MetricList *metric = [[SystemSettingModel shared].metricaccordingtolanguage objectAtIndex:0];
        if ([RealUtility isValid:[SystemSettingModel shared].SystemSettings.userSelectMetric]) {
            metric = [SystemSettingModel shared].SystemSettings.userSelectMetric;
        }
        unit = metric.NativeName;
        if (sizeFilterDict[@"unitString"]) {
            unit = sizeFilterDict[@"unitString"];
        }
        filterDict = [self createFilterDictWithType:FilterTypeSize
                                              image:@"ico_pop_content_ft"
                                              title:@"Size"
                                         filterUnit:unit
                                                min:min
                                                max:max];
    } else if ([filterItem isEqualToString:@"spokenLanguage"]) {
        NSString *lang = [self getFilterByFilterType:filterItem];
        filterDict = [self createFilterDictWithType:FilterTypeSpokenLanguage
                                              image:@"ico_pop_content_lang"
                                              title:@"Spoken Language"
                                         filterUnit:nil
                                                min:lang
                                                max:lang];
    } else if ([filterItem isEqualToString:@"bathroom"]) {
        NSString *bathroom = [self getFilterByFilterType:filterItem];
        filterDict = [self createFilterDictWithType:FilterTypeBathroom
                                              image:@"ico_pop_content_bathroom"
                                              title:@"Bathroom"
                                         filterUnit:nil
                                                min:bathroom
                                                max:bathroom];
    } else if ([filterItem isEqualToString:@"bedroom"]) {
        NSString *bedroom = [self getFilterByFilterType:filterItem];
        filterDict = [self createFilterDictWithType:FilterTypeBedroom
                                              image:@"ico_pop_content_bedroom"
                                              title:@"Bedroom"
                                         filterUnit:nil
                                                min:bedroom
                                                max:bedroom];
    }
    
    return filterDict;
}

- (NSDictionary *)createFilterDictWithType:(NSString *)type
                                     image:(NSString *)image
                                     title:(NSString *)title
                                filterUnit:(NSString *)unit
                                       min:(NSString *)min
                                       max:(NSString *)max {
    
    if (!unit) {
        unit = @"";
    }
    
    if (!title) {
        title = @"";
    }
    
    if (!image) {
        image = @"";
    }
    
    if (!type) {
        type = @"";
    }
    if (!min) {
        if ([type isEqualToString:FilterTypePrice]||[type isEqualToString:FilterTypeSize]) {
            min = @"0";
        }else{
            
            min = JMOLocalizedString(@"search_filter__any", nil);
        }
    }
    if (!max) {
        if ([type isEqualToString:FilterTypePrice]||[type isEqualToString:FilterTypeSize]) {
            max = @"0";
        }else{
            
            max = JMOLocalizedString(@"search_filter__any", nil);
        }
    }
    
    return @{
             @"filterType" : type,
             @"filterImage" : image,
             @"filterTitle" : title,
             @"filterUnit" : unit,
             @"minValue" : min,
             @"maxValue" : max
             };
}

- (void)createFilter:(NSString *)key value:(id)value {
    if (![SystemSettingModel shared].filterDict) {
        [SystemSettingModel shared].filterDict = [[NSMutableDictionary alloc] init];
    }
    if ([RealUtility isValid:key] && [RealUtility isValid:value]) {
        [[SystemSettingModel shared].filterDict setObject:value forKey:key];
    }
}

- (id)getFilterByFilterType:(NSString *)filterType {
    return [SystemSettingModel shared].filterDict[filterType];
}

- (void)filterStepValueDidChange:(NSInteger)value type:(NSString *)type {
    NSString *valueString = [NSString stringWithFormat:@"%d", (int)value];
    NSString *key;
    if ([type isEqual:FilterTypeBathroom]) {
        key = @"bathroom";
    } else if ([type isEqual:FilterTypeBedroom]) {
        key = @"bedroom";
    }
    [self createFilter:key value:valueString];
}

-(void)filterTextInputDidChange:(NSString*)value type:(NSString*)type unit:(NSString*)unit state:(MutipleState)state{
    double doubleValue = [[RealUtility removeDecimalFormat:value] doubleValue];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    NSString *unitValue = unit;
    double min = 0;
    double max = 0;
    double typeMAX = 0;
    NSString *key = @"";
    if ([type isEqualToString:FilterTypePrice]) {
        key =@"price";
        typeMAX = decimalMax;
    } else if ([type isEqualToString:FilterTypeSize]) {
        key = @"size";
        typeMAX = INT32_MAX;
        int unitIndex = 0;
        for (MetricList *metric in [SystemSettingModel shared].metricaccordingtolanguage) {
            if ([metric.NativeName isEqualToString:unit]) {
                unitIndex = metric.Index;
                break;
            }
        }
        unitValue = [NSString stringWithFormat:@"%d",unitIndex];
    }
    
    if (state == MutipleStateAbove) {
        min = doubleValue;
        max = typeMAX;
    }else if(state == MutipleStateAround){
        min = doubleValue*0.8;
        max = doubleValue*1.2;
        if(max >typeMAX){
            max = typeMAX;
        }
    }else if(state == MutipleStateBelow){
        min = 0;
        max = doubleValue;
    }
    
    NSDecimalNumber *minDecimal = [[NSDecimalNumber alloc]initWithDouble:min];
    NSDecimalNumber *maxDecimal = [[NSDecimalNumber alloc]initWithDouble:max];
    NSString *minString = [minDecimal stringValue];
    NSString *maxString = [maxDecimal stringValue];
    [filterDict setObject:minString forKey:@"min"];
    [filterDict setObject:maxString forKey:@"max"];
    [filterDict setObject:value forKey:@"value"];
    [filterDict setObject:unitValue forKey:@"unit"];
    [filterDict setObject:@(state) forKey:@"state"];
    [filterDict setObject:unit forKey:@"unitString"];
    
    [self createFilter:key value:filterDict];
}
- (void)filterTypeDidChange:(NSArray *)selectedArray {
    [self createFilter:@"space" value:selectedArray];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
