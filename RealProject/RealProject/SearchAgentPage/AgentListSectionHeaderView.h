//
//  AgentListSectionHeaderView.h
//  productionreal2
//
//  Created by Alex Hung on 8/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"
#define sectionHeaderProfileImageHeight 58 
#define sectionHeaderHeight 53

@protocol AgentListSectionHeaderDelegate <NSObject>

- (void) AgentListSectionHeaderProfileDidPress:(AgentProfile*)selectedProfile;
- (void) sectionHeaderProfileImageDidFetch:(NSIndexPath*)indexPath;
@end

@interface AgentListSectionHeaderView : BaseView
@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic,strong) id<AgentListSectionHeaderDelegate> delegate;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *agentNameBottomSpacingConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *backgroundTopSpacingConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *contentWidthConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *contentHeightConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *profileImageTopPaddingConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *profileImageWidthConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *profileImageHeightConstraint;

@property (nonatomic,strong) IBOutlet UILabel *agentNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *followerCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *followerTitleLabel;
@property (nonatomic,strong) IBOutlet UIImageView *agentProfileImageView;
@property (nonatomic,strong) IBOutlet UIButton *agentProfileButton;
@property (nonatomic,strong) IBOutlet UIView *agentProfilePlaceholderView;


@property (nonatomic,strong) IBOutletCollection(UIView) NSArray *loadingView;
@property (nonatomic,strong) IBOutletCollection(UIView) NSArray *contentView;

@property (nonatomic,strong) AgentProfile *headerProfile;
@property (nonatomic,assign) BOOL didPlayBouncing;

-(id)initFromXib:(CGRect)frame;

-(void)configureWithAgentProfile:(AgentProfile*)profile;
-(void)adjustWithOffsetY:(CGFloat)offsetY bounceEffect:(CGFloat)bounceScale;
-(void)adjustHeaderOffset:(CGFloat)offsetY;
-(void)reset;
- (void)showLoadingPlaceholder:(BOOL)show;
- (void)cancelImageOperation;
-(IBAction)profileButtonDidPress:(id)sender;

@end
