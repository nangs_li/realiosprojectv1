//
//  AgentPastClosingItemView.h
//  productionreal2
//
//  Created by Alex Hung on 10/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentPastClosing.h"
#import "RealUtility.h"

@interface AgentPastClosingItemView : UIView
@property (nonatomic,assign) ItemViewStyle style;
@property (nonatomic,strong) IBOutlet UILabel *typeLabel;
@property (nonatomic,strong) IBOutlet UILabel *priceLabel;
@property (nonatomic,strong) IBOutlet UILabel *dateLabel;
@property (nonatomic,strong) IBOutlet UILabel *addressLabel;
@property (nonatomic,strong) IBOutlet UIImageView *dividerView;
@property (nonatomic,assign) CGSize preferredSize;

-(id)initWithAgentPastClosing:(AgentPastClosing*)pastClosing size:(CGSize)size withStyle:(ItemViewStyle)style;
-(void)configureView:(AgentPastClosing*)pastClosing;
+(CGSize)calViewSizeWithAgentPastClosing:(AgentPastClosing *)pastClosing withInitSize:(CGSize)size withStyle:(ItemViewStyle)style;
@end
