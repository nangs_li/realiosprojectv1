//
//  AgentProfileCell.h
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentDetailProfile.h"
@interface AgentProfileCell : UICollectionViewCell
@property (nonatomic,strong) AgentDetailProfile* detailProfile;
@property (nonatomic,strong) NSMutableDictionary *originalSizeDict;
@property (nonatomic,strong) NSMutableDictionary *originalFontDict;

-(void)configureCell:(AgentProfile *)profile;
-(void)adjustAgentProfileInfoByContentOffset:(CGPoint)contentOffset withAlphaEffect:(BOOL)needAlphaEffect;
@end
