//
//  AgentProfileCell.m
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentProfileCell.h"
#import "ApartmentDetailViewController.h"
#import "UIView+Utility.h"
#import "RealUtility.h"

@implementation AgentProfileCell

- (void)awakeFromNib {
    // Initialization code
    if (!self.detailProfile) {
        CGRect profileFrame = [RealUtility screenBounds];
        profileFrame.size.height = 426.0f;
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"AgentDetailProfile" owner:self options:nil];
        self.detailProfile = [subviewArray objectAtIndex:0];
        self.detailProfile.profileType = AgentProfileDetailWithoutSlogan;
        self.detailProfile.frame=self.frame;
        self.detailProfile.backgroundImageView.autoresizingMask = 0;
        self.detailProfile.backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
        self.detailProfile.backgroundImageView.contentMode = UIViewContentModeTop;
        [self.detailProfile configureProfile:nil];
        
        [self saveOriginalFrames];
        [self saveOriginalFonts];
        self.detailProfile.contentView.frame =profileFrame;
        [self addSubview:self.detailProfile];
    }
}

-(void) saveOriginalFrames{
    [self saveOriginalViewFrame:self.detailProfile.infoView key:@"infoView"];
    [self saveOriginalViewFrame:self.detailProfile.agentProfileImageView key:@"agentProfileImageView"];
    [self saveOriginalViewFrame:self.detailProfile.agentNameLabel key:@"agentNameLabel"];
    [self saveOriginalViewFrame:self.detailProfile.followerView key:@"followerView"];
    [self saveOriginalViewFrame:self.detailProfile.followCountLabel key:@"followCountLabel"];
    [self saveOriginalViewFrame:self.detailProfile.followTitleLabel key:@"followTitleLabel"];
    [self saveOriginalViewFrame:self.detailProfile.sloganView key:@"sloganView"];
}

-(void)saveOriginalFonts{
    [self saveOriginalFont:self.detailProfile.agentNameLabel.font key:@"agentNameLabel"];
}

-(void)saveOriginalViewFrame:(UIView*)view key:(NSString*)key{
    if (!view || key) {
        return;
    }
    if (!self.originalSizeDict) {
        self.originalSizeDict = [[NSMutableDictionary alloc]init];
    }
    [self.originalSizeDict setObject:NSStringFromCGRect(view.frame) forKey:key];
}

-(CGRect)getOriginalFrame:(NSString*)key{
    CGRect frame = CGRectZero;
    NSString* frameString = self.originalSizeDict[key];
    
    if ([RealUtility isValid:frameString]) {
        frame = CGRectFromString(frameString);
    }
    
    return frame;
}

-(void)saveOriginalFont:(UIFont*)font key:(NSString*)key{
    if (!font || key) {
        return;
    }
    if (!self.originalFontDict) {
        self.originalFontDict = [[NSMutableDictionary alloc]init];
    }
    [self.originalFontDict setObject:font forKey:key];
}

-(UIFont *)getOriginalFont:(NSString*)key{
    UIFont *font = self.originalFontDict[key];
    if (![RealUtility isValid:font]) {
        font =[UIFont systemFontOfSize:[UIFont systemFontSize]];
    }
    return font;
}

-(void)configureCell:(AgentProfile *)profile{
    [self.detailProfile configureProfile:profile];
}

-(void)adjustAgentProfileInfoByContentOffset:(CGPoint)contentOffset withAlphaEffect:(BOOL)needAlphaEffect{
    [self.detailProfile adjustAgentProfileInfoByContentOffset:contentOffset withAlphaEffect:needAlphaEffect];
}

-(CGFloat)getScaledValueWithOriginal:(CGFloat)original scale:(CGFloat)scale percent:(CGFloat)percent{
    int roundMaxScaledValue = roundf(original*scale);
    CGFloat scaledValue = roundMaxScaledValue *percent;
    return original - scaledValue;
}
@end
