//
//  AgentProfileViewController.h
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentActionProfileCell.h"
@interface AgentProfileViewController : BaseViewController <UICollectionViewDataSource,UICollectionViewDelegate,AgentActionProfileCellDelegate>
@property (nonatomic,strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic,strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UIButton *optionButton;
@property(strong, nonatomic) AgentProfile *pendingAgentProfile;
-(void) setupWithAgentProfile:(AgentProfile*)profile;
-(IBAction)shareButtonDidPress:(id)sender;
-(void) setupNavBar;
@end
