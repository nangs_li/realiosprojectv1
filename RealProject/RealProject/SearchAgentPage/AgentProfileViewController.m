//
//  AgentProfileViewController.m
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "AgentProfileViewController.h"
#import "UIImage+Utility.h"
#import "UIView+Utility.h"
#import "RealUtility.h"
#import "AgentDetailCell.h"
#import "PressChatButtonModel.h"
#import "CSStickyHeaderFlowLayout.h"
#import "Chatroom.h"
#import "FollowAgentModel.h"
#import "UnFollowAgentModel.h"
#import "UIActionSheet+Blocks.h"
#import "FXBlurView.h"
#import "RealApiClient.h"


// Mixpanel
#import "Mixpanel.h"

#define expandOffsetY 44.0f
@interface AgentProfileViewController () <AgentDetailCardViewDelegate>
@property(nonatomic, strong) AgentActionProfileCell *profileCell;
@property (nonatomic,assign) BOOL DidGoChatRoom;
@property (nonatomic,assign) CGSize minSize;
@end

@implementation AgentProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //  [self setupWithAgentProfile:self.AgentProfile];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupNavBar];
    [self setupWithAgentProfile:self.pendingAgentProfile];
    if ([RealUtility isValid:_pendingAgentProfile]) {
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Viewed Agent Profile"
             properties:@{
                          @"Agent member ID": [NSNumber numberWithInt:_pendingAgentProfile.MemberID],
                          @"Agent name": _pendingAgentProfile.MemberName
                          }];
        
        [[RealApiClient sharedClient]addViewdListing:@(_pendingAgentProfile.MemberID)];
    }
}

-(void)setupNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarDetail animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        self.navBarContentView = navBarController.agentProfileDetailNavBar;
        if (self.DidGoChatRoom) {
            self.navBarContentView.alpha = 1.0;
            [[self.delegate getTabBarController]setPageControlHidden:NO animated:NO];
        }
        self.titleLabel = navBarController.agentProfileDetailTitleLabel ;
        navBarController.agentProfileDetailTitleLabel.text=JMOLocalizedString(@"common__my_profile", nil);
      //  [self.optionButton setHidden:YES];
        self.optionButton = navBarController.agentProfileDetailMoreButton;
        [self.optionButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [self.optionButton addTarget:self action:@selector(showOptionActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupWithAgentProfile:(AgentProfile *)profile {
    self.pendingAgentProfile =profile;
    self.titleLabel.text = JMOLocalizedString(@"agent_profile__profile", nil);
    //    if (![self.AgentProfile isEqualToProfile:self.pendingAgentProfile]) {
    self.AgentProfile = profile;
    Photos *photos = [self.AgentProfile.AgentListing.Photos firstObject];
    NSString *profileImageURL =  photos.URL;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"AgentDetailCell"
                                                    bundle:nil]
          forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"AgentActionProfileCell"
                                                    bundle:nil]
          forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:@"header"];
    
    CSStickyHeaderFlowLayout *layout =
    (id)self.collectionView.collectionViewLayout;
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        
        CGFloat screenHeight = [RealUtility screenBounds].size.height;
        CGFloat scaleFactor = 0.5;
        
        if (screenHeight >= 667.0f) {
            
            //iphone 6 or above
            scaleFactor = 0.5;
            
        } else if (screenHeight >= 568.0f) {
            
            //iphone 5
            scaleFactor = 0.6;
            
        } else {
            
            //iphone 4
            scaleFactor = 0.7;
        }
        
        layout.parallaxHeaderReferenceSize =
        CGSizeMake(self.view.frame.size.width,
                   self.collectionView.frame.size.height * scaleFactor);
        layout.parallaxHeaderMinimumReferenceSize =
        CGSizeMake(self.view.frame.size.width, profileActionHeaderMinHeight);
        layout.parallaxHeaderAlwaysOnTop = YES;
        CGSize cardSize = [AgentDetailCardView calculateSizeWithAgentProfile:self.AgentProfile];
        CGFloat collectionPreferHeight = [RealUtility screenBounds].size.height -64;
        if (cardSize.height + layout.parallaxHeaderReferenceSize.height < collectionPreferHeight+ 8) {
            cardSize.height =collectionPreferHeight -layout.parallaxHeaderReferenceSize.height+ 8;
            self.minSize = cardSize;
        }
        layout.itemSize = cardSize;
        
        //        }
        __weak typeof(self) weakSelf = self;
        [self.backgroundImageView loadImageURL:[NSURL URLWithString:profileImageURL] withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                UIImage *blurImage = [[UIImage imageWithView:self.backgroundImageView]blurredImageWithRadius:MaxImageBlurRadius iterations:5 tintColor:[UIColor clearColor]];
                //                CGPoint offset = CGPointMake(0, blurImage.size.height*0.75);
                //                blurImage =[blurImage add_imageAddingImage:[UIImage imageNamed:@"gradient_mask"] offset:offset];
                UIImage *layerImage= [UIImage add_imageWithColor:[UIColor colorWithWhite:0 alpha:0.1] size:blurImage.size];
                blurImage = [blurImage add_imageAddingImage:layerImage];
                weakSelf.backgroundImageView.image = blurImage;
                weakSelf.profileCell.detailProfile.backgroundImageView.frame =
                weakSelf.backgroundImageView.bounds;
                weakSelf.profileCell.detailProfile.backgroundImageView.image =
                weakSelf.backgroundImageView.image;
                if (cacheType == SDImageCacheTypeNone) {
                    weakSelf.backgroundImageView.alpha = 0.0;
                    [UIView animateWithDuration:1.0
                                     animations:^{
                                         weakSelf.backgroundImageView.alpha = 1.0;
                                     }];
                }
            }
        }];
        
        [self.collectionView setContentOffset:CGPointZero animated:NO];
        [self.profileCell adjustAgentProfileInfoByContentOffset:CGPointZero
                                                withAlphaEffect:YES];
        
    }
    [self.collectionView reloadData];
}

#pragma mark -
#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    if ([RealUtility isValid:self.AgentProfile]) {
        return 1;
    } else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AgentDetailCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"cell"
                                                                     forIndexPath:indexPath];
    
    [cell configureCell:self.AgentProfile withMinSize:self.minSize];
    cell.cardView.delegate = self;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        //        CSSectionHeader *cell = [collectionView
        //        dequeueReusableSupplementaryViewOfKind:kind
        //                                                                   withReuseIdentifier:@"sectionHeader"
        //                                                                          forIndexPath:indexPath];
        //
        return nil;
        
    } else if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        self.profileCell =
        [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                           withReuseIdentifier:@"header"
                                                  forIndexPath:indexPath];
        self.profileCell.detailProfile.loadCustomBackground = YES;
        self.profileCell.delegate = self;
        [self.profileCell.detailProfile changePageIndex:0];
        self.profileCell.backgroundView = nil;
        [self.profileCell configureCell:self.AgentProfile];
        //        self.profileCell.detailProfile.backgroundImageView.hidden = YES;
        //        self.profileCell.detailProfile.backgroundImageView.layer.transform
        //        = self.backgroundImageView.layer.transform;
        return self.profileCell;
    }
    return nil;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        BOOL needExpandImageTableView = false;
        CGPoint scrollContentOffset = scrollView.contentOffset;
        if (scrollContentOffset.y >= profileActionHeaderMinHeight) {
            scrollContentOffset.y = profileActionHeaderMinHeight;
        } else if (scrollContentOffset.y <= -expandOffsetY) {
            scrollContentOffset.y = -expandOffsetY;
            needExpandImageTableView = true;
        }
        BOOL enoughSpace = [self enoughSpaceForAlphaEffect];
        [self.profileCell adjustAgentProfileInfoByContentOffset:scrollContentOffset
                                                withAlphaEffect:enoughSpace];
    }
}

- (BOOL)enoughSpaceForAlphaEffect {
    CSStickyHeaderFlowLayout *layout =
    (id)self.collectionView.collectionViewLayout;
    //    layout.parallaxHeaderReferenceSize =
    //    CGSizeMake(self.view.frame.size.width,
    //    self.collectionView.frame.size.height -200);
    //    layout.parallaxHeaderMinimumReferenceSize =
    //    CGSizeMake(self.view.frame.size.width, profileHeaderMinHeight);
    
    return layout.itemSize.height +
    layout.parallaxHeaderMinimumReferenceSize.height >=
    self.collectionView.frame.size.height;
}

- (void)profileChatButtonDidPress:(UIButton *)button {
    if ([[LoginBySocialNetworkModel shared] qbAccountIsReady]) {
        BOOL notSelf = self.AgentProfile.MemberID != [MyAgentProfileModel shared].myAgentProfile.MemberID;
        if(self.AgentProfile.MemberID==0)
            return;
        if (notSelf) {
            // Mixpanel
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Started Chat with an Agent"
                 properties:@{
                              @"Agent member ID":@(self.AgentProfile.MemberID),
                              @"Agent name":self.AgentProfile.MemberName
                              }];
            
            if ([self.delegate networkConnection]) {
                [self showLoadingHUD];
            }
            button.userInteractionEnabled=NO;
            [[PressChatButtonModel shared]
             pressChatButtonModelByQBID:self.AgentProfile.QBID
             MemberName:self.AgentProfile.MemberName
             MemberID: [NSString stringWithFormat:@"%d",self.AgentProfile.MemberID]
             success:^(Chatroom *chatroom) {
                 self.DidGoChatRoom = YES;
                 [self hideLoadingHUD];
                 [self.navigationController pushViewController:chatroom animated:YES];
                 button.userInteractionEnabled=YES;
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error,
                       NSString *errorMessage, RequestErrorStatus errorStatus,
                       NSString *alert, NSString *ok) {
                 [self hideLoadingHUD];
                 [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                               errorMessage:errorMessage
                                                                errorStatus:errorStatus
                                                                      alert:alert
                                                                         ok:ok];
                  button.userInteractionEnabled=YES;
             }];
        }
    }
}

- (void)profileFollowButtonDidPress:(UIButton *)button
                              label:(UILabel *)countLabel {
    if (button.selected) {
        UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil
                                                        delegate:nil
                                               cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                             
                                          destructiveButtonTitle:JMOLocalizedString(@"agent_profile__unfollow_button", nil)
                                               otherButtonTitles:nil, nil];
        
        as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        
        as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                if (self.AgentProfile.MemberID==0) {
                    return ;
                }
                //                    [self loadingAndStopLoadingAfterSecond:10];
                [self callServerUnFollowApi:button label:countLabel];
                
            }
        };
        
        [as showInView:self.view];
    } else {
        if (self.AgentProfile.MemberID==0) {
            return ;
        }

        
        [self callServerFollowApi:button label:countLabel];
    }
}

- (void)callServerFollowApi:(UIButton *)followButton label:(UILabel *)label {
    [[FollowAgentModel shared]
     callFollowAgentApiByAgentProfileMemberID:
     [NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
     AgentProfileAgentListingAgentListingID:
     [NSString stringWithFormat:@"%d", self.AgentProfile.AgentListing
      .AgentListingID]
     success:^(id responseObject) {
         [[NSNotificationCenter defaultCenter]
          postNotificationName:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
          object:nil];
         [self.delegate.currentAgentProfile createSpotLightSearchNewFeedItem];
         int const MYFollowerCount =
         self.delegate.currentAgentProfile.FollowerCount;
         followButton.selected = YES;
         self.delegate.currentAgentProfile.FollowerCount = MYFollowerCount + 1;
         NSString *followCount =
         [NSString stringWithFormat:@"%d", self.delegate.currentAgentProfile
          .FollowerCount];
         label.text =
         [RealUtility getDecimalFormatByString:followCount withUnit:nil];
         CGRect labelFrame = label.frame;
         labelFrame.size =
         [label sizeThatFits:CGSizeMake(label.superview.frame.size.width,
                                        label.frame.size.height)];
         [label setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter
                                    padding:0];
         //         [self hideLoadingHUD];
         [self.collectionView reloadData];
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,
               NSString *alert, NSString *ok) {
         //         [self hideLoadingHUD];
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                       errorMessage:errorMessage
                                                        errorStatus:errorStatus
                                                              alert:alert
                                                                 ok:ok];
         
     }];
}
- (void)callServerUnFollowApi:(UIButton *)followButton label:(UILabel *)label {
    //    if (![self.delegate networkConnection]) {
    //        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
    //    }
    [[UnFollowAgentModel shared]
     callUnFollowAgentApiByAgentProfileMemberID:
     [NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
     success:^(id responseObject) {
         [[NSNotificationCenter defaultCenter]
          postNotificationName:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
          object:nil];
         [self.delegate.currentAgentProfile deleteSpotLightSearchNewFeedItem];
         int const MYFollowerCount =
         self.delegate.currentAgentProfile.FollowerCount;
         followButton.selected = NO;
         
         
         self.delegate.currentAgentProfile.FollowerCount = MYFollowerCount - 1;
         NSString *followCount =
         [NSString stringWithFormat:@"%d", self.delegate.currentAgentProfile
          .FollowerCount];
         label.text =
         [RealUtility getDecimalFormatByString:followCount withUnit:nil];
         CGRect labelFrame = label.frame;
         labelFrame.size =
         [label sizeThatFits:CGSizeMake(label.superview.frame.size.width,
                                        label.frame.size.height)];
         [label setViewAlignmentInSuperView:ViewAlignmentHorizontalCenter
                                    padding:0];
         [self hideLoadingHUD];
         [self.collectionView reloadData];
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,
               NSString *alert, NSString *ok) {
         [self hideLoadingHUD];
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                       errorMessage:errorMessage
                                                        errorStatus:errorStatus
                                                              alert:alert
                                                                 ok:ok];
         
         
     }];
}
- (void)blankAskButtonDidPress:(AgentProfile*)profile{
    [self profileChatButtonDidPress:nil];
}
- (IBAction)shareButtonDidPress:(id)sender {
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
