//
//  ApartmentDetailCardView.h
//  productionreal2
//
//  Created by Alex Hung on 25/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentListing.h"
enum {
    ApartmentQuickView = 0,
    ApartmentDetailView
};
typedef NSUInteger ApartmentCardViewType;

@interface ApartmentDetailCardView : UIView


@property (weak,nonatomic) IBOutlet NSLayoutConstraint *infoViewTopPaddingConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceButtonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *image1HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *image2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *image3HeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p1HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p3HeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p1TopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p2TopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p3TopSpaceConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backgroundHeightConstraint;


@property (nonatomic,strong) IBOutletCollection(UIView) NSArray *dividers;

@property (nonatomic,strong) IBOutlet UIButton *priceButton;
@property (nonatomic,strong) IBOutlet UIButton *sizeButton;
@property (nonatomic,strong) IBOutlet UIButton *bedButton;
@property (nonatomic,strong) IBOutlet UIButton *bathroomButton;
@property (nonatomic,strong) IBOutlet UIButton *quickViewPriceButton;

@property (nonatomic,strong) IBOutlet UILabel *potentialLabel1;
@property (nonatomic,strong) IBOutlet UILabel *potentialLabel2;
@property (nonatomic,strong) IBOutlet UILabel *potentialLabel3;
@property (nonatomic,strong) IBOutlet UILabel *sloganLabel;

@property (nonatomic,strong) IBOutlet UIImageView *potentialImage1;
@property (nonatomic,strong) IBOutlet UIImageView *potentialImage2;
@property (nonatomic,strong) IBOutlet UIImageView *potentialImage3;


@property (nonatomic,strong) IBOutlet UILabel *apartmentTypeLabel;
@property (nonatomic,strong) IBOutlet UILabel *apartmentNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *apartmentAddressLabel;


@property (nonatomic,strong) IBOutlet UIView *contentView;
@property (nonatomic,strong) IBOutlet UIView *apartmentInfoView;
@property (nonatomic,strong) IBOutlet UIView *apartmentDescView;
@property (nonatomic,strong) IBOutlet UIView *apartmentLoadingView;

@property (nonatomic,strong) IBOutlet UIButton *potentialIndicator1;
@property (nonatomic,strong) IBOutlet UIButton *potentialIndicator2;
@property (nonatomic,strong) IBOutlet UIButton *potentialIndicator3;


@property (nonatomic,strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic,strong) IBOutlet UIImageView *apartmentImageView;
@property (nonatomic,strong) IBOutlet UIImageView *bottomGapImageView;
@property (nonatomic,assign) ApartmentCardViewType cardViewType;
@property (nonatomic,strong) AgentListing *agentListing;
@property (nonatomic,assign) CGFloat originalBackgroundImageViewHeight;
@property (nonatomic,assign) CGFloat potentialImageWidth;
@property (nonatomic,assign) CGFloat potentialImageHeight;



-(id)initWithType:(ApartmentCardViewType)type withAgentListing:(AgentListing*)agent;
-(void)configureWithAgentListing:(AgentListing*)listing;
+(CGSize)calculateSizeWithAgentListing:(AgentListing*)listing;
-(void)adjustQuickViewInfoOffset:(CGFloat)offset;
-(void)prefetchImage:(AgentListing*)listing;
- (void)showLoadingPlaceholder:(BOOL)show;
- (void) cancelImageOperation;
@end
