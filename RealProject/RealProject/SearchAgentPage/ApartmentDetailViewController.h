//
//  ApartmentDetailViewController.h
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "BaseViewController.h"
#import "SwipeView.h"
@interface ApartmentDetailViewController : BaseViewController <SwipeViewDataSource,SwipeViewDelegate>
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *previewActionViewHeightConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *tableViewTopSpacingConstraint;
@property (nonatomic,assign) IBOutlet NSLayoutConstraint *backgroundHeightConstraint;
@property (nonatomic,strong) IBOutletCollection(UIView) NSArray *previewViews;
@property (nonatomic,strong) IBOutlet UIView *contentView;
@property (nonatomic,strong) IBOutlet UIView *topBar;
@property (nonatomic,strong) IBOutlet UIButton *backButton;
@property (nonatomic,strong) IBOutlet UIButton *centerButton;
@property (nonatomic,strong) IBOutlet UIButton *dismissButton;
@property (nonatomic,strong) AgentProfile *pendingAgentProfile;
@property (nonatomic,strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic,strong) IBOutlet UIImageView *gradientImageView;
@property (nonatomic,strong) IBOutlet UIImageView *secondCellImageView;
@property (nonatomic,strong) IBOutlet UIImageView *thirdCellImageView;
@property (nonatomic,strong) IBOutlet UIImageView *swipeImageView;
@property (nonatomic,strong) IBOutlet UIImageView *previewDividerView;
@property (nonatomic,strong) IBOutlet UIView *previewActionView;
@property (nonatomic,strong) IBOutlet UIView *previewTopBar;
@property (nonatomic,strong) IBOutlet UIButton *previewAddLanguageButton;
@property (nonatomic,strong) IBOutlet UIButton *previewAddPublishButton;
@property (nonatomic,strong) IBOutlet UIButton *previewRemoveLanguageButton;
@property (nonatomic,strong) IBOutlet UILabel *previewAddHintLabel;
@property (nonatomic,strong) IBOutlet SwipeView *previewLanguageSwipeView;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UIButton *optionButton;
@property (strong, nonatomic) IBOutlet UILabel *preViewTitle;
@property (strong, nonatomic) IBOutlet UIButton *languageBtn;
@property (strong, nonatomic) IBOutlet UIButton *publishBtn;
@property (nonatomic,assign) BOOL pressPublishButton;
@property (nonatomic,assign) BOOL didChangeToImageTableView;

-(id)init;
-(void)setupWithAgentProfile:(AgentProfile*)profile;
-(IBAction)scrollTopButtonDidPress:(id)sender;

-(void)setupNavBar;
-(void)compressImageArrayAndSetupObserve;
@end
