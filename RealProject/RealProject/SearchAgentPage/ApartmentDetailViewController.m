//
//  ApartmentDetailViewController.m
//  productionreal2
//
//  Created by Alex Hung on 26/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "ApartmentDetailViewController.h"
#import "CSStickyHeaderFlowLayout.h"
#import "ApartmentDetailCardView.h"
#import "ApartmentDetailCell.h"
#import "AgentProfileCell.h"
#import "OnePageImageDetailCell.h"
#import "UIImage+Utility.h"
#import "MWPhotoBrowser.h"
#import "MWPhoto+Custom.h"
#import "UIView+Utility.h"
#import "RealUtility.h"
#import "DXAlertView.h"
#import "UIActionSheet+Blocks.h"
#import "ReportProblemModel.h"
#import "BaseNavigationController.h"
#import "Addlanguage.h"
#import "RealApiClient.h"
#import "ViralViewController.h"
#import "RealPrefetcher.h"
#import "RealPhoneBookShareViewController.h"
// Mixpanel
#import "Mixpanel.h"

#define VariableName(arg) (@""#arg)
#define backgroundImageZoomScale 1.1
#define animationTime 0.6
#define maxAnimationImageCount 3

@interface ApartmentDetailViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,MWPhotoBrowserDelegate>
@property (nonatomic,strong) AgentProfileCell *profileCell;
@property (nonatomic,strong) ApartmentDetailCell *apartmentDetailCell;
@property (nonatomic,strong) NSMutableDictionary *frameDict;
@property (nonatomic,assign) BOOL isScrollViewDragFromTop;
@property (nonatomic,strong) NSTimer *scrollTimer;
@property (nonatomic,strong) NSMutableArray *listingImageArray;
@property (nonatomic,strong) NSMutableArray *imageAnimationArray;
@property(strong, nonatomic) MWPhotoBrowser *mwPhotoBrowser;
@property(strong, nonatomic) NSMutableArray *mwPhotosBrowerPhotoArray;
@property (nonatomic,strong) NSMutableArray *previewReasonLangIndexArray;
@property (nonatomic,strong) NSMutableArray *previewReasonLangTitleArray;
@property (nonatomic,assign) NSNumber *selectedLangIndex;
@property (nonatomic,assign) BOOL backToDetail;
@property (nonatomic, assign) BOOL tableViewInitAnimation;
@property (nonatomic, assign) BOOL backFromOtherVC;
@property (nonatomic,strong) CAShapeLayer *previewArrowLayer;
@end

@implementation ApartmentDetailViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        //        [self setup];
    }
    return self;
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSwipeView];
    self.backgroundImageView.frame = [self getOriginalFrame:@"backgroundImageView"];
    //    [self recordViewsOriginalFrame];
    self.backgroundHeightConstraint.constant = [RealUtility screenBounds].size.width;
    [self setupCollectionView];
    if (!self.pendingAgentProfile.isPreview) {
        self.previewActionView.hidden = YES;
        self.backButton.hidden = YES;
        self.optionButton.hidden = NO;
    }else{
        self.previewTopBar.backgroundColor = RealNavBlueColor;
        self.previewActionView.hidden = NO;
        self.backButton.hidden = NO;
        self.optionButton.hidden = YES;
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self recordViewsOriginalFrame];
    if (self.pendingAgentProfile.isPreview) {
        [self setupCollectionView];
        [self setupWithAgentProfile:self.pendingAgentProfile];
    }
    
    
    
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    
    
    self.preViewTitle.text=JMOLocalizedString(@"ready_to_publish__title", nil);
    [self.publishBtn setTitle:JMOLocalizedString(@"ready_to_publish__publish", nil) forState:UIControlStateNormal];
    [self.languageBtn setTitle:JMOLocalizedString(@"ready_to_add_languages", nil) forState:UIControlStateNormal];
    
//    self.previewAddHintLabel.text=[NSString stringWithFormat:@"%@\n%@", JMOLocalizedString(@"ready_to_publish__confirm_to_publish", nil), JMOLocalizedString(@"ready_to_publish__or_make_more_language", nil)];
    
}
-(void)setupNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarDetail animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        self.navBarContentView = navBarController.apartmentDetailNavBar;
        if (self.backFromOtherVC) {
            self.navBarContentView.alpha =1.0;
        }
        self.titleLabel = navBarController.apartmentDetailTitleLabel;
        self.optionButton = navBarController.apartmentDetailMoreButton;
      //  [self.optionButton setHidden:YES];
        [self.optionButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [self.optionButton addTarget:self action:@selector(showOptionActionSheet:) forControlEvents:UIControlEventTouchUpInside];
        self.backButton = navBarController.apartmentDetailBackButton;
        [self.backButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [self.backButton addTarget:self action:@selector(backButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

-(void)setupEditView{
    if (self.pendingAgentProfile.AgentListing.Reasons.count >1) {
        self.previewActionViewHeightConstraint.constant = 49;
        self.previewActionView.hidden = NO;
        self.previewDividerView.hidden = YES;
        self.publishBtn.hidden = YES;
        self.languageBtn.hidden = YES;
        if (!self.previewArrowLayer) {
            CGFloat imageWidth = [RealUtility screenBounds].size.width;
            CGFloat imageHeight = self.swipeImageView.frame.size.height;
            CGPoint middlePoint = CGPointMake(imageWidth/2, 0);
            CGSize arrowSize = CGSizeMake(8, 8);
            CGMutablePathRef path = CGPathCreateMutable();
            CGPathMoveToPoint(path,NULL,0.0,0.0);
            CGPathAddLineToPoint(path, NULL, middlePoint.x - arrowSize.width, 0.0f);
            CGPathAddLineToPoint(path, NULL, middlePoint.x, arrowSize.height);
            CGPathAddLineToPoint(path, NULL,  middlePoint.x + arrowSize.width, 0);
            CGPathAddLineToPoint(path, NULL, imageWidth, 0);
            CGPathAddLineToPoint(path, NULL, imageWidth, imageHeight);
            CGPathAddLineToPoint(path, NULL, 0.0f, imageHeight);
            CGPathAddLineToPoint(path, NULL, 0.0f, 0.0f);
            
            
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            [shapeLayer setPath:path];
            [shapeLayer setFillColor:RealNavBlueColor.CGColor];
            [shapeLayer setBounds:self.swipeImageView.bounds];
            [shapeLayer setAnchorPoint:CGPointMake(0.0f, 0.0f)];
            [shapeLayer setPosition:CGPointMake(0.0f, 0.0f)];
            [self.swipeImageView.layer addSublayer:shapeLayer];
            
            CGPathRelease(path);
        }
        Reasons *firstReason = self.AgentProfile.AgentListing.Reasons.firstObject;
        self.selectedLangIndex = @(firstReason.LanguageIndex);
        self.pendingAgentProfile.AgentListing.selectedLangIndex = self.selectedLangIndex;
        self.previewReasonLangIndexArray = [[NSMutableArray alloc]init];
        for (Reasons *reason in self.pendingAgentProfile.AgentListing.Reasons) {
            [self.previewReasonLangIndexArray addObject:@(reason.LanguageIndex)];
        }
        self.previewReasonLangTitleArray = [[NSMutableArray alloc]init];
        for (int i = 0; i < self.previewReasonLangIndexArray.count; i++) {
            NSNumber *index = self.previewReasonLangIndexArray[i];
            NSString *title = [[SystemSettingModel shared]getSettingLanguageString:index];
            [self.previewReasonLangTitleArray addObject:title];
        }
        self.previewRemoveLanguageButton.hidden = YES;
        [self.previewLanguageSwipeView reloadData];
    }else{
        self.previewActionView.hidden = YES;
    }
}

-(void)setupPreview{
    self.previewActionViewHeightConstraint.constant = 90;
    for (UIView *view in self.previewViews) {
        view.hidden = NO;
    }
    if (!self.previewArrowLayer) {
        CGFloat imageWidth = [RealUtility screenBounds].size.width;
        CGFloat imageHeight = self.swipeImageView.frame.size.height;
        CGPoint middlePoint = CGPointMake(imageWidth/2, 0);
        CGSize arrowSize = CGSizeMake(8, 8);
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path,NULL,0.0,0.0);
        CGPathAddLineToPoint(path, NULL, middlePoint.x - arrowSize.width, 0.0f);
        CGPathAddLineToPoint(path, NULL, middlePoint.x, arrowSize.height);
        CGPathAddLineToPoint(path, NULL,  middlePoint.x + arrowSize.width, 0);
        CGPathAddLineToPoint(path, NULL, imageWidth, 0);
        CGPathAddLineToPoint(path, NULL, imageWidth, imageHeight);
        CGPathAddLineToPoint(path, NULL, 0.0f, imageHeight);
        CGPathAddLineToPoint(path, NULL, 0.0f, 0.0f);
        
        
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        [shapeLayer setPath:path];
        [shapeLayer setFillColor:RealNavBlueColor.CGColor];
        [shapeLayer setBounds:self.swipeImageView.bounds];
        [shapeLayer setAnchorPoint:CGPointMake(0.0f, 0.0f)];
        [shapeLayer setPosition:CGPointMake(0.0f, 0.0f)];
        [self.swipeImageView.layer addSublayer:shapeLayer];
        
        CGPathRelease(path);
    }
    [self.previewAddPublishButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self.previewAddLanguageButton setBackgroundImage:[UIImage imageWithBorder:RealBlueColor size:self.previewAddLanguageButton.frame.size] forState:UIControlStateNormal];
    [self.previewAddLanguageButton setUpAutoScaleButton];
    self.previewAddLanguageButton.titleLabel.numberOfLines =2;
    [self updatePreviewProfile];
    [self.previewLanguageSwipeView reloadData];
}



-(void)updatePreviewProfile{
    self.pendingAgentProfile = [[AgentListingModel shared] convertToPreviewAgentProfile];
    self.pendingAgentProfile.AgentListing.selectedLangIndex = self.selectedLangIndex;
    self.previewReasonLangIndexArray = [AgentListingModel shared].reasonDict.allKeys;
    self.previewReasonLangTitleArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < self.previewReasonLangIndexArray.count; i++) {
        NSNumber *index = self.previewReasonLangIndexArray[i];
        NSString *title = [[SystemSettingModel shared]getSettingLanguageString:index];
        [self.previewReasonLangTitleArray addObject:title];
    }
    
    if (self.previewReasonLangIndexArray.count>1) {
        self.previewRemoveLanguageButton.hidden = NO;
    }else{
        self.previewRemoveLanguageButton.hidden = YES;
    }
}

-(void)setupSwipeView{
    self.previewLanguageSwipeView.pagingEnabled = NO;
    self.previewLanguageSwipeView.alignment = SwipeViewAlignmentCenter;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[RealPrefetcher sharePrefetcher]cancelListingImageTask];
    [self setupNavBar];
    
    if (self.didChangeToImageTableView) {
        return;
    }
    if (!self.pendingAgentProfile.isPreview) {
        if (self.pendingAgentProfile.status == AgentProfileStatusEdit) {
            [self setupEditView];
        }
        
        [self setupCollectionView];
        [self setupWithAgentProfile:self.pendingAgentProfile];
        if ([RealUtility isValid:_pendingAgentProfile]) {
            
            // Mixpanel
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Viewed Posting"
                 properties:@{
                              @"Agent member ID": [NSNumber numberWithInt:_pendingAgentProfile.MemberID],
                              @"Agent name": _pendingAgentProfile.MemberName
                              }];
            
            [[RealApiClient sharedClient]addViewdListing:@(_pendingAgentProfile.MemberID)];
        }
        self.tableViewTopSpacingConstraint.constant = -84;
        [self.collectionView reloadData];
    }else{
        [self setupPreview];
        self.previewTopBar.hidden = NO;
        self.tableViewTopSpacingConstraint = 0;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[RealPrefetcher sharePrefetcher]cancelListingImageTask];
    if(self.pendingAgentProfile.isPreview){
        self.previewTopBar.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (NSArray*)imageArray{
    return @[@"http://www.skyscrapernews.com/images/pics/1662XMarksTheSpot_pic1.jpg",@"http://moorepix.com/wp-content/uploads/2015/08/swimming-pool-best-indoor-swimming-pools-london-with-luxurious-and-elegant-marvellous-best-indoor-pool-with-modern-design-also-minimalist-exterior-adds-to-the-impression-of-a-modern-building.jpg",@"http://rumahinteriorminimalis.com/wp-content/uploads/2014/09/build-indoor-swimming-pool.jpg",@"http://cdn.decoist.com/wp-content/uploads/2013/01/indoor-house-plants-for-the-living-room.jpg",@"http://www.iroonie.com/wp-content/uploads/2011/10/elegance-white-indoor-living-room.jpg"];
}
- (void) recordViewsOriginalFrame{
    //    if (![self.frameDict.allKeys containsObject:@"backgroundImageView"]) {
    //        self.backgroundImageView.transform = CGAffineTransformIdentity;
    //        [self recordFrame:self.backgroundImageView withKey:@"backgroundImageView"];
    //    }
    if (![self.frameDict.allKeys containsObject:@"gradientImageView"]) {
        [self recordFrame:self.gradientImageView withKey:@"gradientImageView"];
    }
}

-(void)recordFrame:(UIView*)view withKey:(NSString*)key{
    if (!self.frameDict) {
        self.frameDict =[[NSMutableDictionary alloc]init];
    }
    if(![self.frameDict.allKeys containsObject:key]){
        [self.frameDict setObject:NSStringFromCGRect(view.frame) forKey:key];
    }
}
- (void)setupCollectionView{
    self.collectionView.backgroundColor =[UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ApartmentDetailCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"AgentProfileCell" bundle:nil]forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:@"header"];
    
    CSStickyHeaderFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        
        
        DDLogDebug(@"screen height:%f",[[UIScreen mainScreen]  bounds ].size.height);
        CGFloat scaleFactor = 0.5;
        if (is568h) {
            scaleFactor = 0.5;
        }else{
            scaleFactor = 0.6   ;
        }
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, self.collectionView.frame.size.height * scaleFactor);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, profileHeaderMinHeight);
        layout.parallaxHeaderAlwaysOnTop = YES;
        CGSize cardSize = [ApartmentDetailCardView calculateSizeWithAgentListing:self.pendingAgentProfile.AgentListing];
        cardSize.width = [RealUtility screenBounds].size.width;
        CGFloat collectionPreferHeight = [RealUtility screenBounds].size.height -64;
        if (cardSize.height + layout.parallaxHeaderReferenceSize.height < collectionPreferHeight+ 8) {
            cardSize.height =collectionPreferHeight -layout.parallaxHeaderReferenceSize.height+ 8;
            
            if (self.pendingAgentProfile.isPreview) {
                cardSize.height =collectionPreferHeight -layout.parallaxHeaderReferenceSize.height+ 16;
                CGFloat inset = collectionPreferHeight - layout.parallaxHeaderReferenceSize.height -cardSize.height;
                inset += self.previewActionView.frame.size.height;
                self.collectionView.contentInset = UIEdgeInsetsMake(0, 0,inset, 0);
            }else{
                cardSize.height =collectionPreferHeight -layout.parallaxHeaderReferenceSize.height+ 8;
            }
        }else if(self.pendingAgentProfile.isPreview){
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, self.previewActionView.frame.size.height, 0);
        }else{
            self.collectionView.contentInset = UIEdgeInsetsZero;
        }
        
        layout.itemSize = cardSize;
    }
    if (self.pendingAgentProfile.isPreview) {
        
    }
}
-(CGRect)getOriginalFrame:(NSString*)key{
    CGRect frame = CGRectZero;
    NSString *rectString = self.frameDict[key];
    
    if ([RealUtility isValid:rectString]) {
        frame = CGRectFromString(rectString);
    }
    
    if([key isEqualToString:@"backgroundImageView"]||[key isEqualToString:@"profileCellBackgroundImageView"]){
        frame = CGRectMake(0, 0, [RealUtility screenBounds].size.width, [RealUtility screenBounds].size.width);
    }
    return frame;
}

-(void)setupWithAgentProfile:(AgentProfile *)profile{
    
    if (!profile.isPreview) {
        [self setupCollectionView];
    }
    if (self.didChangeToImageTableView) {
        self.backButton.hidden = NO;
    }else{
        self.backButton.hidden = !self.AgentProfile.isPreview;
    }
    self.pendingAgentProfile = profile;
    GoogleAddress *googleAddress = [profile.AgentListing.GoogleAddress firstObject];
    NSString *title = googleAddress.Name;
    self.titleLabel.text = title;
    //    if ([self.AgentProfile isEqualToProfile:self.pendingAgentProfile]) {
    //        return;
    //    }
    self.AgentProfile = profile;
    NSString *bgUrlString = @"";
    [[RealPrefetcher sharePrefetcher]prefetchAgentListings:@[self.AgentProfile.AgentListing] allContent:YES clearQueue:YES top:NO];
    self.listingImageArray =[[NSMutableArray alloc]init];
    if ([self.AgentProfile hasPhoto] && !self.AgentProfile.isPreview) {
        self.listingImageArray =[NSMutableArray arrayWithArray:self.AgentProfile.AgentListing.Photos];
        [self.listingImageArray removeObjectAtIndex:0];
    }else if(self.AgentProfile.isPreview && self.AgentProfile.AgentListing.previewPhoto.count >1){
        self.listingImageArray = [NSMutableArray arrayWithArray:self.AgentProfile.AgentListing.previewPhoto];
        [self.listingImageArray removeObjectAtIndex:0];
    }
    if ([self.AgentProfile hasPhoto] && !self.AgentProfile.isPreview) {
        Photos *bgPhoto = [self.listingImageArray firstObject];
        bgUrlString = bgPhoto.URL;
        
        self.mwPhotosBrowerPhotoArray = [[NSMutableArray alloc] init];
        for (int i =0; i<self.listingImageArray.count; i++) {
            Photos *photo = self.listingImageArray[i];
            NSString *photocoverurl = photo.URL;
            NSURL *url = [NSURL
                          URLWithString:[NSString stringWithFormat:@"%@", photocoverurl]];
            [self.mwPhotosBrowerPhotoArray addObject:[MWPhoto photoWithURL:url]];
        }
        
        [self showGhostImage:NO];
        self.imageAnimationArray = [NSMutableArray arrayWithArray:[self genImageAnimationArray]];
    }else{
        self.mwPhotosBrowerPhotoArray = [[NSMutableArray alloc] init];
        if(self.AgentProfile.isPreview){
            for (int i =0; i<self.listingImageArray.count; i++) {
                UIImage *image = self.listingImageArray[i];
                [self.mwPhotosBrowerPhotoArray addObject:[MWPhoto photoWithImage:image]];
            }
        }
    }
    NSURL *bgurl = [NSURL URLWithString:bgUrlString];
    //    bgurl = [NSURL URLWithString:@"https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Square_-_black_simple.svg/1000px-Square_-_black_simple.svg.png"];
    if (!self.didChangeToImageTableView) {
        if (self.AgentProfile.isPreview) {
            UIImage *image = [self.listingImageArray firstObject];
            self.backgroundImageView.image = image;
            //            self.profileCell.contentMode = UIViewContentModeScaleAspectFit;
            //            CGRect rect = self.profileCell.detailProfile.backgroundImageView.frame;
            //            rect.size = [self getOriginalFrame:@"backgroundImageView"].size;
            //            self.profileCell.detailProfile.backgroundImageView.frame = rect;
            //            [self recordFrame:self.profileCell.detailProfile.backgroundImageView withKey:@"profileCellBackgroundImageView"];
            //            self.profileCell.detailProfile.backgroundImageView.hidden = NO;
            //            self.profileCell.detailProfile.backgroundImageView.image = [UIImage imageWithView:self.backgroundImageView];
            //            self.profileCell.detailProfile.backgroundImageView.layer.transform = CATransform3DMakeScale(backgroundImageZoomScale, backgroundImageZoomScale, 1.0);
            self.backgroundImageView.layer.transform = CATransform3DMakeScale(backgroundImageZoomScale, backgroundImageZoomScale, 1.0);
            
        }else{
            __weak typeof(self) weakSelf = self;
            [self.backgroundImageView loadImageURL:bgurl withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image && cacheType == SDImageCacheTypeNone) {
                    weakSelf.backgroundImageView.alpha = 0.0;
                    [UIView animateWithDuration:1.0
                                     animations:^{
                                         weakSelf.backgroundImageView.alpha =
                                         1.0;
                                     }];
                }
                weakSelf.profileCell.contentMode = UIViewContentModeScaleAspectFit;
                CGRect rect = weakSelf.profileCell.detailProfile.backgroundImageView.frame;
                rect.size = [weakSelf getOriginalFrame:@"backgroundImageView"].size;
                //                                                   self.profileCell.detailProfile.backgroundImageView.frame = rect;
                //                                                   [self recordFrame:self.profileCell.detailProfile.backgroundImageView withKey:@"profileCellBackgroundImageView"];
                //                                                   self.profileCell.detailProfile.backgroundImageView.hidden = NO;
                //                                                   self.profileCell.detailProfile.backgroundImageView.image = [UIImage imageWithView:self.backgroundImageView];
                //                                                   self.profileCell.detailProfile.backgroundImageView.layer.transform = CATransform3DMakeScale(backgroundImageZoomScale, backgroundImageZoomScale, 1.0);
                weakSelf.backgroundImageView.layer.transform = CATransform3DMakeScale(backgroundImageZoomScale, backgroundImageZoomScale, 1.0);
            }];
            
        }
    }
    [self.profileCell adjustAgentProfileInfoByContentOffset:CGPointZero withAlphaEffect:YES];
    [self.collectionView setContentOffset:CGPointZero animated:NO];
    [self.collectionView reloadData];
    [self.tableView reloadData];
    
    //            CSStickyHeaderFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    //    if (self.collectionView.contentSize.height < self.collectionView.frame.size.height) {
    //        self.collectionView.contentSize = CGSizeMake(self.collectionView.contentSize.width, self.collectionView.frame.size.height +49);
    //    }
}

#pragma mark-
#pragma mark UITableViewDataSource And delagete
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    DDLogInfo(
              @"[self.delegate.currentAgentProfile.AgentListing.Photos count]-->%lu",
              (unsigned long)[self.listingImageArray count]);
    
    if ([RealUtility isValid:self.AgentProfile] && [self.AgentProfile hasPhoto] && !self.AgentProfile.isPreview) {
        return self.listingImageArray.count;
    }else if(self.AgentProfile.isPreview){
        return self.listingImageArray.count;
    }else{
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set height to 116
    return 218;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"OnepageImageDetailCell";
    
    OnePageImageDetailCell *cell = (OnePageImageDetailCell *)
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        // table view cell setting
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OnePageImageDetailCell"
                                                     owner:self
                                                   options:nil];
        
        cell = (OnePageImageDetailCell *)[nib objectAtIndex:0];
    }
    
    DDLogInfo(@"indexPath.rowindexPath.row-->%ld", (long)indexPath.row);
    //*** Terminating app due to uncaught exception 'NSRangeException'
    //-[__NSArrayM objectAtIndex:]: index 1 beyond bounds [0 .. 0]'
    
    DDLogInfo(@"self.AgentProfile.AgentListing.Photos%@",
              self.listingImageArray);
    if (self.AgentProfile.isPreview) {
        cell.imageview.contentMode = UIViewContentModeScaleAspectFill;
        cell.imageview.clipsToBounds = YES;
        cell.imageview.image = self.listingImageArray[indexPath.row];
    }else{
        Photos *photo = [self.listingImageArray objectAtIndex:indexPath.row];
        
        NSString *photocoverurl = photo.URL;
        
        [cell configureCell:@{@"imageURL":photocoverurl}];
    }
    cell.pageControl.hidden = indexPath.row != 0;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"didSelectRowAtIndexPath%ld", (long)indexPath.row);
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Enlarge Posting Photo"];
    
    int imageCellIndex = 0;
    self.mwPhotoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    self.mwPhotoBrowser.displayActionButton = YES;
    self.mwPhotoBrowser.displayNavArrows = NO;
    self.mwPhotoBrowser.displaySelectionButtons = NO;
    self.mwPhotoBrowser.zoomPhotosToFill = YES;
    self.mwPhotoBrowser.alwaysShowControls = NO;
    self.mwPhotoBrowser.autoPlayOnAppear = NO;
    self.mwPhotoBrowser.enableGrid = YES;
    self.mwPhotoBrowser.startOnGrid = NO;
    [self.mwPhotoBrowser reloadData];
    for (MWPhoto *mwphoto in self.mwPhotosBrowerPhotoArray) {
        OnePageImageDetailCell *cell =
        (OnePageImageDetailCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        if ([cell.urlString isEqualToString:[mwphoto.photoURL absoluteString]]) {
            [self.mwPhotoBrowser setCurrentPhotoIndex:imageCellIndex];
            
            UINavigationController *nc = [[UINavigationController alloc]
                                          initWithRootViewController:self.mwPhotoBrowser];
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            nc.navigationBar.titleTextAttributes =
            @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            self.backFromOtherVC = YES;
            [self presentViewController:nc animated:YES completion:nil];
            
            break;
        }
        
        imageCellIndex++;
    }
}


//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 5;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 135;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"temp"];
//    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
//    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
//    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
//    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
//    cell.contentView.backgroundColor = color;
//    return cell;
//}

#pragma mark-
#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([RealUtility isValid:self.pendingAgentProfile]) {
        return  1;
    }else{
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ApartmentDetailCell *cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    self.apartmentDetailCell = cell;
    [cell configureCell:self.pendingAgentProfile.AgentListing];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return nil;
    } else if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        self.profileCell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                              withReuseIdentifier:@"header"
                                                                     forIndexPath:indexPath];
        self.profileCell.detailProfile.backgroundImageView.hidden = YES;
        self.profileCell.detailProfile.loadCustomBackground = YES;
        [self.profileCell.detailProfile changePageIndex:2];
        self.profileCell.backgroundView =nil;
        [self.profileCell configureCell:self.AgentProfile];
        //        self.profileCell.detailProfile.backgroundImageView.image = [UIImage imageWithView:self.backgroundImageView];
        //        self.profileCell.detailProfile.backgroundImageView.hidden = YES;
        //        self.profileCell.detailProfile.backgroundImageView.layer.transform = self.backgroundImageView.layer.transform;
        return self.profileCell;
    }
    return nil;
}

-(BOOL)enoughSpaceForAlphaEffect{
    CSStickyHeaderFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    //    layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, self.collectionView.frame.size.height -200);
    //    layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, profileHeaderMinHeight);
    
    return layout.itemSize.height + layout.parallaxHeaderMinimumReferenceSize.height >= self.collectionView.frame.size.height;
}

#pragma mark-
#pragma mark ScrollViewDelegate
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView == self.tableView && self.backToDetail) {
        self.backToDetail = NO;
        [self switchToDetailCollectionView];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == self.collectionView && !self.didChangeToImageTableView) {
        BOOL needExpandImageTableView = false;
        CGPoint scrollContentOffset = scrollView.contentOffset;
        if (scrollContentOffset.y >= profileHeaderMinHeight) {
            scrollContentOffset.y = profileHeaderMinHeight;
        }else if (scrollContentOffset.y <= -expandOffsetY){
            scrollContentOffset.y = -expandOffsetY;
            needExpandImageTableView = true;
            
        }
        BOOL enoughSpace = [self enoughSpaceForAlphaEffect];
        [self.profileCell adjustAgentProfileInfoByContentOffset:scrollContentOffset withAlphaEffect:enoughSpace];
        if (scrollContentOffset.y >0) {
            self.apartmentDetailCell.cardView.contentView.backgroundColor =[UIColor colorWithWhite:1 alpha:0.8+ 0.2*(scrollContentOffset.y/profileHeaderMinHeight)];
        }
        CGRect originalGradientFrame = [self getOriginalFrame:@"gradientImageView"];
        if (scrollContentOffset.y <= 0) {
            CGFloat imageScale = -scrollContentOffset.y/expandOffsetY;
            CATransform3D  imageLayerTransform = CATransform3DMakeScale(backgroundImageZoomScale -0.1f*imageScale, backgroundImageZoomScale - 0.1f*imageScale, 1.0);
            self.backgroundImageView.layer.transform = imageLayerTransform;
            //            self.profileCell.detailProfile.backgroundImageView.layer.transform = imageLayerTransform;
            self.gradientImageView.frame = CGRectMake(0.0f, originalGradientFrame.origin.y -scrollContentOffset.y, self.gradientImageView.frame.size.width, self.gradientImageView.frame.size.height);
        }else{
            self.gradientImageView.frame = CGRectMake(0.0f, originalGradientFrame.origin.y, self.gradientImageView.frame.size.width, self.gradientImageView.frame.size.height);
        }
        
        if (needExpandImageTableView && self.isScrollViewDragFromTop) {
            [self switchToImageTableView];
        }
    }else if(scrollView == self.tableView && self.didChangeToImageTableView){
        BOOL needCollaspeImageTableView = false;
        CGPoint scrollContentOffset = scrollView.contentOffset;
        
        CGRect firstCellRect = [self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        firstCellRect = CGRectOffset(firstCellRect, -scrollContentOffset.x, -scrollContentOffset.y);
        if (-firstCellRect.origin.y > firstCellRect.size.height) {
            firstCellRect.origin.y = - firstCellRect.size.height;
        }
        
        [self adjustBackgroundImageViewByFirstCellFrame:firstCellRect];
        if (scrollContentOffset.y <=-expandOffsetY) {
            needCollaspeImageTableView = true;
        }
        
        if (needCollaspeImageTableView && self.isScrollViewDragFromTop) {
            [self switchToDetailCollectionView];
        }
    }
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    if (scrollView.contentOffset.y <5.0f) {
        self.isScrollViewDragFromTop = YES;
    }else{
        self.isScrollViewDragFromTop = NO;
    }
    DDLogDebug(@"%@",NSStringFromCGPoint(*targetContentOffset));
}

#pragma mark-
#pragma mark Switch Method
-(void)switchToDetailCollectionView{
    if (self.didChangeToImageTableView) {
        [self showRightArrowButton:YES];
        [self showPageControl:YES animated:YES];
        self.didChangeToImageTableView = NO;
        self.collectionView.alpha = 0.0f;
        self.collectionView.hidden = NO;
        self.backgroundImageView.alpha= 1.0f;
        self.backgroundImageView.hidden = NO;
        self.gradientImageView.alpha = 0.0f;
        self.gradientImageView.hidden = NO;
        if (self.pendingAgentProfile.status == AgentProfileStatusEdit && self.pendingAgentProfile.AgentListing.Reasons.count >1) {
            self.previewActionView.hidden = NO;
            self.previewActionView.alpha = 0.0f;
        }
        
        CGRect backgroundImageFrame = [self getOriginalFrame:@"backgroundImageView"];
        backgroundImageFrame.origin.y = 0;
        self.tableView.alpha = 1.0f;
        self.tableView.hidden = YES;
        [self initGhostImagePositionWithScale:-1.0 visible:NO];
        CGRect ghostImageFrame = backgroundImageFrame;
        ghostImageFrame.origin.y = self.imageAnimationArray.count * ghostImageFrame.size.height;
        [self showGhostImage:YES];
        CATransform3D transform = CATransform3DMakeScale(backgroundImageZoomScale, backgroundImageZoomScale, 1.0);
        [self showBackButton:NO];
        [UIView animateWithDuration:animationTime delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.tableView.alpha = 0.0f;
            self.backgroundImageView.frame = backgroundImageFrame;
            self.backgroundHeightConstraint.constant = backgroundImageFrame.size.height;
            self.backgroundImageView.layer.transform = transform;
            self.collectionView.alpha = 1.0f;
            self.gradientImageView.alpha = 1.0f;
            [self ghostImageGoToRect:ghostImageFrame transform:transform];
            if (self.pendingAgentProfile.status == AgentProfileStatusEdit && self.pendingAgentProfile.AgentListing.Reasons.count >1) {
                self.previewActionView.alpha = 1;
            }
            //            self.profileCell.detailProfile.backgroundImageView.layer.transform = transform;
            [self hideGhostImageWithAnimation];
        } completion:^(BOOL finished) {
            if (finished) {
                self.tableView.hidden = YES;
                //                self.profileCell.detailProfile.backgroundImageView.hidden = NO;
                [self showGhostImage:NO];
            }
        }];
    }
}

-(void)switchToImageTableView{
    if (!self.didChangeToImageTableView && [self.AgentProfile hasPhoto]) {
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"View Posting Photos"];
        
        [self showRightArrowButton:NO];
        [self showPageControl:NO animated:YES];
        self.tableViewInitAnimation = YES;
        self.didChangeToImageTableView = YES;
        
        self.collectionView.hidden = NO;
        self.collectionView.alpha = 1.0f;
        self.backgroundImageView.hidden = NO;
        //        self.backgroundImageView.frame = [self getOriginalFrame:@"backgroundImageView"];
        //        self.backgroundImageView.layer.transform = CATransform3DMakeScale(backgroundImageZoomScale, backgroundImageZoomScale, 1.0);
        [self initGhostImagePositionWithScale:backgroundImageZoomScale visible:YES];
        CGRect backgroundImageFrame;
        self.tableView.alpha = 0.0f;
        self.tableView.hidden = NO;
        //        self.profileCell.detailProfile.backgroundImageView.hidden = YES;
        UITableViewCell *firstCell = [[self.tableView visibleCells]firstObject];
        CGRect cellFrame = firstCell.bounds;
        cellFrame.size.width = [RealUtility screenBounds].size.width;;
        backgroundImageFrame.size = firstCell.frame.size;
        backgroundImageFrame.size.height -= 8;
        backgroundImageFrame.size.width = [RealUtility screenBounds].size.width;
        backgroundImageFrame.origin.x =0;
        backgroundImageFrame.origin.y =0;
        [self showBackButton:YES];
        [UIView animateWithDuration:0.2 delay:animationTime-0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
              self.tableView.alpha = 1.0;
        } completion:nil];
        [UIView animateWithDuration:animationTime delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.collectionView.alpha = 0.0f;
            self.gradientImageView.alpha = 0.0f;
            self.backgroundImageView.layer.transform = CATransform3DIdentity;
            self.backgroundImageView.frame = backgroundImageFrame;
            self.backgroundHeightConstraint.constant = backgroundImageFrame.size.height;
            [self ghostImageGoToRect:cellFrame transform:CATransform3DIdentity];
            if (self.pendingAgentProfile.status == AgentProfileStatusEdit && self.pendingAgentProfile.AgentListing.Reasons.count >1) {
                self.previewActionView.alpha = 0;
            }
        } completion:^(BOOL finished) {
            if (finished) {
                if (self.pendingAgentProfile.status == AgentProfileStatusEdit && self.pendingAgentProfile.AgentListing.Reasons.count >1) {
                    self.previewActionView.hidden = YES;
                }
                self.backgroundImageView.hidden = YES;
                self.collectionView.hidden = YES;
                self.gradientImageView.hidden = YES;
                //                [self performSelector:@selector(showGhostImage:) withObject:@(0) afterDelay:0.3f];
                [self showGhostImage:NO];
                [self.profileCell adjustAgentProfileInfoByContentOffset: CGPointZero withAlphaEffect:YES];
                self.gradientImageView.frame = [self getOriginalFrame:@"gradientImageView"];
            }
        }];
        
    }
}

-(void) showBackButton:(BOOL)show{
    self.backButton.alpha =!show;
    self.backButton.hidden = NO;
    [UIView animateWithDuration:animationTime delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.backButton.alpha = show;
    }completion:^(BOOL finished) {
        if (finished) {
            self.backButton.hidden = !show;
        }
    }];
}

- (void) showRightArrowButton:(BOOL)show{
    UIButton * rightArrowButton= [self.delegate getTabBarController].customNavBarController.rightArrow;
    rightArrowButton.alpha =!show;
    rightArrowButton.hidden = NO;
    [UIView animateWithDuration:animationTime delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        rightArrowButton.alpha = show;
    }completion:^(BOOL finished) {
        if (finished) {
            rightArrowButton.hidden = !show;
        }
    }];
}

- (void) adjustBackgroundImageViewByFirstCellFrame:(CGRect)frame{
    frame.origin.y +=self.topBar.frame.size.height;
    self.backgroundImageView.frame = frame;
}


#pragma mark -
#pragma mark - MWPhotoBrowserDelegate
- (void)getImageFromAgentListingPhotosToMWPhotoBrower {
    dispatch_queue_t backgroundQueue =
    dispatch_queue_create("com.mycompany.myqueue", 0);
    dispatch_barrier_async(backgroundQueue, ^{
        self.mwPhotosBrowerPhotoArray = [[NSMutableArray alloc] init];
        for (Photos *photo in self.listingImageArray) {
            NSString *photocoverurl = photo.URL;
            NSURL *url = [NSURL
                          URLWithString:[NSString stringWithFormat:@"%@", photocoverurl]];
            [self.mwPhotosBrowerPhotoArray addObject:[MWPhoto photoWithURL:url]];
        }
        
    });
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.mwPhotosBrowerPhotoArray.count;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
               photoAtIndex:(NSUInteger)index {
    if (index < self.mwPhotosBrowerPhotoArray.count)
        return [self.mwPhotosBrowerPhotoArray objectAtIndex:index];
    return nil;
}

-(NSArray*)genImageAnimationArray{
    int count = (int)self.mwPhotosBrowerPhotoArray.count;
    if (count > maxAnimationImageCount) {
        count = maxAnimationImageCount;
    }
    
    if (!self.imageAnimationArray) {
        self.imageAnimationArray = [[NSMutableArray alloc]initWithCapacity:count];
        [self.imageAnimationArray addObject:self.backgroundImageView];
    }
    
    for (int i = 1 ; i < count; i++) {
        UIImageView *imageView;
        BOOL needAddImageView = self.imageAnimationArray.count <= i || !self.imageAnimationArray[i];
        if (needAddImageView) {
            imageView = [[UIImageView alloc]initWithFrame:self.backgroundImageView.bounds];
            
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
        }else{
            imageView.hidden = YES;
            imageView = self.imageAnimationArray[i];
        }
        imageView.hidden = NO;
        MWPhoto *mwPhoto = self.mwPhotosBrowerPhotoArray[i];
        if (!mwPhoto.image) {
            [imageView loadImageURL:mwPhoto.photoURL withIndicator:YES];
        }else{
            imageView.image = mwPhoto.image;
        }
        imageView.hidden = YES;
        if (needAddImageView) {
            [self.imageAnimationArray addObject:imageView];
            [self.contentView insertSubview:imageView aboveSubview:self.tableView];
        }else{
            [self.imageAnimationArray replaceObjectAtIndex:i withObject:imageView];
        }
        
    }
    return self.imageAnimationArray;
}

- (void) initGhostImagePositionWithScale:(float)scale visible:(BOOL)visible{
    int count = (int)self.mwPhotosBrowerPhotoArray.count;
    if (count > maxAnimationImageCount) {
        count = maxAnimationImageCount;
    }
    
    for (int i = 1; i < count; i++) {
        UIImageView *aboveImageView = self.imageAnimationArray[i-1];
        UIImageView *imageView = self.imageAnimationArray[i];
        [imageView moveViewTo:aboveImageView direction:RelativeDirectionBottom padding:0];
        if (scale > 0) {
            imageView.layer.transform = CATransform3DMakeScale(scale + 0.1*i, scale+ 0.1*i, 1.0);
        }
        imageView.hidden = !visible;
        imageView.alpha = 1.0f;
    }
}

-(int)getGhostImageCount{
    int count = (int)self.mwPhotosBrowerPhotoArray.count;
    if (count > maxAnimationImageCount) {
        count = maxAnimationImageCount;
    }
    
    if (count > self.imageAnimationArray.count) {
        count = (int)self.imageAnimationArray.count;
    }
    return count;
}

-(void) ghostImageGoToRect:(CGRect)rect transform:(CATransform3D)transform{
    int count = [self getGhostImageCount];
    
    for (int i = 1; i < count; i++) {
        CGRect finalRect = rect;
        UIImageView *imageView = self.imageAnimationArray[i];
        finalRect.origin.y = self.backgroundImageView.frame.origin.y+ rect.size.height*i;
        imageView.layer.transform = transform;
        imageView.frame = finalRect;
    }
}

-(void) showGhostImage:(BOOL)show{
    int count = [self getGhostImageCount];
    
    for (int i = 1; i < count; i++) {
        UIImageView *imageView = self.imageAnimationArray[i];
        imageView.hidden = !show;
    }
}

-(void) hideGhostImageWithAnimation{
    int count = [self getGhostImageCount];
    for (int i = 1; i < count; i++) {
        UIImageView *imageView = self.imageAnimationArray[i];
        imageView.alpha =0.0;
    }
}

-(void)scrollTopButtonDidPress:(id)sender{
    if (self.didChangeToImageTableView) {
        [self.tableView setContentOffset:CGPointZero animated:YES];
        self.isScrollViewDragFromTop = YES;
    }else{
        [self.collectionView setContentOffset:CGPointZero animated:YES];
    }
}

- (IBAction)backButtonDidPress:(id)sender{
    if(!self.AgentProfile.isPreview){
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        if (self.tableView.contentOffset.y <=0) {
            [self switchToDetailCollectionView];
        }else{
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            self.backToDetail = YES;
        }
    }else{
        [self btnBackPressed:nil];
    }
}

- (IBAction)publishButtonDidPress:(id)sender{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Publishing Posting"];
    
    self.pressPublishButton =YES;
    [self showLoadingHUD];
    if ([AgentListingModel shared].imageArrayCompressFinish) {
        [self callAgentListingAddApis];
    }
}

-(void)callAgentListingAddApis{
    [[AgentListingModel shared] callAgentListingAddApisuccess:^(id responseObject) {
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Successful Publish Posting"];
        [mixpanel.people increment:@"# of listings" by:[NSNumber numberWithInt:1]];
        [mixpanel.people set:@"Listing last updated" to:[NSDate date]];
        [mixpanel.people setOnce:@{
                                   @"First listing created": [NSDate date]
                                   }];
        
        AgentProfile *updatedAgentProfile = responseObject;
        [LoginBySocialNetworkModel shared].myLoginInfo.AgentProfile = updatedAgentProfile;
        [[LoginBySocialNetworkModel shared].myLoginInfo saveInDataBase];
        [[LoginBySocialNetworkModel shared].myLoginInfo updateCustomData];
        [self hideLoadingHUD];
        BOOL needShowViral = [[LoginBySocialNetworkModel shared].myLoginInfo showInviteViralAfterPublish];
        
        if (needShowViral) {
            
            __weak typeof(self) weakSelf = self;
            [[REPhonebookManager sharedManager] requestAccess:^(BOOL granted) {
                
                BaseViewController *vc;
                
                if (granted) {
                    
                    vc = [RealPhoneBookShareViewController phoneBookShareVCWithShareType:RealPhoneBookShareTypeBoardcast];
                    
                } else {
                    
                    vc = [[ViralViewController alloc] initWithType:ViralViewControllerTypeBroadcastSkip];
                }
                
                if (vc.needCustomBackground) {
                    
                    weakSelf.navigationController.delegate = weakSelf;
                }
                
                vc.underLayViewController = weakSelf.underLayViewController;
                
                [weakSelf.navigationController pushViewController:vc animated:YES];
                [weakSelf moveToFinishprofileHaveListingPage:NO];
                
                [[LoginBySocialNetworkModel shared].myLoginInfo didShowInviteViral];
            }];
            
        }else{
            [self moveToFinishprofileHaveListingPage:YES];
        }
        self.pressPublishButton=NO;
        [AgentListingModel shared].imageArrayCompressFinish=NO;
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
        [self hideLoadingHUD];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
        self.pressPublishButton=NO;
        
        
    }];
    
}


- (void)moveToFinishprofileHaveListingPage:(BOOL)move {
    NSDictionary* userInfo = @{@"didEdit": @(1),@"tabBarShouldHide":@(!move)};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateAgentListing" object:userInfo];
    if (move) {
        if (self.navigationController.presentingViewController) {
            [self.delegate dismissOverlayerViewController:self];
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(IBAction)removeLanguageButton:(id)sender{
    if (self.pendingAgentProfile.isPreview) {
        if (self.previewReasonLangIndexArray.count >1) {
            if (!self.selectedLangIndex) {
                [self resetSelectedLangIndex];
            }
            
            [[AgentListingModel shared]removeReasonDictByLangIndex:self.selectedLangIndex];
            [self resetSelectedLangIndex];
            [self setupPreview];
            [self updatePreviewProfile];
            [self.previewLanguageSwipeView reloadData];
        }
    }
}

-(void)resetSelectedLangIndex{
    if (self.pendingAgentProfile.status == AgentProfileStatusEdit) {
        Reasons *firstReason = self.AgentProfile.AgentListing.Reasons.firstObject;
        self.selectedLangIndex = @(firstReason.LanguageIndex);
    }else if (self.pendingAgentProfile.status == AgentProfileStatusPreview){
        self.selectedLangIndex = [[AgentListingModel shared]firstReasonLangIndex];
    }
}

-(IBAction)addLanguageButton:(id)sender{
    if([[AgentListingModel shared] canAddMoreLanguage]){
        Addlanguage *addLanguage =[[Addlanguage alloc]init];
        addLanguage.needCustomBackground = self.needCustomBackground;
        if(self.needCustomBackground){
            self.navigationController.delegate = self;
        }
        addLanguage.underLayViewController = self.underLayViewController;
        [self.navigationController pushViewController:addLanguage animated:YES];
    }else{
        [self showAlertWithTitle:nil message:JMOLocalizedString(@"ready_to_publish__max_supporting_languages", nil)];
    }
}


- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    if (self.pendingAgentProfile.isPreview) {
        return [AgentListingModel shared].reasonDict.allKeys.count;
    }else if(self.pendingAgentProfile.status == AgentProfileStatusEdit){
        return self.pendingAgentProfile.AgentListing.Reasons.count;
    }else{
        return 0;
    }
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    UILabel *label;
    if (!view) {
        view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, swipeView.frame.size.height)];
        view.backgroundColor =[UIColor clearColor];
        label = [[UILabel alloc]initWithFrame:view.bounds];
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor =[UIColor clearColor];
        label.textColor = [UIColor darkGrayColor];
        label.font =[UIFont systemFontOfSize:13];
        label.tag = 1000;
        [view addSubview:label];
    }
    
    NSString *langTitle =self.previewReasonLangTitleArray[index];
    label = (UILabel*)[view viewWithTag:1000];
    label.text = langTitle;
    if (index == swipeView.currentItemIndex) {
        label.textColor = [UIColor whiteColor];
    }else{
        label.textColor = [UIColor lightGrayColor];
    }
    return view;
}


- (CGSize)swipeViewItemSize:(SwipeView *)swipeView{
    return CGSizeMake(100, swipeView.frame.size.height);
}
- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{
    [swipeView scrollToItemAtIndex:index duration:0.3];
    //    self.selectedLangIndex = self.previewReasonLangIndexArray[index];
}
- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    self.selectedLangIndex = self.previewReasonLangIndexArray[swipeView.currentItemIndex];
    [swipeView reloadData];
}
- (void)swipeViewDidEndDragging:(SwipeView *)swipeView willDecelerate:(BOOL)decelerate{
    [swipeView scrollToItemAtIndex:swipeView.currentItemIndex duration:0.15];
}

- (void)setSelectedLangIndex:(NSNumber*)selectedLangIndex{
    _selectedLangIndex = selectedLangIndex;
    self.pendingAgentProfile.AgentListing.selectedLangIndex = _selectedLangIndex;
    [self.collectionView setContentOffset:CGPointZero animated:NO];
    [self setupCollectionView];
    [self.collectionView reloadData];
    [self.profileCell.detailProfile resetAgentProfileScale];
}

-(void)setPendingAgentProfile:(AgentProfile *)pendingAgentProfile{
    _pendingAgentProfile =pendingAgentProfile;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)compressImageArrayAndSetupObserve{
    dispatch_queue_t imageQueue = dispatch_queue_create("Image Queue",NULL);
    
    
    dispatch_async(imageQueue, ^{
        [[AgentListingModel shared]compressImageArraywithBlock:^(id response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.pressPublishButton) {
                    
                    [self callAgentListingAddApis];
                }
                
                
            });
        }];
        
        
    });
    
    
}
@end
