//
//  BaseButton.h
//  productionreal2
//
//  Created by Alex Hung on 28/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseButton : UIButton
@property (nonatomic,strong) id ref;

-(void)verticalImageAndText;
@end
