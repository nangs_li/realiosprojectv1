//
//  FilterRangeCell.h
//  productionreal2
//
//  Created by Alex Hung on 7/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#define FilterTypePrice @"FilterTypePrice"
#define FilterTypeSize @"FilterTypeSize"
#define FilterTypeSpokenLanguage @"FilterTypeSpokenLanguage"

@interface FilterRangeCell : UITableViewCell
@property (nonatomic,assign) int minValue;
@property (nonatomic,assign) int maxValue;
@property (nonatomic,strong) IBOutlet UIButton *filterTitleButton;
@property (nonatomic,strong) IBOutlet UIButton *pullDownButton;
@property (nonatomic,strong) IBOutlet UILabel *unitLabel;
@property (nonatomic,strong) IBOutlet UILabel *valueLabel;

- (void)configureCell:(NSDictionary*)dict;
@end
