//
//  FilterRangeCell.m
//  productionreal2
//
//  Created by Alex Hung on 7/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "FilterRangeCell.h"
#import "UIView+Utility.h"
@implementation FilterRangeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)configureCell:(NSDictionary *)dict{
    NSString *filterType = dict[@"filterType"];
    NSString *filterImage = dict[@"filterImage"];
    NSString *filterTitle = dict[@"filterTitle"];
    NSString *filterUnit = dict[@"filterUnit"];
    NSString *maxValueString = dict[@"maxValue"];
    NSString *minValueString = dict[@"minValue"];
    
    NSString *value = nil;
    
    if ([filterType isEqualToString:FilterTypeSpokenLanguage]) {
        value = minValueString;
    }else{
        if ([minValueString isEqualToString:maxValueString]) {
            value = minValueString;
        }else{
            value = [NSString stringWithFormat:@"%@ - %@",minValueString,maxValueString];
        }
        if ([value isEqualToString:JMOLocalizedString(@"common__any", nil)]) {
            filterUnit = @"";
        }
    }
    self.filterTitleButton.titleLabel.numberOfLines = 3;
    self.filterTitleButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.filterTitleButton.titleLabel.minimumScaleFactor = 0.5;
    self.filterTitleButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.filterTitleButton setTitle:filterTitle forState:UIControlStateNormal];
    [self.filterTitleButton setImage:[UIImage imageNamed:filterImage] forState:UIControlStateNormal];
    CGRect filterTitleFrame = self.filterTitleButton.frame;
    filterTitleFrame.size = [self.filterTitleButton sizeThatFits:CGSizeMake(100, CGFLOAT_MAX)];
    filterTitleFrame.size.width += 16;
    self.filterTitleButton.frame = filterTitleFrame;
    
    
    self.unitLabel.text = filterUnit;
    CGRect unitLabelFrame = self.unitLabel.frame;
    unitLabelFrame.size = [self.unitLabel sizeThatFits:CGSizeMake(40, 40)];
    self.unitLabel.frame = unitLabelFrame;
    
    self.valueLabel.text = value;
    CGRect valueLabelFrame = self.valueLabel.frame;
    valueLabelFrame.size = [self.valueLabel sizeThatFits:CGSizeMake(150, 40)];
    self.valueLabel.frame = valueLabelFrame;
    
    UIView *rightMostView = self.pullDownButton;
    if ([filterType isEqualToString:FilterTypePrice] || [filterType isEqualToString:FilterTypeSpokenLanguage]) {
        [self.valueLabel moveViewTo:rightMostView direction:RelativeDirectionLeft padding:8];
        rightMostView = self.valueLabel;
        [self.unitLabel moveViewTo:rightMostView direction:RelativeDirectionLeft padding:4];
    }else if([filterType isEqualToString:FilterTypeSize]){
        [self.unitLabel moveViewTo:rightMostView direction:RelativeDirectionLeft padding:8];
        rightMostView = self.unitLabel;
        [self.valueLabel moveViewTo:rightMostView direction:RelativeDirectionLeft padding:4];
    }
    
    [self.valueLabel setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
    [self.unitLabel align:self.valueLabel direction:ViewAlignmentBottom padding:0];
}

@end
