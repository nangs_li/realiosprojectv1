//
//  FilterStepCell.h
//  productionreal2
//
//  Created by Alex Hung on 8/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#define FilterTypeBathroom @"FilterTypeBathroom"
#define FilterTypeBedroom @"FilterTypeBedroom"

@protocol FilterStepCellDelagate <NSObject>
-(void)filterStepValueDidChange:(NSInteger)value type:(NSString*)type;

@end


@interface FilterStepCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UIButton *filterTitleButton;
@property (nonatomic,strong) IBOutlet UIButton *downButton;
@property (nonatomic,strong) IBOutlet UIButton *upButton;
@property (nonatomic,strong) IBOutlet UILabel *valueLabel;
@property (nonatomic,strong) id<FilterStepCellDelagate>delagate;
@property (nonatomic,assign) NSInteger value;
@property (nonatomic,strong) NSString *filterType;
- (void)configureCell:(NSDictionary*)dict;
- (IBAction) stepButtonDidPress:(UIButton*)button;
@end
