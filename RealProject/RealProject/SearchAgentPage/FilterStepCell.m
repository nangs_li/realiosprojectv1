//
//  FilterStepCell.m
//  productionreal2
//
//  Created by Alex Hung on 8/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "FilterStepCell.h"
#import "UIView+Utility.h"
@implementation FilterStepCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)configureCell:(NSDictionary *)dict{
    NSString *filterType = dict[@"filterType"];
    NSString *filterImage = dict[@"filterImage"];
    NSString *filterTitle = dict[@"filterTitle"];
    NSString *valueString = dict[@"minValue"];
    
    self.filterType =filterType;
    BOOL valid;
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:valueString];
    valid = [alphaNums isSupersetOfSet:inStringSet];
    if (!valid){
        self.value = 0;
    }else{
        self.value = [valueString integerValue];
    }
        
    
    self.filterTitleButton.titleLabel.numberOfLines = 1;
    [self.filterTitleButton setTitle:filterTitle forState:UIControlStateNormal];
    [self.filterTitleButton setImage:[UIImage imageNamed:filterImage] forState:UIControlStateNormal];
    CGRect filterTitleFrame = self.filterTitleButton.frame;
    filterTitleFrame.size = [self.filterTitleButton sizeThatFits:CGSizeMake(100, 23)];
    filterTitleFrame.size.width += 8;
    self.filterTitleButton.frame = filterTitleFrame;
    
    [self updateLabelValue:self.value];
    UIView *rightMostView = self.upButton;
    [self.valueLabel moveViewTo:rightMostView direction:RelativeDirectionLeft padding:0];
    rightMostView = self.valueLabel;
    [self.downButton moveViewTo:rightMostView direction:RelativeDirectionLeft padding:0];
    
    [self.valueLabel setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
}

-(void)updateLabelValue:(NSInteger)value{
    self.value = value;
    NSString *valueString = @"";
    if (value == 0) {
        
        valueString = JMOLocalizedString(@"common__any", nil);
    }else{
        valueString = [NSString stringWithFormat:@"%d",(int)value];
    }
    self.valueLabel.text = valueString;
}
- (IBAction) stepButtonDidPress:(UIButton*)button{
    if (button == self.upButton) {
        if (self.value < 9999) {
            self.value ++;
        }
    }else if(button == self.downButton){
        if (self.value > 0) {
            self.value --;
        }
    }
    
    if ([self.delagate respondsToSelector:@selector(filterStepValueDidChange:type:)]) {
        [self.delagate filterStepValueDidChange:self.value type:self.filterType];
    }
    
    [self updateLabelValue:self.value];
}
@end
