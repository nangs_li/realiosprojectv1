//
//  FilterTypeCell.h
//  productionreal2
//
//  Created by Alex Hung on 28/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"

@protocol FilterTypeCellDelegate <NSObject>

- (void) filterTypeDidChange:(NSArray*)selectedDictArray;

@end


@interface FilterTypeCell : UITableViewCell <SwipeViewDataSource,SwipeViewDelegate>
@property (nonatomic,assign) BOOL isSingleChoice;
@property (nonatomic,strong) id<FilterTypeCellDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *selectedIndexArray;
@property (nonatomic,strong) NSMutableArray *selectedDictArray;
@property (nonatomic,strong) IBOutlet SwipeView *swipeView;
@property (nonatomic,strong) NSArray *typeArray;
@property (nonatomic,assign) BOOL initialAnimationDidShow;
-(void)configureCell:(NSDictionary*)dict;
-(void)initialAnimation:(CGFloat)duration;
- (void)scrollToLastObject;
- (void)scrollToFirstSelectedObject:(CGFloat)duration;
@end
