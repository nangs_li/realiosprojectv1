//
//  FilterTypeCell.m
//  productionreal2
//
//  Created by Alex Hung on 28/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "FilterTypeCell.h"
#import "BaseButton.h"
#import "RealUtility.h"
#import "UIImage+Utility.h"
@implementation FilterTypeCell

- (void)awakeFromNib {
    // Initialization code
    self.swipeView.pagingEnabled = NO;
    self.swipeView.startPositionX = 20;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView{
    return CGSizeMake(90, 88);
}
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    return self.typeArray.count;
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    BaseButton *typeButton;
    if (!view){
        view = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 90, 88)];
        view.backgroundColor = [UIColor clearColor];
        typeButton = [BaseButton buttonWithType:UIButtonTypeCustom];
        typeButton.frame = CGRectMake(0.0f, 0.0f, 88, 88);
        [typeButton addTarget:self action:@selector(typeButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
        typeButton.titleLabel.font =[UIFont systemFontOfSize:10.0f];
        [typeButton setTitleColor:[UIColor colorWithRed:150/255.0f green:150/255.0f blue:150/255.0f alpha:1.0] forState:UIControlStateNormal];
        [typeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [typeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [typeButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrey]] forState:UIControlStateNormal];
        [typeButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateHighlighted];
        [typeButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateSelected];
        typeButton.tag =1000;
        [view addSubview:typeButton];
    }
    
    NSDictionary *typeDict = self.typeArray[index];
    NSString *title = typeDict[@"title"];
    NSString *normalImage = typeDict[@"normalImage"];
    NSString *highlightImage = typeDict[@"highlightImage"];
    
    typeButton = (BaseButton*) [view viewWithTag:1000];
    typeButton.ref = @(index);
    [typeButton setTitle:title forState:UIControlStateNormal];
    [typeButton setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
    [typeButton setImage:[UIImage imageNamed:highlightImage] forState:UIControlStateHighlighted];
    [typeButton setImage:[UIImage imageNamed:highlightImage] forState:UIControlStateSelected];
    [typeButton verticalImageAndText];
    typeButton.selected = [self.selectedIndexArray containsObject:@(index)];
    return view;
}
- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{
    
}

-(void)configureCell:(NSDictionary*)dict{
    BOOL isInit = ![RealUtility isValid:self.selectedDictArray];
    self.typeArray = dict[@"typeArray"];
    self.selectedIndexArray = [NSMutableArray arrayWithArray:dict[@"selectedArray"]];
    if ([RealUtility isValid:self.selectedIndexArray]) {
        for (NSNumber *indexNumber in self.selectedIndexArray) {
            if (!self.selectedDictArray) {
                self.selectedDictArray =[[NSMutableArray alloc]init];
            }
            int index = [indexNumber intValue];
            NSDictionary *typeDict = self.typeArray[index];
            if (![self.selectedDictArray containsObject:typeDict]) {
                [self.selectedDictArray addObject:typeDict];
            }
        }
    }else{
        self.selectedDictArray = [[NSMutableArray alloc]init];
    }
    
    if (!isInit && [self.delegate respondsToSelector:@selector(filterTypeDidChange:)]) {
        [self.delegate filterTypeDidChange:self.selectedDictArray];
    }
    [self.swipeView reloadData];    
}

-(void)initialAnimation:(CGFloat)duration{
    if (!self.initialAnimationDidShow) {
        self.initialAnimationDidShow = YES;
        [self scrollToLastObject];
        [self scrollToFirstSelectedObject:duration];
    }
}

- (void)scrollToLastObject{
    if ([self.typeArray valid]) {
        [self.swipeView scrollToItemAtIndex:self.typeArray.count -1 duration:0];
    }
}

- (void)scrollToFirstSelectedObject:(CGFloat)duration{
    if ([self.typeArray valid]) {
        NSInteger firstSelectedIndex = 0;
        [self.swipeView scrollToItemAtIndex:firstSelectedIndex duration:duration];
    }
}

-(IBAction)typeButtonDidPress:(BaseButton*)button{
    if (!self.selectedIndexArray || self.isSingleChoice) {
        self.selectedIndexArray = [[NSMutableArray alloc]init];
    }
    
    if (!self.selectedDictArray || self.isSingleChoice) {
        self.selectedDictArray = [[NSMutableArray alloc]init];
    }
    
    NSNumber *indexNumber = button.ref;
    DDLogDebug(@"selected button:%d",[indexNumber intValue]);
    button.selected = !button.selected;
    NSDictionary *typeDict = self.typeArray[[indexNumber intValue]];
    NSNumber *propertyType = typeDict[@"propertyType"];
    NSNumber *spaceType = typeDict[@"spaceType"];
    if (button.selected) {
        if (![self.selectedIndexArray containsObject:indexNumber]) {
            [self.selectedIndexArray addObject:indexNumber];
            [self.selectedDictArray addObject:typeDict];
        }
    }else{
        [self.selectedIndexArray removeObject:indexNumber];
        [self.selectedDictArray removeObject:typeDict];
    }
    
    if ([self.delegate respondsToSelector:@selector(filterTypeDidChange:)]) {
        [self.delegate filterTypeDidChange:self.selectedDictArray];
    }
    if (self.isSingleChoice) {
        [self.swipeView reloadData];
    }
}


@end
