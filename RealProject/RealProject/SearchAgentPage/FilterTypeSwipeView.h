//
//  FilterTypeSwipeView.h
//  productionreal2
//
//  Created by Alex Hung on 14/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "SwipeView.h"

@protocol FilterTypeSwipeViewDelegate <NSObject>

-(void)FilterTypeSwipeViewDidSelectFilter:(NSArray*)selectedDictArray;

@end


@interface FilterTypeSwipeView : SwipeView <SwipeViewDataSource,SwipeViewDelegate>
@property (nonatomic,strong) NSMutableArray *selectedIndexArray;
@property (nonatomic,strong) id<FilterTypeSwipeViewDelegate> selectedDelegate;
@property (nonatomic,strong) NSArray *typeArray;
@property (nonatomic,strong) NSMutableArray *selectedDictArray;

-(void)configureFilterTypeSwipeView:(NSDictionary*)dict;
@end
