//
//  FilterTypeSwipeView.m
//  productionreal2
//
//  Created by Alex Hung on 14/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "FilterTypeSwipeView.h"
#import "BaseButton.h"
#import "RealUtility.h"
@implementation FilterTypeSwipeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}


- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    return self.typeArray.count;
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    BaseButton *typeButton;
    if (!view){
        view = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 90, 88)];
        view.backgroundColor = [UIColor clearColor];
        typeButton = [BaseButton buttonWithType:UIButtonTypeCustom];
        typeButton.frame = CGRectMake(0.0f, 0.0f, 88, 88);
        [typeButton addTarget:self action:@selector(typeButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
        typeButton.titleLabel.font =[UIFont systemFontOfSize:10.0f];
        [typeButton setTitleColor:[UIColor colorWithRed:150/255.0f green:150/255.0f blue:150/255.0f alpha:1.0] forState:UIControlStateNormal];
        [typeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [typeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [typeButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:42/255.0f green:42/255.0f blue:42/255.0f alpha:1.0]] forState:UIControlStateNormal];
        [typeButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateHighlighted];
        [typeButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateSelected];
        typeButton.tag =1000;
        [view addSubview:typeButton];
    }
    
    NSDictionary *typeDict = self.typeArray[index];
    NSString *title = typeDict[@"title"];
    NSString *normalImage = typeDict[@"normalImage"];
    NSString *highlightImage = typeDict[@"highlightImage"];
    
    typeButton = (BaseButton*) [view viewWithTag:1000];
    typeButton.ref = @(index);
    [typeButton setTitle:title forState:UIControlStateNormal];
    [typeButton setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
    [typeButton setImage:[UIImage imageNamed:highlightImage] forState:UIControlStateHighlighted];
    [typeButton setImage:[UIImage imageNamed:highlightImage] forState:UIControlStateSelected];
    [typeButton verticalImageAndText];
    typeButton.selected = [self.selectedIndexArray containsObject:@(index)];
    return view;
}
- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{
    
}

-(IBAction)typeButtonDidPress:(BaseButton*)button{
    if (!self.selectedIndexArray) {
        self.selectedIndexArray = [[NSMutableArray alloc]init];
    }
    
    if (!self.selectedDictArray) {
        self.selectedDictArray = [[NSMutableArray alloc]init];
    }
    
    NSNumber *indexNumber = button.ref;
    DDLogDebug(@"selected button:%d",[indexNumber intValue]);
    button.selected = !button.selected;
    NSDictionary *typeDict = self.typeArray[[indexNumber intValue]];
    NSNumber *propertyType = typeDict[@"propertyType"];
    NSNumber *spaceType = typeDict[@"spaceType"];
    if (button.selected) {
        if (![self.selectedIndexArray containsObject:indexNumber]) {
            [self.selectedIndexArray addObject:indexNumber];
            [self.selectedDictArray addObject:typeDict];
        }
    }else{
        [self.selectedIndexArray removeObject:indexNumber];
        [self.selectedDictArray removeObject:typeDict];
    }
    
    if ([self.selectedDelegate respondsToSelector:@selector(FilterTypeSwipeViewDidSelectFilter:)]) {
        [self.selectedDelegate FilterTypeSwipeViewDidSelectFilter:self.selectedDictArray];
    }
    
}

@end
