//
//  TagView.h
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagView : UIView
@property (nonatomic,strong)NSArray *tags;

-(id)initWithTags:(NSArray*)tag withFrame:(CGRect)frame;
@end
