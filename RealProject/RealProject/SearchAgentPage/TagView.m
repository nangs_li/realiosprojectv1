//
//  TagView.m
//  productionreal2
//
//  Created by Alex Hung on 9/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "TagView.h"

@implementation TagView
-(id)initWithTags:(NSArray*)tag withFrame:(CGRect)frame{
    self =[self initWithFrame:frame];
    self.tags =[NSArray arrayWithArray:tag];
    if (self) {
        
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
