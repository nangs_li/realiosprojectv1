//
//  UIDotImageView.m
//  productionreal2
//
//  Created by Alex Hung on 31/8/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "UIDotImageView.h"
#import "UIImage+Utility.h"
@implementation UIDotImageView

-(void)awakeFromNib{
    [super awakeFromNib];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat thickness = self.bounds.size.height;
    
    CGContextRef cx = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(cx, thickness);
    CGContextSetStrokeColorWithColor(cx,[UIColor lightGrayColor].CGColor);
    CGFloat lengths[] = {1, 1};
    NSInteger lengthsCount = 2;
    CGContextSetLineDash(cx, 0.0, lengths, lengthsCount);
    CGContextMoveToPoint(cx, 0, thickness * 0.5);
    CGContextAddLineToPoint(cx, self.bounds.size.width, thickness * 0.5);
    CGContextStrokePath(cx);
}


@end
