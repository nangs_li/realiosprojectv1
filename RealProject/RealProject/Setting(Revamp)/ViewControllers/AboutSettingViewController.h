//
//  AboutSettingViewController.h
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM (NSInteger, AboutSettingRow) {
    AboutSettingRowVersion,
    AboutSettingRowTerms,
    AboutSettingRowPrivacy,
    AboutSettingRowCopyright,
    AboutSettingRowCount
};

@interface AboutSettingViewController : BaseViewController

@end
