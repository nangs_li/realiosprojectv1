//
//  AboutSettingViewController.m
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "AboutSettingViewController.h"

// Views
#import "RealArrowCell.h"
#import "RealCopyrightCell.h"
#import "RealVersionCell.h"

// Controllers
#import "Termsofservice.h"
#import "Privatepolicy.h"

// Constants
static CGFloat kNormalCellHeight = 45.0;
static CGFloat kCopyrightCellHeight = 55.0;

@interface AboutSettingViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *view;

@end

@implementation AboutSettingViewController

@dynamic view;

#pragma mark - View Lifecycle

- (void)loadView
{
    self.view = [UITableView new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
}

#pragma mark - Setup

- (void)setupView
{
    self.view.delegate = self;
    self.view.dataSource = self;
    
    self.view.backgroundColor = RealDarkBlueColor;
    self.view.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view registerClass:[RealArrowCell class] forCellReuseIdentifier:[RealArrowCell reuseIdentifier]];
    [self.view registerClass:[RealCopyrightCell class] forCellReuseIdentifier:[RealCopyrightCell reuseIdentifier]];
    [self.view registerClass:[RealVersionCell class] forCellReuseIdentifier:[RealVersionCell reuseIdentifier]];
}

- (void)setupUpNavBar
{
    [[self.delegate getTabBarController] changeNavBarType:NavigationBarSearch
                                                 animated:YES
                                                    block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController)
     {
         if (didChange) {
             
             self.navBarContentView = navBarController.settingNavBar;
             navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__about", nil);
             [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
             [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
             navBarController.settingBackButton.hidden = NO;
         }
     }];
    self.navBarContentView.alpha = 1.0;
}

#pragma mark - Action

- (IBAction)closebutton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return AboutSettingRowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseTableViewCell *cell;
    
    switch (indexPath.row) {
            
        case AboutSettingRowVersion:
        {
            RealVersionCell *theCell = [tableView dequeueReusableCellWithIdentifier:[RealVersionCell reuseIdentifier]];
            cell = theCell;
            break;
        }
            
        case AboutSettingRowTerms:
        {
            RealArrowCell *theCell = [tableView dequeueReusableCellWithIdentifier:[RealArrowCell reuseIdentifier]];
            theCell.leftLabel.text = JMOLocalizedString(@"setting__terms_of_service", nil);
            theCell.separatorView.hidden = NO;
            theCell.backgroundColor = [UIColor settingsRowBlueColor];
            cell = theCell;
            break;
        }
            
        case AboutSettingRowPrivacy:
        {
            RealArrowCell *theCell = [tableView dequeueReusableCellWithIdentifier:[RealArrowCell reuseIdentifier]];
            theCell.leftLabel.text = JMOLocalizedString(@"setting__privacy_policy", nil);
            theCell.separatorView.hidden = YES;
            theCell.backgroundColor = [UIColor settingsRowBlueColor];
            cell = theCell;
            break;
        }
            
        case AboutSettingRowCopyright:
        {
            RealCopyrightCell *theCell = [tableView dequeueReusableCellWithIdentifier:[RealCopyrightCell reuseIdentifier]];
            cell = theCell;
            break;
        }
    }
    
    return cell;
}

#pragma mark - <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = kNormalCellHeight;
    
    switch (indexPath.row) {
            
        case AboutSettingRowVersion:
            
            rowHeight = self.view.frame.size.height - 2 * kNormalCellHeight - kCopyrightCellHeight;
            break;
            
        case AboutSettingRowTerms:
            
            rowHeight = kNormalCellHeight;
            break;
            
        case AboutSettingRowPrivacy:
            
            rowHeight = kNormalCellHeight;
            break;
            
        case AboutSettingRowCopyright:
            
            rowHeight = kCopyrightCellHeight;
            break;
    }
    
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseViewController *vc = nil;
    
    switch (indexPath.row) {
            
        case AboutSettingRowTerms:
            
            vc = [[Termsofservice alloc] initWithNibName:@"Termsofservice" bundle:nil];
            break;

        case AboutSettingRowPrivacy:
            
            vc = [[Privatepolicy alloc] initWithNibName:@"Privatepolicy" bundle:nil];
            break;
    }
    
    if (vc) {
        
        [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
        vc.view.backgroundColor = self.view.backgroundColor;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
