//
//  ChatsSettingViewController.h
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM (NSInteger, ChatsSettingSection) {
    ChatsSettingSectionMedia,
    ChatsSettingSectionPrivacy,
    ChatsSettingSectionCount
};

typedef NS_ENUM (NSInteger, ChatsSettingSectionMediaRow) {
    ChatsSettingSectionMediaRowMedia,
    ChatsSettingSectionMediaRowCount
};

typedef NS_ENUM (NSInteger, ChatsSettingSectionPrivacyRow) {
    ChatsSettingSectionPrivacyRowLastSeen,
    ChatsSettingSectionPrivacyRowReceipts,
    ChatsSettingSectionPrivacyRowCount
};

@interface ChatsSettingViewController : BaseViewController

@end
