//
//  ChatsSettingViewController.m
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ChatsSettingViewController.h"

// Views
#import "RealSwitchCell.h"

// Models
#import "DBChatSetting.h"
#import "ChatDialogModel.h"
#import "ChatRelationModel.h"

@interface ChatsSettingViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *view;
@property (nonatomic, assign) BOOL saveIncomingMedia;
@property (nonatomic, assign) BOOL showLastSeen;
@property (nonatomic, assign) BOOL showReadReceipts;

@end

@implementation ChatsSettingViewController

@dynamic view;

#pragma mark - View Lifecycle

- (void)loadView
{
    self.view = [UITableView new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
    
    // Save Incoming Media
    DBResultSet *r = [[[DBChatSetting query]
                       whereWithFormat:@"MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]
                      
                      fetch];
    
    for (DBChatSetting *dbChatSetting in r) {
        
        _saveIncomingMedia = dbChatSetting.ChatRoomPhotoAutoDownload;
    }
    _showLastSeen = [[LoginBySocialNetworkModel shared].myLoginInfo customData].chatroomshowlastseen;
    _showReadReceipts = [[LoginBySocialNetworkModel shared].myLoginInfo customData].chatroomshowreadreceipts;
    
    [self.view reloadData];

}

#pragma mark - Setup

- (void)setupView
{
    self.view.delegate = self;
    self.view.dataSource = self;
    
    self.view.backgroundColor = RealDarkBlueColor;
    self.view.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view registerClass:[RealSwitchCell class] forCellReuseIdentifier:[RealSwitchCell reuseIdentifier]];
}

- (void)setupUpNavBar
{
    [[self.delegate getTabBarController] changeNavBarType:NavigationBarSearch
                                                 animated:YES
                                                    block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController)
     {
         if (didChange) {
             
             self.navBarContentView = navBarController.settingNavBar;
             navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__chats", nil);
             [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
             [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
             navBarController.settingBackButton.hidden = NO;
         }
     }];
    self.navBarContentView.alpha = 1.0;
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ChatsSettingSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowNumber = 0;
    
    switch (section) {
            
        case ChatsSettingSectionMedia:
            
            rowNumber = ChatsSettingSectionMediaRowCount;
            break;
            
        case ChatsSettingSectionPrivacy:
            
            rowNumber = ChatsSettingSectionPrivacyRowCount;
            break;
            
        default:
            
            break;
    }
    
    return rowNumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RealSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:[RealSwitchCell reuseIdentifier]];
    
    cell.backgroundColor = [UIColor settingsRowBlueColor];
    
    switch (indexPath.section) {
            
        case ChatsSettingSectionMedia:
            
            switch (indexPath.row) {
                    
                case ChatsSettingSectionMediaRowMedia:
                    
                    cell.leftLabel.text = JMOLocalizedString(@"chat_setting__save_incoming_media", nil);
                    cell.separatorView.hidden = YES;
                    cell.rightIndicatorView.hidden = !_saveIncomingMedia;
                    break;
            }
            
            break;
            
        case ChatsSettingSectionPrivacy:
            
            switch (indexPath.row) {
                    
                case ChatsSettingSectionPrivacyRowLastSeen:
                    
                    cell.leftLabel.text = JMOLocalizedString(@"chat_privacy__show_last_seen", nil);
                    cell.separatorView.hidden = NO;
                    cell.rightIndicatorView.hidden = !_showLastSeen;
                    break;
                    
                case ChatsSettingSectionPrivacyRowReceipts:
                    
                    cell.leftLabel.text = JMOLocalizedString(@"chat_privacy__read_receipts", nil);
                    cell.separatorView.hidden = YES;
                    cell.rightIndicatorView.hidden = !_showReadReceipts;
                    break;
            }
            break;
    }
    
    return cell;
}

#pragma mark - <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = [UIColor clear];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.section) {
            
        case ChatsSettingSectionMedia:
            
            switch (indexPath.row) {
                    
                case ChatsSettingSectionMediaRowMedia:
                
                    [self updateSaveIncomingMedia];
                    break;
            }
            
            break;
            
        case ChatsSettingSectionPrivacy:
            
            switch (indexPath.row) {
                    
                case ChatsSettingSectionPrivacyRowLastSeen:
                    
                    _showLastSeen = !_showLastSeen;
                    [self.view reloadData];
                    break;
                    
                case ChatsSettingSectionPrivacyRowReceipts:
                    
                    _showReadReceipts = !_showReadReceipts;
                    [self.view reloadData];
                    break;
            }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

#pragma mark - Helpers

- (void)updateSaveIncomingMedia
{
    DBResultSet *r = [[[[DBChatSetting query]
                        whereWithFormat:@"MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]
                       limit:1]
                      
                      fetch];
    
    _saveIncomingMedia = !_saveIncomingMedia;
    
    for (DBChatSetting *dbChatSetting in r) {
        
        dbChatSetting.ChatRoomPhotoAutoDownload = _saveIncomingMedia;
        [dbChatSetting commit];
    }
    
    [self.view reloadData];
}

- (void)updateLastSeenAndReadReceipts{
    [ChatService shared].myQBUUserCustomData.chatroomshowlastseen = _showLastSeen;
    [ChatService shared].myQBUUserCustomData.chatroomshowreadreceipts = _showReadReceipts;
    [LoginBySocialNetworkModel shared].myLoginInfo.chatroomshowlastseen = [ChatService shared].myQBUUserCustomData.chatroomshowlastseen;
    [LoginBySocialNetworkModel shared].myLoginInfo.chatroomshowreadreceipts = [ChatService shared].myQBUUserCustomData.chatroomshowreadreceipts;
    
    [[LoginBySocialNetworkModel shared].myLoginInfo updateCustomDataWithSuccessBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
        for (QBChatDialog * qbchatdialog in [ChatDialogModel shared].dialogs) {
            NSString * RecipientID =  [qbchatdialog getRecipientIDFromQBChatDialog];
            [[ChatRelationModel shared] sendSystemMessageToRefreshChatPrivacyRecipientID:RecipientID];
        }
    } errorBlock:^(QBResponse * _Nonnull response) {
        //roll back
        DDLogError(@"QBResponse %@",response);
    }];
    
}

#pragma mark - Action

- (IBAction)closebutton:(id)sender
{
    [self updateLastSeenAndReadReceipts];
    if (![[AppDelegate getAppDelegate] networkConnection]) {
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:JMOLocalizedString(NotNetWorkConnectionText, nil) errorStatus:NotNetWorkConnection alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

@end
