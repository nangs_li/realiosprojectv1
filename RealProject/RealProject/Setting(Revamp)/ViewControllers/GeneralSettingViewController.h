//
//  GeneralSettingViewController.h
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM (NSInteger, GeneralSettingSection) {
    GeneralSettingSectionLanguage,
    GeneralSettingSectionUnit,
    GeneralSettingSectionUser,
    GeneralSettingSectionCount
};

typedef NS_ENUM (NSInteger, GeneralSettingSectionLanguageRow) {
    GeneralSettingSectionLanguageRowLanuage,
    GeneralSettingSectionLanguageRowCount
};

typedef NS_ENUM (NSInteger, GeneralSettingSectionUnitRow) {
    GeneralSettingSectionUnitRowUnit,
    GeneralSettingSectionUnitRowCount
};

typedef NS_ENUM (NSInteger, GeneralSettingSectionUserRow) {
    GeneralSettingSectionUserRowPhone,
    GeneralSettingSectionUserRowCount
};

@interface GeneralSettingViewController : BaseViewController

@end
