//
//  GeneralSettingViewController.m
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "GeneralSettingViewController.h"

// Views
#import "RealArrowCell.h"

// Controllers
#import "Languagepage.h"
#import "RealChangeEmailViewController.h"
#import "MetricSetting.h"
#import "RealChangePhoneIntroViewController.h"

// Models
#import "LoginByEmailModel.h"

// Mixpanel
#import "Mixpanel.h"

@interface GeneralSettingViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *view;
@property (nonatomic, strong) NSString *selectedLanguage;
@property (nonatomic, strong) NSString *selectedUnit;

@end

@implementation GeneralSettingViewController

@dynamic view;

#pragma mark - View Lifecycle

- (void)loadView
{
    self.view = [UITableView new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
    
    // Selected Language
    if ([self isEmpty:[SystemSettingModel shared].selectSystemLanguageList]) {
        
        SystemLanguageList * firstLanguage = [SystemSettingModel shared].SystemSettings.SystemLanguageList.firstObject;
        _selectedLanguage = firstLanguage.NativeName;
        
    } else {
        
        _selectedLanguage = [SystemSettingModel shared].selectSystemLanguageList.NativeName;
    }
    
    // Selected Unit
    if ([self isEmpty:[SystemSettingModel shared].SystemSettings.userSelectMetric]) {
        
        MetricList * firstMetric =[SystemSettingModel shared].metricaccordingtolanguage.firstObject;
        _selectedUnit = firstMetric.NativeName;
       
    } else {
        
        _selectedUnit = [SystemSettingModel shared].SystemSettings.userSelectMetric.NativeName;
    }
    
    [self.view reloadData];
}

#pragma mark - Setup

- (void)setupView
{
    self.view.delegate = self;
    self.view.dataSource = self;
    
    self.view.backgroundColor = RealDarkBlueColor;
    self.view.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view registerClass:[RealArrowCell class] forCellReuseIdentifier:[RealArrowCell reuseIdentifier]];
}

- (void)setupUpNavBar
{
    [[self.delegate getTabBarController] changeNavBarType:NavigationBarSearch
                                                 animated:YES
                                                    block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController)
     {
         if (didChange) {
             
             self.navBarContentView = navBarController.settingNavBar;
             navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__general", nil);
             [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
             [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
             navBarController.settingBackButton.hidden = NO;
         }
     }];
    self.navBarContentView.alpha = 1.0;
}

#pragma mark - Action

- (IBAction)closebutton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([LoginByEmailModel shared].realNetworkMemberInfo.UserID) {
        
        return GeneralSettingSectionCount;
        
    } else {
        
        return (GeneralSettingSectionCount - 1);    // To hide change email & password if FB login
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowNumber = 0;
    
    switch (section) {
            
        case GeneralSettingSectionLanguage:
            
            rowNumber = GeneralSettingSectionLanguageRowCount;
            break;
            
        case GeneralSettingSectionUnit:
            
            rowNumber = GeneralSettingSectionUnitRowCount;
            break;
            
        case GeneralSettingSectionUser:
            
            rowNumber = GeneralSettingSectionUserRowCount;
            break;
            
        default:
            
            break;
    }
    
    return rowNumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RealArrowCell *cell = [tableView dequeueReusableCellWithIdentifier:[RealArrowCell reuseIdentifier]];
    
    cell.backgroundColor = [UIColor settingsRowBlueColor];
    
    switch (indexPath.section) {
            
        case GeneralSettingSectionLanguage:
            
            switch (indexPath.row) {
                    
                case GeneralSettingSectionLanguageRowLanuage:
                    
                    cell.leftLabel.text = JMOLocalizedString(@"setting__language", nil);
                    cell.rightLabel.text = _selectedLanguage;
                    cell.separatorView.hidden = YES;
                    break;
            }
            
            break;
            
        case GeneralSettingSectionUnit:
            
            switch (indexPath.row) {
                    
                case GeneralSettingSectionUnitRowUnit:
                    
                    cell.leftLabel.text = JMOLocalizedString(@"setting__unit", nil);
                    cell.rightLabel.text = _selectedUnit;
                    cell.separatorView.hidden = YES;
                    break;
            }
            
            break;
            
        case GeneralSettingSectionUser:
            
            switch (indexPath.row) {
                    
                case GeneralSettingSectionUserRowPhone:
                    
                    cell.leftLabel.text = JMOLocalizedString(@"change_phone__title", nil);
                    break;
            }
            
            break;
    }
    
    return cell;
}

#pragma mark - <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = [UIColor clear];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    BaseViewController *vc = nil;
    
    switch (indexPath.section) {
            
        case GeneralSettingSectionLanguage:
            
            switch (indexPath.row) {
                    
                case GeneralSettingSectionLanguageRowLanuage:
                    
                    vc = [[Languagepage alloc] initWithNibName:@"Languagepage" bundle:nil];
                    break;
            }
            
            break;
            
        case GeneralSettingSectionUnit:
            
            switch (indexPath.row) {
                    
                case GeneralSettingSectionUnitRowUnit:
                    
                    vc = [[MetricSetting alloc] initWithNibName:@"MetricSetting" bundle:nil];
                    break;
            }
            
            break;
            
        case GeneralSettingSectionUser:
            
            switch (indexPath.row) {
                    
                case GeneralSettingSectionUserRowPhone:
                    
                    vc = [RealChangePhoneIntroViewController changePhoneIntroViewController];
                    break;
                    
            }
            
            break;
    }
    
    if (vc) {
        
        [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
        vc.view.backgroundColor = self.view.backgroundColor;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

@end
