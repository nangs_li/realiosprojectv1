//
//  SettingContentViewController.h
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM (NSInteger, SettingContentRow) {
    SettingContentRowGeneral,
    SettingContentRowChats,
    SettingContentRowFeedback,
    SettingContentRowAbout,
    SettingContentRowCount
};

@class SettingContentViewController;

@protocol SettingContentViewControllerDelegate <NSObject>

@optional

- (void)controllerDidPressLogout:(SettingContentViewController *)controller;

@end

@interface SettingContentViewController : BaseViewController

@property (nonatomic, weak) id<SettingContentViewControllerDelegate> settingContentViewControllerDelegate;

@end
