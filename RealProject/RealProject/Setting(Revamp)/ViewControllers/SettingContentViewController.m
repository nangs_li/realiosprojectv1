//
//  SettingContentViewController.m
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "SettingContentViewController.h"

// Views
#import "RealArrowCell.h"

// Controllers
#import "Reportaproblem.h"
#import "ChatsSettingViewController.h"
#import "GeneralSettingViewController.h"
#import "AboutSettingViewController.h"

@interface SettingContentViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *view;

@end

@implementation SettingContentViewController

@dynamic view;

#pragma mark - View Lifecycle

- (void)loadView
{
    self.view = [UITableView new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view reloadData];
}

#pragma mark - Setup

- (void)setupView
{
    self.view.delegate = self;
    self.view.dataSource = self;
    
    self.view.backgroundColor = RealDarkBlueColor;
    self.view.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    [self.view registerClass:[RealArrowCell class] forCellReuseIdentifier:[RealArrowCell reuseIdentifier]];
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return SettingContentRowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RealArrowCell *cell = [tableView dequeueReusableCellWithIdentifier:[RealArrowCell reuseIdentifier]];
    
    NSString *labelText = @"";
    
    switch (indexPath.row) {
            
        case SettingContentRowGeneral:
            
            labelText = JMOLocalizedString(@"setting__general", nil);
            break;
            
        case SettingContentRowChats:
            
            labelText = JMOLocalizedString(@"setting__chats", nil);
            break;
            
        case SettingContentRowFeedback:
            
            labelText = JMOLocalizedString(@"setting__feedback", nil);
            break;
            
        case SettingContentRowAbout:
            
            labelText = JMOLocalizedString(@"setting__about", nil);
            break;
        default:
            
            break;
    }
    
    cell.leftLabel.text = labelText;
    
    return cell;
}

#pragma mark - <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseViewController *vc = nil;
    
    switch (indexPath.row) {
            
        case SettingContentRowGeneral:
            
            vc = [[GeneralSettingViewController alloc] init];
            break;
            
        case SettingContentRowChats:
            
            vc = [[ChatsSettingViewController alloc] init];
            break;
            
        case SettingContentRowFeedback:
            
            vc = [[Reportaproblem alloc] initWithNibName:@"Reportaproblem" bundle:nil];
            break;
            
        case SettingContentRowAbout:
            
            vc = [[AboutSettingViewController alloc] init];
            break;
            
        default:
            break;
    }
    
    if (vc) {
        
        [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
        vc.view.backgroundColor = self.view.backgroundColor;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
