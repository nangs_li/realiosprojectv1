//
//  RealCopyrightCell.h
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface RealCopyrightCell : BaseTableViewCell

@property (nonatomic, strong) UILabel *leftLabel;

@end
