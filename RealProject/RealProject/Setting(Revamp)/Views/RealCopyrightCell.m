//
//  RealCopyrightCell.m
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealCopyrightCell.h"

@implementation RealCopyrightCell

#pragma mark - Setup

- (void)setup
{
    [super setup];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self setupLeftLabel];
}

- (void)setupLeftLabel
{
    _leftLabel = [UILabel newAutoLayoutView];
    
    _leftLabel.text = JMOLocalizedString(@"setting__copyright", nil);
    _leftLabel.font = [UIFont systemFontOfSize:10];
    _leftLabel.numberOfLines = 2;
    _leftLabel.textColor = [UIColor settingsCellGreyTextColor];
    
    [self.contentView addSubview:_leftLabel];
}

#pragma mark - Setup Contraints

- (void)setupConstraints
{
    [super setupConstraints];
    
    [self setupLeftLabelConstraints];
}

- (void)setupLeftLabelConstraints
{
    [_leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:25.0];
    [_leftLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:25.0];
    [_leftLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
}

@end
