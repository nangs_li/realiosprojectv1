//
//  RealVersionCell.h
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface RealVersionCell : BaseTableViewCell

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *versionLabel;

@end
