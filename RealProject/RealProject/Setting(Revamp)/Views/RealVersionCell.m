//
//  RealVersionCell.m
//  productionreal2
//
//  Created by Derek Cheung on 8/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealVersionCell.h"

@implementation RealVersionCell

#pragma mark - Setup

- (void)setup
{
    [super setup];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImage *backgroundImage = [UIImage imageNamed:@"setting_about_bg"];
    self.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    [self setupContainerView];
    [self setupLogoImageView];
    [self setupTitleLabel];
    [self setupVersionLabel];
}

- (void)setupContainerView
{
    _containerView = [UIView newAutoLayoutView];
    
    [self.contentView addSubview:_containerView];
}

- (void)setupLogoImageView
{
    _logoImageView = [UIImageView newAutoLayoutView];
    
    _logoImageView.image = [UIImage imageNamed:@"setting_appicon"];
    
    [_containerView addSubview:_logoImageView];
}

- (void)setupTitleLabel
{
    _titleLabel = [UILabel newAutoLayoutView];
    
    _titleLabel.text = @"Real";
    _titleLabel.font = [UIFont systemFontOfSize:22];
    _titleLabel.textColor = [UIColor white];
    
    [_containerView addSubview:_titleLabel];
}

- (void)setupVersionLabel
{
    _versionLabel = [UILabel newAutoLayoutView];
    
    NSString *appVerison = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *buildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *commitHash = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"GITHash"];
    NSString *description = [NSString stringWithFormat:@"%@: %@ \n%@: %@ [%@]", JMOLocalizedString(@"about_this_version__version", nil),appVerison,JMOLocalizedString(@"about_this_version__build", nil), buildNumber, commitHash];
    _versionLabel.text = description;
    _versionLabel.font = [UIFont systemFontOfSize:12];
    _versionLabel.textColor = [UIColor settingsCellGreyTextColor];
    _versionLabel.numberOfLines = 2;
    _versionLabel.textAlignment = NSTextAlignmentCenter;
    
    [_containerView addSubview:_versionLabel];
}

#pragma mark - Setup Constraints

- (void)setupConstraints
{
    [super setupConstraints];
    
    [self setupContainerViewConstraints];
    [self setupLogoImageViewConstraints];
    [self setupTitleLabelConstraints];
    [self setupVersionLabelConstraints];
}

- (void)setupContainerViewConstraints
{
    [_containerView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [_containerView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:25.0];
    [_containerView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:25.0];
}

- (void)setupLogoImageViewConstraints
{
    [_logoImageView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [_logoImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];
}

- (void)setupTitleLabelConstraints
{
    [_titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_logoImageView withOffset:20.0];
    [_titleLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
}

- (void)setupVersionLabelConstraints
{
    [_versionLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_titleLabel withOffset:10.0];
    [_versionLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [_versionLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom];
}

@end
