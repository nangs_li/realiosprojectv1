//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Aboutthisversion.h"
#import "LegalStatementGetModel.h"
#import "LoginByEmailModel.h"
#import "CocoaSecurity.h"
@interface Aboutthisversion ()

@end
@implementation Aboutthisversion

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
  [self setupUpNavBar];
    
    
   }

- (void)setupUpNavBar {
  [[self.delegate getTabBarController]
      changeNavBarType:NavigationBarSearch
              animated:YES
                 block:^(BOOL didChange, NavigationBarType currentType,
                         CustomNavigationBarController *navBarController) {
                   if (didChange) {
                     self.navBarContentView = navBarController.settingNavBar;
                     navBarController.settinTitleLabel.text =
                         JMOLocalizedString(@"about_this_version__title", nil);
                     [navBarController.settingBackButton
                             removeTarget:nil
                                   action:NULL
                         forControlEvents:UIControlEventAllEvents];
                     [navBarController.settingBackButton
                                addTarget:self
                                   action:@selector(closebutton:)
                         forControlEvents:UIControlEventTouchUpInside];
                     navBarController.settingBackButton.hidden = NO;
                   }
                 }];
  self.navBarContentView.alpha = 1.0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.reportaproblemtextview.delegate = self;
    
    [[self.delegate getTabBarController] setTabBarHidden:YES];
    
    NSString *appVerison = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *buildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *commitHash = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"GITHash"];
    NSString *description = [NSString stringWithFormat:@"Version: %@ \nBuild: %@ [%@]", appVerison, buildNumber, commitHash];
    
    self.reportaproblemtextview.text = description;
}
- (void)viewDidLoad {
  [super viewDidLoad];
    self.view.backgroundColor = RealDarkBlueColor;
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
  self.titleLabel.text =
      JMOLocalizedString(@"about_this_version__title", nil);
  [self.nextbutton setTitle:JMOLocalizedString(@"common__save", nil)
                   forState:UIControlStateNormal];
}

//#pragma mark - textview Delegate--------------------------------------------------------------------------------
//- (void)textViewDidEndEditing:(UITextView *)textView {
//  if ([textView.text isEqualToString:@""]) {
//    textView.text = @"Report Some Problem Here...";
//    textView.textColor = [UIColor lightGrayColor];  // optional
//  }
//  [textView resignFirstResponder];
//}
//- (void)textViewDidBeginEditing:(UITextView *)textView {
//  if ([textView.text isEqualToString:@"Report Some Problem Here..."]) {
//    textView.text = @"";
//    textView.textColor = [UIColor lightGrayColor];  // optional
//  }
//  [textView becomeFirstResponder];
//}
//
//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
//  return YES;
//}
//- (BOOL)textView:(UITextView *)textView
//    shouldChangeTextInRange:(NSRange)range
//            replacementText:(NSString *)text {
//  DDLogInfo(@"shouldChangeTextInRange");
//
//  if ([text isEqualToString:@"\n"]) {
//    [textView resignFirstResponder];
//
//    return NO;
//  }
//
//  return YES;
//}

#pragma mark - Button
- (IBAction)closebutton:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

@end