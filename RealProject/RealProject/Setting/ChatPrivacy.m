//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "ChatPrivacy.h"
#import "SpokenLanguagescell.h"
#import "DBChatSetting.h"
#import "QBUUserCustomData.h"
#import "RealUtility.h"
#import "ChatRelationModel.h"
#import "ChatDialogModel.h"
@interface ChatPrivacy ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation ChatPrivacy
@synthesize tableData;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
    [QBRequest userWithID:[[LoginBySocialNetworkModel shared]
                           .myLoginInfo.QBID integerValue]
             successBlock:^(QBResponse *response, QBUUser *user) {
                 [self.delegate quickBloxRegisterPushNotification];
                 NSError *err = nil;
                 [ChatService shared].myQBUUserCustomData =
                 [[QBUUserCustomData alloc] initWithString:user.customData
                                                     error:&err];
                 [[ChatService shared].myQBUUserCustomData checkCustomDataValidate:user.customData];
                  [self setUpTableviewFromMyQBUUserCustomData];
                 DDLogDebug(@"userWithID success%@", response);
                 
                 
             }
               errorBlock:^(QBResponse *response) {
                   DDLogError(@"[QBRequest userWithID%@", response);
               }];

}

-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"common__privacy", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
             navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RealDarkBlueColor;
    
    tableData = [[NSMutableArray alloc]
                 initWithObjects:
                 
                 JMOLocalizedString(@"chat_privacy__show_last_seen", nil), JMOLocalizedString(@"chat_privacy__read_receipts", nil), nil];
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.tableview reloadData];
    self.tableview.scrollEnabled = NO;
    
    if (![self isEmpty:[ChatService shared].myQBUUserCustomData]){
        
       
        
    }else{
        
        [self loadingAndStopLoadingAfterSecond:8];
        
        dispatch_time_t popTime =
        dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            if (![self isEmpty:[ChatService shared].myQBUUserCustomData]){
                   [self hideLoadingHUD];
                
            }
            
        });
    }
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpTableviewFromMyQBUUserCustomData{
    
    if ([ChatService shared].myQBUUserCustomData.chatroomshowlastseen) {
        _showLastSeenCell = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    if ([ChatService shared].myQBUUserCustomData.chatroomshowreadreceipts) {
        _showReadReceipts = [NSIndexPath indexPathForRow:1 inSection:0];
    }
    [self.tableview reloadData];
    
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    
    
    self.titleLabel.text=JMOLocalizedString(@"common__privacy", nil);
    
    [self.nextbutton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];
}






#pragma mark - Button
- (IBAction)closebutton:(id)sender {
    [self nextbuttonpressed:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextbuttonpressed:(id)sender {
    
    QBUUserCustomData *qbuusercustomdata = [QBUUserCustomData new];
    
    if ([self isEmpty:_showLastSeenCell]) {
        
        
        qbuusercustomdata.chatroomshowlastseen = NO;
        
    } else {
        
        qbuusercustomdata.chatroomshowlastseen = YES;
    }
    
    if ([self isEmpty:_showReadReceipts]) {
        
        qbuusercustomdata.chatroomshowreadreceipts = NO;
    } else {
        
        qbuusercustomdata.chatroomshowreadreceipts = YES;
    }
    
    
    qbuusercustomdata.mypersonalphotourl = [LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL;
    QBUpdateUserParameters *qbupdateuserparameters = [QBUpdateUserParameters new];
    qbupdateuserparameters.customData = [qbuusercustomdata toJSONString];
    [QBRequest updateCurrentUser:qbupdateuserparameters
                    successBlock:^(QBResponse *response, QBUUser *user) {
                        [ChatService shared].myQBUUserCustomData.chatroomshowlastseen=qbuusercustomdata.chatroomshowlastseen;
                        [ChatService shared].myQBUUserCustomData.chatroomshowreadreceipts=qbuusercustomdata.chatroomshowreadreceipts;
                        
                        for (QBChatDialog * qbchatdialog in [ChatDialogModel shared].dialogs) {
                         NSString * RecipientID =  [qbchatdialog getRecipientIDFromQBChatDialog];
                             [[ChatRelationModel shared] sendSystemMessageToRefreshChatPrivacyRecipientID:RecipientID];
                        }
                       
                    }
                      errorBlock:^(QBResponse *response){
                          DDLogError(@"QBResponse %@",response);
                      }];
//    [self closebutton:self];
}






#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *label;
    UIImageView *markerImageView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:simpleTableIdentifier];
        CGRect cellFrame = cell.bounds;
        cellFrame.size.width = [RealUtility screenBounds].size.width;
        cell.contentView.frame = cellFrame;
        UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(20, 0,cellFrame.size.width - 40, cellFrame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.tag = 1001;
        UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(20, cellFrame.size.height -1, cellFrame.size.width-40, 1)];
        dividerView.image =[UIImage imageNamed:@"pop_dottedline"];
        markerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 15, 15)];
        markerImageView.clipsToBounds = YES;
        markerImageView.tag = 1002;
        markerImageView.image = [UIImage imageWithColor:RealBlueColor];
        markerImageView.hidden = YES;
        label.font=[label.font fontWithSize:14];
        [cell.contentView addSubview:label];
        [cell.contentView addSubview:dividerView];
        [cell.contentView addSubview:markerImageView];
        [markerImageView setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
        [markerImageView setViewAlignmentInSuperView:ViewAlignmentRight padding:20];
    }
    label = (UILabel*) [cell.contentView viewWithTag:1001];
    label.text = [tableData objectAtIndex:indexPath.row];
    label.textColor = [UIColor whiteColor];
    markerImageView = (UIImageView*) [cell.contentView viewWithTag:1002];
    if (indexPath.row == 0) {
        if ([_showLastSeenCell isEqual:indexPath]){
            markerImageView.hidden = NO;
        } else {
            markerImageView.hidden = YES;
        }
    } else {
        if ([_showReadReceipts isEqual:indexPath]){
            markerImageView.hidden = NO;
        } else {
            markerImageView.hidden = YES;
        }
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = nil;
       cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableview.frame.size.width, 44)];
    headerView.backgroundColor = [UIColor clearColor];
//    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(24, 0, headerView.frame.size.width-24, headerView.frame.size.height)];
//    label.backgroundColor = [UIColor clearColor];
//    label.textColor = [UIColor lightGrayColor];
//    label.text = JMOLocalizedString(@"chat_privacy__title", nil);
//    UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(16, 43, headerView.frame.size.width-16, 1)];
//    dividerView.image =[UIImage imageNamed:@"content_breakline"];
//    [headerView addSubview:label];
//    [headerView addSubview:dividerView];
    return headerView;
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if ([_showLastSeenCell isEqual:indexPath])
            
        {
            _showLastSeenCell = nil;
            
        } else {
            _showLastSeenCell = indexPath;
        }
    } else {
        if ([_showReadReceipts isEqual:indexPath])
            
        {
            _showReadReceipts = nil;
            
        } else {
            _showReadReceipts = indexPath;
        }
    }
    [tableView reloadData];
    
}

@end