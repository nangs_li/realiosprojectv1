//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "ChatSetting.h"
#import "SpokenLanguagescell.h"
#import "DBChatSetting.h"
#import "RealUtility.h"

@interface ChatSetting ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation ChatSetting
@synthesize tableData;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
    DBResultSet *r = [[[DBChatSetting query]
                       whereWithFormat:@"MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]
                      
                      fetch];
   
    for (DBChatSetting *dbChatSetting in r) {
        
        if (dbChatSetting.ChatRoomPhotoAutoDownload) {
            _photoAutoDownloadYes = [NSIndexPath indexPathForRow:0 inSection:0];
        }
    }
 [self.tableview reloadData];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"common__chat_setting", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
             navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.view.backgroundColor = RealDarkBlueColor;
  tableData = [[NSMutableArray alloc] initWithObjects:

                                          JMOLocalizedString(@"chat_setting__save_incoming_media", nil), nil];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];

  self.tableview.delegate = self;
  self.tableview.dataSource = self;
  [self.tableview reloadData];
  self.tableview.scrollEnabled = NO;
 
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.titleLabel.text=JMOLocalizedString(@"common__chat_setting", nil);
    [self.nextbutton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];
}





#pragma mark - Button
- (IBAction)closebutton:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextbuttonpressed:(id)sender {
  DBResultSet *r = [[[[DBChatSetting query]
      whereWithFormat:@"MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]
      limit:1]

      fetch];

  for (DBChatSetting *dbChatSetting in r) {
    if ([self isEmpty:_photoAutoDownloadYes]) {
      dbChatSetting.ChatRoomPhotoAutoDownload = NO;

    } else {
      dbChatSetting.ChatRoomPhotoAutoDownload = YES;
    }

    [dbChatSetting commit];
  }
//
//  [self closebutton:self];
}







#pragma mark- UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
  return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  static NSString *simpleTableIdentifier = @"UITableViewCell";

  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *label;
    UIImageView *markerImageView;
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:simpleTableIdentifier];
      CGRect cellFrame = cell.bounds;
      cellFrame.size.width = [RealUtility screenBounds].size.width;
      cell.contentView.frame = cellFrame;
      label =[[UILabel alloc]initWithFrame:CGRectMake(20, 0, cellFrame.size.width-20,cellFrame.size.height)];
      label.backgroundColor = [UIColor clearColor];
      label.textColor = [UIColor whiteColor];
      label.tag = 1001;
      UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(16, cellFrame.size.height -1, cellFrame.size.width-16, 1)];
      dividerView.image =[UIImage imageNamed:@"content_breakline"];
      markerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 15, 15)];
      markerImageView.clipsToBounds = YES;
      markerImageView.tag = 1002;
      markerImageView.image = [UIImage imageWithColor:RealBlueColor];
      markerImageView.hidden = YES;
      
      [cell.contentView addSubview:label];
//      [cell.contentView addSubview:dividerView];
            [cell.contentView addSubview:markerImageView];
      [markerImageView setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
      [markerImageView setViewAlignmentInSuperView:ViewAlignmentRight padding:20];
  }else{
  }
    label = (UILabel*) [cell.contentView viewWithTag:1001];
    label.textColor = [UIColor whiteColor];
    label.text = [tableData objectAtIndex:indexPath.row];
    markerImageView = (UIImageView*) [cell.contentView viewWithTag:1002];
    markerImageView.hidden = ![_photoAutoDownloadYes isEqual:indexPath];

    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = nil;
   cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    return nil;
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableview.frame.size.width, 0)];
    headerView.backgroundColor = [UIColor clearColor];
//    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(24, 0, headerView.frame.size.width-24, headerView.frame.size.height)];
//    label.backgroundColor = [UIColor clearColor];
//    label.textColor = [UIColor lightGrayColor];
//    label.text = JMOLocalizedString(@"setting__title", nil);
//    UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(16, 43, headerView.frame.size.width-16, 1)];
//    dividerView.image =[UIImage imageNamed:@"content_breakline"];
//    [headerView addSubview:label];
//    [headerView addSubview:dividerView];
    
    return headerView;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  if ([_photoAutoDownloadYes isEqual:indexPath])

  {
    _photoAutoDownloadYes = nil;

  } else {
    _photoAutoDownloadYes = indexPath;
  }

  [tableView reloadData];
    [self nextbuttonpressed:nil];
}

@end