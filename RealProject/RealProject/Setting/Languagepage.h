//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

@interface Languagepage : BaseViewController
@property(strong, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic, retain) NSMutableArray *tableData;
@property(nonatomic, retain) NSIndexPath *checkedLanguageCell;
@property(nonatomic, strong) UILabel *tableViewHeaderTitleLabel;


- (IBAction)closebutton:(id)sender;
@end
