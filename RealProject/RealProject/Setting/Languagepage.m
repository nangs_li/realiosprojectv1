//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Languagepage.h"
#import "SpokenLanguagescell.h"
#import "DBSystemSettings.h"
#import "RealUtility.h"
@interface Languagepage ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation Languagepage
@synthesize tableData;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
      [self setupUpNavBar];
//    CGFloat dummyViewHeight = 44;
//    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableview.bounds.size.width, dummyViewHeight)];
//    self.tableview.tableHeaderView = dummyView;
//  //  self.tableview.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0);
//    
//   
//    dummyView.backgroundColor = [UIColor clearColor];
//    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(24, 0, dummyView.frame.size.width-24, dummyView.frame.size.height)];
//    label.backgroundColor = [UIColor clearColor];
//    label.textColor = [UIColor lightGrayColor];
//    label.text = JMOLocalizedString(@"common__languages", nil);
//    self.tableViewHeaderTitleLabel=label;
//    UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(16, 43, dummyView.frame.size.width-16, 1)];
//    dividerView.image =[UIImage imageNamed:@"content_breakline"];
//    [dummyView addSubview:label];
//    [dummyView addSubview:dividerView];
//    [self.tableview addSubview:dummyView];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RealDarkBlueColor;
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    tableData = [[NSMutableArray alloc] init];
    
    for (SystemLanguageList *dict in [SystemSettingModel shared].SystemSettings
         .SystemLanguageList) {
        DDLogInfo(@"dictdict-->%@", dict);
        [tableData addObject:dict.NativeName];
    }
    int i=0;
    for (SystemLanguageList *systemlanguage in[SystemSettingModel shared].SystemSettings.SystemLanguageList) {
        if ([[SystemSettingModel shared].selectSystemLanguageList.NativeName
             isEqualToString:systemlanguage.NativeName]) {
            _checkedLanguageCell = [NSIndexPath indexPathForRow:i inSection:0];
        }
        i++;
    }
    
    if (isEmpty([SystemSettingModel shared].selectSystemLanguageList.NativeName)) {
         _checkedLanguageCell = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    
    [self.tableview reloadData];
}
-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__language", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
//    self.tableViewHeaderTitleLabel.text = JMOLocalizedString(@"common__languages", nil);
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__language", nil);
                       navBarController.settingBackButton.hidden = NO;
        }
    }];
    [self.tableview reloadData];
}







#pragma mark - Button
- (IBAction)closebutton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)savebuttonpressed:(id)sender {
  
    [SystemSettingModel shared].selectSystemLanguageList =
    [[SystemSettingModel shared].SystemSettings
     .SystemLanguageList[self.checkedLanguageCell.row] copy];
    DDLogDebug(@"[SystemSettingModel shared].selectSystemLanguageList %@",[SystemSettingModel shared].selectSystemLanguageList.Code);
             [[AppDelegate getAppDelegate] setupLanguageFromSystemSettingSelectSystemLanguage];
         [[AppDelegate getAppDelegate] customizeTabBarForController:(RDVTabBarController *)[AppDelegate getAppDelegate].viewController];
    
    [self checkIfLayoutChange];
    DBResultSet *  r = [[[[DBSystemSettings query]
                          whereWithFormat:@"MemberID = %@",
                          [NSString
                           stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
                           .myLoginInfo.MemberID]]
                         limit:1] fetch];
    
    for (DBSystemSettings *systemSettings in r) {
        systemSettings.selectSystemLanguageList =[NSKeyedArchiver archivedDataWithRootObject:[SystemSettingModel shared].selectSystemLanguageList
                                                  ];
        [systemSettings commit];
        
        // [NSKeyedUnarchiver unarchiveObjectWithData:systemSettings.selectSystemLanguageList];
    }
    
    //// set delegate store posting data to nil
    [[AgentListingModel shared] resetAllAgentListingPartOnlyLocalData];
    [[SystemSettingModel shared] setupMetricAccordingToLanguage];
    [[SystemSettingModel shared] setupPropertyTypeAccordingToLanguage];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage object:nil];

}



-(void)checkIfLayoutChange{
    if ([NSLocale characterDirectionForLanguage:[SystemSettingModel shared].selectSystemLanguageList.Code]==NSLocaleLanguageDirectionRightToLeft) {
        
        if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionLeftToRight) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                                                                message:JMOLocalizedString(@"language_setting__language_update_need_start", nil)
                                                               delegate:nil
                                                      cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    }
    if ([NSLocale characterDirectionForLanguage:[SystemSettingModel shared].selectSystemLanguageList.Code]==kCFLocaleLanguageDirectionLeftToRight) {
        
        if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                                                                message:JMOLocalizedString(@"language_setting__language_update_need_start", nil)
                                                               delegate:nil
                                                      cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    }

    
}



#pragma mark  UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *label;
    UIImageView *markerImageView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:simpleTableIdentifier];
        CGRect cellFrame = cell.bounds;
        cellFrame.size.width = [RealUtility screenBounds].size.width;
        cell.contentView.frame = cellFrame;
        label =[[UILabel alloc]initWithFrame:CGRectMake(20, 0, cellFrame.size.width-20, cellFrame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.tag = 1001;
        UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(20, cellFrame.size.height -1, cellFrame.size.width-40, 1)];
        dividerView.image =[UIImage imageNamed:@"pop_dottedline"];
        markerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(cellFrame.size.width - 20 -15, 0, 15, 15)];
        markerImageView.clipsToBounds = YES;
        markerImageView.tag = 1002;
        markerImageView.image = [UIImage imageWithColor:RealBlueColor];
        markerImageView.hidden = YES;
        
        [cell.contentView addSubview:label];
        [cell.contentView addSubview:dividerView];
        [cell.contentView addSubview:markerImageView];
        [markerImageView setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
        [markerImageView setViewAlignmentInSuperView:ViewAlignmentRight padding:20];
    }else{
        
    }
    label = (UILabel*) [cell.contentView viewWithTag:1001];
    label.textColor = [UIColor whiteColor];
    label.text = [tableData objectAtIndex:indexPath.row];
    markerImageView = (UIImageView*) [cell.contentView viewWithTag:1002];
    markerImageView.hidden =![_checkedLanguageCell isEqual:indexPath];
    cell.backgroundColor =[UIColor clearColor];
    cell.backgroundView = nil;
    return cell;
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _checkedLanguageCell = indexPath;
    
    [tableView reloadData];
    [self savebuttonpressed:nil];
}

@end