//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "TDPicker.h"
@interface MetricSetting : BaseViewController<TDPickerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;



@property (strong, nonatomic) IBOutlet TDPicker *metricPicker;
@property (strong, nonatomic) IBOutlet UIButton *metric;

@end
