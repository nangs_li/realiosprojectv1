//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "MetricSetting.h"
#import "DBChatSetting.h"
#import "SystemSettingModel.h"
#import "RealUtility.h"
#import "DBSystemSettings.h"
@interface MetricSetting ()

@end
@implementation MetricSetting


- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
}

-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"common__unit", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(backButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}
- (void)viewDidLoad {
  [super viewDidLoad];
self.view.backgroundColor = RealDarkBlueColor;
 self.metricPicker.delegate = self;
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    self.metricPicker.pickerData = [[NSMutableArray alloc] init];
    for (MetricList *metric in [SystemSettingModel shared].metricaccordingtolanguage) {
        [self.metricPicker.pickerData addObject:metric.NativeName];
       
    }
   MetricList * firstMetric =[SystemSettingModel shared].metricaccordingtolanguage.firstObject;
    [self.metric setTitle:firstMetric.NativeName
                 forState:UIControlStateNormal];
    if (![self isEmpty:[SystemSettingModel shared].SystemSettings.userSelectMetric]) {
        [self.metric setTitle:[SystemSettingModel shared].SystemSettings.userSelectMetric.NativeName
                     forState:UIControlStateNormal];
    }
  
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.titleLabel.text=JMOLocalizedString(@"common__unit", nil);
    [self.metricPicker.cancelBtn setTitle:JMOLocalizedString(@"common__cancel", nil)];
    [self.metricPicker.saveBtn setTitle:JMOLocalizedString(@"common__save", nil)];
    
}



#pragma mark -TDPicker(Currency,Metric) Delegate------------------------------------------------------------------

- (IBAction)pressMetricList:(id)sender {
    [self.metricPicker.picker reloadAllComponents];
    [self presentSemiModalViewController:_metricPicker];
}
- (void)picker:(UIPickerView *)picker
  didSelectRow:(NSInteger)row
   inComponent:(NSInteger)component {
   if (picker == self.metricPicker.picker) {
        DDLogInfo(@"_metricpicker.selectpickerdata-->%@",
                  _metricPicker.pickerData[row]);
    }
}
- (void)pickerSetDate:(TDPicker *)viewController {
    [SystemSettingModel shared].SystemSettings.userSelectMetric = [[SystemSettingModel shared].metricaccordingtolanguage
                                                                   objectAtIndex:[self.metricPicker.picker selectedRowInComponent:0]]; ;
        [self.metric setTitle:[SystemSettingModel shared].SystemSettings.userSelectMetric.NativeName
                     forState:UIControlStateNormal];
    DBResultSet *  r = [[[[DBSystemSettings query]
                          whereWithFormat:@"MemberID = %@",
                          [NSString
                           stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
                           .myLoginInfo.MemberID]]
                         limit:1] fetch];
    
    for (DBSystemSettings *systemSettings in r) {
        systemSettings.SystemSettings =[NSKeyedArchiver archivedDataWithRootObject:[SystemSettingModel shared].SystemSettings];
        [systemSettings commit];
      }
    

        [self dismissSemiModalViewController:self.metricPicker];
    
    
}

- (void)pickerClearDate:(TDPicker *)viewController {
   
    if (viewController == self.metricPicker) {
   
        [self dismissSemiModalViewController:self.metricPicker];
    }

}

- (void)pickerCancel:(TDPicker *)viewController {
   
    if (viewController == self.metricPicker) {
        [self dismissSemiModalViewController:self.metricPicker];
    }
}

-(IBAction)backButtonDidPress:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}







@end