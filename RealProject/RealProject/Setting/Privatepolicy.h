//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "TPKeyboardAvoidingScrollView.h"
@interface Privatepolicy : BaseViewController<UITextViewDelegate>

@property (nonatomic,assign) IBOutlet NSLayoutConstraint *textViewTopSpacing;

@property (nonatomic,assign) BOOL needShowTitleView;
@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property(strong, nonatomic) IBOutlet UITextView *reportaproblemtextview;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UIView *titleView;
@property (nonatomic,strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
- (IBAction)closebutton:(id)sender;
@end
