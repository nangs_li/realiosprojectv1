//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Privatepolicy.h"
#import "LegalStatementGetModel.h"
@interface Privatepolicy ()

@end
@implementation Privatepolicy

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    if(self.needShowTitleView){
        self.titleView.hidden = NO;
        self.textViewTopSpacing.constant = 0;
//        self.scrollView.frame = CGRectMake(0, 64, [RealUtility screenBounds].size.width -self.scrollView.frame.origin.x *2, [RealUtility screenBounds].size.height -64);
    }else{
        self.textViewTopSpacing.constant = -64;
        self.titleView.hidden = YES;
    [self setupUpNavBar];
        
    }
  if ([self.delegate networkConnection]) {
    [self loadingAndStopLoadingAfterSecond:10];
  }

  [[LegalStatementGetModel shared]
      callLegalStatementGetModelApiByMessageType:@"1"
      success:^(NSString * message) {

          self.reportaproblemtextview.text =message;
             [self hideLoadingHUD];
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                NSString *ok) {
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
      }];

 }


-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"common__privacy_policy", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.reportaproblemtextview.delegate = self;

  [[self.delegate getTabBarController] setTabBarHidden:YES];
}
- (void)viewDidLoad {
  [super viewDidLoad];
    self.view.backgroundColor = RealDarkBlueColor;
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
  self.titleLabel.text = JMOLocalizedString(@"common__privacy_policy", nil);
}

#pragma mark- Button
- (IBAction)closebutton:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
//#pragma mark
//#pragma mark textview  Delegate-- --------------------------------------------------------------------------- -
//- (void)textViewDidEndEditing:(UITextView *)textView {
//  if ([textView.text isEqualToString:@""]) {
//    textView.text = @"Report Some Problem Here...";
//    textView.textColor = [UIColor lightGrayColor];  // optional
//  }
//  [textView resignFirstResponder];
//}
//- (void)textViewDidBeginEditing:(UITextView *)textView {
//  if ([textView.text isEqualToString:@"Report Some Problem Here..."]) {
//    textView.text = @"";
//    textView.textColor = [UIColor lightGrayColor];  // optional
//  }
//  [textView becomeFirstResponder];
//}
//
//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
//  return YES;
//}
//- (BOOL)textView:(UITextView *)textView
//    shouldChangeTextInRange:(NSRange)range
//            replacementText:(NSString *)text {
//  DDLogInfo(@"shouldChangeTextInRange");
//
//  if ([text isEqualToString:@"\n"]) {
//    [textView resignFirstResponder];
//
//    return NO;
//  }
//
//  return YES;
//}


@end