//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Push.h"
#import "SpokenLanguagescell.h"

@interface Push ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation Push
@synthesize tableData;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    [self setupUpNavBar];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = @"Push Notification";
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
             navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  tableData = [[NSMutableArray alloc] initWithObjects:

                                          @"New Follower", @"New Message", nil];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];

  self.tableview.delegate = self;
  self.tableview.dataSource = self;
  [self.tableview reloadData];
  self.tableview.scrollEnabled = NO;
}
//// - Button
- (IBAction)closebutton:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextbuttonpressed:(id)sender {
}

//// UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section

{
  return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
  static NSString *simpleTableIdentifier = @"UITableViewCell";

  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:simpleTableIdentifier];
  }

  cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
  cell.textLabel.textColor = self.delegate.Greycolor;

  if ([_checkedCell isEqual:indexPath])

  {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;

  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }

  return cell;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  _checkedCell = indexPath;

  [tableView reloadData];
  [self.nextbutton setBackgroundColor:self.delegate.Greencolor];
  self.nextbutton.userInteractionEnabled = YES;
}
//// UITableViewDelegate & UITableViewDataSource end--------------------------------------------------------------------------

@end