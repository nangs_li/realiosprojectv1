//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

@interface Reportaproblem : BaseViewController<UITextViewDelegate>

@property(strong, nonatomic) IBOutlet UIButton *reportButton;
@property(strong, nonatomic) IBOutlet UITextView *reportaproblemtextview;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
;
- (IBAction)closebutton:(id)sender;
@end
