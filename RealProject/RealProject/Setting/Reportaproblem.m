//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Reportaproblem.h"
#import "SpokenLanguagescell.h"
#import "ReportProblemModel.h"
#import "RealUtility.h"
@interface Reportaproblem ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation Reportaproblem

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
    self.reportaproblemtextview.text=JMOLocalizedString(@"setting__sendusfeedback_content", nil);
}

-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__feedback", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.reportaproblemtextview.delegate = self;

  [[self.delegate getTabBarController] setTabBarHidden:YES];
}
- (void)viewDidLoad {
  [super viewDidLoad];
    self.view.backgroundColor = RealDarkBlueColor;
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
  self.titleLabel.text =
      JMOLocalizedString(@"setting__feedback", nil);
//  self.reportaproblemtextview.text = JMOLocalizedString(
//      @"SettingPage.Report a Problem.Report Some Problem Here...", nil);
  [self.reportButton
      setTitle:JMOLocalizedString(@"common__submit", nil)
      forState:UIControlStateNormal];
    [self.reportButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
}

#pragma mark - Button
- (IBAction)closebutton:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextbuttonpressed:(id)sender {
  if ([self.delegate networkConnection]) {
    [self loadingAndStopLoadingAfterSecond:10];
  }

  [[ReportProblemModel shared] callReportProblemApiByProblemType:@"1"
      toMemberID:@"0"
      toListingID:@"0"
      description:self.reportaproblemtextview.text
      success:^(id responseObject) {
       
        [[[UIAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                                    message:JMOLocalizedString(@"invite_to_chat__success", nil)
                                   delegate:self
                          cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
                          otherButtonTitles:nil] show];

           [self hideLoadingHUD];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                NSString *ok) {
           [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
      }];

 }

#pragma mark textview Delegate--------------------------------------------------------------------------- -
- (void)textViewDidEndEditing:(UITextView *)textView {
  if ([textView.text isEqualToString:@""]) {
//    textView.text = JMOLocalizedString(
//        @"SettingPage.Report a Problem.Report Some Problem Here...", nil);
    textView.textColor = [UIColor lightGrayColor];  // optional
  }
  [textView resignFirstResponder];
  [self checkEmptyToChangeSaveButtonColor];
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
  if ([textView.text
          isEqualToString:
              JMOLocalizedString(@"setting__sendusfeedback_content", nil)]) {
    textView.text = @"";
    textView.textColor = [UIColor lightGrayColor];  // optional
  }
  [textView becomeFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
  return YES;
}
- (BOOL)textView:(UITextView *)textView
    shouldChangeTextInRange:(NSRange)range
            replacementText:(NSString *)text {
  DDLogInfo(@"shouldChangeTextInRange");

  if ([text isEqualToString:@"\n"]) {
    [textView resignFirstResponder];

    return NO;
  }

  return YES;
}
#pragma mark Support Method

- (void)checkEmptyToChangeSaveButtonColor {
    self.reportButton.userInteractionEnabled = [self.reportaproblemtextview.text length] > 0;
}

@end