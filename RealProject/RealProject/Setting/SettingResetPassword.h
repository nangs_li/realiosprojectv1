//
//  SettingResetPassword.h
//  productionreal2
//
//  Created by Alex Hung on 30/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingResetPassword : BaseViewController
@property (nonatomic,strong) IBOutlet UITextField *emailTextField;
@property (nonatomic,strong) IBOutlet UIButton *resetButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *textFieldErrorLabel;
@end
