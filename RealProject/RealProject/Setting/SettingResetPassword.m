//
//  SettingResetPassword.m
//  productionreal2
//
//  Created by Alex Hung on 30/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "SettingResetPassword.h"
#import "LoginByEmailModel.h"
#define textFieldPlaceHolderColor [UIColor colorWithRed:193/255.0 green:193/255.0 blue:193/255.0 alpha:1.0]
@interface SettingResetPassword ()

@end

@implementation SettingResetPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RealDarkBlueColor;
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    self.emailTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.emailTextField.frame.size];
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common__email", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    self.emailTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    [self.resetButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:NO];
    [self setupUpNavBar];
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    [self.resetButton setTitle:JMOLocalizedString(@"reset_password__reset", nil)
                     forState:UIControlStateNormal];
}
-(void)setupUpNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"reset_password__title", nil);
            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [navBarController.settingBackButton addTarget:self action:@selector(closeButton:) forControlEvents:UIControlEventTouchUpInside];
            navBarController.settingBackButton.hidden = NO;
        }
    }];
    self.navBarContentView.alpha = 1.0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)inputIsValid{
    BOOL valid = YES;
    NSString *email = self.emailTextField.text;
    
    if (![RealUtility isValid:email]){
        [self showAlertWithTitle:nil message:JMOLocalizedString(Pleaseenteravalidemailaddress, nil)];
        valid= NO;
    }else if(![self emailFormatIsValid:email]){
        valid = NO;
        [self showAlertWithTitle:nil message:JMOLocalizedString(Pleaseenteravalidemailaddress, nil)];
    }
    
    return valid;
}

- (BOOL)emailFormatIsValid:(NSString*)email{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/va ... l-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(IBAction)resetButtonDidPress:(id)sender{
    if ([self inputIsValid]) {
        NSString *email = self.emailTextField.text;
        [[LoginByEmailModel shared]callRealNetworkSendResetPasswordEmailApiEmail:email Success:^(id responseObject) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self closeButton:nil];
            }];
            [self showAlertWithTitle:JMOLocalizedString(Resetemailhasbeesent, nil) message:JMOLocalizedString(Tocontinuepleasecheckyouremailandfollowtheinstructions, nil) withAction:action];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
           // [self showAlertWithTitle:nil message:errorMessage];
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                          errorMessage:errorMessage
                                                           errorStatus:errorStatus
                                                                 alert:alert
                                                                    ok:ok];
        }];
    }
}

-(IBAction)closeButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setupTextFieldCheckError{
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];
    
}

-(BOOL)checkInputIsValidNotAlertTitle{
    BOOL valid = YES;
    NSString *email = self.emailTextField.text;
    
    if (![RealUtility isValid:email]){
     self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteravalidemailaddress, nil);
        valid= NO;
    }else if(![self emailFormatIsValid:email]){
        valid = NO;
        self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteravalidemailaddress, nil);
    }
    
    return valid;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)setupKeyBoardDoneBtnAction{
     [self.emailTextField setReturnKeyType:UIReturnKeyDone];
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x){
        
        [self resetButtonDidPress:self];
    }];
}
@end
