//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Share.h"
#import "SpokenLanguagescell.h"

@interface Share ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation Share
@synthesize tableData;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

- (void)viewDidLoad {
  [super viewDidLoad];

  tableData = [[NSMutableArray alloc]
      initWithObjects:@"Mandarin Chinese", @"English", @"Spanish", @"Hindi",
                      @"Bengali", @"Portguese", @"Russian", @"Arabic",
                      @"French", @"German", @"Indonesian", @"Japanese",
                      @"Korean", @"Javanese", @"Telugu", @"Tamil", @"Yue",
                      @"Italina", @"Turkish", @"Vietnamese", @"Bengali",
                      @"Thai", @"Cantonese", @"Punjabi", @"Polish", @"Dutch",
                      @"Farsi", nil];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];

  self.tableview.delegate = self;
  self.tableview.dataSource = self;
  [self.tableview reloadData];
}
//// - Button
- (IBAction)closebutton:(id)sender {
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionPush;
  transition.subtype = kCATransitionFromBottom;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)nextbuttonpressed:(id)sender {
}

//// UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section

{
  return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
  static NSString *simpleTableIdentifier = @"UITableViewCell";

  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:simpleTableIdentifier];
  }

  cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
  cell.textLabel.textColor = self.delegate.Greycolor;

  if ([_checkedCell isEqual:indexPath])

  {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;

  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }

  return cell;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  _checkedCell = indexPath;

  [tableView reloadData];
  [self.nextbutton setBackgroundColor:self.delegate.Greencolor];
  self.nextbutton.userInteractionEnabled = YES;
}
//// UITableViewDelegate & UITableViewDataSource end--------------------------------------------------------------------------

@end