//
//  ShareViewController.h
//  productionreal2
//
//  Created by Alex Hung on 30/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "BaseViewController.h"
typedef enum {
    ShareListing = 0
}ShareType;

typedef enum{
    SharePlatformFacebook = 0,
    SharePlatformTwitter,
    SharePlatformWeibo,
    SharePlatformGoogle
}SharePlatform;

@interface ShareViewController : BaseViewController



@property (nonatomic,strong) IBOutlet UILabel *shareTitleLabel;
@property (nonatomic,assign) ShareType shareType;
-(IBAction)facebookButtonDidPress:(id)sender;
-(IBAction)twitterButtonDidPress:(id)sender;
-(IBAction)weiboButtonDidPress:(id)sender;
-(IBAction)googleButtonDidPress:(id)sender;
@end
