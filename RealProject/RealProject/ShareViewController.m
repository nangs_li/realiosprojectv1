//
//  ShareViewController.m
//  productionreal2
//
//  Created by Alex Hung on 30/9/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "ShareViewController.h"
#import "UILabel+Utility.h"
#import "Photos.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <TwitterKit/TwitterKit.h>
#import "UIAlertView+Blocks.h"
#import "RealUtility.h"


@interface ShareViewController () <FBSDKSharingDelegate>

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.shareTitleLabel.attributedText = nil;
    NSString * share = JMOLocalizedString(@"common__share", nil);
    if ([share isEqualToString:@"Share"]) {
        share = [share uppercaseString];
    }
    NSString *shareTitle =[NSString stringWithFormat:@"<blue>%@</blue><white>%@</white>",share,JMOLocalizedString(@"common_to", nil)];
    [self.shareTitleLabel setAttributedTextWithMarkUp: shareTitle];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)facebookButtonDidPress:(id)sender{
    NSString *imageURL = [self getShareImageURL:self.AgentProfile.AgentListing];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.imageURL = [NSURL URLWithString:imageURL];
    content.contentURL = [NSURL URLWithString:AppDownLoadLink];
    content.contentTitle = JMOLocalizedString(@"invite_to_download__subject", nil);
    content.contentDescription = JMOLocalizedString(@"invite_to_download__content", nil);
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:self];
}
-(IBAction)twitterButtonDidPress:(id)sender{
    NSString *imageURL = [self getShareImageURL:self.AgentProfile.AgentListing];
    NSString *shareString = [self getShareString:self.AgentProfile.AgentListing];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:imageURL]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                TWTRComposer *composer = [[TWTRComposer alloc] init];
                                [composer setText:shareString];
                                [composer setImage:image];
                                [composer setURL:[NSURL URLWithString:AppDownLoadLink]];
                                // Called from a UIViewController
                                [composer showWithCompletion:^(TWTRComposerResult result) {
                                    if (result == TWTRComposerResultCancelled) {
                                        [self showShareStatusAlert:NO];
                                    }
                                    else {
                                        [self showShareStatusAlert:YES];
                                    }
                                }];
                            }
                        }];

}

- (IBAction)closeButtonDidPress:(id)sender {
    //    [self btnBackPressed:sender];
    [self.delegate dismissOverlayerViewController:self];
}


#pragma mark -
#pragma mark  Facebook delegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    [self showShareStatusAlert:YES];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
   [self showShareStatusAlert:NO];
}

/*!
 @abstract Sent to the delegate when the sharer is cancelled.
 @param sharer The FBSDKSharing that completed.
 */
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    
}


#pragma mark -
#pragma mark GeneralMethod

-(void)showShareStatusAlert:(BOOL)success{
    NSString *shareTitle = @"";
    if (success) {
        shareTitle = @"Share Success";
    }else{
        shareTitle = @"Share Fail,Please try again later";
    }
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:shareTitle message:nil delegate:nil cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil) otherButtonTitles:nil];
    alertView.didDismissBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        [self.delegate dismissOverlayerViewController:self];
    };
}
-(NSString*)getShareString:(AgentListing*)listing{
//    NSString *priceString =  listing.PropertyPriceFormattedForRoman;
//    NSString *sizeString = [NSString stringWithFormat:@"%d %@",listing.PropertySize,[[SystemSettingModel shared]getMetricNativeNameBySizeUnitType:listing.SizeUnitType]];
//    NSString *bedCount = [NSString stringWithFormat:@"%d",listing.BedroomCount];
//    NSString *bathroomCount = [NSString stringWithFormat:@"%d",listing.BathroomCount];
//    NSString *apartmentType = [[SystemSettingModel shared]getSpaceTypeByPropertyType:listing.PropertyType spaceType:listing.SpaceType];
//    //        AddressUserInput *addressuserinput = [listing.AddressUserInput firstObject];
//    GoogleAddress *googleAddress = [listing.GoogleAddress firstObject];
//    NSString *address = googleAddress.FormattedAddress;
//    NSString *name = googleAddress.Name;
    
    NSString *shareString = JMOLocalizedString(@"invite_to_download__content", nil);
    return shareString;
}

-(NSString*)getShareImageURL:(AgentListing*)listing{
    Photos *firstPhoto = [listing.Photos firstObject];
    NSString *imageURL = firstPhoto.URL;
    return imageURL;
}
@end
