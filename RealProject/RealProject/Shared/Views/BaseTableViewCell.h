//
//  BaseTableViewCell.h
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

- (void)setup;

- (void)setupConstraints;

+ (NSString *)reuseIdentifier;

@end
