//
//  RealArrowCell.m
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealArrowCell.h"

@implementation RealArrowCell

#pragma mark - Setup

- (void)setup
{
    [super setup];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self setupLeftLabel];
    [self setupRightLabel];
    [self setupRightImageView];
    [self setupSeparatorView];
}

- (void)setupLeftLabel
{
    _leftLabel = [UILabel newAutoLayoutView];
    
    _leftLabel.textColor = [UIColor white];
    
    [self.contentView addSubview:_leftLabel];
}

- (void)setupRightLabel
{
    _rightLabel = [UILabel newAutoLayoutView];
    
    _rightLabel.textColor = [UIColor settingsCellGreyTextColor];
    
    [self.contentView addSubview:_rightLabel];
    
}

- (void)setupRightImageView
{
    _rightImageView = [UIImageView newAutoLayoutView];
    
    _rightImageView.image = [UIImage imageNamed:@"btn_setting_detail"];
    
    [self.contentView addSubview:_rightImageView];
}

- (void)setupSeparatorView
{
    _separatorView = [UIDotImageView newAutoLayoutView];
    
    [self.contentView addSubview:_separatorView];
}

#pragma mark - Setup Constraints

- (void)setupConstraints
{
    [super setupConstraints];
    
    [self setupLeftLabelConstraints];
    [self setupRightLabelConstraints];
    [self setupRightImageViewConstraints];
    [self setupSeparatorViewConstraints];
}

- (void)setupLeftLabelConstraints
{
    [_leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:25.0];
    [_leftLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
}

- (void)setupRightLabelConstraints
{
    [_rightLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_leftLabel withOffset:5.0 relation:NSLayoutRelationGreaterThanOrEqual];
    [_rightLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
}

- (void)setupRightImageViewConstraints
{
    [_rightImageView autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_rightLabel withOffset:5.0];
    [_rightImageView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:25.0];
    [_rightImageView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
}

- (void)setupSeparatorViewConstraints
{
    [_separatorView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [_separatorView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:20.0];
    [_separatorView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:20.0];
    [_separatorView autoSetDimension:ALDimensionHeight toSize:1.0];
}

@end
