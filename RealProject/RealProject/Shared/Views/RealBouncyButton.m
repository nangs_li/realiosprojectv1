//
//  RealBouncyButton.m
//  productionreal2
//
//  Created by Derek Cheung on 3/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealBouncyButton.h"

@interface RealBouncyButton ()

@property (nonatomic, assign) BOOL isShowingTouchDownAnimation;
@property (nonatomic, assign) BOOL needToShowTouchUpAnimation;
@property (nonatomic, assign) BOOL isCancel;
@end

@implementation RealBouncyButton

#pragma mark - Initialization

- (instancetype)init
{
    if (self = [super init]) {
        
        [self addTarget:self action:@selector(buttonDidTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(buttonDidTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(buttonDidTouchCancel:) forControlEvents:UIControlEventTouchCancel];
    }
    return self;
}

- (void)awakeFromNib
{
    self.adjustsImageWhenHighlighted = NO;
    
    [self addTarget:self action:@selector(buttonDidTouchDown:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(buttonDidTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(buttonDidTouchCancel:) forControlEvents:UIControlEventTouchCancel];
}

#pragma mark - Actions

- (void)buttonDidTouchDown:(UIButton *)sender
{
    [self touchDownAnimation];
}

- (void)buttonDidTouchUpInside:(UIButton *)sender
{
    _isCancel = NO;
    [self touchUpAnimation];
}

- (void)buttonDidTouchCancel:(UIButton *)sender
{
    _isCancel = YES;
    [self touchUpAnimation];
}

#pragma mark - Helpers

- (void)touchDownAnimation
{
    _isShowingTouchDownAnimation = YES;
    
    [UIView animateWithDuration:0.1
                          delay:0
         usingSpringWithDamping:1.0
          initialSpringVelocity:1.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.transform = CGAffineTransformMakeScale(0.95, 0.95);
     }
                     completion:^(BOOL finished)
     {
         _isShowingTouchDownAnimation = NO;
         
         if (_needToShowTouchUpAnimation) {
             
             [self touchUpAnimation];
         }
     }];
}

- (void)touchUpAnimation
{
    if (_isShowingTouchDownAnimation == YES) {
        
        _needToShowTouchUpAnimation = YES;
        
    } else {
    
        [UIView animateWithDuration:0.1
                              delay:0
             usingSpringWithDamping:1.0
              initialSpringVelocity:1.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^
         {
             self.transform = CGAffineTransformMakeScale(1.05, 1.05);
         }
                         completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.1
                                   delay:0
                  usingSpringWithDamping:1.0
                   initialSpringVelocity:1.0
                                 options:UIViewAnimationOptionCurveEaseInOut
                              animations:^
              {
                  self.transform = CGAffineTransformMakeScale(1.0, 1.0);
              }
                              completion:^(BOOL finished)
              {
                  _needToShowTouchUpAnimation = NO;
                  [self notifyDelegate];
              }];
         }];
    }
}

#pragma mark - Helper

- (void)notifyDelegate
{
    if ([_delegate respondsToSelector:@selector(buttonDidPressed:)]) {
        
        if (!_isCancel) {
            
            [_delegate buttonDidPressed:self];
        }
    }
}

#pragma mark - Override

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:NO];
}

@end
