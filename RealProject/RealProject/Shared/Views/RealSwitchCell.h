//
//  RealSwitchCell.h
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseTableViewCell.h"

// Views
#import "UIDotImageView.h"

@interface RealSwitchCell : BaseTableViewCell

@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UIView *rightIndicatorView;
@property (nonatomic, strong) UIDotImageView *separatorView;

@end
