//
//  RealSwitchCell.m
//  productionreal2
//
//  Created by Derek Cheung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealSwitchCell.h"

@implementation RealSwitchCell

#pragma mark - Setup

- (void)setup
{
    [super setup];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self setupLeftLabel];
    [self setupRightIndicatorView];
    [self setupSeparatorView];
}

- (void)setupLeftLabel
{
    _leftLabel = [UILabel newAutoLayoutView];
    
    _leftLabel.textColor = [UIColor white];
    
    [self.contentView addSubview:_leftLabel];
}

- (void)setupRightIndicatorView
{
    _rightIndicatorView = [UIView newAutoLayoutView];
    
    _rightIndicatorView.backgroundColor = RealBlueColor;
    
    [self.contentView addSubview:_rightIndicatorView];
}

- (void)setupSeparatorView
{
    _separatorView = [UIDotImageView newAutoLayoutView];
    
    [self.contentView addSubview:_separatorView];
}

#pragma mark - Setup Constraints

- (void)setupConstraints
{
    [super setupConstraints];
    
    [self setupLeftLabelConstraints];
    [self setupRightIndicatorViewConstraints];
    [self setupSeparatorViewConstraints];
}

- (void)setupLeftLabelConstraints
{
    [_leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:25.0];
    [_leftLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
}

- (void)setupRightIndicatorViewConstraints
{
    [_rightIndicatorView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:25.0];
    [_rightIndicatorView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [_rightIndicatorView autoSetDimensionsToSize:CGSizeMake(15.0, 15.0)];
}

- (void)setupSeparatorViewConstraints
{
    [_separatorView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [_separatorView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:20.0];
    [_separatorView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:20.0];
    [_separatorView autoSetDimension:ALDimensionHeight toSize:1.0];
}

@end
