//
//  SeparatorLabelView.h
//  productionreal2
//
//  Created by Derek Cheung on 29/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseView.h"

@interface SeparatorLabelView : BaseView

+ (SeparatorLabelView *)viewWithText:(NSString *)text;

@end
