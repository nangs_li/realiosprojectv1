//
//  SeparatorLabelView.m
//  productionreal2
//
//  Created by Derek Cheung on 29/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "SeparatorLabelView.h"

@interface SeparatorLabelView ()

@property (nonatomic, strong) UIView *leftSeparatorLineView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIView *rightSeparatorLineView;

@end

@implementation SeparatorLabelView

#pragma mark - Factory

+ (SeparatorLabelView *)viewWithText:(NSString *)text
{
    SeparatorLabelView *view = [[SeparatorLabelView alloc] init];
    view.label.text = text;
    
    return view;
}

#pragma mark - Initialization

- (instancetype)init
{
    if (self = [super init]) {
        
        [self setup];
        [self setupConstraints];
    }
    return self;
}

#pragma mark - Setup

- (void)setup
{
    [self setupLeftSeparatorLineView];
    [self setupLabel];
    [self setupRightSeparatorLineView];
}

- (void)setupLeftSeparatorLineView
{
    _leftSeparatorLineView = [[UIView alloc] init];
    
    [_leftSeparatorLineView setBackgroundColor:[UIColor white]];
    
    [self addSubview:_leftSeparatorLineView];
}

- (void)setupLabel
{
    _label = [[UILabel alloc] init];
    
    [_label setTextColor:[UIColor white]];
    [_label setFont:[UIFont systemFontOfSize:12]];
    
    [self addSubview:_label];
}

- (void)setupRightSeparatorLineView
{
    _rightSeparatorLineView = [[UIView alloc] init];
    
    [_rightSeparatorLineView setBackgroundColor:[UIColor white]];
    
    [self addSubview:_rightSeparatorLineView];
}

#pragma mark - Setup Constraints

- (void)setupConstraints
{
    [self setupLeftSeparatorLineViewConstraints];
    [self setupLabelConstraints];
    [self setupRightSeparatorLineViewConstraints];
}

- (void)setupLeftSeparatorLineViewConstraints
{
    _leftSeparatorLineView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_leftSeparatorLineView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [_leftSeparatorLineView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [_leftSeparatorLineView autoSetDimension:ALDimensionHeight toSize:1.0];
}

- (void)setupLabelConstraints
{
    _label.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_label autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_leftSeparatorLineView withOffset:5.0];
    [_label autoCenterInSuperview];
}

- (void)setupRightSeparatorLineViewConstraints
{
    _rightSeparatorLineView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_rightSeparatorLineView autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_label withOffset:5.0];
    [_rightSeparatorLineView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [_rightSeparatorLineView autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [_rightSeparatorLineView autoSetDimension:ALDimensionHeight toSize:1.0];
}

@end
