//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Sharepage.h"

@interface Sharepage ()

@end
@implementation Sharepage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}
- (IBAction)btnBackPressed:(id)sender {
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionPush;
  transition.subtype = kCATransitionFromBottom;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];
  [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)btnFacebookSharePressed:(id)sender {
  DDLogInfo(@"btnFacebookSharePressed");
}
- (IBAction)btnTwitterSharePressed:(id)sender {
  DDLogInfo(@"btnTwitterSharePressed");
}
- (IBAction)btnGooglePlusSharePressed:(id)sender {
  DDLogInfo(@"btnGooglePlusSharePressed");
}
- (IBAction)btnWeiboSharePressed:(id)sender {
  DDLogInfo(@"btnWeiboSharePressed");
}

- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
  [super viewDidLoad];
}

@end