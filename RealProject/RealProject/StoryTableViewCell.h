//
//  StoryTableViewCell.h
//  productionreal2
//
//  Created by Alex Hung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryTableViewCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UIImageView *storyTypeImageView;
@property (nonatomic,strong) IBOutlet UILabel *storyNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *storyAddressLabel;
@property (nonatomic,strong) IBOutlet UIImageView *separatorImageView;

- (void)configureCell:(AgentListing*)listing separatorImage:(UIImage*)separatorImage;

@end
