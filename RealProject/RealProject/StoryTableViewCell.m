//
//  StoryTableViewCell.m
//  productionreal2
//
//  Created by Alex Hung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "StoryTableViewCell.h"

@implementation StoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(AgentListing*)listing separatorImage:(UIImage*)separatorImage{
    GoogleAddress *googleAddress = [listing.GoogleAddress firstObject];
    NSString *name = googleAddress.Name;
    NSString *address = googleAddress.FormattedAddress;
    NSString *typeImageName = [[SystemSettingModel shared]getPropertyImageByPropertyType:listing.PropertyType spaceType:listing.SpaceType state:@"on"];
    self.storyNameLabel.text = name;
    self.storyAddressLabel.text = address;
    self.storyTypeImageView.image = [UIImage imageNamed:typeImageName];
    self.separatorImageView.image = separatorImage;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
@end
