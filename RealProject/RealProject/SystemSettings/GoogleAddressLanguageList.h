//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@protocol GoogleAddressLanguageList
@end

@interface GoogleAddressLanguageList : JSONModel
//+ (d *)shared;

@property(strong, nonatomic) NSString* Country;
@property(strong, nonatomic) NSArray * Lang;
@end
