//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "GoogleAddressLanguageList.h"

@implementation GoogleAddressLanguageList
static GoogleAddressLanguageList *sharedGoogleAddressLanguageList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (GoogleAddressLanguageList *)shared {
  @synchronized(self) {
    if (!sharedGoogleAddressLanguageList) {
      sharedGoogleAddressLanguageList = [[GoogleAddressLanguageList alloc] init];
    }
    return sharedGoogleAddressLanguageList;
  }
}

@end