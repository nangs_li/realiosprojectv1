//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LegalStatementGetModel.h"

#import "HandleServerReturnString.h"

@implementation LegalStatementGetModel

static LegalStatementGetModel *sharedLegalStatementGetModel;




+ (LegalStatementGetModel *)shared {
    @synchronized(self) {
        if (!sharedLegalStatementGetModel) {
            sharedLegalStatementGetModel = [[LegalStatementGetModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedLegalStatementGetModel;
    }
}


-(void)callLegalStatementGetModelApiByMessageType:(NSString *)messageType success:(void (^)(NSString * message))success
                                          failure:(failureBlock) failure{
    NSString *urlLink = [NSString
                         stringWithFormat:@"%@%@", kServerAddress,
                         @"/Operation.asmx/LegalStatementGet"];
    NSString *memberID = @"0";
    if ([LoginBySocialNetworkModel shared].myLoginInfo.MemberID > 0) {
        memberID = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue];
    }
    NSString *accessToken =[LoginBySocialNetworkModel shared].myLoginInfo.AccessToken?[LoginBySocialNetworkModel shared].myLoginInfo.AccessToken:@"";
    NSString *langCode = [SystemSettingModel shared].selectSystemLanguageList.Code?[SystemSettingModel shared].selectSystemLanguageList.Code:@"en";
    NSString *parameters = [NSString
                            stringWithFormat:@"{\"MemberID\":\"%@\",\"AccessToken\":\"%@\","
                            @"\"MessageType\":\"%@\",\"CountryCode\":\"%@\","
                            @"\"LanguageCode\":\"%@\"}",memberID,accessToken, messageType, @"w",@"en", nil];
    
    
    AFHTTPRequestOperationManager *manager =
    [self getAPiManager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (![self networkConnection]) {
        failure(nil, nil,JMOLocalizedString(NotNetWorkConnectionText, nil),NotNetWorkConnection,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
        return;
    }
    DDLogVerbose(@"apiurlstring-->:%@", urlLink);
    DDLogVerbose(@"InputJson-->:%@", parameters);
    [manager POST:urlLink
       parameters:@{ @"InputJson" : parameters }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSString *dstring = [responseObject objectForKey:@"d"];
              if ([self checkAccessError:dstring]) {
                  failure(operation, nil,JMOLocalizedString(ThisAccountIsInUseOnAnotherDevice, nil),AccessErrorOtherUserLoginThisAccount,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
                  
              }
              NSError *err = nil;
              self.HandleServerReturnString =[[HandleServerReturnString alloc] initWithString:dstring error:&err];
              
              
              
              if (self.HandleServerReturnString.ErrorCode == 0) {
                  success( [self.HandleServerReturnString.Content objectForKey:@"Message"]);
              }else{
                  failure(operation, [self genError:self.HandleServerReturnString.ErrorCode description:self.HandleServerReturnString.ErrorMsg], self.HandleServerReturnString.ErrorMsg, error, JMOLocalizedString(@"alert_alert", nil),
                          JMOLocalizedString(@"alert_ok", nil));
              }
              
              
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              failure(operation, error,JMOLocalizedString(@"error_message__server_not_response", nil),RealServerNotReponse,JMOLocalizedString(@"alert_alert", nil),JMOLocalizedString(@"alert_ok", nil));
              NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
              DDLogError(@"operation.request.HTTPBody-->%@", newStr);
              DDLogError(@"response-->Error: %@", error);
              
          }];
    
    
    
}


@end