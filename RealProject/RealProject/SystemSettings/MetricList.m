//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "MetricList.h"

@implementation MetricList
static MetricList *sharedMetricList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (MetricList *)shared {
  @synchronized(self) {
    if (!sharedMetricList) {
      sharedMetricList = [[MetricList alloc] init];
    }
    return sharedMetricList;
  }
}

@end