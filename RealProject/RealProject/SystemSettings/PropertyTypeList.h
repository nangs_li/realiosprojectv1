//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

#import "SpaceTypeList.h"
@protocol PropertyTypeList
@end

@interface PropertyTypeList : JSONModel
//+ (d *)shared;
@property(assign, nonatomic) int Index;
@property(assign, nonatomic) int LanguageIndex;
@property(assign, nonatomic) int Position;
@property(strong, nonatomic) NSString* PropertyTypeName;
@property(strong, nonatomic) NSArray<SpaceTypeList>* SpaceTypeList;

//-(LoginInfo *)initwithjsonstring:(NSString*)content:(int)error;
//-(LoginInfo *)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
