//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "PropertyTypeList.h"

@implementation PropertyTypeList
static PropertyTypeList *sharedPropertyTypeList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (PropertyTypeList *)shared {
  @synchronized(self) {
    if (!sharedPropertyTypeList) {
      sharedPropertyTypeList = [[PropertyTypeList alloc] init];
    }
    return sharedPropertyTypeList;
  }
}

@end