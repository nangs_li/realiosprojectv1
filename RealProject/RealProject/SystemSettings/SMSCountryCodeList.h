//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@protocol SMSCountryCodeList
@end

@interface SMSCountryCodeList : JSONModel
//+ (d *)shared;


@property(strong, nonatomic) NSString* k;
@property(strong, nonatomic) NSString* v;
//-(SpokenLanguageList *)initwithjsonstring:(NSString*)content:(int)error;
//-(SpokenLanguageList *)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
