//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SMSCountryCodeList.h"

@implementation SMSCountryCodeList
static SMSCountryCodeList *sharedSMSCountryCodeList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (SMSCountryCodeList *)shared {
  @synchronized(self) {
    if (!sharedSMSCountryCodeList) {
      sharedSMSCountryCodeList = [[SMSCountryCodeList alloc] init];
    }
    return sharedSMSCountryCodeList;
  }
}

@end