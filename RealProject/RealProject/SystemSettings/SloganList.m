//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SloganList.h"

@implementation SloganList
static SloganList *sharedSloganList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (SloganList *)shared {
  @synchronized(self) {
    if (!sharedSloganList) {
      sharedSloganList = [[SloganList alloc] init];
    }
    return sharedSloganList;
  }
}

@end