//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SpaceTypeList.h"

@implementation SpaceTypeList
static SpaceTypeList *sharedSpaceTypeList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

+ (SpaceTypeList *)shared {
    @synchronized(self) {
        if (!sharedSpaceTypeList) {
            sharedSpaceTypeList = [[SpaceTypeList alloc] init];
        }
        return sharedSpaceTypeList;
    }
}

+ (SpaceTypeList*)defaultTypeWithPropertType:(int)propertType{
    SpaceTypeList *defaultType = [[SpaceTypeList alloc]init];
    if (propertType == 1) {
        defaultType.Index = 10;
    }else{
        defaultType.Index = 9;
    }
    defaultType.SpaceTypeName = @"Others";
    return  defaultType;
}

@end