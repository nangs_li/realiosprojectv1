//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@protocol SpokenLanguageList
@end

@interface SpokenLanguageList : JSONModel
//+ (d *)shared;

@property(assign, nonatomic) int Index;
@property(assign, nonatomic) int Position;
@property(strong, nonatomic) NSString* Language;
@property(strong, nonatomic) NSString* NativeName;
//-(SpokenLanguageList *)initwithjsonstring:(NSString*)content:(int)error;
//-(SpokenLanguageList *)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
