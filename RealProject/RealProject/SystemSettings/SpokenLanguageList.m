//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SpokenLanguageList.h"

@implementation SpokenLanguageList
static SpokenLanguageList *sharedSpokenLanguageList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (SpokenLanguageList *)shared {
  @synchronized(self) {
    if (!sharedSpokenLanguageList) {
      sharedSpokenLanguageList = [[SpokenLanguageList alloc] init];
    }
    return sharedSpokenLanguageList;
  }
}

@end