//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@protocol SystemLanguageList
@end
@interface SystemLanguageList : JSONModel
//+ (d *)shared;
@property(assign, nonatomic) int Index;
@property(assign, nonatomic) int Position;
@property(strong, nonatomic) NSString* Language;
@property(strong, nonatomic) NSString* Code;
@property(strong, nonatomic) NSString* NativeName;

//-(LoginInfo *)initwithjsonstring:(NSString*)content:(int)error;
//-(LoginInfo *)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
