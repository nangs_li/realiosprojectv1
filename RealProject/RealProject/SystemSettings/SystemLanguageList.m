//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SystemLanguageList.h"

@implementation SystemLanguageList
static SystemLanguageList *sharedSystemLanguageList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (SystemLanguageList *)shared {
  @synchronized(self) {
    if (!sharedSystemLanguageList) {
      sharedSystemLanguageList = [[SystemLanguageList alloc] init];
    }
    return sharedSystemLanguageList;
  }
}

@end