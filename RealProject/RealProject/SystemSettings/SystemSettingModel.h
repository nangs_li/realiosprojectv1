//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"
@protocol SystemSettingModel
@end
enum CommercialSpaceType : NSUInteger {
  CommericalOffice = 0,
  CommericalIndustrial,
  CommericalWarehouse,
  CommericalFlexSpace,
  CommericalRetail,
  CommericalLand,
  CommericalAgricultural,
  CommericalHotelAndMotel,
  CommericalHealthCare,
  CommericalSpecialPurpose,
  CommericalOther
};

enum ResidentialSpaceType : NSUInteger {
  ResidentialApartment = 0,
  ResidentialUnit,
  ResidentialTownhouse,
  ResidentialLoft,
  ResidentialHouseboat,
  ResidentialVilla,
  ResidentialLand,
  ResidentialCondos,
  ResidentialSuburbanHome,
  ResidentialOther
};

enum PropertyType : NSUInteger {
  PropertyTypeResidential,
  PropertyTypeCommerical
};

@interface SystemSettingModel : BaseModel
+ (SystemSettingModel *)shared;
@property(strong, nonatomic) SystemSettings *SystemSettings;
@property(strong, nonatomic) SystemLanguageList *selectSystemLanguageList;
@property(strong, nonatomic)
    NSMutableArray<SpokenLanguageList> *spokenlanguagelist;
@property(strong, nonatomic)
    NSMutableArray<PropertyTypeList> *propertytypeaccordingtolanguage;
@property(strong, nonatomic)
    NSMutableArray<SpaceTypeList> *typeofspaceoneaccordingtolanguage;
@property(strong, nonatomic)
    NSMutableArray<SpaceTypeList> *typeofspacetwoaccordingtolanguage;
@property(strong, nonatomic)
    NSMutableArray<MetricList> *metricaccordingtolanguage;
@property(strong, nonatomic) NSMutableDictionary *filterDict;
@property(strong, nonatomic) NSMutableDictionary *cachedSloganDict;
#pragma mark set Up Data According To Lanuage Method
- (void)setupPropertyTypeAccordingToLanguage;
- (void)setupSpokenLanguageList;
- (void)setupMetricAccordingToLanguage;
- (NSString *)getMetricNativeNameBySizeUnitType:(int)sizeUnitType;
- (NSString *)getSpaceTypeByPropertyType:(int)propertyType
                               spaceType:(int)spaceType;
- (NSString *)getPropertyImageByPropertyType:(int)propertyType
                                  spaceType:(int)spaceType
                                      state:(NSString *)state;
- (NSString *)getMiniViewPropertyImageByPropertyType:(int)propertyType
                                           spaceType:(int)spaceType;
- (NSArray*)getSpaceFilterArray;
-(NSString *)getSloganByIndex:(int)index;

-(NSArray*)getSpokenLanguageStringList;
-(int)getSpokenLanguageIndex:(NSString*)nativeName;
-(NSString*)getSpokenLanguageString:(NSNumber*)index;

-(NSArray*)getSettingLanguageStringList;
-(int)getSettingLanguageIndex:(NSString*)nativeName;
-(NSString*)getSettingLanguageString:(NSNumber*)index;

-(NSString*)getSloganStringByLangIndex:(NSNumber*)langIndex position:(NSNumber*)position;
-(NSArray*)getSloganListByLangIndex:(NSNumber*)langIndex;
-(NSArray*)getSloganStringListByLangIndex:(NSNumber*)langIndex;

-(NSString*)getUserCurrencyUnit;
-(NSString*)serverReturnLanguageCodeChangeToAppleFormat:(NSString *)code;
-(NSLocale*)getCurrentLocale;
-(NSMutableArray*)getNotDuplicateSMSCountryCodeList;
-(int)getIndexFromNotDuplicateSMSCountryCodeListAccordingToCountry;
@end

//@implementation Photos

//@end