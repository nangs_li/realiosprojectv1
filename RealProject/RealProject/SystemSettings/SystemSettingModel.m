//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SystemSettingModel.h"
#import "RealUtility.h"
#import "GetContryLocation.h"
@implementation SystemSettingModel

static SystemSettingModel *sharedSystemSettingModel;

+ (SystemSettingModel *)shared {
    @synchronized(self) {
        if (!sharedSystemSettingModel) {
            sharedSystemSettingModel = [[SystemSettingModel alloc] init];
            [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
        return sharedSystemSettingModel;
    }
}
//// setupspokenlanguagelist
- (void)setupSpokenLanguageList {
    self.spokenlanguagelist = [[NSMutableArray alloc] init];
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"Position"
                                                                ascending: YES];

    self.SystemSettings.SpokenLanguageList =(NSMutableArray<SpokenLanguageList> *)[self.SystemSettings.SpokenLanguageList sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
    for (SpokenLanguageList *spokenlanguagelist in self.SystemSettings
         .SpokenLanguageList) {
        [self.spokenlanguagelist addObject:spokenlanguagelist];
    }
    //  DDLogInfo(@"self.spokenlanguagelist-->%@",self.spokenlanguagelist);
}
//// setuptypeofspacepickerdata
- (void)setupPropertyTypeAccordingToLanguage {
    self.propertytypeaccordingtolanguage = nil;
    self.propertytypeaccordingtolanguage = [[NSMutableArray alloc] init];
    self.typeofspaceoneaccordingtolanguage = nil;
    self.typeofspacetwoaccordingtolanguage = nil;
    
    for (PropertyTypeList *propertytype in self.SystemSettings.PropertyTypeList) {
        if (propertytype.LanguageIndex == self.selectSystemLanguageList.Index) {
            if ([self isEmpty:self.self.typeofspaceoneaccordingtolanguage]) {
                [self.propertytypeaccordingtolanguage addObject:propertytype];
                self.typeofspaceoneaccordingtolanguage = [[NSMutableArray alloc] init];
                
                for (SpaceTypeList *spacetypelist in propertytype.SpaceTypeList) {
                    [self.typeofspaceoneaccordingtolanguage addObject:spacetypelist];
                }
            } else if ([self isEmpty:self.typeofspacetwoaccordingtolanguage]) {
                [self.propertytypeaccordingtolanguage addObject:propertytype];
                self.typeofspacetwoaccordingtolanguage = [[NSMutableArray alloc] init];
                for (SpaceTypeList *spacetypelist in propertytype.SpaceTypeList) {
                    [self.typeofspacetwoaccordingtolanguage addObject:spacetypelist];
                }
            }
        }
    }
    // DDLogInfo(@"self.typeofspaceoneaccordingtolanguage-->%@",self.typeofspaceoneaccordingtolanguage);
    // DDLogInfo(@"self.typeofspacetwoaccordingtolanguage-->%@",self.typeofspacetwoaccordingtolanguage);
    // DDLogInfo(@"self.propertytypeaccordingtolanguage-->%@",self.propertytypeaccordingtolanguage);
}
//// setuptypeofspacepickerdata
- (void)setupMetricAccordingToLanguage {
    self.metricaccordingtolanguage = nil;
    self.metricaccordingtolanguage = [[NSMutableArray alloc] init];
    for (MetricList *metric in self.SystemSettings.MetricList) {
        if (metric.LanguageIndex == self.selectSystemLanguageList.Index) {
            [self.metricaccordingtolanguage addObject:metric];
        }
    }
}

- (NSString *)getMetricNativeNameBySizeUnitType:(int)sizeUnitType {
    NSString *nativeName = JMOLocalizedString(@"common__sq_ft", nil);
    if (![self isEmpty:[SystemSettingModel shared].metricaccordingtolanguage]) {
        for (MetricList *metric in [SystemSettingModel shared].metricaccordingtolanguage) {
            if (metric.Index  == sizeUnitType) {
                nativeName = metric.NativeName;
                break;
            }
        }
    }
    return nativeName;
}

- (SpaceTypeList*)getSpaceTypeListByPropertyType:(int)propertyType spaceType:(int)spaceType{
    SpaceTypeList *spaceTypeItem = [SpaceTypeList defaultTypeWithPropertType:propertyType];
    NSArray *spaceTypeArray = nil;
    if (propertyType == 0) {
        spaceTypeArray = [SystemSettingModel shared].typeofspaceoneaccordingtolanguage;
    } else if (propertyType == 1) {
        spaceTypeArray = [SystemSettingModel shared].typeofspacetwoaccordingtolanguage;
    }
    for (SpaceTypeList *type in spaceTypeArray) {
        if (type.Index == spaceType) {
            spaceTypeItem = type;
        }
    }
    return spaceTypeItem;
}

- (NSString *)getSpaceTypeByPropertyType:(int)propertyType
                               spaceType:(int)spaceType {
    
    SpaceTypeList *spaceTypeItem = [self getSpaceTypeListByPropertyType:propertyType spaceType:spaceType];
    return spaceTypeItem.SpaceTypeName;
}

-(NSArray*) getSpaceFilterArray{
    NSArray *spaceOneTitleArray = [SystemSettingModel shared]
    .typeofspaceoneaccordingtolanguage;
    
    NSMutableArray *filterArray = [[NSMutableArray alloc]init];
    
    for (int i = 0 ; i< spaceOneTitleArray.count; i++) {
        SpaceTypeList *space = spaceOneTitleArray[i];
        int spaceType = space.Index;
        NSString *title = [self getSpaceTypeByPropertyType:0 spaceType:spaceType];
        NSString *normalImage = [self getPropertyImageByPropertyType:0 spaceType:spaceType state:@"off"];
        NSString *highlightImage = [self getPropertyImageByPropertyType:0 spaceType:spaceType state:@"on"];
        [filterArray addObject:[self createFilterDictWithTitle:title withPropertyType:0 withSpaceType:spaceType withNormalImage:normalImage withHighlightImage:highlightImage]];
    }
    
    NSArray *spaceTwoTitleArray = [SystemSettingModel shared].typeofspacetwoaccordingtolanguage;
    for (int i = 0 ; i<spaceTwoTitleArray.count; i++) {
        SpaceTypeList *space = spaceTwoTitleArray[i];
        int spaceType = space.Index;
        NSString *title = [self getSpaceTypeByPropertyType:1 spaceType:spaceType];
        NSString *normalImage = [self getPropertyImageByPropertyType:1 spaceType:spaceType state:@"off"];
        NSString *highlightImage = [self getPropertyImageByPropertyType:1 spaceType:spaceType state:@"on"];
        [filterArray addObject:[self createFilterDictWithTitle:title withPropertyType:1 withSpaceType:spaceType withNormalImage:normalImage withHighlightImage:highlightImage]];
    }
    
    return filterArray;
}


-(NSDictionary*)createFilterDictWithTitle:(NSString*)title withPropertyType:(int)propertyType withSpaceType:(int)spaceType withNormalImage:(NSString*)normalImage withHighlightImage:(NSString*)hightlightImage{
    return @{@"title":title,@"propertyType":@(propertyType),@"spaceType":@(spaceType),@"normalImage":normalImage,@"highlightImage":hightlightImage};
}

- (NSString *)getPropertyImageByPropertyType:(int)propertyType
                                   spaceType:(int)spaceType
                                       state:(NSString *)state {
    SpaceTypeList *spaceTypeItem = [self getSpaceTypeListByPropertyType:propertyType spaceType:spaceType];
    NSString *imageName = @"ico_type_space_others";
    if (propertyType == 0) {
        enum ResidentialSpaceType residentialSpaceType = (int)spaceTypeItem.Index;
        switch (residentialSpaceType) {
            case ResidentialApartment: {
                imageName = @"ico_type_space_apartment";
                break;
            }
            case ResidentialCondos: {
                imageName = @"ico_type_space_condos";
                break;
            }
            case ResidentialHouseboat: {
                imageName = @"ico_type_space_houseboat";
                break;
            }
            case ResidentialLand: {
                imageName = @"ico_type_space_land";
                break;
            }
            case ResidentialLoft: {
                imageName = @"ico_type_space_loft";
                break;
            }
            case ResidentialOther: {
                imageName = @"ico_type_space_others";
                break;
            }
            case ResidentialSuburbanHome: {
                imageName = @"ico_type_space_suburban";
                break;
            }
            case ResidentialTownhouse: {
                imageName = @"ico_type_space_townhouse";
                break;
            }
            case ResidentialUnit: {
                imageName = @"ico_type_space_unit";
                break;
            }
            case ResidentialVilla: {
                imageName = @"ico_type_space_villa";
                break;
            }
            default:
                break;
        }
    } else if (propertyType == 1) {
        enum CommercialSpaceType commericalSpaceType = spaceType;
        switch (commericalSpaceType) {
            case CommericalAgricultural: {
                imageName = @"ico_type_space_agricultural";
                break;
            }
            case CommericalFlexSpace: {
                imageName = @"ico_type_space_flex";
                break;
            }
            case CommericalHealthCare: {
                imageName = @"ico_type_space_health";
                break;
            }
            case CommericalHotelAndMotel: {
                imageName = @"ico_type_space_hotel";
                break;
            }
            case CommericalIndustrial: {
                imageName = @"ico_type_space_industrial";
                break;
            }
            case CommericalLand: {
                imageName = @"ico_type_space_land";
                break;
            }
            case CommericalOffice: {
                imageName = @"ico_type_space_office";
                break;
            }
            case CommericalOther: {
                imageName = @"ico_type_space_others";
                break;
            }
            case CommericalRetail: {
                imageName = @"ico_type_space_retail";
                break;
            }
            case CommericalSpecialPurpose: {
                imageName = @"ico_type_space_special";
                break;
            }
            case CommericalWarehouse: {
                imageName = @"ico_type_space_warehouse";
                break;
            }
            default:
                break;
        }
    }
    if (!state) {
    } else if ([state isEqualToString:@"on"]) {
        imageName = [imageName stringByAppendingString:@"_on"];
    } else if ([state isEqualToString:@"off"]) {
        imageName = [imageName stringByAppendingString:@"_off"];
    }
    //    propertyImage = [UIImage imageNamed:imageName];
    
    return imageName;
}

- (NSString *)getMiniViewPropertyImageByPropertyType:(int)propertyType
                                           spaceType:(int)spaceType{
    SpaceTypeList *spaceTypeItem = [self getSpaceTypeListByPropertyType:propertyType spaceType:spaceType];
        NSString *imageName = nil;
    if (propertyType == 0) {
        enum ResidentialSpaceType residentialSpaceType = spaceTypeItem.Index;
        switch (residentialSpaceType) {
            case ResidentialApartment: {
                imageName = @"ico_type_mini_apartment";
                break;
            }
            case ResidentialCondos: {
                imageName = @"ico_type_mini_condos";
                break;
            }
            case ResidentialHouseboat: {
                imageName = @"ico_type_mini_houseboat";
                break;
            }
            case ResidentialLand: {
                imageName = @"ico_type_mini_land";
                break;
            }
            case ResidentialLoft: {
                imageName = @"ico_type_mini_loft";
                break;
            }
            case ResidentialOther: {
                imageName = @"ico_type_mini_others";
                break;
            }
            case ResidentialSuburbanHome: {
                imageName = @"ico_type_mini_suburban";
                break;
            }
            case ResidentialTownhouse: {
                imageName = @"ico_type_mini_townhouse";
                break;
            }
            case ResidentialUnit: {
                imageName = @"ico_type_mini_unit";
                break;
            }
            case ResidentialVilla: {
                imageName = @"ico_type_mini_villa";
                break;
            }
            default:
                break;
        }
    } else if (propertyType == 1) {
        enum CommercialSpaceType commericalSpaceType = spaceType;
        switch (commericalSpaceType) {
            case CommericalAgricultural: {
                imageName = @"ico_type_mini_agricultural";
                break;
            }
            case CommericalFlexSpace: {
                imageName = @"ico_type_mini_flex";
                break;
            }
            case CommericalHealthCare: {
                imageName = @"ico_type_mini_health";
                break;
            }
            case CommericalHotelAndMotel: {
                imageName = @"ico_type_mini_hotel";
                break;
            }
            case CommericalIndustrial: {
                imageName = @"ico_type_mini_industrial";
                break;
            }
            case CommericalLand: {
                imageName = @"ico_type_mini_land";
                break;
            }
            case CommericalOffice: {
                imageName = @"ico_type_mini_office";
                break;
            }
            case CommericalOther: {
                imageName = @"ico_type_mini_others";
                break;
            }
            case CommericalRetail: {
                imageName = @"ico_type_mini_retail";
                break;
            }
            case CommericalSpecialPurpose: {
                imageName = @"ico_type_mini_special";
                break;
            }
            case CommericalWarehouse: {
                imageName = @"ico_type_mini_warehouse";
                break;
            }
            default:
                break;
        }
    }
    
    //    propertyImage = [UIImage imageNamed:imageName];
    
    return imageName;
}

-(NSString*)getSloganByIndex:(int)index{
    NSArray *sloganArray = @[@"Amazing potential", @"The only one", @"Fire sale",
                             @"Super high yield", @"Final call", @"Nothing to lose",
                             @"With good investment potential",
                             @"The only opportunity in market", @"Below market price",
                             @"High return / High yield", @"Last call offer",
                             @"Immediate sale"];
    NSString *slogan = @"";
    if (sloganArray.count >index) {
        slogan = sloganArray[index];
    }
    return slogan;
}

-(NSArray*)getSpokenLanguageStringList{
    NSMutableArray *langStringList = [[NSMutableArray alloc]init];
    for (int i =0 ; i<self.spokenlanguagelist.count; i++) {
        SpokenLanguageList *lang = self.spokenlanguagelist[i];
        [langStringList addObject:lang.NativeName];
    }
    return langStringList;
}

-(int)getSpokenLanguageIndex:(NSString*)nativeName{
    if ([RealUtility isValid:nativeName]) {
        NSArray *langList =[[[SystemSettingModel shared] getSpokenLanguageStringList] copy];
        return (int)[langList indexOfObject:nativeName];
    }else{
        return (int)NSNotFound;
    }
}

-(NSString*)getSpokenLanguageString:(NSNumber*)index{
    NSString *langString = @"Not Found";
    if (index) {
        int langIndex = [index intValue];
        for (SystemLanguageList *lang in [self.SystemSettings.SystemLanguageList copy]) {
            if(lang.Index == langIndex){
                langString=lang.NativeName;
                break;
            }
        }
    }
    return langString;
}

-(NSArray*)getSettingLanguageStringList{
    NSMutableArray *langStringList = [[NSMutableArray alloc]init];
    for (int i =0 ; i<self.SystemSettings.SystemLanguageList.count; i++) {
        SpokenLanguageList *lang = self.SystemSettings.SystemLanguageList[i];
        [langStringList addObject:lang.NativeName];
    }
    return langStringList;
}
-(int)getSettingLanguageIndex:(NSString*)nativeName{
    if ([RealUtility isValid:nativeName]) {
        NSArray *langList =[[[SystemSettingModel shared] getSpokenLanguageStringList] copy];
        return (int)[langList indexOfObject:nativeName];
    }else{
        return (int)NSNotFound;
    }
    
}
-(NSString*)getSettingLanguageString:(NSNumber*)index{
    NSString *langString = @"Not Found";
    if (index) {
        int langIndex = [index intValue];
        for (SpokenLanguageList *lang in [self.SystemSettings.SystemLanguageList copy]) {
            if(lang.Index == langIndex){
                langString=lang.NativeName;
                break;
            }
        }
    }
    return langString;
}



-(NSString*)getSloganStringByLangIndex:(NSNumber*)langIndex position:(NSNumber*)position{
    NSString *sloganString = @"Not Found";
    if (langIndex) {
        
        int positionINT = [position intValue];
        NSArray *sloganArray  =[self getSloganListByLangIndex:langIndex];
        for (SloganList *sloganList in [sloganArray copy]) {
            if (sloganList.Position == positionINT) {
                sloganString = sloganList.Slogan;
                break;
            }
        }
    }
    return sloganString;
}

-(NSString*)getUserCurrencyUnit{
    return [self.SystemSettings.CurrencyList firstObject];
}

-(NSArray*)getSloganListByLangIndex:(NSNumber*)langIndex{
    NSMutableArray *sloganArray = [[NSMutableArray alloc]init];
    int langINT = [langIndex intValue];
    if (self.cachedSloganDict[langIndex]) {
        sloganArray = [self.cachedSloganDict[langIndex]mutableCopy];
        return sloganArray;
    }else{
        for (SloganList *sloganList in [self.SystemSettings.SloganList copy]) {
            if (sloganList.LanguageIndex == langINT) {
                [sloganArray addObject:sloganList];
            }
        }
        if (!self.cachedSloganDict[langIndex]) {
            self.cachedSloganDict = [[NSMutableDictionary alloc]init];
        }
        [self.cachedSloganDict setObject:sloganArray forKey:langIndex];
    }
    
    return sloganArray;
}

-(NSArray*)getSloganStringListByLangIndex:(NSNumber*)langIndex{
    NSMutableArray *sloganStringArray = [[NSMutableArray alloc]init];
    NSArray *sloganArray  =[self getSloganListByLangIndex:langIndex];
    int langINT = [langIndex intValue];
    for (SloganList *sloganList in [sloganArray copy]) {
        if (sloganList.LanguageIndex == langINT) {
            [sloganStringArray addObject:sloganList.Slogan];
        }
    }
    return sloganStringArray;
}

-(NSString*)serverReturnLanguageCodeChangeToAppleFormat:(NSString *)code{
    if ([code isEqualToString:@"zh-CHT"]) {
        return @"zh-Hant";
    }
    if ([code isEqualToString:@"zh-CHS"]) {
        return @"zh-Hans";
    }
    if ([code isEqualToString:@"zh"]) {
        return @"zh-Hant";
    }
    return code;
}
-(NSLocale*)getCurrentLocale{
    NSLocale *Locale = [[NSLocale alloc] initWithLocaleIdentifier:[LanguagesManager sharedInstance].currentLanguage];
    return Locale;
}

-(NSMutableArray*)getNotDuplicateSMSCountryCodeList{
    NSMutableArray * notDuplicateSMSCountryCodeList= [[NSMutableArray alloc]init];
    
    
    SMSCountryCodeList * smsCountryCodeListFirstObject = [self.SystemSettings.SMSCountryCodeList firstObject];
    NSString * countryCode=smsCountryCodeListFirstObject.v;
   // [notDuplicateSMSCountryCodeList addObject:smsCountryCodeListFirstObject.v];
    
    for (SMSCountryCodeList * smsCountryCodeList in self.SystemSettings.SMSCountryCodeList) {
        if ([countryCode isEqualToString: smsCountryCodeList.v] ) {
            
        }else{
            countryCode=smsCountryCodeList.v;
            [notDuplicateSMSCountryCodeList addObject:smsCountryCodeList.v];
        }
    }
    return notDuplicateSMSCountryCodeList;
    
}
-(int)getIndexFromNotDuplicateSMSCountryCodeListAccordingToCountry{
    
    NSMutableArray * notDuplicateSMSCountryCodeList= [self getNotDuplicateSMSCountryCodeList];
    NSString * smsIndexStringFromCountry= [self getSMSIndexStringFromSMSCountryCodeListAccordingToCountry];
    int i=0;
    for (NSString * smsIndexString in notDuplicateSMSCountryCodeList) {
        
        if ([smsIndexStringFromCountry isEqualToString:smsIndexString]) {
            return i;
        }
        i++;
    }
    
    return 1;
    
    
}
-(NSString *)getSMSIndexStringFromSMSCountryCodeListAccordingToCountry{
    
    for (SMSCountryCodeList * smsCountryCodeList in self.SystemSettings.SMSCountryCodeList) {
        
        if ( [[[GetContryLocation shared]getCountryNameFromSimCard] isEqualToString:smsCountryCodeList.k]) {
            
            return smsCountryCodeList.v;
        }
    }
    return @"1";
    
    
}
@end