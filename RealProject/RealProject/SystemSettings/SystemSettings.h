//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

#import "SpokenLanguageList.h"
#import "SystemLanguageList.h"
#import "PropertyTypeList.h"
#import "MetricList.h"
#import "GoogleAddressLanguageList.h"
#import "TopFiveCitiesList.h"
#import "SloganList.h"
#import "SMSCountryCodeList.h"
@protocol SystemSettings
@end
@interface SystemSettings : JSONModel
//+ (d *)shared;
@property(assign, nonatomic) int Version;
@property(strong, nonatomic) NSArray<SpokenLanguageList>* SpokenLanguageList;
@property(strong, nonatomic) NSArray<SystemLanguageList>* SystemLanguageList;
@property(strong, nonatomic) NSArray<PropertyTypeList>* PropertyTypeList;
@property(strong, nonatomic) NSArray* CurrencyList;
@property(strong, nonatomic) NSArray<MetricList>* MetricList;
@property(strong, nonatomic) NSArray<GoogleAddressLanguageList>*GoogleAddressLanguageList;
@property(strong, nonatomic) NSArray<TopFiveCitiesList>*TopFiveCitiesList;
@property(strong, nonatomic) NSArray<SloganList>*SloganList;
@property(strong, nonatomic) NSArray<SMSCountryCodeList>*SMSCountryCodeList;
@property(strong, nonatomic)MetricList *userSelectMetric;
- (SystemSettings*)initwithjsonstring:(NSString*)content:(int)error;
- (SystemSettings*)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
