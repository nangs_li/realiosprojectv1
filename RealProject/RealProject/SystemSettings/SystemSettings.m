//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SystemSettings.h"

@implementation SystemSettings
static SystemSettings *sharedSystemSettings;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (SystemSettings *)shared {
  @synchronized(self) {
    if (!sharedSystemSettings) {
      sharedSystemSettings = [[SystemSettings alloc] init];
    }
    return sharedSystemSettings;
  }
}

- (SystemSettings *)initwithjsonstring:(NSString *)content:(int)error {
  NSError *err = nil;
  sharedSystemSettings =
      [[SystemSettings alloc] initWithString:content error:&err];
  return sharedSystemSettings;
}
- (SystemSettings *)initwithNSDictionary:(NSDictionary *)content:(int)error {
  NSError *err = nil;
  sharedSystemSettings =
      [[SystemSettings alloc] initWithDictionary:content error:&err];
  return sharedSystemSettings;
}

@end