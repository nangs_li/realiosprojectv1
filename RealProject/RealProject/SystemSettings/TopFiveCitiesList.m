//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "TopFiveCitiesList.h"

@implementation TopFiveCitiesList
static TopFiveCitiesList *sharedTopFiveCitiesList;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (TopFiveCitiesList *)shared {
  @synchronized(self) {
    if (!sharedTopFiveCitiesList) {
      sharedTopFiveCitiesList = [[TopFiveCitiesList alloc] init];
    }
    return sharedTopFiveCitiesList;
  }
}

@end