//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@protocol address_components
@end

@interface address_components : JSONModel

@property(strong, nonatomic) NSString* long_name;
@property(strong, nonatomic) NSString* short_name;
@property(strong, nonatomic) NSArray* types;

-(address_components *)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
