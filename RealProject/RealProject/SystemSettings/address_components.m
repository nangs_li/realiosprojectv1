//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "address_components.h"

@implementation address_components
static address_components *sharedaddress_components;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (address_components *)shared {
  @synchronized(self) {
    if (!sharedaddress_components) {
      sharedaddress_components = [[address_components alloc] init];
    }
    return sharedaddress_components;
  }
}
- (address_components *)initwithNSDictionary:(NSDictionary *)content:(int)error {
    NSError *err = nil;
    sharedaddress_components = [[address_components alloc] initWithDictionary:content error:&err];
    return sharedaddress_components;
}
@end