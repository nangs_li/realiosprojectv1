//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Twopage.h"
@interface TableViewCell : UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier;
//// TableViewcell property
@property(weak, nonatomic) IBOutlet UIView *alphaview;
@property(strong, nonatomic) IBOutlet UIImageView *imageview;
@property(strong, nonatomic) IBOutlet UILabel *follower;
@property(strong, nonatomic) IBOutlet UILabel *price;
@property(strong, nonatomic) IBOutlet UILabel *size;
@property(strong, nonatomic) IBOutlet UILabel *numberofbathroom;
@property(strong, nonatomic) IBOutlet UILabel *numberofbedroom;
@property(strong, nonatomic) IBOutlet UIImageView *personalimage;
@property(strong, nonatomic) IBOutlet UILabel *name;
@property(strong, nonatomic) IBOutlet UILabel *typeofspace;
@property(strong, nonatomic) IBOutlet UILabel *slogan;
@property(strong, nonatomic) IBOutlet UILabel *street;
- (void)setUpPeronalImagelLayerToCircule;

@end
