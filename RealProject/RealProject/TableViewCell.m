//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "TableViewCell.h"

#import <QuartzCore/QuartzCore.h>

@implementation TableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
  if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
  }
  return self;
}

- (void)awakeFromNib {
  // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  // super setSelected

  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}
- (void)setUpPeronalImagelLayerToCircule {
  DDLogInfo(@"setupperonalimagelayer");
  self.personalimage.layer.cornerRadius =
      self.personalimage.frame.size.width / 2;
  self.personalimage.clipsToBounds = YES;
}

@end
