//
//  ActivityLogTableViewCell.h
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ActivityLogModel,ActivityPieView;
@interface ActivityLogTableViewCell : UITableViewCell
@property (nonatomic,strong) IBOutlet NSLayoutConstraint* logLabelBottomVerticalSpacingConstraint;

@property (nonatomic,strong) IBOutlet NSLayoutConstraint *detailViewBottomSpacingConstraint;
@property (nonatomic,strong) IBOutlet NSLayoutConstraint *detailViewHeightConstraint;

@property (nonatomic,strong) IBOutlet UIImageView *logIconImageView;
@property (nonatomic,strong) IBOutlet UILabel *logLabel;
@property (nonatomic,strong) IBOutlet UILabel *timeLabel;
@property (nonatomic,strong) IBOutlet UIButton *expandIndicatorButton;

@property (nonatomic,strong) IBOutlet UIView *detailView;
@property (nonatomic,strong) IBOutlet UILabel *joinDateTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *joinDateLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingCountTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *followedTimeTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *followedTimeLabel;
@property (nonatomic,strong) IBOutlet UILabel *basedInLocationTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *basedInLocationLabel;
@property (nonatomic,strong) IBOutlet UILabel *activeChatTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *activeChatLabel;
@property (nonatomic,strong) IBOutlet UILabel *activityTitleLabel;;
@property (nonatomic,strong) IBOutlet UIView *activityView;
@property (nonatomic,strong) IBOutlet UIView *pieChartContainerView;
@property (nonatomic,strong) IBOutlet ActivityPieView *topOnePieChart;
@property (nonatomic,strong) IBOutletCollection(UIView) NSArray *topActivityList;
@property (nonatomic,strong) IBOutlet UILabel *topOnePercentageLabel;
@property (nonatomic,strong) IBOutlet UILabel *topOneLocationLabel;

@property (nonatomic,strong) NSArray *pieChatSliceArray;

@property (nonatomic,assign) BOOL expanded;
- (void)configureWithActivityLogModel:(ActivityLogModel*)logModel expanded:(BOOL)expanded;

- (BOOL)shouldExpand:(BOOL)expand force:(BOOL)force;

@end
