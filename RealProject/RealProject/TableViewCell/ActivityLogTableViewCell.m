//
//  ActivityLogTableViewCell.m
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ActivityLogTableViewCell.h"
#import "ActivityLogModel.h"
#import "PieChatSlice.h"
#import "ActivityPieView.h"
@interface ActivityLogTableViewCell()
@property (nonatomic,assign) BOOL canExpand;
@end
@implementation ActivityLogTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self shouldExpand:NO force:YES];
    [self setupPieChart];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setupPieChart{
    //    self.topOnePieChart.delegate = self;
    //    self.topOnePieChart.dataSource = self;
    
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.topOnePercentageLabel.layer.cornerRadius = CGRectGetWidth(self.topOnePercentageLabel.bounds);
}

- (void)configureWithActivityLogModel:(ActivityLogModel*)logModel expanded:(BOOL)expanded{
    self.logIconImageView.image = [logModel logIconImage];
    /* hot fix for korean text */
    self.logLabel.textColor = [UIColor darkGrayColor];
    self.logLabel.font = [UIFont systemFontOfSize:14];
    [self.logLabel setAttributedTextWithMarkUp:[logModel logMarkUp]];
    
    if (logModel.LogType == ActivityLogTypeInviteToFollow) {
        [self enableExpand:NO];
        [self shouldExpand:NO force:YES];
    }else{
        [self enableExpand:YES];
        self.joinDateTitleLabel.text=JMOLocalizedString(@"me__join_real", nil);
        self.joinDateLabel.text = logModel.joinDateString;
        NSString *  followingCountTitleLabelText =JMOLocalizedString(@"me__following", nil);
        self.followingCountTitleLabel.text=followingCountTitleLabelText;
        self.followingCountLabel.text = logModel.followingCountString;
        self.followedTimeTitleLabel.text=JMOLocalizedString(@"me__followed_you", nil);
        self.followedTimeLabel.text = logModel.followedTimeString;
        self.basedInLocationTitleLabel.text=JMOLocalizedString(@"me__based_in", nil);
        self.basedInLocationLabel.text = logModel.locationString;
        self.activeChatTitleLabel.text=JMOLocalizedString(@"me__active_chat", nil);
        self.activeChatLabel.text = logModel.activeChatString;
        self.activityTitleLabel.text=JMOLocalizedString(@"me__activity_looking_at", nil);
        [self shouldExpand:expanded force:YES];
        ActivityLogPlaceModel *topPlaceModel = [logModel.Places firstObject];
        NSString *topSpacePercentageString = topPlaceModel.percentageString;
        PieChatSlice *topSlice =[PieChatSlice sliceWithTitle:topPlaceModel.Location value:[topPlaceModel percentageFloat] color:RealBlueColor];
        PieChatSlice *otherSlice = [PieChatSlice sliceWithTitle:@"other" value:1.0-[topPlaceModel percentageFloat] color:[UIColor lightGrayColor]];
        self.topOnePercentageLabel.text = topSpacePercentageString;
        self.topOneLocationLabel.text = topPlaceModel.Location;
        self.pieChatSliceArray = @[topSlice,otherSlice];
        [self configureTopActivityList:logModel.Places];
    }
}

- (void)configureTopActivityList:(NSArray*)topPlace{
    [self resetTopActivityList];
    for (int i = 0; i< topPlace.count; i++) {
        ActivityLogPlaceModel *placeModel = topPlace[i];
        UIView *container = self.topActivityList[i];
        UILabel *percentageLabel = [container viewWithTag:1001];
        UILabel *locationLabel = [container viewWithTag:1002];
        percentageLabel.text = placeModel.percentageString;
        locationLabel.text = placeModel.Location;
        container.hidden = NO;
    }
    
}

-(void)resetTopActivityList{
    for (UIView *container in self.topActivityList) {
        container.hidden = YES;
    }
}

- (void)enableExpand:(BOOL)enable{
    self.canExpand = enable;
    self.expandIndicatorButton.hidden = !enable;
}
- (BOOL)shouldExpand:(BOOL)expand force:(BOOL)force{
    if (!self.canExpand && !force) {
        return NO;
    }
    self.expanded = expand;
    if (self.expanded) {
        self.detailViewBottomSpacingConstraint.active = YES;
        self.detailViewHeightConstraint.active = YES;
    }else{
        self.detailViewBottomSpacingConstraint.active = NO;
        self.detailViewHeightConstraint.active = NO;
    }
    self.expandIndicatorButton.selected = expand;
    [UIView animateWithDuration:0.24 animations:^{
        self.detailView.alpha = expand;
    }completion:^(BOOL finished) {
        if (!finished) {
        }else if (self.expanded) {
            [self.topOnePieChart setSlices:self.pieChatSliceArray];
        }else if (!self.expanded){
            [self.topOnePieChart clearAllSlices];
        }
    }];
    
    return expand;
}


@end
