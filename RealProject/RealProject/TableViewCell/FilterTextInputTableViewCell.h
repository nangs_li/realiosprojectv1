//
//  FilterTextInputTableViewCell.h
//  productionreal2
//
//  Created by Alex Hung on 12/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLGlowingTextField.h"
#import "MutlipleStateButton.h"
#define FilterTypePrice @"FilterTypePrice"
#define FilterTypeSize @"FilterTypeSize"

@protocol FilterTextInputCellDelagate <NSObject>
-(void)filterTextInputDidChange:(NSString*)value type:(NSString*)type unit:(NSString*)unit state:(MutipleState)state;
-(void)closeAllKeyBoard;

@end

@interface FilterTextInputTableViewCell : UITableViewCell <UITextFieldDelegate>
@property (nonatomic,strong) IBOutlet id<FilterTextInputCellDelagate> delegate;
@property (nonatomic,strong) IBOutlet UIButton *filterTitleButton;
@property (nonatomic,strong) IBOutlet SLGlowingTextField *textField;
@property (nonatomic,strong) IBOutlet UIView *priceLeftView;
@property (nonatomic,strong) IBOutlet UIView *sizeRightView;
@property (weak, nonatomic) IBOutlet UIButton *priceBtn;
@property (nonatomic,strong) IBOutlet UIButton *unitButton;
@property (weak, nonatomic) IBOutlet UIButton *unitBtn;
@property (nonatomic,strong) IBOutlet MutlipleStateButton *stateButton;
@property (nonatomic,strong) UIToolbar *inputToolBar;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *unit;
-(void)configureCell:(NSDictionary *)dict;
-(IBAction)textFieldDidChange:(UITextField*)textField;
@end
