//
//  FilterTextInputTableViewCell.m
//  productionreal2
//
//  Created by Alex Hung on 12/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "FilterTextInputTableViewCell.h"
#import "ActionSheetPicker.h"
#import "RealUtility.h"

#import "UIButton+Utility.h"
#define space 5.0f
@implementation FilterTextInputTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)configureCell:(NSDictionary *)dict{
    NSString *filterType = dict[@"filterType"];
    NSString *filterImage = dict[@"filterImage"];
    NSString *filterTitle = dict[@"filterTitle"];
    
    NSString *filterUnit = dict[@"filterUnit"];
    NSString *filterState = dict[@"minValue"];
    NSString *filterValue = dict[@"maxValue"];
    
    self.filterTitleButton.titleLabel.numberOfLines = 1;
    [self.filterTitleButton setTitle:filterTitle forState:UIControlStateNormal];
    [self.filterTitleButton setImage:[UIImage imageNamed:filterImage] forState:UIControlStateNormal];
    CGRect filterTitleFrame = self.filterTitleButton.frame;
    filterTitleFrame.size = [self.filterTitleButton sizeThatFits:CGSizeMake(100, 23)];
    filterTitleFrame.size.width += 8;
    self.filterTitleButton.frame = filterTitleFrame;
    
    self.textField.text = filterValue;
    if (!self.unitButton) {
        self.unitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.unitButton.frame = CGRectMake(0, 0, 60, self.textField.frame.size.height);
        self.unitButton.titleLabel.font =[UIFont systemFontOfSize:12];
        self.unitButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.unitButton addTarget:self action:@selector(unitButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.type = filterType;
    self.unit = filterUnit;
    [self.filterTitleButton setUpAutoScaleButton];
    if (!self.stateButton) {
        self.stateButton = [MutlipleStateButton buttonWithType:UIButtonTypeCustom];
        self.stateButton.frame = CGRectMake(0, 0,70, self.textField.frame.size.height);
        [self.stateButton commonSetup];
        self.stateButton.titleLabel.font =[UIFont systemFontOfSize:12];
        
        [self.stateButton setState:MutipleStateAbove withTitle:JMOLocalizedString(@"search_filter__above", nil)];
        
        [self.stateButton setState:MutipleStateBelow withTitle:JMOLocalizedString(@"search_filter__below", nil)];
        NSString * aroundString =JMOLocalizedString(@"search_filter__around", nil);
//        if ([aroundString isEqualToString:@"around"]) {
//            aroundString=[aroundString uppercaseString];
//        }
        [self.stateButton setState:MutipleStateAround withTitle:aroundString];
        [self.stateButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:23/255.0f green:22/255.0f blue:22/255.0f alpha:1.0]] forState:UIControlStateNormal];
        [self.stateButton addTarget:self action:@selector(stateButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.stateButton.selectState = [filterState intValue];
    
    UIView *textFieldLeftView = nil;
    UIView *textFieldRightView = nil;
    UIImageView *divider = [[UIImageView alloc]initWithFrame:CGRectMake(0, 6, 1, self.textField.frame.size.height-12)];
    UIImageView *arrowImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"btn_pop_pulldown"]];
    divider.backgroundColor =[UIColor lightTextColor];
// the amount of spacing to appear between image and title
    [self.unitButton centerButtonAndImageWithSpacing:space];
    if ([filterType isEqualToString:FilterTypePrice]) {
        textFieldLeftView = [[UIView alloc]initWithFrame:self.unitButton.bounds];
        [textFieldLeftView addSubview:self.unitButton];
        [self.unitButton setImage:[UIImage imageNamed:@"btn_pop_pulldown_small"] forState:UIControlStateNormal];
        self.unitButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.unitButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.unitButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
       
         //[self.unitButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0,6 )];
        
        textFieldRightView = [[UIView alloc]initWithFrame:self.stateButton.bounds];
        [textFieldRightView addSubview:self.stateButton];
        [textFieldLeftView addSubview:divider];
        
        [divider setViewAlignmentInSuperView:ViewAlignmentRight padding:0];
        [divider setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
        self.textField.leftViewMode = UITextFieldViewModeAlways;
        self.textField.rightViewMode = UITextFieldViewModeAlways;
        
    }else if ([filterType isEqualToString:FilterTypeSize]){
        self.textField.leftViewMode = UITextFieldViewModeNever;
        self.textField.rightViewMode = UITextFieldViewModeAlways;
        self.unitButton.frame = CGRectMake(0, 0,70, self.textField.frame.size.height);
        CGRect rightFrame = self.unitButton.bounds;
        rightFrame.size.width += self.stateButton.frame.size.width;
        [self.unitButton setImage:[UIImage imageNamed:@"btn_pop_pulldown_small"] forState:UIControlStateNormal];
        self.unitButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.unitButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.unitButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.unitButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
       // [self.unitButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 6)];
        textFieldRightView = [[UIView alloc]initWithFrame:rightFrame];
        [textFieldRightView addSubview:self.unitButton];
        CGRect stateButtonFrame = self.stateButton.bounds;
        stateButtonFrame.origin.x = self.unitButton.frame.size.width;
        self.stateButton.frame =stateButtonFrame;
      

        [textFieldRightView addSubview:self.stateButton];
        [textFieldRightView addSubview:divider];
        [divider setViewAlignmentInSuperView:ViewAlignmentLeft padding:0];
        [divider setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
    }
    
    self.textField.leftView = textFieldLeftView;
    self.textField.rightView = textFieldRightView;
    if (!self.inputToolBar) {
        
    }
    if (!self.inputToolBar) {
        self.inputToolBar = [[UIToolbar alloc] init];
        [self.inputToolBar sizeToFit];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(Done, nil)
                                                                       style:UIBarButtonItemStyleDone target:self
                                                                      action:@selector(doneButtonDidPress:)];
        [self.inputToolBar setItems:[NSArray arrayWithObjects:doneButton, nil]];
        self.textField.inputAccessoryView = self.inputToolBar;
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (([string isEqualToString:@"0"] || [string isEqualToString:@""]) && [textField.text rangeOfString:@"."].location < range.location) {
        return YES;
    }
    
    // First check whether the replacement string's numeric...
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    bool isNumeric = [string isEqualToString:filtered];
    
    // Then if the replacement string's numeric, or if it's
    // a backspace, or if it's a decimal point and the text
    // field doesn't already contain a decimal point,
    // reformat the new complete number using
    // NSNumberFormatterDecimalStyle
    if (isNumeric ||
        [string isEqualToString:@""] ||
        ([string isEqualToString:@"."] &&
         [textField.text rangeOfString:@"."].location == NSNotFound)) {
            
            // Create the decimal style formatter
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setMaximumFractionDigits:10];
            
            // Combine the new text with the old; then remove any
            // commas from the textField before formatting
            NSString *combinedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSString *numberWithoutCommas = [combinedText stringByReplacingOccurrencesOfString:@"," withString:@""];
            NSNumber *number = [formatter numberFromString:numberWithoutCommas];
            
            float numberDouble = [number floatValue];
            if ([self.type isEqualToString:FilterTypePrice] && numberDouble > decimalMax) {
                return NO;
            }else if([self.type isEqualToString:FilterTypeSize] &&numberDouble >INT32_MAX ){
                return NO;
            }else{
                NSString *formattedString = [formatter stringFromNumber:number];
                
                // If the last entry was a decimal or a zero after a decimal,
                // re-add it here because the formatter will naturally remove
                // it.
                if ([string isEqualToString:@"."] &&
                    range.location == textField.text.length) {
                    formattedString = [formattedString stringByAppendingString:@"."];
                }
                
                textField.text = formattedString;
                   [self changeCallBack];
            }
            
        }
    
    // Return no, because either the replacement string is not
    // valid or it is and the textfield has already been updated
    // accordingly
    return NO;
}
-(IBAction)textFieldDidChange:(UITextField*)textField{
//    [self changeCallBack];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField { //Keyboard becomes visible
    [self.stateButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:9/255.0f green:41/255.0f blue:57/255.0f alpha:1.0]] forState:UIControlStateNormal];
    //perform actions.
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
   
    if (textField.text.length <= 0) {
        textField.text = @"0";
    }
     [self.stateButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:23/255.0f green:22/255.0f blue:22/255.0f alpha:1.0]] forState:UIControlStateNormal];
}

-(IBAction)doneButtonDidPress:(id)sender{
    [self.contentView endEditing:YES];
    [self changeCallBack];
}

-(void)changeCallBack{
    if ([self.delegate respondsToSelector:@selector(filterTextInputDidChange:type: unit: state:)]) {
        NSString *unit = self.unit;
        //        NSString *value = [RealUtility removeDecimalFormat:self.textField.text];
       
        [self.delegate filterTextInputDidChange:self.textField.text type:self.type unit:unit state:self.stateButton.selectState];
    }
}

-(IBAction)stateButtonDidPress:(id)sender{
    [self.stateButton setSelected:YES];
    [self closeTableViewAllKeyboard];
    [self changeCallBack];
}

-(IBAction)unitButtonDidPress:(id)sender{
    [self.contentView endEditing:YES];
    [self closeTableViewAllKeyboard];
    NSString *title =@"";
    NSMutableArray *pickerTitles = [[NSMutableArray alloc]init];
    if ([self.type isEqualToString:FilterTypePrice]) {
        for (NSString *currency in [SystemSettingModel shared].SystemSettings.CurrencyList) {
            [pickerTitles addObject:currency];
        }
        title = @"";
    }else if ([self.type isEqualToString:FilterTypeSize]){
        for (MetricList *metric in [SystemSettingModel shared].metricaccordingtolanguage) {
            [pickerTitles addObject:metric.NativeName];
        }
        title = JMOLocalizedString(@"common__unit", nil);
    }
    NSInteger selectedIndex = [pickerTitles indexOfObject:self.unit];
  
//    [actionSheetSetingPicker showPickerWithTitle:title
//                                            rows:pickerTitles
//                                initialSelection:selectedIndex
//                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
//                                           self.unit = selectedValue;
//                                           [self changeCallBack];
//                                       }
//                                     cancelBlock:^(ActionSheetStringPicker *picker) {
//                                         
//                                     }origin:sender];
    
      ActionSheetStringPicker * actionSheetSetingPicker =   [[ActionSheetStringPicker new] initWithTitle:title rows:pickerTitles initialSelection:selectedIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            self.unit = selectedValue;
                                                   [self changeCallBack];
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:sender];
      [actionSheetSetingPicker setCancelButton: [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(@"common__cancel", nil) style: UIBarButtonItemStyleDone target:nil action:nil]];
     [actionSheetSetingPicker setDoneButton: [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(@"error_message__done", nil) style: UIBarButtonItemStyleDone target:nil action:nil]];
    [actionSheetSetingPicker showActionSheetPicker];
}

-(void)closeTableViewAllKeyboard{
    if ([self.delegate respondsToSelector:@selector(closeAllKeyBoard)]) {
        [self.delegate closeAllKeyBoard];
    }
    
}
-(void)setUnit:(NSString *)unit{
    _unit= unit;
    [self.unitButton setTitle:_unit forState:UIControlStateNormal];
}

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 5, 0);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 5, 0);
}
@end
