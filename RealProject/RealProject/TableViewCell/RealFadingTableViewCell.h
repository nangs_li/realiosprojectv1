//
//  RealFadingTableViewCell.h
//  productionreal2
//
//  Created by Alex Hung on 6/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealFadingTableViewCell : UITableViewCell

- (void)maskCellFromTop:(CGFloat)margin;

@end
