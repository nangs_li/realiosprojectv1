//
//  RealPhoneBookTableViewCell.h
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealFadingTableViewCell.h"
@class REContact;

@interface RealPhoneBookTableViewCell : RealFadingTableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *phoneLabel;
@property (nonatomic ,strong) IBOutlet UIImageView *indicatorImageView;

- (void)configureWithContact:(REContact *)contact invitedTime:(NSInteger)invitedTime;

@end
