//
//  RealPhoneBookTableViewCell.m
//  productionreal2
//
//  Created by Alex Hung on 1/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RealPhoneBookTableViewCell.h"
#import "UIImage+Utility.h"
#import "UIColor+Utility.h"

@implementation RealPhoneBookTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.indicatorImageView.highlighted = selected;
}

- (void)configureWithContact:(REContact *)contact invitedTime:(NSInteger)invitedTime {
    self.nameLabel.text = contact.name;
    self.phoneLabel.text = [contact phone];
    
    UIImage *indicatorNormalImage;
    UIImage *indicatorHighlightImage = [UIImage imageWithColor:[UIColor alexYellow]];
    
    if (invitedTime > 0) {
        indicatorNormalImage = [UIImage imageWithBorder:[UIColor alexYellow] size:self.indicatorImageView.frame.size fillcolor:[UIColor clear]];
    }else{
        indicatorNormalImage = [UIImage imageWithColor:[UIColor clearColor]];
    }
    
    self.indicatorImageView.image = indicatorNormalImage;
    self.indicatorImageView.highlightedImage = indicatorHighlightImage;
}

@end
