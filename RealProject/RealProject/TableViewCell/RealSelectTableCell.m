//
//  RealSelectTableCell.m
//  productionreal2
//
//  Created by Alex Hung on 2/11/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealSelectTableCell.h"

@implementation RealSelectTableCell

- (void)awakeFromNib {
    // Initialization code
    _enable = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (self.enable) {
        self.indicatorImage.hidden = !selected;
    }else{
        self.indicatorImage.hidden = NO;
    }
}

- (void)setEnable:(BOOL)enable{
    _enable = enable;
    self.disableView.hidden = enable;
    if (!enable && self.disableSelection) {
        self.indicatorImage.hidden = NO;
        self.contentView.alpha =0.5;
    }else{
        self.indicatorImage.hidden = YES;
        self.contentView.alpha = 1.0;
    }
}


-(void)configureWithTitle:(NSString*)title indicatorImage:(UIImage*)image{
    self.titleLabel.text = title;
    self.indicatorImage.image = image;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
