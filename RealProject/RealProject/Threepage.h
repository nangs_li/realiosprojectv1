//
//
//
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "Threepage.h"
#import "JSTokenField.h"
@interface Threepage
    : BaseViewController<UITableViewDelegate, UITableViewDataSource,
                         UIScrollViewDelegate, UIGestureRecognizerDelegate,
                         JSTokenFieldDelegate, UIActionSheetDelegate>
@property(nonatomic, strong) QBChatDialog *dialog;
#pragma mark UI
@property(nonatomic, strong) IBOutlet UIButton *chatbutton;
@property(nonatomic, strong) IBOutlet UIButton *followbutton;
@property(strong, nonatomic) IBOutlet UIImageView *personalimage;
@property(strong, nonatomic) IBOutlet UILabel *name;
@property(strong, nonatomic) IBOutlet UILabel *Followers;
#pragma mark membername, photourl
@property(assign, nonatomic) NSString *MemberName;
@property(assign, nonatomic) NSString *AgentPhotoURL;
#pragma mark language tokenfield
@property(nonatomic, strong)
    NSMutableArray *tableViewSection2LanguageFieldArray;
@property(nonatomic, strong) JSTokenField *tableViewSection2LanguageField;

#pragma mark specialty tokenfield
@property(nonatomic, strong)
    NSMutableArray *tableViewSection4SepecialtyFieldArray;
@property(nonatomic, strong) JSTokenField *tableViewSection4SepecialtyField;

@property(strong, nonatomic) IBOutlet UITableView *tableView;

@property(strong, nonatomic) UIActionSheet *followActionSheet;

#pragma mark setupThreepage
- (void)setupThreepage:(BOOL)fade;
- (void)tableViewSection2And4TokenFieldSetting;
@end
