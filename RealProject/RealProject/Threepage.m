//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Threepage.h"

#import "TableViewCell.h"
#import "Experiencecell.h"
#import "Specialty.h"
#import "Top5.h"
#import "Addexperience.h"
#import "SpokenLanguages.h"
#import "Top5cell.h"
#import "Spokenlanguagescell.h"
#import "Twopage.h"
#import "Chatroom.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Twopage.h"
#import "FollowAgentModel.h"
#import "UnFollowAgentModel.h"
#import "ReportProblemModel.h"
#import "PressChatButtonModel.h"
#import "DeepLinkActionHanderModel.h"
@interface Threepage ()<UIScrollViewDelegate>

@property(nonatomic) CGPoint viewInputCenter;

@property(nonatomic, strong) NSMutableArray *dialogs;
@property(nonatomic, strong) Twopage *twopage;

@end
@implementation Threepage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  DDLogInfo(@"ThreepageThreepage");
  [self.tableView reloadData];
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  //*****bug default y=-20;

  //  [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
}
- (void)viewDidLoad {
  [super viewDidLoad];
}
- (void)setupThreepage:(BOOL)fade {
  //[[[AppDelegate getAppDelegate]getTabBarController]setTabBarHidden:YES];

  DDLogInfo(@"setupThreepage-");

  self.AgentProfile = self.delegate.currentAgentProfile;

  self.name.text = self.AgentProfile.MemberName;
  NSURL *url = [NSURL URLWithString:self.AgentProfile.AgentPhotoURL];
  if (fade) {
      [self.personalimage loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
          if (image) {
              self.personalimage.alpha = 0.0;
              [UIView animateWithDuration:1.0
                               animations:^{
                                   self.personalimage.alpha = 1.0;
                               }];
          }
      }];

  } else {
      [self.personalimage loadImageURL:url withIndicator:nil withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
          
      }];
  }
  [self.delegate setupcornerRadius:self.personalimage];
  self.tableView.delegate = self;
  self.tableView.dataSource = self;

  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

  //   DDLogInfo(@"self.AgentProfile-->%@",[self.AgentProfile toJSONString]);

      if ([self.AgentProfile getIsFollowing]) {
    [self.followbutton setTitle:JMOLocalizedString(@"common__following", nil) forState:UIControlStateNormal];

  } else {
    [self.followbutton setTitle:JMOLocalizedString(@"common__following", nil) forState:UIControlStateNormal];
  }
  if (self.AgentProfile.MemberID ==
      [MyAgentProfileModel shared].myAgentProfile.MemberID) {
    [self.followbutton setBackgroundColor:self.delegate.Greycolor];
    self.followbutton.userInteractionEnabled = NO;
    [self.chatbutton setBackgroundColor:self.delegate.Greycolor];
    self.chatbutton.userInteractionEnabled = NO;

  } else {
    [self.followbutton setBackgroundColor:self.delegate.Greencolor];
    self.followbutton.userInteractionEnabled = YES;
    [self.chatbutton setBackgroundColor:self.delegate.Greencolor];
    self.chatbutton.userInteractionEnabled = YES;
  }

  self.Followers.text =
      [NSString stringWithFormat:@"%d", self.AgentProfile.FollowerCount];

  [self tableViewSection2And4TokenFieldSetting];
  [self.tableView reloadData];
}

#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  DDLogInfo(@"numberOfRowsInSection-->%ld", (long)section);

  if (section == 0) {
    if ([self.AgentProfile.AgentExperience count] > 0) {
      DDLogInfo(@"[self.AgentExperience count]-->%lu",
                (unsigned long)[self.AgentProfile.AgentExperience count]);
      return [self.AgentProfile.AgentExperience count];

    } else {
      return 0;
    }
  } else if (section == 1) {
    if ([self.AgentProfile.AgentLanguage count] > 0) {
      DDLogInfo(@"[self.AgentLanguage count]-->%lu",
                (unsigned long)[self.AgentProfile.AgentLanguage count]);
      return 1;
    } else {
      return 0;
    }
  } else if (section == 2) {
    if ([self.AgentProfile.AgentPastClosing count] > 0) {
      DDLogInfo(@"[self.AgentPastClosing count]-->%lu",
                (unsigned long)[self.AgentProfile.AgentPastClosing count]);
      return [self.AgentProfile.AgentPastClosing count];
    } else {
      return 0;
    }

  } else if (section == 3) {
    if ([self.AgentProfile.AgentSpecialty count] > 0) {
      DDLogInfo(@"[self.AgentSpecialty count]-->%lu",
                (unsigned long)[self.AgentProfile.AgentSpecialty count]);
      return 1;
    } else {
      return 0;
    }
  }
  return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"cellForRowAtIndexPath");

  if (indexPath.section == 0) {
    DDLogInfo(@"indexPath.section==0");

    static NSString *cellIdentifier = @"Experiencecell";

    Experiencecell *cell = (Experiencecell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
      DDLogInfo(@"cell==nil");
      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Experiencecell"
                                                   owner:self
                                                 options:nil];
      cell = (Experiencecell *)[nib objectAtIndex:0];
    }
    AgentExperience *AgentExperience =
        [self.AgentProfile.AgentExperience objectAtIndex:indexPath.row];
    cell.company.text = AgentExperience.Company;
    cell.title.text = AgentExperience.Title;

    if ([AgentExperience.IsCurrentJob isEqualToString:@"0"]) {
      cell.date.text = [NSString
          stringWithFormat:
              @"%@  -  %@",
              [self datestringtoMMMYYYFormat:AgentExperience.StartDateString],
              [self datestringtoMMMYYYFormat:AgentExperience.EndDateString]];

      cell.idstring = [NSString stringWithFormat:@"%d", AgentExperience.ID];

    } else {
      cell.date.text = [NSString
          stringWithFormat:@"%@  -  %@",
                           [self datestringtoMMMYYYFormat:AgentExperience
                                                              .StartDateString],
                           @"Present"];

      cell.idstring = [NSString stringWithFormat:@"%d", AgentExperience.ID];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [self.delegate Futurafont:10];

    UIView *separator1 =
        [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1,
                                                 cell.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [cell.contentView addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                   green:(115 / 255.f)
                                                    blue:(118 / 255.f)
                                                   alpha:1.0]];

    return cell;
  } else if (indexPath.section == 1) {
    DDLogInfo(@"indexPath.section==1");

    static NSString *cellIdentifier2 = @"SpokenLanguagescell";

    SpokenLanguagescell *cell = (SpokenLanguagescell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
    /*
     if (cell==nil) {
     DDLogInfo(@"cell==nil");
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SpokenLanguagescell"
                                                 owner:self
                                               options:nil];
    cell = (SpokenLanguagescell *)[nib objectAtIndex:0];
    // }

    //  [cell removeFromSuperview];
    [cell addSubview:self.tableViewSection2LanguageField];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [self.delegate Futurafont:10];
    UIView *separator1 = [[UIView alloc]
        initWithFrame:
            CGRectMake(
                0, self.tableViewSection2LanguageField.bounds.size.height - 1,
                self.tableViewSection2LanguageField.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self.tableViewSection2LanguageField addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                   green:(115 / 255.f)
                                                    blue:(118 / 255.f)
                                                   alpha:1.0]];

    return cell;

  } else if (indexPath.section == 2) {
    DDLogInfo(@"indexPath.section==2");

    static NSString *cellIdentifier3 = @"Top5cell";

    Top5cell *cell = (Top5cell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];

    if (cell == nil) {
      DDLogInfo(@"cell==nil");
      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Top5cell"
                                                   owner:self
                                                 options:nil];
      cell = (Top5cell *)[nib objectAtIndex:0];
    }

    NSString *SpaceType;
    AgentPastClosing *AgentPastClosing =
        [self.AgentProfile.AgentPastClosing objectAtIndex:indexPath.row];
    if (AgentPastClosing.PropertyType == 0) {
      SpaceType = [NSString stringWithFormat:@"ResidentailSpaceType%d",
                                             AgentPastClosing.SpaceType];
    } else {
      SpaceType = [NSString stringWithFormat:@"CommercialSpaceType%d",
                                             AgentPastClosing.SpaceType];
    }

    cell.typeofspace.text = NSLocalizedString(SpaceType, nil);

    cell.price.text = AgentPastClosing.SoldPrice;

    cell.date.text =
        [self datestringtoMMMYYYFormat:AgentPastClosing.SoldDateString];
    cell.idstring = [NSString stringWithFormat:@"%d", AgentPastClosing.ID];

    cell.address.text = AgentPastClosing.Address;

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [self.delegate Futurafont:10];
    UIView *separator1 =
        [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1,
                                                 cell.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [cell.contentView addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                   green:(115 / 255.f)
                                                    blue:(118 / 255.f)
                                                   alpha:1.0]];

    return cell;
  } else {
    DDLogInfo(@"indexPath.section==3");
    static NSString *cellIdentifier4 = @"SpokenLanguagescell";

    SpokenLanguagescell *cell = (SpokenLanguagescell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier4];
    /*
     if (cell==nil) {
     DDLogInfo(@"cell==nil");
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SpokenLanguagescell"
                                                 owner:self
                                               options:nil];
    cell = (SpokenLanguagescell *)[nib objectAtIndex:0];
    //}
    [cell addSubview:self.tableViewSection4SepecialtyField];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    cell.textLabel.font = [self.delegate Futurafont:10];
    UIView *separator1 = [[UIView alloc]
        initWithFrame:
            CGRectMake(
                0, self.tableViewSection4SepecialtyField.bounds.size.height - 1,
                self.tableViewSection4SepecialtyField.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self.tableViewSection4SepecialtyField addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                   green:(115 / 255.f)
                                                    blue:(118 / 255.f)
                                                   alpha:1.0]];
    return cell;
  }
}
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark tableviewheader and footer
- (UIView *)tableView:(UITableView *)tableView
    viewForHeaderInSection:(NSInteger)section {
  UIView *sectionHeaderView = [[UIView alloc]
      initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50.0)];
  sectionHeaderView.backgroundColor = [UIColor whiteColor];
  UIImageView *image =
      [[UIImageView alloc] initWithFrame:CGRectMake(0, 15, 20, 20.0)];

  UILabel *headerLabel = [[UILabel alloc]
      initWithFrame:CGRectMake(30, 15, sectionHeaderView.frame.size.width - 30,
                               25.0)];
  headerLabel.backgroundColor = [UIColor clearColor];

  headerLabel.textAlignment = NSTextAlignmentLeft;
  [headerLabel setFont:[UIFont fontWithName:@"Futura" size:14]];
  headerLabel.textColor = [UIColor colorWithRed:152 / 255.0
                                          green:154 / 255.0
                                           blue:157 / 255.0
                                          alpha:1];

  switch (section) {
    case 0:

      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Experience";

      [image setImage:[UIImage imageNamed:@"icon-experience"]];

      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;
    case 1:
      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Language";

      [image setImage:[UIImage imageNamed:@"icon-language"]];
      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;
    case 2:
      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Top 5 Selected Past Cloings";

      [image setImage:[UIImage imageNamed:@"icon-top5"]];
      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;
    case 3:
      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Specialty";

      [image setImage:[UIImage imageNamed:@"icon-specialty"]];
      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;

    default:
      break;
  }

  return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"heightForRowAtIndexPath");
  if (indexPath.section == 0) {
    DDLogInfo(@"return60");
    return 60;

  } else if (indexPath.section == 1) {
    return self.tableViewSection2LanguageField.bounds.size.height;
    DDLogInfo(@"self.tokenFieldheight");
  } else if (indexPath.section == 2) {
    DDLogInfo(@"return50");
    return 50;
  } else {
    DDLogInfo(@"self.tokenFieldheight2");
    return self.tableViewSection4SepecialtyField.bounds.size.height;
  }
}
- (UIView *)tableView:(UITableView *)tableView
    viewForFooterInSection:(NSInteger)section {
  UIView *sectionFooterView = [[UIView alloc]
      initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
  [sectionFooterView setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                        green:(115 / 255.f)
                                                         blue:(118 / 255.f)
                                                        alpha:1.0]];
  sectionFooterView.autoresizingMask = 0x3f;
  return sectionFooterView;
}
- (CGFloat)tableView:(UITableView *)tableView
    heightForFooterInSection:(NSInteger)section {
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForHeaderInSection:(NSInteger)section {
  return 50.0f;
}
#pragma mark Tableview section one header not scroll
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  CGFloat sectionHeaderHeight = 40;
  if (scrollView.contentOffset.y <= sectionHeaderHeight &&
      scrollView.contentOffset.y >= 0) {
    scrollView.contentInset =
        UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
  } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
    scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
  }
}
#pragma mark datestringtoMMMYYYFormat
- (NSString *)datestringtoMMMYYYFormat:(NSString *)SoldDateString {
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];

  NSDate *date = [dateFormatter dateFromString:SoldDateString];

  [dateFormatter setDateFormat:@"MMM YYYY"];
  NSString *strDate = [dateFormatter stringFromDate:date];
  return strDate;
}

#pragma mark JSTokenFieldDelegate (tableViewSection2And4TokenFieldSetting)-------------------------------------------------------------------------
- (void)tokenField:(JSTokenField *)tokenField
       didAddToken:(NSString *)title
 representedObject:(id)obj {
  NSDictionary *recipient =
      [NSDictionary dictionaryWithObject:title forKey:title];
  [self.tableViewSection2LanguageFieldArray addObject:recipient];

  //  DDLogInfo(@"\n%@", self.toRecipients);
}

- (void)tokenField:(JSTokenField *)tokenField
    didRemoveTokenAtIndex:(NSUInteger)index {
  DDLogInfo(@"didRemoveTokenAtIndex");
  [self.tableViewSection2LanguageFieldArray removeObjectAtIndex:index];
  // DDLogDebug(@"Deleted token %tu\n%@", index, self.toRecipients);
}
- (void)tokenField:(JSTokenField *)tokenField
    didRemoveToken:(NSString *)title
 representedObject:(id)obj {
  DDLogInfo(@"didRemoveToken");
}
- (void)tableViewSection2And4TokenFieldSetting {
  ////set language tokenField height to tableviewheight
  self.tableViewSection2LanguageField =
      [[JSTokenField alloc] initWithFrame:CGRectMake(0, 0, 280, 31)];

  [self.tableViewSection2LanguageField setDelegate:self];

  self.tableViewSection2LanguageField.textField.userInteractionEnabled = NO;

  if ([self.AgentProfile.AgentLanguage count] > 0) {
    //   DDLogInfo(@"self.AgentProfile tostring-->%@",[self.AgentProfile
    //   toJSONString]);
    //  DDLogInfo(@"self.AgentProfile-->%@",[self.AgentProfile toJSONString]);

    for (AgentLanguage *language in self.AgentProfile.AgentLanguage) {
      DDLogInfo(@"language.Language-->%@", language.Language);

      // [self.toField addTokenWithTitle:  language.Language representedObject:
      // language.Language];
      [self.tableViewSection2LanguageField
          addTokenWithTitlenotxbutton:language.Language
                    representedObject:language.Language];
    }

    [self.tableViewSection2LanguageField layoutSubviews];
  }
  ////set specialty tokenField height to tableviewheight
  self.tableViewSection4SepecialtyField =
      [[JSTokenField alloc] initWithFrame:CGRectMake(0, 0, 280, 31)];

  [self.tableViewSection4SepecialtyField setDelegate:self];

  self.tableViewSection4SepecialtyField.textField.userInteractionEnabled = NO;
  if ([self.AgentProfile.AgentSpecialty count] > 0) {
    for (AgentSpecialty *specialty in self.AgentProfile.AgentSpecialty) {
      //            DDLogInfo(@"specialty.specialty-->%@",specialty.specialty);
      //    [self.toField2 addTokenWithTitle: specialty.specialty
      //    representedObject:  specialty.specialty];
      [self.tableViewSection4SepecialtyField
          addTokenWithTitlenotxbutton:specialty.specialty
                    representedObject:specialty.specialty];
    }
    [self.tableViewSection4SepecialtyField layoutSubviews];
  }
}

#pragma mark Follow ------------------------------------------------------------------------------------------------------------------
- (IBAction)pressfolloworunfollow:(id)sender {
  if ([self.followbutton.titleLabel.text isEqualToString:JMOLocalizedString(@"agent_profile__unfollow_button", nil)]) {
    [self showUnfollowActionSheet];

  } else {
    [self callServerFollowApi];
  }
}
- (void)showUnfollowActionSheet {
  self.followActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                                         destructiveButtonTitle:JMOLocalizedString(@"agent_profile__unfollow_button", nil)
                                              otherButtonTitles:nil, nil];

  [self.followActionSheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet
    clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (actionSheet == self.followActionSheet) {
    if (buttonIndex == 0) {
      if ([self.delegate networkConnection]) {
        [self loadingAndStopLoadingAfterSecond:10];
        [self callServerUnFollowApi];
      }
    }
  } else {
    if (buttonIndex == 0) {
      if ([self.delegate networkConnection]) {
        [self loadingAndStopLoadingAfterSecond:10];
      }
      [self reportThisListingProblemToServer];
    }
  }
}

- (void)callServerFollowApi {
    if (![self.delegate networkConnection]) {
        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
    }
  [[FollowAgentModel shared]
      callFollowAgentApiByAgentProfileMemberID:
          [NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
      AgentProfileAgentListingAgentListingID:
          [NSString stringWithFormat:@"%d", self.AgentProfile.AgentListing
                                                .AgentListingID]
      success:^(id responseObject) {
        int const MYFollowerCount =
            self.delegate.currentAgentProfile.FollowerCount;
        [self.followbutton setTitle:JMOLocalizedString(@"agent_profile__unfollow_button", nil) forState:UIControlStateNormal];

      

        self.delegate.currentAgentProfile.FollowerCount = MYFollowerCount + 1;
           [self hideLoadingHUD];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                NSString *ok) {
           [self hideLoading];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                      errorMessage:errorMessage
                                                       errorStatus:errorStatus
                                                             alert:alert
                                                                ok:ok];

      }];
}
- (void)callServerUnFollowApi {

  [[UnFollowAgentModel shared]
      callUnFollowAgentApiByAgentProfileMemberID:
          [NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
      success:^(id responseObject) {
        int const MYFollowerCount =
            self.delegate.currentAgentProfile.FollowerCount;

        [self.followbutton setTitle:@"FOLLOW" forState:UIControlStateNormal];

       

        self.delegate.currentAgentProfile.FollowerCount = MYFollowerCount - 1;
           [self hideLoadingHUD];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                NSString *ok) {
           [self hideLoading];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                      errorMessage:errorMessage
                                                       errorStatus:errorStatus
                                                             alert:alert
                                                                ok:ok];

      }];
}

#pragma mark reportAgentProblem ------------------------------------------------------------------------------------------------------------
- (IBAction)reportAgentProblem:(id)sender {
  UIActionSheet *sheet =
      [[UIActionSheet alloc] initWithTitle:nil
                                  delegate:self
                         cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                    destructiveButtonTitle:nil
                         otherButtonTitles:@"Report Problem", nil];

  [sheet showInView:self.view];
}
- (void)reportThisListingProblemToServer {
  [[ReportProblemModel shared] callReportProblemApiByProblemType:@"4"
      toMemberID:[NSString stringWithFormat:@"%d", self.AgentProfile.MemberID]
      toListingID:[NSString
                      stringWithFormat:@"%d", self.AgentProfile.AgentListing
                                                  .AgentListingID]
      description:@""
      success:^(id responseObject) {

        [[[UIAlertView alloc] initWithTitle:@"Notice"
                                    message:@"Report Problem Successful"
                                   delegate:self
                          cancelButtonTitle:@"OK!"
                          otherButtonTitles:nil] show];

           [self hideLoadingHUD];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                NSString *ok) {
           [self hideLoading];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                      errorMessage:errorMessage
                                                       errorStatus:errorStatus
                                                             alert:alert
                                                                ok:ok];
      }];
}

#pragma mark pressChat(ChatPageDialog) ------------------------------------------------------------------------------------------------------------
- (IBAction)pressChat:(id)sender {
  if ([self.delegate networkConnection]) {
    [self loadingAndStopLoadingAfterSecond:10];
  }

  [[PressChatButtonModel shared]
   pressChatButtonModelByQBID:self.AgentProfile.QBID MemberName:self.AgentProfile.MemberName  MemberID:[NSString stringWithFormat:@"%d",self.AgentProfile.MemberID]
   success:^(Chatroom *chatroom) {
       //[self showPageControl:NO animated:NO];
       
                 [self.navigationController pushViewController:chatroom animated:YES];

           [self hideLoadingHUD];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                NSString *ok) {
           [self hideLoading];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                      errorMessage:errorMessage
                                                       errorStatus:errorStatus
                                                             alert:alert
                                                                ok:ok];
      }];
}
@end
