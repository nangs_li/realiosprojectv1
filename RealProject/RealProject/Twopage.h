//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "Onepage.h"
#import "ApartmentDetailViewController.h"
#import "AgentProfile.h"
#import "TableViewCell.h"
#import "MMDrawerController.h"
#import "AgentFilterViewController.h"
#import "AgentProfileViewController.h"
#import "HandleGoogleAutoCompleteJsonRow.h"
@class SPGooglePlacesAutocompleteQuery;

@interface Twopage
    : BaseViewController<UIScrollViewDelegate, UITableViewDataSource,
                         UITableViewDelegate, UISearchDisplayDelegate,
                         UISearchBarDelegate, MMDrawerTouchDelegate,UITextFieldDelegate,AgentFilterViewControllerDelegate> {
  NSArray *searchResultPlaces;
  SPGooglePlacesAutocompleteQuery *searchQuery;
  BOOL shouldBeginEditing;
}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *locationAddressSearchLoading;
@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) IBOutlet UITableView *addressTableView;
@property(strong, nonatomic) IBOutlet UIView *contentView;
@property(strong, nonatomic) IBOutlet UIView *topBarView;
@property(strong, nonatomic) IBOutlet UIView *searchContainerView;
@property (strong,nonatomic) IBOutlet UILabel *noResultLabel;
@property(strong, nonatomic) IBOutlet UIButton *searchDimButton;
@property(strong, nonatomic) IBOutlet UIButton *testButton;
@property(nonatomic, strong) IBOutlet UIButton *filterButton;
@property(nonatomic,strong) IBOutlet UITextField *searchTextField;
@property(strong, nonatomic) IBOutlet UIView *norecordpage;
@property(assign, nonatomic) UIViewAnimationOptions animationType;
@property(nonatomic, retain) NSMutableArray *sloganarray;
#pragma mark swipe animation core
@property(nonatomic, assign) int currentddirection;
@property(nonatomic, strong) UIImageView *imageFromView;
@property(nonatomic, retain) AgentProfileViewController *Onepageviewcontroller;
@property(nonatomic, retain) ApartmentDetailViewController *Threepageviewcontroller;
@property(nonatomic, strong) NSIndexPath *indexPathForCell;
@property(nonatomic, assign) BOOL tableviewcellfade;
@property(strong, nonatomic) NSMutableArray * searchResultPlacesHistoryArray;
@property(strong, nonatomic) NSMutableArray * topFiveCitiesList;
#pragma mark googlereturnplaceapi;
@property(nonatomic, retain) NSArray *googlereturnplaceapi;
@property(nonatomic, retain) NSString *userInputSearchAddress;
@property(nonatomic, strong) NSArray<HandleGoogleAutoCompleteJsonRow>  *realServerGoogleAutoCompleteApi;
@property(nonatomic, retain) MMDrawerController *MMDrawerController;

#pragma mark - server return server data array
@property(nonatomic, strong) NSMutableArray *agentprofilearray;
#pragma tabbar Delegate-
@property(nonatomic, assign) int  tapCounter;
@property(nonatomic, assign) BOOL insertRowAtBottomEnable;
@property(nonatomic, strong) UIViewController * previousHandledViewController;
//#pragma mark - server otherpagepanHandler
//-(void) otherpagepanHandler: (UIPanGestureRecognizer *)gesture;
#pragma mark only one language pickerData
@property(strong, nonatomic) NSArray *pickerData;
- (void)removeAllImageFromView;
- (IBAction)filterButtonDidPress:(id)sender;
- (IBAction)testButtonDidPress:(id)sender;
- (void)setupNavBar;
@end
