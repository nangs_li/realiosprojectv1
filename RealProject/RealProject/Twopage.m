//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Twopage.h"
#import "TableViewCell.h"

#import "UITableView+Utility.h"
#import "Onepage.h"
#import "SearchAgentProfileModel.h"
#import "Threepage.h"
#import <QuartzCore/QuartzCore.h>
#import "UIScrollView+SVPullToRefresh.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import <CoreLocation/CoreLocation.h>
#import "HandleServerReturnString.h"
#import "AgentProfile.h"
#import "AgentListing.h"
#import "Onepage.h"
#import "Threepage.h"
#import "AddressUserInput.h"
#import "Reachability.h"
#import "AgentCell.h"
#import "UIView+Utility.h"
#import "JSBadgeView.h"
#import "RealUtility.h"
#import "BaseView.h"
#import "AgentListSectionHeaderView.h"
#import "DBSearchAgentHistory.h"
#import "GetContryLocation.h"
#import "ODRefreshControl.h"
#import "DBSearchAgentHistory.h"
#import <PureLayout/PureLayout.h>
#import "AddressSuggestionSearchCell.h"
#import "RealPrefetcher.h"
#import "DBHasBeenDownloadPhoto.h"
#import "DBHaveShowLanding.h"
#import "DBSearchAddressArray.h"
#define maxCardSize 88.0f+[RealUtility screenBounds].size.width
#define googleSearchDelay 0.7

// Mixpanel
#import "Mixpanel.h"

@interface Twopage ()<CLLocationManagerDelegate, RDVTabBarControllerDelegate,AgentListSectionHeaderDelegate,MoveToCenterPage,AgentCellDelegate,RealPrefetcherDelegate> {
    CLLocationManager *locationManager;
}
@property(nonatomic, strong) UILabel *searchAgentCountLabel;
@property(nonatomic) CGPoint viewInputCenter;
@property(nonatomic, assign) CGRect preferCardFrame;
@property(nonatomic, strong) AgentFilterViewController *agentFilterViewController;
@property (nonatomic, strong) NSString *searchGooglePlaceId;
@property(nonatomic, strong) NSTimer *googleAPITimer;
@property(nonatomic, assign) BOOL testModeOn;
@property(nonatomic, assign) BOOL serachContainerViewDidShow;
@property (nonatomic,assign) BOOL detectScrollAction;
@property(nonatomic, strong) JSBadgeView *filterBadgeView;
@property (nonatomic, strong) NSMutableDictionary *sectionHeaderDict;
@property (nonatomic, strong) NSMutableDictionary *sectionHeaderOffsetDict;
@property (nonatomic, assign) NSInteger currentScrollSection;
@property (nonatomic, assign) int totalAgentCount;
@property (nonatomic, assign) CGFloat headerHeight;
@property (nonatomic, assign) BOOL searchKeyDidPress;
@property (nonatomic, strong) NSString *pendingSearchText;
@property (nonatomic, assign) CGFloat previousScrollViewYOffset;
@property (nonatomic, assign) BOOL animatedNavBarHeight;
@property (nonatomic,strong) NSDictionary *filterDict;
@property (nonatomic,strong) ODRefreshControl *topRefreshControl;
@end
@implementation Twopage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
#pragma mark locaiton init
        
        searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
        searchQuery.radius = 5000.0;
        shouldBeginEditing = YES;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.tableviewcellfade = NO;
    [self tableViewReloadAnimationTableView:self.tableView];
    
    self.MMDrawerController.touchDelegate = self;
    [self.delegate getTabBarController].delegate = self;
    [[GetContryLocation shared]getContryLocation];
    if (![self isEmpty:[SearchAgentProfileModel shared].location]) {
        searchQuery.location = CLLocationCoordinate2DMake([SearchAgentProfileModel shared].location.coordinate.latitude, [SearchAgentProfileModel shared].location.coordinate.longitude);
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        //        [self showPageControl:NO animated:NO];
        //        [[self.delegate getTabBarController]setNavBarHidden:YES animated:NO];
        self.navBarContentView.alpha =0;
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.delegate getTabBarController].customNavBarController.delegate=self;
    
    self.detectScrollAction = NO;
    [RealPrefetcher sharePrefetcher].delegate = self;
    [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:[SearchAgentProfileModel shared].AgentProfiles clearQueue:YES];
    [self loadImagesForOnscreenRows];
    if(self.MMDrawerController.openSide == MMDrawerSideNone){
        [[self.delegate getTabBarController]setNavBarHidden:NO animated:NO];
        [[self.delegate getTabBarController]setTabBarHidden:NO animated:NO];
        [UIView performWithoutAnimation:^{
            [self setupNavBar];
            self.navBarContentView.alpha =1;
        }];
        [[self.delegate getTabBarController]changePage:1];
    }
    
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        [self showPageControl:![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles]  animated:self.MMDrawerController.openSide == MMDrawerSideNone];
    }
    if (self.MMDrawerController.openSide != MMDrawerSideNone) {
        [self.MMDrawerController openDrawerSide:self.MMDrawerController.openSide animated:NO completion:nil];
    }
}


- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    //    self.detectScrollAction = YES;
}

-(void)setupNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.searchNavBar;
            self.searchTextField = navBarController.searchNavBarTextField;
            self.searchTextField.returnKeyType= UIReturnKeySearch;
            self.searchTextField.delegate = self;
            if ([[SearchAgentProfileModel shared].userInputSearchAddress valid]) {
                self.searchTextField.text=[SearchAgentProfileModel shared].userInputSearchAddress;
                [SearchAgentProfileModel shared].userInputSearchAddress=nil;
            }
            self.filterButton = navBarController.searchNavBarFilterButton;
            [self.filterButton addTarget:self action:@selector(filterButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
            //            DBSearchAddressArray * dbSearchAddressArray;
            //            DBResultSet * dbSearchAddressArrayResult = [[[[DBSearchAddressArray query]
            //                                                          whereWithFormat:@"MemberID               = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] limit:1]fetch];
            //
            //            for (DBSearchAddressArray * dbSearchAddressArrayFromDB in dbSearchAddressArrayResult) {
            //                dbSearchAddressArray=dbSearchAddressArrayFromDB;
            //            }
            //            &&[dbSearchAddressArray.AddressSearchType isEqualToString:kAddressSearchType]
            
            [self setupTextField];
            
            [self checkDBSearchAgentHistoryHideFilterAndAgentCountDisablePageing];
            
        }
    }];
}
-(void)checkDBSearchAgentHistoryHideFilterAndAgentCountDisablePageing{
    if ([[[DBSearchAgentHistory query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID]  count]!=0) {
        [self.locationAddressSearchLoading stopAnimating];
        self.filterButton.enabled=YES;
        self.searchAgentCountLabel.hidden=NO;
        self.tableView.showsInfiniteScrolling = YES;
        
    }else{
        self.filterButton.enabled=NO;
        //        self.searchAgentCountLabel.hidden=YES;
        [self updateSearchAgentCount:0];
        self.tableView.showsInfiniteScrolling = NO;
        self.searchTextField.text=nil;
    }
    
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    [self setupTextField];
    [self updateNoRecordLabelWithFilter:[self.filterDict valid]];
    self.noResultLabel.text = JMOLocalizedString(@"search_result_detail__no_results_founds", nil);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(followOrUnfollowReloadTableView)
     name:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(addressSearchUpdate:)
     name:kNotificationAddressSearchUpdate
     object:nil];
    self.headerHeight = sectionHeaderHeight;
    [self setupTextField];
    CGRect cardFrame =  CGRectMake(0, 0, [RealUtility screenBounds].size.width, [RealUtility screenBounds].size.height - 64-44);
    if (cardFrame.size.height >maxCardSize) {
        cardFrame.size.height = maxCardSize;
    }
    self.preferCardFrame = cardFrame;
    self.currentddirection = 0;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.contentInset =UIEdgeInsetsMake(0, 0, 49+sectionHeaderHeight, 0);
    __weak Twopage *weakSelf = self;
    if (!self.topRefreshControl) {
        self.topRefreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
        [self.topRefreshControl addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    }
    
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    self.agentprofilearray = [[NSMutableArray alloc] init];
    [self showInfiniteRefreshIfNeed];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil]
     setTextColor:self.delegate.Greycolor];
    
    [self setupTapGestureRecognizer];
}

- (void)setupTapGestureRecognizer {
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self
                                                 action:@selector(didTapOnTableView:)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.addressTableView addGestureRecognizer:gestureRecognizer];
}

- (void)setupTextField {
    [UIView performWithoutAnimation:^{
        NSAttributedString *search = [[NSAttributedString alloc]
                                      initWithString:JMOLocalizedString(@"search_result_detail__search_field_hint", nil)
                                      attributes:@{
                                                   NSForegroundColorAttributeName : [UIColor lightGrayColor]
                                                   }];
        self.searchTextField.attributedPlaceholder = search;
        UIImageView *searchIconImageView =
        [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 18.0f)];
        searchIconImageView.contentMode = UIViewContentModeScaleAspectFit;
        searchIconImageView.image = [UIImage imageNamed:@"icon-search"];
        if (!self.searchTextField.leftView) {
            [self.searchTextField setLeftView:searchIconImageView];
            [self.searchTextField setLeftViewMode:UITextFieldViewModeAlways];
        }
        if (!self.searchTextField.rightView) {
            self.searchAgentCountLabel = [[UILabel alloc] init];
            self.searchAgentCountLabel.textColor = [UIColor whiteColor];
            self.searchAgentCountLabel.font = [UIFont systemFontOfSize:10.0f];
            //    [self.searchAgentCountLabel sizeToFit];
            [self.searchTextField setRightView:self.searchAgentCountLabel];
            [self.searchTextField setRightViewMode:UITextFieldViewModeUnlessEditing];
        }
        [self updateSearchAgentCount:self.totalAgentCount];
    }];
    //    [self.searchTextField addTarget:self
    //                             action:@selector(textFieldDidChange:)
    //                   forControlEvents:UIControlEventEditingChanged];
}
-(void)addressSearchUpdate:(NSNotification*)notification{
    NSDictionary *userInfo = notification.userInfo;
    BOOL success = [userInfo[@"success"]boolValue];
    NSArray *agentProfile = userInfo[@"agentProfile"];
    NSString *totalCountString = userInfo[@"totalCount"];
    if (success) {
        if (![self isEmpty:[SearchAgentProfileModel shared].location]) {
            searchQuery.location = CLLocationCoordinate2DMake([SearchAgentProfileModel shared].location.coordinate.latitude, [SearchAgentProfileModel shared].location.coordinate.longitude);
        }
        [self.locationAddressSearchLoading stopAnimating];
        [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:agentProfile clearQueue:YES];
        [self loadImagesForOnscreenRows];
        [self tableViewReloadAnimationTableView:self.tableView];
        if (![self isEmpty:agentProfile]) {
            int totalCount =(int)agentProfile.count;
            self.tableView.scrollEnabled = totalCount >0;
            if (totalCount <= 0 ) {
                [self showNoRecord];
            }else{
                [self hideNoRecord];
            }
            
            [self showInfiniteRefreshIfNeed];
            [self.topRefreshControl endRefreshing];
            //   self.afterFollowOrUnFollowReloadindexPathForCell = nil;
            if ([totalCountString valid])
                [self updateSearchAgentCount:[totalCountString intValue]];
            [self showPageControl:![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] animated:YES];
            [self resetSectionHeaderView];
            [self tableViewReloadAnimationTableView:self.tableView];
            //    [self.tableView  setContentOffset:CGPointZero animated:YES];
            self.indexPathForCell = nil;
            [self hideLoading];
            
        }
    }else{
        [self showNoRecord];
        [self.locationAddressSearchLoading stopAnimating];
        
        
    }
    [self checkDBSearchAgentHistoryHideFilterAndAgentCountDisablePageing];
    
}
- (void)updateSearchAgentCount:(int)count {
    self.totalAgentCount = count;
    if (count > 0) {
        
        if (count>1) {
            self.searchAgentCountLabel.text =[NSString stringWithFormat:@"%d %@ ", count,JMOLocalizedString(@"common__agents", nil)];
        }else{
            self.searchAgentCountLabel.text =[NSString stringWithFormat:@"%d %@ ", count,JMOLocalizedString(@"common__agent", nil)];
        }
        
    } else if (count > 1000) {
        self.searchAgentCountLabel.text =  [NSString stringWithFormat:@"1000+ %@",JMOLocalizedString(@"common__agents", nil)];
    } else {
        self.searchAgentCountLabel.text = @"";
    }
    [self.searchAgentCountLabel sizeToFit];
    CGRect labelFrame = self.searchAgentCountLabel.frame;
    labelFrame.size.width += 8;
    self.searchAgentCountLabel.frame = labelFrame;
}

- (void)showSloganInCell:(BOOL)show{
    return; /* disable  */
    NSArray *visibleCell = [[self.tableView visibleCells]copy];
    for (AgentCell *cell in visibleCell) {
        [cell showSlogan:show animated:YES];
    }
}

#pragma house swipe animation \
start-- --------------------------------------------------------------------------------
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.Threepageviewcontroller.didChangeToImageTableView ||self.presentedViewController ||!self.norecordpage.hidden ||self.serachContainerViewDidShow||self.delegate.disableViewGesture) {
        return NO;
    }
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [gestureRecognizer locationInView:self.tableView];
#pragma mark if twopage then
    if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:panLocation];
        if (indexPath) {
            AgentProfile *profile = [SearchAgentProfileModel shared].AgentProfiles[indexPath.section];
            
            return profile.readyToShow;
        }
        return NO;
    } else {
        return YES;
    }
}


- (void)panGestureCallback:(UIPanGestureRecognizer *)panGesture {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
            DDLogInfo(@"UIGestureRecognizerStateBegan");
#pragma mark refresh after follow  or unfollow only one tableviewcell  threepage start pangesture
            if (screenRect.size.width > panLocation.x) {
                if (![self isEmpty:self.indexPathForCell]) {
                    //                    self.tableviewcellfade = NO;
                    //                    [self.tableView beginUpdates];
                    //                    [self.tableView reloadRowsAtIndexPaths:@[ self.indexPathForCell ]
                    //                                          withRowAnimation:UITableViewRowAnimationNone];
                    //                    [self.tableView endUpdates];
                    //                    self.tableviewcellfade = YES;
                    [self.tableView reloadData];
                }
            }
#pragma mark get self.view pangesture x   twopage start pangesture
            
            if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
                DDLogInfo(@"-panLocationstart%f", panLocation.x);
                CGPoint panLocation = [panGesture locationInView:self.tableView];
                
                NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:panLocation];
                [self setupPagesWithIndexPath:indexPath];
                
            }
            break;
            
        case UIGestureRecognizerStateChanged:
            if (self.MMDrawerController.openSide == MMDrawerSideNone) {
                
            }
            break;
            
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            break;
            
        default:
            break;
    }
}

- (void)finishPanGestureCallBack:(UIPanGestureRecognizer *)panGesture {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
        [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
        [self hideSpotlight];
    }
    MMDrawerSide drawerSide= self.MMDrawerController.openSide;
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}

- (BOOL)setupPagesWithIndexPath:(NSIndexPath*)indexPath{
    self.indexPathForCell = indexPath;
    
    if (![self isEmpty:indexPath]) {
        if ([self isEmpty:[SearchAgentProfileModel shared].AgentProfiles[indexPath.section]]) {
            return NO;
        }
        
        self.delegate.currentAgentProfile =
        [SearchAgentProfileModel shared].AgentProfiles[indexPath.section];
        if (![self.delegate.currentAgentProfile haveAgentListing]) {
            return NO;
        }
        
        DDLogInfo(@"indexPathindexPathindexPath%@", indexPath);
        self.Onepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
        self.Threepageviewcontroller.pendingAgentProfile = self.delegate.currentAgentProfile;
        
    }else{
        return NO;
    }
    //
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    rectInSuperview.size.height += 53;
    rectInSuperview.origin.y -=53;
    [self showSpotlightInRect:rectInSuperview];
    [[self.delegate getTabBarController]adjustNavBarHeight:RealNavBarHeightWithPageControl animated:NO];
    self.animatedNavBarHeight = YES;
    //    DDLogDebug(@"cellRect:%@",NSStringFromCGRect(rectInSuperview));
    return YES;
}

- (void)openSideDidChange:(MMDrawerSide)drawerSide{
    [self changePage:drawerSide];
}

#pragma mark imageFromView
- (UIImage *)imageFromView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO,
                                           [[UIScreen mainScreen] scale]);
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (void)removeAllImageFromView {
    NSArray *subViewArray = [self.delegate.window subviews];
    for (id obj in subViewArray) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            [obj removeFromSuperview];
        }
    }
}
#pragma house swipe animation \
end-- --------------------------------------------------------------------------------

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Animate in the table view.
    [self showSearchController];
    return YES;
}  // return NO to disallow editing.
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}  // became first responder
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self dismissSearchControllerWhileStayingActive];
    return YES;
}  // return YES to allow editing to stop and to resign first responder status.
// NO to disallow the editing session to end
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (![self.searchTextField.text valid]) {
        DBResultSet * dbSearchAddressArrayResult = [[[[DBSearchAddressArray query]
                                                      whereWithFormat:@"MemberID               = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID ] limit:1]fetch];
        
        for (DBSearchAddressArray * dbSearchAddressArray in dbSearchAddressArrayResult) {
            self.searchTextField.text=dbSearchAddressArray.UserInputSearchAddress;
            int totalCount =(int)[SearchAgentProfileModel shared].searchAgentProfilesTotalCount;
            [self updateSearchAgentCount:totalCount];
        }
    }
}  // may be called if forced even if shouldEndEditing returns NO (e.g. view
// removed from window) or endEditing:YES called

- (void)searchTextFieldDidChange:(NSString*)string {
    
    [self handleSearchForSearchString:string withDelay:googleSearchDelay];
    [self loadSearchAgentHistory];
}
- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string {
    if (textField == self.searchTextField){
        NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        [self searchTextFieldDidChange:proposedNewString];
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self resetSearchAddress];
    
    return YES;
}  // called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    if ([RealUtility isValid:searchResultPlaces] && [self.pendingSearchText isEqualToString:textField.text]) {
        [self tableView:self.addressTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }else if(textField.text.length >0){
        self.searchKeyDidPress = YES;
        [self showLoading];
        [self handleSearchForSearchString:textField.text withDelay:googleSearchDelay];
    }
    return YES;
}  // called when 'return' key pressed. return NO to ignore.

- (void)resetSearchAddress {
    [self updateSearchAgentCount:0];
    searchResultPlaces = nil;
    self.userInputSearchAddress = nil;
    [self.addressTableView reloadData];
}

- (void)reloadAddressTableView {
    [UIView transitionWithView:self.addressTableView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        [self.addressTableView reloadData];
                    }
                    completion:nil];
}
-(void)loadSearchAgentHistory{
    
    if (self.userInputSearchAddress.length == 0) {
        
        
        DBResultSet * r =[[[[[DBSearchAgentHistory query]
                             whereWithFormat:@" MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]orderByDescending:@"Id"  ]limit:20]fetch];
        
        self.searchResultPlacesHistoryArray= [[NSMutableArray alloc]init];
        
        NSMutableString *top5SearchString = [[NSMutableString alloc] init];
        
        for (DBSearchAgentHistory * dbSearchAgentHistory in r) {
            
            [self.searchResultPlacesHistoryArray addObject:dbSearchAgentHistory];
            
            if (dbSearchAgentHistory != [r firstObject]) {
                
                [top5SearchString appendString:@" | "];
            }
            
            [top5SearchString appendString:dbSearchAgentHistory.GooglePlaceAutoCompleteAddress];
        }
        
        if ([top5SearchString length] > 0) {
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel.people set:@"Top 5 search" to:top5SearchString];
        }
        
        if (r.count==0) {
            self.topFiveCitiesList = [[NSMutableArray alloc]init];
            for (TopFiveCitiesList *fiveCitiesList in [SystemSettingModel shared].SystemSettings.TopFiveCitiesList) {
                
                [self.topFiveCitiesList addObject:fiveCitiesList];
                
            }
            
        }
        
        
        [self reloadAddressTableView];
    }
    
}
#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.detectScrollAction = YES;
    [self showSloganInCell:NO];
}
- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.detectScrollAction && scrollView == self.tableView && [SearchAgentProfileModel shared].AgentProfiles.count>0) {
        NSIndexPath *firstVisibleIndexPath = [self.tableView indexPathsForVisibleRows].firstObject;
        CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:firstVisibleIndexPath];
        CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
        CGFloat cellOffset = rectInSuperview.origin.y - sectionHeaderHeight;
        CGFloat infoViewOffset = fabs(cellOffset) - [RealUtility screenBounds].size.width;
        UIView *headerContainerView;
        AgentListSectionHeaderView *topHeaderView;
        headerContainerView =  [self getSectionHeaderViewWithProfile:nil withSection:firstVisibleIndexPath.section];
        topHeaderView = [headerContainerView.subviews firstObject];
        [topHeaderView adjustHeaderOffset:infoViewOffset];
        [self recordHeaderOffset:infoViewOffset section:firstVisibleIndexPath.section];
    }
}

- (void)loadImagesForOnscreenRows{
    if ([SearchAgentProfileModel shared].AgentProfiles.count > 0 && self.visible){
        NSMutableArray *visiblePaths = [[self.tableView indexPathsForVisibleRows] mutableCopy];
        NSMutableArray *listings = [[NSMutableArray alloc]init];
        for (NSIndexPath *indexPath in visiblePaths){
            if ([SearchAgentProfileModel shared].AgentProfiles.count > indexPath.section) {
                AgentProfile *agentProfile =[[SearchAgentProfileModel shared].AgentProfiles objectAtIndex:indexPath.section];
                if (agentProfile.readyToShow && [agentProfile.AgentListing valid]) {
                    [listings addObject:agentProfile.AgentListing];
                }
            }
        }
        [[RealPrefetcher sharePrefetcher]prefetchAgentListings:listings allContent:NO clearQueue:YES top:NO];
    }
}

- (void)shouldShowAgentProfile:(AgentProfile*)agentProfile atIndexPath:(NSIndexPath*)indexPath{
    if(self.visible && [SearchAgentProfileModel shared].AgentProfiles.count > indexPath.section ){
        NSMutableArray *visiblePaths = [[self.tableView indexPathsForVisibleRows] mutableCopy];
        if (!agentProfile.didShow && agentProfile.readyToShow && [visiblePaths containsObject:indexPath]) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                agentProfile.didShow = YES;
                [self.tableView reloadData];
                [self getSectionHeaderViewWithProfile:agentProfile withSection:indexPath.section];
            }];
            if (agentProfile.AgentListing) {
                [[RealPrefetcher sharePrefetcher]prefetchAgentListings:@[agentProfile.AgentListing] allContent:NO clearQueue:NO top:NO];
            }
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //    [self stoppedScrolling];
    //    self.detectScrollAction = NO;
    [self loadImagesForOnscreenRows];
    [self showSloganInCell:YES];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        //        self.detectScrollAction = NO;
        
        [self loadImagesForOnscreenRows];
        [self showSloganInCell:YES];
        //        [self stoppedScrolling];
    }
}

- (void)stoppedScrolling{
    //    CGRect frame = self.navBarContentView.frame;
    //    if (frame.origin.y >-8) {
    //        [[self.delegate getTabBarController]adjustNavBarHeight:RealNavBarHeightWithPageControl animated:YES];
    //    }else{
    //       [[self.delegate getTabBarController]adjustNavBarHeight:34 animated:YES];
    //    }
}

- (void)updateBarButtonItems:(CGFloat)alpha
{
    [self.navigationItem.leftBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
        item.customView.alpha = alpha;
    }];
    [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
        item.customView.alpha = alpha;
    }];
    self.navigationItem.titleView.alpha = alpha;
    self.navigationController.navigationBar.tintColor = [self.navigationController.navigationBar.tintColor colorWithAlphaComponent:alpha];
}

- (void)animateNavBarTo:(CGFloat)y
{
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.navigationController.navigationBar.frame;
        CGFloat alpha = (frame.origin.y >= y ? 0 : 1);
        frame.origin.y = y;
        [self.navigationController.navigationBar setFrame:frame];
        [self updateBarButtonItems:alpha];
    }];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.tableView == tableView) {
        return  [SearchAgentProfileModel shared].AgentProfiles.count;
    }else{
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    // set number of rows
    
    if (tableView == self.tableView) {
        
        if (![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles]) {
            return 1;
        } else {
            return 0;
        }
    } else {
        if (self.userInputSearchAddress.length == 0) {
            if ([[[DBSearchAgentHistory query]
                  whereWithFormat:@" MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]count]==0) {
                return self.topFiveCitiesList.count;
            }
            if (self.searchResultPlacesHistoryArray.count>5) {
                return 5;
            }
            return self.searchResultPlacesHistoryArray.count;
        }
        if ([[GetContryLocation shared] contryLocationISChina]) {
            return self.realServerGoogleAutoCompleteApi.count;
        }
        return [searchResultPlaces count];
    }
}

-(void)showNoRecord{
    self.norecordpage.hidden = NO;
    self.tableView.hidden = YES;
    [self updateNoRecordLabelWithFilter:[self.filterDict valid]];
}

- (void)updateNoRecordLabelWithFilter:(BOOL)appliedFilter{
    if (appliedFilter) {
        self.noResultLabel.text = JMOLocalizedString(@"search_result_detail__filter_no_results_founds", nil);
    }else{
        self.noResultLabel.text = JMOLocalizedString(@"search_result_detail__no_results_founds", nil);
    }
}

-(void)hideNoRecord{
    self.norecordpage.hidden = YES;
    self.tableView.hidden = NO;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.tableView) {
        AgentProfile *agentProfile = [[SearchAgentProfileModel shared].AgentProfiles objectAtIndex:section];
        return [self getSectionHeaderViewWithProfile:agentProfile withSection:section];
    }else{
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.tableView) {
        return self.headerHeight;
    }else{
        return 0;
    }
}
- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    // DDLogInfo(@"placeAtIndexPath");
    return [searchResultPlaces objectAtIndex:indexPath.row];
}
- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set height to 116
    if (tableView == self.tableView) {
        return self.preferCardFrame.size.height;
    } else {
        return 44;
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    if ([cell isKindOfClass:[AgentCell class]]) {
        AgentCell *agentCell = (AgentCell*)cell;
        [agentCell willEndDisplay];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[AgentCell class]]) {
        AgentCell *agentCell = (AgentCell*)cell;
        [agentCell resetLoadingStatus];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        static NSString *CellIdentifier = @"TableViewCell";
        if (![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] ||
            self.testModeOn) {
            
            AgentProfile *agentProfile =
            [[SearchAgentProfileModel shared]
             .AgentProfiles objectAtIndex:indexPath.section];
            
            AgentCell *agentCell =
            [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!agentCell) {
                agentCell = [[AgentCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:CellIdentifier
                                                   withFrame:self.preferCardFrame
                                                 withProfile:agentProfile];
                agentCell.agentProfileView.backgroundButton.hidden = NO;
            }
            [agentCell showSlogan:!self.detectScrollAction animated:NO];
            agentCell.delegate = self;
            agentCell.selectionStyle = UITableViewCellSelectionStyleNone;
            agentCell.backgroundView = nil;
            [agentCell configureWithAgentProfile:agentProfile];
            if (!agentProfile.readyToShow) {
                [agentCell willEndDisplay];
            }else{
                [agentCell resetLoadingStatus];
            }
            return agentCell;
            
        } else {
            AgentCell *agentCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return agentCell;
        }
        
    } else {
#pragma mark PGooglePlacesAutocomplete TableView
        static NSString *cellIdentifier = @"AddressSuggestionSearchCell";
        AddressSuggestionSearchCell *cell =
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddressSuggestionSearchCell"
                                                         owner:self
                                                       options:nil];
            cell = (AddressSuggestionSearchCell *)[nib objectAtIndex:0];
        }
        
        
        if (self.userInputSearchAddress.length ==0) {
            if ([[[DBSearchAgentHistory query]
                  whereWithFormat:@" MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]count]==0) {
                
                
                TopFiveCitiesList *fiveCitiesList = [self.topFiveCitiesList objectAtIndex:indexPath.row];
                
                
                cell.suggestAddress.text =fiveCitiesList.CityName;
                
                
                if (fiveCitiesList.AgentCount > 0) {
                    
                    if (fiveCitiesList.AgentCount>1) {
                        cell.agentCount.text =[NSString stringWithFormat:@"%d %@ ", fiveCitiesList.AgentCount,JMOLocalizedString(@"common__agents", nil)];
                    }else{
                        cell.agentCount.text =[NSString stringWithFormat:@"%d %@ ", fiveCitiesList.AgentCount,JMOLocalizedString(@"common__agent", nil)];
                    }
                    
                } else if (fiveCitiesList.AgentCount > 1000) {
                    cell.agentCount.text =  [NSString stringWithFormat:@"1000+ %@",JMOLocalizedString(@"common__agents", nil)];
                } else {
                    cell.agentCount.text = @"";
                }
            }else{
                DBSearchAgentHistory  * dBSearchAgentHistory=     [self.searchResultPlacesHistoryArray objectAtIndex:indexPath.row];
                
                cell.suggestAddress.text =dBSearchAgentHistory.GooglePlaceAutoCompleteAddress;
                if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
                    [cell.suggestAddress autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
                }
                
            }
        }else{
            
            
            if ([[GetContryLocation shared]contryLocationISChina]) {
                HandleGoogleAutoCompleteJsonRow * handleGoogleAutoCompleteJsonRow=[self.realServerGoogleAutoCompleteApi objectAtIndex:indexPath.row];
                
                DDLogDebug(@"handleGoogleAutoCompleteJsonRow %@",handleGoogleAutoCompleteJsonRow);
                cell.textLabel.font = [UIFont fontWithName:@"System" size:16.0];
                
                cell.suggestAddress.text =handleGoogleAutoCompleteJsonRow.googleDescription;
                
                
            }else{
                
                cell.textLabel.font = [UIFont fontWithName:@"System" size:16.0];
                
                cell.suggestAddress.text = [self placeAtIndexPath:indexPath].name;
                if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
                    [cell.suggestAddress autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
                }
            }
        }
        
        return cell;
    }
}


-(void)showInfiniteRefreshIfNeed{
    
    if ([[[DBSearchAgentHistory query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID]  count]!=0) {
        
        int totalCount =(int)[SearchAgentProfileModel shared].searchAgentProfilesTotalCount;
        int currentCount = (int)[SearchAgentProfileModel shared].AgentProfiles.count;
        if (totalCount > currentCount) {
            self.tableView.showsInfiniteScrolling = YES;
        }else{
            self.tableView.showsInfiniteScrolling = NO;
        }
        
    }else{
        
        self.tableView.showsInfiniteScrolling = NO;
        
    }
    
}

-(void)showPullToRefresh{
    //    if (!self.topRefreshControl.superview) {
    //    [self.tableView addSubview:self.topRefreshControl];
    //    [self.tableView sendSubviewToBack:self.topRefreshControl];
    //    }
}

-(void)hidePullToRefresh{
    //    [self.topRefreshControl removeFromSuperview];
}
-(void)searchByGooglePlaceID:(NSString*)placeId placeName:(NSString *)placeName withFilterDict:(NSDictionary*)filterDict showLoading:(BOOL)showLoading{
    
    self.searchGooglePlaceId = placeId;
    [self showPullToRefresh];
    if (showLoading) {
        [self showLoading];
        [SearchAgentProfileModel shared].AgentProfiles=nil;
        [self.tableView reloadData];
    }
    
    self.filterDict = filterDict;
    if (![RealUtility isValid:filterDict]) {
        
        [[SearchAgentProfileModel shared]callGeoCodeapiConverFromPlaceIDAndAddressSearch:placeId addressSearch:self.searchTextField.text succes:^(NSMutableArray<AgentProfile> *AgentProfiles) {
            
            
            // Mixpanel
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            
            NSString *searchQueryString = placeName;
            
            if (searchQueryString == nil) {
                
                searchQueryString = @"";
            }
            
            [mixpanel track:@"Searched Area"
                 properties:@{
                              @"Search query": searchQueryString,
                              @"Filter applied": @"NO",
                              @"# of results returned": [NSNumber numberWithInt:[SearchAgentProfileModel shared].searchAgentProfilesTotalCount]
                              }];
            
            [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:AgentProfiles clearQueue:YES];
            [self loadImagesForOnscreenRows];
            int totalCount =(int)[SearchAgentProfileModel shared].searchAgentProfilesTotalCount;
            self.tableView.scrollEnabled = totalCount >0;
            if (totalCount <= 0 ) {
                [self showNoRecord];
            }else{
                [self hideNoRecord];
            }
            [self updateSearchAgentCount:totalCount];
            [self.topRefreshControl endRefreshing];
            
            [self showInfiniteRefreshIfNeed];
            //   self.afterFollowOrUnFollowReloadindexPathForCell = nil;
            [self showPageControl:![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] animated:YES];
            [self resetSectionHeaderView];
            [self tableViewReloadAnimationTableView:self.tableView];
            [self.tableView  setContentOffset:CGPointZero animated:YES];
            self.indexPathForCell = nil;
            [self hideLoading];
            [self checkDBSearchAgentHistoryHideFilterAndAgentCountDisablePageing];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            [self.delegate
             handleModelReturnErrorShowUIAlertViewByView:self.view
             errorMessage:errorMessage
             errorStatus:errorStatus
             alert:alert
             ok:ok];
            [self updateSearchAgentCount:0];
            self.tableView.scrollEnabled = NO;
            [self showInfiniteRefreshIfNeed];
            [self hideLoading];
            [self showPageControl:![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] animated:YES];
            [self resetSectionHeaderView];
            [self showNoRecord];
            [self tableViewReloadAnimationTableView:self.tableView];
        }];
        
    }else{
        NSMutableArray *propertyArray = nil;
        NSMutableArray *priceArray =nil;
        int sizeMax =0;
        int sizeMin = 0;
        int bedroomCount = 0;
        int bathroomCount = 0;
        int spokenLanugaeIndex =0;
        int sizeUnitType = 0;
        NSString*currencyUnit = nil;
        
        if ([filterDict objectForKey:@"bathroom"]) {
            bathroomCount = [[filterDict objectForKey:@"bathroom"]intValue];
        }
        
        if ([filterDict objectForKey:@"bedroom"]) {
            bedroomCount = [[filterDict objectForKey:@"bedroom"]intValue];
        }
        
        if ([filterDict objectForKey:@"price"]) {
            NSDictionary *priceDict = [filterDict objectForKey:@"price"];
            priceArray =[[NSMutableArray alloc]init];
            NSString *priceMin = priceDict[@"min"];
            //            priceMin = [NSString stringWithFormat:@"%d",[RealUtility abbreviateToNumber:priceMin]];
            NSString *priceMax = priceDict[@"max"];
            NSString *priceUnit = priceDict[@"unit"];
            currencyUnit = priceUnit;
            //            priceMax =  [NSString stringWithFormat:@"%d",[RealUtility abbreviateToNumber:priceMax]];
            [priceArray addObject:@{@"PriceMax":priceMax,@"PriceMin":priceMin}];
        }
        
        if([filterDict objectForKey:@"size"]){
            NSDictionary *sizeDict = [filterDict objectForKey:@"size"];
            NSString *sizeMinString = sizeDict[@"min"];
            NSString*sizeMaxString = sizeDict[@"max"];
            NSString *sizeUnit = sizeDict[@"unit"];
            sizeMin = [[RealUtility removeDecimalFormat:sizeMinString] intValue];
            sizeMax = [[RealUtility removeDecimalFormat:sizeMaxString] intValue];
            sizeUnitType = [sizeUnit intValue];
        }
        
        if ([filterDict objectForKey:@"space"]) {
            propertyArray = [[NSMutableArray alloc]init];
            NSArray *spaceArray = [filterDict objectForKey:@"space"];
            for (NSDictionary* spaceDict in spaceArray) {
                NSString* propertyType = [spaceDict[@"propertyType"]stringValue];
                NSString* spaceType = [spaceDict[@"spaceType"]stringValue];
                if ([RealUtility isValid:propertyType] && [RealUtility isValid:spaceType]) {
                    [propertyArray addObject:@{@"PropertyType":propertyType,@"SpaceType":spaceType}];
                }
            }
        }
        
        if ([filterDict objectForKey:@"spokenLanguage"]) {
            NSString *nativeName = filterDict[@"spokenLanguage"];
            spokenLanugaeIndex = [[SystemSettingModel shared]getSpokenLanguageIndex:nativeName];
            if (spokenLanugaeIndex == NSNotFound) {
                spokenLanugaeIndex =0;
            }
        }
        
        [[SearchAgentProfileModel shared] callGoogleGeoCodeApiConvertFromPlaceIDAndThenAddressSearchWithFilter:placeId
                                                                                        userInputSearchAddress:self.searchTextField.text
                                                                                               PropertyTypeSet:propertyArray
                                                                                                      PriceSet:priceArray
                                                                                                       SizeMax:sizeMax
                                                                                                       SizeMin:sizeMin
                                                                                                  BedroomCount:bedroomCount
                                                                                                 BathroomCount:bathroomCount
                                                                                           SpokenLanguageIndex:spokenLanugaeIndex
                                                                                                  CurrencyUnit:currencyUnit
                                                                                                  SizeUnitType:sizeUnitType
                                                                                                       success:^(NSMutableArray<AgentProfile> *AgentProfiles)
         {
             
             if([SearchAgentProfileModel shared].searchAgentProfilesTotalCount==0)
                 return;
             // Mixpanel
             Mixpanel *mixpanel = [Mixpanel sharedInstance];
             
             NSString *searchQueryString = placeName;
             
             if (searchQueryString == nil) {
                 
                 searchQueryString = @"";
             }
             
             [mixpanel track:@"Searched Area"
                  properties:@{
                               @"Search query": searchQueryString,
                               @"Filter applied": @"YES",
                               @"# of results returned": [NSNumber numberWithInt:[SearchAgentProfileModel shared].searchAgentProfilesTotalCount]
                               }];
             
             [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:AgentProfiles clearQueue:YES];
             [self loadImagesForOnscreenRows];
             int totalCount =(int)[SearchAgentProfileModel shared].searchAgentProfilesTotalCount;
             self.tableView.scrollEnabled = totalCount >0;
             if (totalCount <= 0 ) {
                 [self showNoRecord];
             }else{
                 [self hideNoRecord];
             }
             [self updateSearchAgentCount:totalCount];
             [self.topRefreshControl endRefreshing];
             [self showInfiniteRefreshIfNeed];
             //   self.afterFollowOrUnFollowReloadindexPathForCell = nil;
             [self showPageControl:![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] animated:YES];
             [self resetSectionHeaderView];
             [self tableViewReloadAnimationTableView:self.tableView];
             [self.tableView  setContentOffset:CGPointZero animated:YES];
             self.indexPathForCell = nil;
             [self hideLoading];
             [self checkDBSearchAgentHistoryHideFilterAndAgentCountDisablePageing];
         }failure:^(AFHTTPRequestOperation *operation, NSError *error,
                    NSString *errorMessage, RequestErrorStatus errorStatus,
                    NSString *alert, NSString *ok) {
             [self.delegate
              handleModelReturnErrorShowUIAlertViewByView:self.view
              errorMessage:errorMessage
              errorStatus:errorStatus
              alert:alert
              ok:ok];
             self.indexPathForCell = nil;
             self.tableView.scrollEnabled = NO;
             [self showNoRecord];
             [self showPageControl:![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] animated:YES];
             [self resetSectionHeaderView];
             [self updateSearchAgentCount:0];
             [self hideLoading];
             [self showPageControl:![self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] animated:YES];
             [self tableViewReloadAnimationTableView:self.tableView];
         }];
    }
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        DDLogInfo(@"indexPathgfd%ld", (long)indexPath.section);
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    } else {
        [self hideNoRecord];
        [SearchAgentProfileModel shared].AgentProfiles = nil;
        [self.tableView reloadData];
        [SearchAgentProfileModel shared].didHandleUserLocation = YES;
        [self.locationAddressSearchLoading stopAnimating];
        [self showLoading];
        [self.searchTextField resignFirstResponder];
        if (self.userInputSearchAddress.length == 0) {
            if ([[[DBSearchAgentHistory query] whereWithFormat:@" MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID]count]==0) {
                [self hideLoading];
                [self showPullToRefresh];
                TopFiveCitiesList * topFiveCitiesList=   [[SystemSettingModel shared].SystemSettings.TopFiveCitiesList objectAtIndex:indexPath.row];
                self.searchTextField.text = topFiveCitiesList.CityName;
                [self searchByGooglePlaceID:topFiveCitiesList.PlaceID placeName:topFiveCitiesList.CityName withFilterDict:nil showLoading:YES];
                NSString *placeId = topFiveCitiesList.PlaceID;
                NSNumber *memberId =[LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
                NSString *memberName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
                NSString *QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
                NSString *address = topFiveCitiesList.CityName;;
                [DBSearchAgentHistory createSearchAgentHistoryWithMemberName:memberName memberId:memberId qbid:QBID address:address placeId:placeId resultPlaces:nil];
                
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                
                return;
            }
            
            
            DBSearchAgentHistory  * dBSearchAgentHistory=     [self.searchResultPlacesHistoryArray objectAtIndex:indexPath.row];
            self.searchGooglePlaceId = dBSearchAgentHistory.PlaceId;
            self.searchTextField.text = dBSearchAgentHistory.GooglePlaceAutoCompleteAddress;
            [self showPullToRefresh];
            [self searchByGooglePlaceID:self.searchGooglePlaceId placeName:dBSearchAgentHistory.GooglePlaceAutoCompleteAddress withFilterDict:[SystemSettingModel shared].filterDict showLoading:YES];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            return;
        }
        NSString *placeId = nil;
        NSNumber *memberId =[LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
        NSString *memberName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
        NSString *QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
        NSString *address = nil;
        
        if ([[GetContryLocation shared] contryLocationISChina]) {
            HandleGoogleAutoCompleteJsonRow * handleGoogleAutoCompleteJsonRow=[self.realServerGoogleAutoCompleteApi objectAtIndex:indexPath.row];
            placeId = handleGoogleAutoCompleteJsonRow.place_id;
            address = handleGoogleAutoCompleteJsonRow.googleDescription;
            self.searchTextField.text = address;
            [self searchByGooglePlaceID:placeId placeName:handleGoogleAutoCompleteJsonRow.googleDescription withFilterDict:[SystemSettingModel shared].filterDict showLoading:YES];
            [DBSearchAgentHistory createSearchAgentHistoryWithMemberName:memberName memberId:memberId qbid:QBID address:address placeId:placeId resultPlaces:nil];
        }else{
            if (![[AppDelegate getAppDelegate]networkConnection]) {
                DBSearchAgentHistory  * dBSearchAgentHistory=     [self.searchResultPlacesHistoryArray objectAtIndex:indexPath.row];
                self.searchGooglePlaceId = dBSearchAgentHistory.PlaceId;
                self.searchTextField.text = dBSearchAgentHistory.GooglePlaceAutoCompleteAddress;
                [self showPullToRefresh];
                [self searchByGooglePlaceID:self.searchGooglePlaceId placeName:dBSearchAgentHistory.GooglePlaceAutoCompleteAddress withFilterDict:[SystemSettingModel shared].filterDict showLoading:YES];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                return;
            }
            
            
            self.searchTextField.text = [self placeAtIndexPath:indexPath].name;
            SPGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
            [place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString,
                                        NSError *error) {
                
                if (error) {
                    //     SPPresentAlertViewWithErrorAndTitle(error,
                    //                                        @"Could not map selected Place");
                    [self showNoRecord];
                    [self.locationAddressSearchLoading stopAnimating];
                    
                    [self hideLoading];
                } else {
                    [self.searchTextField resignFirstResponder];
                    
                    NSDictionary *googlePlaceDict = [self.googlereturnplaceapi objectAtIndex:indexPath.row];
                    NSString *placeId = [googlePlaceDict
                                         objectForKey:@"place_id"];
                    NSNumber *memberId =[LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
                    NSString *memberName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
                    NSString *QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
                    NSString *address = [self placeAtIndexPath:indexPath].name;
                    [DBSearchAgentHistory createSearchAgentHistoryWithMemberName:memberName memberId:memberId qbid:QBID address:address placeId:placeId resultPlaces:nil];
                    [self searchByGooglePlaceID:placeId placeName:addressString withFilterDict:[SystemSettingModel shared].filterDict showLoading:YES];
                }
            }];
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
#pragma didReceiveMemoryWarning clear memory and cleardisk
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    DDLogInfo(@"didReceiveMemoryWarning");
    // Dispose of any resources that can be recreated.
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    //  [imageCache clearDisk];
}
#pragma mark SVPullToRefresh method
- (void)insertRowAtBottom {
    DDLogInfo(@"insertRowAtBottom");
    
    if ([[SearchAgentProfileModel shared].AgentProfiles count] > 0) {
        __weak Twopage *weakSelf = self;
        [[SearchAgentProfileModel shared] callAddressSearchRetrieveAgentProfileSuccess: ^(NSMutableArray<AgentProfile> *AgentProfiles) {
            // [self updateSearchAgentCount:(int)AgentProfiles.count];
            [[RealPrefetcher sharePrefetcher]prefetchAgentProfile:AgentProfiles clearQueue:NO];
            [self showInfiniteRefreshIfNeed];
            //   self.afterFollowOrUnFollowReloadindexPathForCell = nil;
            [self tableViewReloadAnimationTableView:self.tableView];
            [self.tableView resetState];
            [self hideLoading];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error,NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert, NSString *ok) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
            [self updateSearchAgentCount:0];
            [self hideLoading];
            [self.tableView resetState];
            [self tableViewReloadAnimationTableView:self.tableView];
        }];
    }
}
#pragma mark UITableViewDelegate & UITableViewDataSource end--------------------------------------------------------------------------
#pragma Loading
-(void)showLoading{
    [super showLoading];
    self.tableView.scrollEnabled=NO;
}

-(void)hideLoading{
    [super hideLoading];
    self.tableView.scrollEnabled=YES;
    
}
#pragma mark UISearchDisplayDelegate---------------------------------------------------------------------------------------------------------------


-(void)refreshTableView{
    if ([RealUtility isValid:self.searchGooglePlaceId]) {
        
        [self searchByGooglePlaceID:self.searchGooglePlaceId placeName:self.searchTextField.text withFilterDict:[SystemSettingModel shared].filterDict showLoading:NO];
    }else{
        [self.topRefreshControl endRefreshing];
    }
}

- (void)searchFromGoogle:(NSTimer *)timer {
    
    NSDictionary *dict = [timer userInfo];
    NSString *searchString = dict[@"searchString"];
    self.pendingSearchText = searchString;
    self.userInputSearchAddress = searchString;
    
    if ([[GetContryLocation shared] contryLocationISChina]) {
        [self disableTestMode];
        
        [[SearchAgentProfileModel shared]
         callChinaGoogleMapPlaceAPIAutoCompleteUserInputSearchAddress:self.searchTextField.text success:^(HandleGoogleAutoCompleteJson * handleGoogleAutoCompleteJson) {
             [self disableTestMode];
             self.realServerGoogleAutoCompleteApi=handleGoogleAutoCompleteJson.predictions;
             [self reloadAddressTableView];
             if (self.searchKeyDidPress && self.realServerGoogleAutoCompleteApi.count >0) {
                 self.searchKeyDidPress = NO;
                 [self tableView:self.addressTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
             }else{
                 [self showPageControl:NO animated:YES];
                 [self updateSearchAgentCount:0];
             }
             
             [self hideLoading];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorStatus,
                   NSString *alert, NSString *ok) {
             
             [self.delegate
              handleModelReturnErrorShowUIAlertViewByView:self.view
              errorMessage:errorMessage
              errorStatus:errorStatus
              alert:alert
              ok:ok];
             
             [self reloadAddressTableView];
             
         }];
        
        
        
        
    } else {
        
        searchQuery.input = searchString;
        searchQuery.language=@"en";
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            if (error) {
                // SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
                if (error.code ==-1009) {
                    [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:JMOLocalizedString(NotNetWorkConnectionText, nil) errorStatus:NotNetWorkConnection alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
                }
                [self hideLoading];
            } else {
                self.googlereturnplaceapi = searchQuery.googlereturnplaceapi;
                searchResultPlaces = [places init];
                if (self.searchKeyDidPress && searchResultPlaces.count >0) {
                    self.searchKeyDidPress = NO;
                    [self tableView:self.addressTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                }else{
                    
                    
                    [self showPageControl:NO animated:YES];
                    [self updateSearchAgentCount:0];
                    //                    [self showNoRecord];
                    //                    self.searchTextField.text = self.searchTextField.text;
                    self.searchGooglePlaceId = nil;
                    [self hideLoading];
                }
                [self reloadAddressTableView];
            }
        }];
    }
    //    NSString *searchString = timer.userInfo;
}
- (void)handleSearchForSearchString:(NSString *)searchString withDelay:(CGFloat)delay{
    if (self.googleAPITimer) {
        [self.googleAPITimer invalidate];
    }
    if([RealUtility isValid:searchString]){
        self.googleAPITimer =
        [NSTimer scheduledTimerWithTimeInterval:delay
                                         target:self
                                       selector:@selector(searchFromGoogle:)
                                       userInfo:@{@"searchString":searchString}
                                        repeats:NO];
        //        searchQuery.input = searchString;
    }
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString {
    [self handleSearchForSearchString:searchString withDelay:googleSearchDelay];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)showSearchController {
    if (!self.serachContainerViewDidShow) {
        //        [[self.delegate getTabBarController] setPageControlHidden:YES animated:NO];
        self.serachContainerViewDidShow = YES;
        CGRect searchContainerFrame = self.searchContainerView.frame;
        searchContainerFrame.origin.y = -self.searchContainerView.frame.size.height;
        self.searchContainerView.frame = searchContainerFrame;
        self.searchContainerView.hidden = NO;
        [UIView animateWithDuration:0.3f
                         animations:^{
                             [self.searchContainerView setViewAlignmentInSuperView:ViewAlignmentTop padding:0];
                             self.searchContainerView.alpha = 1.0f;
                             [self loadSearchAgentHistory];
                         }
                         completion:^(BOOL finished) {
                             if (finished) {
                             }
                         }];
    }
}

-(void) didTapOnTableView:(UIGestureRecognizer*) recognizer {
    CGPoint tapLocation = [recognizer locationInView:self.addressTableView];
    NSIndexPath *indexPath = [self.addressTableView indexPathForRowAtPoint:tapLocation];
    
    if ([[self.addressTableView indexPathsForVisibleRows] containsObject:indexPath]) { //we are in a tableview cell, let the gesture be handled by the view
        recognizer.cancelsTouchesInView = NO;
    } else { // anywhere else, do what is needed for your case
        [self dismissSearchControllerWhileStayingActive];
    }
}

- (void)dismissSearchControllerWhileStayingActive {
    // Animate out the table view.
    if (self.serachContainerViewDidShow) {
        if([RealUtility isValid:self.searchGooglePlaceId]){
            [[self.delegate getTabBarController] setPageControlHidden:[self isEmpty:[SearchAgentProfileModel shared].AgentProfiles] animated:NO];
        }
        self.serachContainerViewDidShow= NO;
        [self.searchTextField resignFirstResponder];
        [UIView animateWithDuration:0.3f
                         animations:^{
                             [self.searchContainerView setViewAlignmentInSuperView:ViewAlignmentTop padding:-self.searchContainerView.frame.size.height];
                             self.searchContainerView.alpha = 0.0f;
                         }
                         completion:^(BOOL finished) {
                             if (finished) {
                                 self.searchContainerView.hidden = YES;
                             }
                         }];
    }
}
#pragma mark -
#pragma mark UISearchBar Delegate
- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    if (![searchBar isFirstResponder]) {
        // User tapped the 'clear' button.
        shouldBeginEditing = NO;
        [self.searchDisplayController setActive:NO];
    }
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (shouldBeginEditing) {
        // Animate in the table view.
        NSTimeInterval animationDuration = 0.3;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        self.searchDisplayController.searchResultsTableView.alpha = 1.0;
        [UIView commitAnimations];
        
        [self.searchDisplayController.searchBar setShowsCancelButton:YES
                                                            animated:YES];
    }
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    
    return boolToReturn;
}
#pragma mark UISearchDisplayDelegate---------------------------------------------------------------------------------------------------------------cc

#pragma mark tabBarControllerDelegate--------------------------------------------------------------------------------------------------------------
- (BOOL)tabBarController:(RDVTabBarController *)tabBarController
shouldSelectViewController:(UIViewController *)viewController {
    self.tapCounter++;
    
    
    // rule out possibility when the user taps on two different tabs very fast
    BOOL hasTappedTwiceOnOneTab = NO;
    
    if(self.previousHandledViewController == viewController) {
        
        hasTappedTwiceOnOneTab = YES;
    }
    
    self.previousHandledViewController = viewController;
    
    
    
    
    
    if(self.tapCounter == 2 && hasTappedTwiceOnOneTab) {
        [self resetSectionHeaderView];
        [self tableViewReloadAnimationTableView:self.tableView];
        [self.tableView setContentOffset:CGPointZero animated:YES];
        
        // do something when tapped twice
        self.previousHandledViewController=nil;
        self.tapCounter = 0;
        return NO; // or YES when you want the default engine process the event
        
    } else if(self.tapCounter == 1) {
        
        if (viewController == self.navigationController) {
            DDLogInfo(@"viewController==self%@", viewController);
            if (tabBarController.selectedIndex == 1) {
                [self.tableView setContentOffset:CGPointZero animated:YES];
            }
        }
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            self.tapCounter = 0;
            self.previousHandledViewController=nil;
        });
        
        
        return YES; // or YES when you want the default engine process the event
    }
    return YES;
    
    /*
     if (viewController == self.navigationController) {
     //        DDLogInfo(@"viewController==self%@", viewController);
     DDLogDebug(@"selectedIndex:%d",(int)tabBarController.selectedIndex);
     if (tabBarController.selectedIndex == 0) {
     [self.tableView setContentOffset:CGPointZero animated:YES];
     [self resetSectionHeaderView];
     }
     }
     */
    return YES;
}
#pragma mark tabBarControllerDelegate--------------------------------------------------------------------------------------------------------------

- (void)agentFilterDidDismissWithFilterDict:(NSDictionary *)dict applyFilter:(BOOL)apply{
    int filterCount = (int)dict.allKeys.count;
    if (!self.filterBadgeView) {
        self.filterBadgeView =
        [[JSBadgeView alloc] initWithParentView:self.filterButton
                                      alignment:JSBadgeViewAlignmentTopRight];
        self.filterBadgeView.badgePositionAdjustment = CGPointMake(-12, 12);
        self.filterBadgeView.badgeTextFont = [UIFont systemFontOfSize:12];
        self.filterBadgeView.userInteractionEnabled = NO;
    }
    if (filterCount <= 0) {
        self.filterBadgeView.hidden = YES;
    } else {
        self.filterBadgeView.hidden = NO;
    }
    self.filterBadgeView.badgeText = [NSString stringWithFormat:@"%d", filterCount];
    [self.agentFilterViewController dismissViewControllerAnimated:YES
                                                       completion:nil];
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
    if (apply && [RealUtility isValid:self.searchGooglePlaceId]) {
        
        [self searchByGooglePlaceID:self.searchGooglePlaceId placeName:self.searchTextField.text withFilterDict:[SystemSettingModel shared].filterDict showLoading:YES];
    }
    if (apply && [RealUtility isValid:[SearchAgentProfileModel shared].dbPlaceID]) {
        NSString * searchGooglePlaceId = [SearchAgentProfileModel shared].dbPlaceID;
        [SearchAgentProfileModel shared].dbPlaceID=nil;
        [self searchByGooglePlaceID:searchGooglePlaceId placeName:self.searchTextField.text withFilterDict:[SystemSettingModel shared].filterDict showLoading:YES];
    }
}
- (IBAction)filterButtonDidPress:(id)sender {
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    self.agentFilterViewController = [[AgentFilterViewController alloc]
                                      initWithNibName:@"AgentFilterViewController"
                                      bundle:nil];
    self.agentFilterViewController.delegate = self;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        self.definesPresentationContext = YES;
        self.agentFilterViewController.modalPresentationStyle =
        UIModalPresentationOverCurrentContext;
        [self.delegate.rootViewController presentViewController:self.agentFilterViewController
                                                       animated:YES
                                                     completion:nil];
    } else {
        self.delegate.rootViewController.modalPresentationStyle =  UIModalPresentationCurrentContext;
        [self.delegate.rootViewController presentViewController:self.agentFilterViewController animated:YES completion:nil];
    }
}

- (IBAction)testButtonDidPress:(UIButton *)button {
    button.selected = !button.selected;
    self.testModeOn = button.selected;
    [self tableViewReloadAnimationTableView:self.tableView];
}
- (void)disableTestMode {
    self.testModeOn = NO;
    self.testButton.selected = NO;
}
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    // Do your action here
    self.detectScrollAction = NO;
    [self resetSectionHeaderView];
    return YES;
}
-(void)resetSectionHeaderView{
    for (UIView *containerView in self.sectionHeaderDict.allValues) {
        [containerView removeFromSuperview];
    }
    
    self.sectionHeaderDict = [[NSMutableDictionary alloc]init];
    self.sectionHeaderOffsetDict = [[NSMutableDictionary alloc]init];
}

- (void)recordHeaderOffset:(CGFloat)offset section:(NSInteger)section{
    if (!self.sectionHeaderOffsetDict) {
        self.sectionHeaderOffsetDict = [[NSMutableDictionary alloc]init];
    }
    
    [self.sectionHeaderOffsetDict setObject:@(offset) forKey:@(section)];
}

-(CGFloat)getHeaderOffset:(NSInteger)section{
    NSNumber *offsetNumber =self.sectionHeaderOffsetDict[@(section)];
    return [offsetNumber floatValue];
}

-(UIView*)getSectionHeaderViewWithProfile:(AgentProfile*)profile withSection:(NSInteger)section{
    if (!self.sectionHeaderDict) {
        self.sectionHeaderDict =[[NSMutableDictionary alloc]init];
    }
    
    BaseView* headerContainerView = self.sectionHeaderDict[@(section)];
    AgentListSectionHeaderView *headerView = nil;
    if (!headerContainerView) {
        headerContainerView =[[BaseView alloc]initWithFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, sectionHeaderHeight)];
        headerContainerView.backgroundColor = [UIColor clearColor];
        headerView =[[AgentListSectionHeaderView alloc]initFromXib:headerContainerView.bounds];
        headerView.clipsToBounds = NO;
        [headerContainerView addSubview:headerView];
        [headerView autoPinEdgesToSuperviewEdges];
        AgentProfile *agentProfile = [[SearchAgentProfileModel shared].AgentProfiles objectAtIndex:section];
        [headerView configureWithAgentProfile:agentProfile];
        headerView.delegate = self;
        headerContainerView.targetView = headerView.targetView;
        headerContainerView.clipsToBounds = NO;
        [self.sectionHeaderDict setObject:headerContainerView forKey:@(section)];
    }
    
    if (!headerView) {
        headerView = [headerContainerView.subviews firstObject];
    }
    [headerView adjustHeaderOffset:[self getHeaderOffset:section]];
    if ([RealUtility isValid:profile]) {
        [headerView configureWithAgentProfile:profile];
    }
    return headerContainerView;
}

-(void)AgentListSectionHeaderProfileDidPress:(AgentProfile *)selectedProfile{
    int section = (int)[[SearchAgentProfileModel shared].AgentProfiles indexOfObject:selectedProfile];
    BOOL haveData= [self setupPagesWithIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    if (!haveData) {
        return;
    }
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        if (finished) {
            [self changePage:MMDrawerSideRight];
        }
    }];
}

- (void)AgentCellApartmentBackgroundImageView:(AgentProfile *)profile{
    int section = (int)[[SearchAgentProfileModel shared].AgentProfiles indexOfObject:profile];
    BOOL haveData= [self setupPagesWithIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    if (!haveData) {
        return;
    }
    [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if (finished) {
            [self changePage:MMDrawerSideLeft];
        }
    }];
}
- (void)moveToCenterPage{
    
    [[self.delegate getTabBarController]setTabBarHidden:NO animated:YES];
    [self.MMDrawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        [self changePage:MMDrawerSideNone];
    }];
}
-(void) changePage:(MMDrawerSide)drawerSide{
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}


-(void)followOrUnfollowReloadTableView{
    [self tableViewReloadAnimationTableView:self.tableView];
}

-(void)tableViewReloadAnimationTableView:(UITableView *)tableivew{
    [UIView transitionWithView:tableivew
                      duration:0.3f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void){
                        [tableivew reloadData];}
                    completion:^(BOOL finished) {
                        [self loadImagesForOnscreenRows];
                    }];
}

-(void) prefeteherDidFinishFetching:(id)object{
    if([object isKindOfClass:[AgentProfile class]]){
        AgentProfile *profile = object;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"MemberID == %d",profile.MemberID];
        NSInteger index = [[SearchAgentProfileModel shared].AgentProfiles indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return [predicate evaluateWithObject:obj];
        }];
        NSMutableArray *visiblePaths = [[self.tableView indexPathsForVisibleRows] mutableCopy];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
        if ([visiblePaths containsObject:indexPath]) {
            [self shouldShowAgentProfile:object atIndexPath:indexPath];
        }
        
    }else{
        
    }
}

@end
