//
//  ViralViewController.h
//  productionreal2
//
//  Created by Derek Cheung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM (NSInteger, ViralViewControllerType) {
    ViralViewControllerTypeBroadcast,
    ViralViewControllerTypeFindYourAgent,
    ViralViewControllerTypeChat,
    ViralViewControllerTypeBroadcastSkip,
    ViralViewControllerTypeBroadcastBecomeAnAgent,
    ViralViewControllerTypeBroadcastCreatePosting
};

@interface ViralViewController : BaseViewController

- (instancetype)initWithType:(ViralViewControllerType)type;

@end
