//
//  ViralViewController.m
//  productionreal2
//
//  Created by Derek Cheung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ViralViewController.h"

#import "MeProfileViewController.h"

// Views
#import "TPKeyboardAvoidingScrollView.h"
#import "ViralView.h"

// Models
#import "DeepLinkActionHanderModel.h"
#import "SystemSettingModel.h"
#import "SendSMSViaTwilioModel.h"

// Controllers
#import "REPhonebookManager.h"

// Mixpanel
#import "Mixpanel.h"
#import "BEMCheckBox.h"
@interface ViralViewController () <ViralViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate,BEMCheckBoxDelegate,UITextFieldDelegate>

@property (nonatomic, assign) ViralViewControllerType type;
@property (nonatomic, strong) UIPickerView *countryCodePickerView;
@property (nonatomic, strong) NSMutableArray *countryCodeArray;
@property (nonatomic, strong) UIToolbar *keyboardToolbar;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) ViralView *viralView;
@property (nonatomic, strong) BEMCheckBox *checkBox;
@property (nonatomic, strong) UIView *checkBoxBackGroundView;
@property (nonatomic, assign) BOOL isShowingCheckBoxAnimation;

@end

@implementation ViralViewController

#pragma mark - Initialization

- (instancetype)initWithType:(ViralViewControllerType)type
{
    if (self = [self init]) {
        
        _type = type;
    }
    
    return self;
}

#pragma mark - View Lifecycle

- (void)loadView
{
    self.view = [[TPKeyboardAvoidingScrollView alloc] init];
    UIScrollView * scrollView=(UIScrollView * )self.view;
    scrollView.scrollEnabled=NO;
    _viralView = [[ViralView alloc] initWithType:_type];
    _viralView.delegate = self;
    
    [self.view addSubview:_viralView];
    
    [_viralView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self.view];
    [_viralView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (_type == ViralViewControllerTypeChat) {
        
        [self setupKeyboardToolbar];
        [self setupCountryCodePickerView];
        [self setupPhoneNumberTextFieldKeyboard];
    }
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         UIScrollView * scrollView=(UIScrollView * )self.view;
         
         [scrollView setContentOffset:CGPointMake(0,0) animated:YES];
         
     }];
    
}

#pragma mark - Setup

- (void)setupKeyboardToolbar
{
    _keyboardToolbar = [[UIToolbar alloc] init];
    [_keyboardToolbar sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(Done, nil)
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneButtonPressed:)];
    
    [_keyboardToolbar setItems:[NSArray arrayWithObjects:doneButton, nil]];
}

- (void)setupCountryCodePickerView
{
    _countryCodePickerView = [[UIPickerView alloc] init];
    _countryCodePickerView.delegate = self;
    _countryCodePickerView.dataSource = self;
    
    self.viralView.countryCodeTextField.inputView = _countryCodePickerView;
    self.viralView.countryCodeTextField.inputAccessoryView = _keyboardToolbar;
    
    _countryCodeArray = [[SystemSettingModel shared]getNotDuplicateSMSCountryCodeList];
    
    [self prefillCountryCode];
}

- (void)setupPhoneNumberTextFieldKeyboard
{
    self.viralView.phoneNumberTextField.keyboardType = UIKeyboardTypePhonePad;
    self.viralView.phoneNumberTextField.inputAccessoryView = _keyboardToolbar;
}

#pragma mark - Actions

- (void)doneButtonPressed:(UIBarButtonItem *)sender
{
    [self.view endEditing:YES];
}

#pragma mark - ViralViewDelegate

- (void)view:(ViralView *)view closeButtonPressed:(UIButton *)sender{
    if (_type == ViralViewControllerTypeBroadcastSkip) {
        __block UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.image = [UIImage imageNamed:@"ico_pop_viral_broadcast"];
        iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [[kAppDelegate window] addSubview:iconImageView];
        NSLayoutConstraint *topSpacing = [iconImageView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.viralView.iconImageView];
        NSLayoutConstraint *alignCenter = [iconImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [[kAppDelegate window]layoutIfNeeded];
        UIImage *targetImage = [UIImage imageNamed:@"btn_nav_bar_broadcast"];
        CGSize finalImageSize = targetImage.size;
        CGSize rightButtonSize = CGSizeMake(45, 44);
        CGFloat statusBarHeight = 20;
        CGFloat finalTopInset = statusBarHeight + (rightButtonSize.height - finalImageSize.height)/2;
        CGFloat finalRightInset = (rightButtonSize.width - finalImageSize.width)/2;
        [self.delegate dismissOverlayerViewController:self animated:YES
                                      extraAnimations:^{
                                          topSpacing.active = NO;
                                          alignCenter.active = NO;
                                          [iconImageView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:finalTopInset];
                                          [iconImageView autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:finalRightInset];
                                          [iconImageView autoSetDimensionsToSize:finalImageSize];
                                          [[kAppDelegate window]layoutIfNeeded];
                                      } extraCompletion:^(BOOL finished) {
                                          [iconImageView removeFromSuperview];
                                          iconImageView = nil;
                                      } dismissCompletion:^{
                                          
                                      }];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)view:(ViralView *)view inviteButtonPressed:(UIButton *)sender
{
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    if (_type == ViralViewControllerTypeBroadcast || _type == ViralViewControllerTypeBroadcastSkip) {
        
        // Invite to follow
        [mixpanel track:@"Invite to Follow"];
        
        __weak typeof(self) weakSelf = self;
        UIAlertController *alert = [REPhonebookManager noPermissionAlertWithCancelBlock:^(UIAlertAction *action) {
            
             [weakSelf inviteToFollowAction];
        }];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if(_type == ViralViewControllerTypeBroadcastBecomeAnAgent){
        [self becomeAnAgentAction];
    }else if(_type == ViralViewControllerTypeBroadcastCreatePosting){
        [self createPostingAction];
    }else if (_type == ViralViewControllerTypeFindYourAgent) {
        
        // Invite to download
        [mixpanel track:@"Invite to Download"];
        [self inviteToDownloadAction];
        
    } else {
        
        // Invite to chat
        [mixpanel track:@"Invite to Chat_Old Agent"];
        [self inviteToChatActionViaSms:NO];
    }
}

- (void)view:(ViralView *)view smsButtonPressed:(nonnull UIButton *)sender
{
    NSString *countryCode = self.viralView.countryCodeTextField.text;
    NSString *phoneNumber = self.viralView.phoneNumberTextField.text;
    
    if ([RealUtility isValid:countryCode] && [RealUtility isValid:phoneNumber]) {
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Invite to Chat_New Agent (SMS)"];
        
        [self inviteToChatActionViaSms:YES];
    }
}

#pragma mark - Helpers

- (void)inviteToFollowAction
{
    NSString *memberId = [NSString stringWithFormat:@"%d",[MyAgentProfileModel shared].myAgentProfile.MemberID];
    NSString *listingId =[NSString stringWithFormat:@"%d",[MyAgentProfileModel shared].myAgentProfile.AgentListing.AgentListingID];
    NSString *memberName = [MyAgentProfileModel shared].myAgentProfile.MemberName;
    NSString *imageURL =[MyAgentProfileModel shared].myAgentProfile.AgentPhotoURL;
    
    if (![self.delegate networkConnection]) {
        
        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
    }
    
    [self showLoadingHUD];
    [[DeepLinkActionHanderModel shared]inviteToFollowWithMemberId:memberId memberName:memberName imageURL:(NSString*)imageURL listingId:listingId block:^(NSString *url, NSError *error) {
        
        if (!error) {
            
            NSString *subject = JMOLocalizedString(@"invite_to_follow__subject", nil);
            NSString *textToShare = JMOLocalizedString(@"invite_to_follow__content", nil);
            [self showActivityViewControllerWithText:textToShare url:url subject:subject];
        }
        [self hideLoadingHUD];
    }];
}

- (void)becomeAnAgentAction{
    [self goToMePageWithAction:@"becomeAnAgentAction"];
}

- (void)createPostingAction{
    [self goToMePageWithAction:@"createPostingAction"];
}

- (void)goToMePageWithAction:(NSString*)action{
    [self dismissViewControllerAnimated:YES completion:^{
        [kAppDelegate backToTopMainPageSelectedTabBarIndex:3];
        UIViewController *selectedVC = [kAppDelegate getTabBarController].selectedViewController;
        if ([selectedVC isKindOfClass:[UINavigationController class]]) {
            UINavigationController * navigationController= (UINavigationController *)selectedVC;
            if ([navigationController.topViewController isKindOfClass:[MMDrawerController class]]) {
                MMDrawerController * mmDrawerViewController= (MMDrawerController *)navigationController.topViewController;
                [mmDrawerViewController closeDrawerAnimated:YES completion:^(BOOL finished) {
                    if ([mmDrawerViewController.centerViewController isKindOfClass:[MeProfileViewController class]]) {
                        MeProfileViewController *meProfileVC = (MeProfileViewController*) mmDrawerViewController.centerViewController;
                        meProfileVC.pendingAction = action;
                    }
                }];
            }
        }
    }];
}

- (void)inviteToDownloadAction
{
    NSString *memberId = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue];
    NSString* memberName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
    
    if (![self.delegate networkConnection]) {
        
        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
    }
    
    [self showLoadingHUD];
    [[DeepLinkActionHanderModel shared] inviteToDownloadWithMemberId:memberId memberName:memberName block:^(NSString *url, NSError *error) {
        
        if (!error) {
            
            NSString *subject = JMOLocalizedString(@"invite_to_download__subject", nil);
            NSString *textToShare = JMOLocalizedString(@"invite_to_download__content", nil);
            [self showActivityViewControllerWithText:textToShare url:url subject:subject];
        }
        [self hideLoadingHUD];
    }];
    
}

- (void)inviteToChatActionViaSms:(BOOL)viaSms{
    if ([[LoginBySocialNetworkModel shared].myLoginInfo hasQBAccount]) {
        NSString *memberId = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue];
        NSString *QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
        NSString* memberName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
        
        if (![self.delegate networkConnection]) {
            
            [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
        }
        
        [self showLoadingHUD];
        [[DeepLinkActionHanderModel shared]inviteToChatWithQBID:QBID memberId:memberId memberName:memberName block:^(NSString *url, NSError *error) {
            
            if (!error) {
                
                NSString *subject = JMOLocalizedString(@"invite_to_chat__subject", nil);
                NSString *textToShare = JMOLocalizedString(@"invite_to_chat__content", nil);
                
                if (viaSms) {
                    
                    NSString *countryCode = self.viralView.countryCodeTextField.text;
                    NSString *phoneNumber = self.viralView.phoneNumberTextField.text;
                    NSString *rawCountryCode = self.countryCode;
                    BOOL valid = [RealUtility validatePhoneNumber:phoneNumber countryCode:rawCountryCode];
                    if (valid) {
                        [[SendSMSViaTwilioModel shared] SendSMSViaTwilio:countryCode
                                                           ToPhoneNumber:phoneNumber
                                                                     url:url
                                                                 success:^(id response)
                         {
                             self.viralView.phoneNumberTextField.text = nil;
                             [self addCheckBoxAnimation];
                             [self hideLoadingHUD];
                         }
                                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok)
                         {
                             // [self showAlertWithTitle:@"Error" message:errorMessage];
                             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                           errorMessage:errorMessage
                                                                            errorStatus:errorStatus
                                                                                  alert:alert
                                                                                     ok:ok];
                             [self hideLoadingHUD];
                         }];
                    }else{
                        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                      errorMessage:@""
                                                                       errorStatus:80
                                                                             alert:JMOLocalizedString(@"alert_alert", nil)
                                                                                ok:JMOLocalizedString(@"alert_ok", nil)];
                        [self hideLoadingHUD];
                    }
                } else {
                    
                    [self showActivityViewControllerWithText:textToShare url:url subject:subject];
                }
            }
            [self hideLoadingHUD];
            [self.view endEditing:YES];
        }];
    }
}

- (void)showActivityViewControllerWithText:(NSString *)text url:(NSString *)url subject:(NSString *)subject{
    NSArray *objectsToShare;
    NSURL *shareUrl = [NSURL URLWithString:url];
    objectsToShare = @[text, shareUrl];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    [activityVC setValue:subject forKey:@"subject"];
    __weak ViralViewController *weakSelf = self;
    if (_type == ViralViewControllerTypeBroadcastSkip) {
        [activityVC setCompletionWithItemsHandler:^(NSString * __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
            if (completed && !activityError) {
                [weakSelf.viralView setInviteButtonTitle:JMOLocalizedString(@"common_more_invite", nil)];
                [weakSelf.viralView setSkipButtonTitle:JMOLocalizedString(Done, nil)];
            }
        }];
    }
    if ([activityVC respondsToSelector:@selector(completionWithItemsHandler)]) {
        activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            // When completed flag is YES, user performed specific activity
            if (completed) {
                [self addCheckBoxAnimation];
            }
        };
    } else {
        activityVC.completionHandler = ^(NSString *activityType, BOOL completed) {
            // When completed flag is YES, user performed specific activity
            if (completed) {
                [self addCheckBoxAnimation];
            }
        };
    }
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)addCheckBoxAnimation {
    if (self.isShowingCheckBoxAnimation) {
        
    }else{
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Invite succeed"];
        
        self.isShowingCheckBoxAnimation=YES;
        self.checkBox                 = [[BEMCheckBox alloc] init];
        self.checkBox.onAnimationType = BEMAnimationTypeStroke;
        self.checkBox.onTintColor     = [UIColor realGreenColor];
        self.checkBox.onCheckColor    = [UIColor realGreenColor];
        UIColor *black                = [UIColor blackColor];
        black                         = [black colorWithAlphaComponent:0.5f];
        self.checkBox.onFillColor     = black;
        self.checkBox.tintColor       = [UIColor clearColor];
        self.checkBox.delegate        = self;
        self.checkBoxBackGroundView   = [[UIView alloc] init];
        UIColor *blackColor           = [UIColor blackColor];
        blackColor                    = [blackColor colorWithAlphaComponent:0.8f];
        [self.checkBoxBackGroundView setBackgroundColor:blackColor];
        
        [self.checkBoxBackGroundView addSubview:self.checkBox];
        [self.view addSubview:self.checkBoxBackGroundView];
        
        [self.checkBox setOn:NO animated:YES];
        [self.checkBox autoCenterInSuperview];
        [self.checkBox autoSetDimensionsToSize:CGSizeMake(100, 100)];
        [self.checkBoxBackGroundView autoSetDimensionsToSize:self.view.frame.size];
        [self.checkBoxBackGroundView autoCenterInSuperview];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [self.checkBox setOn:YES animated:YES];
        });
    }
}
- (void)updateCountryCode:(NSString*)countryCode
{
    self.countryCode = countryCode;
    self.viralView.countryCodeTextField.text = [NSString stringWithFormat:@"+%@", countryCode];
}

- (void)prefillCountryCode
{
    int selectedIndex= [[SystemSettingModel shared] getIndexFromNotDuplicateSMSCountryCodeListAccordingToCountry];
    
    if (selectedIndex == NSNotFound) {
        
        selectedIndex = 0;
    }
    
    [_countryCodePickerView selectRow:selectedIndex inComponent:0 animated:NO];
    [self updateCountryCode:_countryCodeArray[selectedIndex]];
}

#pragma mark - <UIPickerViewDelegate>

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *countryCode = [_countryCodeArray objectAtIndex:row];
    [self updateCountryCode:countryCode];
}

#pragma mark - <UIPickerViewDataSource>

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _countryCodeArray.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *countryCode = [_countryCodeArray objectAtIndex:row];
    return countryCode;
}
- (void)didTapCheckBox:(BEMCheckBox*)checkBox{
    [checkBox removeFromSuperview];
    [self.checkBoxBackGroundView removeFromSuperview];
    self.isShowingCheckBoxAnimation =NO;
}
- (void)animationDidStopForCheckBox:(BEMCheckBox *)checkBox{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.8 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [checkBox removeFromSuperview];
        [self.checkBoxBackGroundView removeFromSuperview];
        self.isShowingCheckBoxAnimation =NO;
    });
}
@end
