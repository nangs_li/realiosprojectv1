//
//  ViralView.h
//  productionreal2
//
//  Created by Derek Cheung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BaseView.h"

// Controllers
#import "ViralViewController.h"

@class ViralView;

@protocol ViralViewDelegate <NSObject>

@optional

- (void)view:(nonnull ViralView *)view closeButtonPressed:(nonnull UIButton *)sender;

- (void)view:(nonnull ViralView *)view inviteButtonPressed:(nonnull UIButton *)sender;

- (void)view:(nonnull ViralView *)view smsButtonPressed:(nonnull UIButton *)sender;

@end

@interface ViralView : BaseView

@property (nonatomic, strong, readonly) UITextField * _Nullable countryCodeTextField;
@property (nonatomic, strong, readonly) UITextField * _Nullable phoneNumberTextField;
@property (nonatomic, strong, readonly) UIImageView * _Nullable iconImageView;

@property (nonatomic, weak) id <ViralViewDelegate> delegate;

- (nonnull instancetype)initWithType:(ViralViewControllerType)type;

- (void)setInviteButtonTitle:(nonnull NSString*)newTitle;
- (void)setSkipButtonTitle:(nonnull NSString*)newTitle;

@end
