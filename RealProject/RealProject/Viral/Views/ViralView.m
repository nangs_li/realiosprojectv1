//
//  ViralView.m
//  productionreal2
//
//  Created by Derek Cheung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ViralView.h"

// Models
#import "UIColor+Utility.h"
#import "SLSMarkupParser.h"

//Views
#import "SeparatorLabelView.h"

@interface ViralView ()

@property (nonatomic, assign) ViralViewControllerType type;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIView *mainContainerView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextView *upperTextView;

@property (nonatomic, strong) SeparatorLabelView *agentNewLabelView;

@property (nonatomic, strong) UIView *phoneNumberContainerView;
@property (nonatomic, strong) UITextField *countryCodeTextField;
@property (nonatomic, strong) UITextField *phoneNumberTextField;
@property (nonatomic, strong) UIButton *smsButton;

@property (nonatomic, strong) SeparatorLabelView *agentOldLabelView;

@property (nonatomic, strong) UIButton *inviteButton;
@property (nonatomic, strong) UIButton *skipButton;

@property (nonatomic, strong) NSLayoutConstraint *upperTextViewHeightConstraint;

@end

@implementation ViralView

#pragma mark - Initialization

- (instancetype)initWithType:(ViralViewControllerType)type
{
    if (self = [super init]) {
        
        _type = type;
        [self setup];
        [self setupConstraints];
    }
    return self;
}

#pragma mark - Setup

- (void)setup{
    [self setupSkipButton];
    [self setupCloseButton];
    [self setupMainContainerView];
    [self setupIconImageView];
    [self setupTitleLabel];
    [self setupUpperTextView];
    
    if (_type == ViralViewControllerTypeChat) {
        [self setupAgentNewLabelView];
        [self setupPhoneNumberContainerView];
        [self setupAgentOldLabelView];
    }
    
    if (_type == ViralViewControllerTypeBroadcastSkip) {
        _closeButton.hidden = YES;
        _skipButton.hidden = NO;
    }else{
        _closeButton.hidden = NO;
        _skipButton.hidden = YES;
    }
    
    [self setupInviteButton];
}

- (void)setupCloseButton{
    _closeButton = [[UIButton alloc] init];
    UIImage *closeImage = [UIImage imageNamed:@"btn_nav_pop_close"];
    [_closeButton setImage:closeImage forState:UIControlStateNormal];
    [_closeButton setContentEdgeInsets:UIEdgeInsetsMake(15.0, 15.0, 15.0, 15.0)];
    
    [_closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_closeButton];
}

- (void)setupSkipButton{
    _skipButton = [[UIButton alloc]init];
    [self setSkipButtonTitle:JMOLocalizedString(@"invite_to_follow__skip", nil)];
    [_skipButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_skipButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_skipButton];
}

- (void)setupMainContainerView
{
    _mainContainerView = [[UIView alloc] init];
    
    [self addSubview:_mainContainerView];
}

- (void)setupIconImageView
{
    _iconImageView = [[UIImageView alloc] init];
    
    UIImage *iconImage;
    
    if (_type == ViralViewControllerTypeBroadcast || _type == ViralViewControllerTypeBroadcastSkip || _type == ViralViewControllerTypeBroadcastBecomeAnAgent || _type == ViralViewControllerTypeBroadcastCreatePosting) {
        
        iconImage = [UIImage imageNamed:@"ico_pop_viral_broadcast"];
        
    } else if (_type == ViralViewControllerTypeFindYourAgent) {
        
        iconImage = [UIImage imageNamed:@"ico_pop_viral_download"];
        
    } else {
        
        iconImage = [UIImage imageNamed:@"ico_pop_viral_chat"];
    }
    [_iconImageView setImage:iconImage];
    
    [_mainContainerView addSubview:_iconImageView];
}

- (void)setupTitleLabel
{
    _titleLabel = [[UILabel alloc] init];
    
//    NSString *yellowString = JMOLocalizedString(@"common__invite", nil);
//    NSString *whiteString = JMOLocalizedString(@"setting__invite_to", nil);
    
//    NSString *yellowString;
    NSString *markUpStringNotColor;
    if (_type == ViralViewControllerTypeBroadcast || _type == ViralViewControllerTypeBroadcastSkip || _type == ViralViewControllerTypeBroadcastBecomeAnAgent
        || _type == ViralViewControllerTypeBroadcastCreatePosting) {
        
//        whiteString = JMOLocalizedString(@"ToFollow", nil);
        markUpStringNotColor = JMOLocalizedString(@"setting__broadcast", nil);
        
        //uuuuu
        
    } else if (_type == ViralViewControllerTypeFindYourAgent) {
        
//        whiteString = JMOLocalizedString(@"ToDownload", nil);
        markUpStringNotColor = JMOLocalizedString(@"setting__invite_to_download", nil);
        //uuuuu
        
    } else {
        
//        whiteString = JMOLocalizedString(@"ToChat", nil);
        markUpStringNotColor = JMOLocalizedString(@"setting__invite_to_chat", nil);
        //uuuuu
    }
    
    markUpStringNotColor = [markUpStringNotColor stringByReplacingOccurrencesOfString:@"\n"
                                                                           withString:@""];
//    
  NSString *markUpString = [NSString stringWithFormat:@"<title>%@</title>", markUpStringNotColor];
    NSAttributedString *attributedString = [self attributedTextWithMarkUp:markUpString];
    [_titleLabel setAttributedText:attributedString];
    
    [_mainContainerView addSubview:_titleLabel];
}

- (void)setupUpperTextView
{
    _upperTextView = [[UITextView alloc] init];
    
    NSString *markupString;
    
    if (_type == ViralViewControllerTypeBroadcast || _type == ViralViewControllerTypeBroadcastSkip || _type == ViralViewControllerTypeBroadcastBecomeAnAgent
        || _type == ViralViewControllerTypeBroadcastCreatePosting) {
        
        markupString =
        [NSString stringWithFormat:@"<subtitle><yellow>%@\n</yellow></subtitle>%@\n\n<subtitle><yellow>%@\n</yellow></subtitle>%@\n\n<subtitle><yellow>%@\n</yellow></subtitle>%@\n\n",JMOLocalizedString(@"invite_to_broadcast__desc_1", nil),JMOLocalizedString(@"invite_to_broadcast__desc_2", nil),JMOLocalizedString(@"invite_to_broadcast__desc_3", nil),JMOLocalizedString(@"invite_to_broadcast__desc_4", nil),JMOLocalizedString(@"invite_to_broadcast__desc_5", nil),JMOLocalizedString(@"invite_to_broadcast__desc_6", nil)];
        //        @"<subtitle><yellow>More followers means more leads.\n</yellow></subtitle>We rank our search result by the agent’s popularity. It is one of the initial impressions to attract leads.\n\n<subtitle><yellow>Invite your old clients and new clients.\n</yellow></subtitle> When you send an “invite to follow” to them, they will automatically follow you when they installed the app. It is the best way to re-engage your old clients and communicate with new ones.\n\n<subtitle><yellow>The follower list is secret. Even from you.\n</yellow></subtitle> We know they are your assets. We will keep it anonymous forever.\n\n<subtitle><yellow>Keep and maintain all your client at Real\n</yellow></subtitle>Easiest way to update your clients. Do it right, they will be with you as long as you are still in this industry. No matter where you go.";
        
    } else if (_type == ViralViewControllerTypeFindYourAgent) {
        
        markupString =
        [NSString stringWithFormat:@"<subtitle><yellow>%@\n</yellow></subtitle>%@\n\n<subtitle><yellow>%@\n</yellow></subtitle>%@",JMOLocalizedString(@"invite_to_download__desc_1", nil),JMOLocalizedString(@"invite_to_download__desc_2", nil),JMOLocalizedString(@"invite_to_download__desc_3", nil),JMOLocalizedString(@"invite_to_download__desc_4", nil)];
        //        @"<subtitle><yellow>Tell your friends and families.\n</yellow></subtitle>Real shows the best property and best agent.\n\n<subtitle><yellow>Tell all your favorite agents to use it.\n</yellow></subtitle>You can then follow them on Real and see their updates.";
        
    } else {

        markupString =
        [NSString stringWithFormat:@"<subtitle><yellow>%@\n</yellow></subtitle>%@\n\n<subtitle><yellow>%@\n</yellow></subtitle>%@",JMOLocalizedString(@"invite_to_chat__desc_1", nil),JMOLocalizedString(@"invite_to_chat__desc_2", nil),JMOLocalizedString(@"invite_to_chat__desc_3", nil),JMOLocalizedString(@"invite_to_chat__desc_4", nil)];
        //        @"<subtitle><yellow>Met an interesting agent offline\n</yellow></subtitle>If the agent is not already on Real. Send them this “invite to chat”. They will comeback and connect with you on Real.\n\n<subtitle><yellow>You don't even need to exchange your phone number\n</yellow></subtitle>All you need is to key in his/her phone number below, our system will send out the invite for you. Your experience to engage real estate agents will never be the same again.";
        
    }
    
    NSAttributedString *attributedString = [self attributedTextWithMarkUp:markupString];
    
    [_upperTextView setAttributedText:attributedString];
    [_upperTextView setBackgroundColor:[UIColor clear]];
    [_upperTextView setTextAlignment:NSTextAlignmentLeft];
    [_upperTextView setEditable:NO];
    [_upperTextView setSelectable:NO];
    [_upperTextView setScrollEnabled:NO];
    [_upperTextView setTextContainerInset:UIEdgeInsetsZero];
    
    [_mainContainerView addSubview:_upperTextView];
}

- (void)setupAgentNewLabelView
{
    _agentNewLabelView = [SeparatorLabelView viewWithText:JMOLocalizedString(@"invite_to_chat__new_agent", nil)];
    [_mainContainerView addSubview:_agentNewLabelView];
}

- (void)setupPhoneNumberContainerView
{
    _phoneNumberContainerView = [[UIView alloc] init];
    
    [_mainContainerView addSubview:_phoneNumberContainerView];
    
    [self setupCountryCodeTextField];
    [self setupPhoneNumberTextField];
    [self setupSmsButton];
}

- (void)setupAgentOldLabelView
{
    _agentOldLabelView = [SeparatorLabelView viewWithText:JMOLocalizedString(@"invite_to_chat__agent_you_already_know", nil)];
    [_mainContainerView addSubview:_agentOldLabelView];
}

- (void)setupCountryCodeTextField
{
    _countryCodeTextField = [[UITextField alloc] init];
    
    [_countryCodeTextField setTextAlignment:NSTextAlignmentRight];
    [_countryCodeTextField setTextColor:[UIColor white]];
    [_countryCodeTextField setFont:[UIFont systemFontOfSize:12]];
    
    _countryCodeTextField.layer.borderColor = [UIColor white].CGColor;
    _countryCodeTextField.layer.borderWidth = 1.0;
    
    UIImage *pulldownImage = [UIImage imageNamed:@"btn_pop_pulldown_small"];
    UIImageView *pulldownImageView =[[UIImageView alloc] initWithImage:pulldownImage];
    CGRect frame = pulldownImageView.frame;
    frame.size.width += 20.0;
    pulldownImageView.frame = frame;
    pulldownImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    _countryCodeTextField.rightViewMode = UITextFieldViewModeAlways;
    _countryCodeTextField.rightView = pulldownImageView;
    
    [_phoneNumberContainerView addSubview:_countryCodeTextField];
}

- (void)setupPhoneNumberTextField
{
    _phoneNumberTextField = [[UITextField alloc] init];
    
    [_phoneNumberTextField setTextAlignment:NSTextAlignmentCenter];
    [_phoneNumberTextField setTextColor:[UIColor white]];
    [_phoneNumberTextField setFont:[UIFont boldSystemFontOfSize:17]];
    
    _phoneNumberTextField.layer.borderColor = [UIColor white].CGColor;
    _phoneNumberTextField.layer.borderWidth = 1.0;
    
    [_phoneNumberContainerView addSubview:_phoneNumberTextField];
}

- (void)setupSmsButton
{
    _smsButton = [[UIButton alloc] init];
    
    NSString *smsString = JMOLocalizedString(@"common__invite", nil);
    
    [_smsButton setTitle:smsString forState:UIControlStateNormal];
    [_smsButton setContentEdgeInsets:UIEdgeInsetsMake(0, 20.0, 0, 20.0)];
    [_smsButton setBackgroundColor:[UIColor alexYellow]];
    [_smsButton setTitleColor:[UIColor darkBlue] forState:UIControlStateNormal];
    [_smsButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_smsButton addTarget:self action:@selector(smsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [_phoneNumberContainerView addSubview:_smsButton];
}

- (void)setupInviteButton
{
    _inviteButton = [[UIButton alloc] init];
    
    NSString *inviteString = JMOLocalizedString(@"common__invite", nil);
    if (self.type == ViralViewControllerTypeBroadcastCreatePosting || self.type == ViralViewControllerTypeBroadcastBecomeAnAgent ) {
        inviteString = JMOLocalizedString(@"invite_to__create_first_story", nil);
    }
    [self setInviteButtonTitle:inviteString];
    [_inviteButton setContentEdgeInsets:UIEdgeInsetsMake(0, 20.0, 0, 20.0)];
    [_inviteButton setBackgroundColor:[UIColor alexYellow]];
    [_inviteButton setTitleColor:[UIColor darkBlue] forState:UIControlStateNormal];
    [_inviteButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_inviteButton addTarget:self action:@selector(inviteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [_mainContainerView addSubview:_inviteButton];
}

#pragma mark - Setup Constraints

- (void)setupConstraints
{
    [self setupCloseButtonConstraints];
    [self setupMainContainerViewConstraints];
    [self setupIconImageViewConstraints];
    [self setupTitleLabelConstraints];
    [self setupUpperTextViewConstraints];
    [self setupSkipButtonConstraints];
    
    
    if (_type == ViralViewControllerTypeChat) {
        [self setupAgentNewLabelViewConstraints];
        [self setupPhoneNumberContainerViewConstraints];
        [self setupAgentOldLabelViewConstraints];
    }
    
    [self setupInviteButtonConstraints];
}

- (void)setupCloseButtonConstraints
{
    _closeButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_closeButton autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:40.0];
    [_closeButton autoPinEdgeToSuperviewEdge:ALEdgeRight];
}

- (void) setupSkipButtonConstraints
{
    _skipButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_skipButton autoAlignAxis:ALAxisVertical toSameAxisOfView:_inviteButton];
    [_skipButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_inviteButton withOffset:8];
}

- (void)setupMainContainerViewConstraints
{
    _mainContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_mainContainerView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:40.0];
    [_mainContainerView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:40.0];
    [_mainContainerView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
}

- (void)setupIconImageViewConstraints
{
    _iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_iconImageView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [_iconImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];
}

- (void)setupTitleLabelConstraints
{
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_iconImageView withOffset:24.0];
    [_titleLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
}

- (void)setupUpperTextViewConstraints
{
    _upperTextView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_upperTextView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_titleLabel withOffset:24.0];
    [_upperTextView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [_upperTextView autoPinEdgeToSuperviewEdge:ALEdgeRight];
    
    _upperTextViewHeightConstraint = [_upperTextView autoSetDimension:ALDimensionHeight toSize:0.0 relation:NSLayoutRelationEqual];
}

- (void)setupAgentNewLabelViewConstraints
{
    _agentNewLabelView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_agentNewLabelView autoSetDimension:ALDimensionHeight toSize:25.0];
    [_agentNewLabelView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [_agentNewLabelView autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [_agentNewLabelView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_upperTextView withOffset:25.0];
}

- (void)setupPhoneNumberContainerViewConstraints
{
    _phoneNumberContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_phoneNumberContainerView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_agentNewLabelView withOffset:8.0];
    [_phoneNumberContainerView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [_phoneNumberContainerView autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [_phoneNumberContainerView autoSetDimension:ALDimensionHeight toSize:40.0];
    
    [self setupCountryCodeTextFieldConstraints];
    [self setupPhoneNumberTextFieldConstraints];
    [self setupSmsButtonConstraints];
}

- (void)setupCountryCodeTextFieldConstraints
{
    _countryCodeTextField.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_countryCodeTextField autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [_countryCodeTextField autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [_countryCodeTextField autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [_countryCodeTextField autoSetDimension:ALDimensionWidth toSize:75.0];
}

- (void)setupPhoneNumberTextFieldConstraints
{
    _phoneNumberTextField.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_phoneNumberTextField autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [_phoneNumberTextField autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [_phoneNumberTextField autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_countryCodeTextField withOffset:3.0];
}

- (void)setupSmsButtonConstraints
{
    _smsButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_smsButton autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_phoneNumberTextField withOffset:3.0];
    [_smsButton autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [_smsButton autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [_smsButton autoPinEdgeToSuperviewEdge:ALEdgeBottom];
}

- (void)setupAgentOldLabelViewConstraints
{
    _agentOldLabelView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_agentOldLabelView autoSetDimension:ALDimensionHeight toSize:25.0];
    [_agentOldLabelView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [_agentOldLabelView autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [_agentOldLabelView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_phoneNumberContainerView withOffset:15.0];
}

- (void)setupInviteButtonConstraints
{
    _inviteButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (_type == ViralViewControllerTypeChat) {
        
        [_inviteButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_agentOldLabelView withOffset:8.0];
        
    } else {
        
        [_inviteButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_upperTextView withOffset:24.0];
    }
    
    [_inviteButton autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [_inviteButton autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [_inviteButton autoSetDimension:ALDimensionHeight toSize:40.0];
    [_inviteButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [_inviteButton autoPinEdgeToSuperviewEdge:ALEdgeBottom];
}

#pragma mark - Actions

- (void)closeButtonPressed:(UIButton *)sender
{
    if ([_delegate respondsToSelector:@selector(view:closeButtonPressed:)]) {
        
        [_delegate view:self closeButtonPressed:sender];
    }
}

- (void)inviteButtonPressed:(UIButton *)sender
{
    if ([_delegate respondsToSelector:@selector(view:inviteButtonPressed:)]) {
        
        [_delegate view:self inviteButtonPressed:sender];
    }
}

- (void)smsButtonPressed:(UIButton *)sender
{
    if ([_delegate respondsToSelector:@selector(view:smsButtonPressed:)]) {
        
        [_delegate view:self smsButtonPressed:sender];
    }
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _upperTextViewHeightConstraint.constant = [self properHeightForTextView:_upperTextView];
    
    [super layoutSubviews];
}

#pragma mark - Helpers

-(NSAttributedString *)attributedTextWithMarkUp:(NSString*)markup{
    
    NSMutableParagraphStyle *styleDefault = [[NSMutableParagraphStyle alloc] init];
    styleDefault.alignment = NSTextAlignmentLeft;
    
    NSDictionary *style = @{
                            @"$default":@{
                                    NSFontAttributeName : [UIFont systemFontOfSize:12],
                                    NSParagraphStyleAttributeName:styleDefault,
                                    NSForegroundColorAttributeName :[UIColor white]
                                    },
                            @"title":@{
                                    NSFontAttributeName:[UIFont systemFontOfSize:20]
                                    },
                            @"subtitle":@{
                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:14]
                                    },
                            @"yellow":@{
                                    NSForegroundColorAttributeName : [UIColor alexYellow]
                                    },
                            @"white":@{
                                    NSForegroundColorAttributeName : [UIColor white]
                                    },
                            };
    
    NSError *error = nil;
    
    NSAttributedString *attributedString = [SLSMarkupParser attributedStringWithMarkup:markup style:style error:&error];
    
    return attributedString;
}

- (CGFloat)properHeightForTextView:(UITextView *)textView
{
    [textView setNeedsLayout];
    [textView layoutIfNeeded];
    
    CGRect rect = [textView.attributedText boundingRectWithSize:CGSizeMake(textView.bounds.size.width, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return rect.size.height + 1.0;
}

- (void)setInviteButtonTitle:(nonnull NSString*)newTitle{
    [_inviteButton setTitle:newTitle forState:UIControlStateNormal];
}
- (void)setSkipButtonTitle:(nonnull NSString*)newTitle{
    NSMutableAttributedString *attributeTitle = [[NSMutableAttributedString alloc]initWithString:newTitle attributes:@{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),NSForegroundColorAttributeName : [UIColor lightGrayColor],NSFontAttributeName  :[UIFont systemFontOfSize:14]}];
    [_skipButton setAttributedTitle:attributeTitle forState:UIControlStateNormal];
}
@end
