//
//  ActivityLogView.h
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ActivityLogView,ODRefreshControl,FollowerModel;
@protocol ActivityLogViewDelegate <NSObject>

- (void)activityLogView:(ActivityLogView*)logView shouldClose:(BOOL)close;
- (void)activityLogView:(ActivityLogView *)logView didPanDistance:(CGPoint)panDistance;
- (void)activityLogView:(ActivityLogView *)logView didDidEndPanning:(CGFloat) velocity;
- (void)activityLogViewDidTriggerPullToRefresh:(ActivityLogView*)logView;
- (void)activityLogViewDidScrollToBottom:(ActivityLogView*)logView;
@end


@interface ActivityLogView : UIView <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,assign) ActivityLogViewType logViewType;
@property (nonatomic,strong) IBOutlet UIView *loadingView;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic,strong) IBOutlet NSLayoutConstraint *dropperTopSpacingConstraint;
@property (nonatomic,strong) IBOutlet UIButton *leftTitleButton;
@property (nonatomic,strong) IBOutlet UIButton *rightTitleButton;
@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIView *closeGestureView;
@property (nonatomic,strong) ODRefreshControl *topRefreshControl;
@property (nonatomic,strong) id<ActivityLogViewDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *logsArray;
@property (nonatomic,strong) NSIndexPath *selectedIndexPath;
@property (nonatomic,strong) NSIndexPath *expandedIndexPath;
@property (nonatomic,assign) CGPoint lastPanCoordiante;
@property (nonatomic,assign) CGFloat gestureLastChange;
@property (nonatomic,assign) CGPoint gestureFinalSpeed;
@property (nonatomic,assign) BOOL haveMoreItem;
@property (nonatomic,assign) BOOL transparentBackground;

@property (nonatomic,strong) IBOutlet UILabel *followerCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *followerUnitLabel;

- (void)endPullToRefreshAnimation;
- (void)showPullToRefreshAnimation;
- (void)resetInfinitoScrollState;
- (void)setupWithType:(ActivityLogViewType)type initialArray:(NSArray*)initialArray;
- (void)configureWithLogs:(NSArray *)logs type:(ActivityLogViewType)type;

- (void)showLoading:(BOOL)show;
- (void)disableCloseGesture;
- (IBAction)closeButtonDidPress:(id)sender;
@end
