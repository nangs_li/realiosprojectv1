//
//  ActivityLogView.m
//  productionreal2
//
//  Created by Alex Hung on 7/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ActivityLogView.h"
#import "ActivityLogTableViewCell.h"
#import "ActivityLogFollowingCell.h"
#import "ActivityLogModel.h"
#import "FollowerModel.h"
#import "ODRefreshControl.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+EmptyDataSet.h"
#import "AgentProfile.h"
#import "FollowAgentModel.h"
#import "UnFollowAgentModel.h"
@interface ActivityLogView () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,ActivityLogFollowingCellDelegate>

@end

@implementation ActivityLogView

- (void)awakeFromNib{
    [super awakeFromNib];
    //    self.titleLabel.textColor = RealBlueColor;
    if(self.followerCountLabel){
        self.followerCountLabel.textColor = [UIColor realBlueColor];
    }
    
    if (self.followerUnitLabel) {
        self.followerUnitLabel.textColor = [UIColor lightGrayColor];
    }
    [self setupTableView];
    self.leftTitleButton.selected = YES;
    [self.closeGestureView addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(closeGestureViewDidPan:)]];
    if (!self.topRefreshControl) {
        self.topRefreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
        [self.topRefreshControl addTarget:self action:@selector(didTriggerPullToRefresh) forControlEvents:UIControlEventValueChanged];
    }
    self.haveMoreItem = YES;
    __weak ActivityLogView *weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf didScrollToBottom];
    }];
    [self showLoading:NO];
}

#pragma mark - Setup

- (void)setupWithType:(ActivityLogViewType)type initialArray:(NSArray*)initialArray{
    NSString *title = @"";
    if (type == ActivityLogViewTypeFollow) {
        title = JMOLocalizedString(@"me__activity", nil);
    }else{
        title = JMOLocalizedString(@"common__following", nil);
    }
    
    if (![initialArray valid]) {
        initialArray = [[NSMutableArray alloc]init];
    }
    
    if (type != self.logViewType) {
        self.logsArray = [NSMutableArray arrayWithArray:initialArray];
        self.logViewType = type;
        [self.tableView reloadData];
    }else{
        self.logViewType = type;
    }
    
    [UIView performWithoutAnimation:^{
        [self.leftTitleButton setTitle:title forState:UIControlStateNormal];
    }];
    
}
- (void)configureWithLogs:(NSArray *)logs type:(ActivityLogViewType)type{
    
    self.logsArray = [logs mutableCopy];
    [self.tableView reloadData];
    //    [self endPullToRefreshAnimation];
}

- (void)showLoading:(BOOL)show{
    if (self.loadingView.hidden != !show) {
        self.loadingView.hidden = NO;
        [UIView animateWithDuration:[CATransaction animationDuration]  animations:^{
            self.loadingView.alpha = show;
        }completion:^(BOOL finished) {
            if (finished) {
                self.loadingView.hidden = !show;
            }
        }];
    }
    if (show) {
        if (!self.loadingIndicator.isAnimating) {
            [self.loadingIndicator startAnimating];
        }
    }else{
        if (self.loadingIndicator.isAnimating) {
            [self.loadingIndicator stopAnimating];
        }
    }
    
}

- (void)disableCloseGesture{
    self.closeGestureView.hidden = YES;
}

- (void)setupTableView{
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    [self.tableView registerNib:[UINib nibWithNibName:@"ActivityLogTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityLogTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ActivityLogFollowingCell" bundle:nil] forCellReuseIdentifier:@"ActivityLogFollowingCell"];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
}

- (void)didTriggerPullToRefresh{
    if ([self.delegate respondsToSelector:@selector(activityLogViewDidTriggerPullToRefresh:)]) {
        [self.delegate activityLogViewDidTriggerPullToRefresh:self];
    }
}

- (void)didScrollToBottom{
    if ([self.delegate respondsToSelector:@selector(activityLogViewDidScrollToBottom:)]) {
        [self.delegate activityLogViewDidScrollToBottom:self];
    }
}


- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"";
    if (self.logViewType == ActivityLogViewTypeFollow) {
        text = JMOLocalizedString(@"me__no_record_title", nil);
    }else if (self.logViewType == ActivityLogViewTypeFollowing){
        text = JMOLocalizedString(@"me__no_record_title", nil);
    }
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView{
    if (self.transparentBackground) {
        return [UIColor clearColor];
    }else{
    return [UIColor whiteColor];
    }
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return -44;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return NO;
}

#pragma mark - ActivityLogFollowingCellDelegate
- (void)followingCell:(ActivityLogFollowingCell *)cell followerButtonDidPress:(FollowerModel *)model{
    NSString *memberIDString = [@(model.MemberID) stringValue];
    NSString *listingId = [@(model.ListingID) stringValue];
    BOOL isFollowing = [model didFollow];
    
    if (isFollowing) {
        UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil
                                                        delegate:nil
                                               cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                             
                                          destructiveButtonTitle:JMOLocalizedString(@"agent_profile__unfollow_button", nil)
                                               otherButtonTitles:nil, nil];
        
        as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        
        as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                [[UnFollowAgentModel shared] callUnFollowAgentApiByAgentProfileMemberID:memberIDString success:^(id response) {
                    [cell didFollow:!isFollowing];
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
                     object:nil];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                    [[AppDelegate getAppDelegate] handleModelReturnErrorShowUIAlertViewByView:self errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                }];
            }
        };
        
        [as showInView:self];
        
    }else{
        
       
        [[FollowAgentModel shared] callFollowAgentApiByAgentProfileMemberID:memberIDString AgentProfileAgentListingAgentListingID:listingId success:^(id response) {
             [cell didFollow:!isFollowing];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
             object:nil];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
            [[AppDelegate getAppDelegate] handleModelReturnErrorShowUIAlertViewByView:self errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
        }];
    }
    
}
#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.logsArray.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if (self.logViewType == ActivityLogViewTypeFollow) {
        ActivityLogTableViewCell *logTableCell = [tableView dequeueReusableCellWithIdentifier:@"ActivityLogTableViewCell"];
        ActivityLogModel *model = [self activityLogModelAtIntexPath:indexPath];
        [logTableCell configureWithActivityLogModel:model expanded:indexPath == self.expandedIndexPath];
        cell = logTableCell;
    }else if (self.logViewType == ActivityLogViewTypeFollowing){
        ActivityLogFollowingCell *followingCell = [tableView dequeueReusableCellWithIdentifier:@"ActivityLogFollowingCell"];
        followingCell.delegate = self;
        FollowerModel *model  = [self followerModelAtIndexPath:indexPath];
        [followingCell configureWithFollowerModel:model];
        if (self.transparentBackground) {
            followingCell.memberNameLabel.textColor = [UIColor whiteColor];
        }else{
            followingCell.memberNameLabel.textColor = RealNavBlueColor;
        }
        cell = followingCell;
    }else{
        cell = [UITableViewCell new];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.logViewType == ActivityLogViewTypeFollow) {
        self.selectedIndexPath = indexPath;
    }
}

- (void)setSelectedIndexPath:(NSIndexPath *)selectedIndexPath{
    BOOL didExpanded = NO;
    if (selectedIndexPath != _selectedIndexPath) {
        [self expandActivityLogTableViewCell:NO AtIndexPath:_selectedIndexPath];
        _selectedIndexPath = selectedIndexPath;
        didExpanded = [self expandActivityLogTableViewCell:YES AtIndexPath:selectedIndexPath];
    }else{
        didExpanded =  [self toggleActivityLogTableViewCellAtIndexPath:selectedIndexPath];
    }
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    if (_expandedIndexPath && didExpanded) {
        [self.tableView scrollToRowAtIndexPath:_expandedIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
}

- (void) setExpandedIndexPath:(NSIndexPath *)expandedIndexPath{
    _expandedIndexPath = expandedIndexPath;
}

- (BOOL)toggleActivityLogTableViewCellAtIndexPath:(NSIndexPath*)indexPath{
    BOOL shouldExpand = ![self activityLogTableViewCellAtIndexPath:indexPath].expanded;
    if (!shouldExpand) {
        self.expandedIndexPath = nil;
    }
    return [self expandActivityLogTableViewCell:shouldExpand AtIndexPath:indexPath];
}

- (BOOL)expandActivityLogTableViewCell:(BOOL)expand AtIndexPath:(NSIndexPath*)indexPath{
    if (expand) {
        self.expandedIndexPath = indexPath;
    }
    return  [[self activityLogTableViewCellAtIndexPath:indexPath]shouldExpand:expand force:NO];
}

- (ActivityLogTableViewCell*)activityLogTableViewCellAtIndexPath:(NSIndexPath*)indexPath{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    ActivityLogTableViewCell *logCell = nil;
    if ([cell isKindOfClass:[ActivityLogTableViewCell class]]) {
        logCell = (ActivityLogTableViewCell*)cell;
    }
    return logCell;
}

- (ActivityLogModel*)activityLogModelAtIntexPath:(NSIndexPath*)indexPath{
    ActivityLogModel *model = nil;
    if (self.logsArray.count> indexPath.row) {
        model = self.logsArray[indexPath.row];
    }
    return model;
}

- (FollowerModel*)followerModelAtIndexPath:(NSIndexPath*)indexPath{
    FollowerModel *model = nil;
    if (self.logsArray.count >indexPath.row) {
        model = self.logsArray[indexPath.row];
    }
    return model;
}

- (void)endPullToRefreshAnimation{
    [self.topRefreshControl endRefreshing];
    
}

- (void)showPullToRefreshAnimation{
    [UIView performWithoutAnimation:^{
        [self.topRefreshControl beginRefreshing];
    }];
}

- (void)resetInfinitoScrollState{
    [self.tableView resetState];
}

- (void)setHaveMoreItem:(BOOL)haveMoreItem{
    _haveMoreItem = haveMoreItem;
    self.tableView.showsInfiniteScrolling = _haveMoreItem;
}

- (void)setTransparentBackground:(BOOL)transparentBackground{
    _transparentBackground = transparentBackground;
    if (_transparentBackground) {
        self.backgroundColor = [UIColor clearColor];
    }else{
        self.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - IBAction
- (void)closeButtonDidPress:(id)sender{
    self.expandedIndexPath = nil;
    if ([self.delegate respondsToSelector:@selector(activityLogView:shouldClose:)]) {
        [self.delegate activityLogView:self shouldClose:YES];
    }
    
}

- (void)closeGestureViewDidPan:(UIPanGestureRecognizer *)gesture{
    CGPoint gestureVelocity = [gesture velocityInView:self];
    
    if ( gesture.state == UIGestureRecognizerStateChanged )
        self.gestureLastChange = CFAbsoluteTimeGetCurrent();
    else if ( gesture.state == UIGestureRecognizerStateEnded ) {
        double curTime = CFAbsoluteTimeGetCurrent();
        double timeElapsed = curTime - self.gestureLastChange;
        if ( timeElapsed < 0.1 )
            self.gestureFinalSpeed = gestureVelocity;
        else
            self.gestureFinalSpeed = CGPointZero;
    }
    if(gesture.state == UIGestureRecognizerStateBegan){
        self.lastPanCoordiante = [gesture locationInView:gesture.view];
    }
    CGPoint newCoord = [gesture locationInView:gesture.view];
    float dY = newCoord.y-self.lastPanCoordiante.y;
    
    if ([self.delegate respondsToSelector:@selector(activityLogView:didPanDistance:)]) {
        [self.delegate activityLogView:self didPanDistance:CGPointMake(0, dY)];
    }
    if(gesture.state == UIGestureRecognizerStateEnded){
        if ([self.delegate respondsToSelector:@selector(activityLogView:didDidEndPanning:)]) {
            [self.delegate activityLogView:self didDidEndPanning:self.gestureFinalSpeed.y];
        }
    }
}
@end
