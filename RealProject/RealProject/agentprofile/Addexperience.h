//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "TDDatePickerController.h"
#import "TDPicker.h"
#import "TDTwoPicker.h"
#import "SLGlowingTextField.h"
@interface Addexperience
    : BaseViewController<TDDatePickerDelegate, UITextFieldDelegate>

@property(strong, nonatomic) IBOutlet UIButton *startdatebutton;
@property(strong, nonatomic) IBOutlet UIButton *enddatebutton;
@property(strong, nonatomic) IBOutlet TDTwoPicker *datePicker;
@property(strong, nonatomic) NSDate *selectedDate;
@property(strong, nonatomic) IBOutlet SLGlowingTextField *companytextfield;
@property(strong, nonatomic) IBOutlet SLGlowingTextField *titletextfield;
@property(strong, nonatomic) IBOutlet UISwitch *currentjobswitch;
@property(strong, nonatomic) IBOutlet UILabel *endlabel;
@property(strong, nonatomic) IBOutlet UIButton *savebutton;
@property (strong, nonatomic) IBOutlet UILabel *companyName;
@property (strong, nonatomic) IBOutlet UILabel *jobTitle;
@property (strong, nonatomic) IBOutlet UILabel *currentJob;
@property (strong, nonatomic) IBOutlet UILabel *startDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *addExperienceTitle;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property(assign, nonatomic) int endYearSelectRow;
@property(assign, nonatomic) int endMonthSelectRow;
@property(assign, nonatomic) int startYearSelectRow;
@property(assign, nonatomic) int startMonthSelectRow;
#pragma mark Button Method
- (IBAction)closebutton:(id)sender;

@end
