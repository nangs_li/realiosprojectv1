//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Addexperience.h"
#import "UIImage+Utility.h"
#import "RealUtility.h"
#import "TDSemiModal.h"

@interface Addexperience ()
@end
@implementation Addexperience

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.datePicker.delegate = self;
    
}

- (void)viewDidLoad {
    //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
    [super viewDidLoad];
    [self checkEmptyToChangeSaveButtonColor];
    [self.savebutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    self.companytextfield.glowingColor = RealBlueColor;
    self.titletextfield.glowingColor = RealBlueColor;
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    
    self.companytextfield.delegate = self;
    self.titletextfield.delegate = self;
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.addExperienceTitle.text=JMOLocalizedString(@"add_experience__title", nil);
    self.companyName.text=JMOLocalizedString(@"add_experience__company", nil);
    self.jobTitle.text=JMOLocalizedString(@"add_experience__job_title", nil);
    self.currentJob.text=JMOLocalizedString(@"add_experience__current_job", nil);
    self.startDateLabel.text=JMOLocalizedString(@"add_experience__start", nil);
    self.endDateLabel.text=JMOLocalizedString(@"common__end", nil);
    [self.savebutton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];
    [self.datePicker.cancelBtn setTitle:JMOLocalizedString(@"common__cancel", nil)];
    [self.datePicker.saveBtn setTitle:JMOLocalizedString(@"error_message__done", nil)];
    [self.startdatebutton setTitle:JMOLocalizedString(@"add_experience__MM_YY", nil) forState:UIControlStateNormal];
    [self.enddatebutton setTitle:JMOLocalizedString(@"add_experience__MM_YY", nil) forState:UIControlStateNormal];
}



#pragma mark - Button-------------------------------------------------------------------------------------------------------------------------------------
- (IBAction)closebutton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)finishEditing:(BOOL)didEdit{
    
    if (didEdit && [self.baseViewDelegate respondsToSelector:@selector(BaseViewControllerDidAction:withRef:)]) {
        [self.baseViewDelegate BaseViewControllerDidAction:BaseViewActionEdit withRef:@"experience"];
    }
    [self closebutton:nil];
}
- (IBAction)savebutton:(id)sender {
    [self loadingAndStopLoadingAfterSecond:5];
    
    NSString *enddatestring           = self.enddatebutton.titleLabel.text;
    NSString *startDateString         = self.startdatebutton.titleLabel.text;
    NSString * displayStartDateString = [NSString stringWithFormat:@"%@-%d-01", _datePicker.yearArray[self.startYearSelectRow],self.startMonthSelectRow+1];
    NSString * displayEndDateString   = [NSString stringWithFormat:@"%@-%d-01",_datePicker.yearArray[self.endYearSelectRow],self.endMonthSelectRow+1];
    NSDate *startDate;
    NSDate *endDate;
    
    if (self.currentjobswitch.on) {
        enddatestring                     = self.startdatebutton.titleLabel.text;
        displayEndDateString = displayStartDateString;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];;
    startDate = [dateFormatter dateFromString:displayStartDateString];
    startDate = [[NSCalendar currentCalendar] dateBySettingHour:12 minute:0 second:0 ofDate:startDate options:0];
    endDate = [dateFormatter dateFromString:displayEndDateString];
    endDate = [[NSCalendar currentCalendar] dateBySettingHour:12 minute:0 second:0 ofDate:endDate options:0];

    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        DDLogInfo(@"userDefaults objectForKey:@AgentProfile]!=nil");
        [[MyAgentProfileModel shared] callAgentProfileExperienceAddAPIByCompany:self.companytextfield.text title:self.titletextfield.text isCurrentJob:[self currentjob] startDate:displayStartDateString endDate:displayEndDateString  success:^(AgentProfile *myAgentProfile) {
            [self finishEditing:YES];
            [self hideLoadingHUD];
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,NSString *ok) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
        }];
        
    } else {
        AgentExperience *experience =[[AgentExperience alloc]init];
        experience.Company         = self.companytextfield.text;
        experience.Title           = self.titletextfield.text;
        experience.IsCurrentJob    = [self currentjob];
        experience.dateOfEnd       = endDate;
        experience.dateOfStart     = startDate;
        experience.StartDate = [displayEndDateString copy];
        experience.EndDate = [displayEndDateString copy];
        experience.StartDateString = displayStartDateString;
        experience.EndDateString   = displayEndDateString;
        [[AgentProfileRegistrationModel shared].ExperienceArray addObject:experience];
        [[AgentProfileRegistrationModel shared]sortExperienceArray];
        DDLogInfo(@"[AppDelegate getAppDelegate].ExperienceArray%@",
                  [AgentProfileRegistrationModel shared].ExperienceArray);
        [self finishEditing:YES];
    }
}
- (IBAction)currentjobswitch:(id)sender {
    [self checkEmptyToChangeSaveButtonColor];
    if (self.currentjobswitch.on) {
        self.enddatebutton.hidden = YES;
        self.endlabel.hidden = YES;
    } else {
        self.enddatebutton.hidden = NO;
        self.endlabel.hidden = NO;
    }
}
- (NSString *)currentjob {
    if (self.currentjobswitch.on) {
        return @"1";
    } else {
        return @"0";
    }
}
- (void)checkEmptyToChangeSaveButtonColor {
    BOOL dateValid = NO;
    if (self.currentjobswitch.on) {
        NSString * displayStartDateString = [NSString stringWithFormat:@"%@-%d-01", _datePicker.yearArray[self.startYearSelectRow],self.startMonthSelectRow+1];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *startDate = [dateFormatter dateFromString:displayStartDateString];
        startDate = [[NSCalendar currentCalendar] dateBySettingHour:12 minute:0 second:0 ofDate:startDate options:0];
        dateValid = [[NSDate date] compare:startDate ] != NSOrderedAscending;
    }else{
        NSString * displayStartDateString = [NSString stringWithFormat:@"%@-%d-01", _datePicker.yearArray[self.startYearSelectRow],self.startMonthSelectRow+1];
        NSString * displayEndDateString   = [NSString stringWithFormat:@"%@-%d-01",_datePicker.yearArray[self.endYearSelectRow],self.endMonthSelectRow+1];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *startDate = [dateFormatter dateFromString:displayStartDateString];
        NSDate *endDate = [dateFormatter dateFromString:displayEndDateString];
        startDate = [[NSCalendar currentCalendar] dateBySettingHour:12 minute:0 second:0 ofDate:startDate options:0];
        endDate = [[NSCalendar currentCalendar] dateBySettingHour:12 minute:0 second:0 ofDate:endDate options:0];
        dateValid = [[NSDate date] compare:startDate ] != NSOrderedAscending && [startDate compare: endDate] != NSOrderedDescending;
    }
    BOOL inputValid = [RealUtility isValid:self.companytextfield.text] && [RealUtility isValid:self.titletextfield.text] && ![self.startdatebutton.titleLabel.text isEqualToString:JMOLocalizedString(@"add_experience__MM_YY", nil)] && (self.currentjobswitch.on || ![self.enddatebutton.titleLabel.text isEqualToString:JMOLocalizedString(@"add_experience__MM_YY", nil)]);
    self.savebutton.enabled = inputValid && dateValid;
}


#pragma mark  -TDTwoPicker(StartDate And EndDate) Delegate-------------------------------------------------------------------------------------------------
- (IBAction)startDatePickerPress:(id)sender {
    [self.companytextfield resignFirstResponder];
    [self.titletextfield resignFirstResponder];
    _datePicker.startorenddatepicker = StartDatePicker;
    _datePicker.month = _datePicker.monthArray[self.startMonthSelectRow];
    _datePicker.year = _datePicker.yearArray[self.startYearSelectRow];
    [_datePicker.picker selectRow:self.startMonthSelectRow inComponent:0 animated:NO];
    [_datePicker.picker selectRow:self.startYearSelectRow inComponent:1 animated:NO];
    [_datePicker.picker reloadAllComponents];
    [self presentSemiModalViewController:_datePicker];
}

- (IBAction)endDatePickerPress:(id)sender {
    [self.companytextfield resignFirstResponder];
    [self.titletextfield resignFirstResponder];
    _datePicker.startorenddatepicker =  EndDatePicker;
    
    _datePicker.year = _datePicker.yearArray[self.endYearSelectRow];
    _datePicker.month = _datePicker.monthArray[self.endMonthSelectRow];
    [_datePicker.picker selectRow:self.endMonthSelectRow inComponent:0 animated:NO];
    [_datePicker.picker selectRow:self.endYearSelectRow inComponent:1 animated:NO];
    [_datePicker.picker reloadAllComponents];
    [self presentSemiModalViewController:_datePicker];
}

- (void)tdDatePicker:(UIPickerView *)picker
        didSelectRow:(NSInteger)row
         inComponent:(NSInteger)component {
    if (component == 1) {
        DDLogInfo(@"year--->%@", self.datePicker.yearArray[row]);
        _datePicker.year = _datePicker.yearArray[row];
        if ([_datePicker.startorenddatepicker isEqualToString:StartDatePicker]) {
            self.startYearSelectRow=(int)row;
        }else{
            self.endYearSelectRow=(int)row;
        }
        
        
    } else {
        DDLogInfo(@"month--->%@", self.datePicker.monthArray[row]);
        _datePicker.month = _datePicker.monthArray[row];
        if ([_datePicker.startorenddatepicker isEqualToString:StartDatePicker]) {
            self.startMonthSelectRow=(int)row;
        }else{
            self.endMonthSelectRow=(int)row;
        }
    }
}
- (void)tdDatePickerSetDate:(TDTwoPicker *)viewController {
    [self dismissSemiModalViewController:_datePicker];
    
    NSString *datestring =
    [NSString stringWithFormat:@"%@ %@", _datePicker.month, _datePicker.year];
    if ([_datePicker.startorenddatepicker isEqualToString:StartDatePicker]) {
        [self.startdatebutton setTitle:datestring forState:UIControlStateNormal];
        self.startdatebutton.titleLabel.text = datestring;
    } else {
        [self.enddatebutton setTitle:datestring forState:UIControlStateNormal];
        self.enddatebutton.titleLabel.text = datestring;
    }
    DDLogInfo(@"self.enddatebutton.titleLabel.text-->%@",
              self.enddatebutton.titleLabel.text);
    [self checkEmptyToChangeSaveButtonColor];
}

- (void)tdDatePickerClearDate:(TDTwoPicker *)viewController {
    [self dismissSemiModalViewController:_datePicker];
    
    _selectedDate = nil;
}

- (void)tdDatePickerCancel:(TDTwoPicker *)viewController {
    [self dismissSemiModalViewController:_datePicker];
}








#pragma mark  - textfield Delegate--------------------------------------------------------------------------------------------------------------------
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidBeginEditing");
    [self dismissSemiModalViewController:_datePicker];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    [self checkEmptyToChangeSaveButtonColor];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *pendingString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    NSData *pendingData = [pendingString dataUsingEncoding:NSUTF8StringEncoding];
    float dataInKB = pendingData.length;
    if (dataInKB > kMaxGeneralTextFieldDataSizeInByte) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

@end