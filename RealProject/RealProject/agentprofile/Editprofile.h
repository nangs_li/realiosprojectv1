//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "JSTokenField.h"
#import "ATSDragToReorderTableViewController.h"

@interface Editprofile
    : BaseViewController<UITableViewDelegate, UITableViewDataSource,
                         JSTokenFieldDelegate, UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) IBOutlet UITextField *licenseno;
@property(nonatomic, strong) NSMutableArray *FinalAgentProfileArray;
@property (strong, nonatomic) IBOutlet UILabel *meLabel;
@property (strong, nonatomic) IBOutlet UILabel *licenseNoLabel;

#pragma mark language tokenfield tableview section2
@property(nonatomic, strong) NSMutableArray *tableViewSection2LanguageFieldArray;
@property(nonatomic, strong) JSTokenField *tableViewSection2LanguageField;


#pragma mark specialty tokenfield tableview section4
@property(nonatomic, strong) NSMutableArray *tableViewSection4SepecialtyFieldArray;
@property(nonatomic, strong) JSTokenField *tableViewSection4SepecialtyField;

@property(strong, nonatomic) IBOutlet UIButton *donebutton;

#pragma mark membername, photourl
@property(strong, nonatomic) IBOutlet UILabel *membername;
@property(strong, nonatomic) IBOutlet UIImageView *personalphoto;

- (void)btnDonePressed:(id)sender;
@end
