//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "Editprofile.h"
#import "DXAlertView.h"
#import <QuartzCore/QuartzCore.h>

#import "Experiencecell.h"
#import "Specialty.h"
#import "Top5.h"
#import "Addexperience.h"
#import "SpokenLanguages.h"
#import "Top5cell.h"
#import "Spokenlanguagescell.h"
#import "Finishprofile.h"
#import "JSTokenField.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Userprofile.h"
#import "AgentProfile.h"
#import "AgentProfileRegistrationModel.h"
#import "LoginUIViewController.h"
@implementation Editprofile

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.delegate = self;
  self.tableView.dataSource = self;

  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;


  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
      initWithTarget:self
              action:@selector(dismissKeyboard)];

  [self.view addGestureRecognizer:tap];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  CGFloat sectionHeaderHeight = 40;
  if (scrollView.contentOffset.y <= sectionHeaderHeight &&
      scrollView.contentOffset.y >= 0) {
    scrollView.contentInset =
        UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
  } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
    scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
  }
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  self.licenseno.delegate = self;
  self.licenseno.hidden = NO;

  self.tableViewSection2LanguageFieldArray = [[NSMutableArray alloc] init];

  [self tableViewSection2And4TokenFieldSetting];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
//// set membername,photourl from NSUserDefaults

  DDLogInfo(@"EditprofileEditprofile");

  self.membername.text = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;

  NSURL *url = [NSURL URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];

  [self.personalphoto loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
      self.personalphoto.alpha = 0.0;
      [UIView animateWithDuration:1.0
                       animations:^{
                           self.personalphoto.alpha = 1.0;
                           
                       }];
  }];
  [self.delegate setupcornerRadius:self.personalphoto];


  self.FinalAgentProfileArray = [[NSMutableArray alloc] init];
  if ([self isEmpty:[AgentProfileRegistrationModel shared].ExperienceArray]) {
    [AgentProfileRegistrationModel shared].ExperienceArray =
        [[NSMutableArray alloc] init];
  }
  if ([self isEmpty:[AgentProfileRegistrationModel shared].LanguagesArray]) {
    [AgentProfileRegistrationModel shared].LanguagesArray = [[NSMutableArray alloc] init];
  }
  if ([self isEmpty:[AgentProfileRegistrationModel shared].Top5Array]) {
    [AgentProfileRegistrationModel shared].Top5Array = [[NSMutableArray alloc] init];
  }
  if ([self isEmpty:[AgentProfileRegistrationModel shared].SpecialtyArray]) {
    [AgentProfileRegistrationModel shared].SpecialtyArray = [[NSMutableArray alloc] init];
  }

  [self.FinalAgentProfileArray
      addObject:[AgentProfileRegistrationModel shared].ExperienceArray];
  [self.FinalAgentProfileArray
      addObject:[AgentProfileRegistrationModel shared].LanguagesArray];
  [self.FinalAgentProfileArray
      addObject:[AgentProfileRegistrationModel shared].Top5Array];
  [self.FinalAgentProfileArray
      addObject:[AgentProfileRegistrationModel shared].SpecialtyArray];

  [self.tableView reloadData];
  DDLogInfo(@"self.FinalAgentArray %@", self.FinalAgentProfileArray);
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}
#pragma mark 
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.meLabel.text=JMOLocalizedString(@"common__me", nil);
    self.licenseNoLabel.text=JMOLocalizedString(@"create_profile__license_no", nil);
}







#pragma mark  UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  NSArray *finalarray = self.FinalAgentProfileArray[section];

  if (section == 1) {
    if (finalarray.count > 0) {
      return 1;
    } else {
      return 0;
    }
  } else if (section == 3) {
    if (finalarray.count > 0) {
      return 1;
    } else {
      return 0;
    }
  }

  NSMutableArray *dictionary = self.FinalAgentProfileArray[section];

  return [dictionary count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return [self.FinalAgentProfileArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"cellForRowAtIndexPath");
  if (indexPath.section == 0 || indexPath.section == 2) {
    //  DDLogInfo(@"agentinformation-->%@",self.FinalAgentArray[indexPath.section][indexPath.row]);
  }
  NSDictionary *agentinformation =
      self.FinalAgentProfileArray[indexPath.section][indexPath.row];
  if (indexPath.section == 0) {
    DDLogInfo(@"indexPath.section==0");
//// Experiencecell
    static NSString *cellIdentifier = @"Experiencecell";

    Experiencecell *cell = (Experiencecell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
      DDLogInfo(@"cell==nil");
      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Experiencecell"
                                                   owner:self
                                                 options:nil];
      cell = (Experiencecell *)[nib objectAtIndex:0];
    }

    cell.company.text = [agentinformation objectForKey:@"Company"];
    cell.title.text = [agentinformation objectForKey:@"Title"];

    if ([[self.delegate checkisnsnumberchangetonsstring:
                            [agentinformation objectForKey:@"IsCurrentJob"]]
            isEqualToString:@"0"]) {
      cell.date.text = [NSString
          stringWithFormat:@"%@  -  %@",
                           [agentinformation objectForKey:@"StartDate"],
                           [agentinformation objectForKey:@"EndDate"]];
    } else {
      cell.date.text = [NSString
          stringWithFormat:@"%@  -  %@",
                           [agentinformation objectForKey:@"StartDate"],
                           @"Present"];
    }

    [cell addSubview:[self addDeleteButton]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [self.delegate Futurafont:10];

    UIView *separator1 =
        [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1,
                                                 cell.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [cell.contentView addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                   green:(115 / 255.f)
                                                    blue:(118 / 255.f)
                                                   alpha:1.0]];

    return cell;
  } else if (indexPath.section == 1) {
    DDLogInfo(@"indexPath.section==1");
//// SpokenLanguagescell
    static NSString *cellIdentifier2 = @"SpokenLanguagescell";

    SpokenLanguagescell *cell = (SpokenLanguagescell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
    /*
     if (cell==nil) {
     DDLogInfo(@"cell==nil");
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SpokenLanguagescell"
                                                 owner:self
                                               options:nil];
    cell = (SpokenLanguagescell *)[nib objectAtIndex:0];
    // }

    //  [cell removeFromSuperview];
    [cell addSubview:self.tableViewSection2LanguageField];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [self.delegate Futurafont:10];
    UIView *separator1 = [[UIView alloc]
        initWithFrame:CGRectMake(0, self.tableViewSection2LanguageField.bounds.size.height - 1,
                                 self.tableViewSection2LanguageField.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self.tableViewSection2LanguageField addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                   green:(115 / 255.f)
                                                    blue:(118 / 255.f)
                                                   alpha:1.0]];

    return cell;

  } else if (indexPath.section == 2) {
    DDLogInfo(@"indexPath.section==2");
//// Top5cell
    static NSString *cellIdentifier3 = @"Top5cell";

    Top5cell *cell = (Top5cell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];

    if (cell == nil) {
      DDLogInfo(@"cell==nil");
      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Top5cell"
                                                   owner:self
                                                 options:nil];
      cell = (Top5cell *)[nib objectAtIndex:0];
    }

    if ([[[self.FinalAgentProfileArray objectAtIndex:2]
            objectAtIndex:indexPath.row] isKindOfClass:[NSString class]] &&
        [[[self.FinalAgentProfileArray objectAtIndex:2]
            objectAtIndex:indexPath.row] isEqualToString:@"DUMMY"]) {
      DDLogInfo(@"isEqualToString:DUMMY]");

      cell.hidden = YES;
    } else {
      NSString *SpaceType;

      if ([[NSString stringWithFormat:@"%@", [agentinformation
                                                 objectForKey:@"PropertyType"]]
              isEqualToString:@"0"]) {
        SpaceType = [NSString
            stringWithFormat:@"ResidentailSpaceType%@",
                             [agentinformation objectForKey:@"SpaceType"]];
      } else {
        SpaceType = [NSString
            stringWithFormat:@"CommercialSpaceType%@",
                             [agentinformation objectForKey:@"SpaceType"]];
      }

      cell.typeofspace.text = NSLocalizedString(SpaceType, nil);

      // cell.typeofspace.text= [agentinformation objectForKey:@"SpaceType"];

      cell.price.text = [agentinformation objectForKey:@"SoldPrice"];

      cell.date.text = [agentinformation objectForKey:@"SoldDate"];

      // NSString * Datestring=  [NSString stringWithFormat:@"%@, %@,
      // %@",[agentinformation objectForKey:@"Address"],[agentinformation
      // objectForKey:@"State"],[agentinformation objectForKey:@"Country"]];
      NSString *Datestring = [NSString
          stringWithFormat:@"%@", [agentinformation objectForKey:@"Address"]];
      cell.address.text = Datestring;

      [cell addSubview:[self addDeleteButton]];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
      cell.textLabel.font = [self.delegate Futurafont:10];
      UIView *separator1 = [[UIView alloc]
          initWithFrame:CGRectMake(0, cell.frame.size.height - 1,
                                   cell.bounds.size.width, 1)];
      [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
      [cell.contentView addSubview:separator1];
      [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                     green:(115 / 255.f)
                                                      blue:(118 / 255.f)
                                                     alpha:1.0]];
    }
    return cell;
  } else {
//// Sepeciaty
    DDLogInfo(@"indexPath.section==3");

    static NSString *cellIdentifier4 = @"SpokenLanguagescell";

    SpokenLanguagescell *cell = (SpokenLanguagescell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier4];
    /*
     if (cell==nil) {
     DDLogInfo(@"cell==nil");
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SpokenLanguagescell"
                                                 owner:self
                                               options:nil];
    cell = (SpokenLanguagescell *)[nib objectAtIndex:0];
    //}
    [cell addSubview:self.tableViewSection4SepecialtyField];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    cell.textLabel.font = [self.delegate Futurafont:10];
    UIView *separator1 = [[UIView alloc]
        initWithFrame:CGRectMake(0, self.tableViewSection4SepecialtyField.bounds.size.height - 1,
                                 self.tableViewSection4SepecialtyField.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self.tableViewSection4SepecialtyField addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                   green:(115 / 255.f)
                                                    blue:(118 / 255.f)
                                                   alpha:1.0]];
    return cell;
  }
}
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark  tableviewheader and footer
- (UIView *)tableView:(UITableView *)tableView
    viewForHeaderInSection:(NSInteger)section {
  UIView *sectionHeaderView = [[UIView alloc]
      initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60.0)];
  sectionHeaderView.backgroundColor = [UIColor whiteColor];
  UIImageView *image =
      [[UIImageView alloc] initWithFrame:CGRectMake(0, 25, 20, 20.0)];

  UILabel *headerLabel = [[UILabel alloc]
      initWithFrame:CGRectMake(30, 25, sectionHeaderView.frame.size.width - 30,
                               25.0)];
  headerLabel.backgroundColor = [UIColor clearColor];

  headerLabel.textAlignment = NSTextAlignmentLeft;
  [headerLabel setFont:[UIFont fontWithName:@"Futura" size:14]];
  headerLabel.textColor = [UIColor colorWithRed:152 / 255.0
                                          green:154 / 255.0
                                           blue:157 / 255.0
                                          alpha:1];
  UIButton *addbutton = [[UIButton alloc]
      initWithFrame:CGRectMake(sectionHeaderView.frame.size.width - 45, 15, 45,
                               45)];

  [addbutton setImageEdgeInsets:UIEdgeInsetsMake(10, 15, 5, 0)];
  [addbutton setImage:[UIImage imageNamed:@"addbutton.png"]
             forState:UIControlStateNormal];

  switch (section) {
    case 0:

      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Experience";
      [sectionHeaderView addSubview:addbutton];
      [addbutton addTarget:self
                    action:@selector(addExperience:)
          forControlEvents:UIControlEventTouchUpInside];
      [sectionHeaderView addSubview:addbutton];
      [image setImage:[UIImage imageNamed:@"icon-experience"]];

      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;
    case 1:
      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Language";
      [sectionHeaderView addSubview:addbutton];
      [addbutton addTarget:self
                    action:@selector(addLanguage:)
          forControlEvents:UIControlEventTouchUpInside];
      [sectionHeaderView addSubview:addbutton];
      [image setImage:[UIImage imageNamed:@"icon-language"]];
      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;
    case 2:
      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Top 5 Selected Past Cloings";
      [sectionHeaderView addSubview:addbutton];
      [addbutton addTarget:self
                    action:@selector(addTop5:)
          forControlEvents:UIControlEventTouchUpInside];
      [sectionHeaderView addSubview:addbutton];
      [image setImage:[UIImage imageNamed:@"icon-top5"]];
      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;
    case 3:
      [sectionHeaderView addSubview:headerLabel];
      headerLabel.text = @"Specialty";
      [sectionHeaderView addSubview:addbutton];
      [addbutton addTarget:self
                    action:@selector(addSpecialty:)
          forControlEvents:UIControlEventTouchUpInside];
      [sectionHeaderView addSubview:addbutton];
      [image setImage:[UIImage imageNamed:@"icon-specialty"]];
      [sectionHeaderView addSubview:image];
      return sectionHeaderView;
      break;

    default:
      break;
  }

  return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"heightForRowAtIndexPath");
  if (indexPath.section == 0) {
    DDLogInfo(@"return60");
    return 60;

  } else if (indexPath.section == 1) {
    return self.tableViewSection2LanguageField.bounds.size.height;
    DDLogInfo(@"self.tokenFieldheight");
  } else if (indexPath.section == 2) {
    DDLogInfo(@"return50");
    return 50;
  } else {
    DDLogInfo(@"self.tokenFieldheight2");
    return self.tableViewSection4SepecialtyField.bounds.size.height;
  }
}
- (UIView *)tableView:(UITableView *)tableView
    viewForFooterInSection:(NSInteger)section {
  UIView *sectionFooterView = [[UIView alloc]
      initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
  [sectionFooterView setBackgroundColor:[UIColor colorWithRed:(113 / 255.f)
                                                        green:(115 / 255.f)
                                                         blue:(118 / 255.f)
                                                        alpha:1.0]];
  sectionFooterView.autoresizingMask = 0x3f;
  return sectionFooterView;
}
- (CGFloat)tableView:(UITableView *)tableView
    heightForFooterInSection:(NSInteger)section {
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForHeaderInSection:(NSInteger)section {
  return 60.0f;
}







#pragma mark Top 5 move aniation start-------------------------------------------------------------------------------------------------
// This method is called when starting the re-ording process. You insert a blank
// row object into your
// data source and return the object you want to save for later. This method is
// only called once.
- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
  id object = [[self.FinalAgentProfileArray objectAtIndex:2]
      objectAtIndex:indexPath.row];
  [[self.FinalAgentProfileArray objectAtIndex:2]
      replaceObjectAtIndex:indexPath.row
                withObject:@"DUMMY"];

  DDLogInfo(@"saveObjectAndInsertBlankRowAtIndexPath");

  return object;
}

// This method is called when the selected row is dragged to a new position. You
// simply update your
// data source to reflect that the rows have switched places. This can be called
// multiple times
// during the reordering process.
- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
               toIndexPath:(NSIndexPath *)toIndexPath {
  if (fromIndexPath.section == 2 && toIndexPath.section == 2) {
    id object = [[self.FinalAgentProfileArray objectAtIndex:2]
        objectAtIndex:fromIndexPath.row];
    [[self.FinalAgentProfileArray objectAtIndex:2]
        removeObjectAtIndex:fromIndexPath.row];
    [[self.FinalAgentProfileArray objectAtIndex:2]
        insertObject:object
             atIndex:toIndexPath.row];
  }

  DDLogInfo(@"moveRowAtIndexPath");
}

// This method is called when the selected row is released to its new position.
// The object is the same
// object you returned in saveObjectAndInsertBlankRowAtIndexPath:. Simply update
// the data source so the
// object is in its new position. You should do any saving/cleanup here.
- (void)finishReorderingWithObject:(id)object
                       atIndexPath:(NSIndexPath *)indexPath;
{
  if (indexPath.section == 2) {
    if (![self isEmpty:object]) {
      [[self.FinalAgentProfileArray objectAtIndex:2]
          replaceObjectAtIndex:indexPath.row
                    withObject:object];
    }
  }
}





#pragma mark button event---------------------------------------------------------------------------------------------------------
- (UIButton *)addDeleteButton {
  UIButton *deletebutton = [[UIButton alloc]
      initWithFrame:CGRectMake(self.tableView.frame.size.width - 44, 5, 44,
                               44)];

  [deletebutton setImage:[UIImage imageNamed:@"icon-close"]
                forState:UIControlStateNormal];
  [deletebutton setImageEdgeInsets:UIEdgeInsetsMake(16, 24, 18, 10)];
  [deletebutton addTarget:self
                   action:@selector(deleteDrug:)
         forControlEvents:UIControlEventTouchUpInside];
  return deletebutton;
}

- (void)deleteDrug:(id)sender {
  DXAlertView *alert =
      [[DXAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                             contentText:JMOLocalizedString(@"edit_profile__confirm_to_delete_experience_desc", nil)
                         leftButtonTitle:JMOLocalizedString(@"chatroom__yes", nil)
                        rightButtonTitle:JMOLocalizedString(@"chatroom__no", nil)];
  [alert show];
  alert.leftBlock = ^() {

    NSIndexPath *indexPath = [self indexPathForButton:sender];

    if (indexPath != nil) {
      if (indexPath.section == 0) {
        [[AgentProfileRegistrationModel shared].ExperienceArray removeObjectAtIndex:indexPath.row];

        [self viewDidAppear:YES];
      } else if (indexPath.section == 2) {
        [[AgentProfileRegistrationModel shared].Top5Array removeObjectAtIndex:indexPath.row];
        [self viewDidAppear:YES];
      }
    }

  };
  alert.rightBlock = ^() {
    DDLogInfo(@"right button clicked");

  };
  alert.dismissBlock = ^() {
    DDLogInfo(@"Do something interesting after dismiss block");
  };
}

- (NSIndexPath *)indexPathForButton:(UIButton *)button {
  CGPoint point =
      [button convertPoint:button.bounds.origin toView:self.tableView];
  return [self.tableView indexPathForRowAtPoint:point];
}

- (void)addExperience:(id)sender {
  Addexperience *UIViewController =
      [[Addexperience alloc] initWithNibName:@"Addexperience" bundle:nil];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];
  [self.navigationController pushViewController:UIViewController animated:NO];

  DDLogInfo(@"addexperience");
}
- (void)addLanguage:(id)sender {
  Spokenlanguages *UIViewController =
      [[Spokenlanguages alloc] initWithNibName:@"Spokenlanguages" bundle:nil];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController pushViewController:UIViewController animated:NO];

  DDLogInfo(@"addlanguage");
}
- (void)addTop5:(id)sender {
  Top5 *UIViewController = [[Top5 alloc] initWithNibName:@"Top5" bundle:nil];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController pushViewController:UIViewController animated:NO];

  DDLogInfo(@"addTop5");
}
- (void)addSpecialty:(id)sender {
  Specialty *UIViewController =
      [[Specialty alloc] initWithNibName:@"Specialty" bundle:nil];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController pushViewController:UIViewController animated:NO];

  DDLogInfo(@"addSpecialty");
}
- (IBAction)btnDonePressed:(id)sender {
 
  // self.hud.labelText = @"loading";
    self.donebutton.userInteractionEnabled=NO;
  if ([self.delegate networkConnection]) {
    [self loadingAndStopLoadingAfterSecond:10];
      
  }

    [[AgentProfileRegistrationModel shared]  callAgentProfileRegistrationAPIAgentLicenseNo:self.licenseno.text Success:^(AgentProfile *myAgentProfile) {
        
         [self moveToFinishprofilePage];
        self.donebutton.userInteractionEnabled=YES;

    }failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
     [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
        self.donebutton.userInteractionEnabled=YES;

        
    }];

  
    
    
}
- (IBAction)backbutton:(id)sender {
    //[self btnBackPressed:sender];
    
    UIViewController *fourViewController = [[Userprofile alloc] init];
    ;
    
    CATransition *transition = [CATransition animation];
    transition.duration = ANIMATION_DURATION;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition
                                                forKey:kCATransition];
    [self.navigationController pushViewController:fourViewController animated:NO];
}





- (void)moveToFinishprofilePage{
  Finishprofile *UIViewController =
      [[Finishprofile alloc] initWithNibName:@"Finishprofile" bundle:nil];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController pushViewController:UIViewController animated:NO];
     [self hideLoadingHUD];
}




#pragma mark JSTokenFieldDelegate  ( tableViewSection2And4TokenField Delegate)-----------------------------------------------
- (void)tokenField:(JSTokenField *)tokenField
       didAddToken:(NSString *)title
 representedObject:(id)obj {
  NSDictionary *recipient =
      [NSDictionary dictionaryWithObject:title forKey:title];
  [self.tableViewSection2LanguageFieldArray addObject:recipient];

  //  DDLogInfo(@"\n%@", self.toRecipients);
}

- (void)tokenField:(JSTokenField *)tokenField
    didRemoveTokenAtIndex:(NSUInteger)index {
  DDLogInfo(@"didRemoveTokenAtIndex");
  [self.tableViewSection2LanguageFieldArray removeObjectAtIndex:index];
  // DDLogInfo(@"Deleted token %tu\n%@", index, self.toRecipients);
}
- (void)tokenField:(JSTokenField *)tokenField
    didRemoveToken:(NSString *)title
 representedObject:(id)obj {
  DXAlertView *alert =
      [[DXAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                             contentText:JMOLocalizedString(@"edit_profile__confirm_to_delete_experience_desc", nil)
                         leftButtonTitle:JMOLocalizedString(@"chatroom__yes", nil)
                        rightButtonTitle:JMOLocalizedString(@"chatroom__no", nil)];
  [alert show];
  alert.leftBlock = ^() {

    if (tokenField == self.tableViewSection2LanguageField) {
      for (NSDictionary *dict in
           [NSArray arrayWithArray:[AgentProfileRegistrationModel shared].LanguagesArray]) {
        if ([[dict objectForKey:@"Language"] isEqualToString:title]) {
          [[AgentProfileRegistrationModel shared].LanguagesArray removeObject:dict];

          DDLogInfo(@"removeObject:dict-->%@", dict);
        }
      }
      [self tableViewSection2And4TokenFieldSetting];

    } else {
      for (NSDictionary *dict in
           [NSArray arrayWithArray:[AgentProfileRegistrationModel shared].SpecialtyArray]) {
        if ([[dict objectForKey:@"specialty"] isEqualToString:title]) {
          [[AgentProfileRegistrationModel shared].SpecialtyArray removeObject:dict];
        }
      }
      [self tableViewSection2And4TokenFieldSetting];
    }

  };
  alert.rightBlock = ^() {
    DDLogInfo(@"right button clicked");

  };
  alert.dismissBlock = ^() {
    DDLogInfo(@"Do something interesting after dismiss block");
  };

  DDLogInfo(@"didRemoveToken");
}
- (void)tableViewSection2And4TokenFieldSetting {
////set language tokenField height to tableviewheight
  self.tableViewSection2LanguageField = [[JSTokenField alloc] initWithFrame:CGRectMake(0, 0, 280, 31)];

  [self.tableViewSection2LanguageField setDelegate:self];

  self.tableViewSection2LanguageField.textField.userInteractionEnabled = NO;
  if ([[AgentProfileRegistrationModel shared].LanguagesArray count] > 0) {
    for (
        NSDictionary *language in [AgentProfileRegistrationModel shared].LanguagesArray) {
      [self.tableViewSection2LanguageField addTokenWithTitle:[language objectForKey:@"Language"]
                    representedObject:[language objectForKey:@"Language"] withDeleteButton:YES];
    }

    [self.tableViewSection2LanguageField layoutSubviews];

  }
////set specialty tokenField height to tableviewheight
  self.tableViewSection4SepecialtyField =
      [[JSTokenField alloc] initWithFrame:CGRectMake(0, 0, 280, 31)];

  [self.tableViewSection4SepecialtyField setDelegate:self];

  self.tableViewSection4SepecialtyField.textField.userInteractionEnabled = NO;
  if ([[AgentProfileRegistrationModel shared].SpecialtyArray count] > 0) {
    for (
        NSDictionary *language in [AgentProfileRegistrationModel shared].SpecialtyArray) {
      [self.tableViewSection4SepecialtyField addTokenWithTitle:[language objectForKey:@"specialty"]
                     representedObject:[language objectForKey:@"specialty"] withDeleteButton:YES];
    }
    [self.tableViewSection4SepecialtyField layoutSubviews];

   
  }

  [self viewDidAppear:YES];
}








#pragma mark - textfield Delegate------------------------------------------------------------------------------------------
- (void)textFieldDidEndEditing:(UITextField *)textField {
  DDLogInfo(@"textFieldDidEndEditing");

  [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  DDLogInfo(@"textFieldDidEndEditing");
  DXAlertView *alert = [[DXAlertView alloc]
         initWithTitle:JMOLocalizedString(@"alert_alert", nil)
           contentText:JMOLocalizedString(@"create_profile__update_license_number", nil)
       leftButtonTitle:JMOLocalizedString(@"chatroom__yes", nil)
      rightButtonTitle:JMOLocalizedString(@"chatroom__no", nil)];
  [alert show];
  alert.leftBlock = ^() {
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
        [[MyAgentProfileModel shared]
         callAgentProfileAgentLicenseUpdateByAgentLicenseNumber:self.licenseno.text
         success:^(AgentProfile *myAgentProfile) {
             [self moveToFinishprofilePage];
            
         }
        
         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                   NSString *ok) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
             
         }];

    }
  };

  alert.rightBlock = ^() {
    DDLogInfo(@"right button clicked");

  };
  alert.dismissBlock = ^() {
    DDLogInfo(@"Do something interesting after dismiss block");
  };
  [textField resignFirstResponder];
  return NO;
}
- (void)dismissKeyboard {
  [self.licenseno resignFirstResponder];
}




@end
