//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "JSTokenField.h"
#import "ATSDragToReorderTableViewController.h"
#import "FXBlurView.h"
#import "RealUtility.h"
#import "RSKImageCropViewController.h"
#import "MMDrawerController.h"
typedef enum {
    EditProfilePreview =0,
    EditProfileEditView,
    EditProfileCreateNew
}EditProfileViewType;


@protocol EditProfileDelegate <NSObject>

-(void)EditProfileDidEndEditing:(BOOL)isNewProfile success:(BOOL)success;

@end

@interface Editprofilehaveprofile
    : BaseViewController<UITableViewDelegate, UITableViewDataSource,
                         JSTokenFieldDelegate, UITextFieldDelegate,EditProfileDelegate, BaseViewDelegate,RSKImageCropViewControllerDelegate>
@property (nonatomic,strong) MMDrawerController *MMDrawerController;
@property (assign, nonatomic) EditProfileViewType viewType;
@property (nonatomic,strong) id<EditProfileDelegate> editDelegate;
@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) IBOutlet UITextField *licenseno;
@property (strong, nonatomic) IBOutlet UILabel *licenseNoLabel;
@property (nonatomic,assign) BOOL isDismissing;
@property(nonatomic, strong) NSMutableArray *FinalAgentArray;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topBarWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topBarTop;

@property (nonatomic,assign) ItemViewStyle itemStyle;
@property (nonatomic,strong) IBOutlet UIView *topBarView;
@property (nonatomic,strong) IBOutlet UIButton *editButton;
@property (nonatomic,strong) IBOutlet UIButton *closeButton;
@property (nonatomic,strong) IBOutlet UIButton *doneButton;
@property (nonatomic,strong) IBOutlet UIButton *profileButton;
@property (nonatomic,strong) IBOutlet UIButton *backButton;
@property (nonatomic,strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic,strong) IBOutlet UIView *agentInfoView;
//@property (strong, nonatomic) IBOutlet UILabel *meLabel;

#pragma mark language tokenfield
@property(nonatomic, strong) NSMutableArray *tableViewSection2LanguageFieldArray;
@property(nonatomic, strong) JSTokenField *tableViewSection2LanguageField;
@property(nonatomic, strong) UILabel *tableViewSection2Label;
@property(nonatomic,strong) NSIndexPath *movingIndexPath;

#pragma mark specialty tokenfield
@property(nonatomic, strong) NSMutableArray *tableViewSection4SepecialtyFieldArray;
@property(nonatomic, strong) JSTokenField *tableViewSection4SepecialtyField;
@property(nonatomic, strong) UILabel *tableViewSection4Label;


#pragma mark membername, photourl
@property(strong, nonatomic) IBOutlet UILabel *membername;
@property(strong, nonatomic) IBOutlet UIImageView *personalphoto;
@property (strong, nonatomic) IBOutlet UILabel *topBarTitle;

-(IBAction)profileButtonDidPress:(id)sender;
@end
