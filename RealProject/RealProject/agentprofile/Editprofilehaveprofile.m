//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "Editprofilehaveprofile.h"
#import "DXAlertView.h"
#import <QuartzCore/QuartzCore.h>
#import "Spokenlanguageshaveprofile.h"
#import "Experiencecell.h"
#import "Specialty.h"
#import "Top5.h"
#import "Addexperience.h"
#import "SpokenLanguages.h"
#import "Top5cell.h"
#import "Spokenlanguagescell.h"
#import "Finishprofile.h"
#import "JSTokenField.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HandleServerReturnString.h"
#import "AgentProfile.h"
#import "AgentListing.h"
#import "AgentLanguage.h"
#import "AgentExperience.h"
#import "AgentSpecialty.h"
#import "AgentPastClosing.h"
#import "Editprofilehaveprofile.h"
#import "Finishprofilehavelisting.h"
#import "AgentPastClosingItemView.h"
#import "BaseNavigationController.h"
#import "DBMyAgentProfile.h"
#import "RSKImageCropViewController.h"
#import "DBLogInfo.h"
#import "NSString+Utility.h"
#define tableViewWidth [RealUtility screenBounds].size.width -32
// Mixpanel
#import "Mixpanel.h"

@implementation Editprofilehaveprofile
- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.viewType == EditProfilePreview) {
        self.itemStyle = ItemViewStyleBlack;
        self.view.backgroundColor = [UIColor white];
        self.licenseno.enabled = NO;
        self.doneButton.hidden = YES;
        self.profileButton.hidden = YES;
    }else{
        if (self.viewType == EditProfileCreateNew) {
            [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 70, 0)];
            [self checkInputIsValid];
        }
        self.itemStyle = ItemViewStyleWhite;
        self.view.backgroundColor = [UIColor clearColor];
        CALayer *border = [CALayer layer];
        CGFloat borderWidth = 1;
        border.borderColor = [UIColor lightGrayColor].CGColor;
        border.frame = CGRectMake(0, self.licenseno.frame.size.height - borderWidth, self.licenseno.frame.size.width, self.licenseno.frame.size.height);
        border.borderWidth = borderWidth;
        [self.licenseno.layer addSublayer:border];
        self.licenseno.layer.masksToBounds = YES;
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, self.licenseno.frame.size.height)];
        leftView.backgroundColor = self.licenseno.backgroundColor;
        self.licenseno.leftView = leftView;
        self.licenseno.leftViewMode = UITextFieldViewModeAlways;
        
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, self.licenseno.frame.size.height)];
        rightView.backgroundColor = self.licenseno.backgroundColor;
        self.licenseno.rightView = leftView;
        self.licenseno.rightViewMode = UITextFieldViewModeAlways;
        self.licenseno.enabled = YES;
        self.profileButton.hidden = NO;
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    //// tableview delegate
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.doneButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileImageDidUpdate) name:kNotificationMyAgentProfileImageDidUpdate object:nil];
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //// tableViewSectionOne Not Move
    //    CGFloat sectionHeaderHeight = 44;
    //    if (scrollView.contentOffset.y <= sectionHeaderHeight &&
    //        scrollView.contentOffset.y >= 0) {
    //        scrollView.contentInset =
    //        UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    //    } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
    //        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    //    }
}

-(void)setupNavBar{
    if (self.viewType == EditProfilePreview) {
        self.topBarView.backgroundColor = RealNavBlueColor;
        self.membername.textColor       = RealBlueColor;
        self.itemStyle                  = ItemViewStyleBlack;
        self.closeButton.hidden         = YES;
        self.editButton.hidden          = NO;
        self.pageControl.hidden         = NO;
        
        
        [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
            if(didChange){
                self.navBarContentView                      = navBarController.editProfileNavBar;
                NSString * editProfileString                = JMOLocalizedString(@"me_page__title", nil);
                navBarController.editProfileTitleLabel.text = editProfileString;
                self.navBarContentView.alpha                = 1;
                self.editButton                             = navBarController.editProfileEditButton;
                [self.editButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
                [self.editButton addTarget:self action:@selector(editButtonDidPress) forControlEvents:UIControlEventTouchUpInside];
                self.backButton                             = navBarController.backButton;
                [self.backButton addTarget:self action:@selector(backButtonDidPress) forControlEvents:UIControlEventTouchUpInside];
                
            }
        }];
        self.topBarView.hidden = YES;
        self.topBarWidth.constant=0;
        self.topBarTop.constant=0;
        [self.view setNeedsUpdateConstraints];
    }else{
        self.itemStyle                  = ItemViewStyleWhite;
        self.membername.textColor       = [UIColor whiteColor];
        self.topBarView.backgroundColor = [UIColor clearColor];
        self.closeButton.hidden         = NO;
        self.editButton.hidden          = YES;
        self.pageControl.hidden         = YES;
        
        if (self.viewType == EditProfileCreateNew) {
            self.doneButton.hidden          = NO;
        }else{
            self.doneButton.hidden          = YES;
        }
        self.topBarView.hidden          = NO;
        self.topBarWidth.constant=64;
        self.topBarTop.constant=20;
        [self.view setNeedsUpdateConstraints];
    }
}
-(void)backButtonDidPress{
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        [[[AppDelegate getAppDelegate]getTabBarController]setTabBarHidden:NO];
        [[self.delegate getTabBarController]changePage:1];
    }];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupNavBar];
    
    UIView *topView = self.topBarView;
    NSString * editProfileString = @"";
    if (self.viewType == EditProfileCreateNew) {
        editProfileString = JMOLocalizedString(@"create_profile__create_profile_title", nil);
    }else if (self.viewType == EditProfileEditView){
        editProfileString = JMOLocalizedString(@"edit_profile__title", nil);
    }
    self.topBarTitle.text=editProfileString;
    if (self.viewType == EditProfilePreview) {
        topView = nil;
    }
    if (topView) {
        [self.agentInfoView moveViewTo:topView direction:RelativeDirectionBottom padding:8];
    }else{
        [self.agentInfoView setViewAlignmentInSuperView:ViewAlignmentTop padding:8];
    }
    topView = self.agentInfoView;
    [self.tableView moveViewTo:topView direction:RelativeDirectionBottom padding:8];
    topView = self.tableView;
    if (self.viewType == EditProfileCreateNew) {
        CGRect tableFrame = self.tableView.frame;
        tableFrame.size.width = tableViewWidth;
        tableFrame.size.height = self.doneButton.frame.origin.y - self.tableView.frame.origin.y - 8;
        self.tableView.frame = tableFrame;
    }else{
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, tableViewWidth, self.tableView.superview.frame.size.height - self.tableView.frame.origin.y);
    }
    
    self.licenseno.delegate = self;
    self.licenseno.hidden = NO;
    
    self.tableViewSection2LanguageFieldArray = [[NSMutableArray alloc] init];
    
    self.hud.labelText = JMOLocalizedString(@"alert__Initializing", nil);
    //    if ([self.delegate networkConnection]) {
    //        [self loadingAndStopLoadingAfterSecond:10];
    //    }
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        self.licenseno.text = [MyAgentProfileModel shared].myAgentProfile.AgentLicenseNumber;
    }
    
    
    [self tableViewSection2And4TokenFieldSetting];
    [self setUpMembernameAndPhotourl];
    
    
    //       [self hideLoadingHUD];
}
- (void)viewDidAppear:(BOOL)animated {
    DDLogInfo(@"EditprofilehaveprofileEditprofilehaveprofile");
    [super viewDidAppear:animated];
    //    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    //    self.meLabel.text = JMOLocalizedString(@"MePageAgentProfile.Me", nil);
    if (self.viewType != EditProfilePreview) {
        self.profileButton.clipsToBounds = YES;
        self.profileButton.titleLabel.adjustsFontSizeToFitWidth = TRUE;
        self.profileButton.titleLabel.minimumFontSize = 10;
        self.profileButton.layer.cornerRadius = self.profileButton.frame.size.height/2;
        [self.profileButton setTitle:JMOLocalizedString(@"common__edit", nil) forState:UIControlStateNormal];
        [self.profileButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:0.4]] forState:UIControlStateNormal];
    }
    self.licenseNoLabel.text =
    JMOLocalizedString(@"create_profile__license_no", nil);
    [self.doneButton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];
}








#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    DDLogInfo(@"numberOfRowsInSection-->%ld", (long)section);
    if (section == 1 || section == 2 ){
        return [self getSectionArraay:section].count > 0? 1:0;
    }
    return [self getSectionArraay:section].count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"cellForRowAtIndexPath");
    
    if (indexPath.section == 0) {
        DDLogInfo(@"indexPath.section==0");
        //// Experiencecell
        static NSString *cellIdentifier = @"Experiencecell";
        
        Experiencecell *cell = (Experiencecell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            DDLogInfo(@"cell==nil");
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Experiencecell"
                                                         owner:self
                                                       options:nil];
            cell = (Experiencecell *)[nib objectAtIndex:0];
        }
        AgentExperience *AgentExperience = [ self getRowObject:indexPath];
        [cell configureCell:AgentExperience withStyle:self.itemStyle];
        NSInteger totalRow = [self tableView:tableView numberOfRowsInSection:indexPath.section];
        if (indexPath.row  >= totalRow -1) {
            cell.itemView.dividerView.image = [UIImage imageNamed:@"content_breakline"];
        }else{
            cell.itemView.dividerView.image = [UIImage imageNamed:@"dottedline"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundView=[[UIView alloc] initWithFrame:cell.bounds];
        cell.backgroundColor = [UIColor clearColor];
        if (!cell.deleteButton && self.viewType != EditProfilePreview) {
            cell.deleteButton = [self addDeleteButton];
            [cell.contentView addSubview:cell.deleteButton];
        }
        
        return cell;
    } else if (indexPath.section == 1) {
        DDLogInfo(@"indexPath.section==1");
        //// SpokenLanguagescell
        static NSString *cellIdentifier2 = @"SpokenLanguagescell";
        
        SpokenLanguagescell *cell = (SpokenLanguagescell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        /*
         if (cell==nil) {
         DDLogInfo(@"cell==nil");
         */
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SpokenLanguagescell"
                                                     owner:self
                                                   options:nil];
        cell = (SpokenLanguagescell *)[nib objectAtIndex:0];
        // }
        
        //  [cell removeFromSuperview];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [self.delegate Futurafont:10];
        
        UIImageView *separator1;
        if (self.viewType == EditProfilePreview) {
            
            [cell.contentView addSubview:self.tableViewSection2Label];
            separator1 = [[UIImageView alloc]
                          initWithFrame:
                          CGRectMake(
                                     0, self.tableViewSection2Label.bounds.size.height + 15,
                                     tableView.frame.size.width, 1)];
        } else {
            
            [cell.contentView addSubview:self.tableViewSection2LanguageField];
            separator1 = [[UIImageView alloc]
                          initWithFrame:
                          CGRectMake(
                                     0, self.tableViewSection2LanguageField.bounds.size.height - 2,
                                     tableView.frame.size.width, 1)];
        }
        separator1.image = [UIImage imageNamed:@"content_breakline"];
        
        [cell.contentView addSubview:separator1];
        [separator1 setBackgroundColor:[UIColor clearColor]];
        cell.backgroundView=[[UIView alloc] initWithFrame:cell.bounds];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
        
    } else if (indexPath.section == 2) {
        //// Sepeciaty
        DDLogInfo(@"indexPath.section==2");
        static NSString *cellIdentifier4 = @"SpokenLanguagescell";
        
        SpokenLanguagescell *cell = (SpokenLanguagescell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier4];
        /*
         if (cell==nil) {
         DDLogInfo(@"cell==nil");
         */
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SpokenLanguagescell"
                                                     owner:self
                                                   options:nil];
        cell = (SpokenLanguagescell *)[nib objectAtIndex:0];
        //}
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [self.delegate Futurafont:10];
        
        UIImageView *separator1;
        if (self.viewType == EditProfilePreview) {
            
            [cell.contentView addSubview:self.tableViewSection4Label];
            separator1 = [[UIImageView alloc]
                          initWithFrame:
                          CGRectMake(0, self.tableViewSection4Label.bounds.size.height + 15,
                                     tableView.frame.size.width, 1)];
        } else {
            
            [cell.contentView addSubview:self.tableViewSection4SepecialtyField];
            separator1 = [[UIImageView alloc]
                          initWithFrame:
                          CGRectMake(0, self.tableViewSection4SepecialtyField.bounds.size.height - 2,
                                     tableView.frame.size.width, 1)];
        }
        separator1.image = [UIImage imageNamed:@"content_breakline"];
        
        [cell.contentView addSubview:separator1];
        [separator1 setBackgroundColor:[UIColor clearColor]];
        cell.backgroundView=[[UIView alloc] initWithFrame:cell.bounds];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
        
    } else {
        DDLogInfo(@"indexPath.section==3");
        //// Top5cell
        static NSString *cellIdentifier3 = @"Top5cell";
        
        Top5cell *cell = (Top5cell *)
        [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
        
        if (cell == nil) {
            DDLogInfo(@"cell==nil");
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Top5cell"
                                                         owner:self
                                                       options:nil];
            cell = (Top5cell *)[nib objectAtIndex:0];
        }
        AgentPastClosing *pastClosing = [self getRowObject:indexPath];
        [cell configureCell:pastClosing withStyle:self.itemStyle];
        NSInteger totalRow = [self tableView:tableView numberOfRowsInSection:indexPath.section];
        if (indexPath.row  >= totalRow -1) {
            cell.itemView.dividerView.image = [UIImage imageNamed:@"content_breakline"];
        }else{
            cell.itemView.dividerView.image = [UIImage imageNamed:@"dottedline"];
        }
        
        cell.idstring = [NSString stringWithFormat:@"%d",pastClosing.ID];
        if (!cell.deleteButton  && self.viewType != EditProfilePreview) {
            cell.deleteButton = [self addDeleteButton];
            [cell.contentView addSubview:cell.deleteButton];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundView=[[UIView alloc] initWithFrame:cell.bounds];
        cell.backgroundColor = [UIColor clearColor];
        //        if (indexPath == self.movingIndexPath) {
        //            cell.contentView.hidden = YES;
        //        }else{
        //            cell.contentView.hidden = NO;
        //        }
        DDLogDebug(@"cell contentView:%@",cell.contentView);
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark tableviewheader and footer

- (UIView *)tableView:(UITableView *)tableView
viewForHeaderInSection:(NSInteger)section {
    UIView *sectionHeaderView = [[UIView alloc]
                                 initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44.0)];
    sectionHeaderView.backgroundColor = [UIColor clearColor];
    
    CGRect addButtonFrame = CGRectMake(sectionHeaderView.frame.size.width -33, 0, 44, 44);
    CGRect titleButtonFrame = CGRectMake(0, 0, addButtonFrame.origin.x, 44);
    UIButton *sectionTitleButton= [UIButton buttonWithType:UIButtonTypeCustom];
    sectionTitleButton.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    sectionTitleButton.frame = titleButtonFrame;
    [sectionTitleButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    sectionTitleButton.titleEdgeInsets = UIEdgeInsetsMake(0, 9, 0, 0);
    sectionTitleButton.userInteractionEnabled = NO;
    
    UIButton *addbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    addbutton.frame = addButtonFrame;
    [addbutton setImage:[UIImage imageNamed:@"btn_pop_create"]
               forState:UIControlStateNormal];
    [sectionHeaderView addSubview:sectionTitleButton];
    [sectionHeaderView addSubview:addbutton];
    switch (section) {
        case 0:
            [sectionTitleButton setTitle:JMOLocalizedString(@"common__experience", nil) forState:UIControlStateNormal];
            if(self.viewType == EditProfilePreview) {
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_profile_experience"] forState:UIControlStateNormal];
            }else{
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_pop_profile_experience"] forState:UIControlStateNormal];
            }
            [addbutton addTarget:self
                          action:@selector(addExperience:)
                forControlEvents:UIControlEventTouchUpInside];
            break;
        case 1:
            [sectionTitleButton setTitle:JMOLocalizedString(@"common__spoken_languages", nil) forState:UIControlStateNormal];
            if(self.viewType == EditProfilePreview) {
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_profile_language"] forState:UIControlStateNormal];
            }else{
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_pop_profile_language"] forState:UIControlStateNormal];
            }
            [addbutton addTarget:self
                          action:@selector(addLanguage:)
                forControlEvents:UIControlEventTouchUpInside];
            break;
        case 2:
            [sectionTitleButton setTitle:JMOLocalizedString(@"common__specialty", nil) forState:UIControlStateNormal];
            if(self.viewType == EditProfilePreview) {
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_profile_specialty"] forState:UIControlStateNormal];
            }else{
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_pop_profile_specialty"] forState:UIControlStateNormal];
            }
            [addbutton addTarget:self
                          action:@selector(addSpecialty:)
                forControlEvents:UIControlEventTouchUpInside];
            break;
        case 3:
            [sectionTitleButton setTitle:JMOLocalizedString(@"common__agentprofile_title_Personal_best_closings", nil) forState:UIControlStateNormal];
            if(self.viewType == EditProfilePreview) {
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_profile_portfolio"] forState:UIControlStateNormal];
            }else{
                [sectionTitleButton setImage:[UIImage imageNamed:@"ico_pop_profile_portfolio"] forState:UIControlStateNormal];
            }
            [addbutton addTarget:self
                          action:@selector(addTop5:)
                forControlEvents:UIControlEventTouchUpInside];
            break;
        default:
            break;
    }
    if (self.viewType == EditProfilePreview) {
        [sectionTitleButton setTitleColor:RealDarkBlueColor forState:UIControlStateNormal];
        addbutton.hidden = YES;
    }else{
        [sectionTitleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        addbutton.hidden = NO;
    }
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id rowObject = [self getRowObject:indexPath];
    DDLogInfo(@"heightForRowAtIndexPath");
    if (indexPath.section == 0) {
        AgentExperience *exp = rowObject;
        int rowPadding = 8;
        NSInteger totalRow = [self tableView:tableView numberOfRowsInSection:indexPath.section];
        if (indexPath.row  >= totalRow -1) {
            rowPadding = -2;
        }else{
            rowPadding = 8;
        }
        
        return [AgentExperienceItemView calViewSizeWithExperience:exp withInitSize:CGSizeMake(tableViewWidth, 60) withStyle:self.itemStyle].height+rowPadding;
        
    } else if (indexPath.section == 1) {
        
        if (self.viewType == EditProfilePreview) {
            
            return self.tableViewSection2Label.bounds.size.height + 15;
            
        } else {
            
            return self.tableViewSection2LanguageField.bounds.size.height;
        }
        DDLogInfo(@"self.tokenFieldheight");
    } else if (indexPath.section == 2) {
        DDLogInfo(@"self.tokenFieldheight2");
        
        if (self.viewType == EditProfilePreview) {
            
            return self.tableViewSection4Label.bounds.size.height + 15;
            
        } else {
            
            return self.tableViewSection4SepecialtyField.bounds.size.height;
        }
    } else {
        DDLogInfo(@"self.tokenFieldheight2");
        AgentPastClosing *pastClosing = rowObject;
        return [AgentPastClosingItemView calViewSizeWithAgentPastClosing:pastClosing withInitSize:CGSizeMake(tableViewWidth, 60) withStyle:self.itemStyle].height;
    }
}
- (UIView *)tableView:(UITableView *)tableView
viewForFooterInSection:(NSInteger)section {
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

////datestringtoMMMYYYFormat
- (NSString *)dateStringToMMMYYYFormat:(NSString *)SoldDateString {
    NSDateFormatter *dateFormatter = [NSDateFormatter localeDateFormatter];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString:SoldDateString];
    
    [dateFormatter setDateFormat:@"MMM YYYY"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
- (void)setUpMembernameAndPhotourl {
    self.membername.text = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
    
    NSURL *url = [NSURL URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];
    
    [self.personalphoto loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (cacheType == SDImageCacheTypeNone) {
            self.personalphoto.alpha = 0.0;
            [UIView animateWithDuration:1.0
                             animations:^{
                                 self.personalphoto.alpha = 1.0;
                             }];
        }
        
    }];
    [self.delegate setupcornerRadius:self.personalphoto];
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        self.licenseno.text = [MyAgentProfileModel shared].myAgentProfile.AgentLicenseNumber;
    }
    [self.tableView reloadData];
    
    //    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}











#pragma mark Top 5 move aniation start---------------------------------------------------------------------------------------------------
// This method is called when starting the re-ording process. You insert a blank
// row object into your
// data source and return the object you want to save for later. This method is
// only called once.
- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(
              @"[[MyAgentProfileModel shared].myAgentProfile.AgentPastClosing "
              @"count]-->%lu",
              (unsigned long)
              [[MyAgentProfileModel shared].myAgentProfile.AgentPastClosing count]);
    
    DDLogInfo(@"saveObjectAndInsertBlankRowAtIndexPath");
    id object =[self getRowObject:indexPath];
    self.movingIndexPath = indexPath;
    // [[MyAgentProfileModel shared].myAgentProfile.AgentPastClosing
    // replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
    DDLogInfo(@"saveObjectAndInsertBlankRowAtIndexPath");
    
    return object;
}

// This method is called when the selected row is dragged to a new position. You
// simply update your
// data source to reflect that the rows have switched places. This can be called
// multiple times
// during the reordering process.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.viewType != EditProfilePreview && [self getSectionArraay:indexPath.section].count >1;
}

- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
               toIndexPath:(NSIndexPath *)toIndexPath {
    DDLogInfo(@"moveRowAtIndexPath");
    self.movingIndexPath = toIndexPath;
    if (fromIndexPath.section == 3 && toIndexPath.section == 3) {
        id object = [self getRowObject:fromIndexPath];
        if (self.viewType == EditProfileCreateNew) {
            [[AgentProfileRegistrationModel shared].Top5Array removeObjectAtIndex:fromIndexPath.row];
            [[AgentProfileRegistrationModel shared].Top5Array insertObject:object atIndex:toIndexPath.row];
        }else{
            [[MyAgentProfileModel shared]
             .myAgentProfile.AgentPastClosing
             removeObjectAtIndex:fromIndexPath.row];
            
            [[MyAgentProfileModel shared]
             .myAgentProfile.AgentPastClosing insertObject:object
             atIndex:toIndexPath.row];
        }
    }
    
    DDLogInfo(@"moveRowAtIndexPath");
}

// This method is called when the selected row is released to its new position.
// The object is the same
// object you returned in saveObjectAndInsertBlankRowAtIndexPath:. Simply update
// the data source so the
// object is in its new position. You should do any saving/cleanup here.
- (void)finishReorderingWithObject:(id)object
                       atIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"finishReorderingWithObject");
    if (indexPath.section == 3) {
        if (![self isEmpty:object]) {
            if (self.viewType == EditProfileCreateNew) {
                [[AgentProfileRegistrationModel shared].Top5Array
                 replaceObjectAtIndex:indexPath.row
                 withObject:object];
            } else{
                [[MyAgentProfileModel shared]
                 .myAgentProfile.AgentPastClosing
                 replaceObjectAtIndex:indexPath.row
                 withObject:object];
                [[MyAgentProfileModel shared]
                 callAgentProfilePastClosingPositionUpdateByAgentPastClosingArray:[MyAgentProfileModel shared]
                 .myAgentProfile.AgentPastClosing
                 success:^(AgentProfile *myAgentProfile) {
                     
                     [self totalReloadTableView];
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error,
                           NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                           NSString *ok) {
                     [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                     
                 }];
            }
            self.movingIndexPath = nil;
        }
    }
}


#pragma mark button event--------------------------------------------------------------------------------------------------------------------------------------
- (UIButton *)addDeleteButton {
    UIButton *deletebutton = [[UIButton alloc]
                              initWithFrame:CGRectMake(self.tableView.frame.size.width - 33, 0, 44,
                                                       33)];
    
    [deletebutton setImage:[UIImage imageNamed:@"btn_pop_icon_remove"]
                  forState:UIControlStateNormal];
    [deletebutton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 12, 0)];
    [deletebutton addTarget:self
                     action:@selector(deleteDrug:)
           forControlEvents:UIControlEventTouchUpInside];
    
    return deletebutton;
}
- (void)deleteDrug:(id)sender {
    NSIndexPath *indexPath = [self indexPathForButton:sender];
    NSString *deletestringname;
    if (indexPath != nil) {
        if (indexPath.section == 0) {
            AgentExperience *AgentExperience =[self getRowObject:indexPath];
            deletestringname = [NSString
                                stringWithFormat:@"%@ - %@",AgentExperience.Company,AgentExperience.Title];
        } else if (indexPath.section == 3) {
            AgentPastClosing *AgentPastClosing =[self getRowObject:indexPath];
            deletestringname = AgentPastClosing.Address;
        }
    }
    NSString *deletestring = [NSString
                              stringWithFormat:JMOLocalizedString(@"edit_profile__confirm_to_delete_experience_desc", nil), deletestringname];
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:deletestring
                                                    delegate:nil
                                           cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                                      destructiveButtonTitle:JMOLocalizedString(@"common__delete", nil)
                                           otherButtonTitles:nil];
    as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            
            if (self.viewType != EditProfileCreateNew) {
                self.hud.labelText = JMOLocalizedString(@"alert__Initializing", nil);
                if ([self.delegate networkConnection]) {
                    [self loadingAndStopLoadingAfterSecond:10];
                }
                if (indexPath != nil) {
                    if (indexPath.section == 0) {
                        Experiencecell *cell =
                        (Experiencecell *)[self.tableView cellForRowAtIndexPath:indexPath];
                        
                        [[MyAgentProfileModel shared]
                         callAgentProfileExperienceDeleteAPIByIdString:cell.idstring
                         success:^(AgentProfile *myAgentProfile) {
                             
                             [self totalReloadTableView];
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                   NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                                   NSString *ok) {
                             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                             
                         }];
                    } else if (indexPath.section == 3) {
                        Top5cell *cell =
                        (Top5cell *)[self.tableView cellForRowAtIndexPath:indexPath];
                        
                        [[MyAgentProfileModel shared]
                         callAgentProfilePastClosingDeleteAPIByIdString:cell.idstring
                         success:^(AgentProfile *myAgentProfile) {
                             
                             [self totalReloadTableView];
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                   NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                                   NSString *ok) {
                             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                             
                         }];
                    }
                }
            }else{
                if (indexPath != nil) {
                    if (indexPath.section == 0) {
                        [[AgentProfileRegistrationModel shared].ExperienceArray removeObjectAtIndex:indexPath.row];
                        [[AgentProfileRegistrationModel shared]sortExperienceArray];
                        [self.tableView reloadData];
                    } else if (indexPath.section == 3) {
                        [[AgentProfileRegistrationModel shared].Top5Array removeObjectAtIndex:indexPath.row];
                        [self.tableView reloadData];
                    }
                    [self checkInputIsValid];
                }
            }
        }
        
    };
    
    [as showInView:self.view];
}

- (NSIndexPath *)indexPathForButton:(UIButton *)button {
    CGPoint point =
    [button convertPoint:button.bounds.origin toView:self.tableView];
    return [self.tableView indexPathForRowAtPoint:point];
}

- (void)addExperience:(id)sender {
    DDLogInfo(
              @"[MyAgentProfileModel shared].myAgentProfile.AgentPastClosing.count%lu",
              (unsigned long)[MyAgentProfileModel shared]
              .myAgentProfile.AgentPastClosing.count);
    [self.licenseno resignFirstResponder];
    
    if ([self canAddMoreExperience]) {
        Addexperience *addexperience =
        [[Addexperience alloc] initWithNibName:@"Addexperience" bundle:nil];
        addexperience.baseViewDelegate = self;
        self.navigationController.delegate = self;
        [self.navigationController pushViewController:addexperience animated:YES];
        
        DDLogInfo(@"addexperience");
    }else{
        [self showAlertWithTitle:nil message:[NSString stringWithFormat:JMOLocalizedString(@"alert_alert_add_experiences", nil),(int)kMaxAgentExperienceCount]];
    }
}
- (void)addLanguage:(id)sender {
    [self.licenseno resignFirstResponder];
    Spokenlanguageshaveprofile *spokenlanguageshaveprofile =
    [[Spokenlanguageshaveprofile alloc]
     initWithNibName:@"Spokenlanguageshaveprofile"
     bundle:nil];
    spokenlanguageshaveprofile.baseViewDelegate = self;
    self.navigationController.delegate = self;
    [self.navigationController pushViewController:spokenlanguageshaveprofile animated:YES];
    
    DDLogInfo(@"addlanguage");
}
- (void)addTop5:(id)sender {
    [self.licenseno resignFirstResponder];
    if ([self getSectionArraay:3].count >4) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                              message:JMOLocalizedString(@"edit_profile__max_top_5", nil)
                              delegate:nil
                              cancelButtonTitle:JMOLocalizedString(@"alert_ok", nil)
                              otherButtonTitles:nil];
        [alert show];
    } else {
        Top5 *top5 = [[Top5 alloc] initWithNibName:@"Top5" bundle:nil];
        top5.baseViewDelegate = self;
        self.navigationController.delegate = self;
        [self.navigationController pushViewController:top5 animated:YES];
        
        DDLogInfo(@"addTop5");
    }
}
- (void)addSpecialty:(id)sender {
    [self.licenseno resignFirstResponder];
    if ([self canAddMoreSpecialty]) {
        Specialty *specialty = [[Specialty alloc] initWithNibName:@"Specialty" bundle:nil];
        self.navigationController.delegate = self;
        specialty.baseViewDelegate = self;
        [self.navigationController pushViewController:specialty animated:YES];
        
        DDLogInfo(@"addSpecialty");
    }else{
        [self showAlertWithTitle:nil message:[NSString stringWithFormat:JMOLocalizedString(@"alert_alert_add_specialties", nil),(int)kMaxAgentSpecialtyCount]];
    }
}
- (IBAction)backButton:(id)sender {
    self.isDismissing = YES;
    [self dismiss:NO];
}

-(void)dismiss:(BOOL)editSucces{
    if ([self.editDelegate respondsToSelector:@selector(EditProfileDidEndEditing:success:)]) {
        [self.editDelegate EditProfileDidEndEditing:self.viewType == EditProfileCreateNew success:editSucces] ;
    }
    [self.delegate dismissOverlayerViewController:self];
}

- (IBAction)editButtonDidPress{
    Editprofilehaveprofile *editVC = [[Editprofilehaveprofile alloc]
                                      initWithNibName:@"Editprofilehaveprofile"
                                      bundle:nil];
    //        [[self.delegate getTabBarController]setNavBarHidden:YES animated:YES];
    editVC.editDelegate = self;
    editVC.underLayViewController = self;
    editVC.viewType = EditProfileEditView;
    BaseNavigationController *navController = [[BaseNavigationController alloc]initWithRootViewController:editVC];
    navController.navigationBar.hidden = YES;
    [self.delegate overlayViewController:navController onViewController:self];
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Edited Profile"];
}

- (IBAction)doneButtonDidPress:(id)sender{
    if ([self checkInputIsValid]) {
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Saved Agent Profile Details"];
        
        if ([self.delegate networkConnection]) {
            [self loadingAndStopLoadingAfterSecond:10];
        }
        
        [[AgentProfileRegistrationModel shared]  callAgentProfileRegistrationAPIAgentLicenseNo:self.licenseno.text Success:^(AgentProfile *myAgentProfile) {
            
            // Mixpanel
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Became Agent"];
            [mixpanel.people set:@"Agent" to:@"YES"];
            [mixpanel.people set:@"Agent since" to:[NSDate date]];
            
            [self dismiss:YES];
        }failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
        }];
    }
}

- (void)totalReloadTableView {
    [self tableViewSection2And4TokenFieldSetting];
    [self setUpMembernameAndPhotourl];
    [self hideLoadingHUD];
}


-(void)EditProfileDidEndEditing:(BOOL)isNewProfile success:(BOOL)success{
    [self totalReloadTableView];
    NSDictionary* userInfo = @{@"didEdit": @(1)};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateAgentListing" object:userInfo];
}

- (void)BaseViewControllerDidAction:(BaseViewAction)action withRef:(id)ref{
    [self checkInputIsValid];
    [self.tableView reloadData];
}

-(void)profileImageDidUpdate{
    [self setUpMembernameAndPhotourl];
}

- (BOOL)checkInputIsValid{
    self.doneButton.enabled = [self.licenseno.text valid] || [[AgentProfileRegistrationModel shared] inputValid];
    return [self.licenseno.text valid] || [[AgentProfileRegistrationModel shared] inputValid];
}

#pragma mark JSTokenFieldDelegate  ( tableViewSection2And4TokenField Delegate)----------------------------------------------------------------------------
- (void)tokenField:(JSTokenField *)tokenField
       didAddToken:(NSString *)title
 representedObject:(id)obj {
    NSDictionary *recipient =
    [NSDictionary dictionaryWithObject:title forKey:title];
    [self.tableViewSection2LanguageFieldArray addObject:recipient];
    
        [self checkInputIsValid];
    
    //  DDLogInfo(@"\n%@", self.toRecipients);
}

- (void)tokenField:(JSTokenField *)tokenField
didRemoveTokenAtIndex:(NSUInteger)index {
    DDLogInfo(@"didRemoveTokenAtIndex");
    [self.tableViewSection2LanguageFieldArray removeObjectAtIndex:index];
    // DDLogInfo(@"Deleted token %tu\n%@", index, self.toRecipients);
}
- (void)tokenField:(JSTokenField *)tokenField
    didRemoveToken:(NSString *)title
 representedObject:(id)obj {
    NSString *deletestringname ;
    if (tokenField == self.tableViewSection2LanguageField) {
        deletestringname = [NSString stringWithFormat:@"%@",title];
    }else if (tokenField == self.tableViewSection4SepecialtyField){
        deletestringname = [NSString stringWithFormat:@"%@",title];
    }
    NSString *deletestring = [NSString
                              stringWithFormat:JMOLocalizedString(@"edit_profile__confirm_to_delete_experience_desc", nil), deletestringname];
    
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:deletestring
                                                    delegate:nil
                                           cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                                      destructiveButtonTitle:JMOLocalizedString(@"common__delete", nil)
                                           otherButtonTitles:nil];
    
    as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex == 0) {
            self.hud.labelText = JMOLocalizedString(@"alert__Initializing", nil);
            if ([self.delegate networkConnection]) {
                [self loadingAndStopLoadingAfterSecond:10];
            }
            if (tokenField == self.tableViewSection2LanguageField) {
                if (self.viewType == EditProfileEditView) {
                    if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
                        [[MyAgentProfileModel shared]
                         callAgentProfileLanguageDeleteAPIByTitle:title
                         success:^(AgentProfile *myAgentProfile) {
                             
                             [self totalReloadTableView];
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                   NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                                   NSString *ok) {
                             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                             
                         }];
                    }
                }else{
                    AgentLanguage *agentLanguage = [self removeObjectFromAgentLanguageTitle:title];
                    if (![self isEmpty:agentLanguage]) {
                        [[AgentProfileRegistrationModel shared].LanguagesArray removeObject:agentLanguage];
                    }
                    [self totalReloadTableView];
                    [self checkInputIsValid];
                }
            }
            
            if (tokenField == self.tableViewSection4SepecialtyField) {
                if (self.viewType == EditProfileEditView) {
                    if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
                        [[MyAgentProfileModel shared]
                         callAgentProfileSpecialtyDeleteAPIByTitle:title
                         success:^(AgentProfile *myAgentProfile) {
                             
                             [self totalReloadTableView];
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                   NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                                   NSString *ok) {
                             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                             
                         }];
                    }
                }else{
                    AgentSpecialty *agentSpecialty = [self removeObjectFromSpecialtyArrayTitle:title];
                    if (![self isEmpty:agentSpecialty]) {
                        [[AgentProfileRegistrationModel shared].SpecialtyArray removeObject:agentSpecialty];
                    }
                    [self totalReloadTableView];
                    [self checkInputIsValid];
                }
                
            }
            
        }
    };
    [as showInView:self.view];
}
-(AgentSpecialty *)removeObjectFromSpecialtyArrayTitle:(NSString *)Title {
    for (AgentSpecialty *agentSpecialty in[AgentProfileRegistrationModel shared].SpecialtyArray) {
        DDLogDebug(@"agentSpecialty.specialty %@",agentSpecialty.specialty);
        if ([agentSpecialty.specialty isEqualToString:Title]) {
            return agentSpecialty;
        }
    }    return nil;
}
-(AgentLanguage *)removeObjectFromAgentLanguageTitle:(NSString *)Title {
    for (AgentLanguage *agentLanguage in[AgentProfileRegistrationModel shared].LanguagesArray) {
        DDLogDebug(@"agentSpecialty.Language %@",agentLanguage.Language);
        if ([agentLanguage.Language isEqualToString:Title]) {
            return agentLanguage;
        }
    }    return nil;
}
//// tableViewSection2And4TokenFieldSetting
- (void)tableViewSection2And4TokenFieldSetting {
    ////set language tokenField height to tableviewheight
    self.tableViewSection2LanguageField = [[JSTokenField alloc] initWithFrame:CGRectMake(29, 0, tableViewWidth-29*2, 31)];
    
    [self.tableViewSection2LanguageField setDelegate:self];
    
    self.tableViewSection2LanguageField.textField.userInteractionEnabled = NO;
    
    NSMutableString *languageString = [[NSMutableString alloc] init];
    
    NSArray *agentLang = [self getSectionArraay:1];
    if ([agentLang count] > 0) {
        DDLogInfo(@"self.HandleServerReturnString tostring-->%@",
                  [self.HandleServerReturnString toJSONString]);
        for (AgentLanguage *language in agentLang) {
            DDLogInfo(@"language.Language-->%@", language.Language);
            
            [self.tableViewSection2LanguageField addTokenWithTitle:language.Language
                                                 representedObject:language.Language withDeleteButton:self.viewType != EditProfilePreview];
            
            [languageString appendString:language.Language];
            
            if (language != agentLang.lastObject) {
                
                [languageString appendString:@"・"];
            }
        }
        
        [self.tableViewSection2LanguageField layoutSubviews];
        
        CGFloat labelWidth = tableViewWidth - 29*2;
        self.tableViewSection2Label = [[UILabel alloc] initWithFrame:CGRectMake(29, 0, labelWidth, FLT_MAX)];
        self.tableViewSection2Label.lineBreakMode = NSLineBreakByWordWrapping;
        self.tableViewSection2Label.numberOfLines = 0;
        self.tableViewSection2Label.preferredMaxLayoutWidth = labelWidth;
        self.tableViewSection2Label.font = [UIFont systemFontOfSize:14];
        self.tableViewSection2Label.textColor = [UIColor darkGrayColor];
        self.tableViewSection2Label.text = languageString;
        [self.tableViewSection2Label sizeToFit];
    }
    ////set specialty tokenField height to tableviewheight
    self.tableViewSection4SepecialtyField = [[JSTokenField alloc] initWithFrame:CGRectMake(29, 0, tableViewWidth, 31)];
    
    [self.tableViewSection4SepecialtyField setDelegate:self];
    
    self.tableViewSection4SepecialtyField.textField.userInteractionEnabled = NO;
    
    NSMutableString *specialtyString = [[NSMutableString alloc] init];
    
    NSArray *agentSpecialty = [self getSectionArraay:2];
    if ([agentSpecialty count] > 0) {
        for (AgentSpecialty *specialty in agentSpecialty) {
            
            DDLogInfo(@"specialty.specialty-->%@", specialty.specialty);
            [self.tableViewSection4SepecialtyField
             addTokenWithTitle:specialty.specialty
             representedObject:specialty.specialty withDeleteButton:self.viewType != EditProfilePreview];
            
            [specialtyString appendString:specialty.specialty];
            
            if (specialty != agentSpecialty.lastObject) {
                
                [specialtyString appendString:@"・"];
            }
        }
        [self.tableViewSection4SepecialtyField layoutSubviews];
        
        CGFloat labelWidth = tableViewWidth -29*2;
        self.tableViewSection4Label = [[UILabel alloc] initWithFrame:CGRectMake(29, 0, labelWidth, FLT_MAX)];
        self.tableViewSection4Label.lineBreakMode = NSLineBreakByWordWrapping;
        self.tableViewSection4Label.numberOfLines = 0;
        self.tableViewSection4Label.preferredMaxLayoutWidth = labelWidth;
        self.tableViewSection4Label.font = [UIFont systemFontOfSize:14];
        self.tableViewSection4Label.textColor = [UIColor darkGrayColor];
        self.tableViewSection4Label.text = specialtyString;
        [self.tableViewSection4Label sizeToFit];
    }
        [self checkInputIsValid];
}








#pragma mark -textfield Delegate-- ----------------------------------------------------------------------------------------------------------------------
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0 && !self.isDismissing) {
        NSString *deletestring = JMOLocalizedString(@"create_profile__update_license_number", nil);
        
        UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:deletestring
                                                        delegate:nil
                                               cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                                          destructiveButtonTitle:JMOLocalizedString(@"common__update", nil)
                                               otherButtonTitles:nil];
        
        as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        
        as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            
            if (buttonIndex == 0) {
                NSString *trimLicenseNumber = [self.licenseno.text trim];
                [[MyAgentProfileModel shared]
                 callAgentProfileAgentLicenseUpdateByAgentLicenseNumber:trimLicenseNumber
                 success:^(AgentProfile *myAgentProfile) {
                     [self totalReloadTableView];
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error,
                           NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                           NSString *ok) {
                     [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                     
                 }];
            }else{
                self.licenseno.text = [MyAgentProfileModel shared].myAgentProfile.AgentLicenseNumber;
            }
        };
        [as showInView:self.view];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *pendingString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    if ([pendingString valid]) {
        self.doneButton.enabled = YES;
    }else{
        self.doneButton.enabled =[[AgentProfileRegistrationModel shared] inputValid];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    [textField resignFirstResponder];
    return NO;
}
- (void)dismissKeyboard {
    [self.licenseno resignFirstResponder];
}

-(id)getRowObject:(NSIndexPath*)indexPath{
    NSArray *sectionArray =[self getSectionArraay:indexPath.section];
    if(sectionArray.count > indexPath.row){
        return sectionArray[indexPath.row];
    }else{
        return nil;
    }
}

-(NSArray*)getSectionArraay:(NSInteger)section{
    NSArray *sectionArray = nil;
    if (section == 0) {
        if (self.viewType != EditProfileCreateNew) {
            sectionArray = [MyAgentProfileModel shared].myAgentProfile.AgentExperience;
        }else{
            sectionArray =[AgentProfileRegistrationModel shared].ExperienceArray;
        }
        
    }else if(section ==1){
        if (self.viewType != EditProfileCreateNew) {
            sectionArray = [MyAgentProfileModel shared].myAgentProfile.AgentLanguage;
        }else{
            sectionArray =[AgentProfileRegistrationModel shared].LanguagesArray;
        }
    }else if(section ==2){
        if (self.viewType != EditProfileCreateNew) {
            sectionArray = [MyAgentProfileModel shared].myAgentProfile.AgentSpecialty;
        }else{
            sectionArray =[AgentProfileRegistrationModel shared].SpecialtyArray;
        }
    }else if(section ==3){
        if (self.viewType != EditProfileCreateNew) {
            sectionArray = [MyAgentProfileModel shared].myAgentProfile.AgentPastClosing;
        }else{
            sectionArray =[AgentProfileRegistrationModel shared].Top5Array;
        }
    }
    
    return sectionArray;
}


-(IBAction)profileButtonDidPress:(id)sender{
    [kAppDelegate showImagePickerOn:self];
}

-(void)uploadProfileImage:(UIImage*)image{
    [self showLoadingHUDWithProgress:JMOLocalizedString(@"agent_profile__uploading_data", nil)];
    
    [[MyAgentProfileModel shared]uploadProfileImage:image withBlock:^(id response, NSError *error) {
        if (!error && response) {
            NSString *photoURL = response[@"PhotoURL"];
            if (photoURL) {
                [LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL = photoURL;
                [MyAgentProfileModel shared].myAgentProfile.AgentPhotoURL = photoURL;
                [[[DBLogInfo query] fetch] removeAll];
                [[[[DBMyAgentProfile query] whereWithFormat:@" MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID] fetch] removeAll];
                DBMyAgentProfile *dbmyagentprofile = [DBMyAgentProfile new];
                dbmyagentprofile.Date = [NSDate date];
                dbmyagentprofile.MyAgentProfile = [NSKeyedArchiver
                                                   archivedDataWithRootObject:[[MyAgentProfileModel shared].myAgentProfile copy]];
                dbmyagentprofile.MemberID = [[LoginBySocialNetworkModel shared]
                                             .myLoginInfo.MemberID intValue];
                [dbmyagentprofile commit];
                [[LoginBySocialNetworkModel shared].myLoginInfo saveInDataBase];
                [[LoginBySocialNetworkModel shared].myLoginInfo updateCustomData];
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationMyAgentProfileImageDidUpdate object:nil];
            }
            
        }else if(error && response){
            NSString *errorMessage = response [@"ErrorMsg"];
            [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:errorMessage];
        }else{
            [self showAlertWithTitle:nil message:JMOLocalizedString(@"error_message__server_not_response", nil)];
        }
        [self hideLoadingHUD];
    }];
    
}

#pragma mark -UIImagePickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    // assets contains PHAsset objects.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    
    // this one is key
    requestOptions.synchronous = true;
    PHAsset *asset = [assets firstObject];
    if (asset) {
        [RealUtility getImageBy:asset size:RealUploadPhotoSize handler:^(UIImage *result, NSDictionary *info) {
            RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:result];
            imageCropVC.delegate = self;
            [picker.navigationController pushViewController:imageCropVC animated:YES];
        }];
    }else{
        
    }
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    return picker.selectedAssets.count<1;
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [controller.navigationController popViewControllerAnimated:YES];
}

// The original image has been cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect{
    [self uploadProfileImage:croppedImage];
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
}

// The original image has been cropped. Additionally provides a rotation angle used to produce image.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
                  rotationAngle:(CGFloat)rotationAngle{
    [self uploadProfileImage:croppedImage];
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
}

// The original image will be cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                  willCropImage:(UIImage *)originalImage{
    // Use when `applyMaskToCroppedImage` set to YES.
    //    [SVProgressHUD show];
}

- (BOOL) canAddMoreExperience{
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        return kMaxAgentExperienceCount > [MyAgentProfileModel shared].myAgentProfile.AgentExperience.count ;
    } else {
        return kMaxAgentExperienceCount > [AgentProfileRegistrationModel shared].ExperienceArray.count ;
    }
}

- (BOOL) canAddMoreSpecialty{
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        return kMaxAgentSpecialtyCount > [MyAgentProfileModel shared].myAgentProfile.AgentSpecialty.count ;
    } else {
        return kMaxAgentSpecialtyCount >  [AgentProfileRegistrationModel shared].SpecialtyArray.count ;
    }
    
    return NO;
}

@end
