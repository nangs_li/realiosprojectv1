//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentExperienceItemView.h"
@interface Experiencecell : UITableViewCell
@property(weak, nonatomic) UITableView *tableView;
@property(nonatomic, strong) AgentExperienceItemView *itemView;
@property(strong, nonatomic) IBOutlet UILabel *company;
@property(strong, nonatomic) IBOutlet UILabel *title;
@property(strong, nonatomic) IBOutlet UILabel *date;
@property(strong, nonatomic) UIButton *deleteButton;
@property(strong, nonatomic) NSString *idstring;

-(void)configureCell:(AgentExperience*)experience withStyle:(ItemViewStyle)itemStyle;
@end
