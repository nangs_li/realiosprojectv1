//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "Experiencecell.h"

#import <QuartzCore/QuartzCore.h>

@implementation Experiencecell


-(void)configureCell:(AgentExperience*)experience withStyle:(ItemViewStyle)itemStyle{
    if(!self.itemView){
        self.itemView = [[AgentExperienceItemView alloc]initWithAgentExperience:experience size:CGSizeMake([RealUtility screenBounds].size.width -32, 60) withStyle:itemStyle];
        [self.contentView addSubview:self.itemView];
    }else{
        [self.itemView configureWithExperience:experience];
    }
    CGRect itemRect = self.itemView.frame;
    itemRect.origin.y = 0;
    self.itemView.frame = itemRect;
    self.idstring = [NSString stringWithFormat:@"%d", experience.ID];

}
@end
