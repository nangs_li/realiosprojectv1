//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

@interface Finishprofilehavelisting : BaseViewController
@property(strong, nonatomic) IBOutlet UIImageView *personalphoto;
@property(strong, nonatomic) IBOutlet UILabel *membername;
@property(strong, nonatomic) IBOutlet UIView *AgentListing;
@property(strong, nonatomic) IBOutlet UIImageView *listingcoverphoto;
@property(strong, nonatomic) IBOutlet UILabel *price;
@property(strong, nonatomic) IBOutlet UILabel *size;
@property(strong, nonatomic) IBOutlet UILabel *numberofbedroom;
@property(strong, nonatomic) IBOutlet UILabel *numberofbathroom;
@property(strong, nonatomic) IBOutlet UILabel *followercount;
@property(strong, nonatomic) IBOutlet UILabel *followingcount;
@property(strong, nonatomic) IBOutlet UIButton *updatebutton;
@property(strong, nonatomic) IBOutlet UILabel *street;
@property(strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong, nonatomic) IBOutlet UIView *contentView;
@end
