//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Finishprofilehavelisting.h"
#import "Editprofile.h"
#import "Uploadphoto.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Photos.h"
#import "AgentProfile.h"
#import "Editprofilehaveprofile.h"
#import "DBMyAgentProfile.h"
#import "SystemSettingModel.h"
#import "RealUtility.h"

@interface Finishprofilehavelisting ()
@end
@implementation Finishprofilehavelisting

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  //// set delegate store posting data to nil
  [[AgentListingModel shared] resetAllAgentListingPartOnlyLocalData];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
  //// set posting information
  AddressUserInput *addressuserinput =
      [[MyAgentProfileModel shared]
              .myAgentProfile.AgentListing.AddressUserInput objectAtIndex:0];
  self.street.text = addressuserinput.Street;

  self.membername.text =
      [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;

  NSURL *url = [NSURL
      URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];
    [self.personalphoto loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
  [self.delegate setupcornerRadius:self.personalphoto];

  self.followercount.text =
      [NSString stringWithFormat:@"%d", [MyAgentProfileModel shared]
                                            .myAgentProfile.FollowerCount];
  self.followingcount.text =
      [NSString stringWithFormat:@"%d", [MyAgentProfileModel shared]
                                            .myAgentProfile.FollowerCount];

  self.numberofbedroom.text = [NSString
      stringWithFormat:@"%d", [MyAgentProfileModel shared]
                                  .myAgentProfile.AgentListing.BedroomCount];
  self.numberofbathroom.text = [NSString
      stringWithFormat:@"%d", [MyAgentProfileModel shared]
                                  .myAgentProfile.AgentListing.BathroomCount];
  self.price.text =
      [MyAgentProfileModel shared]
          .myAgentProfile.AgentListing.PropertyPriceFormattedForRoman;

  MetricList *metric;
  DDLogInfo(@"self.delegate.metricaccordingtolanguage-->%@",
            [SystemSettingModel shared].metricaccordingtolanguage);
  if (![self isEmpty:[SystemSettingModel shared].metricaccordingtolanguage]) {
    if ([MyAgentProfileModel shared].myAgentProfile.AgentListing.SizeUnitType >
        1) {
      metric = [[SystemSettingModel shared]
                    .metricaccordingtolanguage objectAtIndex:1];
    } else {
      metric = [[SystemSettingModel shared]
                    .metricaccordingtolanguage
          objectAtIndex:[MyAgentProfileModel shared]
                            .myAgentProfile.AgentListing.SizeUnitType];
    }
  }

  self.size.text = [NSString
      stringWithFormat:@"%d %@", [MyAgentProfileModel shared]
                                     .myAgentProfile.AgentListing.PropertySize,
                       metric.NativeName];

  if (![self isEmpty:[MyAgentProfileModel shared]
                         .myAgentProfile.AgentListing.Photos]) {
    Photos *photo = [[MyAgentProfileModel shared]
                         .myAgentProfile.AgentListing.Photos objectAtIndex:0];

    NSString *photostring = [NSString stringWithFormat:@"%@", photo.URL];
    self.listingcoverphoto.contentMode = UIViewContentModeScaleAspectFill;
    self.listingcoverphoto.clipsToBounds = YES;
      [self.personalphoto loadImageURL:[NSURL URLWithString:photostring] withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
          
      }];
    self.updatebutton.hidden = NO;

  } else {
  }

  UINavigationController *nc =
      (UINavigationController *)self.navigationController;
  [nc setViewControllers:@[ self ] animated:NO];

  self.scrollView.contentSize = self.contentView.frame.size;
  if (self.scrollView.contentSize.height <= self.scrollView.frame.size.height) {
    self.scrollView.scrollEnabled = NO;
  }

}

- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];

  [super viewDidLoad];
}

#pragma mark Button
- (IBAction)editProfilePressed:(id)sender {
  if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
    Editprofilehaveprofile *UIViewController = [[Editprofilehaveprofile alloc]
        initWithNibName:@"Editprofilehaveprofile"
                 bundle:nil];
    CATransition *transition = [CATransition animation];
    transition.duration = ANIMATION_DURATION;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    [self.navigationController.view.layer addAnimation:transition
                                                forKey:kCATransition];
    [self.navigationController pushViewController:UIViewController animated:NO];
  } else {
    Editprofile *UIViewController =
        [[Editprofile alloc] initWithNibName:@"Editprofile" bundle:nil];
    CATransition *transition = [CATransition animation];
    transition.duration = ANIMATION_DURATION;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;

    [self.navigationController.view.layer addAnimation:transition
                                                forKey:kCATransition];

    [self.navigationController pushViewController:UIViewController animated:NO];
  }
}

- (IBAction)createNewPressed:(id)sender {
  Uploadphoto *UIViewController =
      [[Uploadphoto alloc] initWithNibName:@"Uploadphoto" bundle:nil];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController pushViewController:UIViewController animated:NO];
}

@end