//
//  MeProfileViewController.h
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "BaseViewController.h"
#import "RealUtility.h"
#import "FXBlurView.h"
#import "YLProgressBar.h"
#import "MCPercentageDoughnutView.h"
#import "Editprofilehaveprofile.h"
#import "ApartmentDetailViewController.h"
#import "MMDrawerController.h"
#import "UIScrollView+EmptyDataSet.h"

@interface MeProfileViewController : BaseViewController <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,UITableViewDataSource,UITableViewDelegate, MMDrawerTouchDelegate,EditProfileDelegate,RSKImageCropViewControllerDelegate>
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *backgroundImageHeight;
@property (nonatomic,strong) MMDrawerController *MMDrawerController;
@property (nonatomic,strong) Editprofilehaveprofile *Onepageviewcontroller;
@property (nonatomic,strong) ApartmentDetailViewController *Threepageviewcontroller;
@property (nonatomic,assign) AgentStatus agentStatus;
@property (nonatomic,strong) NSString *pendingAction;
@property (nonatomic,strong) IBOutlet FXBlurView *backgroundBlurView;
@property (nonatomic,strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic,strong) IBOutlet UIView *completeProfileContainer;
@property (nonatomic,strong) IBOutlet UILabel *completeLabel;
@property (nonatomic,strong) IBOutlet YLProgressBar *completeProgressBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *followViewTopSpace;


@property (nonatomic,strong) IBOutlet UIView *followInfoView;
@property (nonatomic,strong) IBOutlet UILabel *followerLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingLabel;
@property (nonatomic,strong) IBOutlet UILabel *followerTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingTitleLabel;

@property (nonatomic,strong) IBOutlet UIView *profileImageContainer;
@property (nonatomic,strong) IBOutlet UIImageView *profileImageView;
@property (nonatomic,strong) IBOutlet UILabel *profileNameLabel;
@property (nonatomic,strong) IBOutlet MCPercentageDoughnutView *profileCircularProgressBar;

@property (nonatomic,strong) IBOutlet UIView *agentListingContainer;
@property (nonatomic,strong) IBOutlet UITableView *listingTableView;
@property (nonatomic,strong) IBOutlet UIView *noPostingView;
@property (nonatomic,strong) IBOutlet UIView *backgroundView;
@property (nonatomic,strong) IBOutlet UIButton *actionButton;
@property (nonatomic,strong) IBOutlet UIButton *inviteButton;
@property (nonatomic,strong) IBOutlet UIButton *editButton;


@property (nonatomic,strong) IBOutlet UIButton *followerButton;
@property (nonatomic,strong) IBOutlet UIButton *followingButton;
@property (nonatomic,strong) IBOutlet UIView *followNotificationDotView;
//@property (nonatomic,strong) MCPercentageDoughnutView *completer
- (IBAction)followerButtonDidPress:(UIButton*)sender;
- (IBAction)followingButtonDidPress:(UIButton*)sender;
- (IBAction)backgroundButtonDidPress:(id)sender;
- (IBAction)actionButtonDidPress:(id)sender;
@end
