//
//  MeProfileViewController.m
//  productionreal2
//
//  Created by Alex Hung on 15/9/15.
//  Copyright (c) 2015 Real. All rights reserved.
//

#import "MeProfileViewController.h"
#import "Taochum.h"
#import "RealUtility.h"
#import "DeepLinkActionHanderModel.h"
#import "NotificationMessageCenter.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import "CMPopTipView.h"
#import "ActivityLogView.h"
#import "RealApiClient.h"
#import "ActivityLogGetModel.h"
#import "FollowerListGetModel.h"
#import "FollowerModel.h"
#import "DBMyAgentProfile.h"
#import "DBLogInfo.h"
#import "MyActivityLogModel.h"
#import "Reachability.h"
#import "StoryTableViewCell.h"
#import "StoryTableHeaderView.h"
#define TrackColor [UIColor colorWithRed:93/255.0f green:93/255.0f blue:93/255.0f alpha:1.0f]
#define kLogViewIsAgentDefaultBottomSpaceConstant 57
#define kLogViewIsNotAgentDefaultBottomSpaceConstant 105
#define kLogViewMaxPanningDistance 20
// Mixpanel
#import "Mixpanel.h"

// Controllers
#import "ViralViewController.h"
#import "PhoneNumberViewController.h"
#import "RealPhoneBookShareViewController.h"

@interface MeProfileViewController () <CMPopTipViewDelegate,MoveToCenterPage,ActivityLogViewDelegate,RDVTabBarControllerDelegate>
@property (nonatomic,strong) NSMutableArray *activityLogItems;
@property (nonatomic,strong) NSMutableArray *followerLogItems;
@property (nonatomic,assign) NSInteger totalActivityLogItemCount;
@property (nonatomic,assign) NSInteger activityLogCurrentPage;
@property (nonatomic,assign) BOOL needChangeType;
@property (nonatomic,strong) CMPopTipView *inviteToFollowTipView;
@property (nonatomic,strong) ActivityLogView *activityLogView;
@property (nonatomic,strong) NSLayoutConstraint *activityLogViewHeightConstraint;
@property (nonatomic,strong) NSLayoutConstraint *activityLogViewBottomSpaceConstraint;
@property (nonatomic,assign) CGRect originalLogViewFrame;
@property (nonatomic,strong) NSTimer *logViewBouncingTimer;
@property (nonatomic,assign) NSInteger tapCount;
@property (nonatomic,weak) UIViewController *previousHandledViewController;
@end

@implementation MeProfileViewController
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.delegate getTabBarController].delegate = self;
    if (self.MMDrawerController.openSide == MMDrawerSideNone){
        self.navBarContentView.alpha =1.0;
    }
    
    if([self.pendingAction valid]){
        self.pendingAction = nil;
        [self actionButtonDidPress:self.actionButton];
    }
    if (!isEmpty(self.activityLogView)) {
        [self.activityLogView.tableView reloadData];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        return YES;
    }
    
    return NO;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(followingCountDidChange)
     name:kNotificationFollowOrUnFollowThenReloadSearchNewsFeedTableView
     object:nil];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(agentImageTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.profileImageContainer addGestureRecognizer:singleTap];
    
    [self.actionButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    self.followNotificationDotView.layer.cornerRadius = CGRectGetHeight(self.followNotificationDotView.frame)/2;
    //    [self configureWithAgentStatus:self.agentStatus];
    CGRect backgroundFrame =CGRectMake(0, 0, self.view.frame.size.width, [RealUtility screenBounds].size.height);
    self.backgroundView.frame =backgroundFrame;
    self.backgroundImageView.frame = self.backgroundView.bounds;
    self.backgroundImageHeight.constant = [RealUtility screenBounds].size.height;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateAgentStatus:) name:@"DidUpdateAgentListing" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileImageDidUpdate) name:kNotificationMyAgentProfileImageDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didReceiveNewFollow) name:kNotificationDidReceiveNewFollow object:nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ((self.MMDrawerController.openSide == MMDrawerSideNone && !self.presentedViewController )||self.needChangeType) {
        self.needChangeType = NO;
        [self setupNavBar];
        [self configureWithAgentStatus:self.agentStatus];
    }
    if(self.MMDrawerController.openSide == MMDrawerSideNone){
        [self showPageControl:[MyAgentProfileModel shared].myAgentProfile.MemberID > 0 animated:self.MMDrawerController.openSide == MMDrawerSideNone];
    }
    if (self.MMDrawerController.openSide != MMDrawerSideNone) {
        [self.MMDrawerController openDrawerSide:self.MMDrawerController.openSide animated:NO completion:nil];
    }
    
    
    //        [self showInviteToFollowTutorial];
    [self cachedActivityLogWithPage:self.activityLogCurrentPage];
    [self didReceiveNewFollow];
    
    self.MMDrawerController.touchDelegate = self;
    [self.delegate getTabBarController].customNavBarController.delegate=self;
    [[[AppDelegate getAppDelegate]getTabBarController]setNavBarHidden:NO];
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]setTabBarHidden:NO animated:NO];
    }
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    if (self.activityLogView) {
        if (self.activityLogView.logViewType==ActivityLogViewTypeFollow) {
            [self.activityLogView setupWithType:ActivityLogViewTypeFollow initialArray:self.activityLogItems];
        }else{
            [self.activityLogView setupWithType:ActivityLogViewTypeFollowing initialArray:self.followerLogItems];
        }
    }
}
- (void)reloadCachedActivtyLogWithOffset:(int)offset withLimit:(int)limit {
    if (self.activityLogView.logViewType == ActivityLogViewTypeFollow) {
        [[RealApiClient sharedClient]cachedActivityLogGetWithOffset:offset limit:limit withBlock:^(ActivityLogGetModel *response, NSError *error) {
            if (response.Log) {
                self.activityLogItems = [NSMutableArray arrayWithArray:response.Log];
                self.activityLogCurrentPage = lroundf(self.activityLogItems.count)/20;
                self.activityLogCurrentPage ++;
                [self.activityLogView configureWithLogs:self.activityLogItems type:ActivityLogViewTypeFollow];
                
            }
        }];
    }else if (self.activityLogView.logViewType == ActivityLogViewTypeFollowing){
        [[RealApiClient sharedClient]cachedFollowerLogGetWithOffset:offset limit:limit withBlock:^(FollowerListGetModel *response, NSError *error) {
            if (response.Followers) {
                self.followerLogItems = [NSMutableArray arrayWithArray:response.Followers];
                self.activityLogCurrentPage = lroundf(self.followerLogItems.count)/20;
                self.activityLogCurrentPage ++;
                [self.activityLogView configureWithLogs:self.followerLogItems type:ActivityLogViewTypeFollowing];
                
            }
        }];
    }
}

- (void)cachedActivityLogWithPage:(NSInteger)page{
    if (self.activityLogView.logViewType == ActivityLogViewTypeFollow) {
        [[RealApiClient sharedClient]cachedActivityLogGetWithPage:page withBlock:^(ActivityLogGetModel *response, NSError *error) {
            if (response.Log.count > 0) {
                if (page == 1) {
                    self.activityLogItems = [NSMutableArray arrayWithArray:response.Log];
                }else{
                    [self.activityLogItems addObjectsFromArray:response.Log];
                }
                self.activityLogCurrentPage = page;
                self.activityLogCurrentPage ++;
                [self.activityLogView configureWithLogs:self.activityLogItems type:ActivityLogViewTypeFollow];
                [self.activityLogView resetInfinitoScrollState];
            }else if(self.activityLogView.haveMoreItem){
                [self activityLogGetWithPage:page showLoading:NO];
            }
        }];
    }else if (self.activityLogView.logViewType == ActivityLogViewTypeFollowing){
        [[RealApiClient sharedClient]cachedFollowerLogGetWithPage:page withBlock:^(FollowerListGetModel *response, NSError *error) {
            if (response.Followers.count > 0) {
                if (page == 1) {
                    self.followerLogItems = [NSMutableArray arrayWithArray:response.Followers];
                }else{
                    [self.followerLogItems addObjectsFromArray:response.Followers];
                }
                self.activityLogCurrentPage = page;
                self.activityLogCurrentPage ++;
                [self.activityLogView configureWithLogs:self.followerLogItems type:ActivityLogViewTypeFollowing];
                [self.activityLogView resetInfinitoScrollState];
            }else if (self.activityLogView.haveMoreItem){
                [self activityLogGetWithPage:page showLoading:NO];
            }
        }];
    }
}

- (void)activityLogGetWithPage:(NSInteger)page showLoading:(BOOL)showLoading{
    if (showLoading) {
        [self.activityLogView showLoading:YES];
    }
    if (self.activityLogView.logViewType == ActivityLogViewTypeFollow) {
        [[RealApiClient sharedClient]activityLogGetWithPage:page WithBlock:^(ActivityLogGetModel *response, NSError *error) {
            if (error) {
                
            }else{
                self.totalActivityLogItemCount = response.TotalCount;
                NSArray *incomingArray = response.Log;
                if (incomingArray) {
                    if (page == 1) {
                        [MyActivityLogModel shared].newLogCount = 0;
                        self.followNotificationDotView.hidden = YES;
                        ActivityLogModel *firstModel = [incomingArray firstObject];
                        ActivityLogModel *firstCachedModel = [self.activityLogItems firstObject];
                        if (firstModel.ID != firstCachedModel.ID) {
                            int limit = (int)self.activityLogItems.count;
                            if(limit <20){
                                limit = 20;
                            }
                            [self reloadCachedActivtyLogWithOffset:0 withLimit:limit];
                        }
                        [self cachedActivityLogWithPage:page];
                        [self.activityLogView endPullToRefreshAnimation];
                    }else{
                        [self cachedActivityLogWithPage:page];
                        [self.activityLogView resetInfinitoScrollState];
                    }
                }
                self.activityLogView.haveMoreItem = response.RecordCount > 0;
            }
            if (showLoading) {
                [self.activityLogView showLoading:NO];
            }
        }];
    }else if (self.activityLogView.logViewType == ActivityLogViewTypeFollowing){
        [[RealApiClient sharedClient] followingListGetWithPage:page block:^(FollowerListGetModel *response, NSError *error) {
            if (error) {
                
            }else{
                self.totalActivityLogItemCount = response.TotalCount;
                NSArray *incomingArray = response.Followers;
                if (incomingArray) {
                    if (page == 1) {
                        FollowerModel *firstModel = [incomingArray firstObject];
                        FollowerModel *firstCachedModel = [self.followerLogItems firstObject];
                        if (firstModel.MemberID  != firstCachedModel.MemberID) {
                            int limit = (int) self.followerLogItems.count;
                            if (limit< 20) {
                                limit = 20;
                            }
                            [self reloadCachedActivtyLogWithOffset:0 withLimit:limit];
                        }
                        [self cachedActivityLogWithPage:page];
                        [self.activityLogView endPullToRefreshAnimation];
                    }else{
                        [self cachedActivityLogWithPage:page];
                        [self.activityLogView resetInfinitoScrollState];
                    }
                }
                self.activityLogView.haveMoreItem = response.RecordCount > 0;
            }
            if (showLoading) {
                [self.activityLogView showLoading:NO];
            }
        }];
        
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[[AppDelegate getAppDelegate]getTabBarController] customNavBarController].apartmentDetailMoreButton.hidden=NO;
    if (self.activityLogView && !self.activityLogView.hidden && self.activityLogView.logViewType == ActivityLogViewTypeFollowing) {
        [self activityLogGetWithPage:1 showLoading:NO];
    }
}
-(void)setupNavBar{
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        
        if(didChange){
            self.navBarContentView             = navBarController.meNavBar;
            self.navBarContentView.alpha       = 1;
            self.editButton = navBarController.meEditButton;
            self.inviteButton                  = navBarController.meInviteButton;
            self.inviteButton.hidden           = ![[MyAgentProfileModel shared].myAgentProfile haveAgentListing];
            [self.inviteButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [self.inviteButton addTarget:self action:@selector(inviteButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
            [self.editButton addTarget:self action:@selector(agentImageTapping:) forControlEvents:UIControlEventTouchUpInside];
            self.followingTitleLabel.text      = JMOLocalizedString(@"common__following", nil);
            self.followerTitleLabel.text       = JMOLocalizedString(@"common__followers", nil);
            navBarController.meTitleLabel.text = JMOLocalizedString(@"me__title", nil);
            if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
                UITapGestureRecognizer *singleFingerTap =
                [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(handleSingleTap:)];
                [self.profileImageContainer addGestureRecognizer:singleFingerTap];
            }
            
        }
        
    }];
}
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [[[AppDelegate getAppDelegate]getTabBarController]setTabBarHidden:YES];
    [self.MMDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        if (finished) {
            [self changePage:MMDrawerSideRight];
        }
    }];
    
}
-(void)updateAgentStatus:(NSNotification*)notification{
    NSDictionary* userInfo = notification.object;
    BOOL didEdit = [userInfo[@"didEdit"]boolValue];
    BOOL tabBarShouldHide = NO;
    if(userInfo[@"tabBarShouldHide"]){
        tabBarShouldHide = [userInfo[@"tabBarShouldHide"] boolValue];
    }
    if (didEdit) {
        AgentStatus status;
        if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
            if ([RealUtility isValid:[MyAgentProfileModel shared].myAgentProfile.AgentListing]) {
                status = AgentStatusWithListing;
                //                [self showInviteToFollowTutorial];
            } else {
                status = AgentStatusWithoutListing;
            }
        } else {
            status = AgentStatusNotAgent;
        }
        self.agentStatus = status;
        self.inviteButton.hidden = ![[MyAgentProfileModel shared].myAgentProfile haveAgentListing];
        [self configureWithAgentStatus:status];
    }
    
    [self configureCreateListingButton];
    if (self.MMDrawerController.openSide == MMDrawerSideNone) {
        [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:tabBarShouldHide];
    }
}

- (void)configureCreateListingButton{
    if (self.agentStatus == AgentStatusWithListing || self.agentStatus == AgentStatusWithoutListing) {
        if([[AgentListingModel shared] haveHistory]){
            NSString * continueMyPostingString =JMOLocalizedString(@"me__continue_my_posting", nil);
            [self.actionButton setTitle:continueMyPostingString forState:UIControlStateNormal];
        }else if(self.agentStatus == AgentStatusWithListing){
            [self.actionButton setTitle:JMOLocalizedString(@"me__btn_update_now", nil) forState:UIControlStateNormal];
        }else if (self.agentStatus == AgentStatusWithoutListing){
            [self.actionButton setTitle:JMOLocalizedString(@"me__create_posting_now", nil) forState:UIControlStateNormal];
        }
    }
}

- (void)moveToCenterPage{
    
    [[self.delegate getTabBarController]setTabBarHidden:NO animated:YES];
    [self.MMDrawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        [self changePage:MMDrawerSideNone];
    }];
}
-(void) changePage:(MMDrawerSide)drawerSide{
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}



-(void)applicationWillResignActive{
    
}


-(void)showInviteToFollowTutorial{
    if([kAppDelegate userNeedShowTutorial:TutorialTypeInviteToFollow]){
        self.inviteToFollowTipView = [[CMPopTipView alloc] initWithMessage:@"Invite people to follow you!"];
        self.inviteToFollowTipView.delegate = self;
        self.inviteToFollowTipView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        self.inviteToFollowTipView.textColor = [UIColor whiteColor];
        self.inviteToFollowTipView.has3DStyle = NO;
        self.inviteToFollowTipView.hasGradientBackground = NO;
        self.inviteToFollowTipView.hasShadow = YES;
        self.inviteToFollowTipView.dismissTapAnywhere = YES;
        self.inviteToFollowTipView.borderWidth =0;
        self.inviteToFollowTipView.animation = CMPopTipAnimationPop;
        [self.inviteToFollowTipView presentPointingAtView:self.inviteButton inView:self.navBarContentView animated:YES];
        
    }
}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView{
    [kAppDelegate userNeedShowTutorial:TutorialTypeInviteToFollow];
    
}
-(void)profileImageDidUpdate{
    NSURL *profileImageURL = [NSURL URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];
    //    [self.profileImageView sd_setImageWithURL:profileImageURL placeholderImage:nil options:SDWebImageHighPriority];
    [self.profileImageView loadImageURL:profileImageURL withIndicator:YES];
    [self updateBackgroundImageView];
    self.Threepageviewcontroller.AgentProfile = nil;
}

-(void)updateBackgroundImageView{
    
    NSString *imageURL = nil;
    if (self.agentStatus == AgentStatusWithListing) {
        Photos *photos = [[MyAgentProfileModel shared].myAgentProfile.AgentListing.Photos firstObject];
        imageURL = photos.URL;
    }
    if (!imageURL) {
        imageURL = [LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL;
    }
    NSURL *backgroundImageURL = [NSURL URLWithString:imageURL];
    __weak typeof(self) weakSelf = self;
    [self.backgroundImageView loadImageURL:backgroundImageURL withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            UIImage *blurImage = [[UIImage imageWithView:self.backgroundImageView]blurredImageWithRadius:MaxImageBlurRadius iterations:5 tintColor:[UIColor clearColor]];
            UIImage *layerImage= [UIImage add_imageWithColor:[UIColor colorWithWhite:0 alpha:0.5] size:blurImage.size];
            blurImage = [blurImage add_imageAddingImage:layerImage];
            weakSelf.backgroundImageView.image = blurImage;
            if (cacheType == SDImageCacheTypeNone) {
                weakSelf.backgroundImageView.alpha = 0.0;
                [UIView animateWithDuration:1.0
                                 animations:^{
                                     weakSelf.backgroundImageView.alpha = 1.0;
                                 }];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)panGestureCallback:(UIPanGestureRecognizer *)panGesture {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
            if ([self.Threepageviewcontroller isKindOfClass:[ApartmentDetailViewController class]]) {
                ApartmentDetailViewController *vc = (ApartmentDetailViewController*) self.Threepageviewcontroller;
                //                [MyAgentProfileModel shared].myAgentProfile.status = AgentProfileStatusEdit;
                vc.pendingAgentProfile = [MyAgentProfileModel shared].myAgentProfile;
                vc.pendingAgentProfile.status = AgentProfileStatusEdit;
                vc.pendingAgentProfile.meProfile = @(1);
            }
            break;
        case UIGestureRecognizerStateChanged:
            break;
            
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            DDLogInfo(@"-panLocationend%f", panLocation.x);
            DDLogInfo(@"UIGestureRecognizerStateCancelled-");
            if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
                DDLogInfo(@"UIGestureRecognizerState- two page");
                
                DDLogInfo(@"self.MMDrawerController.centerContainerView-->%ld",
                          (long)self.MMDrawerController.openSide);
                //                [self configureWithAgentStatus:self.agentStatus];
                //                [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
            } else if (320 < panLocation.x) {
                DDLogInfo(@"UIGestureRecognizerState- three page");
                if ([self.Threepageviewcontroller isKindOfClass:[ApartmentDetailViewController class]]) {
                    ApartmentDetailViewController *vc = (ApartmentDetailViewController*) self.Threepageviewcontroller;
                    vc.pendingAgentProfile = [MyAgentProfileModel shared].myAgentProfile;
                }
            } else if (0 > panLocation.x) {
                DDLogInfo(@"UIGestureRecognizerState- one page");
            }
            break;
            
        default:
            break;
    }
}

- (void)openSideDidChange:(MMDrawerSide)drawerSide{
    if (drawerSide == MMDrawerSideNone) {
        [[self.delegate getTabBarController]changePage:1];
    }else if (drawerSide == MMDrawerSideRight){
        [[self.delegate getTabBarController]changePage:2];
    }else if (drawerSide == MMDrawerSideLeft){
        [[self.delegate getTabBarController]changePage:0];
    }
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if(self.Threepageviewcontroller.didChangeToImageTableView ||self.presentedViewController || self.agentStatus == AgentStatusNotAgent) {
        return NO;
    }
    
    CGPoint velocity = [(UIPanGestureRecognizer*)gestureRecognizer velocityInView:gestureRecognizer.view];
    
    return fabs(velocity.x) > fabs(velocity.y) && self.agentStatus != AgentStatusNotAgent;
}

-(void)finishPanGestureCallBack:(UIPanGestureRecognizer *)panGesture{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGPoint panLocation = [panGesture locationInView:self.view];
    if (panLocation.x >= 0 && screenRect.size.width >= panLocation.x) {
        [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
    }
}

- (void)updateFollowerCount:(NSString*)count{
    NSString *followerFormatString = [RealUtility getDecimalFormatByString:count withUnit:nil];
    self.followerLabel.text = followerFormatString;
    
    if ([count isEqualToString:@"0"]) {
        self.followerLabel.textColor = [UIColor lightGrayColor];
    }else{
        if (self.agentStatus != AgentStatusNotAgent) {
            self.followerLabel.textColor = RealNavBlueColor;
        }
    }
}

- (void)updateFollowingCount:(NSString*)count{
    NSString *followingFormatString = [RealUtility getDecimalFormatByString:count withUnit:nil];
    if ([count isEqualToString:@"0"]) {
        self.followingLabel.textColor = [UIColor lightGrayColor];
    }else{
        self.followingLabel.textColor = RealNavBlueColor;
    }
    if (self.agentStatus == AgentStatusNotAgent){
        self.activityLogView.followerCountLabel.text = followingFormatString;
        self.activityLogView.followerUnitLabel.text = JMOLocalizedString(@"common__following",nil);
    }else{
        self.followingLabel.text = followingFormatString;
    }
}

-(void)configureWithAgentStatus:(AgentStatus)status{
    self.agentStatus = status;
    switch (self.agentStatus) {
        case AgentStatusNotAgent:{
            self.agentListingContainer.hidden = YES;
            self.editButton.hidden = NO;
            break;
        }case AgentStatusWithListing:{
            self.agentListingContainer.hidden = NO;
            self.editButton.hidden = YES;
            if (![self.Threepageviewcontroller isKindOfClass:[ApartmentDetailViewController class]] || !self.Threepageviewcontroller) {
                self.Threepageviewcontroller = [[ApartmentDetailViewController alloc]init];
                [self.MMDrawerController setLeftDrawerViewController:self.Threepageviewcontroller];
            }
            [self setupListingTableView];
            break;
        }
        case AgentStatusWithoutListing:{
            self.editButton.hidden = YES;
            self.agentListingContainer.hidden = NO;
            [self setupListingTableView];
            break;
        }
        default:
            break;
    }
    [self initActivityLogView];
    
    NSString *agentName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
    NSURL *profileImageURL = [NSURL URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];
    NSString *followerRawString = [NSString
                                   stringWithFormat:@"%d", [LoginBySocialNetworkModel shared].myLoginInfo.FollowerCount];
    NSString *followingRawString = [NSString
                                    stringWithFormat:@"%d", [LoginBySocialNetworkModel shared].myLoginInfo.FollowingCount];
    [self updateFollowerCount:followerRawString];
    [self updateFollowingCount:followingRawString];
    if (self.activityLogView && !self.activityLogView.hidden && self.activityLogView.logViewType == ActivityLogViewTypeFollowing) {
        [self activityLogGetWithPage:1 showLoading:NO];
    }
    if(self.profileNameLabel){
        self.profileNameLabel.text = agentName;
    }
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.height /2;
    [self.profileImageView loadImageURL:profileImageURL withIndicator:YES];
    [self updateBackgroundImageView];
    self.completeProgressBar.type = YLProgressBarTypeFlat;
    self.completeProgressBar.hideGloss = YES;
    self.completeProgressBar.indicatorTextLabel.font = self.completeLabel.font;
    self.completeProgressBar.progressTintColor = RealBlueColor;
    self.completeProgressBar.trackTintColor = TrackColor;
    self.completeProgressBar.hideStripes = YES;
    self.completeProgressBar.hideGradient = YES;
    self.completeProgressBar.progress = 0;
    self.completeProgressBar.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeTrack;
    
    self.profileCircularProgressBar.animatesBegining = YES;
    self.profileCircularProgressBar.fillColor = RealBlueColor;
    self.profileCircularProgressBar.showTextLabel = NO;
    self.profileCircularProgressBar.unfillColor = TrackColor;
    
    if (status != AgentStatusNotAgent) {
        [self configureAgentListing:[MyAgentProfileModel shared].myAgentProfile.AgentListing];
        [self configureCreateListingButton];
    }else{
        [self.actionButton setTitle:JMOLocalizedString(@"me__be_an_agent", nil) forState:UIControlStateNormal];
    }
    [self updateCompleteProgress];
    [[[AppDelegate getAppDelegate]getTabBarController] customNavBarController].apartmentDetailMoreButton.hidden=YES;
    [self.actionButton  setBackgroundColor:[UIColor realBlueColor]];
    
}

-(void)updateCompleteProgress{
    NSString *agentName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
    NSArray *nameArray = [agentName componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    nameArray = [nameArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
    NSString *nickName =@"";
    if ([RealUtility isValid:nameArray]) {
        nickName = [nameArray firstObject];
    }
    
    self.completeLabel.text = [NSString stringWithFormat:@"%@, %@...",nickName,JMOLocalizedString(@"me__complete_your_profile", nil)];
    CGFloat profileCompletePercentage;
    if ([RealUtility isValid:[MyAgentProfileModel shared].myAgentProfile]) {
        profileCompletePercentage = [[MyAgentProfileModel shared].myAgentProfile getProfileCompletePercentage];
    }else{
        profileCompletePercentage = 0.0;
    }
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people set:@"Profile complete %" to:[NSNumber numberWithFloat:profileCompletePercentage * 100]];
    if (profileCompletePercentage >= 1.0) {
        self.completeProfileContainer.hidden = YES;
        self.profileCircularProgressBar.hidden = YES;
        self.followViewTopSpace.constant=8;
    }else{
        self.completeProfileContainer.hidden = NO;
        self.profileCircularProgressBar.hidden = NO;
        self.followViewTopSpace.constant = 46;
        [self.completeProgressBar setProgress:profileCompletePercentage animated:YES];
        self.profileCircularProgressBar.percentage = profileCompletePercentage;
    }
    
}

- (NSString*)creationDateTimeRelative:(AgentListing*)listing{
    NSString *creationDateString = listing.CreationDateString;
    
    NSDateFormatter *dateFormat = [NSDateFormatter localeDateFormatter];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *currentDate = [NSDate date];
    NSDate *createDate = [dateFormat dateFromString:creationDateString];
    return [RealUtility timeDifferenceStringFromDate:createDate toDate:currentDate];
}

-(void)configureAgentListing:(AgentListing*)listing{
    NSString *name = nil;
    NSString *address = nil;
    NSString *typeImageName = nil;
    NSString *actionButtonString = nil;
    CGFloat padding = 8.0;
    if (is568h) {
        padding =8.0f;
    }else{
        padding =2.0f;
    }
    if ([RealUtility isValid:listing]) {
        GoogleAddress *googleAddress = [listing.GoogleAddress firstObject];
        name = googleAddress.Name;
        address = googleAddress.FormattedAddress;
        actionButtonString = JMOLocalizedString(@"me__btn_update_now", nil);
        typeImageName = [[SystemSettingModel shared]getPropertyImageByPropertyType:listing.PropertyType spaceType:listing.SpaceType state:@"on"];
    }
    
    if (self.agentListingContainer && !self.agentListingContainer.hidden) {
        [self.actionButton setTitle:actionButtonString forState:UIControlStateNormal];
    }
    
}

-(IBAction)actionButtonDidPress:(id)sender{
    if (self.agentStatus == AgentStatusNotAgent) {
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Attempted to Become Agent"];
        
        Editprofilehaveprofile *editVC = [[Editprofilehaveprofile alloc]
                                          initWithNibName:@"Editprofilehaveprofile"
                                          bundle:nil];
        editVC.editDelegate = self;
        editVC.underLayViewController = self;
        editVC.viewType = EditProfileCreateNew;
        BaseNavigationController *navController = [[BaseNavigationController alloc]initWithRootViewController:editVC];
        navController.navigationBar.hidden = YES;
        [self.delegate overlayViewController:navController onViewController:self];
    }else {
        
        if (self.activityLogView) {
            [self showActivityLogView:NO animated:YES];
        }
        
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        
        if ([RealUtility isValid:[MyAgentProfileModel shared].myAgentProfile.AgentListing]) {
            
            [mixpanel track:@"Attempted to Update Posting"];
            
        } else {
            
            [mixpanel track:@"Attempted to Create First Posting"];
        }
        
        Taochum *taochum = [[Taochum alloc]init];
        taochum.needCustomBackground = YES;
        taochum.underLayViewController = self;
        BaseNavigationController *navController = [[BaseNavigationController alloc]initWithRootViewController:taochum];
        navController.type = @"posting";
        navController.navigationBar.hidden = YES;
        [self.delegate overlayViewController:navController onViewController:self];
    }
}

-(IBAction)inviteButtonDidPress:(id)sender{
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Invite to Follow @ Me"];
    
    __weak typeof(self) weakSelf = self;
    [[REPhonebookManager sharedManager] requestAccess:^(BOOL granted) {
        
        if (granted) {
            
            RealPhoneBookShareViewController *vc = [RealPhoneBookShareViewController phoneBookShareVCWithShareType:RealPhoneBookShareTypeBoardcast];
            vc.underLayViewController = weakSelf;
            [weakSelf.delegate overlayViewController:vc onViewController:weakSelf];
            
        } else {
            
            ViralViewController *viralViewController = [[ViralViewController alloc] initWithType:ViralViewControllerTypeBroadcast];
            viralViewController.underLayViewController = weakSelf;
            [weakSelf.delegate overlayViewController:viralViewController onViewController:weakSelf];
        }
    }];
}

-(void)EditProfileDidEndEditing:(BOOL)isNewProfile success:(BOOL)success{
    if (success) {
        if (isNewProfile) {
            self.agentStatus = AgentStatusWithoutListing;
            NSString *meProfileNib = @"MeProfileViewController";
            MeProfileViewController *twopage3 =
            [[MeProfileViewController alloc] initWithNibName:meProfileNib bundle:nil];
            twopage3.agentStatus = self.agentStatus;
            twopage3.MMDrawerController = self.MMDrawerController;
            twopage3.Onepageviewcontroller = self.Onepageviewcontroller;
            //            twopage3.Threepageviewcontroller = threepage3;
            twopage3.needChangeType = YES;
            self.previousPageControlIsHidden = NO;
            [self.MMDrawerController setCenterViewController:twopage3];
            //            [twopage3 viewDidLoad];
            //            [twopage3 viewWillAppear:NO];
        }else{
            [self setupNavBar];
            [self configureWithAgentStatus:self.agentStatus];
        }
    }
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}

- (IBAction)followerButtonDidPress:(UIButton*)sender{
    if (self.agentStatus != AgentStatusNotAgent) {
        sender.selected = !sender.selected;
        self.followerLabel.highlighted = sender.selected;
        self.followingButton.selected = NO;
        self.followingLabel.highlighted = NO;
        [self.activityLogView setupWithType:ActivityLogViewTypeFollow initialArray:self.activityLogItems];
        [self showActivityLogView:sender.selected animated:YES];
        
        if (sender.selected) {
            // Mixpanel
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Viewed Followers Activity"];
        }
    }
}
- (IBAction)followingButtonDidPress:(UIButton*)sender{
    if (self.agentStatus != AgentStatusNotAgent ) {
        sender.selected = !sender.selected;
        self.followingLabel.highlighted = sender.selected;
        self.followerButton.selected = NO;
        self.followerLabel.highlighted = NO;
        [self.activityLogView setupWithType:ActivityLogViewTypeFollowing initialArray:self.followerLogItems];
        [self showActivityLogView:sender.selected animated:YES];
    }
}

- (IBAction)backgroundButtonDidPress:(id)sender{
    if (self.agentStatus == AgentStatusWithListing) {
        ApartmentDetailViewController *vc = (ApartmentDetailViewController*) self.Threepageviewcontroller;
        [MyAgentProfileModel shared].myAgentProfile.status = AgentProfileStatusNormal;
        vc.pendingAgentProfile = [MyAgentProfileModel shared].myAgentProfile;
        vc.pendingAgentProfile.meProfile = @(1);
        [[[AppDelegate getAppDelegate]getTabBarController]setTabBarHidden:YES];
        [self.MMDrawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
            if (finished) {
                [self changePage:MMDrawerSideLeft];
            }
        }];
    }
}

- (void) initActivityLogView{
    if (!self.activityLogView) {
        if (self.followInfoView) {
            CGFloat inset = 0;
            if (self.agentStatus != AgentStatusNotAgent) {
                self.activityLogView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityLogView" owner:self options:nil] objectAtIndex:0];
                inset = kLogViewIsAgentDefaultBottomSpaceConstant;
            }else{
                self.activityLogView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityFollowingLogView" owner:self options:nil] objectAtIndex:0];
                inset = kLogViewIsNotAgentDefaultBottomSpaceConstant;
            }
            self.activityLogView.hidden = YES;
            [self.view insertSubview:self.activityLogView belowSubview:self.followInfoView];
            
            [NSLayoutConstraint autoSetPriority:UILayoutPriorityDefaultHigh forConstraints:^{
                [self.activityLogView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.followInfoView withOffset:2];
            }];
            [self.activityLogView autoPinEdge:ALEdgeLeading toEdge:ALEdgeLeading ofView:self.followInfoView];
            [self.activityLogView autoPinEdge:ALEdgeTrailing toEdge:ALEdgeTrailing ofView:self.followInfoView];
            
            
            [NSLayoutConstraint autoSetPriority:UILayoutPriorityDefaultLow forConstraints:^{
                self.activityLogViewBottomSpaceConstraint = [self.activityLogView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:inset];
                self.activityLogViewBottomSpaceConstraint.active = NO;
            }];
            self.activityLogViewHeightConstraint = [self.activityLogView autoSetDimension:ALDimensionHeight toSize:0 relation:NSLayoutRelationEqual];
            self.activityLogCurrentPage = 1;
            self.activityLogView.delegate = self;
            
            if (self.agentStatus == AgentStatusNotAgent) {
                [self.activityLogView disableCloseGesture];
                self.activityLogView.transparentBackground = YES;
                self.activityLogView.leftTitleButton.hidden = YES;
                self.activityLogView.rightTitleButton.hidden = YES;
                [self.activityLogView setupWithType:ActivityLogViewTypeFollowing initialArray:self.followerLogItems];
                [self showActivityLogView:YES animated:NO];
            }else{
                
            }
        }
    }
}



-(void)showActivityLogView:(BOOL)show animated:(BOOL)animated{
    __weak MeProfileViewController *weakSelf = self;
    
    if (self.activityLogView.hidden == !show) {
        animated = NO;
    }
    
    if (show) {
        self.activityLogView.hidden = !show;
        if (self.agentStatus != AgentStatusNotAgent) {
            self.activityLogViewBottomSpaceConstraint.constant = -kLogViewIsAgentDefaultBottomSpaceConstant;
        }else{
            self.activityLogViewBottomSpaceConstraint.constant = -kLogViewIsNotAgentDefaultBottomSpaceConstant;
        }
        [self activityLogGetWithPage:1 showLoading:NO];
    }else{
        self.followerButton.selected = NO;
        self.followerLabel.highlighted = NO;
        self.followingButton.selected = NO;
        self.followingLabel.highlighted = NO;
    }
    
    void (^block)() = ^{
        self.activityLogViewHeightConstraint.active = !show;
        self.activityLogViewBottomSpaceConstraint.active = show;
        [self.activityLogView layoutIfNeeded];
        if (show) {
            if (self.originalLogViewFrame.size.height <= 0) {
                self.originalLogViewFrame = self.activityLogView.frame;
            }
        }
    };
    
    void (^completion)(BOOL) = ^(BOOL finished){
        if(finished){
            weakSelf.activityLogView.hidden = !show;
        }
    };
    
    if (animated) {
        [UIView animateWithDuration:0.24 animations:block completion:completion];
    } else {
        block();
        completion(YES);
    }
}
-(void)agentImageTapping:(id)sender{
    [kAppDelegate showImagePickerOn:self];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)uploadProfileImage:(UIImage*)image{
    [self showLoadingHUDWithProgress:JMOLocalizedString(@"agent_profile__uploading_data", nil)];
    
    [[MyAgentProfileModel shared]uploadProfileImage:image withBlock:^(id response, NSError *error) {
        if (!error && response) {
            NSString *photoURL = response[@"PhotoURL"];
            if (photoURL) {
                [LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL = photoURL;
                [MyAgentProfileModel shared].myAgentProfile.AgentPhotoURL = photoURL;
                [[[DBLogInfo query] fetch] removeAll];
                [[[[DBMyAgentProfile query] whereWithFormat:@" MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID] fetch] removeAll];
                DBMyAgentProfile *dbmyagentprofile = [DBMyAgentProfile new];
                dbmyagentprofile.Date = [NSDate date];
                dbmyagentprofile.MyAgentProfile = [NSKeyedArchiver
                                                   archivedDataWithRootObject:[[MyAgentProfileModel shared].myAgentProfile copy]];
                dbmyagentprofile.MemberID = [[LoginBySocialNetworkModel shared]
                                             .myLoginInfo.MemberID intValue];
                [dbmyagentprofile commit];
                [[LoginBySocialNetworkModel shared].myLoginInfo saveInDataBase];
                [[LoginBySocialNetworkModel shared].myLoginInfo updateCustomData];
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationMyAgentProfileImageDidUpdate object:nil];
            }
            
        }else if(error && response){
            NSString *errorMessage = response [@"ErrorMsg"];
            [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:errorMessage];
        }else{
            [self showAlertWithTitle:nil message:JMOLocalizedString(@"error_message__server_not_response", nil)];
        }
        [self hideLoadingHUD];
    }];
    
}

- (void)followingCountDidChange{
    NSString *followingRawString = [NSString
                                    stringWithFormat:@"%d", (int)[[LoginBySocialNetworkModel shared].myLoginInfo followingCount]];
    [self updateFollowingCount:followingRawString];
}

- (void)setupListingTableView{
    if (self.listingTableView) {
        self.listingTableView.rowHeight = UITableViewAutomaticDimension;
        self.listingTableView.estimatedRowHeight = 44.0;
        [self.listingTableView registerNib:[UINib nibWithNibName:@"StoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"StoryTableViewCell"];
        self.listingTableView.tableFooterView = [UIView new];
        [self.listingTableView registerNib:[UINib nibWithNibName:@"StoryTableHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"StoryTableHeaderView"];
        [self.listingTableView reloadData];
    }
}

#pragma mark -UIImagePickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    // assets contains PHAsset objects.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    
    // this one is key
    requestOptions.synchronous = true;
    PHAsset *asset = [assets firstObject];
    if (asset) {
        [RealUtility getImageBy:asset size:RealUploadPhotoSize handler:^(UIImage *result, NSDictionary *info) {
            RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:result];
            imageCropVC.delegate = self;
            [picker.navigationController pushViewController:imageCropVC animated:YES];
        }];
    }else{
        
    }
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    return picker.selectedAssets.count<1;
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [controller.navigationController popViewControllerAnimated:YES];
}

// The original image has been cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect{
    [self uploadProfileImage:croppedImage];
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
}

// The original image has been cropped. Additionally provides a rotation angle used to produce image.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
                  rotationAngle:(CGFloat)rotationAngle{
    [self uploadProfileImage:croppedImage];
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
}

// The original image will be cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                  willCropImage:(UIImage *)originalImage{
    // Use when `applyMaskToCroppedImage` set to YES.
    //    [SVProgressHUD show];
}

#pragma mark - RDVTabBarControllerDelegate
- (BOOL)tabBarController:(RDVTabBarController *)tabBarController
shouldSelectViewController:(UIViewController *)viewController {
    if( self.agentStatus == AgentStatusWithListing){
        self.tapCount ++;
        
        BOOL hasTappedTwiceOnOneTab = NO;
        
        if(self.previousHandledViewController == viewController) {
            hasTappedTwiceOnOneTab = YES;
        }
        
        self.previousHandledViewController = viewController;
        
        if (viewController == self.navigationController && tabBarController.selectedIndex == 3) {
            DDLogInfo(@"viewController==self%@", viewController);
            if(self.tapCount == 2 && hasTappedTwiceOnOneTab) {
                [self followerButtonDidPress:self.followerButton];
                self.previousHandledViewController=nil;
                self.tapCount = 0;
                return NO;
            } else if(self.tapCount == 1) {
                [self.activityLogView.tableView setContentOffset:CGPointZero animated:YES];
                dispatch_time_t popTime =
                dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    self.tapCount = 0;
                    self.previousHandledViewController = nil;
                });
                return NO; // or YES when you want the default engine process the event
            }
        }else{
            self.tapCount = 0;
            self.previousHandledViewController = nil;
        }
        return YES;
    }
    return YES;
}

#pragma mark - UITableViewDelegate And DataSource
#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView{
    NSString *text = JMOLocalizedString(@"me__no_posting", nil);
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:36.0f],
                                 NSForegroundColorAttributeName: [UIColor lightTextColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor clearColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return NO;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return [MyAgentProfileModel shared].myAgentProfile.AgentListingHistory.count;
    }else{
        return [[MyAgentProfileModel shared].myAgentProfile.AgentListing valid];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger section = 1;
    if ([[MyAgentProfileModel shared].myAgentProfile.AgentListingHistory valid]) {
        section = 2;
    }
    return section;
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([self tableView:tableView numberOfRowsInSection:section] > 0) {
        return 32;
    }
    return 0;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    StoryTableHeaderView* headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"StoryTableHeaderView"];
    NSString *title = @"";
    NSString *time = @"";
    if (section == 0) {
        title = JMOLocalizedString(@"me__current_story", nil);
        time = [self creationDateTimeRelative:[MyAgentProfileModel shared].myAgentProfile.AgentListing];
        headerView.titleLabel.alpha = 1.0;
    }else{
        if ([self tableView:tableView numberOfRowsInSection:section] > 1) {
            title = JMOLocalizedString(@"me__previous_storys", nil);
        }else{
            title = JMOLocalizedString(@"me__previous_story", nil);
        }
        headerView.titleLabel.alpha = 0.5;
    }
    [headerView configureWithTitle:title time:time];
    return headerView;
}

- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StoryTableViewCell"];
    AgentListing *listing = nil;
    UIImage *separatorImage = nil;
    if (indexPath.section == 0) {
        listing = [MyAgentProfileModel shared].myAgentProfile.AgentListing;
        if ([self numberOfSectionsInTableView:tableView] > 1) {
            separatorImage = [UIImage imageWithColor:[UIColor lightTextColor]];
        }else{
            separatorImage = [UIImage imageWithColor:[UIColor clearColor]];
        }
        
        cell.contentView.alpha = 1.0;
    }else{
        if (indexPath.row < [MyAgentProfileModel shared].myAgentProfile.AgentListingHistory.count) {
            listing = [MyAgentProfileModel shared].myAgentProfile.AgentListingHistory[indexPath.row];
        }
        separatorImage = [UIImage imageNamed:@"pop_dottedline"];
        cell.contentView.alpha = 0.5;
    }
    [cell configureCell:listing separatorImage:separatorImage];
    if (!cell) {
        cell = [UITableViewCell new];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        [self backgroundButtonDidPress:nil];
    }
}


#pragma mark - ActivityLogViewDelegate
-(void)didReceiveNewFollow{
    if([MyActivityLogModel shared].newLogCount >0){
        self.activityLogCurrentPage = 1;
        self.followNotificationDotView.hidden = NO;
        self.followerLabel.highlighted = YES;
    }else{
        self.followNotificationDotView.hidden = YES;
        self.followerLabel.highlighted = NO;
    }
    
    [[RealApiClient sharedClient]followInfoGetWithBlock:^(NSDictionary *response, NSError *error) {
        NSString *followerCountString = [response[@"FollowerCount"] stringValue];
        [[LoginBySocialNetworkModel shared].myLoginInfo updateFollowerCount:[followerCountString intValue]];
        [self updateFollowerCount:followerCountString];
    }];
}
- (void) activityLogView:(ActivityLogView *)logView shouldClose:(BOOL)close{
    self.followerButton.selected = NO;
    self.followingButton.selected = NO;
    [self showActivityLogView:NO animated:YES];
}

- (void)activityLogView:(ActivityLogView *)logView didPanDistance:(CGPoint)panDistance{
    CGFloat newBottomSpaceConstant = self.activityLogViewBottomSpaceConstraint.constant += panDistance.y;
    
    if (self.agentStatus != AgentStatusNotAgent) {
        if (newBottomSpaceConstant < -self.originalLogViewFrame.size.height - kLogViewIsAgentDefaultBottomSpaceConstant + kLogViewMaxPanningDistance) {
            newBottomSpaceConstant = -self.originalLogViewFrame.size.height - kLogViewIsAgentDefaultBottomSpaceConstant + kLogViewMaxPanningDistance;
        }else if (newBottomSpaceConstant >= -kLogViewIsAgentDefaultBottomSpaceConstant + kLogViewMaxPanningDistance){
            newBottomSpaceConstant =-kLogViewIsAgentDefaultBottomSpaceConstant + kLogViewMaxPanningDistance;
        }
    }else{
        if (newBottomSpaceConstant < -self.originalLogViewFrame.size.height - kLogViewIsNotAgentDefaultBottomSpaceConstant  + kLogViewMaxPanningDistance) {
            newBottomSpaceConstant = -self.originalLogViewFrame.size.height - kLogViewIsNotAgentDefaultBottomSpaceConstant + kLogViewMaxPanningDistance;
        }else if (newBottomSpaceConstant >= -kLogViewIsNotAgentDefaultBottomSpaceConstant +kLogViewMaxPanningDistance){
            newBottomSpaceConstant = -kLogViewIsNotAgentDefaultBottomSpaceConstant +kLogViewMaxPanningDistance;
        }
    }
    self.activityLogViewBottomSpaceConstraint.constant = newBottomSpaceConstant;
}

- (void) activityLogView:(ActivityLogView *)logView didDidEndPanning:(CGFloat) velocity{
    [self triggerBounceEffect:velocity];
}

- (void)activityLogViewDidScrollToBottom:(ActivityLogView *)logView{
    
    if (logView.haveMoreItem) {
        if(![Reachability reachabilityForInternetConnection]){
            [self cachedActivityLogWithPage:self.activityLogCurrentPage];
        }else{
            [self activityLogGetWithPage:self.activityLogCurrentPage showLoading:NO];
        }
    }
    
}

- (void)activityLogViewDidTriggerPullToRefresh:(ActivityLogView *)logView{
    [self activityLogGetWithPage:1 showLoading:YES];
}

- (void)triggerBounceEffect:(CGFloat)verticalVelocity{
    
    int maxConstant = -80;
    if (self.agentStatus == AgentStatusWithListing) {
        maxConstant = -80;
    }else if (self.agentStatus == AgentStatusWithoutListing){
        maxConstant = -20;
    }
    
    if (fabs(verticalVelocity) > 400) {
        [self showActivityLogView:verticalVelocity > 0 animated:YES];
    }else if (self.activityLogViewBottomSpaceConstraint.constant != maxConstant) {
        if(fabs(self.activityLogViewBottomSpaceConstraint.constant) - maxConstant > self.originalLogViewFrame.size.height/3){
            [self showActivityLogView:NO animated:YES];
        }else{
            [self showActivityLogView:YES animated:YES];
        }
    }
}

@end
