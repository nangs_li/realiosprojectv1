//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "SLGlowingTextField.h"

@interface Specialty : BaseViewController
@property(strong, nonatomic) IBOutlet UIScrollView *Specialtyscrollview;
@property(strong, nonatomic) IBOutlet SLGlowingTextField *yourspecialty;
@property(strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UILabel *specialtyTitle;
@property (strong, nonatomic) IBOutlet SLGlowingTextField *shareYourSpecialty;

- (IBAction)closeButton:(id)sender;
@end
