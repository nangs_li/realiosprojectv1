//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Specialty.h"
#import "RealUtility.h"
#import "JSTokenField.h"
@interface Specialty ()<JSTokenFieldDelegate, UITextFieldDelegate>
@property(nonatomic, strong) NSMutableArray *specialtyFieldArray;
@property(nonatomic, strong) JSTokenField *specialtyField;
@property(nonatomic) CGFloat tokenFieldheight;

@end
@implementation Specialty

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.yourspecialty.delegate = self;
    ////JSTokenField init
    self.specialtyField =
    [[JSTokenField alloc] initWithFrame:CGRectMake(0, self.yourspecialty.frame.origin.y +self.yourspecialty.frame.size.height, self.yourspecialty.frame.size.width, 31)];
    [self.specialtyField setDelegate:self];
    self.specialtyFieldArray = [[NSMutableArray alloc] init];
    self.specialtyField.textField.userInteractionEnabled = NO;
    [self.Specialtyscrollview addSubview:self.specialtyField];
    [self.specialtyField moveViewTo:self.yourspecialty direction:RelativeDirectionBottom padding:8];
}

- (void)viewDidLoad {
    //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
    [super viewDidLoad];
    UIColor *color = [UIColor lightGrayColor];
    self.yourspecialty.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"Enter your Specialty"
     attributes:@{NSForegroundColorAttributeName:color}];
    [self.saveButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self checkSave];
    self.yourspecialty.glowingColor = RealBlueColor;
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
}
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    //    self.meLabel.text = JMOLocalizedString(@"MePageAgentProfile.Me", nil);
    self.specialtyTitle.text =
    JMOLocalizedString(@"common__specialty", nil);
     UIColor *color = [UIColor lightGrayColor];
    self.yourspecialty.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString: JMOLocalizedString(@"specialty__share_your_specialty", nil)
     attributes:@{NSForegroundColorAttributeName:color}];
 
     [self.saveButton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];
}
#pragma mark - Button----------------------------------------------------------------------------------------------------
- (IBAction)closeButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)checkSave{
    self.saveButton.enabled = [RealUtility isValid:self.specialtyFieldArray];
    return [RealUtility isValid:self.specialtyFieldArray];
}

- (IBAction)savebutton:(id)sender {
    if ([self checkSave ]) {
        [self loadingAndStopLoadingAfterSecond:5];
        if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
            [[MyAgentProfileModel shared]
             callAgentProfileSpecialtyAddAPIBySpecialtyFieldArray:
             self.specialtyFieldArray
             success:^(AgentProfile *myAgentProfile) {
                 [self closeButton:nil];
                    [self hideLoadingHUD];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error,
                       NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                       NSString *ok) {
                 [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];        }];
            
        } else {
            for (NSDictionary *specialty in self.specialtyFieldArray) {
                NSString *string = [[specialty allValues] objectAtIndex:0];
                DDLogInfo(@"string-->%@", string);
                AgentSpecialty *specialty = [[AgentSpecialty alloc]init];
                specialty.specialty = string;
                [[AgentProfileRegistrationModel shared].SpecialtyArray addObject:specialty];
            }
            [self closeButton:nil]; 
        }
    }
}


#pragma mark JSTokenFieldDelegate ------------------------------------------------------------------------------------------------
- (void)tokenField:(JSTokenField *)tokenField
       didAddToken:(NSString *)title
 representedObject:(id)obj {
    NSDictionary *recipient =
    [NSDictionary dictionaryWithObject:title forKey:obj];
    [self.specialtyFieldArray addObject:recipient];
    
    DDLogInfo(@"self.specialtyFieldArray-->%@", self.specialtyFieldArray);
}

- (void)tokenField:(JSTokenField *)tokenField
didRemoveTokenAtIndex:(NSUInteger)index {
    DDLogInfo(@"didRemoveTokenAtIndex");
    [self.specialtyFieldArray removeObjectAtIndex:index];
    // DDLogDebug(@"Deleted token %tu\n%@", index, self.toRecipients);
}
- (void)tokenField:(JSTokenField *)tokenField
    didRemoveToken:(NSString *)title
 representedObject:(id)obj {
  DDLogInfo(@"didRemoveToken");
  DDLogInfo(@"titletitletitle-->%@", title);
  [tokenField removeTokenWithRepresentedObject:title];
  NSDictionary *removeDict = [self removeObjectFromSpecialtyFieldArrayRepresentedObject:obj];
  if (![self isEmpty:removeDict]) {
    [self.specialtyFieldArray removeObject:removeDict];
  }
    
}
-(NSDictionary *)removeObjectFromSpecialtyFieldArrayRepresentedObject:(id)obj {
  for (NSDictionary *specialty in self.specialtyFieldArray) {
    if (![self isEmpty:[specialty objectForKey:obj]]) {
      return specialty;
    }
  }
  return nil;
}
- (BOOL)tokenFieldShouldReturn:(JSTokenField *)tokenField {
    NSMutableString *recipient = [NSMutableString string];
    
    NSMutableCharacterSet *charSet =
    [[NSCharacterSet whitespaceCharacterSet] mutableCopy];
    [charSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
    
    NSString *rawStr = [[tokenField textField] text];
    for (int i = 0; i < [rawStr length]; i++) {
        if (![charSet characterIsMember:[rawStr characterAtIndex:i]]) {
            [recipient
             appendFormat:@"%@",
             [NSString stringWithFormat:@"%c",
              [rawStr characterAtIndex:i]]];
        }
    }
    
    if ([rawStr length]) {
        [tokenField addTokenWithTitle:rawStr representedObject:recipient withDeleteButton:YES];
    }
    
    [[tokenField textField] setText:@""];
    
    return NO;
}


#pragma mark textfield delegate-----------------------------------------------------------------------------------------------------------------------
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldShouldEndEditing");
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    NSMutableString *recipient = [NSMutableString string];
    
    NSMutableCharacterSet *charSet =
    [[NSCharacterSet whitespaceCharacterSet] mutableCopy];
    [charSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
    
    NSString *rawStr = textField.text;
    for (int i = 0; i < [rawStr length]; i++) {
        if (![charSet characterIsMember:[rawStr characterAtIndex:i]]) {
            [recipient
             appendFormat:@"%@",
             [NSString stringWithFormat:@"%c",
              [rawStr characterAtIndex:i]]];
        }
    }
    
    if ([rawStr length]) {
        [self.specialtyField addTokenWithTitle:rawStr representedObject:recipient withDeleteButton:YES];
    }
    
    [textField setText:@""];
    [self checkSave];
    [textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSMutableString *recipient = [NSMutableString string];
    
    NSMutableCharacterSet *charSet =
    [[NSCharacterSet whitespaceCharacterSet] mutableCopy];
    [charSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
    
    NSString *rawStr = textField.text;
    for (int i = 0; i < [rawStr length]; i++) {
        if (![charSet characterIsMember:[rawStr characterAtIndex:i]]) {
            [recipient
             appendFormat:@"%@",
             [NSString stringWithFormat:@"%c",
              [rawStr characterAtIndex:i]]];
        }
    }
    
    if ([rawStr length] && [self canAddMoreSpeicalty]) {
        [self.specialtyField addTokenWithTitle:rawStr representedObject:recipient withDeleteButton:YES];
    }else if([rawStr length]){
        [self showAlertWithTitle:nil message:[NSString stringWithFormat:JMOLocalizedString(@"alert_alert_add_specialties", nil),(int)kMaxAgentSpecialtyCount]];
    }
    
    [textField setText:@""];
    
    [textField resignFirstResponder];
    DDLogInfo(@"textFieldShouldReturn");
    
    return YES;
}

- (BOOL) canAddMoreSpeicalty{
    NSInteger totalCount = 0;
    NSInteger currentCount = 0;
    NSInteger newCount =  self.specialtyFieldArray.count;
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
        currentCount = [MyAgentProfileModel shared].myAgentProfile.AgentSpecialty.count;
    } else {
        currentCount = [AgentProfileRegistrationModel shared].SpecialtyArray.count;
    }
    totalCount = currentCount + newCount;
    return kMaxAgentSpecialtyCount > totalCount;
}

@end