//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Spokenlanguages.h"
#import "SpokenLanguagescell.h"
#import "SpokenLanguageList.h"

@interface Spokenlanguages ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation Spokenlanguages

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];


  self.localLangIndexarray = [[NSMutableArray alloc] init];

  for (NSDictionary *dict in [AgentProfileRegistrationModel shared].LanguagesArray) {
    [self.localLangIndexarray addObject:[dict objectForKey:@"LangIndex"]];
  }

  DDLogInfo(@"self.localiLangIndexarray-->%@", self.localLangIndexarray);
  [self.tableview reloadData];
  [self checkEmptyToChangeSaveButtonColor];
}

- (void)viewDidLoad {
  [super viewDidLoad];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];

  self.tableview.delegate = self;
  self.tableview.dataSource = self;

  [self.tableview reloadData];

}


-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.spokenLanguageTitle.text=JMOLocalizedString(@"common__spoken_languages", nil);
    [self.savebutton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];
}



#pragma mark - Button
- (IBAction)closebutton:(id)sender {
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromBottom;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)savebutton:(id)sender {
  [self loadingAndStopLoadingAfterSecond:5];

  [self languagesArrayInitAndAddObject];

  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromBottom;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController popViewControllerAnimated:NO];
}
- (void)checkEmptyToChangeSaveButtonColor {
    if ([self.localLangIndexarray count] > 0) {
        self.savebutton.enabled = YES;
    } else {
        self.savebutton.enabled = NO;
    }
}
- (void)languagesArrayInitAndAddObject {
    [AgentProfileRegistrationModel shared].LanguagesArray = [[NSMutableArray alloc] init];
    for (NSString *LangIndex in self.localLangIndexarray) {
        for (SpokenLanguageList *spokenlanguage in [SystemSettingModel shared]
             .spokenlanguagelist) {
            if ([LangIndex
                 isEqualToString:[NSString stringWithFormat:@"%d", spokenlanguage
                                  .Index]]) {
                     NSDictionary *dict = @{
                                            @"Language" : spokenlanguage.NativeName,
                                            @"LangIndex" : [NSString stringWithFormat:@"%d", spokenlanguage.Index]
                                            };
                     [[AgentProfileRegistrationModel shared].LanguagesArray addObject:dict];
                 }
        }
    }
    DDLogInfo(@" [AppDelegate getAppDelegate].LanguagesArray%@",
              [AgentProfileRegistrationModel shared].LanguagesArray);
}




#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
  return [[SystemSettingModel shared].spokenlanguagelist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  static NSString *simpleTableIdentifier = @"UITableViewCell";

  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:simpleTableIdentifier];
  }

  SpokenLanguageList *spokenlanguage =
      [[SystemSettingModel shared].spokenlanguagelist objectAtIndex:indexPath.row];
  cell.textLabel.text = spokenlanguage.NativeName;
  cell.textLabel.textColor = self.delegate.Greycolor;
  cell.textLabel.font = [self.delegate Futurafont:14];

  for (NSString *LangIndex in self.localLangIndexarray) {
    if ([LangIndex
            isEqualToString:[NSString stringWithFormat:@"%d",
                                                       spokenlanguage.Index]]) {
      UIImageView *tickimage =
          [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
      [tickimage setImage:[UIImage imageNamed:@"tableviewcelltick"]];
      cell.accessoryView = tickimage;
      return cell;
    }
  }
  cell.accessoryView = nil;

  return cell;
}
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

  SpokenLanguageList *spokenlanguage =
      [[SystemSettingModel shared].spokenlanguagelist objectAtIndex:indexPath.row];

  if (cell.accessoryView == nil) {
    [self.localLangIndexarray
        addObject:[NSString stringWithFormat:@"%d", spokenlanguage.Index]];

  } else {
    for (int i = 0; i < self.localLangIndexarray.count; i++) {
      NSString *LangIndex = [self.localLangIndexarray objectAtIndex:i];
      if ([LangIndex
              isEqualToString:[NSString stringWithFormat:@"%d", spokenlanguage
                                                                    .Index]]) {
        [self.localLangIndexarray removeObject:LangIndex];
      }
    }
  }
  [tableView reloadData];
  [self checkEmptyToChangeSaveButtonColor];
}



@end