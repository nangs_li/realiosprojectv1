//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Spokenlanguageshaveprofile.h"
#import "SpokenLanguagescell.h"
#import "RealUtility.h"

@interface Spokenlanguageshaveprofile ()<UITableViewDataSource,
UITableViewDelegate>

@end
@implementation Spokenlanguageshaveprofile

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.localLangIndexarray = [[NSMutableArray alloc] init];
    NSArray *currentLang = nil;
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        currentLang = [MyAgentProfileModel shared].myAgentProfile.AgentLanguage;
        for (AgentLanguage *agentlanguage in currentLang) {
            DDLogInfo(@"agentlanguage-->%@", agentlanguage);
            [self.localLangIndexarray
             addObject:[NSString stringWithFormat:@"%d", agentlanguage.LangIndex]];
        }
    }else{
        currentLang = [AgentProfileRegistrationModel shared].LanguagesArray;
        for (AgentLanguage *agentlanguage in currentLang) {
            DDLogInfo(@"agentlanguage-->%@", agentlanguage);
            [self.localLangIndexarray
             addObject:[NSString stringWithFormat:@"%d", agentlanguage.LangIndex]];
        }
    }
    
    DDLogInfo(@"self.localiLangIndexarray-->%@", self.localLangIndexarray);
    [self.tableview reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.savebutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self checkEmptyToChangeSaveButtonColor];
    self.tableview.allowsMultipleSelection = YES;
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.spokenLanguageTitle.text=JMOLocalizedString(@"common__spoken_languages", nil);
    [self.savebutton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];
}



#pragma mark  - Button
- (IBAction)closebutton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)finishEditing:(BOOL)didEdit{
    if (didEdit && [self.baseViewDelegate respondsToSelector:@selector(BaseViewControllerDidAction:withRef:)]) {
        [self.baseViewDelegate BaseViewControllerDidAction:BaseViewActionEdit withRef:@"language"];
    }
    [self closebutton:nil];
}

- (IBAction)savebutton:(id)sender {
    [self loadingAndStopLoadingAfterSecond:5];
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
        [[MyAgentProfileModel shared]
         callAgentProfileLanguageAddAPIByLocalLangIndexarray:self.localLangIndexarray
         
         success:^(AgentProfile *myAgentProfile) {
             
             [self finishEditing:YES];
             [self hideLoadingHUD];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                   NSString *ok) {
             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
             [self hideLoadingHUD];
         }];
    }else{
        [AgentProfileRegistrationModel shared].LanguagesArray = [[NSMutableArray alloc] init];
        for (NSString *LangIndex in self.localLangIndexarray) {
            for (SpokenLanguageList *spokenlanguage in [SystemSettingModel shared]
                 .spokenlanguagelist) {
                if ([LangIndex
                     isEqualToString:[NSString stringWithFormat:@"%d", spokenlanguage
                                      .Index]]) {
                         AgentLanguage *lang =[[AgentLanguage alloc]init];
                         lang.Language = spokenlanguage.NativeName;
                         lang.LangIndex = spokenlanguage.Index;
                         [[AgentProfileRegistrationModel shared].LanguagesArray addObject:lang];
                     }
            }
        }
        [self finishEditing:YES];
    }
    
    
}
- (void)checkEmptyToChangeSaveButtonColor {
    if ([self.localLangIndexarray count] > 0) {
        self.savebutton.enabled = YES;
        return;
    } else {
        self.savebutton.enabled = NO;
    }
}





#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return [[SystemSettingModel shared].spokenlanguagelist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SpokenLanguagescanselectcell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *langLabel;
    UILabel *indicatorLabel;
    // weak   is not show then disalloc
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:simpleTableIdentifier];
        langLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)];
        langLabel.tag = 1000;
        langLabel.textColor = [UIColor whiteColor];
        langLabel.font = [UIFont systemFontOfSize:14.0f];
        langLabel.backgroundColor = [UIColor clearColor];
        
        indicatorLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 15, 15)];
        indicatorLabel.textAlignment = NSTextAlignmentCenter;
        indicatorLabel.textColor = [UIColor whiteColor];
        indicatorLabel.font = [UIFont systemFontOfSize:12.0f];
        indicatorLabel.clipsToBounds = YES;
        indicatorLabel.tag = 1001;
        indicatorLabel.backgroundColor = [UIColor realBlueColor];
        indicatorLabel.hidden = YES;
        
        [cell.contentView addSubview:langLabel];
        [cell.contentView addSubview:indicatorLabel];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundView = nil;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    langLabel = (UILabel*) [cell.contentView viewWithTag:1000];
    indicatorLabel = (UILabel*) [cell.contentView viewWithTag:1001];
    
    [langLabel setViewAlignmentInSuperView:ViewAlignmentLeft padding:24];
    [langLabel setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
    SpokenLanguageList *spokenlanguage =
    [[SystemSettingModel shared].spokenlanguagelist objectAtIndex:indexPath.row];
    langLabel.text = spokenlanguage.NativeName;
    
    
    if ([RealUtility isValid:self.localLangIndexarray]) {
        for (NSString *LangIndex in self.localLangIndexarray) {
            if ([LangIndex isEqualToString:[NSString stringWithFormat:@"%d",spokenlanguage.Index]]) {
                indicatorLabel.hidden = NO;
//                indicatorLabel.text = [NSString stringWithFormat:@"%d",(int)[self.localLangIndexarray indexOfObject:LangIndex]];
                break;
            }else{
                indicatorLabel.hidden = YES;
            }
        }
    }else{
        indicatorLabel.hidden = YES;
    }
    
    [indicatorLabel setViewAlignmentInSuperView:ViewAlignmentRight padding:16];
    [indicatorLabel setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell) {
        UILabel *langLabel;
        UILabel *indicatorLabel;
        langLabel = (UILabel*) [cell.contentView viewWithTag:1000];
        indicatorLabel = (UILabel*) [cell.contentView viewWithTag:1001];
        if (langLabel) {
            [langLabel setViewAlignmentInSuperView:ViewAlignmentLeft padding:24];
            [langLabel setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
        }
        if (indicatorLabel) {
            [indicatorLabel setViewAlignmentInSuperView:ViewAlignmentRight padding:16];
            [indicatorLabel setViewAlignmentInSuperView:ViewAlignmentVerticalCenter padding:0];
        }
    }
    
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *markerImageView = (UIImageView*) [cell.contentView viewWithTag:1001];
    SpokenLanguageList *spokenlanguage =
    [[SystemSettingModel shared].spokenlanguagelist objectAtIndex:indexPath.row];
    
    if (markerImageView.hidden) {
        [self.localLangIndexarray
         addObject:[NSString stringWithFormat:@"%d", spokenlanguage.Index]];
        
    } else {
        for (int i = 0; i < self.localLangIndexarray.count; i++) {
            NSString *LangIndex = [self.localLangIndexarray objectAtIndex:i];
            if ([LangIndex
                 isEqualToString:[NSString stringWithFormat:@"%d", spokenlanguage
                                  .Index]]) {
                     [self.localLangIndexarray removeObject:LangIndex];
                 }
        }
    }
    [tableView reloadData];
    [self checkEmptyToChangeSaveButtonColor];
}





@end