//
//  StoryTableHeaderView.h
//  productionreal2
//
//  Created by Alex Hung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryTableHeaderView : UITableViewHeaderFooterView
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UILabel *timeLabel;

- (void)configureWithTitle:(NSString*)title time:(NSString*)time;
@end
