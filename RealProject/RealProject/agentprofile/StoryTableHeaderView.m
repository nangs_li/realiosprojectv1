//
//  StoryTableHeaderView.m
//  productionreal2
//
//  Created by Alex Hung on 7/4/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "StoryTableHeaderView.h"

@implementation StoryTableHeaderView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.timeLabel.textColor = [UIColor realBlueColor];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)configureWithTitle:(NSString*)title time:(NSString*)time{
    self.titleLabel.text = title;
    self.timeLabel.text = time;
}

@end
