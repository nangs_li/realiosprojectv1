//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "TDDatePickerController.h"
#import "TDTwoPicker.h"
#import "FilterTypeCell.h"
#import "SLGlowingTextField.h"

@interface Top5
    : BaseViewController<TDDatePickerDelegate, UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet TDTwoPicker *datePicker;
@property(strong, nonatomic) NSDate *selectedDate;
@property(strong, nonatomic) NSDictionary *typeDict;
@property(strong, nonatomic) IBOutlet UIButton *yearmonthdate;
@property(strong, nonatomic) IBOutlet SLGlowingTextField *Addressone;
@property(strong, nonatomic) IBOutlet SLGlowingTextField *price;
@property (strong, nonatomic) FilterTypeCell *filterTypeCell;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(strong, nonatomic) IBOutlet UIButton *savebutton;
@property(nonatomic, strong) NSDictionary *PastClosing;
@property (nonatomic,strong) IBOutlet UILabel *topBarLabel;
@property (nonatomic,strong) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *address;
@property (strong, nonatomic) IBOutlet UILabel *soldPrice;
@property (strong, nonatomic) IBOutlet UILabel *closingDate;
#pragma mark Button Method
- (IBAction)closebutton:(id)sender;
- (IBAction)dateButtonClicked:(id)sender;
@end
