//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Top5.h"
#import "RealUtility.h"
#import "TDSemiModal.h"
@interface Top5 () <FilterTypeCellDelegate>
@end
@implementation Top5

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.filterTypeCell initialAnimation:0.3];
    
}

- (void) filterTypeDidChange:(NSArray*)selectedArray{
    self.typeDict = [selectedArray firstObject];
        [self checkEmptyToChangeSaveButtonColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
    [self checkEmptyToChangeSaveButtonColor];
    self.topBarLabel.text = JMOLocalizedString(@"top_5_title", nil);
    [self.savebutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    self.Addressone.glowingColor = RealBlueColor;
    self.price.glowingColor = RealBlueColor;
    
    if (!self.filterTypeCell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FilterTypeCell" owner:self options:nil];
        self.filterTypeCell = (FilterTypeCell *)[nib objectAtIndex:0];
        self.filterTypeCell.frame = CGRectMake(self.filterTypeCell.frame.origin.x, self.filterTypeCell.frame.origin.y, [RealUtility screenBounds].size.width, self.filterTypeCell.frame.size.height);
        self.filterTypeCell.contentView.frame = CGRectMake(self.filterTypeCell.frame.origin.x, self.filterTypeCell.frame.origin.y, [RealUtility screenBounds].size.width, self.filterTypeCell.frame.size.height);
        NSArray *selectedArray = [SystemSettingModel shared].filterDict[@"space"];
        if (!selectedArray) {
            selectedArray = @[@0];
        }
        NSArray *typeArray = [[SystemSettingModel shared] getSpaceFilterArray];
        if (!typeArray) {
            typeArray = @[];
        }
        
        NSDictionary *firstType = typeArray.firstObject;
        if ([firstType valid]) {
            self.typeDict = firstType;
            selectedArray= @[@(0)];
        }
        
        NSDictionary *filterDict = @{
                                     @"typeArray" : typeArray,
                                     @"selectedArray" :selectedArray
                                     };
        [self.filterTypeCell configureCell:filterDict];
        self.filterTypeCell.delegate = self;
        self.filterTypeCell.isSingleChoice = YES;
        [self.scrollView addSubview:self.filterTypeCell.contentView];
        [self.infoView moveViewTo:self.filterTypeCell.contentView direction:RelativeDirectionBottom padding:8];
        [self.filterTypeCell scrollToLastObject];
    }
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    self.Addressone.delegate = self;
    self.price.delegate = self;
    self.datePicker.delegate = self;

    UIView *paddingView = [[UIView alloc]
                           initWithFrame:CGRectMake(0, 0, 10, self.Addressone.frame.size.height)];
    self.Addressone.leftView = paddingView;
    self.Addressone.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView2 = [[UIView alloc]
                            initWithFrame:CGRectMake(0, 0, 10, self.price.frame.size.height)];
    self.price.leftView = paddingView2;
    self.price.leftViewMode = UITextFieldViewModeAlways;
}



-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.topBarLabel.text=JMOLocalizedString(@"common__agentprofile_title_Personal_best_closings", nil);
    self.address.text=JMOLocalizedString(@"common__address", nil);
    self.soldPrice.text=JMOLocalizedString(@"top_5__sold_price", nil);

    self.closingDate.text=JMOLocalizedString(@"top_5__closing_date", nil);

    [self.savebutton setTitle:JMOLocalizedString(@"common__save", nil) forState:UIControlStateNormal];

    [self.yearmonthdate setTitle:JMOLocalizedString(@"add_experience__MM_YY", nil) forState:UIControlStateNormal];
    [self.datePicker.cancelBtn setTitle:JMOLocalizedString(@"common__cancel", nil)];
    [self.datePicker.saveBtn setTitle:JMOLocalizedString(@"error_message__done", nil)];
}








-(void)finishEditing:(BOOL)didEdit{
    if (didEdit && [self.baseViewDelegate respondsToSelector:@selector(BaseViewControllerDidAction:withRef:)]) {
        [self.baseViewDelegate BaseViewControllerDidAction:BaseViewActionEdit withRef:@"language"];
    }
    [self closebutton:nil];
}

#pragma mark - Button-------------------------------------------------------------------------------------------------------------
- (IBAction)closebutton:(id)sender {
    [self btnBackPressed:nil];
}

- (IBAction)savebutton:(id)sender {
    NSDateFormatter *formatter= [NSDateFormatter enUsDateFormatter];
    if ([self checkEmptyToChangeSaveButtonColor]) {
        [self loadingAndStopLoadingAfterSecond:5];
        NSNumber *propertyType = self.typeDict[@"propertyType"];
        NSNumber *spaceType = self.typeDict[@"spaceType"];
          [formatter setDateFormat:@"yyyy-MM-dd"];
        if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
            [[MyAgentProfileModel shared]
             callAgentProfilePastClosingAddAPIByPropertyTypeNumber:
             [NSString stringWithFormat:@"%ld", (long)[propertyType longValue]]
             spaceTypeNumber:[NSString
                              stringWithFormat:@"%ld", (long)[spaceType longValue]]
             soldPrice:self.price.text
             soldDate:[formatter stringFromDate:_selectedDate]
             address:self.Addressone.text
             success:^(AgentProfile *myAgentProfile) {
                 [self finishEditing:YES];
                    [self hideLoadingHUD];
                 
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error,
                       NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert,
                       NSString *ok) {
                 [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
                 
             }];
            
        } else {
            [formatter setDateFormat:@"yyyy-MM-dd"];
            _PastClosing = @{
                             @"PropertyType" :
                                 [NSString stringWithFormat:@"%ld", (long)[propertyType longValue]],
                             @"SpaceType" : [NSString stringWithFormat:@"%ld", (long)[spaceType longValue]],
                             @"SoldPrice" : self.price.text,
                             @"SoldDate" : [formatter stringFromDate:_selectedDate],
                             @"Address" : self.Addressone.text,
                             @"State" : @"",
                             @"Country" : @""
                             };
            AgentPastClosing *pastClosing =[[AgentPastClosing alloc]init];
            pastClosing.PropertyType = [propertyType intValue];
            pastClosing.SpaceType = [spaceType intValue];
            pastClosing.SoldPrice = self.price.text;
            pastClosing.SoldDate = [formatter stringFromDate:_selectedDate];
            pastClosing.SoldDateString = [formatter stringFromDate:_selectedDate];
            pastClosing.Address = self.Addressone.text;
            pastClosing.State = @"";
            pastClosing.Country =@"";
            [[AgentProfileRegistrationModel shared].Top5Array addObject:pastClosing];
            [self finishEditing:YES];
        }
    }
}

- (IBAction)dateButtonClicked:(id)sender{
    [self.view endEditing:YES];
    [self presentSemiModalViewController:_datePicker];
}
- (BOOL)checkEmptyToChangeSaveButtonColor {
    BOOL valid = [RealUtility isValid:self.Addressone.text] && [RealUtility isValid:self.price] && ![self.yearmonthdate.titleLabel.text isEqualToString:JMOLocalizedString(@"add_experience__MM_YY", nil)] &&[RealUtility isValid:self.typeDict] && [[NSDate date] compare:self.selectedDate ] != NSOrderedAscending;
    self.savebutton.enabled = valid;
    return valid;
}

#pragma mark  -TDTwoPicker(StartDate And EndDate) Delegate-------------------------------------------------------------------------------------------------

- (void)tdDatePicker:(UIPickerView *)picker
        didSelectRow:(NSInteger)row
         inComponent:(NSInteger)component {
    if (component == 1) {
        DDLogInfo(@"year--->%@", self.datePicker.yearArray[row]);
        _datePicker.year = _datePicker.yearArray[row];
        
    } else {
        DDLogInfo(@"month--->%@", self.datePicker.monthArray[row]);
        _datePicker.month = _datePicker.monthArray[row];
    }
}
- (void)tdDatePickerSetDate:(TDTwoPicker *)viewController {
    [self dismissSemiModalViewController:viewController];
  NSString * dateString = [NSString stringWithFormat:@"%@-%@-01", _datePicker.year,_datePicker.month];
    NSDateFormatter *dateFormatter = [NSDateFormatter localeDateFormatter];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"yyyy-MMMM-dd"];
    _selectedDate = [dateFormatter dateFromString:dateString];
    NSDateFormatter *formatter = [NSDateFormatter localeDateFormatter];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateFormat:@"MMM yyyy"];
    
    [self.yearmonthdate
     setTitle:[formatter stringFromDate:_selectedDate]
     forState:UIControlStateNormal];
    
    self.yearmonthdate.titleLabel.text =
    [formatter stringFromDate:_selectedDate];
    
    [self checkEmptyToChangeSaveButtonColor];
}

- (void)tdDatePickerClearDate:(TDTwoPicker *)viewController {
    [self dismissSemiModalViewController:_datePicker];
    
    _selectedDate = nil;
}

- (void)tdDatePickerCancel:(TDTwoPicker *)viewController {
    [self dismissSemiModalViewController:_datePicker];
}

#pragma mark -textfield   Delegate-- --------------------------------------------------------------------------------------------------------
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidBeginEditing");
    [self dismissSemiModalViewController:_datePicker];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    [self checkEmptyToChangeSaveButtonColor];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.price) {
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 20;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
@end