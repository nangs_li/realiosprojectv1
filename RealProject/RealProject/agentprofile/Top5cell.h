//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentPastClosingItemView.h"
@interface Top5cell : UITableViewCell
@property(weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIButton *deleteButton;
@property(strong, nonatomic) IBOutlet UITextField *typeofspace;
@property(strong, nonatomic) IBOutlet UILabel *price;

@property(strong, nonatomic) IBOutlet UILabel *date;
@property(strong, nonatomic) IBOutlet UILabel *address;
@property(strong, nonatomic) NSString *idstring;

@property (nonatomic,strong) AgentPastClosingItemView *itemView;

-(void)configureCell:(AgentPastClosing*)pastClosing  withStyle:(ItemViewStyle)style;
@end
