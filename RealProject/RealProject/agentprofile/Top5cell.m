//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "Top5cell.h"

#import <QuartzCore/QuartzCore.h>

@implementation Top5cell

- (void)awakeFromNib {
  // Initialization code
}

-(void)configureCell:(AgentPastClosing *)pastClosing withStyle:(ItemViewStyle)style{
    if ([RealUtility isValid:pastClosing]) {
        self.itemView.hidden = NO;
    }else{
        self.itemView.hidden = YES;
    }
    if (!self.itemView) {
        self.itemView = [[AgentPastClosingItemView alloc]initWithAgentPastClosing:pastClosing size:CGSizeMake([RealUtility screenBounds].size.width, self.contentView.frame.size.height) withStyle:style];
        [self.contentView addSubview:self.itemView];
    }
    [self.itemView configureView:pastClosing];
}
@end
