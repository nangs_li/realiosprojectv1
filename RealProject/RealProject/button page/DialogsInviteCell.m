//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "DialogsInviteCell.h"
#import "UIImage+Utility.h"
@implementation DialogsInviteCell

- (void)awakeFromNib {
  // Initialization code
}
- (void)setupLanguage{
    self.notOnRealLabel.text =JMOLocalizedString(@"chat_lobby_search__not_on_real", nil);
    [self.inviteButton setTitle:JMOLocalizedString(@"common__invite", nil)
                     forState:UIControlStateNormal];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  // super setSelected

  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}
@end
