//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>

// Views
#import "RealBouncyButton.h"
@class DialogsSearchAgentCell;

@protocol DialogsSearchAgentCellDelegate <NSObject>

@optional

- (void)searchCell:(DialogsSearchAgentCell *)cell followButtonDidPress:(UIButton *)button;

@end

@interface DialogsSearchAgentCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *title;

@property(strong, nonatomic) IBOutlet UILabel *agentlistingstreetname;
@property (strong, nonatomic) IBOutlet RealBouncyButton *followButton;
@property (strong, nonatomic) IBOutlet UIButton *agentImageBtn;
@property(strong, nonatomic) IBOutlet UIImageView *personalimageview;
@property(strong, nonatomic) IBOutlet UIImageView *apartmentImageView;
@property (strong, nonatomic) AgentProfile *agentProfile;
@property (weak, nonatomic) id<DialogsSearchAgentCellDelegate> delegate;

#pragma mark Message SetUp Method
- (void)configureWithAgentProfile:(AgentProfile*)profile;
- (void)setupFollowButton:(AgentProfile *)agentProfile;
@end
