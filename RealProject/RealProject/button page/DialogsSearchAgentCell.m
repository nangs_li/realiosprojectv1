//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "DialogsSearchAgentCell.h"
#import "UIImage+Utility.h"
@implementation DialogsSearchAgentCell

- (void)awakeFromNib {
    // Initialization code
    self.personalimageview.contentMode = UIViewContentModeScaleAspectFill;
}
- (void)configureWithAgentProfile:(AgentProfile*)profile{
    self.title.text = profile.MemberName;
    self.agentlistingstreetname.text = [profile agentAddress];
    NSString *apartmentImage = [[SystemSettingModel shared]getMiniViewPropertyImageByPropertyType:profile.AgentListing.PropertyType spaceType:profile.AgentListing.SpaceType];
    self.personalimageview.layer.cornerRadius = self.personalimageview.frame.size.height / 2;
    [self.personalimageview loadImageURL:[NSURL URLWithString:profile.AgentPhotoURL] withIndicator:YES];
    
    if ([profile getIsFollowing]) {
        self.personalimageview.borderType = UIImageViewBorderTypeFollowing;
    }else{
        self.personalimageview.borderType = UIImageViewBorderTypeNone;
        
        
    }

    self.apartmentImageView.image = [UIImage imageNamed:apartmentImage];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // super setSelected
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setupFollowButton:(AgentProfile *)agentProfile {
    
    _agentProfile = agentProfile;
    
    if (agentProfile.MemberID==0) {
        self.followButton.hidden=YES;
        
        return;
    } else {
        [self.followButton addTarget:self
                              action:@selector(followPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
        self.followButton.hidden = NO;
    }
    if ([agentProfile getIsFollowing]) {
        [self.followButton setTitle:JMOLocalizedString(@"common__following", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor whiteColor]
                                forState:UIControlStateNormal];
        [self.followButton setBackgroundColor:[UIColor colorWithRed:(0 / 255.0)
                                                              green:(153 / 255.0)
                                                               blue:(255 / 255.0)
                                                              alpha:1.0]];
        [self.followButton setImage:[UIImage imageNamed:@"btn_ico_follow_unfollow"]
                           forState:UIControlStateNormal];
        self.personalimageview.borderType = UIImageViewBorderTypeFollowing;
        
    } else {
        
        [self.followButton setTitle:JMOLocalizedString(@"common__follow", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor colorWithRed:(0 / 255.0)
                                                         green:(153 / 255.0)
                                                          blue:(255 / 255.0)
                                                         alpha:1.0]
                                forState:UIControlStateNormal];
        [self.followButton setBackgroundColor:[UIColor whiteColor]];
        [self.followButton setImage:[UIImage imageNamed:@"btn_ico_follow_follow"]
                           forState:UIControlStateNormal];
        [self.followButton
         setBackgroundImage:
         [UIImage imageWithBorder:[UIColor colorWithRed:(0 / 255.0)
                                                  green:(153 / 255.0)
                                                   blue:(255 / 255.0)
                                                  alpha:1.0]
                             size:self.followButton.frame.size]
         forState:UIControlStateNormal];
        
        self.personalimageview.borderType = UIImageViewBorderTypeNone;
        
        
        
    }
    if (agentProfile.MemberID ==
        [MyAgentProfileModel shared].myAgentProfile.MemberID) {
        self.followButton.userInteractionEnabled = NO;
    }
    
}
- (BOOL)isEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

- (void)followPressed:(UIButton *)sender {
    
    if ([_delegate respondsToSelector:@selector(searchCell:followButtonDidPress:)]) {
        
        [_delegate searchCell:self followButtonDidPress:sender];
    }
}
@end
