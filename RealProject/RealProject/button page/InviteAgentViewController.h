//
//  InviteAgentViewController.h
//  productionreal2
//
//  Created by Alex Hung on 23/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "BaseViewController.h"

@interface InviteAgentViewController : BaseViewController
@property (nonatomic,strong) IBOutlet UILabel *inviteTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *inviteDescriptionLabel;
@property (nonatomic,strong) IBOutlet UITextField *countryCodeTextField;
@property (nonatomic,strong) IBOutlet UITextField *phoneNumberTextField;
@property (nonatomic,strong) IBOutlet UIButton *inviteButton;

@property (nonatomic,strong) NSString *shareText;
@property (nonatomic,strong) NSString *shareURL;

-(IBAction)inviteButtonDidPress:(id)sender;
-(IBAction)phoneCodeButtonDidPress:(id)sender;
-(IBAction)dismissButtonDidPress:(id)sender;

@end
