//
//  InviteAgentViewController.m
//  productionreal2
//
//  Created by Alex Hung on 23/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "InviteAgentViewController.h"
#import "RealUtility.h"
#import "SendSMSViaTwilioModel.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "DeepLinkActionHanderModel.h"
@interface InviteAgentViewController () <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
@property (nonatomic,strong) NSString *phoneCountryCode;
@property (nonatomic,strong) NSString *phoneNumber;
@property (nonatomic,strong) NSMutableArray *countryMapDict;
@property (nonatomic,strong) UIPickerView *countryCodePickerView;
@end

@implementation InviteAgentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self commonSetup];
    // Do any additional setup after loading the view from its nib.
}

- (void)commonSetup{
    NSString *titleMarkUpString = [NSString stringWithFormat:@"<blue>%@</blue><white>%@</white>", JMOLocalizedString(@"invite__title", nil),JMOLocalizedString(@"common__agents", nil)];
    
    NSString *descritionString = JMOLocalizedString(@"invite_agent__TextBox", nil);
    [self.inviteTitleLabel setAttributedTextWithMarkUp:titleMarkUpString];
    [self.inviteDescriptionLabel setText:descritionString];
    
    self.phoneNumberTextField.text =@"";
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(Done, nil)
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneButtonDidPress:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.phoneNumberTextField.inputAccessoryView = keyboardDoneButtonView;
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, self.phoneNumberTextField.frame.size.height)];
    leftView.backgroundColor = self.phoneNumberTextField.backgroundColor;
    self.phoneNumberTextField.leftView = leftView;
    self.phoneNumberTextField.leftViewMode = UITextFieldViewModeAlways;
    self.phoneNumberTextField.background = [UIImage imageWithBorder:[UIColor whiteColor] size:self.phoneNumberTextField.bounds.size];
    
    UIView *countryLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, self.countryCodeTextField.frame.size.height)];
    countryLeftView.backgroundColor = self.countryCodeTextField.backgroundColor;
    self.countryCodeTextField.leftView = countryLeftView;
    self.countryCodeTextField.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView* countryRightImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 31, self.countryCodeTextField.frame.size.height)];
    countryRightImageView.contentMode = UIViewContentModeCenter;
    countryRightImageView.image = [UIImage imageNamed:@"btn_pop_pulldown"];
    
    self.countryCodeTextField.rightView = countryRightImageView;
    self.countryCodeTextField.rightViewMode = UITextFieldViewModeAlways;
    self.countryCodeTextField.background = [UIImage imageWithBorder:[UIColor whiteColor] size:self.phoneNumberTextField.bounds.size];
    self.countryCodeTextField.delegate = self;
    UIToolbar* countryDoneButtonView = [[UIToolbar alloc] init];
    [countryDoneButtonView sizeToFit];
    UIBarButtonItem* countryDoneButton = [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(Done, nil)
                                                                          style:UIBarButtonItemStylePlain target:self
                                                                         action:@selector(doneButtonDidPress:)];
    [countryDoneButtonView setItems:[NSArray arrayWithObjects:countryDoneButton, nil]];
    
    self.countryCodePickerView = [[UIPickerView alloc] init];
    self.countryCodePickerView.delegate = self;
    self.countryCodePickerView.showsSelectionIndicator = YES;
    self.countryCodeTextField.inputView = self.countryCodePickerView;
    self.countryCodeTextField.inputAccessoryView = countryDoneButtonView;
    UIView *overlay = [[UIView alloc] init];
    [overlay setFrame:CGRectMake(0, 0, self.countryCodeTextField.frame.size.width, self.countryCodeTextField.frame.size.height)];
    [self.countryCodeTextField addSubview:overlay];
    overlay.userInteractionEnabled = NO;
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"phonecode" ofType:@"plist"];
//    self.countryMapDict = [[NSDictionary alloc] initWithContentsOfFile:path];
//    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
//    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
//    NSString *cc = [carrier isoCountryCode];
//    cc = [cc uppercaseString];
//    NSString *userCurrentCountyCode = self.countryMapDict[cc];
//    [self updateContryCode:userCurrentCountyCode];
//    int rowIndex = (int)[self.countryMapDict.allKeys indexOfObject:cc];
//    if (rowIndex != NSNotFound) {
//        [self.countryCodePickerView selectRow:rowIndex inComponent:0 animated:NO];
//    }
    self.countryMapDict = [[SystemSettingModel shared]getNotDuplicateSMSCountryCodeList];

    int smsindex= [[SystemSettingModel shared]getIndexFromNotDuplicateSMSCountryCodeListAccordingToCountry];
    
    
    if (smsindex != NSNotFound) {
        [self.countryCodePickerView selectRow:smsindex inComponent:0 animated:NO];
        [self updateContryCode:self.countryMapDict[smsindex]] ;
    }else{
        if (![self isEmpty:self.countryMapDict]) {
            [self.countryCodePickerView selectRow:0 inComponent:0 animated:NO];
            [self updateContryCode:self.countryMapDict[0]] ;
        }
        
        
    }}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    [self.inviteButton setTitle:JMOLocalizedString(@"invite__title", nil) forState:UIControlStateNormal];
    
    NSString *titleMarkUpString = [NSString stringWithFormat:@"<blue>%@</blue><white>%@</white>", JMOLocalizedString(@"invite__title", nil),JMOLocalizedString(@"common__agents", nil)];
    
    NSString *descritionString = JMOLocalizedString(@"invite_agent__TextBox", nil);
    [self.inviteTitleLabel setAttributedTextWithMarkUp:titleMarkUpString];
    [self.inviteDescriptionLabel setText:descritionString];
    
}
-(void)updateContryCode:(NSString*)countryCode{
    self.phoneCountryCode = countryCode;
    NSString *phoneCodeString = @"";
    if (self.phoneCountryCode) {
        phoneCodeString = [NSString stringWithFormat:@"+%@",self.phoneCountryCode];
    }
    
    self.countryCodeTextField.text =phoneCodeString;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    //    [self phoneCodeButtonDidPress:nil];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)doneButtonDidPress:(id)sender{
    [self.view endEditing:YES];
}



-(IBAction)inviteButtonDidPress:(id)sender{
    if (![self.delegate networkConnection]) {
        [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:JMOLocalizedString(NotNetWorkConnectionText, nil)];
        return;
    }
    if ([RealUtility isValid:self.phoneCountryCode] && [RealUtility isValid:self.phoneNumberTextField.text]) {
        NSString *countryCode = [NSString stringWithFormat:@"+%@",self.phoneCountryCode];
        NSString *phoneNumber = self.phoneNumberTextField.text;
        NSString *memberId = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID stringValue];
        NSString *QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
        NSString* memberName = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
        
        [self showLoadingHUD];
       
        [[DeepLinkActionHanderModel shared]inviteToChatWithQBID:QBID memberId:memberId memberName:memberName block:^(NSString *url, NSError *error) {
            if (!error) {
                [[SendSMSViaTwilioModel shared]SendSMSViaTwilio:countryCode ToPhoneNumber:phoneNumber url:url success:^(id response) {
                     [self showAlertWithTitle:JMOLocalizedString(kSuccess, nil) message:JMOLocalizedString(AgentinvitationSMShasbeensentsuccessfully, nil)];
                    [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [self hideLoadingHUD];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
                    [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                  errorMessage:errorMessage
                                                                   errorStatus:errorStatus
                                                                         alert:alert
                                                                            ok:ok];
                    [self hideLoadingHUD];
                }];
            }else{
                [self showAlertWithTitle:JMOLocalizedString(@"alert_alert", nil) message:error.localizedDescription];
                
                [self hideLoadingHUD];
            }

        }];
        
    }
}
-(IBAction)phoneCodeButtonDidPress:(id)sender{
    
}

-(IBAction)dismissButtonDidPress:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.countryMapDict.count;
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *countryCode = self.countryMapDict[row];
    return countryCode;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSString *countryCode = self.countryMapDict[row];
    [self updateContryCode:countryCode];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
