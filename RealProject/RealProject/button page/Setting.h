//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface Setting
    : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *settingTableView;
@property(nonatomic, retain) NSMutableArray *accountData;
@property(nonatomic, retain) NSMutableArray *supportData;

@property (strong, nonatomic) IBOutlet UIButton *broadcastBtn;
@property (strong, nonatomic) IBOutlet UIButton *connectAgentBtn;
@property (strong, nonatomic) IBOutlet UIButton *inviteChatBtn;
@property (strong, nonatomic) IBOutlet UILabel *broadcastLabel;
@property (strong, nonatomic) IBOutlet UILabel *connectAgentLabel;
@property (strong, nonatomic) IBOutlet UILabel *inviteChatLabel;
@property (strong, nonatomic) IBOutlet UIView *settingContentContainerView;

@end
