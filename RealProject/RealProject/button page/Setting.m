//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Setting.h"
#import "Push.h"
#import "Share.h"
#import "Languagepage.h"
#import "BaseViewController.h"
#import "Reportaproblem.h"
#import "Privatepolicy.h"
#import "Termsofservice.h"
#import "Aboutthisversion.h"
#import "ChatSetting.h"
#import "ChatPrivacy.h"
#import "DBQBChatDialog.h"
#import "MetricSetting.h"
#import "UIActionSheet+Blocks.h"
#import "RealUtility.h"
#import "LoginByEmailModel.h"
#import "RealChangePasswordViewController.h"
#import "RealChangeEmailViewController.h"
#import "SettingResetPassword.h"
#import "DeepLinkActionHanderModel.h"
#import "InviteAgentViewController.h"
#import "Languagepage.h"
#import "UILabel+Utility.h"

// Mixpanel
#import "Mixpanel.h"

// Controllers
#import "ViralViewController.h"
#import "SettingContentViewController.h"
#import "RealPhoneBookShareViewController.h"

@interface Setting () <SettingContentViewControllerDelegate>

@property (nonatomic, strong) SettingContentViewController *contentViewController;

@end
@implementation Setting
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupUpNavBar];
    [[self.delegate getTabBarController]setTabBarHidden:NO animated:YES];
}

-(void)setupUpNavBar{
    
    [[self.delegate getTabBarController]changeNavBarType:NavigationBarSearch animated:YES block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
        if(didChange){
            self.navBarContentView = navBarController.settingNavBar;
            navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__settings", nil);;
            navBarController.settingBackButton.hidden = YES;
            [self showPageControl:NO animated:YES];
        }
    }];
    self.navBarContentView.alpha = 1.0;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[self.delegate getTabBarController] setTabBarHidden:NO];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //// init accountTableiVew
    self.view.backgroundColor = RealDarkBlueColor;
    self.settingTableView.delegate = self;
    self.settingTableView.dataSource = self;
    self.settingTableView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0);
    [self.settingTableView reloadData];
    
    [self setupSettingContentViewController];
}

- (void)setupSettingContentViewController
{
    _contentViewController = [SettingContentViewController new];
    _contentViewController.settingContentViewControllerDelegate = self;
    
    [self addChildViewController:_contentViewController];
    
    [self.settingContentContainerView addSubview:_contentViewController.view];
    _contentViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [_contentViewController.view autoPinEdgesToSuperviewEdges];
    
    [_contentViewController willMoveToParentViewController:self];
}

#pragma mark - <SettingContentViewControllerDelegate>

- (void)controllerDidPressLogout:(SettingContentViewController *)controller
{
    [self logoutpress:nil];
}

#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    
    _accountData = [[NSMutableArray alloc] initWithObjects:
                    JMOLocalizedString(@"setting__chats", nil),
                    JMOLocalizedString(@"setting__privacy", nil),
                    JMOLocalizedString(@"setting__language", nil),
                    JMOLocalizedString(@"setting__unit", nil),
                    nil];
    
    if ([LoginByEmailModel shared].realNetworkMemberInfo.UserID){
        
        [_accountData addObject:JMOLocalizedString(@"change_password__title", nil)];
        [_accountData addObject:JMOLocalizedString(@"change_email__title", nil)];
        [_accountData addObject:JMOLocalizedString(@"reset_password__title", nil)];
    }
    
    _supportData = [[NSMutableArray alloc] initWithObjects:
                    JMOLocalizedString(@"setting__feedback", nil),
                    JMOLocalizedString(@"setting__privacy_policy", nil),
                    JMOLocalizedString(@"setting__terms_of_service", nil),
                    JMOLocalizedString(@"setting__about_this_version",nil),
                    JMOLocalizedString(@"common__log_out", nil),
                    nil];
    
    //  [self.Logoutbutton setTitle:JMOLocalizedString(@"SettingPage.Log Out", nil)
    //                  forState:UIControlStateNormal];
    
    NSString *chatTitle =JMOLocalizedString(@"setting__invite_to_chat", nil);
    NSString *broadcastTitle = JMOLocalizedString(@"setting__broadcast", nil);
    NSString *connectAgentTitle = JMOLocalizedString(@"setting__connect_agent", nil);
    
    [self.inviteChatLabel setAttributedTextWithMarkUp:chatTitle];
    [self.broadcastLabel setAttributedTextWithMarkUp:broadcastTitle];
    [self.connectAgentLabel setAttributedTextWithMarkUp:connectAgentTitle];
    //    self.inviteChatLabel.text =
    //    JMOLocalizedString(@"common__invite_to_chat", nil);
    
    //    self.inviteFollowLabel.text =
    //    JMOLocalizedString(@"common__invite_to_follow", nil);
    //
    //    self.inviteDownloadLabel.text =
    //    JMOLocalizedString(@"setting__invite_to_download", nil);
    
    [self.settingTableView reloadData];
}

#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return [_accountData count];
    
    else {
        return [_supportData count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    UILabel *label;
    UIImageView *markerImageView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:simpleTableIdentifier];
        CGRect cellFrame = cell.bounds;
        cellFrame.size.width = [RealUtility screenBounds].size.width;
        cell.contentView.frame = cellFrame;
        label =[[UILabel alloc]initWithFrame:CGRectMake(20, 0, cell.contentView.frame.size.width-40, cell.contentView.frame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.tag = 1001;
        UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(20, cell.contentView.frame.size.height -1, tableView.frame.size.width-20, 1)];
        dividerView.image =[UIImage imageNamed:@"pop_dottedline"];
        dividerView.clipsToBounds = YES;
        markerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, cell.contentView.frame.size.width -40 -15, 15, 15)];
        markerImageView.clipsToBounds = YES;
        markerImageView.tag = 1002;
        markerImageView.image =[UIImage imageNamed:@"btn_setting_detail"];
        markerImageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        //        markerImageView.hidden = YES;
        [cell.contentView addSubview:label];
        [cell.contentView addSubview:dividerView];
        [cell.contentView addSubview:markerImageView];
        [label autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
        [label autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:20];
        
        [dividerView autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:20];
        [dividerView autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:20];
        
        [dividerView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
        
        [markerImageView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
        [markerImageView autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:20];
    }else{
        label = (UILabel*)[cell.contentView viewWithTag:1001];
        markerImageView = (UIImageView*)[cell.contentView viewWithTag:1002];
    }
    if (indexPath.section == 0){
        label.text = [_accountData objectAtIndex:indexPath.row];
        if (indexPath.row == 0 || indexPath.row == 1) {
            if ([[LoginBySocialNetworkModel shared] qbAccountIsReady]) {
                label.textColor = [UIColor whiteColor];
            }else{
                label.textColor = [UIColor lightGrayColor];
            }
        }
        
    }else {
        label.textColor = [UIColor whiteColor];
        label.text = [_supportData objectAtIndex:indexPath.row];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = nil;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [label setUpDefaultDetailTextFontSize];
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BaseViewController *UIViewController = nil;
    BOOL didPush = NO;
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                if ([[LoginBySocialNetworkModel shared] qbAccountIsReady]) {
                    UIViewController = [[ChatSetting alloc] initWithNibName:@"ChatSetting" bundle:nil];
                }
                break;
            case 1:
                if ([[LoginBySocialNetworkModel shared] qbAccountIsReady]) {
                    UIViewController = [[ChatPrivacy alloc] initWithNibName:@"ChatPrivacy" bundle:nil];
                }
                break;
            case 2:
                UIViewController = [[Languagepage alloc] initWithNibName:@"Languagepage" bundle:nil];
                break;
            case 3:
                UIViewController = [[MetricSetting alloc] initWithNibName:@"MetricSetting" bundle:nil];
                break;
            case 4:
                UIViewController = [[RealChangePasswordViewController alloc]initWithNibName:@"RealChangePasswordViewController" bundle:nil];
                break;
            case 5:{
                
                // Mixpanel
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel track:@"Attempted to Change Email Address"];
                
                UIViewController = [[RealChangeEmailViewController alloc]initWithNibName:@"RealChangeEmailViewController" bundle:nil];
                break;
            }
                
            case 6:
                UIViewController = [[SettingResetPassword alloc]initWithNibName:@"SettingResetPassword" bundle:nil];
                break;
            default:
                break;
        }
    } else {
        
        switch (indexPath.row) {
                
            case 0:
                UIViewController =
                [[Reportaproblem alloc] initWithNibName:@"Reportaproblem"
                                                 bundle:nil];
                
                break;
            case 1:
                UIViewController =
                [[Privatepolicy alloc] initWithNibName:@"Privatepolicy" bundle:nil];
                
                break;
                
            case 2:
                UIViewController =
                [[Termsofservice alloc] initWithNibName:@"Termsofservice"
                                                 bundle:nil];
                
                break;
            case 3:{
                UIViewController = [[Aboutthisversion alloc] initWithNibName:@"Aboutthisversion" bundle:nil];
                break;
            }case 4:
                [self logoutpress:nil];
                return;
                break;
                
            default:
                break;
        }
    }
    if (UIViewController) {
        [[self.delegate getTabBarController]setTabBarHidden:YES animated:YES];
        UIViewController.view.backgroundColor = self.view.backgroundColor;
        [self.navigationController pushViewController:UIViewController animated:YES];
    }
}
#pragma mark UITableView  viewForHeaderInSection
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.settingTableView.frame.size.width, 44)];
    headerView.backgroundColor = RealDarkBlueColor;
    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(24, 0, headerView.frame.size.width-24, headerView.frame.size.height)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor lightGrayColor];
    if (section == 0) {
        label.text = JMOLocalizedString(@"common__account", nil);
    } else {
        label.text = JMOLocalizedString(@"common__support", nil);
    }
    
    DDLogDebug(@" label.font %@", label.font);
    UIImageView *dividerView =[[UIImageView alloc]initWithFrame:CGRectMake(16, 43, headerView.frame.size.width-16, 1)];
    dividerView.image =[UIImage imageNamed:@"content_breakline"];
    dividerView.clipsToBounds=YES;
    dividerView.backgroundColor=[UIColor clearColor];
    [dividerView autoSetDimension:ALDimensionHeight toSize:1];
    [headerView addSubview:label];
    [headerView addSubview:dividerView];
    
    [label autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [label autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:20];
    
    [dividerView autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:20];
    [dividerView autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:20];
    [dividerView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [label setUpDefaultTitleFontSize];
    
    return headerView;
}

- (IBAction)broadcast:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Invite to Follow @ Settings"];
    
    ViralViewControllerType controllerType = ViralViewControllerTypeBroadcast;
    
    if ([MyAgentProfileModel shared].myAgentProfile.MemberID >0) {
        
        if (![RealUtility isValid:[MyAgentProfileModel shared].myAgentProfile.AgentListing]) {
            
            controllerType = ViralViewControllerTypeBroadcastCreatePosting;
        }
        
    } else {
        
        controllerType = ViralViewControllerTypeBroadcastBecomeAnAgent;
    }
    
    if (controllerType == ViralViewControllerTypeBroadcast) {
    
        __weak typeof(self) weakSelf = self;
        [[REPhonebookManager sharedManager] requestAccess:^(BOOL granted) {
            
            if (granted) {
                
                RealPhoneBookShareViewController *vc = [RealPhoneBookShareViewController phoneBookShareVCWithShareType:RealPhoneBookShareTypeBoardcast];
                vc.underLayViewController = weakSelf;
                [weakSelf.delegate overlayViewController:vc onViewController:weakSelf];
                
            } else {
                
                ViralViewController *viralViewController = [[ViralViewController alloc] initWithType:controllerType];
                viralViewController.underLayViewController = weakSelf;
                [weakSelf.delegate overlayViewController:viralViewController onViewController:weakSelf];
            }
        }];
        
    } else {
        
        ViralViewController *viralViewController = [[ViralViewController alloc] initWithType:controllerType];
        viralViewController.underLayViewController = self;
        [self.delegate overlayViewController:viralViewController onViewController:self];
    }
}

- (IBAction)connectAgent:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Invite to Download @ Settings"];
    
    __weak typeof(self) weakSelf = self;
    [[REPhonebookManager sharedManager] requestAccess:^(BOOL granted) {
        
        if (granted) {
            
            RealPhoneBookShareViewController *vc = [RealPhoneBookShareViewController phoneBookShareVCWithShareType:RealPhoneBookShareTypeConnectAgent];
            vc.underLayViewController = weakSelf;
            [weakSelf.delegate overlayViewController:vc onViewController:weakSelf];
            
        } else {
            
            UIViewController *alert = [REPhonebookManager noPermissionAlertWithCancelBlock:nil];
            [weakSelf presentViewController:alert animated:YES completion:nil];
        }
    }];
}
- (IBAction)inviteToChat:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Invite to Chat @ Settings"];
    
    ViralViewController *viralViewController = [[ViralViewController alloc] initWithType:ViralViewControllerTypeChat];
    viralViewController.underLayViewController = self;
    [self.delegate overlayViewController:viralViewController onViewController:self];
}

#pragma mark Button
- (IBAction)logoutpress:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Attempted to Log Out"];
    
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:JMOLocalizedString(@"setting__confirm_logout", nil)
                                                    delegate:nil
                                           cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                                      destructiveButtonTitle:JMOLocalizedString(@"common__log_out", nil)
                                           otherButtonTitles:nil];
    
    as.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    as.tapBlock = ^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            
            // Mixpanel
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Logged Out"];
            [mixpanel flush];
            
            [self.delegate totalLogoutErrorMessage:@"logout"];
        }
    };
    
    [as showInView:self.view];
}

- (IBAction)changeToArButton:(id)sender {
    NSArray* languages = [NSArray arrayWithObjects:@"ar", nil];
    [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    /*
     if([[[UIView alloc] init] respondsToSelector:@selector(setSemanticContentAttribute:)]) {
     [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
     [self.delegate restartAllView];
     }
     */
}
- (IBAction)changeToEnButton:(id)sender {
    NSArray* languages = [NSArray arrayWithObjects:@"en", nil];
    [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    /*
     if([[[UIView alloc] init] respondsToSelector:@selector(setSemanticContentAttribute:)]) {
     [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
     [self.delegate restartAllView];
     }
     */
    
}
@end