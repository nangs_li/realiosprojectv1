//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

@interface Userprofile : BaseViewController

@property(weak, nonatomic) IBOutlet UILabel *labelname;

@property(strong, nonatomic) IBOutlet UIImageView *personalphoto;

@property(strong, nonatomic) IBOutlet UISwitch *Switch;
@property(strong, nonatomic) IBOutlet UIView *whiteuiview;

@end
