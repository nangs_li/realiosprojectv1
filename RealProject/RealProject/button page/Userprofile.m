//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Userprofile.h"
#import "Editprofile.h"
#import "Finishprofile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Finishprofilehavelisting.h"
@interface Userprofile ()
@end
@implementation Userprofile

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
//// set delegate store posting data to nil
    [[AgentListingModel shared] resetAllAgentListingPartOnlyLocalData];

//// set membername,photourl from NSUserDefaults
  if (![self isEmpty:[MyAgentProfileModel shared].myAgentProfile.AgentListing]) {
    Finishprofilehavelisting *UIViewController =
        [[Finishprofilehavelisting alloc]
            initWithNibName:@"Finishprofilehavelisting"
                     bundle:nil];
    [self.navigationController pushViewController:UIViewController animated:NO];

  } else {
    if (![MyAgentProfileModel shared].myAgentProfile.MemberID==0) {
      
        
      Finishprofile *UIViewController =
          [[Finishprofile alloc] initWithNibName:@"Finishprofile" bundle:nil];
      [self.navigationController pushViewController:UIViewController
                                           animated:NO];

    } else {
      self.whiteuiview.hidden = YES;

      self.labelname.text = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;
      NSURL *url = [NSURL URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];
        [self.personalphoto loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
      [self.delegate setupcornerRadius:self.personalphoto];
    }
  }
}

- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
  [super viewDidLoad];

  [self.Switch addTarget:self
                  action:@selector(stateChanged:)
        forControlEvents:UIControlEventValueChanged];
}
#pragma mark stateChanged
- (void)stateChanged:(UISwitch *)switchState {
  if ([switchState isOn]) {
    // PostingScrollview *postingScrollview = [[PostingScrollview alloc]
    // initWithNibName:@"PostingScrollview" bundle:nil];
    Editprofile *registeragentscrollview =
        [[Editprofile alloc] initWithNibName:@"Editprofile" bundle:nil];

    CATransition *transition = [CATransition animation];
    transition.duration = ANIMATION_DURATION;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;

    [self.navigationController.view.layer addAnimation:transition
                                                forKey:kCATransition];

    [self.navigationController pushViewController:registeragentscrollview
                                         animated:NO];
    //   [self.navigationController pushViewController:postingScrollview
    //   animated:NO];

  } else {
  }
}

@end