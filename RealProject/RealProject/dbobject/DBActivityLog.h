//
//  DBActivityLog.h
//  productionreal2
//
//  Created by Alex Hung on 12/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <DBAccess/DBAccess.h>
@class ActivityLogModel;
@interface DBActivityLog : DBObject
@property (nonatomic,assign) NSInteger ID;
@property (nonatomic,assign) NSNumber *MemberID;
@property (nonatomic,strong) NSDate *CreationDate;
@property (nonatomic,strong) NSString *CreationDateString;
@property (nonatomic,assign) NSInteger LogType;
@property (nonatomic,strong) NSData *Places;
@property (nonatomic,strong) NSString *BasedIn;
@property (nonatomic,strong) NSDate *DateOfJoin;
@property (nonatomic,strong) NSDate *DateOfFollow;
@property (nonatomic,strong) NSString *DateOfJoinString;
@property (nonatomic,strong) NSString *DateOfFollowString;
@property (nonatomic,assign) NSInteger FollowingCount;
@property (nonatomic,assign) NSInteger ActiveChatCount;

+ (DBActivityLog*) initWithActivityLogModel:(ActivityLogModel*)model;

@end
