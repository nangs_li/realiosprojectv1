//
//  DBActivityLog.m
//  productionreal2
//
//  Created by Alex Hung on 12/1/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "DBActivityLog.h"
#import "ActivityLogModel.h"
@implementation DBActivityLog

@dynamic ID,MemberID,CreationDate,CreationDateString, LogType,Places,BasedIn,DateOfFollow,DateOfJoin,DateOfFollowString,DateOfJoinString,ActiveChatCount,FollowingCount;
+ (DBActivityLog*) initWithActivityLogModel:(ActivityLogModel*)model{
    DBActivityLog *dbLog = [[DBActivityLog alloc]init];
    dbLog.ID = model.ID;
    dbLog.Id = @(model.ID);
    dbLog.MemberID = @(model.MemberID);
    dbLog.CreationDateString = model.CreationDateString;
    dbLog.LogType = model.LogType;
    dbLog.Places = [NSKeyedArchiver archivedDataWithRootObject:model.Places];
    dbLog.BasedIn = model.BasedIn;
    dbLog.DateOfFollow = model.DateOfFollow;
    dbLog.DateOfJoin = model.DateOfJoin;
    dbLog.DateOfJoinString = model.DateOfJoinString;
    dbLog.DateOfFollowString = model.DateOfFollowString;
    dbLog.FollowingCount = model.FollowingCount;
    dbLog.ActiveChatCount = model.ActiveChatCount;
    return dbLog;
}


@end
