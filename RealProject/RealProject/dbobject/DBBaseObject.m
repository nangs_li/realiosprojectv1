//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBBaseObject.h"

@implementation DBBaseObject
@dynamic MemberID, Date, QBID;

+(DBBaseObject*)setCommonValue:(DBBaseObject*)dbBaseObject{
    
    
    dbBaseObject.MemberID = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    dbBaseObject.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
    dbBaseObject.Date = [NSDate date];
   
    
    return dbBaseObject;
}
@end