//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>
#import "ChatRoomRelationObject.h"
@interface DBChatRoomRelationArray : DBObject
//+ (d *)shared;
@property(strong, nonatomic) NSNumber* MemberID;
@property(strong, nonatomic) NSString* QBID;
@property(strong, nonatomic) NSData *ChatRoomRelationArray;
+(DBChatRoomRelationArray*)createSearchAgentHistoryWithChatRoomRelationArray:(NSArray<ChatRoomRelationObject> *)chatRoomRelationArray;

@end
