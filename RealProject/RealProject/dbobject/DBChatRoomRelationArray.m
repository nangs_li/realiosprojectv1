//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBChatRoomRelationArray.h"

@implementation DBChatRoomRelationArray
@dynamic  MemberID, QBID, ChatRoomRelationArray;

+(DBChatRoomRelationArray*)createSearchAgentHistoryWithChatRoomRelationArray:(NSArray<ChatRoomRelationObject> *)chatRoomRelationArray{
   
        DBChatRoomRelationArray *newHistory = [DBChatRoomRelationArray new];
    newHistory.MemberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
     newHistory.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
       newHistory.ChatRoomRelationArray = [NSKeyedArchiver archivedDataWithRootObject:chatRoomRelationArray];
    [newHistory commit];
    
    return newHistory;
}
@end