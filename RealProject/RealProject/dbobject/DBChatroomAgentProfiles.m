//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBChatroomAgentProfiles.h"

@implementation DBChatroomAgentProfiles
@dynamic MemberID, Date, ChatroomAgentProfile, QBID,AgentProfileQBID;

+(DBChatroomAgentProfiles*)createDBLobbyAgentProfile:(AgentProfile *)chatroomAgentProfile{
    if (![[LoginBySocialNetworkModel shared].myLoginInfo isLoggedIn])
        return nil;
    DBChatroomAgentProfiles *dbChatroomAgentProfiles = [DBChatroomAgentProfiles new];
    dbChatroomAgentProfiles.QBID =[LoginBySocialNetworkModel shared].myLoginInfo.QBID ;
    dbChatroomAgentProfiles.MemberID =[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    dbChatroomAgentProfiles.Date= [NSDate date];
    dbChatroomAgentProfiles.ChatroomAgentProfile = [NSKeyedArchiver
archivedDataWithRootObject:[chatroomAgentProfile copy]];
    dbChatroomAgentProfiles.AgentProfileQBID=chatroomAgentProfile.QBID;
    [dbChatroomAgentProfiles commit];
    
    DDLogDebug(@"dbChatroomAgentProfiles %@",dbChatroomAgentProfiles);
    return dbChatroomAgentProfiles;
}
+(void)removeDBLobbyAgentProfile:(AgentProfile *)chatroomAgentProfile{
    if (![[LoginBySocialNetworkModel shared].myLoginInfo isLoggedIn])
        return ;
    
    [[[[DBChatroomAgentProfiles query]
      whereWithFormat:
      @"MemberID = %@ And AgentProfileQBID = %@",
      [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
       .myLoginInfo.MemberID],chatroomAgentProfile.QBID]
     fetch] removeAll];

}
@end