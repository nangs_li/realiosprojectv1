//
//  DBDialogSetting.h
//  productionreal2
//
//  Created by Alex Hung on 31/12/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <DBAccess/DBAccess.h>

@interface DBDialogSetting : DBObject
@property (nonatomic,assign) NSInteger *dialogID;
@property (nonatomic,assign) NSInteger *QBID;
@property (nonatomic,assign) BOOL mute;
@property (nonatomic,assign) BOOL block;
@property (nonatomic,assign) BOOL exite;
@property (nonatomic,assign) NSInteger *lastUpdateTimeStamp;
@end
