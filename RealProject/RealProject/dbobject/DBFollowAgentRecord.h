//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>

@interface DBFollowAgentRecord : DBObject
//+ (d *)shared;

@property(nonatomic, strong) NSString *QBID;
@property(nonatomic, assign) int MemberID;
@property(nonatomic, assign) BOOL IsFollowing;
@property(nonatomic, assign) int FollowingMemberID;
+(DBFollowAgentRecord*)createDBFollowAgentRecordFromFollowingMemberID:(int)followingMemberID isFollowing:(BOOL)isFollowing;
+(void)removeDBFollowAgentRecordFollowingMemberID:(int)followingMemberID;

@end
