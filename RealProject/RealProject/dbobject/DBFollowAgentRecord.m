//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBFollowAgentRecord.h"

@implementation DBFollowAgentRecord
@dynamic QBID, MemberID,IsFollowing,FollowingMemberID;
+ (DBIndexDefinition*)indexDefinitionForEntity {
  /* create an index definition object */
  DBIndexDefinition* idx = [DBIndexDefinition new];

  /* now specify which properties are going to be indexed */
  [idx addIndexForProperty:@"MemberID"
             propertyOrder:DBIndexSortOrderDescending];

  return idx;
}

+(DBFollowAgentRecord*)createDBFollowAgentRecordFromFollowingMemberID:(int)followingMemberID isFollowing:(BOOL)isFollowing{
    if (![[LoginBySocialNetworkModel shared].myLoginInfo isLoggedIn])
        return nil;
    DBFollowAgentRecord *dbFollowAgentRecord = [DBFollowAgentRecord new];
    dbFollowAgentRecord.MemberID =[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    dbFollowAgentRecord.IsFollowing= isFollowing;
    dbFollowAgentRecord.FollowingMemberID = followingMemberID;
    [dbFollowAgentRecord commit];
    
    return dbFollowAgentRecord;
}

+(void)removeDBFollowAgentRecordFollowingMemberID:(int)followingMemberID{
    if (![[LoginBySocialNetworkModel shared].myLoginInfo isLoggedIn])
        return;
        int selfMemberID=[[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    [[[[DBFollowAgentRecord query]
       whereWithFormat:@"MemberID = %d AND FollowingMemberID = %d",selfMemberID,followingMemberID] fetch] removeAll];

   DBResultSet *r = [[[DBFollowAgentRecord query]
       whereWithFormat:@"MemberID = %d AND FollowingMemberID = %d",selfMemberID,followingMemberID] fetch];
    for (DBFollowAgentRecord * dbFollowAgentRecord in r) {
        DDLogDebug(@"dbFollowAgentRecordShow%@",dbFollowAgentRecord);
    }
  

}
@end