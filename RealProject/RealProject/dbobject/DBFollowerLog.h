//
//  DBFollowerLog.h
//  productionreal2
//
//  Created by Alex Hung on 2/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <DBAccess/DBAccess.h>
@class FollowerModel;
@interface DBFollowerLog : DBObject
@property (nonatomic,assign) NSInteger ID;
@property (nonatomic,strong) NSNumber *SenderID;
@property (nonatomic,assign) NSInteger MemberType;
@property (nonatomic,assign) NSInteger MemberID;
@property (nonatomic,assign) NSInteger ListingID;
@property (nonatomic,assign) BOOL isFollowing;
@property (nonatomic,strong) NSString* MemberName;
@property (nonatomic,strong) NSString* AgentPhotoURL;
@property (nonatomic,strong) NSString* Location;
@property (nonatomic,strong) NSDate* FollowDate;
+ (DBFollowerLog*) initWithFollowerModel:(FollowerModel*)model;
+ (void) removeLogByMemberID:(NSString*)memberID;
@end
