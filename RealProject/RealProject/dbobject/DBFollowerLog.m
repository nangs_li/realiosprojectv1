//
//  DBFollowerLog.m
//  productionreal2
//
//  Created by Alex Hung on 2/2/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "DBFollowerLog.h"
#import "FollowerModel.h"
@implementation DBFollowerLog
@dynamic ID,MemberType,MemberID,ListingID,isFollowing,MemberName,AgentPhotoURL,Location,SenderID, FollowDate;
+ (DBFollowerLog*) initWithFollowerModel:(FollowerModel*)model{

    DBFollowerLog *log = [[DBFollowerLog alloc]init];
    log.ID = model.MemberID;
    log.MemberType = model.MemberType;
    log.MemberID = model.MemberID;
    log.ListingID = model.ListingID;
    log.isFollowing = model.IsFollowing;
    log.MemberName = model.MemberName;
    log.AgentPhotoURL = model.AgentPhotoURL;
    log.Location = model.Location;
    log.FollowDate = model.FollowDate;
    log.SenderID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    return log;
}

+ (void) removeLogByMemberID:(NSString*)memberID{
    DBQuery *followerLogQuery = [[DBFollowerLog query] whereWithFormat:@"SenderID = %@ AND MemberID = %@", [LoginBySocialNetworkModel shared].myLoginInfo.MemberID,memberID];
    [[followerLogQuery fetch]removeAll];
}
@end
