//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBHasBeenDownloadPhoto.h"
@implementation DBHasBeenDownloadPhoto
@dynamic  MemberID, QBID,PhotoUrl;

+(DBHasBeenDownloadPhoto*)createSearchAgentHistoryWithPhotoUrl:(NSString*)photourl {
    
    DBHasBeenDownloadPhoto *newHistory = [DBHasBeenDownloadPhoto new];
    newHistory.MemberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
    
    newHistory.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
    
    newHistory.PhotoUrl = photourl;
    [newHistory commit];
    
    return newHistory;
}
@end