    //
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBHaveShowLanding.h"

@implementation DBHaveShowLanding
@dynamic MemberID, Date, QBID;
+(DBHaveShowLanding*)createDBHaveShowLanding{
    
  DBHaveShowLanding * dbHaveShowLanding=  [DBHaveShowLanding new];
    dbHaveShowLanding.MemberID = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID ;
    dbHaveShowLanding.QBID = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
    dbHaveShowLanding.Date = [NSDate date];
    [dbHaveShowLanding commit];
    return dbHaveShowLanding;
}
+(void)removeDBHaveShowLanding{
[[[[DBHaveShowLanding query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID]  fetch] removeAll];
}

+(int )queryMyDBHaveShowLandingRecordCount{
  
    
    return   [[[DBHaveShowLanding query]whereWithFormat:@"MemberID = %@",[LoginBySocialNetworkModel shared].myLoginInfo.MemberID]  count]  ;
}
@end