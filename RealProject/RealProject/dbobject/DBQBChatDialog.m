//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBQBChatDialog.h"

@implementation DBQBChatDialog
@dynamic DialogID, QBID, LastMessageText, MemberID, LastMessageDate,
    QBChatDialog, IsAgent, LastMessageUserID;
+ (DBIndexDefinition*)indexDefinitionForEntity {
  /* create an index definition object */
  DBIndexDefinition* idx = [DBIndexDefinition new];

  /* now specify which properties are going to be indexed */
  [idx addIndexForProperty:@"MemberID"
             propertyOrder:DBIndexSortOrderDescending];

  return idx;
}

@end