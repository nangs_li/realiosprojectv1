//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>
#import "DBQBChatDialog.h"
@interface DBQBChatDialogClearMessage : DBObject
//+ (d *)shared;
@property(nonatomic, strong) NSString *DialogID;
@property(nonatomic, strong) NSString *QBID;
@property(nonatomic, assign) int MemberID;
@property(nonatomic, strong) NSData *QBChatDialog;
+(DBQBChatDialogClearMessage *)createDBQBChatDialogClearMessage:(QBChatDialog *)chatDialog;
@end
