//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBQBChatDialogClearMessage.h"

@implementation DBQBChatDialogClearMessage
@dynamic DialogID, QBID, MemberID,QBChatDialog;
+ (DBIndexDefinition*)indexDefinitionForEntity {
  /* create an index definition object */
  DBIndexDefinition* idx = [DBIndexDefinition new];

  /* now specify which properties are going to be indexed */
  [idx addIndexForProperty:@"MemberID"
             propertyOrder:DBIndexSortOrderDescending];

  return idx;
}
+(DBQBChatDialogClearMessage *)createDBQBChatDialogClearMessage:(QBChatDialog *)chatDialog{
    DBQBChatDialogClearMessage * dbQBChatDialogClearMessage =[DBQBChatDialogClearMessage new];
    dbQBChatDialogClearMessage.DialogID = chatDialog.ID;
    
    dbQBChatDialogClearMessage.QBChatDialog =
    [NSKeyedArchiver archivedDataWithRootObject:[chatDialog copy]];
    dbQBChatDialogClearMessage.MemberID =
    [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    
    dbQBChatDialogClearMessage.QBID =
    [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
    [dbQBChatDialogClearMessage commit];
    return dbQBChatDialogClearMessage;
}
@end