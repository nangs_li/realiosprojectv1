//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBQBChatDialogDelete.h"

@implementation DBQBChatDialogDelete
@dynamic DialogID, QBID, MemberID,QBChatDialog;
+ (DBIndexDefinition*)indexDefinitionForEntity {
  /* create an index definition object */
  DBIndexDefinition* idx = [DBIndexDefinition new];

  /* now specify which properties are going to be indexed */
  [idx addIndexForProperty:@"MemberID"
             propertyOrder:DBIndexSortOrderDescending];

  return idx;
}

@end