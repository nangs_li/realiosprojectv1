//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBQBChatMessage.h"

@implementation DBQBChatMessage
@dynamic MessageID, DialogID, QBID, MessageType, MemberID, RecipientID,
    SenderID, DateSent, InsertDBDateTime, SendToServerDate,
    QBChatHistoryMessage, PhotoURL, Text, CustomParameters, Attachments,
    ChatMessageType,PhotoType, NumberofDeliverStatus, DidDeliverStatus, ErrorStatus, DrawStatus,
    ReadStatus, Retrycount,IsDeleteMessage,IsBlockedSendMessage;

+ (DBIndexDefinition*)indexDefinitionForEntity {
  /* create an index definition object */
  DBIndexDefinition* idx = [DBIndexDefinition new];

  /* now specify which properties are going to be indexed */

  [idx addIndexForProperty:@"DialogID" propertyOrder:DBIndexSortOrderAscending];
    [idx addIndexForProperty:@"MessageID"
               propertyOrder:DBIndexSortOrderAscending];
  [idx addIndexForProperty:@"MemberID"
             propertyOrder:DBIndexSortOrderAscending];

  return idx;
}
-(void)sendSuccessChangeDB{
    self.NumberofDeliverStatus = 2;
    self.ErrorStatus = NO;
    self.SendToServerDate = [NSDate date];
  [self commit];
}
-(void)sendFailChangeDB{
    self.NumberofDeliverStatus = 1;
    self.ErrorStatus = YES;
    self.SendToServerDate = [NSDate date];
    [self commit];

}

- (NSString*)getLastMessage {
  if (isEmpty(self.Text)) {
    if (!isEmpty(self.ChatMessageType)) {
        if ([self.ChatMessageType isEqualToString:kChatMessageTypeText]) {
            return self.Text;
        }
      return self.ChatMessageType;
    } else {
      return @" ";
    }

  } else {
    return self.Text;
  }
  return @" ";
}
@end