//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>

@interface DBQBUUser : DBObject
//+ (d *)shared;
@property(strong) NSString *DialogID;
@property(assign) NSString *QBID;
@property(strong) NSString *FullName;
@property(strong) NSString *Login;
@property(strong) NSString *Password;
@property(strong) NSString *CustomData;
@property(assign, nonatomic) int MemberID;
@property(strong, nonatomic) NSDate *InsertDataBaseDate;
@property(strong, nonatomic) NSData *QBUUser;
@property(assign, nonatomic) BOOL IsAgent;

@end
