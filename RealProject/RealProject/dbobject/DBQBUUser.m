//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBQBUUser.h"

@implementation DBQBUUser
@dynamic DialogID, QBID, FullName, Login, Password, CustomData, MemberID,
    InsertDataBaseDate, QBUUser, IsAgent;
@end