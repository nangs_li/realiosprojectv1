//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>
@interface DBSearchAddressArray : DBObject
//+ (d *)shared;
@property(strong, nonatomic) NSString* QBID;
@property(strong, nonatomic) NSNumber* MemberID;
@property(strong, nonatomic) NSDate* Date;
@property(strong, nonatomic) NSData* SearchAddressArrayHaveCount;
@property(strong, nonatomic) NSString* PlaceID;
@property(strong, nonatomic) NSString* UserInputSearchAddress;
@property(strong, nonatomic) NSString* GoogleAddressPack;
@property(strong, nonatomic) NSString* AddressSearchType;
@property(strong, nonatomic) NSDictionary* GoogleAddressPackDict;
+ (DBSearchAddressArray*)createDBSearchAddressArrayHaveCount:(SearchAgentProfile*)SearchAddressArrayHaveCount placeID:(NSString*)placeID userInputSearchAddress:(NSString*)userInputSearchAddress googleAddressPackDict:(NSDictionary*)googleAddressPackDict;
@end
