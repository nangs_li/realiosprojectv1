//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBSearchAddressArray.h"

@implementation DBSearchAddressArray
@dynamic MemberID, Date, QBID, SearchAddressArrayHaveCount, PlaceID, UserInputSearchAddress, GoogleAddressPack, AddressSearchType,GoogleAddressPackDict;
+ (DBSearchAddressArray*)createDBSearchAddressArrayHaveCount:(SearchAgentProfile*)SearchAddressArrayHaveCount placeID:(NSString*)placeID userInputSearchAddress:(NSString*)userInputSearchAddress googleAddressPackDict:(NSDictionary*)googleAddressPackDict {
  DBSearchAddressArray* dbSearchAddressArray = [DBSearchAddressArray new];
  dbSearchAddressArray.MemberID              = [LoginBySocialNetworkModel shared].myLoginInfo.MemberID;
  dbSearchAddressArray.QBID                  = [LoginBySocialNetworkModel shared].myLoginInfo.QBID;
  dbSearchAddressArray.Date                  = [NSDate date];
  dbSearchAddressArray.PlaceID               = placeID;
  if ([userInputSearchAddress isEqualToString:kLocationSearchType]) {
    dbSearchAddressArray.AddressSearchType = kLocationSearchType;
      dbSearchAddressArray.UserInputSearchAddress      = nil;
  } else {
    dbSearchAddressArray.AddressSearchType = kAddressSearchType;
      dbSearchAddressArray.UserInputSearchAddress      = userInputSearchAddress;
  }
  
  dbSearchAddressArray.SearchAddressArrayHaveCount = [NSKeyedArchiver archivedDataWithRootObject:[SearchAddressArrayHaveCount copy]];
  dbSearchAddressArray.GoogleAddressPackDict       = googleAddressPackDict;
  [dbSearchAddressArray commit];
  return dbSearchAddressArray;
}
@end