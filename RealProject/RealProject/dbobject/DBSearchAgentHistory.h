//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>

@interface DBSearchAgentHistory : DBObject
//+ (d *)shared;
@property(strong, nonatomic) NSNumber* MemberID;
@property(strong, nonatomic) NSString* MemberName;
@property(strong, nonatomic) NSString* QBID;
@property(strong, nonatomic) NSString* GooglePlaceAutoCompleteAddress;
@property(strong, nonatomic) NSString *PlaceId;
@property(strong, nonatomic) NSData *SearchResultPlaces;
+(DBSearchAgentHistory*)createSearchAgentHistoryWithMemberName:(NSString*)name memberId:(NSNumber*)memberId qbid:(NSString*)qbid address:(NSString*)address placeId:(NSString*)placeId resultPlaces:(NSData *)resultPlace;

@end
