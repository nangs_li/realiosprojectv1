//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBSearchAgentHistory.h"

@implementation DBSearchAgentHistory
@dynamic MemberName, MemberID, QBID, GooglePlaceAutoCompleteAddress,PlaceId;

+(DBSearchAgentHistory*)createSearchAgentHistoryWithMemberName:(NSString*)name memberId:(NSNumber*)memberId qbid:(NSString*)qbid address:(NSString*)address placeId:(NSString*)placeId resultPlaces:(NSData *)resultPlace{
    
    if ([[[DBSearchAgentHistory query]
          whereWithFormat:@" GooglePlaceAutoCompleteAddress = %@", address]count]!=0) {
        [[[[DBSearchAgentHistory query]
           whereWithFormat:@" GooglePlaceAutoCompleteAddress = %@", address]fetch]removeAll];
    }
    
    DBSearchAgentHistory *newHistory = [DBSearchAgentHistory new];
    
    if (!isEmpty(address)&&!isEmpty(placeId)) {
        newHistory.MemberID = memberId;
        newHistory.MemberName = name;
        newHistory.QBID = qbid;
        newHistory.GooglePlaceAutoCompleteAddress = address;
        newHistory.PlaceId = placeId;
        [newHistory commit];
        
    } else {
        NSError *error = [NSError errorWithDomain:@"SearchAgentHistory.invalid" code:404 userInfo:nil];
        NSMutableDictionary *content = [[NSMutableDictionary alloc]init];
        
        if (!isEmpty(placeId)){
            [content setObject:placeId forKey:@"placeId"];
        }
        
        if (!isEmpty(memberId)){
            [content setObject:memberId forKey:@"memberId"];
        }
        
        if ([content valid]) {
            [CrashlyticsKit recordError:error withAdditionalUserInfo:content];
        }
        
    }
    
    return newHistory;
}
@end