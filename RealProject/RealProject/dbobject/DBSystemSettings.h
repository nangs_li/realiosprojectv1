//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>

@interface DBSystemSettings : DBObject
//+ (d *)shared;
@property(strong, nonatomic) NSString* QBID;
@property(assign, nonatomic) int MemberID;
@property(strong, nonatomic) NSDate* Date;
@property(strong, nonatomic) NSData* SystemSettings;
@property(strong, nonatomic) NSData* selectSystemLanguageList;
+(DBSystemSettings*)createSearchAgentHistoryWithChatRoomRelationArray:(NSData*)systemSettings;

-(void)updateDBSystemSettings:(NSData*)systemSettings;
@end
