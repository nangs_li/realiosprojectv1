//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "DBSystemSettings.h"

@implementation DBSystemSettings
@dynamic MemberID, Date, SystemSettings, QBID,selectSystemLanguageList;
-(void)updateDBSystemSettings:(NSData*)systemSettings{
    self.Date = [NSDate date];
    self.SystemSettings = systemSettings;
    self.MemberID = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    [self commit];
}
+(DBSystemSettings*)createSearchAgentHistoryWithChatRoomRelationArray:(NSData*)systemSettings{
    DBSystemSettings *dbsystemsettings = [DBSystemSettings new];
    dbsystemsettings.Date = [NSDate date];
    
    dbsystemsettings.SystemSettings = systemSettings;
    dbsystemsettings.MemberID = [[LoginBySocialNetworkModel shared].myLoginInfo.MemberID intValue];
    [dbsystemsettings commit];
    return self;
}
@end