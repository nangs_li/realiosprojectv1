//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
/**
 *  
 Version 1.0  not     databaseVersion
 Version 1.1  update  dialogPageSearchAgentProfiles to dialogPageSearchAgentProfile
              have    databaseVersion
has install Version 1.1
 */
#import "DBVersion.h"
#import "DBChatroomAgentProfiles.h"
#import "DialogPageAgentProfilesModel.h"
@implementation DBVersion
@dynamic   DataBaseVersion;
+ (DBIndexDefinition*)indexDefinitionForEntity {
  /* create an index definition object */
  DBIndexDefinition* idx = [DBIndexDefinition new];

  /* now specify which properties are going to be indexed */
  [idx addIndexForProperty:@"MemberID"
             propertyOrder:DBIndexSortOrderDescending];

  return idx;
}
+(void)checkVersion{
    DBResultSet * r =[[[DBVersion query] limit:1]fetch];
    if (r.count==0) {
       DBVersion * dbVersion= [DBVersion new];
        dbVersion.DataBaseVersion = CurrentDBVersion;
        [dbVersion commit];
    }
    for (DBVersion * dbVersion in r) {
        if (dbVersion.DataBaseVersion <CurrentDBVersion) {
            /**
             *
             */
        }
      
    }
//    BOOL dbChatroomAgentProfileshaveChatroomAgentProfile =NO;
//    DBResultSet *  dbChatroomAgentProfiles = [[[[DBChatroomAgentProfiles query]
//                             whereWithFormat:
//                             @"MemberID = %@",
//                             [NSString stringWithFormat:@"%@", [LoginBySocialNetworkModel shared]
//                              .myLoginInfo.MemberID]] limit:1]
//                            fetch];
//        for (DBChatroomAgentProfiles *p in dbChatroomAgentProfiles) {
//            if ([p.ChatroomAgentProfile valid]){
//                dbChatroomAgentProfileshaveChatroomAgentProfile=YES;
//            }
//            else{
//                dbChatroomAgentProfileshaveChatroomAgentProfile=NO;
//
//            }
//        }
//    
//    if (r.count==0&&!dbChatroomAgentProfileshaveChatroomAgentProfile) {
//        [[[DBChatroomAgentProfiles query]fetch]removeAll];
//        [[DialogPageAgentProfilesModel shared] AgentProfileGetListsuccess:^(NSMutableArray<AgentProfile> *AgentProfiles) {
//            
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDialogsUpdated object:nil];
//          
//            
//           
//          
//        }
//        failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
//                                                                   
//                                                                  }];
//        
//    }
}
@end