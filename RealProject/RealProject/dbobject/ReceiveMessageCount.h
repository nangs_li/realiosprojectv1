//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>

@interface ReceiveMessageCount : DBObject
//+ (d *)shared;
@property(nonatomic, strong) NSString *DialogID;
@property(nonatomic, strong) NSString *QBID;
@property(nonatomic, strong) NSString *LastMessageText;
@property(nonatomic, assign) int MemberID;
@property(nonatomic, strong) NSDate *LastMessageDate;
@property(nonatomic, assign) int ReceiveMessageCount;

@end
