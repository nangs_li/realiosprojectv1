//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "HandleServerReturnString.h"

@implementation HandleServerReturnString
static HandleServerReturnString *sharedHandleServerReturnString;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (HandleServerReturnString *)shared {
  @synchronized(self) {
    if (!sharedHandleServerReturnString) {
      sharedHandleServerReturnString = [[HandleServerReturnString alloc] init];
    }
    return sharedHandleServerReturnString;
  }
}
- (HandleServerReturnString *)initwithstring:(NSString *)json {
  NSError *err = nil;

  sharedHandleServerReturnString =
      [[HandleServerReturnString alloc] initWithString:json error:&err];
  return sharedHandleServerReturnString;
}

@end