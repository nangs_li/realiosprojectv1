//
//  AddReasonView.h
//  productionreal2
//
//  Created by Alex Hung on 16/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
@class AddReasonView;
@protocol AddReasonViewDelegate <NSObject>

-(void)addReasonView:(AddReasonView*)reasonView didPressAddImageButtonAtIndex:(int)viewIndex;
-(void)addReasonView:(AddReasonView *)reasonView reasonTextDidChangeAtIndex:(int)viewIndex;
-(void)addReasonView:(AddReasonView *)reasonView reasonImageButtonDidPress:(int)viewIndex;
@end



@interface AddReasonView : UIView <HPGrowingTextViewDelegate>

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) PHAsset *imageAsset;
@property (nonatomic,strong) HPGrowingTextView *reasonTextView;
@property (nonatomic,strong) UIImageView *reasonImageView;
@property (nonatomic,strong) UIImageView *reasonImageBorderView;
@property (nonatomic,strong) UIButton *reasonImageButton;
@property (nonatomic,strong) UIButton *addImageButton;
@property (nonatomic,strong) id<AddReasonViewDelegate> delegate;
@property (nonatomic,assign) int itemIndex;
@property (nonatomic,strong) UIImageView *backgroundImageView;
@property (nonatomic,assign) CGRect originalKeyboardFrame;

-(void)configureWithReason:(NSString*)reason withImageAsset:(PHAsset*)imageURL;
@end
