//
//  AddReasonView.m
//  productionreal2
//
//  Created by Alex Hung on 16/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "AddReasonView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "RealUtility.h"
#import "TPKeyboardAvoidingScrollView.h"
@implementation AddReasonView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonSetup];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)commonSetup{
    CGRect contentFrame = self.bounds;
    contentFrame.origin.x = 20;
    contentFrame.size.width -= contentFrame.origin.x*2;
    
    if (!self.addImageButton) {
        self.addImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.addImageButton.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
        [self.addImageButton setTitleColor:[UIColor colorWithRed:213/255.0f green:213/255.0f blue:213/255.0f alpha:1.0] forState:UIControlStateNormal];
        [self.addImageButton.titleLabel setFont:[UIFont systemFontOfSize:13]];
        NSString * supportWithPhotoString=  JMOLocalizedString(@"top_3_reasons__add_photo", nil);
        [self.addImageButton setTitle:supportWithPhotoString forState:UIControlStateNormal];
        [self.addImageButton sizeToFit];
        [self.addImageButton setBackgroundImage:[UIImage imageWithBorder:[UIColor colorWithRed:213/255.0f green:213/255.0f blue:213/255.0f alpha:1.0] size:self.addImageButton.bounds.size] forState:UIControlStateNormal];
        [self.addImageButton addTarget:self action:@selector(addImageButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
        self.addImageButton.hidden = YES;
    }
    
    if (!self.scrollView) {
        CGRect scrollFrame = contentFrame;
        self.originalKeyboardFrame = scrollFrame;
        self.scrollView = [[UIScrollView alloc]initWithFrame:scrollFrame];
        self.scrollView.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:self.reasonTextView];
        [self.scrollView addSubview:self.reasonImageView];
    }
    
    if (!self.backgroundImageView) {
        self.backgroundImageView = [[UIImageView alloc]initWithFrame:contentFrame];
        self.backgroundImageView.backgroundColor = [UIColor colorWithRed:34/255.0f green:34/255.0f blue:34/255.0f alpha:0.55];
    }
    
    if (!self.reasonTextView) {
        CGRect textViewFrame = self.scrollView.bounds;
        self.reasonTextView = [[HPGrowingTextView alloc]initWithFrame:textViewFrame];
        self.reasonTextView.backgroundColor = [UIColor clearColor];
        self.reasonTextView.textColor = [UIColor whiteColor];
        self.reasonTextView.hidden= YES;
        self.reasonTextView.delegate = self;
    }
    
    if (!self.reasonImageView) {
        CGRect imageFrame =  self.scrollView.bounds;
        imageFrame.size.height = imageFrame.size.width/1.5;
        self.reasonImageView = [[UIImageView alloc]initWithFrame:imageFrame];
        self.reasonImageView.backgroundColor =[UIColor clearColor];
        self.reasonImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.reasonImageView.clipsToBounds = YES;
        self.reasonImageView.hidden = YES;
    }
    
    if (!self.reasonImageButton) {
        self.reasonImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.reasonImageButton.frame = self.reasonImageView.frame;
        [self.reasonImageButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:0.7]] forState:UIControlStateHighlighted];
        [self.reasonImageButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:0.7]] forState:UIControlStateSelected];
        [self.reasonImageButton addTarget:self action:@selector(reasonImageButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
        self.reasonImageButton.hidden = YES;
    }
    
    if (!self.reasonImageBorderView) {
        self.reasonImageBorderView = [[UIImageView alloc]initWithFrame:self.reasonImageView.bounds];
        self.reasonImageBorderView.image =[UIImage imageWithBorder:[UIColor whiteColor] size:self.reasonImageView.bounds.size fillcolor:[UIColor clearColor]];
        self.reasonImageBorderView.clipsToBounds = YES;
        self.reasonImageBorderView.hidden = YES;
    }
    
    if (!self.backgroundImageView.superview) {
        [self addSubview:self.backgroundImageView];
    }
    
    if (!self.reasonImageBorderView.superview) {
        [self.reasonImageView addSubview:self.reasonImageBorderView];
    }
    if (!self.reasonTextView.superview) {
        [self.scrollView addSubview:self.reasonTextView];
    }
    
    if (!self.reasonImageView.superview) {
        [self.scrollView addSubview:self.reasonImageView];
    }
    
    if (!self.reasonImageButton.superview) {
        [self.scrollView addSubview:self.reasonImageButton];
    }
    
    
    if (!self.scrollView.superview  ) {
        [self addSubview:self.scrollView];
    }
    
    if (!self.addImageButton.superview) {
        [self addSubview:self.addImageButton];
    }
}


-(void)configureWithReason:(NSString*)reason withImageAsset:(PHAsset*)asset{
    
    if(asset){
        self.imageAsset = asset;
        //        self.reasonImageView.image = image;
        //        NSURL *referenceURL = imageURL
        [RealUtility getImageBy:asset size:CGSizeMake(500, 500) handler:^(UIImage *result, NSDictionary *info) {
            if (result) {
                self.reasonImageView.image =result;
                self.reasonImageView.hidden = NO;
                self.reasonImageButton.hidden = NO;
                [self showAddImageButtonIfNeed];
                [self adjustViewPosition];
            }else{
                self.reasonImageView.hidden = YES;
                self.reasonImageButton.hidden = YES;
                [self showAddImageButtonIfNeed];
                [self adjustViewPosition];
            }
        }];
    }else{
        self.imageAsset = nil;
        self.reasonImageView.image = nil;
        self.reasonImageView.hidden= YES;
        self.reasonImageButton.hidden = YES;
    }
    
    if (reason) {
        self.reasonTextView.text = reason;
        self.reasonTextView.hidden = NO;
    }else{
        self.reasonTextView.text =@"";
        self.reasonTextView.hidden = NO;
    }
    
    [self showAddImageButtonIfNeed];
    
    [self adjustViewPosition];
    
}

-(void)showAddImageButtonIfNeed{
    CGRect scrollFrame = self.bounds;
    scrollFrame.origin.x = 20;
    scrollFrame.size.width -= scrollFrame.origin.x*2;
    
    if (!self.reasonImageView.image) {
        self.reasonImageBorderView.hidden = YES;
        self.addImageButton.hidden = NO;
        scrollFrame.size.height -= self.addImageButton.frame.size.height +16;
    }else{
        scrollFrame.size.height -=8;
        self.reasonImageBorderView.hidden = NO;
        self.addImageButton.hidden = YES;
    }
    self.originalKeyboardFrame = scrollFrame;
    self.scrollView.frame = scrollFrame;
}
-(void)adjustViewPosition{
    
    UIView *mostBottomView = nil;
    CGPoint topViewStartPoint = CGPointMake(16, 16);
    
    if (!self.reasonImageView.hidden) {
        CGRect imageFrame =  self.scrollView.bounds;
        imageFrame.origin = topViewStartPoint;
        imageFrame.size.width = imageFrame.size.width - imageFrame.origin.x *2;
        imageFrame.size.height = imageFrame.size.width/1.5;
        self.reasonImageView.frame = imageFrame;
        self.reasonImageButton.frame = imageFrame;
        mostBottomView = self.reasonImageView;
    }
    
    CGRect reasonTextFrame = self.reasonTextView.bounds;
    reasonTextFrame.origin = topViewStartPoint;
    reasonTextFrame.size.width = self.scrollView.frame.size.width - reasonTextFrame.origin.x*2;
    self.reasonTextView.frame = reasonTextFrame;
    if (mostBottomView) {
        [self.reasonTextView moveViewTo:mostBottomView direction:RelativeDirectionBottom padding:8];
    }

    mostBottomView = self.reasonTextView;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, mostBottomView.frame.size.height +mostBottomView.frame.origin.y);
    [self.addImageButton moveViewTo:self.scrollView direction:RelativeDirectionBottom padding:8];
    [self.addImageButton setViewAlignmentInSuperView:ViewAlignmentRight padding:36];
    
    
}


- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView{
    if ([self.delegate respondsToSelector:@selector(addReasonView:reasonTextDidChangeAtIndex:)]) {
        [self.delegate addReasonView:self reasonTextDidChangeAtIndex:self.itemIndex];
    }
}

-(IBAction)addImageButtonDidPress:(id)sender{
    if([self.delegate respondsToSelector:@selector(addReasonView:didPressAddImageButtonAtIndex:)]){
        [self.delegate addReasonView:self didPressAddImageButtonAtIndex:self.itemIndex];
    }
}

-(IBAction)reasonImageButtonDidPress:(id)sender{
    if([self.delegate respondsToSelector:@selector(addReasonView:reasonImageButtonDidPress:)]){
        [self.delegate addReasonView:self reasonImageButtonDidPress:self.itemIndex];
    }
}

- (void)growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView{
    self.reasonImageButton.enabled = NO;
}
- (void)growingTextViewDidEndEditing:(HPGrowingTextView *)growingTextView{
    self.reasonImageButton.enabled = YES;
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView didChangeHeight:(float)height{
    
}

- (void)scrollToCaretInTextView:(UITextView *)textView animated:(BOOL)animated{
    UITextPosition *selectedTextRangeEnd = textView.selectedTextRange.end;
    CGRect rect = [textView caretRectForPosition:selectedTextRangeEnd];
    UITextPosition *givenCaretPosition = [textView closestPositionToPoint:CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect))];
    if ([textView comparePosition:givenCaretPosition toPosition:selectedTextRangeEnd] == NSOrderedSame)
    {
        rect.size.height += textView.textContainerInset.bottom;
        [textView scrollRectToVisible:rect animated:animated];
    }
}
- (void)keyboardWillShow:(NSNotification*)notification{
    NSDictionary *info = [notification userInfo];
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [self convertRect:keyboardFrame fromView:nil];
    CGRect scrollViewFrame =  self.originalKeyboardFrame;
    scrollViewFrame.size.height -= keyboardFrame.size.height -64;
    self.scrollView.frame = scrollViewFrame;
    [self.addImageButton moveViewTo:self.scrollView direction:RelativeDirectionBottom padding:8];
}

- (void)keyboardWillHide:(NSNotification*)notification{
    self.scrollView.frame = self.originalKeyboardFrame;
    [self.addImageButton moveViewTo:self.scrollView direction:RelativeDirectionBottom padding:8];
}
@end
