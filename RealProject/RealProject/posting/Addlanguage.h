//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "RealSelectTableViewController.h"

@interface Addlanguage : RealSelectTableViewController
@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property (strong, nonatomic) IBOutlet UILabel *addLanguageTitle;


- (IBAction)closebutton:(id)sender;
@end
