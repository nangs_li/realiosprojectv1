//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Addlanguage.h"
#import "SystemLanguageList.h"
#import "SystemSettingModel.h"
#import "Threereason.h"
#import "unique.h"
@interface Addlanguage ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSArray *langTitleArray;
@property (nonatomic,strong) NSArray *langArray;
@property (nonatomic,strong) NSArray *selectedArray;
@property (nonatomic,assign) int selectedLangIndex;
@end
@implementation Addlanguage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.disableSelection = YES;
    self.langArray = [[SystemSettingModel shared].SystemSettings.SystemLanguageList copy];
    self.titleArray =[[[SystemSettingModel shared]getSettingLanguageStringList] copy];
    self.selectedArray = [self getSelectedReasonLang];
    [self configureWithTitles:self.titleArray selection:self.selectedArray indicatorName:nil];
    [self.nextbutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    
}

-(NSArray*)getSelectedReasonLang{
    NSMutableArray *selectedTitleArray = [[NSMutableArray alloc]init];
    NSDictionary *reasonDict = [[AgentListingModel shared].reasonDict copy];
    NSArray *selectedLangIndexArray = [reasonDict.allKeys copy];
    for (NSNumber *indexNumber in selectedLangIndexArray) {
        NSString *title =[[SystemSettingModel shared]getSettingLanguageString:indexNumber];
        [selectedTitleArray addObject:title];
    }
    return selectedTitleArray;
}

-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.addLanguageTitle.text=JMOLocalizedString(@"addLanguage__title", nil);
    [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
}


- (IBAction)closebutton:(id)sender {
    if(self.needCustomBackground){
        self.navigationController.delegate = self;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextbuttonpressed:(id)sender {
    
    unique *uniqueVc = [[unique alloc] initWithNibName:@"unique" bundle:nil];
    uniqueVc.needCustomBackground = self.needCustomBackground;
    uniqueVc.currentLangIndex = @(self.selectedLangIndex);
    uniqueVc.type = AddSloganTypeAdd;
    if(uniqueVc.needCustomBackground){
        self.navigationController.delegate = self;
    }
    uniqueVc.underLayViewController = self.underLayViewController;
    [self.navigationController pushViewController:uniqueVc animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SpokenLanguageList *langList = self.langArray[indexPath.row];
    self.selectedLangIndex = langList.Index;
    //  [tableView reloadData];
    //  [self.nextbutton setBackgroundColor:self.delegate.Greencolor];
    //  self.nextbutton.userInteractionEnabled = YES;
}

@end