//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "Addlanguage.h"
@interface Addthreereason : BaseViewController<UITextViewDelegate>
@property(strong, nonatomic) IBOutlet UITextView *addreasononetextfield;
@property(strong, nonatomic) IBOutlet UITextView *addreasontwotextfield;
@property(strong, nonatomic) IBOutlet UITextView *addreasonthreetextfield;
@property(strong, nonatomic) IBOutlet UILabel *currentlanguage;
@property(strong, nonatomic) IBOutlet UIButton *savebutton;

@property(strong, nonatomic) Addlanguage *previousController;
@end
