//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Addthreereason.h"
#import "SpokenLanguagescell.h"
#import "Finishposting.h"

@interface Addthreereason ()

@end
@implementation Addthreereason

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.savebutton.enabled = YES;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.delegate = [AppDelegate getAppDelegate];
  self.currentlanguage.text =
      [NSString stringWithFormat:@"(%@)", [AgentListingModel shared]
                                              .currentaddnewlanguage];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
  self.addreasononetextfield.delegate = self;
  self.addreasontwotextfield.delegate = self;
  self.addreasonthreetextfield.delegate = self;

  [self.delegate setborder:self.addreasononetextfield width:1.f];
  [self.delegate setborder:self.addreasontwotextfield width:1.f];
  [self.delegate setborder:self.addreasonthreetextfield width:1.f];
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    
   
}

#pragma mark - Button
- (IBAction)closebutton:(id)sender {
  [[AgentListingModel shared].addnewlanguageindex removeLastObject];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionPush;
  transition.subtype = kCATransitionFromBottom;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)savebutton:(id)sender {
  self.savebutton.enabled = NO;

  [[AgentListingModel shared]
          .addnewonereason addObject:self.addreasononetextfield.text];
  [[AgentListingModel shared]
          .addnewtworeason addObject:self.addreasontwotextfield.text];
  [[AgentListingModel shared]
          .addnewthreereason addObject:self.addreasonthreetextfield.text];
  DDLogInfo(@"[AgentListingModel shared].addnewonereason%@",
            [AgentListingModel shared].addnewonereason);
  DDLogInfo(@"[AgentListingModel shared].addnewtworeason%@",
            [AgentListingModel shared].addnewtworeason);
  DDLogInfo(@"[AgentListingModel shared].addnewthreereason%@",
            [AgentListingModel shared].addnewthreereason);

  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionPush;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];
  NSInteger currentIndex =
      [self.navigationController.viewControllers indexOfObject:self];
  if (currentIndex - 2 >= 0) {
    [self.navigationController
        popToViewController:[self.navigationController.viewControllers
                                objectAtIndex:currentIndex - 2]
                   animated:YES];
  }
}

#pragma mark- textview Delegate---------------------------------------------------------------------------------
- (BOOL)textView:(UITextView *)textView
    shouldChangeTextInRange:(NSRange)range
            replacementText:(NSString *)text {
  if ([text isEqualToString:@"\n"]) {
    [textView resignFirstResponder];
    return NO;
  }

  return YES;
}
- (void)textViewDidChange:(UITextView *)textView {
  NSUInteger maxNumberOfLines = 3;
  NSUInteger numLines = textView.contentSize.height / textView.font.lineHeight;
  if (numLines > maxNumberOfLines) {
    textView.text = [textView.text substringToIndex:textView.text.length - 1];
  }

  NSUInteger newLength = [textView.text length];
  DDLogInfo(@"newLength%lu", (unsigned long)newLength);
  if (newLength > 0) {
    [self.savebutton setBackgroundColor:self.delegate.Greencolor];
    self.savebutton.userInteractionEnabled = YES;

  } else {
    [self.savebutton setBackgroundColor:self.delegate.Greycolor];
    self.savebutton.userInteractionEnabled = NO;
  }
}

@end