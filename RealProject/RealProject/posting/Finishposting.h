//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "TDPicker.h"
@interface Finishposting : BaseViewController<TDPickerDelegate>

#pragma mark  perViewPage label
@property(strong, nonatomic) IBOutlet UILabel *perviewnumberofbedroomlabel;
@property(strong, nonatomic) IBOutlet UILabel *perviewnumberofbathroomlabel;
@property(strong, nonatomic) IBOutlet UILabel *perviewtypeofspace;
@property(strong, nonatomic) IBOutlet UILabel *perviewprice;
@property(strong, nonatomic) IBOutlet UILabel *perviewsize;
@property(strong, nonatomic) IBOutlet UIImageView *perviewcoverphoto;
@property(strong, nonatomic) IBOutlet UILabel *perviewunqiue;
@property(strong, nonatomic) IBOutlet UILabel *perviewreason1;
@property(strong, nonatomic) IBOutlet UILabel *perviewreason2;
@property(strong, nonatomic) IBOutlet UILabel *perviewreason3;
@property(strong, nonatomic) IBOutlet UIImageView *personalphoto;
@property(strong, nonatomic) IBOutlet UILabel *membername;
@property(strong, nonatomic) IBOutlet UILabel *followercount;
@property(strong, nonatomic) IBOutlet UIButton *reasonlanguagetype;

@property(strong, nonatomic) IBOutlet TDPicker *reasonlanguagetypepicker;
@property(strong, nonatomic) IBOutlet UIImageView *dropdownimage;
@property(nonatomic, assign) NSInteger reasonlanguagetypeindex;
@property(nonatomic, retain) NSMutableArray *tableData;
@end
