//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Finishposting.h"
#import "Addlanguage.h"
#import "DXAlertView.h"
#import "Finishprofilehavelisting.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AFURLRequestSerialization.h"
#import "AFURLSessionManager.h"
#import "AgentListingModel.h"
@interface Finishposting ()
@end
@implementation Finishposting

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  self.membername.text = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;

  NSURL *url = [NSURL URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];
    [self.personalphoto loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.personalphoto.alpha = 0.0;
        [UIView animateWithDuration:1.0
                         animations:^{
                             self.personalphoto.alpha = 1.0;
                             
                         }];
    }];
  [self.delegate setupcornerRadius:self.personalphoto];

  self.followercount.text = [NSString
      stringWithFormat:@"%d", [MyAgentProfileModel shared].myAgentProfile.FollowerCount];

  self.tableData = [[NSMutableArray alloc]
      initWithObjects:@"Amazing potential", @"The only one", @"Fire sale",
                      @"Super high yield", @"Final call", @"Nothing to lose",
                      @"With good investment potential",
                      @"The only opportunity in market", @"Below market price",
                      @"High return / High yield", @"Last call offer",
                      @"Immediate sale", nil];
  [self.perviewprice setText:[AgentListingModel shared].price];

  DDLogInfo(@"self.delegate.selectmetric-->%@", [AgentListingModel shared].selectmetric);
  self.perviewsize.text =
      [NSString stringWithFormat:@"%@ %@", [AgentListingModel shared].size,
                                 [AgentListingModel shared].selectmetric.NativeName];
  [self.perviewtypeofspace
      setText:[AgentListingModel shared].selectspacetypelist.SpaceTypeName];
  [self.perviewnumberofbathroomlabel setText:[AgentListingModel shared].numberofbathroom];
  [self.perviewnumberofbedroomlabel setText:[AgentListingModel shared].numberofbedroom];

  NSString *reasonone = [[AgentListingModel shared].addnewonereason lastObject];
  NSString *reasontwo = [[AgentListingModel shared].addnewtworeason lastObject];
  NSString *reasothree = [[AgentListingModel shared].addnewthreereason lastObject];

  [self.perviewreason1 setText:reasonone];
  [self.perviewreason2 setText:reasontwo];
  [self.perviewreason3 setText:reasothree];
    
//  [self.perviewunqiue setText:self.tableData[[AgentListingModel shared].unqiueindex]];
  self.perviewcoverphoto.contentMode = UIViewContentModeScaleAspectFill;
  self.perviewcoverphoto.clipsToBounds = YES;

  [self.perviewcoverphoto setImage:[AgentListingModel shared].chosenImages[0]];
  self.reasonlanguagetypepicker.delegate = self;
  self.reasonlanguagetypepicker.pickerData = nil;

  self.reasonlanguagetypepicker.pickerData = [[NSMutableArray alloc] init];
  DDLogInfo(@"[AgentListingModel shared].addnewlanguageindex-->%@",
            [AgentListingModel shared].addnewlanguageindex);
  for (NSString *languageindex in [AgentListingModel shared].addnewlanguageindex) {
    for (SystemLanguageList *systemlanguagelist in [SystemSettingModel shared].SystemSettings
             .SystemLanguageList) {
      if (systemlanguagelist.Index == [languageindex intValue]) {
        DDLogInfo(@"addObject-->%@", systemlanguagelist.NativeName);
        [self.reasonlanguagetypepicker.pickerData
            addObject:systemlanguagelist.NativeName];
      }
    }

    [self.reasonlanguagetype
        setTitle:[self.reasonlanguagetypepicker.pickerData lastObject]
        forState:UIControlStateNormal];
  }

  DDLogInfo(@"self.reasonlanguagetypepicker.pickerData-->%@",
            self.reasonlanguagetypepicker.pickerData);

  [self.reasonlanguagetypepicker.picker reloadAllComponents];
}
- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
  [super viewDidLoad];

}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
}

#pragma mark Button---------------------------------------------------------------------------------------------
//// - btnBackpressed
- (IBAction)btnBackpressed:(id)sender {
  [self btnBackPressed:sender];
}
//// - Xbutton
- (IBAction)xbuttonpressed:(id)sender {
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromBottom;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController popViewControllerAnimated:NO];
}
//// - Publishbutton
- (IBAction)publishbuttonpressed:(id)sender {
  DDLogInfo(@"publishbuttonpressed");

  DXAlertView *alert =
      [[DXAlertView alloc] initWithTitle:JMOLocalizedString(@"alert_alert", nil)
                             contentText:@"Are you sure to pulish posting?"
                         leftButtonTitle:@"Yes"
                        rightButtonTitle:@"Add Language"];
  [alert show];
  alert.leftBlock = ^() {
    if ([self.delegate networkConnection]) {
      [self loadingAndStopLoadingAfterSecond:10];
    }
    DDLogInfo(@"left button clicked");
  
      
      
      [[AgentListingModel shared] callAgentListingAddApisuccess:^(id responseObject) {
          
          [self moveToFinishprofileHaveListingPage];
          
      }failure:^(AFHTTPRequestOperation *operation, NSError *error,
                 NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
          
      }];
      
      
      

  };
  alert.rightBlock = ^() {
    DDLogInfo(@"right button clicked");
    [self morelanguagebutton:self];

  };
  alert.dismissBlock = ^() {
    DDLogInfo(@"Do something interesting after dismiss block");
  };
}
//// - addmorelanguagebutton
- (IBAction)morelanguagebutton:(id)sender {
  Addlanguage *UIViewController =
      [[Addlanguage alloc] initWithNibName:@"Addlanguage" bundle:nil];

  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionPush;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];
  [self.navigationController pushViewController:UIViewController animated:NO];
}


#pragma mark TDPicker Delegate-- ------------------------------------------------------------------------------ -
- (IBAction)pressreasonlanguagetype:(id)sender {
  [self presentSemiModalViewController:self.reasonlanguagetypepicker];
}
- (void)picker:(UIPickerView *)picker
  didSelectRow:(NSInteger)row
   inComponent:(NSInteger)component {
  DDLogInfo(@"self.propertytypepicker.selectpickerdata-->%@",
            self.reasonlanguagetypepicker.pickerData[row]);
}
- (void)pickerSetDate:(TDPicker *)viewController {
  // self.reasonlanguagetypeindex = [viewController.picker
  // selectedRowInComponent:0];

  [self.perviewreason1
      setText:[[AgentListingModel shared].addnewonereason
                  objectAtIndex:[viewController.picker
                                    selectedRowInComponent:0]]];
  [self.perviewreason2
      setText:[[AgentListingModel shared].addnewtworeason
                  objectAtIndex:[viewController.picker
                                    selectedRowInComponent:0]]];
  [self.perviewreason3
      setText:[[AgentListingModel shared].addnewthreereason
                  objectAtIndex:[viewController.picker
                                    selectedRowInComponent:0]]];

  [self.reasonlanguagetype
      setTitle:[self.reasonlanguagetypepicker.pickerData
                   objectAtIndex:[viewController.picker
                                     selectedRowInComponent:0]]
      forState:UIControlStateNormal];

  [self dismissSemiModalViewController:self.reasonlanguagetypepicker];
}

- (void)pickerClearDate:(TDPicker *)viewController {
  [self dismissSemiModalViewController:self.reasonlanguagetypepicker];
}

- (void)pickerCancel:(TDPicker *)viewController {
  [self dismissSemiModalViewController:self.reasonlanguagetypepicker];
}




#pragma mark moveToFinishprofileHaveListingPage
- (void)moveToFinishprofileHaveListingPage {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end