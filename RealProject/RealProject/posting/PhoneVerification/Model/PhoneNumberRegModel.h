//
//  PhoneNumberRegModel.h
//  productionreal2
//
//  Created by Li Ken on 15/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "JSONModel.h"

@interface PhoneNumberRegModel : JSONModel

@property (nonatomic,assign) int RegType;
@property (nonatomic,assign) int PhoneRegID;
//new countryCode and phoneNumber
@property (nonatomic,strong) NSString *countryCode;
@property (nonatomic,strong) NSString *phoneNumber;
@property (nonatomic,strong) NSString *oldCountryCode;
@property (nonatomic,strong) NSString *oldPhoneNumber;

- (NSString *)getStringRegType;
- (NSString *)getStringPhoneRegID;

@end
