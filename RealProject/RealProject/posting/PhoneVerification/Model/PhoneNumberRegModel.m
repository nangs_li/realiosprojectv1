//
//  PhoneNumberRegModel.m
//  productionreal2
//
//  Created by Li Ken on 15/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "PhoneNumberRegModel.h"

@implementation PhoneNumberRegModel

#pragma mark - Override

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    
    return YES;
}

- (NSString *)getStringRegType {
    
    if (self.RegType) {
        
        return  [NSString stringWithFormat:@"%d" ,self.RegType];
        
    } else {
        
        return nil;
        
    }
    
}

- (NSString *)getStringPhoneRegID {
    
    if (self.PhoneRegID) {
        
        return  [NSString stringWithFormat:@"%d" ,self.PhoneRegID];
        
    } else {
        
        return nil;
        
    }
    
}

@end
