//
//  PhoneNumberRegViewController.h
//  productionreal2
//
//  Created by Li Ken on 2/6/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"
#import "PhoneNumberViewController.h"

@class PhoneNumberRegView;

@interface PhoneNumberRegView : BaseView

@property (strong, nonatomic, readonly) IBOutlet UITextField * _Nullable countryCodeTextField;
@property (strong, nonatomic, readonly) IBOutlet UITextField * _Nullable phoneNumberTextField;
@property (strong, nonatomic, readonly) IBOutlet UIImageView * _Nullable phoneVerifyImage;
@property (strong, nonatomic) IBOutlet UIButton *reportProblemButton;

- (nonnull instancetype)initWithType:(PhoneNumberControllerType)type;

@end
