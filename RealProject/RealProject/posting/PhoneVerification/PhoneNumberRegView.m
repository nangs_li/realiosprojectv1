//
//  PhoneNumberRegViewController.m
//  productionreal2
//
//  Created by Li Ken on 2/6/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import "PhoneNumberRegView.h"
#import "RealApiClient.h"
#import "PhoneNumberSMSVerifyView.h"

@interface PhoneNumberRegView ()

@property (strong, nonatomic) UIToolbar *keyboardToolbar;
@property (strong, nonatomic) UIPickerView *countryCodePickerView;
@property (strong, nonatomic) NSMutableArray *countryCodeArray;
@property (strong, nonatomic) NSString *countryCode;
@property (strong, nonatomic) NSString *phoneNumber;
@property (assign, nonatomic) PhoneNumberControllerType type;
@property (strong, nonatomic) IBOutlet UITextField *countryCodeTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumberDesc1;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumberDesc2;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumberDesc3;
@property (strong, nonatomic) IBOutlet UIImageView *phoneVerifyImage;

@end

@implementation PhoneNumberRegView

#pragma mark - Initialization

- (instancetype)initWithType:(PhoneNumberControllerType)type
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"PhoneNumberRegView" owner:self options:nil] objectAtIndex:0];
    
    if(self) {
        self.type = type;
        [self setupView];
        
    }
    
    return self;
}

#pragma mark - Setup
- (void)setupView {
    
    [self setupCountryCodeTextField];
    [self.phoneNumberTextField setUpPhoneNumberType];
    [self setupPhoneNumberTextFieldKeyboard];
    [self.phoneNumberDesc1 setTextColor:[UIColor whiteColor]];
    [self.phoneNumberDesc1 setFont:[UIFont systemFontOfSize:25.0]];
    [self.phoneNumberDesc2 setTextColor:[UIColor whiteColor]];
    [self.phoneNumberDesc2 setUpBoldTitleFontSize];
    [self.phoneNumberDesc3 setUpSubTitleFontSize];
    
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         
         [self setUpFixedLabelTextAccordingToSelectedLanguage];
         
     }];
    
    [self setUpFixedLabelTextAccordingToSelectedLanguage];
    
}

- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"phone1__logging_problem", nil)];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor lightTextColor] range:NSMakeRange(0, attributeString.length)];
    [self.reportProblemButton setAttributedTitle:attributeString forState:UIControlStateNormal];
    self.phoneNumberDesc1.text      = JMOLocalizedString(@"phone1__desc1", nil);
    
    if (self.type == PhoneNumberControllerTypeReg) {
        
        self.phoneNumberDesc2.text      = JMOLocalizedString(@"phone1__desc2", nil);
        
    } else {
        
        self.phoneNumberDesc2.text      = JMOLocalizedString(@"phone1__desc2_mirgation", nil);
        
    }
    
    self.phoneNumberDesc3.text      = JMOLocalizedString(@"phone1__desc3", nil);
    
}

- (void)setupCountryCodeTextField {
    
    [_countryCodeTextField setTextAlignment:NSTextAlignmentRight];
    [_countryCodeTextField setTextColor:[UIColor white]];
    [_countryCodeTextField setUpTitleFontSize];
    
    _countryCodeTextField.layer.borderColor = [UIColor white].CGColor;
    _countryCodeTextField.layer.borderWidth = 1.0;
    
    UIImage *pulldownImage = [UIImage imageNamed:@"btn_pop_pulldown_small"];
    UIImageView *pulldownImageView =[[UIImageView alloc] initWithImage:pulldownImage];
    CGRect frame = pulldownImageView.frame;
    frame.size.width += 20.0;
    pulldownImageView.frame = frame;
    pulldownImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    _countryCodeTextField.rightViewMode = UITextFieldViewModeAlways;
    _countryCodeTextField.rightView = pulldownImageView;
    
}

- (void)setupPhoneNumberTextFieldKeyboard {
    
    _phoneNumberTextField.keyboardType = UIKeyboardTypePhonePad;
    _phoneNumberTextField.inputAccessoryView = _keyboardToolbar;
    [_phoneNumberTextField setUpTitleFontSize];
    
}

@end
