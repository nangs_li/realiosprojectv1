//
//  PhoneNumberSMSVerifyViewController.h
//  productionreal2
//
//  Created by Li Ken on 2/6/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"

@class PhoneNumberSMSVerifyView;

@interface PhoneNumberSMSVerifyView : BaseView

@property (strong, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (strong, nonatomic) IBOutlet UITextField * phoneNumberTextField1;
@property (strong, nonatomic) IBOutlet UITextField * phoneNumberTextField2;
@property (strong, nonatomic) IBOutlet UITextField * phoneNumberTextField3;
@property (strong, nonatomic) IBOutlet UITextField * phoneNumberTextField4;

- (instancetype)initWithPhoneNumberSMSVerify;
- (void)setUpPhoneNumberTextFieldInputAccessoryView:(nonnull UIView *)inputAccessoryView;

@end
