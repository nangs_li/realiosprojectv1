//
//  PhoneNumberSMSVerifyViewController.m
//  productionreal2
//
//  Created by Li Ken on 2/6/2016.
//  Copyright © 2016年 Real. All rights reserved.
//
#import "PhoneNumberSMSVerifyView.h"


@interface PhoneNumberSMSVerifyView ()

@property (strong, nonatomic) IBOutlet UILabel *phoneNumberDesc1;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumberDesc2;

@end

@implementation PhoneNumberSMSVerifyView

#pragma mark - Initialization

- (instancetype)initWithPhoneNumberSMSVerify {
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"PhoneNumberSMSVerifyView" owner:self options:nil] objectAtIndex:0];
    
    if (self) {
        
        [self setupView];
        
    }
    
    return self;
}

#pragma mark - Setup

- (void)setupView {
    
    [self.phoneNumberLabel setFont:[UIFont systemFontOfSize:28.0]];
    [self.phoneNumberDesc1 setUpBoldTitleFontSize];
    [self.phoneNumberDesc1 setTextColor:[UIColor whiteColor]];
    [self.phoneNumberDesc2 setUpSubTitleFontSize];
    [self.phoneNumberTextField1 setUpBigPhoneNumberType];
    [self.phoneNumberTextField2 setUpBigPhoneNumberType];
    [self.phoneNumberTextField3 setUpBigPhoneNumberType];
    [self.phoneNumberTextField4 setUpBigPhoneNumberType];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationSetUpFixedLabelTextAccordingToSelectedLanguage object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         
         [self setUpFixedLabelTextAccordingToSelectedLanguage];
         
     }];
    [self setUpFixedLabelTextAccordingToSelectedLanguage];
}

- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    
    self.phoneNumberDesc1.text      = JMOLocalizedString(@"phone2_desc1", nil);
    self.phoneNumberDesc2.text      = JMOLocalizedString(@"phone2_desc2", nil);
    
    
}

- (void)setUpPhoneNumberTextFieldInputAccessoryView:(nonnull UIView *)inputAccessoryView {
    
    _phoneNumberTextField1.inputAccessoryView = inputAccessoryView;
    _phoneNumberTextField2.inputAccessoryView = inputAccessoryView;
    _phoneNumberTextField3.inputAccessoryView = inputAccessoryView;
    _phoneNumberTextField4.inputAccessoryView = inputAccessoryView;
    
}

@end
