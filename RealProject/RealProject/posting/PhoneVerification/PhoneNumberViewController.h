//
//  PhoneNumberViewController.h
//  productionreal2
//
//  Created by Li Ken on 3/6/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ResendButtonDisabledTimer.h"

typedef NS_ENUM (NSInteger, PhoneNumberControllerType) {
    PhoneNumberControllerTypeReg,
    PhoneNumberControllerTypeSMSVerifyView,
    PhoneNumberControllerTypeRegFromPhoneNumberMigrate,
    PhoneNumberControllerTypeSMSVerifyViewFromPhoneNumberMigrate,
    PhoneNumberControllerTypeSMSVerifyViewSettingPage
};

@interface PhoneNumberViewController : BaseViewController

- (instancetype)initWithType:(PhoneNumberControllerType)type;
- (instancetype)initWithType:(PhoneNumberControllerType)type phoneNumberRegModel:(PhoneNumberRegModel*)phoneNumberRegModel resentButtonDisabledTimer:(ResendButtonDisabledTimer *)resentButtonDisabledTimer;

@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *resendButton;
@property (strong, nonatomic) IBOutlet UIButton *verifyButton;

//phoneNumber
@property (nonatomic,strong) PhoneNumberRegModel * phoneNumberRegModel;
@property (nonatomic,strong) NSString *pinInput;

@end