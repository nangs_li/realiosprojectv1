//
//  PhoneNumberViewController.m
//  productionreal2
//
//  Created by Li Ken on 3/6/2016.
//  Copyright © 2016年 Real. All rights reserved.
//

#import "PhoneNumberViewController.h"
#import "PhoneNumberRegView.h"
#import "PhoneNumberSMSVerifyView.h"
#import "BEMCheckBox.h"
#import "LoginBySocialNetworkModel.h"
#import "RealSignUpDetalViewController.h"
#import "LoginByEmailModel.h"
#import "mixpanel.h"
#import "ResendButtonDisabledTimer.h"

@interface PhoneNumberViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, assign) PhoneNumberControllerType type;
//phoneNumberRegView
@property (nonatomic, strong) PhoneNumberRegView *phoneNumberRegView;
@property (nonatomic, strong) UIPickerView *countryCodePickerView;
@property (nonatomic, strong) NSMutableArray *countryCodeArray;
@property (nonatomic, strong) UIToolbar *keyboardToolbar;
// phoneNumberSMSVerifyView
@property (nonatomic, strong) PhoneNumberSMSVerifyView *phoneNumberSMSVerifyView;
@property (nonatomic, strong) ResendButtonDisabledTimer *resentButtonDisabledTimer;

@end

@implementation PhoneNumberViewController

#pragma mark - Initialization

- (instancetype)initWithType:(PhoneNumberControllerType)type
{
    
    
    if (type == PhoneNumberControllerTypeSMSVerifyViewSettingPage) {
        
        self = [self initWithNibName:@"PhoneNumberViewControllerSettingPage" bundle:nil];
        
        _type = type;
        self.automaticallyAdjustsScrollViewInsets = NO;
        
    } else {
        
        if (self = [self init]) {
            
            _type = type;
            _phoneNumberRegModel = [[PhoneNumberRegModel alloc]init];
            self.automaticallyAdjustsScrollViewInsets = NO;
            
        }
        
    }
    
    return self;
}

- (instancetype)initWithType:(PhoneNumberControllerType)type phoneNumberRegModel:(PhoneNumberRegModel*)phoneNumberRegModel resentButtonDisabledTimer:(ResendButtonDisabledTimer *)resentButtonDisabledTimer {
    
    if (self = [self initWithType:type]) {
        
        self.phoneNumberRegModel = phoneNumberRegModel;
        self.resentButtonDisabledTimer = resentButtonDisabledTimer;
        
    }
    
    return self;
}

#pragma mark - Setup

- (void)setupView {
    
    if (self.type == PhoneNumberControllerTypeReg || self.type == PhoneNumberControllerTypeRegFromPhoneNumberMigrate) {
        
        _phoneNumberRegView = [[PhoneNumberRegView alloc] initWithType:self.type];
        [_scrollView addSubview:  _phoneNumberRegView];
        
        //setup autolayout
        [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(0);
            
            
        }];
        
        
        
        [_phoneNumberRegView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.center.equalTo(self.scrollView).centerOffset(CGPointMake(0, 42));
            make.leading.equalTo(self.verifyButton);
            make.trailing.equalTo(self.verifyButton);
            
        }];
        
        [self setupVeifyBtn];
        [self setupPhoneNumberRegViewButtonAction];
        [self checkButtonEnable];
 
    }
    
    if (self.type == PhoneNumberControllerTypeSMSVerifyView || self.type == PhoneNumberControllerTypeSMSVerifyViewSettingPage || self.type == PhoneNumberControllerTypeSMSVerifyViewFromPhoneNumberMigrate) {
        
        _phoneNumberSMSVerifyView = [[PhoneNumberSMSVerifyView alloc] initWithPhoneNumberSMSVerify];
        
        [_scrollView addSubview:  _phoneNumberSMSVerifyView];
        
        [self setupReSendBtn];
        [self setupPhoneNumberLabel];
        [self.titleLabel setUpBoldTitleFontSize];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        
        //setup autolayout
        
        if (self.type == PhoneNumberControllerTypeSMSVerifyViewSettingPage) {
            
            [_phoneNumberSMSVerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.scrollView).centerOffset(CGPointMake(0, 24));
                
            }];
            
        } else {
            
            [_phoneNumberSMSVerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.scrollView).centerOffset(CGPointMake(0, 0));
                
            }];
            
        }
        
        [_phoneNumberSMSVerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.leading.equalTo(self.resendButton);
            make.trailing.equalTo(self.resendButton);
            
        }];
        
        [self setupKeyboardToolbar];
        [self.phoneNumberSMSVerifyView setUpPhoneNumberTextFieldInputAccessoryView:_keyboardToolbar];
        [self setupPhoneNumberSMSVerifyViewButtonAction];
        
    }
    
    [self setupViewShowOrHide];
    [self setupKeyboardToolbar];
    [self setupCountryCodePickerView];
    [self setupPhoneNumberTextFieldKeyboard];
    //setupView Action
    [self setUpFixedLabelTextAccordingToSelectedLanguage];
    [self setupNotificationResendButtonDisableAction];
    
}

- (void)setupKeyboardToolbar {
    
    _keyboardToolbar = [[UIToolbar alloc] init];
    [_keyboardToolbar sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:JMOLocalizedString(Done, nil)
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneButtonPressed:)];
    
    [_keyboardToolbar setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
}

- (void)setupCountryCodePickerView {
    
    __weak typeof(self) weakSelf = self;
    
    _countryCodePickerView = [[UIPickerView alloc] init];
    _countryCodePickerView.delegate = self;
    _countryCodePickerView.dataSource = self;
    
    _phoneNumberRegView.countryCodeTextField.inputView = _countryCodePickerView;
    _phoneNumberRegView.countryCodeTextField.inputAccessoryView = _keyboardToolbar;
    
    if ([[[SystemSettingModel shared]getNotDuplicateSMSCountryCodeList] valid]) {
        
        _countryCodeArray = [[SystemSettingModel shared]getNotDuplicateSMSCountryCodeList];
        
        [weakSelf prefillCountryCode];
        
    } else {
        dispatch_time_t popTime =
        dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            
            [[RealApiClient sharedClient] getCountryCodeBlock:^(id response, NSError *error) {
                
                if ([weakSelf notError:error view:self.view]) {
                    
                    _countryCodeArray = [[SystemSettingModel shared]getNotDuplicateSMSCountryCodeList];
                    
                    [weakSelf prefillCountryCode];
                    
                }
                
            }];
        });
        
    }
    
    
    
}

- (void)setupPhoneNumberTextFieldKeyboard {
    
    self.phoneNumberRegView.phoneNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneNumberRegView.phoneNumberTextField.inputAccessoryView = _keyboardToolbar;
    
}
- (void)prefillCountryCode {
    
    int selectedIndex= [[SystemSettingModel shared] getIndexFromNotDuplicateSMSCountryCodeListAccordingToCountry];
    
    if (selectedIndex == NSNotFound) {
        
        selectedIndex = 0;
    }
    
    if ([_countryCodeArray valid]) {
        
        [_countryCodePickerView selectRow:selectedIndex inComponent:0 animated:NO];
        [self updateCountryCode:_countryCodeArray[selectedIndex]];
        
    }
    
}

- (void)updateCountryCode:(NSString*)countryCode {
    
    self.phoneNumberRegView.countryCodeTextField.text = [NSString stringWithFormat:@"+%@", countryCode];
    
}

- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    
    [self.verifyButton setTitle:JMOLocalizedString(@"phone1__button", nil) forState:UIControlStateNormal];
    [self.resendButton setTitle:JMOLocalizedString(@"phone2__button", nil) forState:UIControlStateNormal];
    self.titleLabel.text            = JMOLocalizedString(@"phone2__title", nil);
    
}

- (void)setupVeifyBtn {
    
    [self.verifyButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self.verifyButton setBackgroundImage:[UIImage imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateDisabled];
    [self.verifyButton setTitleColor:[UIColor white] forState:UIControlStateNormal];
    
}

- (void)setupReSendBtn {
    
    [self.resendButton setBackgroundImage:[UIImage imageWithColor:[UIColor realBlueColor]] forState:UIControlStateNormal];
    [self.resendButton setBackgroundImage:[UIImage imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateDisabled];
    [self.resendButton setTitleColor:[UIColor white] forState:UIControlStateNormal];
    
}

- (void)setupPhoneNumberLabel{
    
    _phoneNumberSMSVerifyView.phoneNumberLabel.text = [NSString stringWithFormat:@"%@  %@" ,self.countryCode ,self.phoneNumber];
    
}

- (void)setupNotificationResendButtonDisableAction {
    
    __weak typeof(self) weakSelf = self;
    
    weakSelf.resentButtonDisabledTimer = [ResendButtonDisabledTimer shared];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationResendButtonChangeUI object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         
         NSNotification * notification = (NSNotification *) x;
         NSString *_resendButtonDisabledTime =  [notification.userInfo objectForKey:@"resendButtonDisabledTime"];
         NSString * resendButtonTitle = [NSString stringWithFormat:@"%@  %@", JMOLocalizedString(@"phone2__button", nil), [self setupResentButtonTitleFromTotalSeconds:[_resendButtonDisabledTime intValue]]];
         [weakSelf.resendButton setTitle:resendButtonTitle forState:UIControlStateNormal];
         [weakSelf.resendButton setEnabled:NO];
         
     }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationResendButtonEnable object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         
         [weakSelf.resendButton setEnabled:YES];
         [weakSelf.resendButton setTitle:JMOLocalizedString(@"phone2__button", nil) forState:UIControlStateNormal];
         
     }];
    
}

- (NSString *)setupResentButtonTitleFromTotalSeconds:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    
    
    return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
}

- (void)setupUpNavBar
{
    [[self.delegate getTabBarController] changeNavBarType:NavigationBarSearch
                                                 animated:YES
                                                    block:^(BOOL didChange, NavigationBarType currentType, CustomNavigationBarController *navBarController) {
                                                        
                                                        if (didChange) {
                                                            
                                                            self.navBarContentView = navBarController.settingNavBar;
                                                            navBarController.settinTitleLabel.text = JMOLocalizedString(@"setting__chats", nil);
                                                            [navBarController.settingBackButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
                                                            [navBarController.settingBackButton addTarget:self action:@selector(closeButtonPressd:) forControlEvents:UIControlEventTouchUpInside];
                                                            navBarController.settingBackButton.hidden = NO;
                                                            
                                                        }
                                                        
                                                    }];
    
    self.navBarContentView.alpha = 1.0;
    
}

- (void)setupViewShowOrHide {
    
    if (self.type == PhoneNumberControllerTypeSMSVerifyViewSettingPage) {
        
        [self setupUpNavBar];
        
    }
    
    if (self.type == PhoneNumberControllerTypeReg || self.type == PhoneNumberControllerTypeRegFromPhoneNumberMigrate) {
        
        [self showPhoneNumberRegView:YES];
        [self showPhoneNumberSMSVerifyView:NO];
        
    }
    
    if (self.type == PhoneNumberControllerTypeSMSVerifyView || self.type == PhoneNumberControllerTypeSMSVerifyViewSettingPage || self.type == PhoneNumberControllerTypeSMSVerifyViewFromPhoneNumberMigrate) {
        
        [self showPhoneNumberSMSVerifyView:YES];
        [self showPhoneNumberRegView:NO];
        
    }
    
}
#pragma mark - IOS life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setupViewShowOrHide];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self setViewEndEditing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Getter

- (UITextField *)phoneNumberTextField1 {
    
    return _phoneNumberSMSVerifyView.phoneNumberTextField1;
}

- (UITextField *)phoneNumberTextField2 {
    
    return _phoneNumberSMSVerifyView.phoneNumberTextField2;
}

- (UITextField *)phoneNumberTextField3 {
    
    return _phoneNumberSMSVerifyView.phoneNumberTextField3;
}

- (UITextField *)phoneNumberTextField4 {
    
    return _phoneNumberSMSVerifyView.phoneNumberTextField4;
}

- (NSString *)countryCode {
    
    return self.phoneNumberRegModel.countryCode;
    
}

- (NSString *)phoneNumber {
    
    return self.phoneNumberRegModel.phoneNumber;
    
}

#pragma mark - Actions

- (void)setupPhoneNumberRegViewButtonAction {
    
    __weak typeof(self) weakSelf = self;
    
    //verify Button
    [[self.verifyButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        [weakSelf setViewEndEditing];
        
        if ([self.phoneNumber isEqualToString:self.phoneNumberRegView.phoneNumberTextField.text]&&[self.countryCode isEqualToString:self.phoneNumberRegView.countryCodeTextField.text]&&self.phoneNumber) {
            
            PhoneNumberControllerType type;
            
            if (self.type == PhoneNumberControllerTypeReg) {
                
                type = PhoneNumberControllerTypeSMSVerifyView;
                
            } else {
                
                type = PhoneNumberControllerTypeSMSVerifyViewFromPhoneNumberMigrate;
                
            }
            
            PhoneNumberViewController * phoneNumberViewController = [[PhoneNumberViewController alloc] initWithType:type phoneNumberRegModel:self.phoneNumberRegModel resentButtonDisabledTimer:self.resentButtonDisabledTimer];
            phoneNumberViewController.underLayViewController = self.underLayViewController;
            self.navigationController.delegate = self;
            [self.navigationController pushViewController:phoneNumberViewController animated:YES];
            
        } else {
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:JMOLocalizedString(@"alert_ok", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [weakSelf callSendSMSMethod];
                                     
                                 }];
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:JMOLocalizedString(@"chatlobby__edit", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         
                                     }];
            
            NSString *phoneNumberText = [NSString stringWithFormat:@"%@ %@" ,_phoneNumberRegView.countryCodeTextField.text ,_phoneNumberRegView.phoneNumberTextField.text];
            NSString * title = [NSString stringWithFormat:@"%@\n\n%@\n\n%@" ,JMOLocalizedString(@"phone1__number_confirmation", nil) ,phoneNumberText ,JMOLocalizedString(@"phone1__phone_correct_question", nil)];
            [weakSelf showAlertWithTitle:title message:nil withActions:@[cancel,ok]];
            
        }
        
    }];
    
    //phoneNumberTextField
    [[self.phoneNumberRegView.phoneNumberTextField rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(id x) {
        
        [weakSelf checkButtonEnable];
        
    }];
    
    //reportProblemButton
    [[self.phoneNumberRegView.reportProblemButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        NSString *url = [@"mailto:cs@real.co" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
        [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
        
        
    }];
    
}

- (void)pushToSMSVerifyViewControllerWithType:(PhoneNumberControllerType)type {
    
    self.phoneNumberRegModel.countryCode = _phoneNumberRegView.countryCodeTextField.text;
    self.phoneNumberRegModel.phoneNumber = _phoneNumberRegView.phoneNumberTextField.text;
    
    PhoneNumberViewController * phoneNumberViewController = [[PhoneNumberViewController alloc] initWithType:type phoneNumberRegModel:self.phoneNumberRegModel resentButtonDisabledTimer:self.resentButtonDisabledTimer];
    phoneNumberViewController.underLayViewController = self.underLayViewController;
    self.navigationController.delegate = self;
    [self.resentButtonDisabledTimer callResendButtonDisabledTimer];
    [self.navigationController pushViewController:phoneNumberViewController animated:YES];
    
}

//keyboard done button
- (void)setupPhoneNumberSMSVerifyViewButtonAction {
    
    __weak typeof(self) weakSelf = self;
    
    //back Button
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        [weakSelf.navigationController popViewControllerAnimated:YES];
        
    }];
    
    //resend Button
    [[self.resendButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        [self.resentButtonDisabledTimer callResendButtonDisabledTimer];
        
        [weakSelf checkButtonEnable];
        
        [self callSendSMSMethod];
        
    }];
    
    //phoneNumberTextField1
    [[self.phoneNumberTextField1 rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(id x) {
        
        if (!isEmpty(self.phoneNumberTextField1.text)) {
            
            if (self.phoneNumberTextField1.text.length > 1) {
                
                self.phoneNumberTextField1.text = [self.phoneNumberTextField1.text substringToIndex:1];
                
            }
            
            [self.phoneNumberTextField2 becomeFirstResponder];
            
        }
        
        [weakSelf callVerifyPhoneNumberSMS];
        
    }];
    
    //phoneNumberTextField2
    [[self.phoneNumberTextField2 rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(id x) {
        
        if (!isEmpty(self.phoneNumberTextField2.text)) {
            
            if (self.phoneNumberTextField2.text.length > 1) {
                
                self.phoneNumberTextField2.text = [self.phoneNumberTextField2.text substringToIndex:1];
                
            }
            
            [self.phoneNumberTextField3 becomeFirstResponder];
            
        } else {
            
            [self.phoneNumberTextField1 becomeFirstResponder];
        }
        
        [weakSelf callVerifyPhoneNumberSMS];
        
    }];
    
    //phoneNumberTextField3
    [[self.phoneNumberTextField3 rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(id x) {
        
        if (!isEmpty(self.phoneNumberTextField3.text)) {
            
            if (self.phoneNumberTextField3.text.length > 1) {
                
                self.phoneNumberTextField3.text = [self.phoneNumberTextField3.text substringToIndex:1];
                
            }
            
            [self.phoneNumberTextField4 becomeFirstResponder];
            
        } else {
            
            [self.phoneNumberTextField2 becomeFirstResponder];
        }
        
        [weakSelf callVerifyPhoneNumberSMS];
        
    }];
    
    //phoneNumberTextField4
    [[self.phoneNumberTextField4 rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(id x) {
        
        if (!isEmpty(self.phoneNumberTextField4.text)) {
            
            if (self.phoneNumberTextField4.text.length > 1) {
                
                self.phoneNumberTextField4.text = [self.phoneNumberTextField4.text substringToIndex:1];
                
            }
            
        } else {
            
            [self.phoneNumberTextField3 becomeFirstResponder];
            
        }
        
        [weakSelf callVerifyPhoneNumberSMS];
        
    }];
    
}

//keyboard done button
- (void)doneButtonPressed:(UIBarButtonItem *)sender {
    
    [self setViewEndEditing];
    
}

- (void)closeButtonPressd:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma mark - Support Method

- (void)checkButtonEnable {
    
    if (self.phoneNumberRegView.phoneNumberTextField.text.length > 4 && self.phoneNumberRegView.phoneNumberTextField.text.length < 23) {
        
        [self.verifyButton setEnabled:YES];
        
    } else {
        
        [self.verifyButton setEnabled:NO];
        
    }
    
}

- (void)showPhoneNumberRegView:(BOOL)show{
    
    [self.phoneNumberRegView setHidden:!show];
    [self.verifyButton setHidden:!show];
    
}

- (void)showPhoneNumberSMSVerifyView:(BOOL)show{
    
    [self.backButton setHidden:!show];
    [self.titleLabel setHidden:!show];
    [self.phoneNumberSMSVerifyView setHidden:!show];
    [self.resendButton setHidden:!show];
    
}
- (void)callSendSMSMethod {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.type == PhoneNumberControllerTypeReg || self.type == PhoneNumberControllerTypeRegFromPhoneNumberMigrate) {
        
        [self showLoadingHUDWithTitleText:JMOLocalizedString(@"alert__checking", nil) titleTextFont: [UIFont systemFontOfSize:28.0] titleTextColor:nil detailText:[NSString stringWithFormat:@"%@ %@" ,_phoneNumberRegView.countryCodeTextField.text ,_phoneNumberRegView.phoneNumberTextField.text] detailTextFont: [UIFont systemFontOfSize:28.0] detailTextColor:RealPhoneNumberBlueColor windowColor:[UIColor clearColor]];
        [self showPhoneNumberRegView:NO];
        
    }
    
    if (self.type == PhoneNumberControllerTypeReg || self.type == PhoneNumberControllerTypeSMSVerifyView) {
        
        [[RealApiClient sharedClient] registerphoneNumberFromCountryCode:_phoneNumberRegView.countryCodeTextField.text phoneNumber:_phoneNumberRegView.phoneNumberTextField.text   block:^(PhoneNumberRegModel *response, NSError *error2) {
            
            if (weakSelf.type == PhoneNumberControllerTypeReg) {
                
                [weakSelf hideLoadingHUD];
                [weakSelf setupViewShowOrHide];
            }
            
            
            if ([weakSelf notError:error2 view:self.view]) {
                
                weakSelf.phoneNumberRegModel.RegType = response.RegType;
                weakSelf.phoneNumberRegModel.PhoneRegID = response.PhoneRegID;
                
                if (weakSelf.type == PhoneNumberControllerTypeReg) {
                    
                    [weakSelf pushToSMSVerifyViewControllerWithType:PhoneNumberControllerTypeSMSVerifyView];
                    
                }
                
                
                
            }
            
        }];
    }
    
    if (self.type == PhoneNumberControllerTypeRegFromPhoneNumberMigrate || self.type == PhoneNumberControllerTypeSMSVerifyViewFromPhoneNumberMigrate) {
        
        [[RealApiClient sharedClient] migratePhoneNumberFromCountryCode:_phoneNumberRegView.countryCodeTextField.text phoneNumber:_phoneNumberRegView.phoneNumberTextField.text   block:^(PhoneNumberRegModel *response, NSError *error2) {
            
            if (weakSelf.type == PhoneNumberControllerTypeRegFromPhoneNumberMigrate) {
                
                [weakSelf hideLoadingHUD];
                [weakSelf setupViewShowOrHide];
            }
            
            if ([weakSelf notError:error2 view:self.view]) {
                
                weakSelf.phoneNumberRegModel.RegType = response.RegType;
                weakSelf.phoneNumberRegModel.PhoneRegID = response.PhoneRegID;
                
                if (weakSelf.type == PhoneNumberControllerTypeRegFromPhoneNumberMigrate) {
                    
                    [weakSelf pushToSMSVerifyViewControllerWithType:PhoneNumberControllerTypeSMSVerifyViewFromPhoneNumberMigrate];
                    
                }
                
            }
            
        }];
    }
    
    if (self.type == PhoneNumberControllerTypeSMSVerifyViewSettingPage) {
        
        NSString *oldCountryCode =  self.phoneNumberRegModel.oldCountryCode;
        NSString *oldNumber = self.phoneNumberRegModel.oldPhoneNumber;
        NSString *changeCountryCode = self.phoneNumberRegModel.countryCode;
        NSString *changeNumber = self.phoneNumberRegModel.phoneNumber;
        NSString *lang = [LanguagesManager sharedInstance].currentLanguage;
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:oldCountryCode forKey:@"CountryCodeOld"];
        [dict setObject:oldNumber forKey:@"PhoneNumberOld"];
        [dict setObject:changeNumber forKey:@"PhoneNumber"];
        [dict setObject:changeCountryCode forKey:@"CountryCode"];
        [dict setObject:lang forKey:@"Lang"];
        
        [weakSelf showLoading];
        
        [[RealApiClient sharedClient]changePhoneNumberWithDict:dict completion:^(PhoneNumberRegModel *response, NSError *error) {
            
            if ([weakSelf notError:error view:self.view]) {
                weakSelf.phoneNumberRegModel.PhoneRegID = response.PhoneRegID;
                [weakSelf.resentButtonDisabledTimer callResendButtonDisabledTimer];
                
            }
            
            [weakSelf hideLoading];
            
        }];
    }
    
    
    
}

- (void)callVerifyPhoneNumberSMS {
    
    __weak typeof(self) weakSelf = self;
    
    if (isEmpty(self.phoneNumberSMSVerifyView.phoneNumberTextField1.text)||isEmpty(self.phoneNumberTextField2.text)||isEmpty(self.phoneNumberTextField3.text)||isEmpty(self.phoneNumberTextField4.text)) {
        
    } else {
        
        [weakSelf setViewEndEditing];
        
        NSString *phoneNumberText = [NSString stringWithFormat:@"%@%@%@%@" ,self.phoneNumberTextField1.text ,self.phoneNumberTextField2.text ,self.phoneNumberTextField3.text ,self.phoneNumberTextField4.text];
        self.phoneNumberTextField1.text =nil;
        self.phoneNumberTextField2.text =nil;
        self.phoneNumberTextField3.text =nil;
        self.phoneNumberTextField4.text =nil;
        
        if (self.type == PhoneNumberControllerTypeSMSVerifyView ) {
            
            [[RealApiClient sharedClient] verifyPhoneNumberSMSFromPhoneRegID:[self.phoneNumberRegModel getStringPhoneRegID] pinInput:phoneNumberText block:^(id response, NSError *error) {
                
                if ([weakSelf notError:error view:self.view]) {
                    
                    weakSelf.pinInput = phoneNumberText;
                    
                    if (weakSelf.phoneNumberRegModel.RegType==114||weakSelf.phoneNumberRegModel.RegType==115) {
                        
                        RealSignUpDetalViewController *signUpDetialVC = [[RealSignUpDetalViewController alloc]initWithNibName:@"RealSignUpDetalViewController" bundle:nil];
                        signUpDetialVC.phoneNumberRegModel = self.phoneNumberRegModel;
                        signUpDetialVC.pinInput = self.pinInput;
                        signUpDetialVC.phoneNumber = self.phoneNumber;
                        signUpDetialVC.countryCode = self.countryCode;
                        [weakSelf.navigationController pushViewController: signUpDetialVC animated:YES];
                        
                    } else {
                        
                        [weakSelf callRealNetworkMemberLoginPhoneRegID];
                    }
                    
                } else {
                    
                    if (error.code == 111) {
                        
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                        
                    }
                    
                }
                
            }];
            
        }
        
        if (self.type == PhoneNumberControllerTypeSMSVerifyViewFromPhoneNumberMigrate) {
            
            [[RealApiClient sharedClient] migratePhoneForPhoneNumberSMSVerifyFromPhoneRegID:[self.phoneNumberRegModel getStringPhoneRegID] pinInput:phoneNumberText block:^(id response, NSError *error) {
                
                if ([weakSelf notError:error view:self.view]) {
                    
                    weakSelf.pinInput = phoneNumberText;
                    
                    [weakSelf callRealNetworkMemberLoginPhoneRegID];
                    
                } else {
                    
                    if (error.code == 111) {
                        
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                        
                    }
                    
                }
                
            }];
            
        }
        
        if (self.type == PhoneNumberControllerTypeSMSVerifyViewSettingPage) {
            
            [[RealApiClient sharedClient] verifyPhoneNumberSMSForChangingPhoneFromPhoneRegID:[self.phoneNumberRegModel getStringPhoneRegID] pinInput:phoneNumberText block:^(id response, NSError *error) {
                
                if ([weakSelf notError:error view:self.view]) {
                    
                    weakSelf.pinInput = phoneNumberText;
                    [LoginBySocialNetworkModel shared].myLoginInfo.PhoneNumber = self.phoneNumber;
                    [LoginBySocialNetworkModel shared].myLoginInfo.CountryCode = self.countryCode;
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:JMOLocalizedString(@"alert_ok", nil)
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                                             
                                             
                                         }];
                    
                    
                    NSString *message = [NSString stringWithFormat:JMOLocalizedString(@"change_numberdeatil__successmessage", nil) ,_phoneNumberRegView.countryCodeTextField.text ,_phoneNumberRegView.phoneNumberTextField.text];
                    
                    [weakSelf showAlertWithTitle:message message:nil withActions:@[ok]];
                    
                    
                } else {
                    
                    if (error.code == 111) {
                        
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                        
                    }
                    
                }
                
            }];
            
            
        }
        
    }
    
}

- (void)setViewEndEditing {
    
    [self.view endEditing:YES];
    
}

- (void)callRealNetworkMemberLoginPhoneRegID {
    
    __weak typeof(self) weakSelf = self;
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    //  [mixpanel track:@"Logged in with Email"];
    
    [self showPhoneNumberSMSVerifyView:NO];
    [self showLoadingHUDWithTitleText:JMOLocalizedString(@"phone2_validatingcode", nil) titleTextFont: [UIFont systemFontOfSize:28.0] titleTextColor:nil detailText:nil detailTextFont:nil detailTextColor:nil windowColor:[UIColor clearColor]];
    
    [[LoginByEmailModel shared]callRealNetworkMemberLoginPhoneRegID:[self.phoneNumberRegModel getStringPhoneRegID] pinInput:self.pinInput Success:^(RealNetworkMemberInfo *realNetworkMemberInfo) {
        
        [weakSelf loginBySocialNetwork:realNetworkMemberInfo];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert, NSString *ok) {
        
        [weakSelf.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
        [weakSelf hideLoadingHUD];
        [weakSelf setupViewShowOrHide];
        
    }];
    
}

- (void)loginBySocialNetwork:(RealNetworkMemberInfo*)memberInfo {
    
    NSString *name = [NSString stringWithFormat:@"%@ %@",memberInfo.FirstName,memberInfo.LastName];
    
    __weak typeof(self) weakSelf = self;
    
    [[LoginBySocialNetworkModel shared]
     callLoginBySocialNetworkModelApiDeviceType:@"1"
     deviceToken:self.delegate.devicetoken
     socialNetworkType:@"6"
     userName:name
     userID:memberInfo.UserID
     email:memberInfo.Email
     photoURL:memberInfo.PhotoURL
     accessToken:memberInfo.AccessToken
     Success:^(LoginInfo *myLoginInfo, SystemSettings *SystemSettings) {
         
         [weakSelf registerQBWithInfo:myLoginInfo];
         
         Mixpanel *mixpanel = [Mixpanel sharedInstance];
         [mixpanel identify:myLoginInfo.MemberID.stringValue];
         // [mixpanel track:@"Verified Email Login"];
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
               NSString *ok) {
         
         [weakSelf.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
         [weakSelf hideLoadingHUD];
         
     }];
    
}

#pragma mark - <UIPickerViewDelegate>

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSString *countryCode = [_countryCodeArray objectAtIndex:row];
    [self updateCountryCode:countryCode];
    
}

#pragma mark - <UIPickerViewDataSource>

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return _countryCodeArray.count;
    
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *countryCode = [_countryCodeArray objectAtIndex:row];
    return countryCode;
    
}

@end
