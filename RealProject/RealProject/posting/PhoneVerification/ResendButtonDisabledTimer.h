//
//  ResendButtonDisabledTimer.h
//  productionreal2
//
//  Created by Li Ken on 16/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kNotificationResendButtonChangeUI @"kNotificationResendButtonChangeUI"
#define kNotificationResendButtonEnable @"kNotificationResendButtonEnable"

@interface ResendButtonDisabledTimer : NSObject

@property (nonatomic, strong) NSTimer *resendButtonDisabledTimer;
@property (nonatomic, assign) int resendButtonDisabledTime;

+ (ResendButtonDisabledTimer *)shared;
- (void)callResendButtonDisabledTimer;
@end
