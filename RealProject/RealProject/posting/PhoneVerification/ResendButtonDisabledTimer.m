//
//  ResendButtonDisabledTimer.m
//  productionreal2
//
//  Created by Li Ken on 16/6/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ResendButtonDisabledTimer.h"

@implementation ResendButtonDisabledTimer

static ResendButtonDisabledTimer *sharedResendButtonDisabledTimer;

+ (ResendButtonDisabledTimer *)shared {
    @synchronized(self) {
        if (!sharedResendButtonDisabledTimer) {
            
            sharedResendButtonDisabledTimer = [[ResendButtonDisabledTimer alloc] init];
            
        }
        
        return sharedResendButtonDisabledTimer;
    }
}

- (id) init
{
    if (self = [super init])
    {
        
    }
    return self;
}

- (void)callResendButtonDisabledTimer {
    
    [self.resendButtonDisabledTimer invalidate];
    self.resendButtonDisabledTime = 120;
    self.resendButtonDisabledTimer = [NSTimer scheduledTimerWithTimeInterval: 1 target: self
                                                                    selector: @selector(setUpResendButtonTitle) userInfo: nil repeats: YES];
}

- (void)setUpResendButtonTitle {
    
    self.resendButtonDisabledTime--;
    
    if (self.resendButtonDisabledTime == 0) {
        
        [self.resendButtonDisabledTimer invalidate];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationResendButtonEnable object:nil userInfo:nil];
        
    } else {
        
        NSDictionary * userInfo = @{ @"resendButtonDisabledTime" : @(_resendButtonDisabledTime) };
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationResendButtonChangeUI object:nil userInfo:userInfo];
        
        
    }
    
}

@end
