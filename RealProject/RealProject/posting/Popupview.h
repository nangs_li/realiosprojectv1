//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

@protocol PopUpViewDelegate <NSObject>

-(void)PopUpViewDidSelectFormattedAddress;

@end



@interface Popupview
    : BaseViewController<UITableViewDataSource, UITableViewDelegate>
@property(strong, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong) id<PopUpViewDelegate> popUpDelegate;
@property(nonatomic, retain) NSMutableArray *tableData;
@property(strong, nonatomic) IBOutlet UIButton *savebutton;
@property(strong, nonatomic) IBOutlet UIButton *xbutton;
@property(nonatomic, retain) NSIndexPath *checkedCell;
@property (strong, nonatomic) IBOutlet UILabel *googleAddressSuggestion;
@end
