//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Popupview.h"
#import "SpokenLanguagescell.h"
#import "RealUtility.h"
#import "AutoReSizeTableCell.h"
@interface Popupview ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation Popupview
@synthesize tableData;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableview reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableview setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableview setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pop_dottedline"]]];
    self.tableview.estimatedRowHeight = 44.0;
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    [self.tableview registerNib:[UINib nibWithNibName:@"AutoReSizeTableCell" bundle:nil] forCellReuseIdentifier:@"AutoReSizeTableCell"];
    [self.savebutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    self.tableview.estimatedRowHeight = 80;
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    [self.tableview reloadData];
    
}


-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.googleAddressSuggestion.text=JMOLocalizedString(@"more_about_your_listing__confirm_address", nil);
}



#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    AutoReSizeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AutoReSizeTableCell"];
    NSDictionary *dict = [tableData objectAtIndex:indexPath.row];
    // DDLogInfo(@"NSDictionary * dict---->%@",dict);
    NSString *title = [dict objectForKey:@"formatted_address"];
    [cell configureWithTitle:title indicatorImage:nil];
    cell.enable = YES;
    return cell;
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _checkedCell = indexPath;
    [tableView reloadData];
    NSDictionary *dict = [tableData objectAtIndex:indexPath.row];
    [AgentListingModel shared].googleapireturnaddress =
    [dict objectForKey:@"formatted_address"];
    if([self.popUpDelegate respondsToSelector:@selector(PopUpViewDidSelectFormattedAddress)]){
        [self.popUpDelegate PopUpViewDidSelectFormattedAddress];
    }
    
    //  [self.savebutton setBackgroundColor:self.delegate.Greencolor];
    //  self.savebutton.userInteractionEnabled = YES;
}


@end