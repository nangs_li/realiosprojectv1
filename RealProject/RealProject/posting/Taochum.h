//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "LXReorderableCollectionViewFlowLayout.h"
#import "JFMinimalNotification.h"
#import "ELCImagePickerHeader.h"
#import "Popupview.h"
#import "TDPicker.h"
#import "Taochum2.h"
#import "FilterTypeCell.h"
@interface Taochum : BaseViewController<TDPickerDelegate, UITextFieldDelegate>

#pragma mark house information part1 Button
@property(strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong, nonatomic) IBOutlet FilterTypeCell *filterTypeCell;
@property(strong, nonatomic) IBOutlet UITextField *street;
@property(strong, nonatomic) IBOutlet UITextField *apt;
@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property(strong, nonatomic) IBOutlet UIView *topBarView;
@property(strong, nonatomic) NSDate *selectedDate;
@property(strong,nonatomic) NSString *cachedStreet;
@property(strong,nonatomic) NSMutableArray *googleGeoCodeApiUrlArray;
@property(strong,nonatomic) NSMutableArray *googleGeoCodeApiLanguageArray;
@property(strong,nonatomic) NSMutableArray *googleGeoCodePlaceIDArray;
@property(strong,nonatomic) NSString *googleGeoCodeApiUserInputLanguage;
@property(strong, nonatomic) IBOutlet UILabel *googlereturnaddress;
@property(strong, nonatomic) IBOutlet UILabel *addressTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *moreAboutYourListing;
@property (strong, nonatomic) IBOutlet UILabel *showUsLabel;
@property (strong, nonatomic) IBOutlet UILabel *theEarlyBirdLabel;

#pragma mark house information part1 button press
- (IBAction)SpaceofTypePickerButtonClicked:(id)sender;
- (IBAction)propertTypePickerButtonClicked:(id)sender;

#pragma mark popupview
@property(strong, nonatomic) Popupview *popupview;


@end
