//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Taochum.h"
#import "MJPopupBackgroundView.h"
#import "Popupview.h"
#import "UIViewController+MJPopupViewController.h"
#import "Taochum2.h"
#import "PropertyTypeList.h"
#import "SpaceTypeList.h"
#import "address_components.h"
#import "AgentListingModel.h"
#import "SystemSettingModel.h"
#import "RealUtility.h"
#import "AgentListingModel.h"
#import "SPGooglePlacesAutocompleteUtilities.h"
#import "RealApiClient.h"
// Mixpanel
#import "Mixpanel.h"
#import "GetContryLocation.h"
@interface Taochum () <FilterTypeCellDelegate,PopUpViewDelegate>
@property (nonatomic,strong) NSDictionary *typeDict;
@end
@implementation Taochum

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[GetContryLocation shared]getContryLocation];
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    //  self.propertytypepicker.delegate = self;
    //  self.spaceoftypepicker.delegate = self;
    self.street.delegate = self;
    self.apt.delegate = self;
    
    if (![self isEmpty:[AgentListingModel shared].street]) {
        self.street.text = [AgentListingModel shared].street;
    }
    if (![self isEmpty:[AgentListingModel shared].apt]) {
        self.apt.text = [AgentListingModel shared].apt;
    }
    
    if (![self isEmpty:[AgentListingModel shared].googleapireturnaddress]) {
        self.googlereturnaddress.text =
        [NSString stringWithFormat:@"%@",[AgentListingModel shared].googleapireturnaddress];
    }
    
    [self checkEmptyToChangeNextButtonColor];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
    
    
    if (!self.filterTypeCell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FilterTypeCell" owner:self options:nil];
        self.filterTypeCell = (FilterTypeCell *)[nib objectAtIndex:0];
        self.filterTypeCell.frame = CGRectMake(self.filterTypeCell.frame.origin.x, self.filterTypeCell.frame.origin.y, [RealUtility screenBounds].size.width, self.filterTypeCell.frame.size.height);
        self.filterTypeCell.contentView.frame = CGRectMake(self.filterTypeCell.frame.origin.x, self.filterTypeCell.frame.origin.y, [RealUtility screenBounds].size.width, self.filterTypeCell.frame.size.height);
        NSMutableDictionary *selectedDict = [[NSMutableDictionary alloc]init];
        NSArray *selectedArray =@[];
        if([RealUtility isValid:[AgentListingModel shared].selectpropertytypelist]){
            [selectedDict setObject:@([AgentListingModel shared].selectpropertytypelist.Position) forKey:@"propertyType"];
        }
        
        if([RealUtility isValid:[AgentListingModel shared].selectspacetypelist]){
            [selectedDict setObject:@([AgentListingModel shared].selectspacetypelist.Position) forKey:@"spaceType"];
        }
        NSArray *typeArray = [[SystemSettingModel shared] getSpaceFilterArray];
        if (!typeArray) {
            typeArray = @[];
        }
        if([RealUtility isValid:[AgentListingModel shared].selectpropertytypelist] && [RealUtility isValid:[AgentListingModel shared].selectspacetypelist]){
            for (NSDictionary *typeDict in typeArray) {
                if([typeDict valid]){
                    int spaceType=  [typeDict[@"spaceType"] intValue];
                    int propertyType = [typeDict[@"propertyType"]intValue];
                    int index = 0;
                    if (propertyType == [AgentListingModel shared].selectpropertytypelist.Position && spaceType == [AgentListingModel shared].selectspacetypelist.Position) {
                        if ([typeArray indexOfObject:typeDict] != NSNotFound) {
                            index = (int)[typeArray indexOfObject:typeDict];
                            self.typeDict = @{@"spaceType":@(spaceType),@"propertyType":@(propertyType)};
                            PropertyTypeList *propertyTypeList = [[PropertyTypeList alloc]init];
                            propertyTypeList.Position = [self.typeDict[@"propertyType"] intValue];
                            SpaceTypeList *spaceTypeList = [[SpaceTypeList alloc]init];
                            spaceTypeList.Position = [self.typeDict[@"spaceType"] intValue];
                            [AgentListingModel shared].selectpropertytypelist = propertyTypeList;
                            [AgentListingModel shared].selectspacetypelist = spaceTypeList;
                            selectedArray= @[@(index)];
                        }
                    }
                }
            }
        }else{
            NSDictionary *firstType = typeArray.firstObject;
            if ([firstType valid]) {
                int spaceType=  [firstType[@"spaceType"] intValue];
                int propertyType = [firstType[@"propertyType"]intValue];
                self.typeDict = @{@"spaceType":@(spaceType),@"propertyType":@(propertyType)};
                PropertyTypeList *propertyTypeList = [[PropertyTypeList alloc]init];
                propertyTypeList.Position = [self.typeDict[@"propertyType"] intValue];
                SpaceTypeList *spaceTypeList = [[SpaceTypeList alloc]init];
                spaceTypeList.Position = [self.typeDict[@"spaceType"] intValue];
                [AgentListingModel shared].selectpropertytypelist = propertyTypeList;
                [AgentListingModel shared].selectspacetypelist = spaceTypeList;
                selectedArray= @[@(0)];
            }
        }
        NSDictionary *filterDict = @{
                                     @"typeArray" : typeArray,
                                     @"selectedArray" :selectedArray
                                     };
        self.filterTypeCell.delegate = self;
        [self.filterTypeCell configureCell:filterDict];
        
        self.filterTypeCell.isSingleChoice = YES;
        [self.scrollView addSubview:self.filterTypeCell.contentView];
        [self.filterTypeCell.contentView moveViewTo:self.topBarView direction:RelativeDirectionBottom padding:8];
        [self.street moveViewTo:self.filterTypeCell.contentView direction:RelativeDirectionBottom padding:8];
//        [self.apt moveViewTo:self.street direction:RelativeDirectionBottom padding:8];
//        [self.googlereturnaddress moveViewTo:self.apt direction:RelativeDirectionBottom padding:8];
        [self.filterTypeCell initialAnimation:0.6];
    }
    [self.apt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.nextbutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    UIColor *color = [UIColor lightGrayColor];
    self.street.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"Address"
     attributes:@{NSForegroundColorAttributeName:color}];
    self.apt.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"Apartment/ Unit/ Suit No. (Optional)"
     attributes:@{NSForegroundColorAttributeName:color}];
    self.street.text = [AgentListingModel shared].street;
    self.apt.text = [AgentListingModel shared].apt;
    [self checkEmptyToChangeNextButtonColor];
    
}

- (void) filterTypeDidChange:(NSArray*)selectedArray{
    self.typeDict = [selectedArray firstObject];
    PropertyTypeList *propertyTypeList = [[PropertyTypeList alloc]init];
    propertyTypeList.Position = [self.typeDict[@"propertyType"] intValue];
    SpaceTypeList *spaceTypeList = [[SpaceTypeList alloc]init];
    spaceTypeList.Position = [self.typeDict[@"spaceType"] intValue];
    [AgentListingModel shared].selectpropertytypelist = propertyTypeList;
    [AgentListingModel shared].selectspacetypelist = spaceTypeList;
    
    [self checkEmptyToChangeNextButtonColor];
}

#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.addressTitleLabel.text = JMOLocalizedString(@"more_about_your_listing__display_address", nil);
    self.showUsLabel.text =JMOLocalizedString(@"more_about_your_listing__new_title", nil);
    self.theEarlyBirdLabel.text =JMOLocalizedString(@"more_about_your_listing__new_subtitle", nil);
    self.popupview.googleAddressSuggestion.text=[NSString stringWithFormat:@"%@:",
                                                 JMOLocalizedString(@"google_address_suggestion__title", nil)];
    
    self.moreAboutYourListing.text=JMOLocalizedString(@"more_about_your_listing__title", nil);
    UIColor *color = [UIColor lightGrayColor];
    self.street.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:JMOLocalizedString(@"more_about_your_listing__address", nil)
     attributes:@{NSForegroundColorAttributeName:color}];
    
    self.apt.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:JMOLocalizedString(@"more_about_your_listing__apt", nil)
     attributes:@{NSForegroundColorAttributeName:color}];
    [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    
    
}







#pragma mark button
- (IBAction)backButtonPressed:(id)sender {
    if (self.needCustomBackground) {
        NSDictionary* userInfo = @{@"didEdit": @(0)};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateAgentListing" object:userInfo];
        [self.delegate dismissOverlayerViewController:self];
    }else{
        NSDictionary* userInfo = @{@"didEdit": @(0)};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateAgentListing" object:userInfo];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)nextbuttonpressed:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Saved Posting Details_Address"];
    
    [AgentListingModel shared].street = self.street.text;
    [AgentListingModel shared].apt = self.apt.text;
    
    
    Taochum2 *taochum2VC = [[Taochum2 alloc] initWithNibName:@"Taochum2" bundle:nil];
    taochum2VC.needCustomBackground = self.needCustomBackground;
    if(taochum2VC.needCustomBackground){
        self.navigationController.delegate = self;
    }
    taochum2VC.underLayViewController = self.underLayViewController;
    
    [self.navigationController pushViewController:taochum2VC animated:YES];
    
}
#pragma mark checkempty
- (void)checkEmptyToChangeNextButtonColor {
    DDLogInfo(@"checkemptystart");
    self.nextbutton.enabled = [RealUtility isValid:self.typeDict] && [RealUtility isValid:self.street.text] &&[[AgentListingModel shared].googleGeoCodeApiAddressArray count] > 0;
    
}








#pragma mark -TDPicker (PropertType and SpaceOfType)Delegate------------------------------------------------------
//- (IBAction)propertTypePickerButtonClicked:(id)sender {
//  [self.apt resignFirstResponder];
//  [self.street resignFirstResponder];
//
//  [self.propertytypepicker.picker reloadAllComponents];
//  [self presentSemiModalViewController:self.propertytypepicker];
//}
//- (IBAction)SpaceofTypePickerButtonClicked:(id)sender {
//  [self.apt resignFirstResponder];
//  [self.street resignFirstResponder];
//
//  [self.spaceoftypepicker.picker reloadAllComponents];
//  [self presentSemiModalViewController:self.spaceoftypepicker];
//}

//- (void)picker:(UIPickerView *)picker
//  didSelectRow:(NSInteger)row
//   inComponent:(NSInteger)component {
//  if (picker == self.propertytypepicker.picker) {
//    DDLogInfo(@"self.propertytypepicker.selectpickerdata-->%@",
//              self.propertytypepicker.pickerData[row]);
//
//  } else {
//    DDLogInfo(@"self.spaceoftypepicker.selectpickerdata-->%@",
//              self.spaceoftypepicker.pickerData[row]);
//  }
//}

//- (void)pickerSetDate:(TDPicker *)viewController {
//  if (viewController == self.propertytypepicker) {
//    [self.propertytype
//        setTitle:[self.propertytypepicker.pickerData
//                     objectAtIndex:[self.propertytypepicker.picker
//                                       selectedRowInComponent:0]]
//        forState:UIControlStateNormal];
//
//    [AgentListingModel shared].selectpropertytypelist =
//        [[SystemSettingModel shared].propertytypeaccordingtolanguage
//            objectAtIndex:[self.propertytypepicker.picker
//                              selectedRowInComponent:0]];
//
//    DDLogInfo(@"self.delegate.selectpropertytypelist-->%@",
//              [AgentListingModel shared].selectpropertytypelist);
//
//    [self.typeofspace setTitle:@"Type of Space" forState:UIControlStateNormal];
//    self.typeofspace.titleLabel.text = @"Type of Space";
//    [AgentListingModel shared].selectspacetypelist = nil;
//    DDLogInfo(@"[self.propertytypepicker.pickerData  objectAtIndex:   "
//              @"[self.propertytypepicker.picker "
//              @"selectedRowInComponent:0]]-->%@",
//              [self.propertytypepicker.pickerData
//                  objectAtIndex:[self.propertytypepicker.picker
//                                    selectedRowInComponent:0]]);
//
//    if ([self.propertytypepicker.picker selectedRowInComponent:0] == 0) {
//      self.spaceoftypepicker.pickerData = [[NSMutableArray alloc] init];
//      for (SpaceTypeList *typeofspace in [SystemSettingModel shared]
//               .typeofspaceoneaccordingtolanguage) {
//        [self.spaceoftypepicker.pickerData addObject:typeofspace.SpaceTypeName];
//      }
//
//    } else {
//      self.spaceoftypepicker.pickerData = [[NSMutableArray alloc] init];
//      for (SpaceTypeList *typeofspace in [SystemSettingModel shared]
//               .typeofspacetwoaccordingtolanguage) {
//        [self.spaceoftypepicker.pickerData addObject:typeofspace.SpaceTypeName];
//      }
//    }
//    [self dismissSemiModalViewController:self.propertytypepicker];
//
//  } else if (viewController == self.spaceoftypepicker) {
//    [self.typeofspace setTitle:[self.spaceoftypepicker.pickerData
//                                   objectAtIndex:[self.spaceoftypepicker.picker
//                                                     selectedRowInComponent:0]]
//                      forState:UIControlStateNormal];
//    self.typeofspace.titleLabel.text = [self.spaceoftypepicker.pickerData
//        objectAtIndex:[self.spaceoftypepicker.picker selectedRowInComponent:0]];
//
//    [AgentListingModel shared].selectspacetypelist = [[AgentListingModel shared].selectpropertytypelist.SpaceTypeList
//        objectAtIndex:[self.spaceoftypepicker.picker selectedRowInComponent:0]];
//    DDLogInfo(@"self.delegate.selectspacetypelist-->%@",
//              [AgentListingModel shared].selectspacetypelist);
//    DDLogInfo(@"[self.spaceoftypepicker.pickerData  "
//              @"objectAtIndex:self.inttypeofspace]-->%@",
//              [self.spaceoftypepicker.pickerData
//                  objectAtIndex:[self.spaceoftypepicker.picker
//                                    selectedRowInComponent:0]]);
//
//    [self dismissSemiModalViewController:self.spaceoftypepicker];
//  }
//
//  [self checkEmptyToChangeNextButtonColor];
//}

//- (void)pickerClearDate:(TDPicker *)viewController {
//  if (viewController == self.propertytypepicker) {
//    [self dismissSemiModalViewController:self.propertytypepicker];
//  } else if (viewController == self.spaceoftypepicker) {
//    [self dismissSemiModalViewController:self.spaceoftypepicker];
//  }
//}
//
//- (void)pickerCancel:(TDPicker *)viewController {
//  if (viewController == self.propertytypepicker) {
//    [self dismissSemiModalViewController:self.propertytypepicker];
//  } else if (viewController == self.spaceoftypepicker) {
//    [self dismissSemiModalViewController:self.spaceoftypepicker];
//  }
//}
//
//







- (void)PopUpViewDidSelectFormattedAddress{
    [self confirmButtonPressed:nil];
}

#pragma mark -   Google Geocode api return Address String-----------------------------------------------------------
- (void)showPopUpViewFromGoogleGeocodeAPiReturnString:(NSMutableDictionary *)dict {
    
    
    ////setup popupview
    DDLogVerbose(@"dic-->t%@", dict);
    NSMutableArray *resultarry = [dict objectForKey:@"results"];
    
    _popupview = [[Popupview alloc] initWithNibName:@"Popupview" bundle:nil];
    _popupview.view.frame = [RealUtility screenBounds];
    _popupview.popUpDelegate = self;
    self.popupview.googleAddressSuggestion.text=[NSString stringWithFormat:@"%@:",
                                                 JMOLocalizedString(@"google_address_suggestion__title", nil)];
    
    
    if (resultarry.count == 0) {
    } else {
        _popupview.tableData = resultarry;
        
    }
    [self.apt resignFirstResponder];
    [self.street resignFirstResponder];
    [self presentPopupViewController:_popupview
                       animationType:MJPopupViewAnimationSlideBottomTop];
    ////setup popupview
    [_popupview.xbutton addTarget:self
                           action:@selector(dismissButtonPressed:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [_popupview.savebutton addTarget:self
                              action:@selector(confirmButtonPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
    //    [_popupview.tableview reloadData];
}

- (void)dismissButtonPressed:(id)sender {
    [self checkEmptyToChangeNextButtonColor];
    [self dismissPopupViewControllerWithanimationType:
     MJPopupViewAnimationSlideTopBottom];
}

- (void)confirmButtonPressed:(id)sender {
    
    
    if([self isEmpty:[[self.popupview.tableData
                       objectAtIndex:self.popupview.checkedCell
                       .row]
                      objectForKey:@"place_id"]]){
        return;
    }
    NSString* placeID=[[self.popupview.tableData
                        objectAtIndex:self.popupview.checkedCell
                        .row]
                       objectForKey:@"place_id"];
    
    NSString *googleUrlString          = [NSString
                                          stringWithFormat:
                                          @"https://maps.googleapis.com/maps/api/geocode/"
                                          @"json?place_id=%@&key="
                                          @"%@&language=%@",
                                          placeID, kGoogleAPIKey,self.googleGeoCodeApiUserInputLanguage];
    self.googleGeoCodeApiUrlArray      = [[NSMutableArray alloc]init];
    self.googleGeoCodeApiLanguageArray = [[NSMutableArray alloc]init];
    self.googleGeoCodePlaceIDArray     = [[NSMutableArray alloc]init];
    [self.googleGeoCodeApiUrlArray addObject:googleUrlString];
    [self.googleGeoCodeApiLanguageArray addObject:self.googleGeoCodeApiUserInputLanguage];
    [self.googleGeoCodePlaceIDArray addObject:placeID];
    self.googlereturnaddress.hidden = NO;
    self.addressTitleLabel.hidden = NO;
    self.googlereturnaddress.text = [NSString stringWithFormat:@"%@",[AgentListingModel shared].googleapireturnaddress];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopTop];
    [AgentListingModel shared].googleGeoCodeApiAddressArray = [[NSMutableArray alloc] init];
    //// Find addressComponents have Country  Like tw
    
    for (NSDictionary *addressNSDictionary in
         [[self.popupview.tableData objectAtIndex:self.popupview.checkedCell.row]
          objectForKey:@"address_components"]) {
             address_components *addressComponents =
             [[address_components alloc] initwithNSDictionary:addressNSDictionary:0];
             for (NSString *type in addressComponents.types) {
                 if ([type isEqualToString:@"country"]) {
                     [self geoCodeApiFromPlaceIDAndLanguageFromAddressComponentHaveCountryName:addressComponents googlePlaceId:
                      placeID];
                     return;
                 }
             }
         }
    [self checkEmptyToChangeNextButtonColor];
}

- (void)geoCodeApiFromPlaceIDAndLanguageFromAddressComponentHaveCountryName:(address_components *)addressComponent
                                                              googlePlaceId:(NSString *)googlePlaceId {
    ////SystemSettingsGoogleAddressLanguageListFoundLanguage  So  Language
    BOOL SystemSettingsGoogleAddressLanguageListNotFoundLanguage = YES;
    for (GoogleAddressLanguageList *googleAddressLanguageList in [SystemSettingModel shared]
         .SystemSettings.GoogleAddressLanguageList) {
        if ([googleAddressLanguageList.Country
             isEqualToString:addressComponent.short_name]) {
            for (NSString *googleAddressLanguage in googleAddressLanguageList.Lang) {
                NSString *googleUrlString = [NSString
                                             stringWithFormat:
                                             @"https://maps.googleapis.com/maps/api/geocode/"
                                             @"json?place_id=%@&key="
                                             @"%@&language=%@",
                                             googlePlaceId, kGoogleAPIKey,googleAddressLanguage];
                
                if (![self.googleGeoCodeApiUserInputLanguage isEqualToString:googleAddressLanguage]) {
                    [self.googleGeoCodeApiUrlArray addObject:googleUrlString];
                    [self.googleGeoCodeApiLanguageArray addObject:googleAddressLanguage];
                    [self.googleGeoCodePlaceIDArray addObject:googlePlaceId];
                }
                SystemSettingsGoogleAddressLanguageListNotFoundLanguage = NO;
            }
        }
    }
    ////SystemSettingsGoogleAddressLanguageListNotFoundLanguage  So English Language only
    if (SystemSettingsGoogleAddressLanguageListNotFoundLanguage) {
        NSString *googleUrlString = [NSString
                                     stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/"
                                     @"json?place_id=%@&key="
                                     @"%@&language=en",
                                     googlePlaceId,kGoogleAPIKey];
        [self.googleGeoCodeApiUrlArray addObject:googleUrlString];
        [self.googleGeoCodeApiLanguageArray addObject:@"en"];
        [self.googleGeoCodePlaceIDArray addObject:googlePlaceId];
        
    }
    [[CrashlyticsModel shared]checkPlaceIDVaildateFromGoogleGeoCodePlaceIDArray:self.googleGeoCodePlaceIDArray];
    [self showLoadingHUD];
    [AgentListingModel shared].googleGeoCodeApiAddressArray=[[NSMutableArray alloc]init];
    [self startGeoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLinkArrayCount:(int)self.googleGeoCodeApiUrlArray.count completionBlock:^(id response, NSError *error) {
        [AgentListingModel shared].street = self.street.text;
        [self hideLoadingHUD];
        [self checkEmptyToChangeNextButtonColor];
        
    }];
}
-(void)startGeoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLinkArrayCount:(int)arrayCount completionBlock:(CommonBlock)completionBlock{
    [self geoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLinkArrayCount:arrayCount arrayIndex:0  completionBlock:completionBlock];
}

-(void)geoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLinkArrayCount:(int)arrayCount arrayIndex:(int)arrayIndex completionBlock:(CommonBlock)completionBlock{
    
    
    NSString *googleGeoCodeApiUrl      = self.googleGeoCodeApiUrlArray[arrayIndex] ;
    NSString *googleGeoCodeApiLanguage = self.googleGeoCodeApiLanguageArray[arrayIndex];
    NSString *googleGeoCodeApiPlaceID  = self.googleGeoCodePlaceIDArray[arrayIndex];
    [[AgentListingModel shared]geoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLink:googleGeoCodeApiUrl languageString:googleGeoCodeApiLanguage placeId:googleGeoCodeApiPlaceID success:^(id responseObject)  {
        if (arrayCount==arrayIndex+1) {
            completionBlock(nil,nil);
            
        }else{
            [self geoCodeApiFromPlaceIDAddToGoogleGeoCodeApiAddressArrayByUrlLinkArrayCount:arrayCount arrayIndex:arrayIndex+1 completionBlock:completionBlock];
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorStatus,NSString *alert,NSString *ok) {
        [self hideLoadingHUD];
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorStatus alert:alert ok:ok];
        
    }];
}
#pragma mark -textfield Delegate------------------------------------------------------------------------------ -
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.street) {
        NSString *address = [NSString stringWithFormat:@"%@", self.street.text];
        
        [self getDominantLanguageWithString:address];
        
        
    }
    
    DDLogInfo(@"textFieldDidEndEditing");
    [self checkEmptyToChangeNextButtonColor];
}

- (void)textFieldDidChange:(UITextField*)textField{
    if (textField == self.apt) {
        [AgentListingModel shared].apt = self.apt.text;
    }
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *pendingString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    NSData *pendingData = [pendingString dataUsingEncoding:NSUTF8StringEncoding];
    float dataInKB = pendingData.length;
    if (dataInKB > kMaxGeneralTextFieldDataSizeInByte) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - Detect Input Language

- (void)getDominantLanguageWithString:(NSString *)inputString {
    
    NSString *string = [self shortenAddress:inputString];
    
    RealApiClient *client = [RealApiClient sharedClient];
    [client detectLanguageWithString:string
                             success:^(GoogleTranslateModel *response) {
                                 
                                 // Use response.languageDetection.language
                                 if ([RealUtility isValid:inputString]) {
                                     self.cachedStreet = inputString;
                                     
                                     [self showLoadingHUD];
                                     
                                     NSString *language = response.languageDetection.mappedLanguage;
                                     
                                     if ([language isEqualToString:@"zh-CN"]) {
                                         
                                         language = @"zh-TW";
                                     }
                                     [AgentListingModel shared].userInputTextLanguage=language;
                                     [[AgentListingModel shared] geoCodeJsonApiFromAddress:inputString language:language success:^(id responseObject) {
                                         NSArray * googleReturnResultArray  = [responseObject objectForKey:@"results"];
                                         
                                         if ([googleReturnResultArray count] > 0) {
                                             self.googleGeoCodeApiUserInputLanguage=language;
                                         }
                                         [self hideLoadingHUD];
                                         [self showPopUpViewFromGoogleGeocodeAPiReturnString:responseObject];
                                         
                                     } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                 NSString *errorMessage, RequestErrorStatus errorStatus,
                                                 NSString *alert, NSString *ok) {
                                         [self hideLoadingHUD];
                                         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                                       errorMessage:errorMessage
                                                                                        errorStatus:errorStatus
                                                                                              alert:alert
                                                                                                 ok:ok];
                                     }];
                                     
                                 }else if (![RealUtility isValid:inputString] ){
                                     self.googlereturnaddress.text = @"";
                                     self.googlereturnaddress.hidden = YES;
                                     self.addressTitleLabel.hidden = YES;
                                     [[AgentListingModel shared].googleGeoCodeApiAddressArray removeAllObjects];
                                 }
                             }
                             failure:^(id response, NSError *error) {
                                 
                                 // Error Handling
                                 [self hideLoadingHUD];
                                 [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                                               errorMessage:JMOLocalizedString(@"error_message__server_not_response", nil)
                                                                                errorStatus:GoogleServerNotReponse
                                                                                      alert:JMOLocalizedString(@"alert_alert", nil)
                                                                                         ok:JMOLocalizedString(@"alert_ok", nil)];
                             }];
}

- (NSString *)shortenAddress:(NSString *)address {
    
    NSInteger maxLength = 20;
    
    NSString *longestWord = nil;
    
    if ([address length] > maxLength) {
        
        NSArray *words = [address componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        
        for (NSString *word in words) {
            
            if (longestWord == nil || [word length] > [longestWord length]) {
                
                longestWord = word;
            }
        }
        
        if ([longestWord length] > maxLength) {
            
            longestWord = [longestWord substringToIndex:maxLength];
        }
        
    } else {
        
        longestWord = address;
    }
    
    return longestWord;
}
@end