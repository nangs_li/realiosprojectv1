//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "TDPicker.h"
@interface Taochum2 : BaseViewController<UITextFieldDelegate, TDPickerDelegate>

#pragma mark house information part2 Button
@property(strong, nonatomic) IBOutlet UILabel *numberofbedroomlabel;
@property(strong, nonatomic) IBOutlet UILabel *numberofbathroomlabel;
@property(nonatomic) int numberofbedroom;
@property(nonatomic) int numberofbathroom;
@property(strong, nonatomic) IBOutlet UITextField *price;
@property(strong, nonatomic) IBOutlet UITextField *size;
@property(strong, nonatomic) IBOutlet TDPicker *currencypicker;
@property(strong, nonatomic) IBOutlet UIButton *currency;
@property(strong, nonatomic) IBOutlet UIButton *metric;
@property (strong, nonatomic) IBOutlet UIButton *priceBtn;
@property (strong, nonatomic) IBOutlet UIButton *sizeBtn;
@property (strong, nonatomic) IBOutlet UIButton *bedRoomBtn;
@property (strong, nonatomic) IBOutlet UIButton *bathRoomBtn;

@property (strong, nonatomic) IBOutlet UIButton *bedRoomIncreaseBtn;
@property (strong, nonatomic) IBOutlet UIButton *bedRoomDecreaseBtn;
@property (strong, nonatomic) IBOutlet UIButton *bathRoomIncreaseBtn;
@property (strong, nonatomic) IBOutlet UIButton *bathRoomDecreaseBtn;
@property(strong, nonatomic) IBOutlet TDPicker *metricpicker;
@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property (strong, nonatomic) IBOutlet UIView *priceView;
@property (strong, nonatomic) IBOutlet UIView *sizeView;
@property (strong, nonatomic) IBOutlet UIView *priceLeftView;
@property (strong, nonatomic) IBOutlet UIView *sizeRightView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *showUsTheLabel;
@property (strong, nonatomic) IBOutlet UILabel *theEarlyBirdLabel;
@end
