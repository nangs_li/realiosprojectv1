//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Taochum2.h"
#import "Finishposting.h"
#import "MetricList.h"
#import "RealUtility.h"
#import "unique.h"

// Mixpanel
#import "Mixpanel.h"

@interface Taochum2 ()

@end
@implementation Taochum2

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //// button border setting
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    self.price.delegate = self;
    self.size.delegate = self;
    self.currencypicker.delegate = self;
    self.metricpicker.delegate = self;
    
    if (![self isEmpty:[AgentListingModel shared].price]) {
        self.price.text = [RealUtility getDecimalFormatByString:[AgentListingModel shared].price withUnit:@""];
    }
    if (![self isEmpty:[AgentListingModel shared].size]) {
        self.size.text = [RealUtility getDecimalFormatByString:[AgentListingModel shared].size withUnit:@""];
    }
    if (![self isEmpty:[AgentListingModel shared].numberofbathroom]) {
        self.numberofbathroom = [[AgentListingModel shared].numberofbathroom intValue];
    }else{
        self.numberofbathroom = 0;
    }
    if (![self isEmpty:[AgentListingModel shared].numberofbedroom]) {
        self.numberofbedroom = [[AgentListingModel shared].numberofbedroom intValue];
    }else{
        self.numberofbedroom = 0;
    }
    ////picker currency setup
    _currencypicker.pickerData = [[NSMutableArray alloc] init];
    for (NSString *currency in [SystemSettingModel shared].SystemSettings.CurrencyList) {
        [_currencypicker.pickerData addObject:currency];
    }
    
    if (![self isEmpty:[AgentListingModel shared].selectcurrencytype]) {
        [self.currency setTitle:[AgentListingModel shared].selectcurrencytype
                       forState:UIControlStateNormal];
        
    } else {
        [self.currency
         setTitle:[[SystemSettingModel shared].SystemSettings.CurrencyList objectAtIndex:0]
         forState:UIControlStateNormal];
    }
    
    ////picker metric setup
    _metricpicker.pickerData = [[NSMutableArray alloc] init];
    for (MetricList *metric in [SystemSettingModel shared].metricaccordingtolanguage) {
        [_metricpicker.pickerData addObject:metric.NativeName];
    }
    
    if (![self isEmpty:[AgentListingModel shared].selectmetric]) {
        [self.metric setTitle:[AgentListingModel shared].selectmetric.NativeName
                     forState:UIControlStateNormal];
        
    } else {
        if (![self isEmpty:[SystemSettingModel shared].metricaccordingtolanguage]) {
            MetricList *metric =
            [[SystemSettingModel shared].metricaccordingtolanguage objectAtIndex:0];
            if ([RealUtility isValid:[SystemSettingModel shared].SystemSettings.userSelectMetric]) {
                metric = [SystemSettingModel shared].SystemSettings.userSelectMetric;
            }
            [AgentListingModel shared].selectmetric = metric;
            [self.metric setTitle:metric.NativeName forState:UIControlStateNormal];
            self.metric.titleLabel.text = metric.NativeName;
        }
    }
    [self checkEmptyToChangeNextButtonColor];
    
    self.price.leftViewMode =UITextFieldViewModeAlways;
    self.price.leftView = self.priceLeftView;
    self.size.rightViewMode = UITextFieldViewModeAlways;
    self.size.rightView = self.sizeRightView;
    [self.priceBtn setUpAutoScaleButton];
     [self.sizeBtn setUpAutoScaleButton];
    [self.bedRoomBtn setUpAutoScaleButton];
    [self.bathRoomBtn setUpAutoScaleButton];
    [self.nextbutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [AgentListingModel shared].selectcurrencytype = [self.currencypicker.pickerData firstObject];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.showUsTheLabel.text =JMOLocalizedString(@"listing_detail__show_us", nil);
    self.theEarlyBirdLabel.text =JMOLocalizedString(@"listing_detail__the_early", nil);
    [self.priceBtn setTitle:JMOLocalizedString(@"listing_detail__price", nil) forState:UIControlStateNormal];
    [self.sizeBtn setTitle:JMOLocalizedString(@"listing_detail__size", nil) forState:UIControlStateNormal];
    [self.bedRoomBtn setTitle:JMOLocalizedString(@"listing_detail__bedroom", nil) forState:UIControlStateNormal];
    [self.bathRoomBtn setTitle:JMOLocalizedString(@"listing_detail__bathroom", nil) forState:UIControlStateNormal];
    
    [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    [self.metricpicker.cancelBtn setTitle:JMOLocalizedString(@"common__cancel", nil)];
    [self.metricpicker.saveBtn setTitle:JMOLocalizedString(@"error_message__done", nil)];
    [self.currencypicker.cancelBtn setTitle:JMOLocalizedString(@"common__cancel", nil)];
    [self.currencypicker.saveBtn setTitle:JMOLocalizedString(@"error_message__done", nil)];

}

#pragma mark -TDPicker(Currency,Metric) Delegate------------------------------------------------------------------
- (IBAction)pressCurrencyList:(id)sender {
    [self.price resignFirstResponder];
    [self.size resignFirstResponder];
    [self dismissSemiModalViewController:_metricpicker];
    
    [_currencypicker.picker reloadAllComponents];
    [self presentSemiModalViewController:_currencypicker];
}
- (IBAction)pressMetricList:(id)sender {
    [self.price resignFirstResponder];
    [self.size resignFirstResponder];
    [self dismissSemiModalViewController:_currencypicker];
    
    [_metricpicker.picker reloadAllComponents];
    [self presentSemiModalViewController:_metricpicker];
}
- (void)picker:(UIPickerView *)picker
  didSelectRow:(NSInteger)row
   inComponent:(NSInteger)component {
    if (picker == self.currencypicker.picker) {
        DDLogInfo(@"_currencypicker.selectpickerdata-->%@",
                  _currencypicker.pickerData[row]);
        
    } else if (picker == self.metricpicker.picker) {
        DDLogInfo(@"_metricpicker.selectpickerdata-->%@",
                  _metricpicker.pickerData[row]);
    }
}
- (void)pickerSetDate:(TDPicker *)viewController {
    if (viewController == self.currencypicker) {
        [self.currency setTitle:[self.currencypicker.pickerData
                                 objectAtIndex:[self.currencypicker.picker
                                                selectedRowInComponent:0]]
                       forState:UIControlStateNormal];
        [AgentListingModel shared].selectcurrencytype = [self.currencypicker.pickerData
                                                         objectAtIndex:[self.currencypicker.picker selectedRowInComponent:0]];
        [self dismissSemiModalViewController:self.currencypicker];
        
    } else if (viewController == self.metricpicker) {
        [AgentListingModel shared].selectmetric = [[SystemSettingModel shared].metricaccordingtolanguage
                                                   objectAtIndex:[self.metricpicker.picker selectedRowInComponent:0]];
        [self.metric setTitle:[AgentListingModel shared].selectmetric.NativeName
                     forState:UIControlStateNormal];
        [self dismissSemiModalViewController:self.metricpicker];
    }
    [self checkEmptyToChangeNextButtonColor];
}

- (void)pickerClearDate:(TDPicker *)viewController {
    if (viewController == self.currencypicker) {
        [self dismissSemiModalViewController:_currencypicker];
    } else if (viewController == self.metricpicker) {
        [self dismissSemiModalViewController:_metricpicker];
    }
}

- (void)pickerCancel:(TDPicker *)viewController {
    if (viewController == self.currencypicker) {
        [self dismissSemiModalViewController:_currencypicker];
    } else if (viewController == self.metricpicker) {
        [self dismissSemiModalViewController:_metricpicker];
    }
}



#pragma mark - Button---------------------------------------------------------------------------------
- (IBAction)btnBackpressed:(id)sender {
    [self btnBackPressed:sender];
}
- (IBAction)nextbuttonpressed:(id)sender {
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Saved Posting Details_Price"];
    //    [AgentListingModel shared].numberofbathroom = [NSString stringWithFormat:@"%d",self.numberofbathroom];
    //    [AgentListingModel shared].numberofbedroom = [NSString stringWithFormat:@"%d",self.numberofbedroom];
    
    
    
    unique *uniqueVc = [[unique alloc] initWithNibName:@"unique" bundle:nil];
    uniqueVc.needCustomBackground = self.needCustomBackground;
    if(uniqueVc.needCustomBackground){
        self.navigationController.delegate = self;
    }
    if ([AgentListingModel shared].reasonDict.allKeys.count >0){
        uniqueVc.type = AddSloganTypeEdit;
    }else{
        uniqueVc.type = AddSloganTypeNew;
    }
    
    uniqueVc.underLayViewController = self.underLayViewController;
    [self.navigationController pushViewController:uniqueVc animated:YES];
    
    
}


- (void)setNumberofbathroom:(int)numberofbathroom{
    _numberofbathroom = numberofbathroom;
    
    self.bathRoomDecreaseBtn.enabled = _numberofbathroom >0;
    self.bathRoomIncreaseBtn.enabled = _numberofbathroom < kMaxBathroomCount;
    if (_numberofbathroom >= 0) {
        [self.numberofbathroomlabel setText:[NSString stringWithFormat:@"%d", self.numberofbathroom]];
        [AgentListingModel shared].numberofbathroom = self.numberofbathroomlabel.text;
    }
}

- (void)setNumberofbedroom:(int)numberofbedroom{
    _numberofbedroom = numberofbedroom;
    self.bedRoomDecreaseBtn.enabled = _numberofbedroom > 0;
    self.bedRoomIncreaseBtn.enabled = _numberofbedroom < kMaxBathroomCount;
    if (_numberofbedroom >= 0) {
        [self.numberofbedroomlabel setText:[NSString stringWithFormat:@"%d",self.numberofbedroom]];
        [AgentListingModel shared].numberofbedroom =  self.numberofbedroomlabel.text;
    }
}

- (IBAction)increaseBedRoomCount:(id)sender {
    if (self.numberofbedroom < kMaxBedroomCount) {
        self.numberofbedroom++;
    }
    [self.view endEditing:YES];
}
- (IBAction)decreaseBedRoomCount:(id)sender {
    if (self.numberofbedroom > 0) {
        self.numberofbedroom--;
    }
    [self.view endEditing:YES];
}

- (IBAction)increaseBathRoomCount:(id)sende {
    if (self.numberofbathroom < kMaxBathroomCount) {
        self.numberofbathroom++;
    }
    [self.view endEditing:YES];
}
- (IBAction)decreaseBathRoomCount:(id)sende {
    if (self.numberofbathroom > 0) {
        self.numberofbathroom--;
    }
    [self.view endEditing:YES];
}





#pragma mark- textfield Delegate-------------------------------------------------------------------------------

- (void)textFieldDidChange:(UITextField*)textField{
    if (textField == self.price) {
        [AgentListingModel shared].price = [RealUtility removeDecimalFormat:self.price.text];
    }else if (textField == self.size){
        [AgentListingModel shared].size = [RealUtility removeDecimalFormat:self.size.text];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    [self checkEmptyToChangeNextButtonColor];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (([string isEqualToString:@"0"] || [string isEqualToString:@""]) && [textField.text rangeOfString:@"."].location < range.location) {
        return YES;
    }
    
    // First check whether the replacement string's numeric...
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    bool isNumeric = [string isEqualToString:filtered];
    
    // Then if the replacement string's numeric, or if it's
    // a backspace, or if it's a decimal point and the text
    // field doesn't already contain a decimal point,
    // reformat the new complete number using
    // NSNumberFormatterDecimalStyle
    if (isNumeric ||
        [string isEqualToString:@""] ||
        ([string isEqualToString:@"."] &&
         [textField.text rangeOfString:@"."].location == NSNotFound)) {
            
            // Create the decimal style formatter
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];

            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setMaximumFractionDigits:10];
            
            // Combine the new text with the old; then remove any
            // commas from the textField before formatting
            NSString *combinedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSString *numberWithoutCommas = [combinedText stringByReplacingOccurrencesOfString:@"," withString:@""];
            NSNumber *number = [formatter numberFromString:numberWithoutCommas];
            
            float numberDouble = [number floatValue];
            if (textField == self.price && numberDouble > decimalMax) {
                return NO;
            }else if(textField == self.size && numberDouble >INT32_MAX ){
                return NO;
            }else{
                NSString *formattedString = [formatter stringFromNumber:number];
                
                // If the last entry was a decimal or a zero after a decimal,
                // re-add it here because the formatter will naturally remove
                // it.
                if ([string isEqualToString:@"."] &&
                    range.location == textField.text.length) {
                    formattedString = [formattedString stringByAppendingString:@"."];
                }
                
                textField.text = formattedString;
                
                [self textFieldDidChange:textField];
            }
            
        }
    
    return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)checkEmptyToChangeNextButtonColor {
    self.nextbutton.enabled = [RealUtility isValid:self.price.text] && [RealUtility isValid:self.size.text];
}
@end