//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "Addlanguage.h"
#import "SwipeView.h"
typedef enum {
    AddReasonTypeNew = 0,
    AddReasonTypeEdit,
    AddReasonTypeAdd
}AddReasonType;


@interface Threereason : BaseViewController<UITextViewDelegate,SwipeViewDataSource,SwipeViewDelegate, UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic,assign) AddReasonType addReasonType;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *reasonSwipeviewHeightConstraint;
@property (nonatomic,strong) IBOutlet NSLayoutConstraint *languageButtonHeightConstraint;
@property (nonatomic,strong) IBOutletCollection(UIButton) NSArray *reasonHeaderButtons;
@property (strong, nonatomic) IBOutlet UIButton *reasonOneButton;
@property (strong, nonatomic) IBOutlet UIButton *reasonTwoButton;
@property (strong, nonatomic) IBOutlet UIButton *reasonThreeButton;
@property (nonatomic,assign) NSNumber *currentLangIndex;

@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property (strong, nonatomic) IBOutlet UIButton *languageButton;
@property(strong, nonatomic) IBOutlet UILabel *Slogan;
@property (strong, nonatomic) IBOutlet SwipeView *reasonSwipeView;
@property (strong, nonatomic) IBOutlet UILabel *wowYourBuyerLabel;
@property (strong, nonatomic) IBOutlet UILabel *threeReasonsLabel;

@end
