//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Threereason.h"
#import "SpokenLanguagescell.h"
#import "Finishposting.h"
#import "Uploadphoto.h"
#import "RealUtility.h"
#import "AddReasonView.h"
#import "UIActionSheet+Blocks.h"
#import "ActionSheetPicker.h"
#import "ApartmentDetailViewController.h"
#import "RealImagePickerViewController.h"
#import "CTAssetsPickerController.h"
#import "NSString+Utility.h"
// Mixpanel
#import "Mixpanel.h"

@interface Threereason () <AddReasonViewDelegate,UIImagePickerControllerDelegate>
@property (nonatomic,assign) BOOL nextReasonHint;
@property (nonatomic,strong)AddReasonView *pendingAddReasonView;
@property (nonatomic,assign) int currentReasonIndex;
@property (nonatomic,strong) NSString* currentLangTitle;
@property (nonatomic,assign) BOOL enableUpdate;
@end
@implementation Threereason

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.currentLangIndex) {
        self.currentLangIndex = @([SystemSettingModel shared].selectSystemLanguageList.Index);
    }else{
        self.currentLangIndex = self.currentLangIndex;
    }
    if (self.addReasonType == AddReasonTypeEdit) {
        if ([AgentListingModel shared].reasonDict.allKeys.count >0) {
            self.currentLangIndex = [[AgentListingModel shared].reasonDict.allKeys lastObject];
            //            self.currentReasonIndex = 0;
        }
    }else{
        [self setupWithType:self.addReasonType];
    }
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.addReasonType == AddReasonTypeNew){
        if (self.currentReasonIndex == 0) {
            UIView *firstItemView = [self.reasonSwipeView itemViewAtIndex:0];
            AddReasonView *firstReasonView = [firstItemView viewWithTag:1000];
            [firstReasonView.reasonTextView becomeFirstResponder];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.languageButton setBackgroundImage:[UIImage imageWithBorder:[UIColor lightGrayColor] size:CGSizeMake(20, 20) fillcolor:[UIColor clear]] forState:UIControlStateNormal];
    [self.languageButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.languageButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.languageButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.languageButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(hideKeyBoard)];
    self.enableUpdate = YES;
    self.Slogan.textColor = RealBlueColor;
    [self.view addGestureRecognizer:tapGesture];
    self.reasonSwipeView.pagingEnabled =NO;
    self.reasonSwipeView.scrollEnabled = NO;
    self.reasonSwipeviewHeightConstraint.constant = [RealUtility screenBounds].size.height - self.reasonSwipeView.frame.origin.y -self.nextbutton.frame.size.height -8;
    [self.nextbutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    
    [self setupWithType:self.addReasonType];
    [self reasonHeaderButtonDidPress:self.reasonOneButton];
    [self checkEmptyToChangeSaveButtonColor];
    if (!self.currentLangIndex) {
        self.currentLangIndex = @([SystemSettingModel shared].selectSystemLanguageList.Index);
    }else{
        self.currentLangIndex = self.currentLangIndex;
    }
}

-(void)setupWithType:(AddReasonType)reasonType{
    if (reasonType == AddReasonTypeAdd) {
        [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
        self.languageButton.userInteractionEnabled = NO;
        self.languageButtonHeightConstraint.constant = 0;
        self.languageButton.hidden = YES;
    }else if (reasonType == AddReasonTypeEdit){
        if ([AgentListingModel shared].reasonDict.allKeys.count >1) {
            self.languageButton.userInteractionEnabled = YES;
            self.languageButtonHeightConstraint.constant = 27;
            self.languageButton.hidden = NO;
        }else{
            self.languageButton.userInteractionEnabled = NO;
            self.languageButtonHeightConstraint.constant = 0;
            self.languageButton.hidden = YES;
        }
        [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    }else if (reasonType == AddReasonTypeNew){
        self.languageButton.userInteractionEnabled = NO;
        self.languageButtonHeightConstraint.constant = 0;
        self.languageButton.hidden = YES;
        [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    }
    
    //    if ([self getCurrentLangDict].allKeys.count > 2) {
    //        self.nextReasonHint = NO;
    //    }else{
    //        self.nextReasonHint = YES;
    //    }
    [self checkEmptyToChangeSaveButtonColor];
}

-(void)setCurrentLangIndex:(NSNumber*)currentLangIndex{
    _currentLangIndex = currentLangIndex;
    _currentLangTitle = [[SystemSettingModel shared]getSettingLanguageString:_currentLangIndex];
    NSDictionary *reasonDict =[self getCurrentLangDict];
    int sloganIndex = [reasonDict[@"sloganIndex"] intValue];
    NSString *slogan =[[SystemSettingModel shared]getSloganStringByLangIndex:_currentLangIndex position:@(sloganIndex)];
    [self.Slogan setText:slogan];
    [self.languageButton setTitle:_currentLangTitle forState:UIControlStateNormal];
    [self.reasonSwipeView reloadData];
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.threeReasonsLabel.text=JMOLocalizedString(@"top_3_reasons__title", nil);
    self.wowYourBuyerLabel.text=JMOLocalizedString(@"top_3_reasons__wow_your", nil);
    [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    
    [self setupWithType:self.addReasonType];
    
}


#pragma mark - Button

-(IBAction)langButtonDidPress:(id)sender{
    [self.view endEditing:YES];
    NSMutableArray *selectedLang = [[NSMutableArray alloc]init];
    for (int i =0; i<[AgentListingModel shared].reasonDict.allKeys.count; i++) {
        [selectedLang addObject:[[SystemSettingModel shared]getSettingLanguageString:[AgentListingModel shared].reasonDict.allKeys[i]]];
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select a Langugae"
                                            rows:selectedLang
                                initialSelection:[[AgentListingModel shared].reasonDict.allKeys indexOfObject:self.currentLangIndex]
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           self.currentLangIndex = [AgentListingModel shared].reasonDict.allKeys[selectedIndex];
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         DDLogDebug(@"Block Picker Canceled");
                                     }
                                          origin:sender];
}

-(BOOL)checkReasonIsValid:(int)reasonIndex{
    NSDictionary *currentLangDict = [self getCurrentLangDict];
    NSDictionary *reasonDict = currentLangDict[@(reasonIndex)];
    NSString *reasonString = reasonDict[@"reasonString"];
    NSString *reasonImageAsset = reasonDict[@"reasonImageAsset"];
    return [RealUtility isValid:reasonDict] && ([RealUtility isValid:reasonString] || [RealUtility isValid:reasonImageAsset]);
}

- (IBAction)btnBackpressed:(id)sender {
    if (self.currentReasonIndex == 0) {
        if (self.addReasonType ==  AddReasonTypeAdd){
            [[AgentListingModel shared].reasonDict removeObjectForKey:self.currentLangIndex];
        }
        [super btnBackPressed:sender];
    }else{
        self.currentReasonIndex--;
        if (self.currentReasonIndex == 1) {
            [self reasonHeaderButtonDidPress:self.reasonTwoButton];
        }else if (self.currentReasonIndex == 0){
            [self reasonHeaderButtonDidPress:self.reasonOneButton];
        }
    }
}
- (IBAction)savebutton:(id)sender {
    
    if (self.currentReasonIndex <2 && [self checkReasonIsValid:self.currentReasonIndex]) {
        self.nextReasonHint = NO;
        if(self.currentReasonIndex == 0){
            [self reasonHeaderButtonDidPress:self.reasonTwoButton];
        }else{
            [self reasonHeaderButtonDidPress:self.reasonThreeButton];
        }
    }else if(![self checkReasonIsValid:self.currentReasonIndex] && [self checkReasonIsValid:self.currentReasonIndex+1]){
        if(self.currentReasonIndex == 0){
            [self reasonHeaderButtonDidPress:self.reasonTwoButton];
        }else{
            [self reasonHeaderButtonDidPress:self.reasonThreeButton];
        }
    }else if(self.addReasonType == AddReasonTypeAdd){
        [self reorderReasonIfNeed];
        self.navigationController.delegate = self;
        for (UIViewController *viewController in self.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[ApartmentDetailViewController class]]) {
                ((ApartmentDetailViewController*)viewController).needCustomBackground = self.needCustomBackground;
                if(((ApartmentDetailViewController*)viewController).needCustomBackground){
                    self.navigationController.delegate = self;
                }
                [self.navigationController popToViewController:viewController animated:YES];
                break;
            }
        }
    }else{
        [self reorderReasonIfNeed];
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Saved Posting Details_Three Reasons"];
        
        self.addReasonType = AddReasonTypeEdit;
        Uploadphoto *uploadPhotoVc = [[Uploadphoto alloc] initWithNibName:@"Uploadphoto" bundle:nil];
        uploadPhotoVc.needCustomBackground = self.needCustomBackground;
        uploadPhotoVc.underLayViewController = self.underLayViewController;
        if(uploadPhotoVc.needCustomBackground){
            self.navigationController.delegate = self;
        }
        [self.navigationController pushViewController:uploadPhotoVc animated:YES];
    }
}

- (void)reorderReasonIfNeed{
    NSMutableDictionary *currentReasonLangDict= [self getCurrentLangDict];
    NSMutableArray *originalArray = [[NSMutableArray alloc]init];
    for (int i = 0; i<3; i++) {
        NSDictionary *reasonDict = currentReasonLangDict[@(i)];
        if ([reasonDict valid]) {
            [originalArray addObject:reasonDict];
            [currentReasonLangDict removeObjectForKey:@(i)];
        }
    }
    for (int i = 0; i< originalArray.count; i++) {
        NSDictionary *reasonDict = originalArray[i];
        [currentReasonLangDict setObject:reasonDict forKey:@(i)];
    }
    
}

- (void)checkEmptyToChangeSaveButtonColor {
    
    if ([self getCurrentLangDict].allKeys.count>0) {
        BOOL enable = NO;
        for (NSNumber *reasonIndex in [[self getCurrentLangDict]copy]) {
            if (![reasonIndex isKindOfClass:[NSString class]]) {
                NSDictionary *dict = [self getCurrentLangDict][reasonIndex];
                for (id object in dict) {
                    if ([RealUtility isValid:object]) {
                        enable = YES;
                        break;
                    }
                }
            }
        }
        self.nextbutton.enabled = enable;
    } else {
        self.nextbutton.enabled = NO;
    }
}

-(IBAction)reasonHeaderButtonDidPress:(UIButton*)button{
    if (button == self.reasonTwoButton) {
        if (![self checkReasonIsValid:0] && ![self checkReasonIsValid:1]) {
            return;
        }
    }else if (button == self.reasonThreeButton){
        if (![self checkReasonIsValid:1] && ![self checkReasonIsValid:2]) {
            return;
        }
    }
    if (!button.selected) {
        int reasonIndex = (int)button.tag;
        for (UIButton *headerButton in self.reasonHeaderButtons) {
            headerButton.selected = button == headerButton;
        }
        self.enableUpdate = NO;
        self.currentReasonIndex = reasonIndex;
        [self.reasonSwipeView scrollToItemAtIndex:reasonIndex duration:0.3];
        
    }
    
}

-(void)hideKeyBoard{
    [self.view endEditing:YES];
}


#pragma mark - SwipeViewDataSoureAndDelgate
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    return 3;
}

- (BOOL)swipeView:(SwipeView *)swipeView shouldSelectItemAtIndex:(NSInteger)index{
    return YES;
}
- (void)swipeViewDidEndScrollingAnimation:(SwipeView *)swipeView{
    self.enableUpdate = YES;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    AddReasonView *reasonView;
    if (!view) {
        view =[[UIView alloc]initWithFrame:swipeView.bounds];
        view.backgroundColor = [UIColor clearColor];
        reasonView = [[AddReasonView alloc]initWithFrame:swipeView.bounds];
        reasonView.backgroundColor = [UIColor clearColor];
        reasonView.tag = 1000;
        
        [view addSubview:reasonView];
    }else{
        reasonView = (AddReasonView*) [view viewWithTag:1000];
        
    }
    
    reasonView.itemIndex = (int)index;
    reasonView.delegate = self;
    NSDictionary *langDict = [self getCurrentLangDict];
    NSDictionary *reasonDict = langDict[@(index)];
    NSString *reasonString = reasonDict[@"reasonString"];
    PHAsset *reasonAsset = reasonDict[@"reasonImageAsset"];
    
    [reasonView configureWithReason:reasonString withImageAsset:reasonAsset];
    
    return view;
}

-(NSMutableDictionary*)getCurrentLangDict{
    NSNumber *langKey = self.currentLangIndex;
    NSMutableDictionary *currentReasonLangDict= [AgentListingModel shared].reasonDict[langKey];
    return currentReasonLangDict;
}

-(void) updateReason:(NSString*)reason withImageAsset:(PHAsset*)imageAsset withReasonIndex:(int)index{
    if (self.enableUpdate) {
        if (![AgentListingModel shared].reasonDict) {
            [AgentListingModel shared].reasonDict = [[NSMutableDictionary alloc]init];
        }
        NSMutableDictionary *currentReasonLangDict= [self getCurrentLangDict];
        if (!currentReasonLangDict) {
            currentReasonLangDict = [[NSMutableDictionary alloc]init];
        }
        reason = [reason trim];
        NSDictionary *newReason = [self createReasonDictWithReason:reason withImageAsset:imageAsset];
        [currentReasonLangDict setObject:newReason forKey:@(index)];
        [[AgentListingModel shared].reasonDict setObject:currentReasonLangDict forKey:self.currentLangIndex];
    }
    [self checkEmptyToChangeSaveButtonColor];
}

-(NSDictionary*) createReasonDictWithReason:(NSString*)reason withImageAsset:(PHAsset*)asset{
    NSMutableDictionary *reasonDict= [[NSMutableDictionary alloc]init];
    if ([RealUtility isValid:reason]) {
        [reasonDict setObject:reason forKey:@"reasonString"];
    }
    
    if ([RealUtility isValid:asset]) {
        [reasonDict setObject:asset forKey:@"reasonImageAsset"];
    }
    return reasonDict;
}

-(void)presentImagePicker:(AddReasonView*)reasonView{
    self.pendingAddReasonView = reasonView;
    [kAppDelegate showImagePickerOn:self];
}

-(void)presentEditReasonImageActionSheet:(AddReasonView*)reasonView{
    self.pendingAddReasonView = reasonView;
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:nil
                                           cancelButtonTitle:JMOLocalizedString(@"common__cancel", nil)
                                      destructiveButtonTitle:JMOLocalizedString(@"create_listing__remove_photo", nil)
                                           otherButtonTitles:JMOLocalizedString(@"create_listing__photo_library", nil),nil];
    as.didDismissBlock =^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            [self.pendingAddReasonView configureWithReason:self.pendingAddReasonView.reasonTextView.text withImageAsset:nil];
            [self updateReason:self.pendingAddReasonView.reasonTextView.text withImageAsset:nil withReasonIndex:self.currentReasonIndex];
            self.pendingAddReasonView = nil;
        }else if(buttonIndex ==1){
            
            [self presentImagePicker:reasonView];
        }else{
            self.pendingAddReasonView = nil;
        }
        //        DDLogDebug(@"Chose %@", [actionSheet buttonTitleAtIndex:buttonIndex]);
    };
    [as showInView:self.view];
}
#pragma mark - AddReasonViewDelgate
-(void)addReasonView:(AddReasonView*)reasonView didPressAddImageButtonAtIndex:(int)viewIndex{
    [self presentImagePicker:reasonView];
}

-(void)addReasonView:(AddReasonView *)reasonView reasonTextDidChangeAtIndex:(int)viewIndex{
    [self checkEmptyToChangeSaveButtonColor];
    
    [self updateReason:reasonView.reasonTextView.text withImageAsset:reasonView.imageAsset withReasonIndex:self.currentReasonIndex];
}

-(void)addReasonView:(AddReasonView *)reasonView reasonImageButtonDidPress:(int)viewIndex{
    [self checkEmptyToChangeSaveButtonColor];
    [self presentEditReasonImageActionSheet:reasonView];
    
}

#pragma mark -UIImagePickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    // assets contains PHAsset objects.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    
    // this one is key
    requestOptions.synchronous = true;
    PHContentEditingInputRequestOptions *editOptions = [[PHContentEditingInputRequestOptions alloc]init];
    
    PHAsset *asset = [assets firstObject];
    [self.pendingAddReasonView configureWithReason:self.pendingAddReasonView.reasonTextView.text withImageAsset:asset];
    [self updateReason:self.pendingAddReasonView.reasonTextView.text withImageAsset:asset withReasonIndex:self.currentReasonIndex];
    self.pendingAddReasonView = nil;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    return picker.selectedAssets.count<1;
}


#pragma mark - text field

@end