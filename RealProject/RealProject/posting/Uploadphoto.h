//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "Collectionviewcell.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "ELCImagePickerHeader.h"
@interface Uploadphoto : BaseViewController<LXReorderableCollectionViewDataSource,LXReorderableCollectionViewDelegateFlowLayout>

//// upload photo collectionview

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *presentTopBarHeightConstraint;
@property (nonatomic, strong) IBOutlet LXReorderableCollectionViewFlowLayout* collectionLayout;
@property(nonatomic, retain) NSMutableArray *collectionviewdata;
@property(nonatomic, retain) NSMutableArray *tableData;
@property(strong, nonatomic) IBOutlet UICollectionView *CollectionView;
@property(nonatomic, assign) BOOL addOneAddButton;
@property(nonatomic, retain) NSIndexPath *checkedCell;
@property(nonatomic, strong) NSMutableArray *chosenImages;
@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property(strong, nonatomic) IBOutlet UIButton *editDoneButton;
@property (strong,nonatomic) IBOutlet UIButton *dismissButton;
@property(strong, nonatomic) IBOutlet UIView *presentTopBar;
@property (strong, nonatomic) IBOutlet UILabel *creatPhotoAlbumTitle;
@property (strong, nonatomic) IBOutlet UILabel *tenPhotosLabel;
@property (strong, nonatomic) IBOutlet UILabel *toCompleteTheStory;
-(IBAction)editModeDoneButtonDidPress:(id)sender;
@end
