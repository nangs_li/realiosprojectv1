//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Uploadphoto.h"
#import "ApartmentDetailViewController.h"
#import "RealUtility.h"
#import "uploadPhotoCollectionCell.h"
#import "CTAssetsPickerController.h"
#import "CTAssetsPickerController+Utility.h"
#import "Addlanguage.h"

// Mixpanel
#import "Mixpanel.h"

@interface Uploadphoto () <UploadPhotoCellDelegate,UIGestureRecognizerDelegate,CTAssetsPickerControllerDelegate>
@property (nonatomic,strong) PhotoEditorView *coverPhotoEditorView;
@property (nonatomic,assign) BOOL enterEditMode;
@property (nonatomic,strong) PHAsset *coverImage;

//@property (nonatomic,strong) NSIndexPath *coverImageIndexPath;
@end
@implementation Uploadphoto
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setEnterEditMode:(BOOL)enterEditMode{
    _enterEditMode = enterEditMode;
    self.dismissButton.hidden = _enterEditMode;
    self.editDoneButton.hidden = !_enterEditMode;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([AgentListingModel shared].chosenImages.count > 0) {
        if ([AgentListingModel shared].totalSelectedImageCount < [AgentListingModel shared].chosenImages.count) {
            [[AgentListingModel shared].chosenImages removeObjectAtIndex:0];
        }
        
        if (!self.chosenImages) {
            self.chosenImages = [NSMutableArray arrayWithArray:[AgentListingModel shared].chosenImages];
            [self reloadColletionView:NO];
        }
        self.coverImage = [self.chosenImages firstObject];
    }
    if (self.chosenImages == nil) {
        self.nextbutton.enabled = NO;
    } else {
        self.nextbutton.enabled = YES;
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self exitEditMode];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
    
}

-(void)applicationDidBecomeActive{
    [self startEditMode];
}

-(void)applicationWillResignActive{
    if (self.enterEditMode) {
        [self editModeDoneButtonDidPress:nil];
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.nextbutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(applicationDidBecomeActive) name:kNotificationApplcationDidBecomeActive object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(applicationWillResignActive) name:kNotificationApplicationWillResignActive object:nil];
    [self.CollectionView registerClass:[uploadPhotoCollectionCell class]
            forCellWithReuseIdentifier:@"uploadPhotoCollectionCell"];
    self.collectionviewdata = [[NSMutableArray alloc]
                               initWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8",
                               @"9", nil];
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                             action:@selector(handleLongPressGesture:)];
    longPressGestureRecognizer.delegate = self;
    [self.CollectionView addGestureRecognizer:longPressGestureRecognizer];
    [self reloadColletionView:NO];
    
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
    self.creatPhotoAlbumTitle.text=JMOLocalizedString(@"listing_photo__title", nil);
    [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil)
                     forState:UIControlStateNormal];
   
    self.tenPhotosLabel.text =
    JMOLocalizedString(@"listing_photo__ten_photos", nil);
    
    self.toCompleteTheStory.text =
    JMOLocalizedString(@"listing_photo__subtitle", nil);

    
}

#pragma mark Button-----------------------------------------------------------------------------------------------
//// - btnbackpress
- (IBAction)btnbackpress:(id)sender {
    [self btnBackPressed:sender];
}
//// - nextbuttonpressed
- (IBAction)nextbuttonpressed:(id)sender {
      [self exitEditMode];
        __weak __typeof(self)weakSelf = self;
    [self.coverPhotoEditorView cropImageViewBlock:^(UIImage *croppedImage) {
        if ([RealUtility isValid:weakSelf.chosenImages]) {
            [AgentListingModel shared].totalSelectedImageCount = (int)weakSelf.chosenImages.count;
            NSMutableArray *tempArray= [NSMutableArray arrayWithArray:weakSelf.chosenImages];
            
            [tempArray insertObject:croppedImage atIndex:0];
            [AgentListingModel shared].chosenImages = tempArray;
            
            ApartmentDetailViewController *previewController = [[ApartmentDetailViewController alloc]init];
            [previewController compressImageArrayAndSetupObserve];
            previewController.needCustomBackground = weakSelf.needCustomBackground;
            AgentProfile *previewProfile = [[AgentListingModel shared] convertToPreviewAgentProfile];
            previewController.underLayViewController = weakSelf.underLayViewController;
            previewController.pendingAgentProfile = previewProfile;
            if(previewController.needCustomBackground){
                weakSelf.navigationController.delegate = self;
            }
            
            // Mixpanel
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Saved Posting Details_Photos"];
            
            [weakSelf.navigationController pushViewController:previewController animated:YES];
        }
    }];
    
}
//// -RemovePhotoPrssd
- (void)RemovePhotoPrssd:(id)sender {
    DDLogInfo(@"before remove prssed self.chosenImages---->%@",
              self.chosenImages);
    DDLogInfo(@"after remove prssed self.chosenImages---->%@", self.chosenImages);
}
//// CoverPrssd
- (void)addPhoto{
    DDLogInfo(@"CoverPrssd");
    if(self.needCustomBackground){
        self.navigationController.delegate = self;
    }
    [AppDelegate getAppDelegate].hasBeenSelectedUploadPhoto =self.chosenImages;
    [kAppDelegate showImagePickerOn:self];
    
}

-(void)changeCoverPhoto:(NSIndexPath*)fromIndexPath{
    if (self.chosenImages.count > fromIndexPath.row) {
       return;
    }
        PHAsset *newCoverPhoto = self.chosenImages[fromIndexPath.row ];
    if (self.coverImage != newCoverPhoto) {
        self.coverImage = newCoverPhoto;
        [AgentListingModel shared].coverPhotoScrollView = nil;
        [self reloadColletionView:YES];
        
    }
}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)gestureRecognizer {
    switch(gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan: {
            [self startEditMode];
        }
    }
}

-(void)startEditMode{
    if (!self.enterEditMode && [RealUtility isValid:self.chosenImages]) {
        self.collectionLayout.longPressGestureRecognizer.minimumPressDuration = 0.01;
        self.CollectionView.scrollEnabled = NO;
        self.enterEditMode = YES;
        self.editDoneButton.hidden = NO;
        [self reloadColletionView:NO];
    }
}

-(void)reloadColletionView:(BOOL)save{
    if (save) {
        [_coverPhotoEditorView cropImageViewBlock:^(UIImage *croppedImage) {
            if ([RealUtility isValid:self.chosenImages]) {
                [AgentListingModel shared].totalSelectedImageCount = (int)self.chosenImages.count;
                NSMutableArray *tempArray= [NSMutableArray arrayWithArray:self.chosenImages];
                [tempArray insertObject:croppedImage atIndex:0];
                [AgentListingModel shared].chosenImages = tempArray;
            }
        }];
    }
    [self.CollectionView reloadData];
    [self toggleJiggling];
}

-(void)toggleJiggling{
    if (self.enterEditMode) {
        [self.CollectionView.visibleCells makeObjectsPerformSelector:@selector(startJiggling)];
    }
}

- (void)exitEditMode{
    if(self.enterEditMode){
        self.collectionLayout.longPressGestureRecognizer.minimumPressDuration = 0.5;
        self.editDoneButton.hidden = YES;
        self.enterEditMode = NO;
        self.CollectionView.scrollEnabled = YES;
        [self reloadColletionView:NO];
    }
}

-(IBAction)editModeDoneButtonDidPress:(id)sender{
    [self exitEditMode];
    [self reloadColletionView:NO];
}
#pragma mark - UICollectionViewDataSource methods-----------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)theCollectionView
     numberOfItemsInSection:(NSInteger)theSectionIndex {
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    uploadPhotoCollectionCell *photoCell = (uploadPhotoCollectionCell *)[collectionView
                                                                         dequeueReusableCellWithReuseIdentifier:@"uploadPhotoCollectionCell"
                                                                         forIndexPath:indexPath];
    photoCell.delegate =self;
    int imageIndex = (int)indexPath.row ;
    PHAsset *selectedImage =  [self getSelectedImageByIndex:imageIndex];
    if (indexPath.row ==0) {
        photoCell.previousPhotoEditorScrollView = [AgentListingModel shared].coverPhotoScrollView ;
        [photoCell configureCellWithImage:selectedImage withType:UploadPhotoCellCoverPhoto withIndexPath:indexPath];
        self.coverPhotoEditorView = photoCell.coverPhotoEditorView;
        if ([self.chosenImages valid]) {
            [photoCell enableEditMode:self.enterEditMode];
        }else{
            [photoCell enableEditMode:NO];
        }
    }else{
        UploadPhotoCellType cellType = UploadPhotoCellEmpty;
        if ([RealUtility isValid:[self chosenImages]]) {
            if (selectedImage) {
                cellType = UploadPhotoCellEditView;
                
            }else{
                cellType = UploadPhotoCellSelect;
            }
        }else{
            cellType = UploadPhotoCellSelect;
        }
        [photoCell configureCellWithImage:selectedImage withType:cellType   withIndexPath:indexPath];
        if (cellType == UploadPhotoCellEditView || cellType == UploadPhotoCellEditCoverView) {
            [photoCell enableEditMode:self.enterEditMode];
        }else{
            [photoCell enableEditMode:NO];
        }
    }
    photoCell.cellCoverImage.text=JMOLocalizedString(@"listing_photo__cover_photo", nil);
    //Date: 2016-04-01
    //Author: Ken
    //Hardcode a smaller font size to fit the longest translation of "Select",
    //Should improve coding to autosizing the font size later.
    photoCell.selectLabel.text=JMOLocalizedString(@"listing_photo__select", nil);
    photoCell.coverPhotoLabel.text=JMOLocalizedString(@"listing_photo__cover_photo", nil);
    photoCell.clickToSelectTheCoverPhotoLabel.text=@"";
    return photoCell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"collectionViewindexPath=%@", indexPath);
    
    if(![RealUtility isValid:self.chosenImages]){
        [self addPhoto];
    }else if (indexPath.row > self.chosenImages.count-1) {
        [self addPhoto];
    }
}

#pragma mark - LXReorderableCollectionViewDelegateFlowLayout methods----------------------------------------------

- (void)LXReorderableCollectionViewDidEnterEditMode{
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        return YES;
    }
    return self.enterEditMode;
}

- (void)collectionView:(UICollectionView *)collectionView
                layout:
(UICollectionViewLayout *)collectionViewLayout
willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"will begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                layout:
(UICollectionViewLayout *)collectionViewLayout
didBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"did begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                layout:
(UICollectionViewLayout *)collectionViewLayout
willEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"will end drag");
    
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"did end drag");
    [self reloadColletionView:YES];
}

-(void)UploadPhotoCellDidSavePhotoEditorScrollView:(UIScrollView *)scrollView{
    [AgentListingModel shared].coverPhotoScrollView  =scrollView;
}

-(void)UploadPhotoCellRemoveButtonDidPress:(NSIndexPath *)indexPath{
    int photoIndex = (int)indexPath.row ;
    PHAsset *removeImage = [self getSelectedImageByIndex:photoIndex];
    [self.chosenImages removeObject:removeImage];
    if (removeImage == self.coverImage) {
//        if([RealUtility isValid:self.chosenImages]){
//            [self changeCoverPhoto:[NSIndexPath indexPathForRow:1 inSection:0]];
//        }else{
            self.coverImage = nil;
            [AgentListingModel shared].coverPhotoScrollView  = nil;
        [self reloadColletionView:YES];
//        }
    }else{
        [self reloadColletionView:YES];
    }
    
    if (![self.chosenImages valid]) {
        [AgentListingModel shared].chosenImages = nil;
        [self exitEditMode];
    }
    self.nextbutton.enabled = [self.chosenImages valid];

}
#pragma mark - LXReorderableCollectionViewDataSource methods----------------------------------------------
- (void)collectionView:(UICollectionView *)collectionView
       itemAtIndexPath:(NSIndexPath *)fromIndexPath
   willMoveToIndexPath:(NSIndexPath *)toIndexPath {
    DDLogInfo(@"before moveing self.chosenImages%@", self.chosenImages);
    DDLogInfo(@"fromIndexPath.item%ld", (long)fromIndexPath.item);
    DDLogInfo(@"toIndexPath.item%ld", (long)toIndexPath.item);
    
    int fromPhotoIndex = (int)fromIndexPath.row ;
    int toPhotoIndex = (int)toIndexPath.row ;
    UIImage *tempimage = self.chosenImages[fromPhotoIndex];
    [self.chosenImages removeObjectAtIndex:fromPhotoIndex];
    [self.chosenImages insertObject:tempimage atIndex:toPhotoIndex];
    
    //    [self reloadColletionView];
    //    if (coverImageIndexPath) {
    //        self.coverImageIndexPath = coverImageIndexPath;
    //    }
    DDLogInfo(@"after moveing self.chosenImages%@", self.chosenImages);
}

- (BOOL)collectionView:(UICollectionView *)collectionView
canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    int photoIndex = (int)indexPath.row ;
    PHAsset *indexPathImage =[self getSelectedImageByIndex:photoIndex];
    return self.enterEditMode  && indexPathImage!=nil;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
       itemAtIndexPath:(NSIndexPath *)fromIndexPath
    canMoveToIndexPath:(NSIndexPath *)toIndexPath {
    if (toIndexPath == 0) {
        [AgentListingModel shared].coverPhotoScrollView = nil;
    }
    /*
     if (toIndexPath.row ==0) {
     [self changeCoverPhoto:fromIndexPath];
     }
     */
    int photoIndex = (int)toIndexPath.row ;
    PHAsset *toIndexPathImage =[self getSelectedImageByIndex:photoIndex];
    int fromPhotoIndex = (int)fromIndexPath.row ;
    PHAsset *fromIndexPathImage =[self getSelectedImageByIndex:fromPhotoIndex];
    
    return  (toIndexPathImage!= nil)&&(fromIndexPathImage!= nil);
}

#pragma mark ELCImagePickerControllerDelegate Methods-------------------------------------------------------------

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    // assets contains PHAsset objects.
    
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;
    if (![RealUtility isValid:assets]) {
        return;
    }
            self.chosenImages = [NSMutableArray arrayWithArray:assets];
    if (!self.coverImage) {
        self.coverImage  = [self.chosenImages firstObject];
    }
    [self reloadColletionView:YES];
    [picker.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    NSInteger max = 10;
    if (picker.selectedAssets.count >= max){
        
        [picker showHint:[NSString stringWithFormat:JMOLocalizedString(@"chatroom__max_photo", nil),[NSString stringWithFormat:@"%ld", (long)max]]];
    }
    return (picker.selectedAssets.count < max);
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didDeselectAsset:(PHAsset *)asset{
    [picker hideHint];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)toInterfaceOrientation {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
    }
}

-(PHAsset*)getSelectedImageByIndex:(int)index{
    PHAsset *selectedImageAtIndex = nil;
    if (self.chosenImages.count > index) {
        selectedImageAtIndex = self.chosenImages[index];
    }
    return selectedImageAtIndex;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"chosenImages"]) {
        DDLogDebug(@"%@", change);
    }
}
@end