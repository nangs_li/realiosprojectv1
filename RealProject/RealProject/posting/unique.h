//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>
#import "RealSelectTableViewController.h"

typedef enum {
    AddSloganTypeNew = 0,
    AddSloganTypeEdit,
    AddSloganTypeAdd
}AddSloganType;

@interface unique : RealSelectTableViewController
@property (nonatomic,assign) AddSloganType type;
@property(nonatomic, retain) NSIndexPath *checkedCell;
@property (nonatomic,strong) NSNumber *currentLangIndex;
@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property (nonatomic,strong) IBOutlet UIButton *langButton;

@property (nonatomic,strong) IBOutlet NSLayoutConstraint *langButtonHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *uniqueTopTitle;
@property (strong, nonatomic) IBOutlet UILabel *uniqueTopTitle2;
@end
