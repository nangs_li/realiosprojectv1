//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "unique.h"
#import "SpokenLanguagescell.h"
#import "Threereason.h"
#import "RealUtility.h"
#import "ActionSheetPicker.h"

// Mixpanel
#import "Mixpanel.h"

@interface unique ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSMutableArray *sloganArray;
@property (nonatomic,strong) NSMutableArray *selectedArray;
@property (nonatomic,strong) NSNumber *currentSloganIndex;
@end
@implementation unique

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.type == AddSloganTypeEdit) {
        if ([AgentListingModel shared].reasonDict.allKeys.count >0) {
            self.currentLangIndex = [[AgentListingModel shared].reasonDict.allKeys lastObject];
        }
    }
    [self setupWithType:self.type];
}

-(void)setupWithType:(AddSloganType)type{
    
    if (type == AddReasonTypeAdd) {
        [self.nextbutton setTitle:JMOLocalizedString(@"common_add", nil) forState:UIControlStateNormal];
        self.langButton.userInteractionEnabled = NO;
        self.langButtonHeightConstraint.constant = 0;
        self.langButton.hidden = YES;
    }else if (type == AddReasonTypeEdit){
        if ([AgentListingModel shared].reasonDict.allKeys.count >1) {
            self.langButton.hidden = NO;
            self.langButton.userInteractionEnabled = YES;
            self.langButtonHeightConstraint.constant = 27;
        }else{
            self.langButton.userInteractionEnabled = NO;
            self.langButtonHeightConstraint.constant = 0;
            self.langButton.hidden = YES;
        }
        [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    }else if (type == AddReasonTypeNew){
         self.langButton.hidden = YES;
        self.langButton.userInteractionEnabled = NO;
        self.langButtonHeightConstraint.constant = 0;
        [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
    }
    
    if (self.currentSloganIndex) {
        self.nextbutton.enabled = YES;
    }else{
        self.nextbutton.enabled = NO;
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.langButton setBackgroundImage:[UIImage imageWithBorder:[UIColor lightGrayColor] size:CGSizeMake(20, 20) fillcolor:[UIColor clear]] forState:UIControlStateNormal];
    [self.langButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.langButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.langButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.langButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.disableSelection = NO;
    self.sloganArray = [[SystemSettingModel shared].SystemSettings.SloganList copy];
    self.titleArray =[[[SystemSettingModel shared]getSettingLanguageStringList] copy];
    //    self.selectedArray = [self getSelectedReasonLang];
    [self configureWithTitles:self.titleArray selection:self.selectedArray indicatorName:nil];
    if (!self.currentLangIndex) {
        self.currentLangIndex = @([SystemSettingModel shared].selectSystemLanguageList.Index);
    }else{
        self.currentLangIndex = self.currentLangIndex;
    }
    if (self.currentSloganIndex) {
        self.nextbutton.enabled = YES;
    }else{
        self.nextbutton.enabled = NO;
    }
    
    
    [self.nextbutton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pop_dottedline"]]];
}
#pragma mark - setUpFixedLabelTextAccordingToSelectedLanguage
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.uniqueTopTitle.text=JMOLocalizedString(@"slogan__title", nil);
     self.uniqueTopTitle2.text=JMOLocalizedString(@"slogan__subtitle", nil);
    [self.nextbutton setTitle:JMOLocalizedString(@"common__next", nil) forState:UIControlStateNormal];
}

#pragma mark - Button
//// - btnBackpressed
- (IBAction)btnBackpressed:(id)sender {
    if(self.type == AddSloganTypeAdd){
        [[AgentListingModel shared].reasonDict removeObjectForKey:self.currentLangIndex];
    }
    [self btnBackPressed:sender];
}

//// - btnBackpressed
- (IBAction)langButtonpressed:(id)sender {
    [self.view endEditing:YES];
    NSMutableArray *selectedLang = [[NSMutableArray alloc]init];
    for (int i =0; i<[AgentListingModel shared].reasonDict.allKeys.count; i++) {
        [selectedLang addObject:[[SystemSettingModel shared]getSettingLanguageString:[AgentListingModel shared].reasonDict.allKeys[i]]];
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select a Langugae"
                                            rows:selectedLang
                                initialSelection:[[AgentListingModel shared].reasonDict.allKeys indexOfObject:self.currentLangIndex]
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           self.currentLangIndex = [AgentListingModel shared].reasonDict.allKeys[selectedIndex];
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         DDLogDebug(@"Block Picker Canceled");
                                     }
                                          origin:sender];
}

-(void)updateReasonDict:(NSNumber*)sloganIndex{
    NSMutableDictionary *reasonDict = [self getCurrentLangDict];
    if (!reasonDict) {
        reasonDict = [[NSMutableDictionary alloc]init];
    }
    [reasonDict setObject:sloganIndex forKey:@"sloganIndex"];
    if (![AgentListingModel shared].reasonDict) {
        [AgentListingModel shared].reasonDict = [[NSMutableDictionary alloc]init];
    }
    [[AgentListingModel shared].reasonDict setObject:reasonDict forKey:_currentLangIndex];
    
    
}
//// - nextbuttonpressed
- (IBAction)nextbuttonPressed:(id)sender {
    Threereason *threeReasonVc = [[Threereason alloc] initWithNibName:@"Threereason" bundle:nil];
    threeReasonVc.needCustomBackground = self.needCustomBackground;
    if(threeReasonVc.needCustomBackground){
        self.navigationController.delegate = self;
    }
    threeReasonVc.underLayViewController = self.underLayViewController;
    if (self.type == AddSloganTypeNew) {
        // Mixpanel
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Saved Posting Details_Slogan"];
        threeReasonVc.addReasonType = AddReasonTypeNew;
        self.type = AddSloganTypeEdit;
    }else if (self.type == AddSloganTypeAdd){
        threeReasonVc.addReasonType = AddReasonTypeAdd;
        threeReasonVc.currentLangIndex = self.currentLangIndex;
    }else if (self.type == AddSloganTypeEdit){
        threeReasonVc.addReasonType = AddReasonTypeEdit;
    }
    //self.navigationController.delegate = self;
    [self.navigationController pushViewController:threeReasonVc animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    self.nextbutton.enabled = YES;
    
    self.currentSloganIndex = @(indexPath.row);
}


-(NSMutableDictionary*)getCurrentLangDict{
    NSNumber *langKey = self.currentLangIndex;
    NSMutableDictionary *currentReasonLangDict= [AgentListingModel shared].reasonDict[langKey];
    return currentReasonLangDict;
}

-(NSArray*)getSelectedReasonLang{
    NSMutableArray *selectedTitleArray = [[NSMutableArray alloc]init];
    NSDictionary *reasonDict = [[AgentListingModel shared].reasonDict copy];
    NSArray *selectedLangIndexArray = [reasonDict.allKeys copy];
    for (NSNumber *indexNumber in selectedLangIndexArray) {
        NSString *title =[[SystemSettingModel shared]getSettingLanguageString:indexNumber];
        [selectedTitleArray addObject:title];
    }
    return selectedTitleArray;
}

-(void)setCurrentLangIndex:(NSNumber*)currentLangIndex{
    _currentLangIndex = currentLangIndex;
    NSDictionary *langDict = [self getCurrentLangDict];
    NSNumber *positionIndex = langDict[@"sloganIndex"];
    int positionInt = [positionIndex intValue];
    self.titleArray = [[SystemSettingModel shared]getSloganStringListByLangIndex:_currentLangIndex];
    if (self.titleArray.count < [positionIndex intValue]) {
        positionIndex =0;
    }
    if (positionInt > self.titleArray.count) {
        positionInt = 0;
    }
    
    self.currentSloganIndex = @(positionInt);
    
    self.selectedArray = [@[self.titleArray[positionInt]]mutableCopy];
    [self configureWithTitles:self.titleArray selection:self.selectedArray indicatorName:nil];
    NSString *currentLangTitle = [[SystemSettingModel shared]getSettingLanguageString:_currentLangIndex];
    [self.langButton setTitle:currentLangTitle forState:UIControlStateNormal];
    [self.tableView reloadData];
}

- (void)setCurrentSloganIndex:(NSNumber *)currentSloganIndex{
    _currentSloganIndex = currentSloganIndex;
    [self updateReasonDict:_currentSloganIndex];
}

@end