//
//  uploadPhotoCollectionCell.h
//  productionreal2
//
//  Created by Alex Hung on 15/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoEditorView.h"
typedef enum {
    UploadPhotoCellEmpty= 0,
    UploadPhotoCellSelect,
    UploadPhotoCellEditView,
    UploadPhotoCellEditCoverView,
    UploadPhotoCellAddPhoto,
    UploadPhotoCellCoverPhoto
}UploadPhotoCellType;


@protocol UploadPhotoCellDelegate <NSObject>

-(void) UploadPhotoCellDidSavePhotoEditorScrollView:(UIScrollView*)scrollView;
-(void) UploadPhotoCellRemoveButtonDidPress:(NSIndexPath*)indexPath;
@end


@interface uploadPhotoCollectionCell : UICollectionViewCell <PhotosEditorViewDelegate>
@property (nonatomic,strong) id<UploadPhotoCellDelegate> delegate;
@property (nonatomic,assign) BOOL isJiggling;
@property (nonatomic,strong) IBOutlet UIView *selectView;
@property (nonatomic,strong) IBOutlet UIView *addPhotoView;
@property (nonatomic,strong) IBOutlet UIView *coverPhotoview;
@property (nonatomic,strong) IBOutlet UIView *editView;
@property (nonatomic,strong) IBOutlet UIImageView *editImageView;
@property (nonatomic,strong) IBOutlet UIView *editNormalImageView;
@property (nonatomic,strong) IBOutlet UIView *editCoverImageView;
@property (nonatomic,strong) IBOutlet PhotoEditorView *coverPhotoEditorView;
@property (nonatomic,strong) IBOutlet UIView *coverPhotoHintView;
@property (nonatomic,strong) IBOutlet UIScrollView *previousPhotoEditorScrollView;
@property (nonatomic,strong) IBOutlet UIButton *removeButton;
@property (nonatomic,strong) NSIndexPath *indexPath;
@property (strong, nonatomic) IBOutlet UILabel *coverPhotoLabel;
@property (strong, nonatomic) IBOutlet UILabel *clickToSelectTheCoverPhotoLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectLabel;
@property (strong, nonatomic) IBOutlet UILabel *cellCoverImage;
-(void)configureCellWithImage:(PHAsset*)image withType:(UploadPhotoCellType)type withIndexPath:(NSIndexPath*)indexPath;
-(void)enableEditMode:(BOOL)isEditing;
-(void)startJiggling;
-(IBAction)removeButtoDidPress:(id)sender;
@end
