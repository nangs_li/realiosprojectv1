//
//  uploadPhotoCollectionCell.m
//  productionreal2
//
//  Created by Alex Hung on 15/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "uploadPhotoCollectionCell.h"
#import "RealUtility.h"
#import <Photos/Photos.h>
#define degreesToRadians(x) (M_PI * (x) / 180.0)
#define kAnimationRotateDeg 1.0


@implementation uploadPhotoCollectionCell

- (void)awakeFromNib {
    // Initialization code
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews =
        [[NSBundle mainBundle] loadNibNamed:@"uploadPhotoCollectionCell"
                                      owner:self
                                    options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0]
              isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    self.coverPhotoEditorView.delegate = self;
    self.selectView.layer.borderWidth =1.0f;
    self.selectView.layer.borderColor = [UIColor lightTextColor].CGColor;
    
    self.editNormalImageView.layer.borderWidth =1.0f;
    self.editNormalImageView.layer.borderColor = [UIColor lightTextColor].CGColor;
    
    self.coverPhotoview.layer.borderWidth =1.0f;
    self.coverPhotoview.layer.borderColor = [UIColor lightTextColor].CGColor;
    
    self.editCoverImageView.layer.borderWidth = 1.0f;
    self.editCoverImageView.layer.borderColor = RealBlueColor.CGColor;
    return self;
}


-(void)configureCellWithImage:(PHAsset *)asset withType:(UploadPhotoCellType)type withIndexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
    switch (type) {
        case UploadPhotoCellEmpty:
            self.selectView.hidden = YES;
            self.addPhotoView.hidden = YES;
            self.coverPhotoview.hidden = YES;
            self.editView.hidden = YES;
            break;
        case UploadPhotoCellSelect:
            self.selectView.hidden = NO;
            self.addPhotoView.hidden = YES;
            self.coverPhotoview.hidden = YES;
            self.editView.hidden = YES;
            break;
        case UploadPhotoCellAddPhoto:
            self.selectView.hidden = YES;
            self.addPhotoView.hidden = NO;
            self.coverPhotoview.hidden = YES;
            self.editView.hidden = YES;
            break;
        case UploadPhotoCellCoverPhoto:
            self.selectView.hidden = YES;
            self.addPhotoView.hidden = YES;
            self.coverPhotoview.hidden = NO;
            self.editView.hidden = YES;
            if (asset) {
                self.coverPhotoEditorView.hidden = NO;
                self.coverPhotoHintView.hidden = YES;
                [RealUtility getImageBy:asset size:CGSizeMake(600, 600)  handler:^(UIImage *result, NSDictionary *info) {
                    self.coverPhotoEditorView.editingImage = result;
                    if (self.previousPhotoEditorScrollView) {
                        [self.coverPhotoEditorView restorePreviousScrollViewStatus:self.previousPhotoEditorScrollView];
                    }
                }];
            }else{
                self.coverPhotoEditorView.hidden = YES;
                self.coverPhotoHintView.hidden = NO;
            }
            break;
        case UploadPhotoCellEditView:{
            self.editView.hidden = NO;
            self.selectView.hidden = YES;
            self.addPhotoView.hidden = YES;
            self.coverPhotoview.hidden = YES;
            self.editCoverImageView.hidden = YES;
            [RealUtility getImageBy:asset size:CGSizeMake(400, 400) handler:^(UIImage *result, NSDictionary *info) {
                    self.editImageView.image =result;
            }];
            break;
        }case UploadPhotoCellEditCoverView:{
            self.editView.hidden = NO;
            self.selectView.hidden = YES;
            self.addPhotoView.hidden = YES;
            self.coverPhotoview.hidden = YES;
            [RealUtility getImageBy:asset size:CGSizeMake(400, 400) handler:^(UIImage *result, NSDictionary *info) {
                self.editImageView.image =result;
            }];
            self.editCoverImageView.hidden = NO;
            break;
        }default:
            break;
    }
}

-(void)photoEditorScrollViewDidChange:(UIScrollView *)scrollView{
    if ([self.delegate respondsToSelector:@selector(UploadPhotoCellDidSavePhotoEditorScrollView:)]) {
        [self.delegate UploadPhotoCellDidSavePhotoEditorScrollView:scrollView];
    }
}

-(void)enableEditMode:(BOOL)isEditing{
        self.removeButton.hidden = !isEditing;
    if(isEditing){
        [self startJiggling];
    }else{
        self.isJiggling = NO;
        [self stopJiggling];
    }
    
}


-(IBAction)removeButtoDidPress:(id)sender{
    if ([self.delegate respondsToSelector:@selector(UploadPhotoCellRemoveButtonDidPress:)]) {
        [self.delegate UploadPhotoCellRemoveButtonDidPress:self.indexPath];
    }
}

-(void)stopJiggling{
    self.isJiggling = NO;
    [self.layer removeAllAnimations];
    self.transform = CGAffineTransformIdentity;
}
-(void)startJiggling{

    if (!self.isJiggling) {
        NSInteger randomInt = arc4random_uniform(500);
        float r = (randomInt/500.0)+0.5;
        
        CGAffineTransform leftWobble = CGAffineTransformMakeRotation(degreesToRadians( (kAnimationRotateDeg * -1.0) - r ));
        CGAffineTransform rightWobble = CGAffineTransformMakeRotation(degreesToRadians( kAnimationRotateDeg + r ));
        
        self.transform = leftWobble;  // starting point
        
        [[self layer] setAnchorPoint:CGPointMake(0.5, 0.5)];
        
        [UIView animateWithDuration:0.1
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                         animations:^{
                             [UIView setAnimationRepeatCount:NSNotFound];
                             self.transform = rightWobble; }
                         completion:nil];
    }
}
@end
