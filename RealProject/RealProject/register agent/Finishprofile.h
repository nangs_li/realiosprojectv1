//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
#import <objc/message.h>

@interface Finishprofile : BaseViewController
@property(strong, nonatomic) IBOutlet UIImageView *personalphoto;
@property(strong, nonatomic) IBOutlet UILabel *membername;
@property(strong, nonatomic) IBOutlet UILabel *followercount;
@property(strong, nonatomic) IBOutlet UILabel *followingcount;


@end
