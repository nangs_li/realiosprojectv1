//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Finishprofile.h"
#import "Editprofile.h"
#import "Uploadphoto.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AgentProfile.h"
#import "HandleServerReturnString.h"
#import "Editprofilehaveprofile.h"
#import "BaseNavigationController.h"
@interface Finishprofile ()
@end
@implementation Finishprofile

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
//// set membername,photourl from NSUserDefaults

  self.membername.text = [LoginBySocialNetworkModel shared].myLoginInfo.MemberName;

  NSURL *url = [NSURL URLWithString:[LoginBySocialNetworkModel shared].myLoginInfo.PhotoURL];
    [self.personalphoto loadImageURL:url withIndicator:YES withCompletion:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
  [self.delegate setupcornerRadius:self.personalphoto];
  self.followercount.text = [NSString
      stringWithFormat:@"%d", [MyAgentProfileModel shared].myAgentProfile.FollowerCount];
  self.followingcount.text = [NSString
      stringWithFormat:@"%d", [MyAgentProfileModel shared].myAgentProfile.FollowerCount];

  UINavigationController *nc =
      (UINavigationController *)self.navigationController;
  [nc setViewControllers:@[ self ] animated:NO];
}

- (void)viewDidLoad {
  [super viewDidLoad];
}



#pragma mark Button

- (IBAction)createNowButton:(id)sender {
  Uploadphoto *UIViewController =
      [[Uploadphoto alloc] initWithNibName:@"Uploadphoto" bundle:nil];
  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionMoveIn;
  transition.subtype = kCATransitionFromTop;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self.navigationController pushViewController:UIViewController animated:NO];
}

- (IBAction)editProfilePressed:(id)sender {
  if ([MyAgentProfileModel shared].myAgentProfile.MemberID > 0) {
    Editprofilehaveprofile *editVC = [[Editprofilehaveprofile alloc]
        initWithNibName:@"Editprofilehaveprofile"
                 bundle:nil];
      editVC.underLayViewController = self;
      BaseNavigationController *navController = [[BaseNavigationController alloc]initWithRootViewController:editVC];
      navController.navigationBar.hidden = YES;
      [self.delegate overlayViewController:navController onViewController:self];

  } else {
    Editprofile *UIViewController =
        [[Editprofile alloc] initWithNibName:@"Editprofile" bundle:nil];
    CATransition *transition = [CATransition animation];
    transition.duration = ANIMATION_DURATION;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;

    [self.navigationController.view.layer addAnimation:transition
                                                forKey:kCATransition];

    [self.navigationController pushViewController:UIViewController animated:NO];
  }
}

@end