//
//  RealLoginViewController.h
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface RealLoginViewController : BaseViewController
@property (nonatomic,strong) IBOutlet UILabel *loginTitleLabel;
@property (nonatomic,strong) IBOutlet UITextField *emailTextField;
@property (nonatomic,strong) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *signUpBtn;
@property (strong, nonatomic) IBOutlet UILabel *loginTitle;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *textFieldErrorLabel;
@property (strong, nonatomic) IBOutlet UILabel *loginDesc;
@property (strong, nonatomic) IBOutlet UIButton*loginwithFacebookBtn;
@property (strong, nonatomic) IBOutlet UILabel *loginwithFacebookLabel;
@property (strong, nonatomic) IBOutlet UILabel *orLabel;
@property (strong, nonatomic) IBOutlet UIButton *loginBottomBtn;
@property (strong, nonatomic) UIButton *forgottenBtn;
@end
