//
//  RealLoginViewController.m
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealLoginViewController.h"
#import "RealSignUpViewController.h"
#import "RealResetPasswordViewController.h"
#import "LoginByEmailModel.h"
#import "LoginBySocialNetworkModel.h"
#import "LoginByEmailModel.h"
#import "FacebookLoginModel.h"


// Mixpanel
#import "Mixpanel.h"

@interface RealLoginViewController () <UITextFieldDelegate>

@end

@implementation RealLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.loginBottomBtn setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self.loginBottomBtn setBackgroundImage:[UIImage imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateDisabled];
    [self.signUpBtn setBackgroundImage:[UIImage imageWithBorder:self.signUpBtn.titleLabel.textColor
                                                             size:self.signUpBtn.frame.size] forState:UIControlStateNormal];
    NSString *loginTitle =JMOLocalizedString(@"login__login", nil);
    [self.loginTitleLabel setAttributedTextWithMarkUp:loginTitle];
    [[AppDelegate getAppDelegate]setUpBlueAndWhiteTitleFontSizeUILabel:self.loginTitleLabel];
//    self.emailTextField.delegate = self;
    self.emailTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.emailTextField.frame.size];
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor textFieldPlaceholderColor]}];
    self.emailTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.passwordTextField.delegate = self;
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor textFieldPlaceholderColor]}];
    self.passwordTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.passwordTextField.frame.size];
    self.passwordTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    UIButton *forgottenButton  =[UIButton buttonWithType:UIButtonTypeCustom];
    [forgottenButton setTitle:@"Forgotten?" forState:UIControlStateNormal];
    forgottenButton.frame = CGRectMake(0, 0, 100, self.passwordTextField.frame.size.height);
    forgottenButton.titleLabel.font =[UIFont systemFontOfSize:14];
    [forgottenButton addTarget:self action:@selector(forgottenButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    [forgottenButton setTitleColor:RealLightBlueColor forState:UIControlStateNormal];
    self.forgottenBtn=forgottenButton;
    self.passwordTextField.rightView =forgottenButton;
    self.passwordTextField.rightViewMode = UITextFieldViewModeAlways;
    // Do any additional setup after loading the view from its nib.
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    NSString *loginString =JMOLocalizedString(@"login__signin", nil);
    self.loginTitle.text = loginString; 
    self.loginDesc.text = JMOLocalizedString(@"common__desc_discover_real", nil);
    self.loginwithFacebookLabel.text = JMOLocalizedString(@"login__login_facebook", nil);
    self.orLabel.text = JMOLocalizedString(@"login__or", nil);
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common__email", nil) attributes:@{NSForegroundColorAttributeName: [UIColor textFieldPlaceholderColor]}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common_password", nil) attributes:@{NSForegroundColorAttributeName: [UIColor textFieldPlaceholderColor]}];

   [ self.forgottenBtn setTitle:JMOLocalizedString(@"login__forgotten", nil) forState:UIControlStateNormal];
       NSString *loginTitle =JMOLocalizedString(@"login__login", nil);
    [self.loginTitleLabel setAttributedTextWithMarkUp:loginTitle];
    [self.loginBottomBtn setTitle:loginString forState:UIControlStateNormal];
    
}

-(IBAction)loginButtonDidPress:(id)sender{
    
    // Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Logged in with Email"];
    
    if ([self checkInputIsValidAlertTitle:YES]) {
        [self showLoadingHUD];
        NSString *email = self.emailTextField.text;
        NSString *password = self.passwordTextField.text;
        
        [[LoginByEmailModel shared]callRealNetworkMemberLoginEmail:email passwordMD5:password Success:^(RealNetworkMemberInfo *realNetworkMemberInfo) {
            [self loginBySocialNetwork:realNetworkMemberInfo];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert, NSString *ok) {
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];

            [self hideLoadingHUD];
        }];
    }
}

-(void)loginBySocialNetwork:(RealNetworkMemberInfo*)memberInfo{
    NSString *name = [NSString stringWithFormat:@"%@ %@",memberInfo.FirstName,memberInfo.LastName];
    [[LoginBySocialNetworkModel shared]
     callLoginBySocialNetworkModelApiDeviceType:@"1"
     deviceToken:self.delegate.devicetoken
     socialNetworkType:@"6"
     userName:name
     userID:memberInfo.UserID
     email:memberInfo.Email
     photoURL:memberInfo.PhotoURL
     accessToken:memberInfo.AccessToken
     Success:^(LoginInfo *myLoginInfo, SystemSettings *SystemSettings) {
         [self registerQBWithInfo:myLoginInfo];
         
         Mixpanel *mixpanel = [Mixpanel sharedInstance];
         [mixpanel identify:myLoginInfo.MemberID.stringValue];
         [mixpanel track:@"Verified Email Login"];

     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error,
               NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
               NSString *ok) {
         [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
          [self hideLoadingHUD];
     }];
}

-(IBAction)signupButtonDidPress:(id)sender{
//    RealSignUpViewController *signupVC = [[RealSignUpViewController alloc]initWithNibName:@"RealSignUpViewController" bundle:nil];
//    [self.navigationController pushViewController:signupVC animated:YES];
}

-(IBAction)backButtonDidPress:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)forgottenButtonDidPress:(id)sender{
    RealResetPasswordViewController *resetVC = [[RealResetPasswordViewController alloc]initWithNibName:@"RealResetPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:resetVC animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.passwordTextField == textField) {
        return [self passwordFormatIsValid:string];
    }else{
        return YES;
    }
}
-(BOOL)passwordFormatIsValid:(NSString*)password{
    NSString *passwordRegex = @"[A-Z0-9a-z_!]*"; ;
    NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordPredicate   evaluateWithObject:password];
}

- (BOOL)emailFormatIsValid:(NSString*)email{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/va ... l-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)facebookLoginPress:(id)sender {
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Logged in with Facebook"];
    
    if ([self.delegate networkConnection]) {
        [self showLoadingHUD];
        [[FacebookLoginModel shared] facebookLoginPressSuccess:^(LoginInfo *myLoginInfo, SystemSettings *SystemSettings) {
             [[LoginBySocialNetworkModel shared] loginByQuickBloxOrSignUpQuickBloxWithInfo:myLoginInfo Success:^(id responseObject){
                 [self checkContactListRequestAccess];
             } failure:^(AFHTTPRequestOperation *operation, NSError *error,
                         NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
                         NSString *ok) {
                 [self hideLoadingHUD];
                 [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
                 
             }];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error,
                   NSString *errorMessage, RequestErrorStatus errorstatus, NSString *alert,
                   NSString *ok) {
             [self hideLoadingHUD];
             [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:errorMessage errorStatus:errorstatus alert:alert ok:ok];
             
         } fromViewController:self.delegate.rootViewController];

    }else{
        
       
        [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view errorMessage:JMOLocalizedString(NotNetWorkConnectionText, nil) errorStatus:NotNetWorkConnection alert:JMOLocalizedString(@"alert_alert", nil) ok:JMOLocalizedString(@"alert_ok", nil)];
    }
   
}
- (IBAction)signUpBtnPress:(id)sender {
    RealSignUpViewController *loginVC = [[RealSignUpViewController alloc]initWithNibName:@"RealSignUpViewController" bundle:nil];
    [self.navigationController pushViewController:loginVC animated:YES];
    
}
-(void)setupTextFieldCheckError{
    [self.emailTextField.rac_textSignal
    
     subscribeNext:^(id x) {
         if (([self checkInputIsValidAlertTitle:NO])) {
              self.textFieldErrorLabel.text=@"";
         }
         
     }];
    [self.passwordTextField.rac_textSignal
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidAlertTitle:NO])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];

}

-(BOOL)checkInputIsValidAlertTitle:(BOOL)show{
    NSString *password = self.passwordTextField.text;
    int passwordMinLength = 4;
    int passwordMaxLength = 20;
    BOOL valid = YES;
    if(![RealUtility isValid:self.emailTextField.text]){
        self.textFieldErrorLabel.text=JMOLocalizedString(Emailisrequried, nil);
        
        
        valid = NO;
    }else if([RealUtility isValid:self.emailTextField.text] && ![self emailFormatIsValid:self.emailTextField.text]){
        valid = NO;
         self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteravalidemailaddress, nil);
        
    }else if (![RealUtility isValid:password]){
         self.textFieldErrorLabel.text=JMOLocalizedString(Passwordisrequired, nil);
        
        valid= NO;
    }else if([RealUtility isValid:password]){
        if (password.length < passwordMinLength || password.length > passwordMaxLength ) {
             self.textFieldErrorLabel.text=JMOLocalizedString(PasswordlengthmustbebetweenCharacters, nil);
            
            valid = NO;
        }
    }
    if (show&&!valid) {
    [self showAlertWithTitle:nil message:self.textFieldErrorLabel.text];
    }
   
    return valid;
}
-(void)setupKeyBoardDoneBtnAction{
    
     [self.passwordTextField setReturnKeyType:UIReturnKeyDone];
    [[self.passwordTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x){
        
        [self loginButtonDidPress:self];
    }];
}
@end
