//
//  RealResetPasswordViewController.h
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface RealResetPasswordViewController : BaseViewController
@property (nonatomic,strong) IBOutlet UILabel *resetTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *resetDescriptionLabel;
@property (nonatomic,strong) IBOutlet UITextField *emailTextField;
@property (nonatomic,strong) IBOutlet UIImageView *loginBGImageView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *textFieldErrorLabel;
@property (strong, nonatomic) IBOutlet UILabel *signINHelpLabel;
@property (strong, nonatomic) IBOutlet UILabel *enterEmailAssociatedLabel;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@end
