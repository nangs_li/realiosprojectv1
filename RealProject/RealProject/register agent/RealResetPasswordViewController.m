//
//  RealResetPasswordViewController.m
//  productionreal2
//
//  Created by Alex Hung on 29/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "RealResetPasswordViewController.h"
#import "LoginByEmailModel.h"
#define textFieldPlaceHolderColor [UIColor colorWithRed:193/255.0 green:193/255.0 blue:193/255.0 alpha:1.0]

@interface RealResetPasswordViewController ()

@end

@implementation RealResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateDisabled];
    
    NSString *resetTitle =JMOLocalizedString(@"sign_up__resetpassword", nil);
    [self.resetTitleLabel setAttributedTextWithMarkUp:resetTitle];
   
    [[AppDelegate getAppDelegate]setUpBlueAndWhiteTitleFontSizeUILabel:self.resetTitleLabel];
//    self.emailTextField.delegate = self;
    self.emailTextField.background =[UIImage imageWithBorder:[UIColor whiteColor] size:self.emailTextField.frame.size];
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"common__email", nil) attributes:@{NSForegroundColorAttributeName: textFieldPlaceHolderColor}];
    self.emailTextField.leftView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 5)];
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    // Do any additional setup after loading the view from its nib.
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=NO;
     }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil]
      takeUntil:[self rac_willDeallocSignal]]
     subscribeNext:^(id x) {
         self.scrollView.scrollEnabled=YES;
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    self.signINHelpLabel.text=JMOLocalizedString(@"forgot_password__sign_in_help", nil);
    self.enterEmailAssociatedLabel.text=JMOLocalizedString(@"forgot_password__desc", nil);
    self.emailTextField.placeholder=JMOLocalizedString(@"common__email", nil);
    [self.submitBtn setTitle:JMOLocalizedString(@"common__submit", nil)
                     forState:UIControlStateNormal];
    
}
-(IBAction)submitButtonDidPress:(id)sender{
    if ([self checkInputIsValid]) {
        NSString *email = self.emailTextField.text;
        [[LoginByEmailModel shared]callRealNetworkSendResetPasswordEmailApiEmail:email Success:^(id responseObject) {
           UIAlertAction *action = [UIAlertAction actionWithTitle:JMOLocalizedString(@"alert_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self backButtonDidPress:nil];
            }];
            [self showAlertWithTitle:JMOLocalizedString(Resetemailhasbeesent, nil) message:JMOLocalizedString(Tocontinuepleasecheckyouremailandfollowtheinstructions, nil) withAction:action];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorMessage, RequestErrorStatus errorStatus, NSString *alert, NSString *ok) {
           // [self showAlertWithTitle:nil message:errorMessage];
            [self.delegate handleModelReturnErrorShowUIAlertViewByView:self.view
                                                          errorMessage:errorMessage
                                                           errorStatus:errorStatus
                                                                 alert:alert
                                                                    ok:ok];
        }];
    }
}

-(IBAction)backButtonDidPress:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkInputIsValid{
    BOOL valid = YES;
    NSString *email = self.emailTextField.text;
    if (![RealUtility isValid:email]) {
        valid = NO;
        [self showAlertWithTitle:nil message:JMOLocalizedString(Emailisrequried, nil)];
    }else if(![self emailFormatIsValid:email]){
        valid = NO;
        [self showAlertWithTitle:nil message:JMOLocalizedString(Pleaseenteravalidemailaddress, nil)];
    }
    
    return valid;
}

- (BOOL)emailFormatIsValid:(NSString*)email{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/va ... l-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(void)setupTextFieldCheckError{
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEnd|UIControlEventEditingDidEndOnExit]
     
     subscribeNext:^(id x) {
         if (([self checkInputIsValidNotAlertTitle])) {
             self.textFieldErrorLabel.text=@"";
         }
         
     }];
    
}
- (BOOL)checkInputIsValidNotAlertTitle{
    BOOL valid = YES;
    NSString *email = self.emailTextField.text;
    if (![RealUtility isValid:email]) {
        valid = NO;
       self.textFieldErrorLabel.text=JMOLocalizedString(Emailisrequried, nil);
    }else if(![self emailFormatIsValid:email]){
        valid = NO;
       self.textFieldErrorLabel.text=JMOLocalizedString(Pleaseenteravalidemailaddress, nil);
    }
    
    return valid;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setupKeyBoardDoneBtnAction{
     [self.emailTextField setReturnKeyType:UIReturnKeyDone];
    [[self.emailTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x){
        
        [self submitButtonDidPress:self];
    }];
}
@end
