//
//  SearchTutorialViewController.h
//  productionreal2
//
//  Created by Alex Hung on 5/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SearchTutorialViewController : BaseViewController
@property (nonatomic,strong) IBOutlet UIImageView *navigationTipsImageView;
@property (strong, nonatomic) IBOutlet UILabel *enterCountryDescTopLabel;
@property (strong, nonatomic) IBOutlet UIButton *topSearchButton;

@property (strong, nonatomic) IBOutlet UILabel *navigationTipsLabel;
@property (strong, nonatomic) IBOutlet UILabel *youCanSwipeToViewLabel;
@property (strong, nonatomic) IBOutlet UIButton *bottomButton;

-(IBAction)startButtonDidPress:(id)sender;
@end
