//
//  SearchTutorialViewController.m
//  productionreal2
//
//  Created by Alex Hung on 5/11/2015.
//  Copyright © 2015 Real. All rights reserved.
//

#import "SearchTutorialViewController.h"

@interface SearchTutorialViewController ()

@end

@implementation SearchTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    animationImageView.animationImages = images;
//    animationImageView.animationDuration = 0.5;
    
    
    self.navigationTipsImageView.animationImages =@[[UIImage imageNamed:@"tutorial_swipe01"],[UIImage imageNamed:@"tutorial_swipe02"],[UIImage imageNamed:@"tutorial_swipe03"],[UIImage imageNamed:@"tutorial_swipe02"]];
    self.navigationTipsImageView.animationDuration =1.0;
    [self.navigationTipsImageView startAnimating];
    [self.bottomButton setBackgroundImage:[UIImage imageWithColor:RealBlueColor] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpFixedLabelTextAccordingToSelectedLanguage{
    [self.topSearchButton setTitle:JMOLocalizedString(@"common__search", nil)forState:UIControlStateNormal];
    self.enterCountryDescTopLabel.text =JMOLocalizedString(@"tutorial__desc", nil);
      self.navigationTipsLabel.text =JMOLocalizedString(@"tutorial__navigation_tips", nil);
    self.youCanSwipeToViewLabel.text =JMOLocalizedString(@"tutorial__swipe_desc", nil);
    [self.bottomButton setTitle:JMOLocalizedString(@"tutorial__Start_Now", nil)forState:UIControlStateNormal];

    
}
-(IBAction)startButtonDidPress:(id)sender{

    [kAppDelegate userDidReadTutorial:TutorialTypeSearch];
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
