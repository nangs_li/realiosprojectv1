#!/bin/bash

#Creates an iOS OTA (Over the Air) Minifest File

url="$1"
plist_path="$2"
bundle_id="$3"
bundle_version="1" #can be anything
app_name="Real" #can be anything

rm -rf $plist_path

/usr/libexec/PlistBuddy -c "add :items array" $plist_path
/usr/libexec/PlistBuddy -c "add :items:0:assets array" $plist_path
/usr/libexec/PlistBuddy -c "add :items:0:assets:0::kind string software-package" $plist_path
/usr/libexec/PlistBuddy -c "add :items:0:assets:0::url string ${url}" $plist_path
/usr/libexec/PlistBuddy -c "add :items:0:metadata:bundle-identifier string ${bundle_id}" $plist_path
/usr/libexec/PlistBuddy -c "add :items:0:metadata:bundle-version string ${bundle_version}" $plist_path
/usr/libexec/PlistBuddy -c "add :items:0:metadata:kind string software" $plist_path
/usr/libexec/PlistBuddy -c "add :items:0:metadata:title string ${app_name}" $plist_path