#pragma mark getChatmessagefromdbnotscroll
- (void)getChatmessagefromdbnotscroll {
    
    DBResultSet *r = [[[[DBQBChatMessage query]
                        whereWithFormat:@" DialogID = %@", self.dialog.ID] orderBy:@"DateSent"]
                      
                      fetch];
    
    if (r.count == 0) {
        
        [self loadingstartsecond:30];
    }
    
    /*
     [[[[DBQBChatHistoryMessage query]
     whereWithFormat:@" DialogID = %@",self.dialog.ID]
     fetch]removeAll];
     */
    
    NSMutableArray *messagearray = [[NSMutableArray alloc] init];
    
    for (DBQBChatMessage *p in r) {
        
        [messagearray addObject:p];
        //      DLog(@"DBQBChatDialog-->%@",p);
    }
    DLog(@"messagearray count-->%lu", (unsigned long)[messagearray count]);
    if (!self.enterchatroomfromchatbuttondialognottargetID) {
        [[ChatService shared].messages removeObjectForKey:self.dialog.ID];
        [[ChatService shared]
         .messages setObject:messagearray
         forKey:self.dialog.ID];
    }
    [self.collectionView reloadData];
}
#pragma mark set draw message YES
- (void)drawmessagesetyes {
    NSMutableArray *messagearray =
    [[ChatService shared].messages objectForKey:self.dialog.ID];
    
    for (DBQBChatMessage *historyMessage in
         [messagearray reverseObjectEnumerator]) {
        if (!historyMessage.DrawStatus) {
            historyMessage.DrawStatus = YES;
            [historyMessage commit];
        } else {
        }
    }
}
#pragma mark set remove unreadlabel
- (void)removeunreadlabel {
    NSMutableArray *messagearray =
    [[ChatService shared].messages objectForKey:self.dialog.ID];
    
    for (DBQBChatMessage *historyMessage in
         [messagearray reverseObjectEnumerator]) {
        if ([historyMessage.MessageID isEqualToString:@"unreadlabel"]) {
            
            [historyMessage remove];
        }
    }
    
    [self.collectionView reloadData];
}

#pragma mark - Actions

- (void)receiveMessagePressed:(UIBarButtonItem *)sender {
    /**
     *  DEMO ONLY
     *
     *  The following is simply to simulate received messages for the demo.
     *  Do not actually do this.
     */
    
    /**
     *  Show the typing indicator to be shown
     */
    self.showTypingIndicator = !self.showTypingIndicator;
    
    /**
     *  Scroll to actually view the indicator
     */
    [self scrollToBottomAnimated:YES];
    
    /**
     *  Copy last sent message, this will be the new "received" message
     */
    
    JSQMessage *copyMessage = nil;
    /*
     JSQMessage *copyMessage =
     [[[ChatService shared].chatroomJsqMessageArray lastObject] copy];
     */
    
    if (!copyMessage) {
        copyMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdJobs
                                          displayName:kJSQDemoAvatarDisplayNameJobs
                                                 text:@"First received!"];
    }
    
    /**
     *  Allow typing indicator to show
     */
    dispatch_after(
                   dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                   dispatch_get_main_queue(), ^{
                       
                       NSMutableArray *userIds =
                       [[[ChatService shared].jsqusers allKeys] mutableCopy];
                       [userIds removeObject:self.senderId];
                       NSString *randomUserId =
                       userIds[arc4random_uniform((int)[userIds count])];
                       
                       JSQMessage *newMessage = nil;
                       id<JSQMessageMediaData> newMediaData = nil;
                       id newMediaAttachmentCopy = nil;
                       
                       if (copyMessage.isMediaMessage) {
                           /**
                            *  Last message was a media message
                            */
                           id<JSQMessageMediaData> copyMediaData = copyMessage.media;
                           
                           if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                               JSQPhotoMediaItem *photoItemCopy =
                               [((JSQPhotoMediaItem *)copyMediaData)copy];
                               photoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                               newMediaAttachmentCopy =
                               [UIImage imageWithCGImage:photoItemCopy.image.CGImage];
                               
                               /**
                                *  Set image to nil to simulate "downloading" the image
                                *  and show the placeholder view
                                */
                               photoItemCopy.image = nil;
                               
                               newMediaData = photoItemCopy;
                           } else if ([copyMediaData
                                       isKindOfClass:[JSQLocationMediaItem class]]) {
                               JSQLocationMediaItem *locationItemCopy =
                               [((JSQLocationMediaItem *)copyMediaData)copy];
                               locationItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                               newMediaAttachmentCopy = [locationItemCopy.location copy];
                               
                               /**
                                *  Set location to nil to simulate "downloading" the location data
                                */
                               locationItemCopy.location = nil;
                               
                               newMediaData = locationItemCopy;
                           } else if ([copyMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                               JSQVideoMediaItem *videoItemCopy =
                               [((JSQVideoMediaItem *)copyMediaData)copy];
                               videoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                               newMediaAttachmentCopy = [videoItemCopy.fileURL copy];
                               
                               /**
                                *  Reset video item to simulate "downloading" the video
                                */
                               videoItemCopy.fileURL = nil;
                               videoItemCopy.isReadyToPlay = NO;
                               
                               newMediaData = videoItemCopy;
                           } else {
                               NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
                           }
                           
                           newMessage = [JSQMessage
                                         messageWithSenderId:randomUserId
                                         displayName:[ChatService shared].jsqusers[randomUserId]
                                         media:newMediaData];
                       } else {
                           /**
                            *  Last message was a text message
                            */
                           newMessage = [JSQMessage
                                         messageWithSenderId:randomUserId
                                         displayName:[ChatService shared].jsqusers[randomUserId]
                                         text:copyMessage.text];
                       }
                       
                       /**
                        *  Upon receiving a message, you should:
                        *
                        *  1. Play sound (optional)
                        *  2. Add new id<JSQMessageData> object to your data source
                        *  3. Call `finishReceivingMessage`
                        */
                       [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
                       //  [[ChatService shared].chatroomJsqMessageArray addObject:newMessage];
                       [self finishReceivingMessageAnimated:YES];
                       
                       if (newMessage.isMediaMessage) {
                           /**
                            *  Simulate "downloading" media
                            */
                           dispatch_after(
                                          dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)),
                                          dispatch_get_main_queue(), ^{
                                              /**
                                               *  Media is "finished downloading", re-display visible cells
                                               *
                                               *  If media cell is not visible, the next time it is dequeued
                                               *the view controller will display its new attachment data
                                               *
                                               *  Reload the specific item, or simply call `reloadData`
                                               */
                                              
                                              if ([newMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                                                  ((JSQPhotoMediaItem *)newMediaData).image =
                                                  newMediaAttachmentCopy;
                                                  [self.collectionView reloadData];
                                              } else if ([newMediaData
                                                          isKindOfClass:[JSQLocationMediaItem class]]) {
                                                  [((JSQLocationMediaItem *)newMediaData)
                                                   setLocation:newMediaAttachmentCopy
                                                   withCompletionHandler:^{
                                                       [self.collectionView reloadData];
                                                   }];
                                              } else if ([newMediaData
                                                          isKindOfClass:[JSQVideoMediaItem class]]) {
                                                  ((JSQVideoMediaItem *)newMediaData).fileURL =
                                                  newMediaAttachmentCopy;
                                                  ((JSQVideoMediaItem *)newMediaData).isReadyToPlay = YES;
                                                  [self.collectionView reloadData];
                                              } else {
                                                  NSLog(@"%s error: unrecognized media item",
                                                        __PRETTY_FUNCTION__);
                                              }
                                              
                                          });
                       }
                       
                   });
}







//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

/*
 #pragma mark
 #pragma mark UITableViewDelegate & UITableViewDataSource
 
 - (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
 if ([self IsEmpty:[[ChatService shared]
 .messages objectForKey:self.dialog.ID]]) {
 return 0;
 }
 
 return [[[ChatService shared] messagsForDialogId:self.dialog.ID] count];
 }
 
 - (UITableViewCell *)tableView:(UITableView *)tableView
 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 // //*****bug perpertcell replace indexpath.row==0 messages cell
 DLog(@"cellForRowAtIndexPath%ld", (long)indexPath.row);
 
 DBQBChatMessage *message =
 [[[ChatService shared] messagsForDialogId:self.dialog.ID]
 objectAtIndex:indexPath.row];
 
 static NSString *ChatMessageCellIdentifier = @"ChatMessageCellIdentifier";
 
 ChatMessageTableViewCell *cell =
 [tableView dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier];
 if (cell == nil) {
 
 cell = [[ChatMessageTableViewCell alloc]
 initWithStyle:UITableViewCellStyleDefault
 reuseIdentifier:UITableViewCellStyleDefault];
 }
 if (![self IsEmpty:message]) {
 // DLog(@"DBQBChatHistoryMessage:%@",message);
 
 
 
 if ([message.MessageID isEqualToString:@"unreadlabel"]) {
 static NSString *ChatMessageCellIdentifier =
 @"UnreadMessageCell";
 
 UnreadMessageCell *cell = [tableView
 dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier];
 if (cell == nil) {
 NSArray *nib = [[NSBundle mainBundle]
 loadNibNamed:@"UnreadMessageCell" owner:self options:nil];
 cell  =(UnreadMessageCell *)[nib objectAtIndex:0];
 
 
 }
 
 [cell configureCellWithMessage:message];
 return cell;
 }else{
 
 
 
 
 if (![self IsEmpty:message.Attachments]) {
 
 static NSString *ChatMessageCellIdentifier = @"PhotoMessageCell";
 
 PhotoMessageCell *cell = [tableView
 dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier];
 if (cell == nil) {
 NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PhotoMessageCell"
 owner:self
 options:nil];
 cell = (PhotoMessageCell *)[nib objectAtIndex:0];
 
 [cell configureCellWithMessage:message];
 return cell;
 }
 
 } else {
 
 [cell configureCellWithMessage:message];
 if (![_messageWholeday
 isEqualToString:[_messageDateFormatter
 stringFromDate:message.DateSent]]) {
 DLog(@"![_messageWholeday isEqualToString:[_messageDateFormatter "
 @"stringFromDate:message.datetime]");
 
 _messageWholeday =
 [_messageDateFormatter stringFromDate:message.DateSent];
 [cell
 setupdate:[_messageDateFormatter stringFromDate:message.DateSent]];
 }
 
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
 if (message.NumberofDeliverStatus == 1) {
 static NSString *ChatMessageCellIdentifier =
 @"ChatMessageErrorStatusTableViewCell";
 
 ChatMessageErrorStatusTableViewCell *cell = [tableView
 dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier];
 if (cell == nil) {
 
 cell = [[ChatMessageErrorStatusTableViewCell alloc]
 initWithStyle:UITableViewCellStyleDefault
 reuseIdentifier:UITableViewCellStyleDefault];
 [cell.warning addTarget:self
 action:@selector(clickMe:)
 forControlEvents:UIControlEventTouchUpInside];
 }
 
 [cell configureCellWithMessage:message];
 if (![_messageWholeday
 isEqualToString:[_messageDateFormatter
 stringFromDate:message.DateSent]]) {
 DLog(@"![_messageWholeday isEqualToString:[_messageDateFormatter "
 @"stringFromDate:message.datetime]");
 
 _messageWholeday =
 [_messageDateFormatter stringFromDate:message.DateSent];
 [cell setupdate:[_messageDateFormatter
 stringFromDate:message.DateSent]];
 }
 
 return cell;
 // }
 }
 }
 }
 //  }
 return cell;
 }
 - (void)clickMe:(UIButton *)sender {
 
 ChatMessageErrorStatusTableViewCell *cell =
 (ChatMessageErrorStatusTableViewCell *)[[sender superview] superview];
 // NSIndexPath *indexPath = [self.messagesTableView indexPathForCell:cell];
 
 self.selectederrorchatmessage =
 [[[ChatService shared] messagsForDialogId:self.dialog.ID]
 objectAtIndex:indexPath.row];
 
 UIActionSheet *actionSheet =
 [[UIActionSheet alloc] initWithTitle:@"Your message was not sent."
 delegate:self
 cancelButtonTitle:@"Cancel"
 destructiveButtonTitle:@"Delete"
 otherButtonTitles:@"Send again", nil];
 
 [actionSheet showInView:self.view];
 }
 - (CGFloat)tableView:(UITableView *)tableView
 heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 DBQBChatMessage *chatMessage =
 [[[ChatService shared] messagsForDialogId:self.dialog.ID]
 objectAtIndex:indexPath.row];
 CGFloat cellHeight =
 [ChatMessageTableViewCell heightForCellWithMessage:chatMessage];
 
 if (![self IsEmpty:chatMessage.Attachments]) {
 CGFloat cellHeight =
 [PhotoMessageCell heightForCellWithMessage:chatMessage];
 
 return cellHeight;
 }
 
 DLog(@"cellHeight-->%f", cellHeight);
 return cellHeight;
 }
 
 - (void)tableView:(UITableView *)tableView
 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 [tableView deselectRowAtIndexPath:indexPath animated:NO];
 }
 
 #pragma mark UIActionsheet
 - (void)actionSheet:(UIActionSheet *)actionSheet
 willDismissWithButtonIndex:(NSInteger)buttonIndex {
 
 DLog(@"buutonIndex-->%ld", (long)buttonIndex);
 
 if (buttonIndex == 0) {
 [self.selectederrorchatmessage remove];
 //  [self.messagesTableView reloadData];
 }
 
 if (buttonIndex == 1) {
 
 QBChatMessage *message = [QBChatMessage markableMessage];
 
 NSMutableDictionary *params = [NSMutableDictionary dictionary];
 params[@"save_to_history"] = @YES;
 [message setCustomParameters:params];
 
 // 1-1 Chat
 if (self.dialog.type == QBChatDialogTypePrivate) {
 // send message
 message.recipientID = [self.dialog recipientID];
 message.senderID = [QBSession currentSession].currentUser.ID;
 message.text = self.selectederrorchatmessage.Text;
 #pragma save message into DB
 DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
 
 dbhistorymessage.MessageID = message.ID;
 dbhistorymessage.DialogID = self.dialog.ID;
 dbhistorymessage.QBID = [AppDelegate getAppDelegate].myLoginInfo.QBID;
 dbhistorymessage.MemberID = [self.delegate.myLoginInfo.MemberID intValue];
 dbhistorymessage.RecipientID = [self.dialog recipientID];
 dbhistorymessage.SenderID = [QBSession currentSession].currentUser.ID;
 dbhistorymessage.DateSent = message.dateSent;
 dbhistorymessage.InsertDBDateTime = [NSDate date];
 
 dbhistorymessage.QBChatHistoryMessage =
 [NSKeyedArchiver archivedDataWithRootObject:[message copy]];
 
 dbhistorymessage.Text = self.selectederrorchatmessage.Text;
 dbhistorymessage.CustomParameters = message.customParameters;
 dbhistorymessage.Attachments = message.attachments;
 dbhistorymessage.NumberofDeliverStatus = 1;
 dbhistorymessage.DidDeliverStatus = NO;
 dbhistorymessage.ErrorStatus = NO;
 dbhistorymessage.DrawStatus = YES;
 dbhistorymessage.ReadStatus = NO;
 dbhistorymessage.Retrycount = 0;
 
 // message is sent
 
 if ([self.dialog
 sendMessage:message
 sentBlock:^(NSError *error) {
 DLog(@"sendMessageerror-->1%@", error);
 if (![[AppDelegate getAppDelegate] IsEmpty:error]) {
 
 UIAlertView *alert = [[UIAlertView alloc]
 initWithTitle:@"Alert"
 message:@"Send Message Fail,Please try again."
 delegate:nil
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 }
 
 }]) {
 DLog(@"sendmessageok");
 dbhistorymessage.DateSent = [NSDate date];
 
 dbhistorymessage.NumberofDeliverStatus = 2;
 
 [dbhistorymessage commit];
 
 [[ChatService shared] addMessage:dbhistorymessage
 forDialogId:self.dialog.ID];
 [self.selectederrorchatmessage remove];
 
 [self getChatmessagefromdbnotscroll];
 }
 }
 }
 if (buttonIndex == 2) {
 }
 }
 
 */
